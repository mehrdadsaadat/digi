<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
        ['title'=>'مدیریت انبار ها','url'=>url('admin/stockrooms')],
        ['title'=>'لیست محصولات','url'=>url('admin/stockrooms/'.$stockroom->id)]
        ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

         <?php
             $Jdf=new \App\Lib\Jdf()
         ?>
        <div class="header">
            لیست محصولات موجود در <?php echo e($stockroom->name); ?>

        </div>

        <div class="panel_content">

            <form method="get" class="search_form search_item">
                <div class="form-group">
                    <?php echo e(Form::select('seller_id',$seller,$req->get('seller_id',''),['class'=>'selectpicker auto_width'])); ?>

                </div>
                <input type="text" name="search_text" class="form-control" value="<?php echo e($req->get('search_text','')); ?>" placeholder="نام محصول ..."><button class="btn btn-primary">جست و جو</button>
            </form>


            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>تصویر محصول</th>
                        <th>عنوان محصول</th>
                        <th>فروشنده</th>
                        <th>گارانتی</th>
                        <th>رنگ</th>
                        <th>تعداد</th>
                    </tr>
                </thead>
                <tbody>
                   <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>
                   <?php $__currentLoopData = $inventory_lists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <?php $i++; ?>
                       <tr>
                           <td><?php echo e(replace_number($i)); ?></td>
                           <td>
                             <img src="<?php echo e(url('files/thumbnails/'.$value->getProductWarranty->getProduct->image_url)); ?>" class="product_pic stockroom_product">
                           </td>
                           <td><?php echo e($value->getProductWarranty->getProduct->title); ?></td>
                           <td><?php echo e($value->getProductWarranty->getSeller->brand_name); ?></td>
                           <td><?php echo e($value->getProductWarranty->getWarranty->name); ?></td>
                           <td style="width:150px">
                                <?php if($value->getProductWarranty->getColor->id>0): ?>
                                  <?php if($value->getProductWarranty->getColor->type==1): ?>
                                       <span style="background:#<?php echo e($value->getProductWarranty->getColor->code); ?>" class="color_td">
                                       <span style="color:white"><?php echo e($value->getProductWarranty->getColor->name); ?></span>
                                   </span>
                                   <?php else: ?>
                                      <span>سایز : <?php echo e($value->getProductWarranty->getColor->name); ?></span>
                                  <?php endif; ?>
                                <?php endif; ?>
                           </td>
                           <td><?php echo e(replace_number($value->product_count)); ?></td>
                       </tr>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                   <?php if(sizeof($inventory_lists)==0): ?>
                   <tr>
                       <td colspan="7">رکوردی برای نمایش یافت نشد</td>
                   </tr>
                   <?php endif; ?>
                </tbody>
            </table>

            <?php echo e($inventory_lists->links()); ?>


        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/stockroom/list.blade.php ENDPATH**/ ?>