<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
class PageController extends CustomController
{
    protected  $model='Page';
    protected  $title='صفحه';
    protected $route_params='pages';
    public function index(Request $request)
    {
        $pages=Page::getData($request->all());
        $trash_page_count=Page::onlyTrashed()->count();
        return view('pages.index',['pages'=>$pages,'trash_page_count'=>$trash_page_count,'req'=>$request]);
    }
    public function create()
    {
        return view('pages.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,['title'=>['required','unique:pages'],'content'=>'required'],[],['title'=>'عنوان صفحه','content'=>'محتوای صفحه']);
        $page=new Page($request->all());
        $page_url=get_url($request->get('title'));
        $page->url=$page_url;
        $page->saveOrFail();
        return redirect('admin/pages')->with('message','ثبت صفحه با موفقیت انجام شد');
    }
    public function edit($id){
        $page=Page::findOrFail($id);
        return view('pages.edit',['page'=>$page]);
    }
    public function update($id,Request $request)
    {
        $page=Page::findOrFail($id);
        $this->validate($request,['title'=>['required','unique:pages,title,'.$id.''],'content'=>'required'],[],['title'=>'عنوان صفحه','content'=>'محتوای صفحه']);
        $page_url=get_url($request->get('title'));
        $page->url=$page_url;
        $page->update($request->all());
        return redirect('admin/pages')->with('message','ویرایش صفحه با موفقیت انجام شد');
    }
}
