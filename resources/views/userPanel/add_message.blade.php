@extends('layouts.shop')

@section('content')

    <div class="row">

        <div class="col-md-3">
            @include('include.user_panel_menu',['active'=>'messages'])
        </div>
        <div class="col-md-9" style="padding-right: 0px">

            <div class="profile_menu">

                <span class="profile_menu_title">ارسال پیام جدید</span>

                    <div class="message_form">

                    {!! Form::open(['url' => 'user/profile/messages','files'=>true]) !!}

                     <div class="form-group">
                       {{ Form::label('title','عنوان پیام : ') }}
                       {{ Form::text('title',null,['class'=>'form-control']) }}
                       @if($errors->has('title'))
                         <span class="has_error">{{ $errors->first('title') }}</span>
                       @endif
                     </div>

                    <div class="form-group center_item">
                       {{ Form::label('content','محتوای پیام : ') }}
                       {{ Form::textArea('content',null,['class'=>'form-control','style'=>'width:100%']) }}
                    </div>

                     <div class="form-group">
                       @if($errors->has('content'))
                       <p class="has_error" >{{ $errors->first('content') }}</p>
                      @endif
                    </div>
                    
                    <div class="form-group">
                      {{ Form::label('pic','انتخاب فایل : ') }}
                      <input type="file" name="pic" id="pic" style="direction:ltr">
                      @if($errors->has('pic'))
                        <span class="has_error">{{ $errors->first('pic') }}</span>
                      @endif
                    </div>

                    <button class="btn btn-success">ارسال پیام</button>
                     {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>

@endsection
