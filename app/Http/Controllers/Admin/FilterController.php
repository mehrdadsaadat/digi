<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Filter;
use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterController extends Controller
{
    public function filters($id)
    {
        $category=Category::findOrFail($id);
        $filters=Filter::with('getChild')->where(['category_id'=>$id,'parent_id'=>0])->orderBy('position','ASC')->get();
        $items=Item::getCategoryItem($id);
        return view('filter.index',['category'=>$category,'filters'=>$filters,'items'=>$items]);
    }
    public function add_filters($cat_id,Request $request)
    {
       $filter=$request->get('filter');
       $child_filter=$request->get('child_filter',array());
       $itemValue=$request->get('item_id',array());
       Filter::addFilter($filter,$child_filter,$cat_id,$itemValue);
       return redirect()->back()->with('message','ثبت فیلتر ها با موفقیت انجام شد');
    }
    public function destroy($id)
    {
        $filter=Filter::findOrFail($id);
        $filter->getChild()->delete();
        $filter->delete();
        return redirect()->back()->with('message','حذف فیلتر  با موفقیت انجام شد');

    }
}
