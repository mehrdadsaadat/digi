<?php
namespace App;
use Session;
use Auth;
use DB;
class Cart
{
    public static function add_cart($data)
    {
        Session::forget('cart_final_price');
        Session::forget('discount_value');
        $product_id=array_key_exists('product_id',$data) ? $data['product_id'] : 0;
        $color_id=array_key_exists('color_id',$data) ? $data['color_id'] : 0;
        $warranty_id=array_key_exists('warranty_id',$data) ? $data['warranty_id'] : 0;
        $s_c=$warranty_id.'_'.$color_id;
        if(!Auth::check()){
            $cart=Session::get('cart',array());
            if(array_key_exists($product_id,$cart))
            {
                $product_data=$cart[$product_id]['product_data'];
                if(array_key_exists($s_c,$product_data))
                {
                    $count=$cart[$product_id]['product_data'][$s_c]+1;
                    if(self::check($product_id,$color_id,$warranty_id,$count))
                    {
                        $cart[$product_id]['product_data'][$s_c]++;
                    }
                }
                else{
                    $cart[$product_id]['product_data'][$s_c]=1;
                }
            }
            else
            {
                $cart[$product_id]=[
                    'product_data'=>[$s_c=>1]
                ];
            }
            Session::put('cart',$cart);
        }
        else{
            self::add_product_to_cart_table($product_id,$color_id,$warranty_id);
        }
    }
    public static function check($product_id,$color_id,$warranty_id,$count)
    {
        $e=explode('_',$warranty_id);
        if(sizeof($e)==2){
            $ProductWarranty=ProductWarranty::where([
                'id'=>$e[0],
                'product_id'=>$product_id
            ])->first();
            if($ProductWarranty && $ProductWarranty->product_number>=$count && $ProductWarranty->product_number_cart>=$count)
            {
                 return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
    public static function getCartData($type='cart')
    {
        $user_id=Auth::check() ? Auth::user()->id : 0;
        $cart_table_data=DB::table('cart')->where('user_id',$user_id)->get();
        $cart=get_cart_data($cart_table_data);
        $product_id=array();
        $color_id=array();
        $warranty_id=array();
        $seller_id=array();
        $cart_product_number=array();
        $data=array();
        $i=0;
        foreach ($cart as $key=>$value)
        {
             foreach ($value['product_data'] as $key2=>$value2)
             {
                 $a=explode('_',$key2);
                 if(sizeof($a)==3)
                 {
                     $product_id[$key]=$key;
                     $color_id[$a[2]]=$a[2];
                     $warranty_id[$a[1]]=$a[1];
                     $row=ProductWarranty::where([
                             'product_id'=>$key,
                             'color_id'=>$a[2],
                             'warranty_id'=>$a[1],
                             'id'=>$a[0]
                         ])->first();
                     if($row){
                         $seller_id[$row->seller_id]=$row->seller_id;
                         $data[$i]=$row;
                         $i++;

                         $k=$key.'_'.$a[2].'_'.$a[1].'_'.$a[0];
                         $cart_product_number[$k]=$value2;
                     }
                 }
             }
        }

        $products=Product::whereIn('id',$product_id)->select(['id','title','image_url','cat_id','product_url'])->get();
        $colors=Color::whereIn('id',$color_id)->get();
        $warranties=Warranty::whereIn('id',$warranty_id)->get();
        $sellers=Seller::whereIn('id',$seller_id)->get();
        $total_price=0;
        $cart_price=0;
        $cart_data=array();
        $j=0;
        $r=0;
        foreach ($data as $k=>$v)
        {
            $product=getCartProductData($products,$v->product_id);
            $color=getCartColorData($colors,$v->color_id);
            $seller=getCartSellerData($sellers,$v->seller_id);
            $warranty=getCartWarrantyData($warranties,$v->warranty_id);
            $n=$v->product_id.'_'.$v->color_id.'_'.$v->warranty_id.'_'.$v->id;
            $product_number=array_key_exists($n,$cart_product_number) ? $cart_product_number[$n] : 0;
            if($product && $warranty && $product_number>0)
            {
                 $product_number=$v->product_number>$product_number ? $product_number : $v->product_number;
                 $cart_data['product'][$j]['product_id']=$product->id;
                 $cart_data['product'][$j]['product_url']=$product->product_url;
                 $cart_data['product'][$j]['product_title']=$product->title;
                 $cart_data['product'][$j]['product_image_url']=$product->image_url;
                 $cart_data['product'][$j]['cat_id']=$product->cat_id;
                 $cart_data['product'][$j]['warranty_name']=$warranty->name;
                 $cart_data['product'][$j]['warranty_id']=$warranty->id;
                 $cart_data['product'][$j]['product_warranty_id']=$v->id;
                 $cart_data['product'][$j]['send_day']=$v->send_time;
                 $cart_data['product'][$j]['seller_id']=$v->seller_id;
                 if($color){
                     $cart_data['product'][$j]['color_name']=$color->name;
                     $cart_data['product'][$j]['color_type']=$color->type;
                     $cart_data['product'][$j]['color_code']=$color->code;
                     $cart_data['product'][$j]['color_id']=$color->id;
                 }
                 else{
                     $cart_data['product'][$j]['color_id']=0;
                 }
                 $cart_data['product'][$j]['price1']=$product_number*$v->price1;
                 if($type=='cart')
                 {
                     $cart_data['product'][$j]['offers_last_time']=$v->offers_last_time;
                     $cart_data['product'][$j]['int_price']=$product_number*$v->price2;
                     $cart_data['product'][$j]['price2']=replace_number(number_format($product_number*$v->price2));
                 }
                 else
                 {
                     $cart_data['product'][$j]['price2']=$product_number*$v->price2;
                 }


                if($seller){
                    $cart_data['product'][$j]['seller_name']=$seller->brand_name;
                }
                else{
                    $cart_data['product'][$j]['seller_name']=config('shop-info.shop_name');
                }
                 $cart_data['product'][$j]['product_number']=$v->product_number;
                 $cart_data['product'][$j]['product_number_cart']=$v->product_number_cart;
                 $cart_data['product'][$j]['product_count']=$product_number;
                 $total_price+=$product_number*$v->price1;
                 $cart_price+=$product_number*$v->price2;
                 if($user_id>0){
                     $cart_table_result=check_save_product_to_cart_table($cart_data['product'][$j],$cart_table_data,$user_id);
                     $cart_data['product'][$j]['initial_amount']=$cart_table_result['initial_amount'];
                     $cart_data['product'][$j]['initial_product_count']=$cart_table_result['initial_product_count'];
                 }
                 if($product_number>0){
                     $r++;
                 }
                 $j++;
            }
        }
        $discount=$total_price-$cart_price;

        Session::put('total_product_price',$total_price);
        Session::put('final_price',$cart_price);
        $cart_data['total_price']=replace_number(number_format($total_price));
        $cart_data['cart_price']=replace_number(number_format($cart_price));
        $cart_data['discount']=$discount>0 ? replace_number(number_format($discount)) : 0;
        $cart_data['product_count']=$r;
        if($user_id>0){
            Session::forget('cart');
        }
        return $cart_data;
    }
    public static function removeProduct($request)
    {
        $product_id=$request->get('product_id',0);
        $warranty_id=$request->get('warranty_id',0);
        $color_id=$request->get('color_id',0);
        $cart=Session::get('cart',array());
        if(sizeof($cart)>0){
            if(array_key_exists($product_id,$cart))
            {
                $a=$cart[$product_id]['product_data'];
                $childKey=$warranty_id.'_'.$color_id;
                if(array_key_exists($childKey,$a))
                {
                    Session::forget('cart_final_price');
                    Session::forget('discount_value');
                    unset($cart[$product_id]['product_data'][$childKey]);
                    if(empty($cart[$product_id]['product_data']))
                    {
                        unset($cart[$product_id]);
                    }
                    if(empty($cart))
                    {
                        Session::forget('cart');
                    }
                    else{
                        Session::put('cart',$cart);
                    }

                    return self::getCartData();
                }
                else{
                    return 'error';
                }
            }
            else{
                return 'error';
            }
        }
        else{
           return remove_product_of_cart_table($product_id,$warranty_id,$color_id);
        }

    }
    public static function ChangeProductCount($request)
    {
        $product_id=$request->get('product_id',0);
        $warranty_id=$request->get('warranty_id',0);
        $color_id=$request->get('color_id',0);
        $product_count=$request->get('product_count',1);
        settype($product_count,'integer');
        $cart=Session::get('cart',array());
        if(sizeof($cart)>0){
            if(array_key_exists($product_id,$cart))
            {
                $a=$cart[$product_id]['product_data'];
                $childKey=$warranty_id.'_'.$color_id;
                if(array_key_exists($childKey,$a))
                {
                    if(self::check($product_id,$color_id,$warranty_id,$product_count) && $product_count>0)
                    {
                        $cart[$product_id]['product_data'][$childKey]=$product_count;
                        Session::put('cart',$cart);
                        Session::forget('cart_final_price');
                        Session::forget('discount_value');
                        return self::getCartData();
                    }
                    else{
                        return "error";
                    }
                }
                else{
                    return "error";
                }
            }
            else{
                return "error";
            }
        }
        else{
            return change_product_count($product_id,$warranty_id,$color_id,$product_count);
        }
    }
    public static function get_product_count()
    {
        $count=0;
        $cart=Session::get('cart',array());
        if(sizeof($cart)>0)
        {
            foreach ($cart as $key=>$value)
            {
                $count+=sizeof($value['product_data']);
            }

        }
        else if(Auth::check()){
            $user_id=Auth::user()->id;
            $count=DB::table('cart')->where([
                'user_id'=>$user_id,
                'product_status'=>'available'
            ])->count();
        }
        return $count;
    }
    public static function add_product_to_cart_table($product_id,$color_id,$warranty_id)
    {
        $user_id=Auth::user()->id;
        $e=explode('_',$warranty_id);
        if(sizeof($e)==2){
            $ProductWarranty=ProductWarranty::where([
                'id'=>$e[0],
                'product_id'=>$product_id
            ])->first();
            if($ProductWarranty){
                $row=DB::table('cart')->where([
                    'product_id'=>$product_id,
                    'product_warranty_id'=>$e[0],
                    'warranty_id'=>$e[1],
                    'color_id'=>$color_id,
                    'user_id'=>$user_id
                ])->first();
                if($row){
                    $new_count=$row->count+1;
                    if($ProductWarranty->product_number>=$new_count && $ProductWarranty->product_number_cart>=$new_count){
                        DB::table('cart')->where([
                            'product_id'=>$product_id,
                            'product_warranty_id'=>$e[0],
                            'warranty_id'=>$e[1],
                            'color_id'=>$color_id,
                            'user_id'=>$user_id
                        ])->update(['count'=>$new_count]);
                    }
                }
                else{
                    DB::table('cart')->insert([
                        'product_id'=>$product_id,
                        'product_warranty_id'=>$e[0],
                        'warranty_id'=>$e[1],
                        'color_id'=>$color_id,
                        'user_id'=>$user_id,
                        'count'=>1,
                        'initial_amount'=>$ProductWarranty->price2
                    ]);
                }
            }

        }
    }
    public static function refresh_cart_table(){
        $user_id=Auth::user()->id;
        DB::table('cart')->where(['user_id'=>$user_id,'product_status'=>'unavailable'])->delete();
        $cart_table_data=DB::table('cart')->where('user_id',$user_id)->get();
        foreach ($cart_table_data as $key=>$value)
        {
            $update_array=[];
            if(!empty($value->final_amount))
            {
                $update_array['initial_amount']=$value->final_amount;
                $update_array['final_amount']=null;
            }
            $product_status=$value->product_status;
            settype($product_status,'integer');
            if($product_status>0)
            {
                $update_array['count']=$product_status;
                $update_array['product_status']='available';
            }

            if(sizeof($update_array)){
                DB::table('cart')->where('id',$value->id)
                    ->update($update_array);
            }
        }
    }
    public static function empty_cart()
    {
        if(Auth::check())
        {
            $user_id=Auth::user()->id;
            DB::table('cart')->where(['user_id'=>$user_id])->delete();
        }
    }
}
