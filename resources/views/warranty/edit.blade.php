@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت گارانتی ها','url'=>url('admin/warranties')],
         ['title'=>'ویرایش گارانتی','url'=>url('admin/warranties/'.$color->id)]
    ]])

    <div class="panel">

        <div class="header">ویرایش گارانتی - {{ $color->name }}</div>

        <div class="panel_content">


            {!! Form::model($warranty,['url' => 'admin/warranties/'.$color->id]) !!}

            {{ method_field('PUt') }}
            <div class="form-group">

                {{ Form::label('name','نام گارانتی : ') }}
                {{ Form::text('name',null,['class'=>'form-control']) }}
                @if($errors->has('name'))
                    <span class="has_error">{{ $errors->first('name') }}</span>
                @endif
            </div>




            <button class="btn btn-primary">ویرایش گارانتی</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

