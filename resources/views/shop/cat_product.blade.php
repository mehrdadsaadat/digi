@extends('layouts.shop')

@section('content')

    <div class="row" id="product_box">

        <div class="col-3">

            <div class="item_box" id="filter_div" @if(sizeof($_GET)==0 || (sizeof($_GET)==1) && array_key_exists('page',$_GET)) style="display: none" @endif>
                <div class="title_box">
                    <label>فیلتر های اعمال شده</label>
                    <span id="remove_all_filter">حذف</span>
                </div>
                <div id="selected_filter_box">

                </div>
            </div>
            @if(isset($category) && sizeof($category->getChild)>0)
                <div class="item_box">
                    <div class="title_box">
                        <label>دسته بندی</label>
                    </div>
                    <ul class="search_category_ul">
                       <li class="parent">
                           <a href="{{ url('search/'.$category->url) }}">{{ $category->name }}</a>
                           <ul>
                               @foreach($category->getChild as $cat)
                                   <li>
                                       <a href="{{ url('search/'.$cat->url) }}">{{ $cat->name }}</a>
                                   </li>
                               @endforeach
                           </ul>
                       </li>
                    </ul>
                </div>
            @endif
            @if(isset($brands) && sizeof($brands)>0)
                <div class="item_box">
                    <div class="title_box">
                        <label>برند</label>
                        <span class="fa fa-angle-down"></span>
                    </div>
                    <div>
                        <div class="filter_box filter_brand_div" style="display: block">
                            <input type="text" id="brand_search" placeholder="جست و جوی نام برند ...">

                            <ul class="list-inline product_cat_ul">
                                @foreach($brands as $key=>$value)

                                    <li data="brand_param_{{ $value->brand_id }}">
                                        <span class="check_box"></span>
                                        <span class="title">{{ $value->getBrand->brand_name }}</span>
                                        <span class="ename">{{ $value->getBrand->brand_ename }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            <div class="item_box">
                <div class="title_box">
                    <label>جست و جو در نتایج:</label>
                </div>
                <div>
                    <input type="text" @if(array_key_exists('string',$_GET)) value="{{ $_GET['string'] }}" @endif id="search_input" placeholder="نام محصول با نام برند مورد نظر خود را بنوسید ...">
                </div>
            </div>

            <div class="item_box toggle_box">
                <div class="toggle-light" id="product_status"></div>
                <span>فقط کالاهای موجود</span>
            </div>

            <div class="item_box toggle_box">
                <div class="toggle-light" id="send_status"></div>
                <span>فقط کالاهای آماده ارسال</span>
            </div>

            @if(isset($filter))
                @foreach($filter as $key=>$value)
                    <div class="item_box">
                        <div class="title_box">
                            <label>{{ $value->title }}</label>
                            <span class="fa fa-angle-down"></span>
                        </div>
                        <div>
                            <div class="filter_box">
                                <ul class="list-inline product_cat_ul">
                                    @foreach($value->getChild as $key2=>$value2)
                                        <?php
                                        $filter_key='attribute['.$value->id.']';
                                        ?>
                                        <li data="{{ $filter_key }}_param_{{ $value2->id }}">
                                            <span class="check_box"></span>
                                            <span class="title">{{ $value2->title }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="item_box">
               <div class="title_box">
                   <label>محدوده قیمت مورد نظر</label>
                   <span class="fa fa-angle-down"></span>
               </div>
               <div>
                   <div class="filter_box" style="display:block">
                       <div class="range_slider_div">
                           <div id="slider" class="price_rang_slider"></div>
                       </div>
                       <ul class="filter_price_ul">
                           <li>
                               <div>از</div>
                               <div class="price" id="min_price"></div>
                               <div>تومان</div>
                           </li>
                           <li>
                               <div>تا</div>
                               <div class="price" id="max_price"></div>
                               <div>تومان</div>
                           </li>
                       </ul>
                       <button class="btn btn-primary" id="price_filter_btn">
                           <span class="fa fa-filter"></span>
                           <span>اعمال محدوده قیمت</span>
                       </button>
                   </div>
               </div>
            </div>
            @if(isset($colors) && sizeof($colors)>1)
               <div class="item_box">
                   <div class="title_box">
                       <label>رنگ ها</label>
                       <span class="fa fa-angle-down"></span>
                   </div>
                   <div>
                       <div class="filter_box" style="display: block">
                           <ul class="list-inline product_cat_ul color_filter_ul">
                             @foreach($colors as $key=>$value)
                                <li data="color_param_{{ $value->id }}">
                                    <div>
                                        <span class="check_box"></span>
                                        <span class="title">{{ $value->name }}</span>
                                    </div>
                                    <div style="background:#<?= $value->code ?>;@if($value->name=='سفید') border:1px solid black @endif" class="color_div"></div>
                                </li>
                             @endforeach
                           </ul>
                       </div>
                   </div>
              </div>
           @endif
        </div>

        <div class="col-9" style="position:relative">
            <div style="display: flex;justify-content: space-between;align-items: center">
                <ul class="list-inline map_ul">
                    <li>
                        <a href="{{ url('/') }}">فروشگاه</a>
                        @if(isset($category)) / @endif
                    </li>
                    @if(isset($category))
                        @if($category->getParent->getParent->name!="-")
                            <li><a href="{{ url('main/'.$category->getParent->getParent->url)  }}">{{ $category->getParent->getParent->name }}</a> </li>
                        @endif
                        @if($category->getParent->name!="-")
                            <li>  @if($category->getParent->getParent->name!="-")
                                      /
                                  @endif
                                <a href="{{ url('search/'.$category->getParent->url)  }}">{{ $category->getParent->name }}</a></li>
                        @endif
                        <li>
                            / <a href="{{ url()->current() }}">
                                {{ $category->name }}
                            </a>
                        </li>
                    @endif
                </ul>
                <div id="product_count">
                </div>
            </div>
            <product-box  :compare="'yes'"></product-box>
        </div>
    </div>

@endsection

@section('head')
    <link rel="stylesheet" href="{{ url('css/nouislider.min.css') }}"/>
    <link rel="stylesheet" href="{{ url('css/toggles-full.css') }}"/>
    <script type="text/javascript" src="{{ url('js/nouislider.min.js') }}"></script>
@endsection

@section('footer')
  <script type="text/javascript" src="{{ url('js/toggles.min.js') }}"></script>
  <script>
      $("#product_status").toggles({
          type:'Light',
          text:{'on':'','off':''},
          width:50,
          direction:'rtl',
          on:true
      });
      $("#send_status").toggles({
          type:'Light',
          text:{'on':'','off':''},
          width:50,
          direction:'rtl',
          on:true
      });
  </script>
@endsection
