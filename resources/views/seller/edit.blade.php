@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
           ['title'=>'مدیریت فروشگان','url'=>url('admin/sellers')],
           ['title'=>'ویرایش اطلاعات فروشنده','url'=>url('admin/sellers/'.$seller->id.'/edit')]
      ]])
    <div class="panel">

        <div class="header">
            ویرایش اطلاعات فروشنده - {{ $seller->brand_name }}
        </div>

        <div class="panel_content">


            {!! Form::model($seller,['url' => 'admin/sellers/'.$seller->id]) !!}

            {{ method_field('PUT') }}
            <div class="form-group">
                {{ Form::label('fname','نام : ') }}
                {{ Form::text('fname',null,['class'=>'form-control']) }}
                @if($errors->has('fname'))
                    <span class="has_error">{{ $errors->first('fname') }}</span>
                @endif
            </div>
            <div class="form-group">
                {{ Form::label('lname','نام خانوادگی : ') }}
                {{ Form::text('lname',null,['class'=>'form-control']) }}
                @if($errors->has('lname'))
                    <span class="has_error">{{ $errors->first('lname') }}</span>
                @endif
            </div>
            <div class="form-group">
                {{ Form::label('brand_name','نام فروشنده : ') }}
                {{ Form::text('brand_name',null,['class'=>'form-control']) }}
                @if($errors->has('brand_name'))
                    <span class="has_error">{{ $errors->first('brand_name') }}</span>
                @endif
            </div>
            <div class="form-group">
                {{ Form::label('email','ایمیل : ') }}
                {{ Form::text('email',null,['class'=>'form-control']) }}
                @if($errors->has('email'))
                    <span class="has_error">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <div class="form-group">
                {{ Form::label('mobile','شماره موبایل : ') }}
                {{ Form::text('mobile',null,['class'=>'form-control']) }}
                @if($errors->has('mobile'))
                    <span class="has_error">{{ $errors->first('mobile') }}</span>
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('password','کلمه عبور : ') }}
                {{ Form::password('password',['class'=>'form-control']) }}
                @if($errors->has('password'))
                    <span class="has_error">{{ $errors->first('password') }}</span>
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('account_status','وضعیت اکانت : ') }}
                {{ Form::select('account_status',[
                    'active'=>'فعال',
                    'Inactive'=>'غیر فعال',
                    'awaiting_approval'=>'در انتظار تایید',
                    'reject'=>'رد شده',
                ],null,['class'=>'selectpicker','data-live-search'=>'true']) }}
            </div>

            <div class="form-group">
                {{ Form::label('province_id','استان : ') }}
                {{ Form::select('province_id',$province,null,['class'=>'selectpicker','data-live-search'=>'true']) }}
                @if($errors->has('province_id'))
                    <span class="has_error">{{ $errors->first('province_id') }}</span>
                @endif
            </div>
             <div class="form-group">
                {{ Form::label('city_id','شهر : ') }}
                {{ Form::select('city_id',$city,null,['class'=>'selectpicker','data-live-search'=>'true']) }}
                @if($errors->has('city_id'))
                    <span class="has_error">{{ $errors->first('city_id') }}</span>
                @endif
            </div>

            <button class="btn btn-primary">ویرایش</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $("#province_id").on('change',function(){
            const value=$(this).val();
            const url="<?= url('api/get_city') ?>/"+value;
            $.ajaxSetup(
            {
              'headers':{
                  'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
               }
            });
            $.ajax({
               url: url,
               type:"get",
               success:function (response) {
                  let html='';
                  for(let i=0;i<response.length;i++){
                      html+='<option value='+response[i].id+'>'+response[i].name+'</option>';
                  }
                  if(html.trim()=="")
                  {
                      html='<option value="">انتخاب شهر</option>';
                  }
                  $("#city_id").html(html).selectpicker('refresh');
               }
           });
        })
    </script>
@endsection