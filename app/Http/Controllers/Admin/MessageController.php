<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends CustomController
{
    protected  $model='Message';
    protected  $title='پیام';
    protected $route_params='messages';
    public function index(Request $request)
    {
        $messages=Message::getMessage(0,null,$request->all());
        $trash_message_count=Message::onlyTrashed()->count();
        return view('messages.index', ['messages'=> $messages,'req'=>$request,'trash_message_count'=>$trash_message_count]);
    }
    public function show($id){
        $message = Message::with(['getAnswer'=>function($query){
            $query->with(['to','from']);
        }])->where(['id' => $id,'parent_id'=>0])->firstOrFail();
        if($message->status==1){
            $message->status=0;
            $message->update();
        }
        return view('messages.show', ['message'=> $message]);
    }
    public function addAnswer($id,MessageRequest $request)
    {
        $user_id = $request->user()->id;
        $message = Message::where(['id' => $id,'parent_id' => 0])->firstOrFail();
        $time = time();
        $answer = new Message($request->all());
        $answer->parent_id = $message->id;
        $answer->from_type = 'App\User';
        $answer->from_id = $user_id;
        $answer->user_type =$message->user_type;
        $answer->to_type =$message->user_type;
        $answer->to_id = $message->to_id==0 ? $message->from_id : $message->to_id;
        $answer->time = $time;
        $answer->user_id =$message->user_id;
        $img_url = upload_file($request, 'pic', 'upload');
        if ($img_url) {
            $answer->file = $img_url;
        }
        $message->status=-1;
        $message->save();
        $answer->save();
        send_notification($answer->parent_id,$answer->user_id, $answer->user_type,  $message);
        return redirect()->back()->with('message', 'ثبت با موفقیت انجام شد');
    }
}
