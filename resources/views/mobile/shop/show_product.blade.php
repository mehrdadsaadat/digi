@extends('layouts.mobile')

@section('head')
    <link href="{{ url('css/swiper.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div style="position: relative;padding-bottom: 50px">

        @if(Session::has('comment_status'))
            <div class="alert @if(Session::get('comment_status')=='ok') alert-success @else alert-danger @endif custom-alert">
                @if(Session::get('comment_status')=='ok')
                    نظر شما با موفقیت ثبت شد و بعد از تایید نمایش داده خواهد شد
                @else
                    خطا در ثبت اطلاعات،مجددا تلاش نمایید
                @endif
            </div>
        @endif
        <div class="product_item_box margin">
            <div class="product_headline">
                <offer-time></offer-time>
                <h6 class="product_title">
                    {{ $product->title }}
                    @if(!empty($product->ename) && $product->ename!='null') <span>{{ $product->ename  }}</span> @endif
                </h6>
            </div>
            <div class="product_options">
                <div>
                    <a class="favorite" product-id="{{ $product->id }}">
                        <span class="fa fa-heart-o  @if($favorite) chosen @endif" ></span>
                    </a>
                    <span class="fa fa-share-alt"></span>
                    <span class="fa fa-line-chart"></span>
                </div>
                <div style="display: flex;align-items: center">
                    <?php
                    $width=0;
                    if($product->score_count>0)
                    {
                        $width=$product->score/($product->score_count*6);
                    }
                    $width=$width*20;
                    ?>
                    <span>{{ replace_number($product->score_count) }} نفر</span>
                    <div class="score">
                        <div class="gray">
                            <div class="red" style="width: {{ $width }}%"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                @if(sizeof($product->Gallery)>0)

                    <div class="swiper-container" id="gallery" dir="rtl">
                        <div class="swiper-wrapper">
                            @foreach($product->Gallery as $key=>$value)
                                <div class="swiper-slide">
                                    <img src="{{ url('files/gallery/'.$value->image_url) }}">
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                @else
                    <img src="{{ url('files/products/'.$product->image_url) }}" class="product_image">
                @endif
            </div>

            <div class="row">
                <ul class="list-inline product_data_ul">
                    <li>
                        <span>برند : </span>
                        <a href="{{ url('brand/'.$product->getBrand->brand_ename) }}" class="data_link">
                            <span>{{ $product->getBrand->brand_name }}</span>
                        </a>
                    </li>
                    <li>
                        <span>دسته بندی : </span>
                        <a href="{{ url('search/'.$product->getCat->url) }}" class="data_link">
                            <span>{{ $product->getCat->name }}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="product_item_box">
            <div style="padding: 20px">
                @if($product->status==1)
                    <div id="warranty_box">
                        @include('include.warranty',['color_id'=>0])
                        @if($product->fake==1)
                        <p class="fake_tag">
                            <span class="fa fa-warning"></span>
                            <span>این محصول توسط تولید کننده اصلی (برند) تولید نشده است</span>
                        </p>
                        @endif
                    </div>
                @else
                    <div class="product_unavailable">
                       <span>
                           @if($product->status==-1)
                               توقف تولید
                           @else
                               ناموجود
                           @endif
                        </span>
                        <p>
                            @if($product->status==-1)
                                متاسفانه تولید و فروش این کالا متوقف شده است. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید.
                            @else
                                متاسفانه این کالا در حال حاضر موجود نیست. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید
                            @endif
                        </p>
                    </div>
                @endif
            </div>
        </div>

        @if($product->status==1)
            <mobile-other-price :product_id="{{ $product->id }}"></mobile-other-price>
        @endif

        @if($product->status==1)
            <div class="add_product_link">
                <span>افزودن محصول به سبد خرید</span>
            </div>
        @endif
        @if($product_item_count>0)
            <div class="product_item_box">
                <div style="padding: 15px">
                    <div class="item_box remove_item_box_shadow">
                        <span>مشخصات فنی</span>
                        <a id="show_more_item_product">
                            <span>بیشتر</span>
                            <span class="fa fa-angle-left"></span>
                        </a>
                    </div>
                    @include('include.show_important_item',['remove_title'=>true])
                </div>
            </div>
        @endif
        @include('mobile.show_product_comments')
        @include('mobile.show_product_questions')
    </div>
    @include('mobile.product_item_list')
     <div id="share_div">
         <div id="share_box">
             <p>
                 با استفاده از روش‌های زیر می‌توانید این صفحه را با دوستان خود به اشتراک بگذارید.
             </p>
             <ul>
                 <li>
                     <a href="https://telegram.me/share/url?url=<?= url('product/dkp-'.$product->id.'/'.$product->product_url) ?>&ref=telegram">
                         <span class="fa fa-telegram"></span>
                     </a>
                 </li>
                 <li>
                     <a href="https://twitter.com/intent/tweet/?url=<?= url('product/dkp-'.$product->id.'/'.$product->product_url) ?>">
                         <span class="fa fa-twitter"></span>
                     </a>
                 </li>
                 <li>
                     <a href="https://www.facebook.com/sharer/sharer.php?m2w&s=100&p[url]=<?= url('product/dkp-'.$product->id.'/'.$product->product_url) ?>">
                         <span class="fa fa-facebook"></span>
                     </a>
                 </li>
                 <li>
                     <a href="https://wa.me?text=<?= url('product/dkp-'.$product->id.'/'.$product->product_url) ?>&ref=telegram">
                         <span class="fa fa-whatsapp"></span>
                     </a>
                 </li>
             </ul>

         </div>
     </div>
    <mobile-vue-chart :product_id="{{ $product->id }}"></mobile-vue-chart>

@endsection

@section('footer')
    <script src="{{ url('js/swiper.min.js') }}" type="text/javascript"></script>
    <script>
        var sliders=new Swiper('#gallery',{
            pagination:{
                el:'.swiper-pagination',
                dynamicBullets:true
            }
        });
        var product_slider=new Swiper('.products',{
            slidesPerView:2,
            spaceBetween:10
        });
    </script>
@endsection
@section('seo')
<meta name="description" content="{{ $product->description }}"/>
    <meta name="keywords" content="{{ $product->keywords }}"/>
    <meta property="og:site_name" content="{{ config('shop-info.shop_name') }}"/>
    <meta property="og:description" content="{{ $product->description }}"/>
    <meta property="og:title" content="{{ $product->title }}"/>
    <meta property="og:locale" content="fa_IR"/>
    <meta property="og:image" content="{{  url('files/products/'.$product->image_url) }}"/>
    <meta name="twitter:description" content="{{ $product->description }}"/>
    <meta name="twitter:title" content="{{ $product->title }}"/>
    <meta name="twitter:image" content="{{  url('files/products/'.$product->image_url) }}"/>
@endsection
