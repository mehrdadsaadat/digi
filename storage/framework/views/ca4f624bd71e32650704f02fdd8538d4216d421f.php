<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'مدیریت اسلایدر ها','url'=>url('admin/sliders')]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت اسلایدر ها

            <?php echo $__env->make('include.item_table',['count'=>$trash_slider_count,'route'=>'admin/sliders','title'=>'اسلایدر'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>عنوان</th>
                        <th>تصویر</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr>
                            <td>
                                <input type="checkbox" name="sliders_id[]" class="check_box" value="<?php echo e($value->id); ?>"/>
                            </td>
                            <td><?php echo e(replace_number($i)); ?></td>

                            <td><?php echo e($value->title); ?></td>
                            <td>
                                <img src="<?php echo e(url('files/slider/'.$value->image_url)); ?>" class="slide_image">

                            </td>
                            <td>
                                <?php if(!$value->trashed()): ?>
                                <a href="<?php echo e(url('admin/sliders/'.$value->id.'/edit')); ?>"><span class="fa fa-edit"></span></a>
                                <?php endif; ?>

                                <?php if($value->trashed()): ?>
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی اسلایدر' onclick="restore_row('<?php echo e(url('admin/sliders/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از بازیابی این اسلایدر مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                <?php endif; ?>

                                <?php if(!$value->trashed()): ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف اسلایدر' onclick="del_row('<?php echo e(url('admin/sliders/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این اسلایدر مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                <?php else: ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف اسلایدر برای همیشه' onclick="del_row('<?php echo e(url('admin/sliders/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این اسلایدر مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if(sizeof($sliders)==0): ?>
                        <tr>
                            <td colspan="5">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </form>

            <?php echo e($sliders->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/slider/index.blade.php ENDPATH**/ ?>