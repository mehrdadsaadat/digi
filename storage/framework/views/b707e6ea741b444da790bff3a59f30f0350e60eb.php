<div class="profile_menu">

    <div class="profile_menu_header">حساب کاربری شما</div>

    <ul class="profile_menu_ul">
        <li <?php if($active=='profile'): ?> class="active_li" <?php endif; ?>>
            <a href="<?php echo e(url('user/profile')); ?>">
                <span class="fa fa-user"></span>
                پروفایل
            </a>
        </li>

        <li <?php if($active=='orders'): ?> class="active_li" <?php endif; ?>>
            <a href="<?php echo e(url('user/profile/orders')); ?>">
                <span class="fa fa-shopping-cart"></span>
                سفارشات
            </a>
        </li>

        <li <?php if($active=='favorite'): ?> class="active_li" <?php endif; ?>>
            <a href="<?php echo e(url('user/profile/favorite')); ?>">
                <span class="fa fa-star-o"></span>
                لیست علاقه مندی ها
            </a>
        </li>

        <li <?php if($active=='comments'): ?> class="active_li" <?php endif; ?>>
            <a href="<?php echo e(url('user/profile/comments')); ?>">
                <span class="fa fa-camera-retro"></span>
               نقد و نظرات
            </a>
        </li>

        <li <?php if($active=='gift-cart'): ?> class="active_li" <?php endif; ?>>
            <a href="<?php echo e(url('user/profile/gift-cart')); ?>">
                <span class="fa fa-gift"></span>
                کارت های هدیه
            </a>
        </li>

        <li <?php if($active=='address'): ?> class="active_li" <?php endif; ?>>
            <a href="<?php echo e(url('user/profile/address')); ?>">
                <span class="fa fa-address-card"></span>
               آدرس ها
            </a>
        </li>

        <li <?php if($active=='messages'): ?> class="active_li" <?php endif; ?>>
            <a href="<?php echo e(url('user/profile/messages')); ?>">
                <span class="fa fa-envelope"></span>
                پیغام های من
            </a>
            <?php if(isset($new_message_count) && $new_message_count>0): ?>
                <span class="count_span"><?php echo e(replace_number($new_message_count)); ?></span>
            <?php endif; ?>
        </li>
    </ul>

</div>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/include/user_panel_menu.blade.php ENDPATH**/ ?>