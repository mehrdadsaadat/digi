<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Exports\Pay;
use App\Http\Controllers\Controller;
use App\Http\Requests\SellerRequest;
use App\Imports\PaymentImport;
use App\ProductWarranty;
use App\Province;
use App\Seller;
use App\SellerProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;
use Excel;
class SellerController extends CustomController
{
    protected  $model = 'Seller';
    protected  $title = 'فروشنده';
    protected $route_params = 'sellers';
    public function index(Request $request)
    {
        $sellers = Seller::getData($request->all());
        $trash_seller_count = Seller::onlyTrashed()->count();
        return view('seller.index', ['sellers' => $sellers, 'trash_seller_count' => $trash_seller_count, 'req' => $request]);
    }
    public function edit($id)
    {
       $seller=Seller::findOrFail($id);
       $province=Province::pluck('name','id')->toArray();
       $city = City::where('province_id',$seller->province_id)->pluck('name', 'id')->toArray();
       return view('seller.edit',[
            'seller'=> $seller,
            'province'=> $province,
            'city'=> $city
       ]);
    }
    public function update($id,SellerRequest $request)
    {
        $seller = Seller::findOrFail($id);
        $account_status=$seller->account_status;
        $data=$request->all();
        if (!empty($request->get('password'))) {
            $data['password']= Hash::make($request->get('password'));
        } else {
            unset($data['password']);
        }
        $seller->update($data);

        if($seller->account_status=='Inactive' && $request->get('account_status')!=$account_status){
            ProductWarranty::where(['seller_id'=>$id,'status'=>1])->update(['status'=>2]);
            ProductWarranty::where(['seller_id'=>$id])->delete();
        }
        elseif ($seller->account_status=='active' && $request->get('account_status')!=$account_status){
            ProductWarranty::where(['seller_id'=>$id,'status'=>2])->withTrashed()
                ->update(['status'=>1,'deleted_at'=>null]);
        }
        return redirect('admin/sellers')->with('message', 'ویرایش اطلاعات فروشنده با موفقیت انجام شد');
    }
    public function show($id, Request $request)
    {
        $seller = Seller::with(['getProvince','getCity'])->withCount('product')->where('id',$id)->firstOrFail();
        $products=SellerProduct::with('getProduct')->whereHas('getProduct')->where('seller_id', $id)->paginate(10);
        $document=DB::table('seller_document')->where('seller_id',$id)->first();
        return view('seller.show',[
            'products'=>$products,
            'document'=>$document,
            'seller'=>$seller
        ]);
    }
    public function export()
    {
       $count=Seller::whereRaw('total_price>=total_commission+paid_commission+100000')
       ->whereNotNull('shaba')->count();
       if($count>0){
            $sum = Seller::whereRaw('total_price>=total_commission+paid_commission+100000')
            ->whereNotNull('shaba')->sum(DB::raw('total_price - total_commission - paid_commission'));

            return Excel::download(new Pay($count,$sum), 'pay.xlsx');
        }
        else{
            return view('seller.excel_export');
        }

    }
    public function import()
    {
        return view('seller.import');
    }
    public function add_payment(Request $request)
    {
        $this->validate($request,['payment_file'=>'required|file|mimes:xlsx'],[],['payment_file'=>'فایل اکسل']);
        $file_name=upload_file($request, 'payment_file', 'import');
        if($file_name){
            $file= 'files/import/'.$file_name;
            DB::beginTransaction();
            try {
                Excel::import(new PaymentImport(), $file);
                DB::commit();
                return redirect()->back()->with('message', 'ثبت اطلاعات با موفقیت انجام شد');
            } catch (\Exception $exception) {
                DB::rollBack();
                return redirect()->back()->with('warring', 'خطا در ثبت اطلاعات مجددا تلاش نمایید');
            }
        }
        else{
            return redirect()->back()->with('warring','خطا در ثبت اطلاعات مجددا تلاش نمایید');
        }
    }
}
