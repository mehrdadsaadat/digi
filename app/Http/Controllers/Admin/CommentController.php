<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class CommentController extends CustomController
{
    protected  $model='Comment';
    protected  $title='نظر';
    protected $route_params='comments';
    public function  index(Request $request)
    {
        $comments=Comment::getData($request->all());
        $trash_comment_count=Comment::onlyTrashed()->count();
        return view('comment.index',['comments'=>$comments,'trash_comment_count'=>$trash_comment_count,'req'=>$request]);
    }
    public function change_status(Request $request)
    {
       if($request->ajax())
       {
           $comment_id=$request->get('comment_id');
           $comment=Comment::find($comment_id);
           if($comment){
               $status=$comment->status==1 ? 0 : 1;
               $comment->status=$status;
               DB::table('comment_scores')->where('comment_id',$comment->id)->update(['status'=>$status]);
               if($comment->update())
               {
                   return 'ok';
               }
               else{
                   return 'error';
               }
           }
           else{
               return 'error';
           }
       }
    }

}
