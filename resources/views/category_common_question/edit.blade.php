@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت دسته ها','url'=>url('admin/category-common-question')],
         ['title'=>'ویرایش دسته','url'=>url('admin/category-common-question/'.$CategoryCommonQuestion->id.'/edit')]
    ]])

    <div class="panel">

        <div class="header">
            ویرایش دسته - {{ $CategoryCommonQuestion->title }}
        </div>

        <div class="panel_content">


            {!! Form::model($CategoryCommonQuestion,['url' => 'admin/category-common-question/'.$CategoryCommonQuestion->id,'files'=>true]) !!}
            {{ method_field('PUT') }}
            <div class="form-group">

                {{ Form::label('title','نام دسته : ') }}
                {{ Form::text('title',null,['class'=>'form-control']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>




            <div class="form-group">
                <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                {{ Form::label('pic','انتخاب ایکون  : ') }}
                <img src="{{ url('files/images/pic_1.jpg') }}" onclick="select_file()" width="100" id="output">
                @if($errors->has('pic'))
                    <span class="has_error">{{ $errors->first('pic') }}</span>
                @endif
            </div>



            <button class="btn btn-primary">ویرایش دسته</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
