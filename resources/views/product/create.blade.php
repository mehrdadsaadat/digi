@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت محصولات','url'=>url('admin/products')],
         ['title'=>'افزودن محصول جدید','url'=>url('admin/products/create')]
    ]])

    <?php
    use App\Product;$status=Product::ProductStatus();
    ?>
    <div class="panel">

        <div class="header">افزودن محصول جدید</div>

        <div class="panel_content">
            {!! Form::open(['url' => 'admin/products','files'=>true]) !!}

            <div class="form-group">

                {{ Form::label('title','عنوان محصول : ') }}
                {{ Form::text('title',null,['class'=>'form-control total_width_input']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::textarea('tozihat',null,['class'=>'form-control ckeditor']) }}
            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        {{ Form::label('ename','نام لاتین محصول : ') }}
                        {{ Form::text('ename',null,['class'=>'form-control left']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('cat_id','انتخاب دسته : ') }}
                        {{ Form::select('cat_id',$catList,null,['class'=>'selectpicker','data-live-search'=>'true']) }}
                        @if($errors->has('cat_id'))
                            <span class="has_error">{{ $errors->first('cat_id') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('brand_id','انتخاب برند : ') }}
                        {{ Form::select('brand_id',$brand,null,['class'=>'selectpicker','data-live-search'=>'true']) }}
                        @if($errors->has('brand_id'))
                            <span class="has_error">{{ $errors->first('brand_id') }}</span>
                        @endif
                    </div>



                    <div class="form-group">
                        {{ Form::label('status','وضعیت محصول : ') }}
                        {{ Form::select('status',$status,0,['class'=>'selectpicker','data-live-search'=>'true']) }}

                    </div>
                </div>
                <div class="col-md-6">

                    <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                    <div class="choice_pic_box" onclick="select_file()">

                        <span class="title">انتخاب تصویر محصول</span>
                        <img id="output" class="pic_tag">
                    </div>
                    @if($errors->has('pic'))
                        <span class="has_error">{{ $errors->first('pic') }}</span>
                    @endif
                </div>
            </div>

            <p class="message_text">برچسب ها با استفاده از (،) از هم جدا شود</p>
            <div class="form-group">
                <input type="text" name="tag_list" id="tag_list" class="form-control" placeholder="برجسب های محصول">
                <div class="btn btn-success" onclick="add_tag()">افزودن</div>
                <input type="hidden" name="keywords" id="keywords">
            </div>

            <div id="tag_box">

            </div>
            <div style="clear:both"></div>

            <div class="form-group">

                {{ Form::label('description','توضیحات مختصر در مورد محصول (حداکثر 150 کاراکتر) : ',['style'=>'color:red;width:100%']) }}
                {{ Form::textarea('description',null,['class'=>'form-control','id'=>'description']) }}
                @if($errors->has('description'))
                    <span class="has_error">{{ $errors->first('description') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label>
                    <input type="checkbox" name="use_for_gift_cart" id="use_for_gift_cart">
                    استفاده به عنوان کارت هدیه
                </label>
            </div>

            <div class="form-group">
                <label>
                    {{ Form::checkbox('fake') }}
                    کالای غیر اصل
                </label>
            </div>

            <button class="btn btn-success">ثبت محصول</button>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('footer')
<script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
@endsection
