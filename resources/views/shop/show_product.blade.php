@extends('layouts.shop')

@section('content')

    <ul class="list-inline map_ul" style="margin-bottom:5px !important">
        <li>
            <a href="{{ url('/') }}">فروشگاه</a>
            /
        </li>
        @if($category && $category->getParent->getParent->name!='-')
           <li>
               <a href="{{ url('main/'.$category->getParent->getParent->url) }}">
                   {{ $category->getParent->getParent->name }}
               </a>
               /
           </li>
        @endif
        @if($category &&  $category->getParent->name!='-')
           <li>
              <a href="{{ url('search/'.$category->getParent->url) }}">
                 {{ $category->getParent->name }}
              </a>
             /
           </li>
        @endif
        @if($category)
           <li>
              <a href="{{ url('search/'.$category->url) }}">
                 {{ $category->name }}
              </a>
             /
           </li>
        @endif
        <li>
            <a href="{{ url()->current() }}">
                {{ $product->title }}
            </a>
        </li>
    </ul>
    <div class="content">

        @if(Session::has('comment_status'))
          <div class="alert @if(Session::get('comment_status')=='ok') alert-success @else alert-danger @endif">
              @if(Session::get('comment_status')=='ok')
                  نظر شما با موفقیت ثبت شد و بعد از تایید نمایش داده خواهد شد
              @else
                  خطا در ثبت اطلاعات،مجددا تلاش نمایید
              @endif
          </div>
        @endif
        <div class="product_info">

            <div class="product_image_box">
               <offer-time></offer-time>
               <div>
                   <ul class="product_options">
                       <li data-toggle="tooltip" data-placement="left" title="افزودن به علاقه مندی ها">
                            <a class="favorite" product-id='{{ $product->id }}'>
                               <span class="fa fa-heart-o @if($favorite) chosen @endif"></span>
                           </a>
                       </li>

                       <li data-toggle="tooltip" data-placement="left" title="اشتراک گذاری">
                           <a>
                               <span data-toggle="modal" data-target="#share_box" class="fa fa-share-alt"></span>
                           </a>
                       </li>

                       <li data-toggle="tooltip" data-placement="left" title="مقایسه">
                           <a href="{{url('compare/dkp-'.$product->id)}}">
                               <span class="fa fa-compress"></span>
                           </a>
                       </li>

                       <li data-toggle="tooltip" data-placement="left" title="نمودار قیمت">
                           <a>
                               <span class="fa fa-line-chart"></span>
                           </a>
                       </li>
                   </ul>

                      <div class="default_product_pic">
                          @if(!empty($product->image_url))
                            <img class="default_pic" src="{{ url('files/products/'.$product->image_url) }}" data-zoom-img="{{ url('files/products/'.$product->image_url) }}">
                          @endif
                      </div>
                      <div class="product_gallery_box">
                          @include('include.Gallery')
                      </div>



               </div>
            </div>
            <div class="product_data">

                <div id="zoom_box"></div>
                <div class="product_headline">
                    <h6 class="product_title">
                        {{ $product->title }}
                        @if(!empty($product->ename) && $product->ename!='null') <span>{{ $product->ename  }}</span> @endif
                    </h6>
                </div>
                <div>
                    <ul class="list-inline product_data_ul">
                        <li>
                            <span>برند : </span>
                            <a href="{{ url('brand/'.$product->getBrand->brand_ename) }}" class="data_link">
                                <span>{{ $product->getBrand->brand_name }}</span>
                            </a>
                        </li>
                        <li>
                            <span>دسته بندی : </span>
                            <a href="{{ url('search/'.$product->getCat->url) }}" class="data_link">
                                <span>{{ $product->getCat->name }}</span>
                            </a>
                        </li>
                    </ul>
                    <div class="row">

                        <div class="col-8">

                            @if($product->status==1)
                                <div id="warranty_box">
                                    @include('include.warranty',['color_id'=>0])
                                </div>
                                <div class="send_btn" id="cart_btn">
                                    <span class="line"></span>
                                    <span class="title">افزودن به سبد خرید</span>
                                </div>

                                @if($product->fake==1)
                                    <p class="fake_tag">
                                        <span class="fa fa-warning"></span>
                                        <span>این محصول توسط تولید کننده اصلی (برند) تولید نشده است</span>
                                    </p>
                                @endif
                            @else
                                <div class="product-unavailable">
                                    <span>
                                        @if($product->status==-1)
                                            توقف تولید
                                        @else
                                            ناموجود
                                        @endif
                                    </span>
                                    <p>
                                        @if($product->status==-1)
                                            متاسفانه تولید و فروش این کالا متوقف شده است. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید.
                                        @else
                                            متاسفانه این کالا در حال حاضر موجود نیست. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید
                                        @endif
                                    </p>
                                </div>
                            @endif
                        </div>
                        <div class="col-4">
                            @include('include.show_important_item')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($product->status==1)
        <other-price :product_id="{{ $product->id }}"></other-price>
        @endif
        @include('include.horizontal_product_list',['title'=>'محصولات مرتبط','products'=>$relate_products])

        <div id="tab_div">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#review" role="tab" aria-selected="true">
                        <span class="fa fa-camera-retro"></span>
                        <span>نقد و بررسی</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#product_items" role="tab" aria-selected="false">
                        <span class="fa fa-list-ul"></span>
                        <span>مشخصات فنی</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" id="comments" href="#product_comments" role="tab" aria-selected="false">
                        <span class="fa fa-comment-o"></span>
                        <span>نظرات کاربران</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#question"  id="questions" role="tab" aria-selected="false">
                        <span class="fa fa-question-circle"></span>
                        <span>پرسش و پاسخ</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="review" role="tabpanel" aria-labelledby="home-tab">
                    @include('include.product_review')
                </div>
                <div class="tab-pane fade" id="product_items" role="tabpanel" aria-labelledby="profile-tab">
                    @include('include.product_items')
                </div>
                <div class="tab-pane fade" id="product_comments" role="tabpanel" aria-labelledby="contact-tab">

                    <comment-list auth="<?php echo Auth::check() ?  'ok' : 'no' ?>" product_id="<?= $product->id ?>" product_title="<?= $product->title ?>" ></comment-list>

                </div>
                <div class="tab-pane fade" id="question" role="tabpanel" aria-labelledby="contact-tab">
                    <question-list auth="<?php echo Auth::check() ?  'ok' : 'no' ?>" product_id="<?= $product->id ?>" product_title="<?= $product->title ?>" ></question-list>
                </div>
            </div>
        </div>

        <login-box></login-box>
        <vue-chart :product_id="{{ $product->id }}"></vue-chart>

        @include('include.share-link')
        @include('include.gallery-modal-box')
    </div>
@endsection



@section('head')
    <link rel="stylesheet" href="{{ asset('slick/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('slick/slick-theme.css') }}" />
@endsection

@section('seo')
<meta name="description" content="{{ $product->description }}"/>
    <meta name="keywords" content="{{ $product->keywords }}"/>
    <meta property="og:site_name" content="{{ config('shop-info.shop_name') }}"/>
    <meta property="og:description" content="{{ $product->description }}"/>
    <meta property="og:title" content="{{ $product->title }}"/>
    <meta property="og:locale" content="fa_IR"/>
    <meta property="og:image" content="{{  url('files/products/'.$product->image_url) }}"/>
    <meta name="twitter:description" content="{{ $product->description }}"/>
    <meta name="twitter:title" content="{{ $product->title }}"/>
    <meta name="twitter:image" content="{{  url('files/products/'.$product->image_url) }}"/>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('slick/slick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.elevateZoom-3.0.8.min.js') }}"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $('.product_list').slick({
            speed: 900,
            slidesToShow: 4,
            slidesToScroll: 3,
            rtl:true,
            infinite: false,
        });
        const product_tozihat=$("#product_tozihat")[0].scrollHeight;
        if(product_tozihat<250)
        {
            $('.more_content').hide();
        }
        $('.default_pic').elevateZoom({
            zoomWindowPosition:"zoom_box",
            borderSize:1,
            zoomWindowWidth:500,
            zoomWindowHeight:450,
            cursor:'zoom-in',
            scrollZoom:true
        });
    </script>
@endsection
