@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[

       ['title'=>'مدیریت دسته بندی ها','url'=>url('admin/category')],
       ['title'=>'مدیریت معیار تعیین قیمت','url'=>url('admin/category/'.$category->id.'/price_variation')]
      ]])
    <div class="panel">

        <div class="header">
            مدیریت معیار تعیین قیمت دسته {{ $category->name }}
        </div>

        <div class="panel_content">

            @include('include.Alert')
            @include('include.warring')

            <form method="post" action="{{ url('admin/category').'/'.$category->id.'/price_variation' }}">
                @csrf

                <div class="category_items">
                    @if(sizeof($priceVariation)>0)
                        @foreach($priceVariation as $key=>$value)
                            <div class="form-group item_groups" id="price_variation_{{ $value->id }}">
                                <input type="text" value="{{ $value->name }}" class="form-control item_input" name="price_variation[{{ $value->id }}]" placeholder="مقدار">

                                <span class="item_remove_message" onclick="del_row('{{ url('admin/category/price_variation/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این مقدار مطمئن هستین ؟ ')">حذف</span>
                            </div>
                        @endforeach
                    @else
                        <div class="form-group item_groups" id="price_variation_-1">
                            <input type="text"  class="form-control item_input" name="price_variation[-1]" placeholder="مقدار">
                            <div class="child_item_box">

                            </div>
                        </div>
                    @endif

                </div>

                <div id="price_variation_box"></div>
                <span class="fa fa-plus-square" onclick="add_price_variation_input()"></span>
                <div class="form-group">
                    <button class="btn btn-primary">ثبت اطلاعات</button>
                </div>
            </form>
        </div>

    </div>

@endsection


@section('footer')
    <script>
        $(document).ready(function () {
            $('.category_items').sortable();
            $('.child_item_box').sortable();
        });
    </script>
@endsection
