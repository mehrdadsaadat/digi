<?php $__env->startSection('content'); ?>

    <shopping-cart :cart_data="<?php echo e(json_encode($cart_data)); ?>"></shopping-cart>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
   <script>
       $('[data-toggle="tooltip"]').tooltip();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.shop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/shop/cart.blade.php ENDPATH**/ ?>