<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت اسلایدر ها','url'=>url('admin/sliders')],
         ['title'=>'افزودن اسلایدر جدید','url'=>url('admin/brands/sliders')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="panel">

        <div class="header">افزودن اسلایدر جدید</div>

        <div class="panel_content">


            <?php echo Form::open(['url' => 'admin/sliders','files'=>true]); ?>


            <div class="form-group">

                <?php echo e(Form::label('title','عنوان : ')); ?>

                <?php echo e(Form::text('title',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('title')): ?>
                    <span class="has_error"><?php echo e($errors->first('title')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('url','آدرس (url) : ')); ?>

                <?php echo e(Form::text('url',null,['class'=>'form-control left total_width_input'])); ?>

                <?php if($errors->has('url')): ?>
                    <span class="has_error"><?php echo e($errors->first('url')); ?></span>
                <?php endif; ?>
            </div>





            <div class="form-group">
                <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                <div onclick="select_file()" class="btn btn-primary">انتخاب تصویر اسلایدر</div>
                <?php if($errors->has('pic')): ?>
                    <span class="has_error"><?php echo e($errors->first('pic')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <img  onclick="select_file()" style="margin-top:0px" id="output" class="slider_img">
            </div>


            <div class="form-group">
                <input type="file" name="mobile_pic" id="mobile_pic" onchange="loadFile2(event)" style="display:none">
                <div onclick="select_file2()" class="btn btn-primary">انتخاب تصویر اسلایدر برای نمایش در گوشی موبایل</div>
                <?php if($errors->has('mobile_pic')): ?>
                    <span class="has_error"><?php echo e($errors->first('mobile_pic')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <img  onclick="select_file2()" style="margin-top:0px" id="output2" class="slider_img">
            </div>

            <button class="btn btn-success">ثبت اسلایدر</button>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/slider/create.blade.php ENDPATH**/ ?>