@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت فایل ها','url'=>url('admin/filemanage')]]])
    <div class="panel">
        <div id="elfinder"></div>
    </div>
@endsection

@section('head')
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('elFinder/css/elfinder.min.css') }}">
    <link rel="stylesheet" href="{{ asset('elFinder/css/theme.css') }}">
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('elFinder/js/elfinder.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('elFinder/js/i18n/elfinder.fa.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#elfinder").elfinder({
                height:'500px',
                cssAutoLoad:false,
                url:'{{ url('elFinder/php/connector.minimal.php') }}',
            });
        });
    </script>
@endsection
