<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Http\Requests\BrandRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandController extends CustomController
{
    protected  $model='Brand';
    protected  $title='برند';
    protected $route_params='brands';
    public function index(Request $request)
    {
        $brand=Brand::getData($request->all());
        $trash_brand_count=Brand::onlyTrashed()->count();
        return view('brand.index',['brand'=>$brand,'trash_brand_count'=>$trash_brand_count,'req'=>$request]);

    }
    public function create()
    {
        return view('brand.create');
    }
    public function store(BrandRequest $request)
    {
        $brand=new Brand($request->all());
        $img_url=upload_file($request,'pic','upload');
        $brand->brand_icon=$img_url;
        $brand->saveOrFail();
        return redirect('admin/brands')->with('message','ثبت برند با موفقیت انجام شد');
    }
    public function edit($id)
    {
        $brand=Brand::findOrFail($id);
        return view('brand.edit',['brand'=>$brand]);
    }
    public function update($id,BrandRequest $request)
    {
        $brand=Brand::findOrFail($id);
        $img_url=upload_file($request,'pic','upload');
        if($img_url){
            $brand->brand_icon=$img_url;
        }
        $brand->update($request->all());
        return redirect('admin/brands')->with('message','ویرایش برند با موفقیت انجام شد');
    }
}
