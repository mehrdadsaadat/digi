<?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>
<?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <?php if(!isset($remove_delete_link)): ?> <th>#</th> <?php endif; ?>
        <th>ردیف</th>
        <th>شماره سفارش</th>
        <th>زمان ثبت</th>
        <th>مبلغ سفارش</th>
        <th>وضعیت سفارش</th>
        <th>عمیات</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php $i++; ?>
        <tr>
            <?php if(!isset($remove_delete_link)): ?>
            <td>
                <input type="checkbox" name="orders_id[]" class="check_box" value="<?php echo e($value->id); ?>"/>
            </td>
            <?php endif; ?>
            <td><?php echo e(replace_number($i)); ?></td>
            <td>
                <span <?php if($value->order_read=='no'): ?> style="color: red" <?php endif; ?>>
                    <?php echo e(replace_number($value->order_id )); ?>

                </span>
            </td>

            <td><?php echo e($jdf->jdate('H:i:s',$value->created_at)); ?> / <?php echo e($jdf->jdate('Y-n-j',$value->created_at)); ?></td>
           <td>
               <span class="alert alert-primary" style="padding: 5px 10px;border-radius:0px;">
                   <?php echo e(replace_number(number_format($value['price']))); ?> تومان
               </span>
           </td>
            <td>
                <?php if($value['pay_status']=='awaiting_payemnt'): ?>
                    <span class="alert alert-warning" style="padding: 5px 10px;border-radius:0px;">در انتظار پرداخت</span>
                <?php elseif($value['pay_status']=='ok'): ?>
                    <span  class="alert alert-success"  style="padding: 5px 10px;border-radius:0px;">پرداخت شده</span>
                <?php elseif($value['pay_status']=='canceled'): ?>
                    <span  class="alert alert-warning"  style="padding: 5px 10px;border-radius:0px;">لغو شده</span>
                <?php else: ?>
                    <span  class="alert alert-danger"  style="padding: 5px 10px;border-radius:0px;">خطا در اتصال به درگاه</span>
                <?php endif; ?>
            </td>
           <td>
                <?php if(!$value->trashed()): ?>
                    <a href="<?php echo e(url('admin/orders/'.$value->id)); ?>"><span class="fa fa-eye"></span></a>
                <?php endif; ?>

                <?php if(!isset($remove_delete_link)): ?>
                   <?php if($value->trashed()): ?>
                     <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی سفارش' onclick="restore_row('<?php echo e(url('admin/orders/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از بازیابی این سفارش مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                   <?php endif; ?>

                  <?php if(!$value->trashed()): ?>
                     <span data-toggle="tooltip" data-placement="bottom"  title='حذف سفارش' onclick="del_row('<?php echo e(url('admin/orders/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این سفارش مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                  <?php else: ?>
                     <span data-toggle="tooltip" data-placement="bottom"  title='حذف سفارش برای همیشه' onclick="del_row('<?php echo e(url('admin/orders/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این سفارش مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                  <?php endif; ?>
                <?php endif; ?>

            </td>
        </tr>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php if(sizeof($orders)==0): ?>
        <tr>
            <td colspan="7">رکوردی برای نمایش وجود ندارد</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
<?php /**PATH C:\xampp\htdocs\digikala\resources\views/include/orderList.blade.php ENDPATH**/ ?>