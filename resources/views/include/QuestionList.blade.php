<form method="post" id="data_form">
    @csrf

    @php $jdf=new \App\Lib\Jdf(); @endphp

    @foreach($questions as $key=>$value)
        <div class="question_div">
            <div class="question_div_header @if($value->status==0) question-pending-approval @endif">
                <div>
                    @if(!isset($remove_delete_link))<input type="checkbox" name="questions_id[]" class="check_box" value="{{ $value->id }}"> @endif
                    <span class="questions_status" question-id="{{ $value->id }}" status="{{ $value->status }}">
                        @if($value->status==1)
                        تایید شده
                   @else
                       در انتظار تایید
                   @endif
                    </span>
                 </div>
     
                 <div>
                     <?php $string='پرسش'; ?>
                     @if($value->question_id==0)
                       <span class="fa fa-qestion-circle"></span>
                       پرسش
                     @else
                       <span class="fa fa-reply"></span>
                       پاسخ
                       <?php $string='پاسخ'; ?>
                     @endif
     
                     @if($value->getUserInfo)
                        {{ $value->getUserInfo->first_name.' '.$value->getUserInfo->last_name }}
                     @else
                        <span>ناشناس</span>
                     @endif
                       <span>در تاریخ</span>
                       <span>{{ $jdf->jdate('d F Y',$value->time) }}</span>
     
                 </div>
                 <div>
     
     
                    @if(!isset($remove_delete_link))
                        @if(!$value->trashed())
                          <span data-toggle="tooltip" data-placement="bottom"  title="حذف <?= $string ?>" onclick="del_row('{{ url('admin/questions/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این <?= $string ?> مطمئن هستین ؟ ')" class="fa fa-trash"></span>
                        @else
                          <span data-toggle="tooltip" data-placement="bottom"  title="حذف <?= $string ?> برای همیشه" onclick="del_row('{{ url('admin/questions/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این <?= $string ?> مطمئن هستین ؟ ')" class="fa fa-trash"></span>
                        @endif

                        @if($value->trashed())
                           <span  data-toggle="tooltip" data-placement="bottom"  title="بازیابی <?= $string ?>" onclick="restore_row('{{ url('admin/questions/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این <?= $string ?> مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                         @endif
                    @endif
                     
                  </div>
            </div>

            <div class="question_content">
                {!! $value->question!!}

                <div style="min-height:70px">
                    @if($value->question_id!=0)
                    <div class="main">
                        <p>
                            <span class="fa fa-question"></span>
                            <span>پرسش اصلی</span>
                        </p>
                        {!! $value->getParent->question!!}
                    </div>
                    @endif
                </div>

                @if($value->question_id==0)
                <div class="answer_div">
                  <textarea name="answer" id="answer_{{ $value->id }}"  placeholder="پاسخ شما"></textarea>
                  <a onclick="add_answer('<?= csrf_token() ?>','{{ $value->id }}')" class="btn btn-success answer_btn">ثبت پاسخ</a>
                </div>
                @endif

                <div class="question_footer">
                  <a target="_blank" href="{{ url('product/dkp-'.$value->getProduct->id.'/'.$value->getProduct->product_url) }}">
                       ثبت شده در محصول : {{ $value->getProduct->title }}
                  </a>
                  @if($value->question_id==0)
                      <span class="add_answer">ارسال پاسخ به این پرسش</span>
                  @endif
                </div>
            </div>
            


       </div>

    @endforeach
</form>