@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت شهر ها','url'=>url('admin/city')]]])
    <div class="panel">

        <div class="header">
            مدیریت شهر ها

            @include('include.item_table',['count'=>$trash_city_count,'route'=>'admin/city','title'=>'شهر'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="string" class="form-control" value="{{ $req->get('string','') }}" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام شهر</th>
                        <th>استان</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($city as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="city_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->getProvince->name }}</td>
                            <td>
                                @if(!$value->trashed())
                                    <a href="{{ url('admin/city/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                    <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی شهر' onclick="restore_row('{{ url('admin/city/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این شهر مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                    <span data-toggle="tooltip" data-placement="bottom"  title='حذف شهر' onclick="del_row('{{ url('admin/city/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این شهر مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                    <span data-toggle="tooltip" data-placement="bottom"  title='حذف  شهر همیشه' onclick="del_row('{{ url('admin/city/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این شهر مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($city)==0)
                        <tr>
                            <td colspan="5">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $city->links() }}
        </div>
    </div>

@endsection
