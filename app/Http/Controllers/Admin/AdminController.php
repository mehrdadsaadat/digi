<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Offers;
use App\Product;
use App\ProductWarranty;
use App\Question;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Order;
use App\Payment;
use App\Seller;
use DB;
class AdminController extends Controller
{
    public function index()
    {
        $indexChartData=Order::ChartData();
        $submissions=DB::table('order_infos')->count();
        $submissions_approved=DB::table('order_infos')->where('send_status',1)->count();
        $items_today=DB::table('order_infos')->where('send_status',2)->count();
        $submissions_ready=DB::table('order_infos')->where('send_status',3)->count();
        $posting_send=DB::table('order_infos')->where('send_status',4)->count();
        $posting_receive=DB::table('order_infos')->where('send_status',5)->count();
        $delivered=DB::table('order_infos')->where('send_status',6)->count();
        $user_count=User::count();
        $order_count=Order::count();
        $comment_count=Comment::count();
        $total_question_count=Question::where('question_id',0)->count();
        $product_count=Product::count();
        $seller_count=Seller::count();
        $last_orders=Order::orderBy('id','DESC')->limit(5)->get();
        return view('admin.index',[
            'indexChartData'=>$indexChartData,
            'submissions'=>$submissions,
            'submissions_approved'=>$submissions_approved,
            'items_today'=>$items_today,
            'submissions_ready'=>$submissions_ready,
            'posting_send'=>$posting_send,
            'posting_receive'=>$posting_receive,
            'delivered'=>$delivered,
            'user_count'=>$user_count,
            'order_count'=>$order_count,
            'comment_count'=>$comment_count,
            'total_question_count'=>$total_question_count,
            'product_count'=>$product_count,
            'seller_count'=>$seller_count,
            'last_orders'=>$last_orders
        ]);
    }
    public function incredible_offers()
    {
        return view('admin.incredible_offers');
    }
    public function getWarranty(Request $request)
    {
        $search_text=$request->get('search_text','');
        $ProductWarranty=ProductWarranty::with(['getColor','getProduct','getWarranty'])
            ->orderBy('offers','DESC');
        $ProductWarranty=$ProductWarranty->whereHas('getWarranty');
        if(empty(trim($search_text)))
        {
            $ProductWarranty=$ProductWarranty->whereHas('getProduct');
        }
        else{
            define('search_text',$search_text);
            $ProductWarranty=$ProductWarranty->whereHas('getProduct',function (Builder $query){
                $query->where('title','like','%'.search_text.'%');
            });
        }

        $ProductWarranty=$ProductWarranty->paginate(10);
        return $ProductWarranty;
    }
    public function add_incredible_offers($id,Request $request)
    {
         $ProductWarranty=ProductWarranty::find($id);
         if($ProductWarranty){

             $offers=new Offers();
             $res=$offers->add($request,$ProductWarranty);
             return $res;
         }
         else{
             return 'error';
         }
    }
    public function remove_incredible_offers($id,Request $request)
    {
        $ProductWarranty=ProductWarranty::find($id);
        if($ProductWarranty){
            $offers=new Offers();
            $res=$offers->remove($ProductWarranty);
            return $res;
        }
        else
        {
           return 'error';
        }
    }
    public function filemanager()
    {
        return view('admin.filemanager');
    }
    public function admin_login_form()
    {
        return view('admin.admin_login_form');
    }
    public function error403()
    {
        return view('admin.403');
    }
    public function author_panel()
    {
        return view('admin.author_panel');
    }
    public function sale_report()
    {
        $tatal_sale=DB::table('sale_statistics')->sum('price');
        $commission=DB::table('sale_statistics')->sum('commision');
        return view('admin.sale_report',['commission'=>$commission,'tatal_sale'=>$tatal_sale]);
    }
    public function get_sale_report(Request $request)
    {
        $jdf=new \App\Lib\Jdf();
        $y=$jdf->tr_num($jdf->jdate('Y'));
        $y=!empty($request->get('default_year')) ?  $request->get('default_year') : $y;
        $now=$jdf->tr_num($jdf->jdate('Y'));

        return get_sale_report($request,$y,'sale_statistics',['year'=>$y],'price',$now);
    }
    public function payments(Request $request)
    {
        $payments=Payment::getData($request->all());
        $sellers = ['' => 'انتخاب فروشنده'] + Seller::pluck('brand_name', 'id')->toArray();
        return view('admin.payments',['payments'=> $payments, 'sellers'=> $sellers,'req'=> $request]);
    }
}
