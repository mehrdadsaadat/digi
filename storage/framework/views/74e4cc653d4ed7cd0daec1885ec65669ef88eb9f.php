<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
     <div class="panel">
         <div>
             <p style="margin-top:40px;text-align:center;padding-bottom:20px;color:red">
                <span><?php echo e(Auth::user()->name); ?> عزیز</span>
                به پنل مدیریت فروشگاه اینترنتی <?php echo e(env('SHOP_NAME','')); ?> خوش آمدید
             </p>
         </div>
     </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/admin/author_panel.blade.php ENDPATH**/ ?>