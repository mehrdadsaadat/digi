<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Category;
use App\Color;
use App\Filter;
use App\Http\Requests\ProductRequest;
use App\Item;
use App\Product;
use App\ProductFilter;
use App\ProductGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\RejectMessage;
class ProductController extends CustomController
{
    protected  $model='Product';
    protected  $title='محصول';
    protected $route_params='products';
    public function index(Request $request)
    {
        $product=Product::getData($request->all());
        $trash_product_count=Product::onlyTrashed()->count();
        return view('product.index',['product'=>$product,'trash_product_count'=>$trash_product_count,'req'=>$request]);
    }
    public function create()
    {
        $brand['']='انتخاب برند';
        $brand=$brand+Brand::pluck('brand_name','id')->toArray();
        $catList=Category::get_parent2();
        return view('product.create',[
            'brand'=>$brand,
            'catList'=>$catList
        ]);
    }
    public function store(ProductRequest $request)
    {
        $use_for_gift_cart=$request->has('use_for_gift_cart') ? 'yes' : 'no';
        $fake=$request->has('fake') ? 1 : 0;
        $product=new Product($request->all());
        $product_url=get_url($request->get('title'));
        $product->product_url=$product_url;
        $image_url=upload_file($request,'pic','products');
        $product->image_url=$image_url;
        $product->use_for_gift_cart=$use_for_gift_cart;
        $product->fake=$fake;
        $product->view=0;
        create_fit_pic('files/products/'.$image_url,$image_url);
        $product->saveOrFail();

        set_cat_brand($product,null);
        return redirect('admin/products')->with('message','ثبت محصول با موفقیت انجام شد');
    }
    public function show($id)
    {
        $product=Product::findOrFail($id);
        $tatal_sale=DB::table('product_sale_statistics')->where('product_id',$id)->sum('price');
        $commission=DB::table('product_sale_statistics')->where('product_id',$id)->sum('commision');
        return view('product.show',[
            'tatal_sale'=>$tatal_sale,
            'commission'=>$commission,
            'product'=>$product
            ]);
    }
    public function edit($id)
    {
        $product=Product::findOrFail($id);
        $brand['']='انتخاب برند';
        $brand=$brand+Brand::pluck('brand_name','id')->toArray();
        $catList=Category::get_parent2();
        $reject_message=RejectMessage::with('getUser')->where('product_id',$id)->orderBy('id','DESC')->get();
        return view('product.edit',[
            'brand'=>$brand,
            'catList'=>$catList,
            'product'=>$product,
            'reject_message'=>$reject_message
        ]);
    }
    public function update(ProductRequest $request, $id)
    {
        $data=$request->all();
        $use_for_gift_cart=$request->has('use_for_gift_cart') ? 'yes' : 'no';
        $fake=$request->has('fake') ? 1 : 0;
        $product=Product::findOrFail($id);
        $oldData=[
            'cat_id'=>$product->cat_id,
            'brand_id'=>$product->brand_id
        ];

        $product_url=get_url($request->get('title'));
        $product->product_url=$product_url;
        $data['use_for_gift_cart']=$use_for_gift_cart;
        $data['fake']=$fake;
        $image_url=upload_file($request,'pic','products');
        if (!empty($image_url))
        {
            remove_file($product->image_url,'products');
            remove_file($product->image_url,'thumbnails');
            create_fit_pic('files/products/'.$image_url,$image_url);
            $product->image_url=$image_url;
        }
        $old_status=$product->status;

        $product->update($data);

        Product::setProductStatus($product,$request,$old_status);
        set_cat_brand($product,$oldData);

        return redirect('admin/products')->with('message','ویرایش محصول با موفقیت انجام شد');
    }
    public function gallery($id)
    {
        $product=Product::where('id',$id)->select(['id','title'])->firstorFail();
        $product_gallery=ProductGallery::where('product_id',$id)->orderBy('position','ASC')->get();
        return view('product.gallery',['product'=>$product,'product_gallery'=>$product_gallery]);
    }
    public function gallery_upload($id,Request $request)
    {
        $product=Product::where('id',$id)->select(['id'])->firstorFail();
        if($product)
        {
            $count=DB::table('product_gallery')->where('product_id',$id)->count();
            $image_url=upload_file($request,'file','gallery','image_'.$id.rand(1,100));
            if($image_url!=null){
                $count++;
                DB::table('product_gallery')->insert([
                    'product_id'=>$id,
                    'image_url'=>$image_url,
                    'position'=>$count
                ]);
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public function removeImageGallery($id)
    {
        $image=ProductGallery::findOrFail($id);
        $image_url=$image->image_url;
        $image->delete();

        if(file_exists('files/gallery/'.$image_url))
        {
            unlink('files/gallery/'.$image_url);
        }

        return redirect()->back()->with('message','حذف تصویر با موفقیت انجام شد');
    }
    public function change_images_status($id,Request $request)
    {
        $n=1;
       $parameters=$request->get('parameters');
       $parameters=explode(',',$parameters);
       foreach ($parameters as $key=>$value)
       {
           if(!empty($value)){
              DB::table('product_gallery')->where('id',$value)->update(['position'=>$n]);
               $n++;
           }
       }
       return 'ok';
    }
    public function items($id)
    {
        $product=Product::where('id',$id)->select(['id','title','cat_id'])->firstOrFail();
        $data=Item::getProductItemWithFilter($product);
        $product_items=$data['items'];
        $filters=$data['filters'];

        $product_filters=ProductFilter::where('product_id',$product->id)
            ->pluck('filter_id','filter_value')->toArray();
        return view('product.items',['product'=>$product,'product_items'=>$product_items,'filters'=>$filters,'product_filters'=>$product_filters]);
    }
    public function add_items($id,Request $request)
    {
        $product=Product::where('id',$id)->select(['id','title','cat_id'])->firstOrFail();
        $item_value=$request->get('item_value');
        $filter_value=$request->get('filter_value',array());
        DB::table('item_value')->where(['product_id'=>$id])->delete();
        foreach ($item_value as $key=>$value)
        {
            foreach ($value as $key2=>$value2)
            {
                if(!empty($value2)){
                    DB::table('item_value')->insert([
                       'product_id'=>$id,
                       'item_id'=>$key,
                       'item_value'=>$value2
                    ]);
                }
            }
            Item::addFilter($key,$filter_value,$id);
        }
        return redirect()->back()->with('message','ثبت مشخصات فنی برای محصول انجام شد');
    }
    public function filters($id)
    {
        $product=Product::where('id',$id)->select(['id','title','cat_id'])->firstOrFail();
        $product_filter=Filter::getProductFilter($product);
        return view('product.filter',['product'=>$product,'product_filters'=>$product_filter]);
    }
    public function add_filters($id,Request $request)
    {
        $product=Product::where('id',$id)->select(['id','title','cat_id'])->firstOrFail();
        $filters=$request->get('filter');
        DB::table('filter_product')->where(['product_id'=>$id])->delete();
        if(is_array($filters))
        {
            foreach ($filters as $key=>$value)
            {
                if(is_array($value))
                {
                    foreach ($value as $key2=>$value2)
                    {
                        DB::table('filter_product')->insert([
                           'product_id'=>$id,
                           'filter_id'=>$key,
                            'filter_value'=>$value2
                        ]);
                    }
                }
            }
        }
        return redirect()->back()->with('message','ثبت فیلتر برای محصول انجام شد');

    }
    public function get_sale_report(Request $request)
    {
        $product_id=$request->get('product_id');
        $jdf=new \App\Lib\Jdf();
        $y=$jdf->tr_num($jdf->jdate('Y'));
        $y=!empty($request->get('default_year')) ?  $request->get('default_year') : $y;
        $now=$jdf->tr_num($jdf->jdate('Y'));
        return get_sale_report($request,$y,'product_sale_statistics',
        ['year'=>$y,'product_id'=>$product_id],'price',$now);
    }
}
