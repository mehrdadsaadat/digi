@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
        ['title'=>'مدیریت فروشندگان','url'=>url('admin/sellers')],
        ['title'=>' دریافت لیست پرداخت','url'=>url('admin/seller/pay/export')]
    ]])
    <div class="panel">

        <div class="header">
            دریافت لیست پرداخت
        </div>

        <div class="panel_content">

            <p style="color:red;text-align:center;margin-top:10px">
                در حال حاضر هیچ لیستی برای پرداخت وجود ندارد
            </p>
            
        </div>
    </div>

@endsection
