<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'مدیریت برند ها','url'=>url('admin/brands')]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت برند ها

            <?php echo $__env->make('include.item_table',['count'=>$trash_brand_count,'route'=>'admin/brands','title'=>'برند'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                <?php if(isset($_GET['trashed']) && $_GET['trashed']==true): ?>
                    <input type="hidden" name="trashed" value="true">
                <?php endif; ?>
                <input type="text" name="string" class="form-control" value="<?php echo e($req->get('string','')); ?>" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>ایکون</th>
                        <th>نام برند</th>
                        <th>نام انگلیسی برند</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $brand; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr>
                            <td>
                                <input type="checkbox" name="brands_id[]" class="check_box" value="<?php echo e($value->id); ?>"/>
                            </td>
                            <td><?php echo e(replace_number($i)); ?></td>
                            <td>
                                <?php if(!empty($value->brand_icon)): ?>
                                    <img src="<?php echo e(url('files/upload/'.$value->brand_icon)); ?>">
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($value->brand_name); ?></td>
                            <td><?php echo e($value->brand_ename); ?></td>
                            <td>
                                <?php if(!$value->trashed()): ?>
                                <a href="<?php echo e(url('admin/brands/'.$value->id.'/edit')); ?>"><span class="fa fa-edit"></span></a>
                                <?php endif; ?>

                                <?php if($value->trashed()): ?>
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی برند' onclick="restore_row('<?php echo e(url('admin/brands/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از بازیابی این برند مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                <?php endif; ?>

                                <?php if(!$value->trashed()): ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف برند' onclick="del_row('<?php echo e(url('admin/brands/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این برند مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                <?php else: ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف برند برای همیشه' onclick="del_row('<?php echo e(url('admin/brands/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این برند مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if(sizeof($brand)==0): ?>
                        <tr>
                            <td colspan="6">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </form>

            <?php echo e($brand->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/brand/index.blade.php ENDPATH**/ ?>