<?php

namespace App\Imports;

use App\Payment;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DB;
class PaymentImport implements ToCollection,WithHeadingRow,WithChunkReading
{
    public function collection(Collection $rows)
    {
        if(sizeof($rows)>0){
            foreach($rows as $row){
                if ($row->has('instrid') && $row->has('amt')) {
                    $time = time();
                    if(!empty($row['instrid']))
                    {
                        $payment = new Payment([
                            'time' => $time,
                            'seller_id' => $row['instrid'],
                            'price' => $row['amt'],
                        ]);
                        if($row->has('shenase')){
                            $payment->shenase=$row['shenase']; 
                        }
                        $payment->save();
                        $price = $row['amt']/10;
                        DB::table('sellers')->where('id', $row['instrid'])->increment('paid_commission',$price);
                    }
                    
                }
            }
        }
    }
    public function chunkSize(): int
    {
        return 1;
    }
}
