window.Vue = require('vue');
Vue.component('pagination',require('laravel-vue-pagination'));
import Counter from './components/Counter';
import OfferTime from './components/OfferTime';
import ShoppingCart from './components/ShoppingCart';
import AddressList from './components/AddressList';
import GiftCart from './components/GiftCart';
import DiscountBox from './components/DiscountBox';
import ProductBox from './components/ProductBox';
import CompareProductList from './components/CompareProductList';
import CommentList from './components/CommentList';
import VueChart from './components/VueChart';
import HeaderCart from './components/HeaderCart';
import OtherPrice from './components/OtherPrice';
import MobileOtherPrice from './components/MobileOtherPrice';
import MobileShoppingCart from './components/MobileShoppingCart';
import MobileAddressList from './components/MobileAddressList';
import MobileProductBox from './components/MobileProductBox';
import MobileThemeCommentList from './components/MobileThemeCommentList';
import ProfileAddress from './components/ProfileAddress';
import QuestionList from './components/QuestionList';
import LoginBox from './components/LoginBox';
import FavoriteList from './components/FavoriteList';
import MobileFavoriteList from './components/MobileFavoriteList';
import MobileThemeQuestionList from './components/MobileThemeQuestionList';
import MobileVueChart from './components/MobileVueChart';
import HeaderSearch from './components/HeaderSearch';
import MobileHeaderSearch from './components/MobileHeaderSearch';

import axios from 'axios';
import VueAxsio from 'vue-axios';
Vue.use(VueAxsio,axios);
Vue.prototype.$siteUrl='https://tenix.ir';
if (process.env.NODE_ENV == 'development'){
    Vue.prototype.$siteUrl='http://localhost/digikala/public';
}

const app = new Vue({
    el: '#app',
    components:{
        Counter,
        OfferTime,
        ShoppingCart,
        AddressList,
        GiftCart,
        DiscountBox,
        ProductBox,
        CompareProductList,
        CommentList,
        VueChart,
        HeaderCart,
        OtherPrice,
        MobileOtherPrice,
        MobileShoppingCart,
        MobileAddressList,
        MobileProductBox,
        MobileThemeCommentList,
        ProfileAddress,
        QuestionList,
        LoginBox,
        FavoriteList,
        MobileFavoriteList,
        MobileThemeQuestionList,
        MobileVueChart,
        HeaderSearch,
        MobileHeaderSearch
    }
});

