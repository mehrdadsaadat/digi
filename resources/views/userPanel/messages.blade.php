@extends('layouts.shop')

@section('content')

    <div class="row">

        <div class="col-md-3">
            @include('include.user_panel_menu',['active'=>'messages'])
        </div>
        <div class="col-md-9" style="padding-right: 0px">

            <div class="profile_menu">

              <span class="profile_menu_title">پیام ها</span>
              <?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
             <div class="messageList">
                @include('include.Alert')
                <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>
                <form method="get" class="search_form">
                    <input type="text" name="title" class="form-control" value="{{ $req->get('title','') }}" placeholder="عنوان پیام"><button class="btn btn-primary">جست و جو</button>
                </form>
                <a href="{{ url('user/profile/messages/create') }}" class="btn btn-success" style="margin-bottom:10px;font-size:14px">
                    <span class="fa fa-envelope"></span>
                    <span>ارسال پیام جدید</span>
                </a>
                <table class="table table-bordered table-striped message_table">
                    <thead>
                        <tr>
                            <th>ردیف</th>
                            <th>عنوان</th>
                            <th>ارسال کننده</th>
                            <th>دریافت کننده</th>
                            <th>زمان ارسال</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $key=>$value)
                        @php $i++; @endphp
                        <tr>

                            <td>{{ replace_number($i) }}</td>
                            <td>
                                {{ $value->title }}
                            </td>
                            <td>
                                @if($value->from)
                                   <span class="form_link">
                                      @if($value->from_id==$value->user_id)
                                         {{ $value->from->name }}
                                      @else
                                         {{ config('shop-info.shop_name') }}
                                      @endif
                                   </span>
                                @endif
                            </td>
                            <td>
                                @if($value->to)
                                    <span class="to_link">
                                      {{ $value->to->name }}
                                    </span>
                                @else
                                    <span class="to_link">
                                         {{ config('shop-info.shop_name') }}
                                    </span>
                                @endif
                            </td>
                            <td>{{ $jdf->jdate('H:i:s',$value->time)  }} / {{  $jdf->jdate('Y-n-j',$value->time) }}</td>
                            <td>
                                <a href="{{ url('user/profile/messages/'.$value->id) }}">
                                    <span class="fa fa-eye" @if($value->status==-1) style="color:red" @endif>
                                    </span>
                                </a>
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($messages)==0)
                        <tr>
                            <td colspan="6">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                    </table>


                {{ $messages->links() }}
                </div>
            </div>
        </div>

    </div>

@endsection
