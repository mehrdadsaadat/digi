@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت اسلایدر ها','url'=>url('admin/sliders')],
         ['title'=>'ویرایش اسلایدر','url'=>url('admin/sliders/'.$slider->id.'/edit')]
    ]])

    <div class="panel">

        <div class="header">ویرایش اسلایدر - {{ $slider->title }}</div>

        <div class="panel_content">


            {!! Form::model($slider,['url' => 'admin/sliders/'.$slider->id,'files'=>true]) !!}

            {{ method_field('PUT') }}
            <div class="form-group">

                {{ Form::label('title','عنوان : ') }}
                {{ Form::text('title',null,['class'=>'form-control']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('url','آدرس (url) : ') }}
                {{ Form::text('url',null,['class'=>'form-control left total_width_input']) }}
                @if($errors->has('url'))
                    <span class="has_error">{{ $errors->first('url') }}</span>
                @endif
            </div>





            <div class="form-group">
                <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                <div onclick="select_file()" class="btn btn-primary">انتخاب تصویر اسلایدر</div>
                @if($errors->has('pic'))
                    <span class="has_error">{{ $errors->first('pic') }}</span>
                @endif
            </div>

            <div class="form-group">
                <img src="{{ url('files/slider/'.$slider->image_url) }}" onclick="select_file()" style="margin-top:0px" id="output" class="slider_img">
            </div>


            <div class="form-group">
                <input type="file" name="mobile_pic" id="mobile_pic" onchange="loadFile2(event)" style="display:none">
                <div onclick="select_file2()" class="btn btn-primary">انتخاب تصویر اسلایدر برای نمایش در گوشی موبایل</div>
                @if($errors->has('mobile_pic'))
                    <span class="has_error">{{ $errors->first('mobile_pic') }}</span>
                @endif
            </div>

            <div class="form-group">
                <img @if(!empty($slider->mobile_image_url)) src="{{ url('files/slider/'.$slider->mobile_image_url) }}" @endif onclick="select_file2()" style="margin-top:0px" id="output2" class="slider_img">
            </div>

            <button class="btn btn-primary">ویرایش اسلایدر</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
