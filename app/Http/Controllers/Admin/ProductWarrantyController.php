<?php

namespace App\Http\Controllers\Admin;

use App\Color;
use App\Http\Requests\WarrantyRequest;
use App\Product;
use App\ProductColor;
use App\ProductWarranty;
use App\Warranty;
use Illuminate\Http\Request;

class ProductWarrantyController extends CustomController
{
    protected  $model='ProductWarranty';
    protected  $title='تنوع قیمت';
    protected $route_params='product_warranties';
    protected $product;
    protected $query_string;
    public function __construct(Request $request)
    {
        $product_id=$request->get('product_id');
        $this->product=Product::findOrFail($product_id);
        $this->query_string='product_id='.$product_id;
    }
    public function index(Request $request)
    {
        $product_warranties=ProductWarranty::getData($request->all());
        $trash_product_warranties_count=ProductWarranty::where(['product_id'=>$this->product->id])->onlyTrashed()->count();
        return view('product_warranties.index',
            [
                'product_warranties'=>$product_warranties,
                'trash_product_warranties_count'=>$trash_product_warranties_count,
                'product'=>$this->product
            ]);
    }
    public function create()
    {
        $warranty=Warranty::orderBy('id','DESC')->pluck('name','id')->toArray();
        $colors=ProductWarranty::getPriceItem($this->product);
        return view('product_warranties.create',[
            'warranty'=>$warranty,
            'colors'=>$colors,
            'product'=>$this->product
        ]);
    }
    public function store(WarrantyRequest $request)
    {
        $color_id=$request->get('color_id');
        $e=explode('color_',$color_id);
        $color_id=sizeof($e)==2 ? $e[1] : $e[0];
        $check=ProductWarranty::where([
            'seller_id'=>0,
            'warranty_id'=>$request->get('warranty_id'),
            'product_id'=>$request->get('product_id'),
            'color_id'=>$color_id
        ])->first();
        if(!$check)
        {
            $warranty=new ProductWarranty($request->all());
            $warranty->color_id=$color_id;
            $warranty->saveOrFail();
            add_min_product_price($warranty);
            update_product_price($this->product);
            if(sizeof($e)==2){
                add_product_color($request->get('product_id'),$color_id);
            }
            return redirect('admin/product_warranties?product_id='.$this->product->id)->with('message','ثبت تنوع قیمت با موفقیت انجام شد');
        }
        else{
            return redirect()->back()->withInput()->with('warring','تنوع قیمت با مشخصات انتخابی قبلا ثبت شده');
        }
   }
    public function edit($id)
    {
        $product_warranties=ProductWarranty::findOrFail($id);
        $warranty=Warranty::orderBy('id','DESC')->pluck('name','id')->toArray();
        $colors=ProductWarranty::getPriceItem($this->product);
        return view('product_warranties.edit',[
            'warranty'=>$warranty,
            'colors'=>$colors,
            'product'=>$this->product,
            'product_warranties'=>$product_warranties
        ]);
    }
    public function update($id,WarrantyRequest $request)
    {
        $color_id=$request->get('color_id');
        $e=explode('color_',$color_id);
        $color_id=sizeof($e)==2 ? $e[1] : $e[0];
        $data=$request->all();
        $data['color_id']=$color_id;
        $product_warranties=ProductWarranty::findOrFail($id);
        $product_warranties->update($data);
        update_product_price($this->product);
        add_min_product_price($product_warranties);
        if(sizeof($e)==2){
            add_product_color($request->get('product_id'),$color_id);
        }
        return redirect('admin/product_warranties?product_id='.$this->product->id)->with('message','ویرایش تنوع قیمت با موفقیت انجام شد');
    }
}
