<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:seller_api')->group(function (){
    Route::get('/seller/profile', function (Request $request) {
        $prodinve=\App\Province::get();
        return [
            'seller'=>$request->user(),
            'province'=>$prodinve
        ];
    });
    Route::post('seller/profle/edit','api\SellerController@edit');
    Route::post('seller/logout','api\SellerController@logout');
    Route::post('seller/profile_check_active_code', 'api\SellerController@profile_check_active_code');
    Route::post('seller/profile_resend_active_code', 'api\SellerController@profile_resend_active_code');
    Route::get('seller/payemnt', 'api\SellerController@payemnt');
    Route::middleware('checkAccountSeller')->group(function (){
        Route::get('seller/category','api\SellerController@getCategory');
        Route::get('seller/brand','api\SellerController@getBrand');
        Route::get('seller/color','api\SellerController@getColor');
        Route::post('seller/product/create','api\ProductController@create_product');
        Route::get('seller/product/list','api\ProductController@product_list');
        Route::delete('seller/product/{id}','api\ProductController@remove_product');
        Route::get('seller/product/{id}/edit','api\ProductController@edit_product');
        Route::put('seller/product/{id}','api\ProductController@update_product');
        Route::get('seller/product/{id}/items','api\ProductController@items');
        Route::post('seller/product/{id}/items','api\ProductController@add_items');
        Route::get('seller/product/{id}/gallery','api\ProductController@gallery');
        Route::post('seller/product/{id}/gallery','api\ProductController@upload_image');
        Route::delete('seller/product/gallery/{id}','api\ProductController@remove_gallery_image');
        Route::post('seller/product/gallery/change_images_position/{id}','api\ProductController@change_images_position');

        Route::get('orders/get_sale_report','api\SellerController@get_sale_report');
        Route::get('orders/get_month_sales_statistics','api\SellerController@get_month_sales_statistics');
        Route::get('seller/orders','api\SellerController@orders');
        Route::get('seller/order/read/{id}','api\SellerController@read_order');

        Route::get('seller/ProductWarranty/{id}','api\ProductWarrantyController@index');
        Route::get('seller/ProductWarranty/{id}/create','api\ProductWarrantyController@create');
        Route::post('seller/ProductWarranty/{id}','api\ProductWarrantyController@store');
        Route::delete('seller/ProductWarranty/{id}','api\ProductWarrantyController@destroy');
        Route::put('seller/ProductWarranty/{id}','api\ProductWarrantyController@update');
        Route::get('seller/total_product','api\SellerController@total_product');

        Route::get('seller/stockroomList','api\PackageController@stockroomList');
        Route::get('seller/stockroom/getProductWarrnty','api\PackageController@getProductWarrnty');
        Route::post('stockroom/seller/add_product','api\PackageController@add_product');
        Route::get('seller/packages','api\PackageController@getPackages');
        Route::get('seller/getPackageContent/{id}','api\PackageController@getPackageContent');
         Route::get('seller/getPackageContent/factor/{id}','api\PackageController@factor');
        Route::get('seller/getStockroomProductList','api\PackageController@getStockroomProductList');
    });
    Route::get('seller/messages/list', 'api\SellerController@getMessageList');
    Route::post('seller/message/add', 'api\SellerController@addMessage');
    Route::get('seller/messages/content/{id}', 'api\SellerController@getMessageContent');
    Route::post('seller/message/{id}/answer', 'api\SellerController@addAnswer');
});

Route::post('seller/first_step_register','api\SellerController@first_step_register');
Route::get('seller/get_province','ApiController@get_province');
Route::get('seller/get_city/{province_id}','ApiController@get_city');
Route::post('seller/second_step_register','api\SellerController@second_step_register');
Route::post('seller/resend_active_code','api\SellerController@resend_active_code');
Route::post('seller/check_active_code','api\SellerController@check_active_code');
Route::post('seller/upload_file','api\SellerController@upload_file');

Route::get('get_province','ApiController@get_province');
Route::get('get_city/{province_id}','ApiController@get_city');
Route::get('getProductChartData/{product_id}','ApiController@getProductChartData');
Route::post('getWarranty/{product_id}','ApiController@getWarranty');




//application api

Route::get('getCategory','api\ApiController@getCategory');
Route::get('getChildCategory/{id}','api\ApiController@getChildCategory');
Route::get('category/new_product/{cat_id}','api\ApiController@category_new_product');
Route::get('category/best_selling_product/{cat_id}','api\ApiController@category_best_selling_product');
Route::post('product/getList','api\ApiController@product_getList');
