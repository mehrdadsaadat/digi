@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت نظرات','url'=>url('admin/comments')]]])
    <div class="panel">

        <div class="header">
           نظرات کاربران

            @include('include.item_table',['count'=>$trash_comment_count,'route'=>'admin/comments','title'=>'نظر','remove_new_record'=>true])
        </div>

        <div class="panel_content">
            @include('include.Alert')
            @include('include.CommentList')
            {{ $comments->links() }}
        </div>
    </div>
@endsection
