<?php if(Session::has('message')): ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">

        <?php echo e(Session::get('message')); ?>

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/include/Alert.blade.php ENDPATH**/ ?>