@extends('layouts.admin')




@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت فروشندگان','url'=>url('admin/sellers')]]])
    <div class="panel">

        <div class="header">
            مدیریت فروشندگان

            @include('include.item_table',['count'=>$trash_seller_count,'route'=>'admin/sellers',
            'title'=>'فروشنده','remove_new_record'=>false,'other'=>[
                ['label'=>'دریافت خروجی پرداخت','url'=>url('admin/seller/pay/export'),'icon'=>'fa-file-excel-o'],
                ['label'=>'ثبت پرداخت ها','url'=>url('admin/seller/pay/import'),'icon'=>'fa-paypal'],
            ]])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="brand_name" class="form-control" value="{{ $req->get('brand_name','') }}" placeholder="نام فروشگاه"><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام فروشنده</th>
                        <th>نام فروشگاه</th>
                        <th>شماره موبایل</th>
                        <th>تعداد محصول</th>
                        <th>وضعیت</th>
                        <th>کمیسون دریافت شده</th>
                        <th>مبلغ قابل واریز</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sellers as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="sellers_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->fname.' '.$value->lname }}</td>
                            <td>{{ $value->brand_name }}</td>
                            <td>{{ replace_number($value->mobile) }}</td>
                            <td>{{ replace_number($value->product_count) }}</td>
                            <td>
                                @if ($value->account_status=='active')
                                   <div class="alert alert-seccuss">فعال</div>
                                @elseif ($value->account_status=='Inactive') 
                                   <div class="alert alert-secondary">غیر فعال</div>
                                @elseif ($value->account_status=='reject')   
                                    <div class="alert alert-danger">رد شده</div>
                                @else
                                    <div class="alert alert-warning">در انتظار تایید</div>
                                @endif
                            </td>
                            <td>
                                {{ replace_number(number_format($value->total_commission)) }} تومان
                            </td>
                            <td>
                                 @php
                                     $price=$value->total_price - $value->total_commission - $value->paid_commission;
                                 @endphp
                                 {{ replace_number(number_format($price)) }} تومان
                            </td>
                            <td>
                                <a href="{{ url('admin/sellers/'.$value->id.'/messages') }}" style="color:black">
                                    <span data-toggle="tooltip" data-placement="bottom"  title='پیام ها' class="fa fa-comment-o"></span>
                                </a>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/sellers/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی فروشنده' onclick="restore_row('{{ url('admin/sellers/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این فروشنده مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                <a href="{{ url('admin/sellers/'.$value->id) }}">
                                    <span  data-toggle="tooltip" data-placement="bottom"  title='محصولات فروشنده'  class="fa fa-eye"></span>
                                </a>
                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف فروشنده' onclick="del_row('{{ url('admin/sellers/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این فروشنده مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف فروشنده برای همیشه' onclick="del_row('{{ url('admin/sellers/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این فروشنده مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($sellers)==0)
                        <tr>
                            <td colspan="10">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $sellers->links() }}
        </div>
    </div>

@endsection
@section('footer')
   <script>
        $("#sidebarToggle").click();
   </script>
@endsection