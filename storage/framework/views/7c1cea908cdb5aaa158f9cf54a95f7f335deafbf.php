<?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
<table class="table product_list_table">
    <thead>
    <tr>
        <th>#</th>
        <th>شماره سفارش</th>
        <th>تاریخ ثبت سفارش</th>
        <th>مبلغ قابل پرداخت</th>
        <th>مبلغ کل</th>
        <th>عملیات پرداخت</th>
        <th>جزییات</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <tr>
            <td><?php echo e(replace_number(++$key)); ?></td>
            <td><?php echo e(replace_number($value->order_id )); ?></td>
            <td><?php echo e($jdf->jdate('j F Y',$value->created_at)); ?></td>
            <td><?php echo e(replace_number(number_format($value->price))); ?> تومان</td>
            <td><?php echo e(replace_number(number_format($value->total_price))); ?> تومان </td>
            <td>
                <?php if($value['pay_status']=='awaiting_payemnt'): ?>
                    در انتظار پرداخت
                <?php elseif($value['pay_status']=='ok'): ?>
                    پرداخت شده
                <?php elseif($value['pay_status']=='canceled'): ?>
                    لغو شده
                <?php else: ?>
                    خطا در اتصال به درگاه
                <?php endif; ?>
            </td>
            <td>
                <a href="<?php echo e(url('user/profile/orders/'.$value->id)); ?>">
                    <span class="fa fa-angle-left"></span>
                </a>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/include/user_order_list.blade.php ENDPATH**/ ?>