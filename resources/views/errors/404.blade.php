@extends("layouts.".$errorLayout)

@section('content')

    <div class="error_content">

        @if($errorLayout=='shop')
            <h3>صفحه‌ای که دنبال آن بودید پیدا نشد!</h3>

        @else
            <h4>صفحه‌ای که دنبال آن بودید پیدا نشد!</h4>
        @endif
        <a href="{{ url('/') }}" class="btn btn-success">صفحه اصلی</a>

    </div>

@endsection
