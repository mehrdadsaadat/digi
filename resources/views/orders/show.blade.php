@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
           ['title'=>'مدیریت سفارشات','url'=>url('admin/orders')],
           ['title'=>'جزییات سفارش','url'=>url('admin/orders/'.$order->id)],
         ]])
    <div class="panel">

        <div class="header">
            <span>جزییات سفارش  - {{ replace_number($order->order_id) }}</span>
        </div>

        @php use App\Lib\Jdf;use App\Order;$Jdf=new Jdf(); $OrderStatus=Order::OrderStatus() @endphp
        <table class="table table-bordered order_table_info">

            <tr>
                <td>
                    تحویل گیرنده:
                    <span>{{ $order->getAddress->name }}</span>
                </td>
                <td>
                    شماره تماس تحویل گیرنده:
                    <span>{{ replace_number($order->getAddress->mobile ) }}</span>
                </td>
            </tr>

            <tr>
                <td>
                    آدرس تحویل گیرنده:
                    <span>{{ $order->getAddress->getProvince->name.' '. $order->getAddress->getCity->name.' '. $order->getAddress->address }}</span>
                </td>
                <td>
                    تعداد مرسوله:
                    <span>{{ replace_number(sizeof($order->getOrderInfo)) }}</span>
                </td>
            </tr>

            <tr>
                <td>
                    مبلغ قابل پرداخت:
                    <span>{{ replace_number(number_format($order->price)) }} تومان</span>
                </td>
                <td>
                    مبلغ کل :
                    <span>{{ replace_number(number_format($order->total_price)) }} تومان</span>
                </td>
            </tr>

            @if(!empty($order->gift_value) && $order->gift_value>0)
                <tr>
                    <td>
                       مبلغ کارت هدیه:
                        <span>{{ replace_number(number_format($order->gift_value)) }} تومان</span>
                    </td>
                    <td>
                       کد کارت هدیه:
                        <span>{{ $order->getGiftCart->code }} </span>
                    </td>
                </tr>
            @endif

            @if(!empty($order->discount_value) && $order->discount_value>0)
                <tr>
                    <td>
                        مبلغ کد تخفیف:
                        <span>{{ replace_number(number_format($order->discount_value)) }} تومان</span>
                    </td>
                    <td>
                        کد تخفیف:
                        <span>{{ $order->discount_code }} </span>
                    </td>
                </tr>
            @endif
        </table>
        @foreach($order->getOrderInfo as $key=>$value)
            <div class="order_info_div">

                <div class="header">
                    {{ \App\Order::getOrderStatus($value['send_status'],$OrderStatus) }}
                </div>


                <order-step :steps="{{ json_encode($OrderStatus) }}" :send_status="{{ $value['send_status'] }}" :order_id="{{ $value->id }}"></order-step>


                <a href="{{ url('admin/orders/submission/factor/'.$value['id']) }}" class="btn btn-primary" target="_blank"  style="margin-bottom:20px">نمایش فاکتور</a>

                <table class="table table-bordered order_table_info">

                    <tr>
                        <td>
                            کد مرسوله:
                            <span>{{ replace_number($value['id'])  }}</span>
                        </td>
                        <td>
                            زمان تحویل:
                            <span>{{ $value['delivery_order_interval'] }}</span>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            نحوه ارسال:
                            <span>پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                        </td>
                        <td>
                            هزینه ارسال:
                            <span>
                                    @if($value['send_order_amount']==0)
                                    رایگان
                                @else
                                    {{ replace_number(number_format($value['send_order_amount'])) }} تومان
                                @endif
                                </span>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="text-align: center">
                            مبلغ این مرسوله:
                            <span>
                                    {{ replace_number(number_format($order_data['order_row_amount'][$value->id])) }} تومان
                                </span>
                        </td>
                    </tr>

                </table>

                <table class="table product_list_data">
                    <tr>
                        <th>نام محصول</th>
                        <th>تعداد</th>
                        <th>قیمت واحد</th>
                        <th>قیمت کل</th>
                        <th>تخفیف</th>
                        <th>قیمت نهایی</th>
                    </tr>
                    @foreach($order_data['row_data'][$value->id] as $product)

                        <tr>
                            <td class="product__info">
                                <div>
                                    <img src="{{ url('files/thumbnails/'.$product['image_url']) }}" />
                                    <ul>
                                        <li class="title">
                                            {{ $product['title'] }}
                                        </li>
                                        <li>
                                            <span>فروشنده :‌ ‌</span>
                                            <span>{{ $product['seller'] }}</span>
                                        </li>
                                        @if($product['color_id']>0)
                                            <li>
                                                @if($product['color_type']==1)
                                                    <span> رنگ :‌</span>
                                                @else
                                                    <span> سایز :‌</span>
                                                @endif
                                                <span>{{ $product['color_name'] }}</span>
                                            </li>
                                        @endif
                                        <li>
                                            <span>گارانتی :‌ ‌</span>
                                            <span>{{ $product['warranty_name'] }}</span>
                                        </li>
                                        @if($product['send_status']==6)
                                        <li>
                                            <a href="{{ url('admin/orders/return-product/'.$product['row_id']) }}">
                                                ثبت به عنوان کالای مرجوعی
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                            <td>
                                {{ replace_number($product['product_count']) }}
                            </td>
                            <td>
                                {{ replace_number(number_format($product['product_price1'])) }} تومان
                            </td>
                            <td>
                                {{ replace_number(number_format($product['product_price1']*$product['product_count'])) }} تومان
                            </td>
                            <td>
                                <?php
                                $discount=(($product['product_price1']*$product['product_count'])-($product['product_price2']*$product['product_count']));
                                ?>
                                {{ replace_number(number_format($discount)) }} تومان

                            </td>
                            <td>
                                {{ replace_number(number_format($product['product_price2']*$product['product_count'])) }} تومان
                                @if ($product['commission']>0 && $product['send_status']>-1)
                                    <div class="alert alert-success" style="padding:8px 10px;border-radius:0px;margin-top:15px">
                                        <span> : کمسیون کسر شده</span>
                                        {{ replace_number(number_format($product['commission'])) }}  تومان
                                    </div>
                                @endif
                            </td>
                        </tr>
                        @if($product['send_status']==-1)
                           <tr>
                               <td colspan="3">
                                   <span>این کالا توسط مشتری برگشت داده شده</span>
                                   @if (!empty($product['tozihat']))
                                       <p style=" margin-top:10px;color:red">
                                           {{ $product['tozihat'] }}
                                       </p>
                                   @endif
                               </td>
                               <td colspan="3">
                                   @php
                                       $p=($product['product_price2']*$product['product_count']);
                                   @endphp
                                   <span>هزینه قابل پرداخت به کاربر :</span>
                                   {{ replace_number(number_format(get_return_product_price($product['cat_id'],$order_discount,$p))).' تومان' }}
                               </td>
                           </tr>
                        @endif
                    @endforeach

                </table>


                @if(sizeof($value->getEvent)>0)
                   <div style="width: 95%;margin:20px auto">
                       <p>رویداد های مرسوله</p>
                       <table class="table table-bordered table-striped" >
                           <thead>
                           <tr>
                               <th>ردیف</th>
                               <th>تغییر از وضعیت</th>
                               <th>به وضعیت</th>
                               <th>زمان تغییر وضعیت</th>
                               <th>توسط</th>
                           </tr>
                           </thead>

                           @foreach($value->getEvent as $k=>$event)
                               <tr>
                                   <td>{{ replace_number(++$k) }}</td>
                                   <td>
                                       @if(array_key_exists($event->from,$OrderStatus))
                                           {{  $OrderStatus[$event->from] }}
                                       @endif
                                   </td>
                                   <td>
                                       @if(array_key_exists($event->to,$OrderStatus))
                                           {{  $OrderStatus[$event->to] }}
                                       @endif
                                   </td>
                                   <td>
                                       @php
                                           $e=explode(' ',$event->created_at);
                                           $e2=explode('-',$e[0]);
                                       @endphp
                                       {{ replace_number($Jdf->gregorian_to_jalali($e2[0],$e2[1],$e2[2],'-')) }}
                                   </td>
                                   <td>
                                       @if($event->getUser)
                                           <a href="{{ url('admin/users/'.$event->getUser->id) }}" style="color: black">
                                               {{ $event->getUser->name }}
                                           </a>
                                       @endif
                                   </td>
                               </tr>

                               @if(!empty($event->tozihat))
                                   <tr>
                                       <td colspan="5" style="text-align: right">
                                           <span>توضیحات : </span>
                                           {{ $event->tozihat }}
                                       </td>
                                   </tr>
                               @endif
                           @endforeach
                       </table>
                   </div>
                @endif
            </div>
        @endforeach

    </div>
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}" />
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
    <script>
        $("#sidebarToggle").click();
        const swiper=new Swiper('.swiper-container',{
            slidesPerView:5,
            spaceBetween:0,
            navigation:{
                nextEl:'.swiper-button-next',
                prevEl:'.swiper-button-prev'
            }
        });
    </script>
@endsection
