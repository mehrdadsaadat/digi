<?php $__env->startSection('content'); ?>

    <div class="content">

        <p style="padding-top:10px">لیست مقایسه <?php echo e($category->name); ?></p>
        <div class="compare_item_list">

            <div class="content">
                <div class="compare_box">
                    <div class="compare_product_gallery">
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="gallery_box">
                                <div class="remove_product_of_compare_list" data-id="<?php echo e($value->id); ?>">
                                    <span class="fa fa-close"></span>
                                </div>
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php $__currentLoopData = $value->Gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="swiper-slide">
                                                <img src="<?php echo e(url('files/gallery/'.$value2->image_url)); ?>"  class="compare_gallery_pic">
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="title">
                                    <a href="<?php echo e(url('product/dkp-'.$value->id.'/'.$value->product_url)); ?>"><?php echo e($value->title); ?></a>
                                </div>
                                <p class="price"><?php echo e(replace_number(number_format($value->price))); ?> تومان</p>
                                <a class="btn btn-primary" href="<?php echo e(url('product/dkp-'.$value->id.'/'.$value->product_url)); ?>">
                                    مشاهده و خرید محصول
                                </a>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php for($i=sizeof($products);$i<4;$i++): ?>
                            <div class="compare_add" data-toggle="modal" data-target=".product_list">
                               <button class="add">
                                   <p class="fa fa-plus-circle"></p>
                                   <p>برای افزودن کالا به لیست مقایسه کلیک کنید</p>
                               </button>
                               <button class="btn btn-dark">افزودن کالا به لیست مقایسه</button>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <h5 class="compare_title"><?php echo e($value->title); ?></h5>
                        <ul class="compare_ul">
                            <?php $__currentLoopData = $value->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li class="title"><?php echo e($value2->title); ?></li>
                                <li class="value">
                                    <div <?php if(sizeof($products)>0): ?> class="left_border" <?php endif; ?>>
                                        <?php echo strip_tags( get_item_value(0,$products,$value2->id),'<br>'); ?>

                                    </div>
                                    <div <?php if(sizeof($products)>1): ?> class="left_border" <?php endif; ?>>
                                        <?php echo strip_tags( get_item_value(1,$products,$value2->id),'<br>'); ?>

                                    </div>
                                    <div <?php if(sizeof($products)>2): ?> class="left_border" <?php endif; ?>>
                                        <?php echo strip_tags( get_item_value(2,$products,$value2->id),'<br>'); ?>

                                    </div>
                                    <div <?php if(sizeof($products)>3): ?> class="left_border" <?php endif; ?>>
                                        <?php echo strip_tags( get_item_value(3,$products,$value2->id),'<br>'); ?>

                                    </div>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>


            <compare-product-list :cat_id="<?php echo e($category->id); ?>"></compare-product-list>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/swiper.min.css')); ?>" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/swiper.min.js')); ?>"></script>
<script>
const swiper=new Swiper('.swiper-container',{
     navigation:{
         nextEl:'.swiper-button-next',
         prevEl:'.swiper-button-prev',
     }
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.shop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/shop/compare.blade.php ENDPATH**/ ?>