<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
           ['title'=>'مدیریت سفارشات','url'=>url('admin/orders')],
           ['title'=>'جزییات سفارش','url'=>url('admin/orders/'.$order->id)],
         ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            <span>جزییات سفارش  - <?php echo e(replace_number($order->order_id)); ?></span>
        </div>

        <?php use App\Lib\Jdf;use App\Order;$Jdf=new Jdf(); $OrderStatus=Order::OrderStatus() ?>
        <table class="table table-bordered order_table_info">

            <tr>
                <td>
                    تحویل گیرنده:
                    <span><?php echo e($order->getAddress->name); ?></span>
                </td>
                <td>
                    شماره تماس تحویل گیرنده:
                    <span><?php echo e(replace_number($order->getAddress->mobile )); ?></span>
                </td>
            </tr>

            <tr>
                <td>
                    آدرس تحویل گیرنده:
                    <span><?php echo e($order->getAddress->getProvince->name.' '. $order->getAddress->getCity->name.' '. $order->getAddress->address); ?></span>
                </td>
                <td>
                    تعداد مرسوله:
                    <span><?php echo e(replace_number(sizeof($order->getOrderInfo))); ?></span>
                </td>
            </tr>

            <tr>
                <td>
                    مبلغ قابل پرداخت:
                    <span><?php echo e(replace_number(number_format($order->price))); ?> تومان</span>
                </td>
                <td>
                    مبلغ کل :
                    <span><?php echo e(replace_number(number_format($order->total_price))); ?> تومان</span>
                </td>
            </tr>

            <?php if(!empty($order->gift_value) && $order->gift_value>0): ?>
                <tr>
                    <td>
                       مبلغ کارت هدیه:
                        <span><?php echo e(replace_number(number_format($order->gift_value))); ?> تومان</span>
                    </td>
                    <td>
                       کد کارت هدیه:
                        <span><?php echo e($order->getGiftCart->code); ?> </span>
                    </td>
                </tr>
            <?php endif; ?>

            <?php if(!empty($order->discount_value) && $order->discount_value>0): ?>
                <tr>
                    <td>
                        مبلغ کد تخفیف:
                        <span><?php echo e(replace_number(number_format($order->discount_value))); ?> تومان</span>
                    </td>
                    <td>
                        کد تخفیف:
                        <span><?php echo e($order->discount_code); ?> </span>
                    </td>
                </tr>
            <?php endif; ?>
        </table>
        <?php $__currentLoopData = $order->getOrderInfo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="order_info_div">

                <div class="header">
                    <?php echo e(\App\Order::getOrderStatus($value['send_status'],$OrderStatus)); ?>

                </div>


                <order-step :steps="<?php echo e(json_encode($OrderStatus)); ?>" :send_status="<?php echo e($value['send_status']); ?>" :order_id="<?php echo e($value->id); ?>"></order-step>


                <a href="<?php echo e(url('admin/orders/submission/factor/'.$value['id'])); ?>" class="btn btn-primary" target="_blank"  style="margin-bottom:20px">نمایش فاکتور</a>

                <table class="table table-bordered order_table_info">

                    <tr>
                        <td>
                            کد مرسوله:
                            <span><?php echo e(replace_number($value['id'])); ?></span>
                        </td>
                        <td>
                            زمان تحویل:
                            <span><?php echo e($value['delivery_order_interval']); ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            نحوه ارسال:
                            <span>پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                        </td>
                        <td>
                            هزینه ارسال:
                            <span>
                                    <?php if($value['send_order_amount']==0): ?>
                                    رایگان
                                <?php else: ?>
                                    <?php echo e(replace_number(number_format($value['send_order_amount']))); ?> تومان
                                <?php endif; ?>
                                </span>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="text-align: center">
                            مبلغ این مرسوله:
                            <span>
                                    <?php echo e(replace_number(number_format($order_data['order_row_amount'][$value->id]))); ?> تومان
                                </span>
                        </td>
                    </tr>

                </table>

                <table class="table product_list_data">
                    <tr>
                        <th>نام محصول</th>
                        <th>تعداد</th>
                        <th>قیمت واحد</th>
                        <th>قیمت کل</th>
                        <th>تخفیف</th>
                        <th>قیمت نهایی</th>
                    </tr>
                    <?php $__currentLoopData = $order_data['row_data'][$value->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>
                            <td class="product__info">
                                <div>
                                    <img src="<?php echo e(url('files/thumbnails/'.$product['image_url'])); ?>" />
                                    <ul>
                                        <li class="title">
                                            <?php echo e($product['title']); ?>

                                        </li>
                                        <li>
                                            <span>فروشنده :‌ ‌</span>
                                            <span><?php echo e($product['seller']); ?></span>
                                        </li>
                                        <?php if($product['color_id']>0): ?>
                                            <li>
                                                <?php if($product['color_type']==1): ?>
                                                    <span> رنگ :‌</span>
                                                <?php else: ?>
                                                    <span> سایز :‌</span>
                                                <?php endif; ?>
                                                <span><?php echo e($product['color_name']); ?></span>
                                            </li>
                                        <?php endif; ?>
                                        <li>
                                            <span>گارانتی :‌ ‌</span>
                                            <span><?php echo e($product['warranty_name']); ?></span>
                                        </li>
                                        <?php if($product['send_status']==6): ?>
                                        <li>
                                            <a href="<?php echo e(url('admin/orders/return-product/'.$product['row_id'])); ?>">
                                                ثبت به عنوان کالای مرجوعی
                                            </a>
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <?php echo e(replace_number($product['product_count'])); ?>

                            </td>
                            <td>
                                <?php echo e(replace_number(number_format($product['product_price1']))); ?> تومان
                            </td>
                            <td>
                                <?php echo e(replace_number(number_format($product['product_price1']*$product['product_count']))); ?> تومان
                            </td>
                            <td>
                                <?php
                                $discount=(($product['product_price1']*$product['product_count'])-($product['product_price2']*$product['product_count']));
                                ?>
                                <?php echo e(replace_number(number_format($discount))); ?> تومان

                            </td>
                            <td>
                                <?php echo e(replace_number(number_format($product['product_price2']*$product['product_count']))); ?> تومان
                                <?php if($product['commission']>0 && $product['send_status']>-1): ?>
                                    <div class="alert alert-success" style="padding:8px 10px;border-radius:0px;margin-top:15px">
                                        <span> : کمسیون کسر شده</span>
                                        <?php echo e(replace_number(number_format($product['commission']))); ?>  تومان
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php if($product['send_status']==-1): ?>
                           <tr>
                               <td colspan="3">
                                   <span>این کالا توسط مشتری برگشت داده شده</span>
                                   <?php if(!empty($product['tozihat'])): ?>
                                       <p style=" margin-top:10px;color:red">
                                           <?php echo e($product['tozihat']); ?>

                                       </p>
                                   <?php endif; ?>
                               </td>
                               <td colspan="3">
                                   <?php
                                       $p=($product['product_price2']*$product['product_count']);
                                   ?>
                                   <span>هزینه قابل پرداخت به کاربر :</span>
                                   <?php echo e(replace_number(number_format(get_return_product_price($product['cat_id'],$order_discount,$p))).' تومان'); ?>

                               </td>
                           </tr>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </table>


                <?php if(sizeof($value->getEvent)>0): ?>
                   <div style="width: 95%;margin:20px auto">
                       <p>رویداد های مرسوله</p>
                       <table class="table table-bordered table-striped" >
                           <thead>
                           <tr>
                               <th>ردیف</th>
                               <th>تغییر از وضعیت</th>
                               <th>به وضعیت</th>
                               <th>زمان تغییر وضعیت</th>
                               <th>توسط</th>
                           </tr>
                           </thead>

                           <?php $__currentLoopData = $value->getEvent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               <tr>
                                   <td><?php echo e(replace_number(++$k)); ?></td>
                                   <td>
                                       <?php if(array_key_exists($event->from,$OrderStatus)): ?>
                                           <?php echo e($OrderStatus[$event->from]); ?>

                                       <?php endif; ?>
                                   </td>
                                   <td>
                                       <?php if(array_key_exists($event->to,$OrderStatus)): ?>
                                           <?php echo e($OrderStatus[$event->to]); ?>

                                       <?php endif; ?>
                                   </td>
                                   <td>
                                       <?php
                                           $e=explode(' ',$event->created_at);
                                           $e2=explode('-',$e[0]);
                                       ?>
                                       <?php echo e(replace_number($Jdf->gregorian_to_jalali($e2[0],$e2[1],$e2[2],'-'))); ?>

                                   </td>
                                   <td>
                                       <?php if($event->getUser): ?>
                                           <a href="<?php echo e(url('admin/users/'.$event->getUser->id)); ?>" style="color: black">
                                               <?php echo e($event->getUser->name); ?>

                                           </a>
                                       <?php endif; ?>
                                   </td>
                               </tr>

                               <?php if(!empty($event->tozihat)): ?>
                                   <tr>
                                       <td colspan="5" style="text-align: right">
                                           <span>توضیحات : </span>
                                           <?php echo e($event->tozihat); ?>

                                       </td>
                                   </tr>
                               <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       </table>
                   </div>
                <?php endif; ?>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/swiper.min.css')); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(asset('js/swiper.min.js')); ?>"></script>
    <script>
        $("#sidebarToggle").click();
        const swiper=new Swiper('.swiper-container',{
            slidesPerView:5,
            spaceBetween:0,
            navigation:{
                nextEl:'.swiper-button-next',
                prevEl:'.swiper-button-prev'
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/orders/show.blade.php ENDPATH**/ ?>