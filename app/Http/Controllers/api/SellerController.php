<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Seller;
use App\Category;
use App\Brand;
use App\Color;
use App\Message;
use App\Product;
use DB;
use Validator;
use App\Order;
use Illuminate\Contracts\Encryption\DecryptException;
use App\OrderProduct;
use App\Payment;
use Crypt;
class SellerController extends Controller
{
    public function first_step_register(Request $request)
    {
        return Seller::first_step_register($request);
    }
    public function second_step_register(Request $request)
    {
        return Seller::second_step_register($request);
    }
    public function resend_active_code(Request $request)
    {
        $mobile=$request->get('mobile');
        $seller=Seller::where(['mobile'=>$mobile,'step'=>2])->whereNotNull('active_code')->first();
        if($seller)
        {
            $active_code=rand(99999,1000000);
            $seller->active_code=$active_code;
            $seller->update($request->all());

            $message=config('shop-info.shop_name')."\n";
            $message.='کد تایید';
            $message.=" : ".$seller->active_code;
           
            $seller->notify(new \App\Notifications\SendSms($seller->mobile,$message));
            return ['status'=>'ok'];
        }
        else{
            return ['status'=>'error','message'=>'خطا در ثبت اطلاعات - مجددا تلاش نمایید'];
        }
    }
    public function check_active_code(Request $request)
    {
        return Seller::check_active_code($request);
    }
    public function upload_file(Request $request)
    {
        return Seller::upload_file($request);
    }
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return 'ok';
    }
    public function getCategory(Request $request)
    {
        return Category::get_parent2();
    }
    public function getBrand()
    {
        $brand['']='انتخاب برند';
        $brand=$brand+Brand::pluck('brand_name','id')->toArray();
        return $brand;
    }
    public function getColor()
    {
        $colors=Color::get();
        return $colors;
    }

    public function total_product(Request $request)
    {
        $search_text=$request->get('search_text');
        $seller_id=$request->user()->id;
        $Product=Product::orderBy('id','DESC');
        if(!empty($search_text))
        {
            $Product=$Product->where('title','like','%'.$search_text.'%');
        }
        $Product=$Product->select(['id','title','image_url','price','discount_price','status','cat_id'])
        ->where('seller_id','!=',$seller_id)
        ->whereIn('status',[0,1])
        ->withCount('sellerList')
        ->withCount('getWarranty')
        ->with('DigiWarranty')
        ->paginate(5);
        return   $Product;
    }
    public function get_sale_report(Request $request)
    {
        $seller_id=$request->user()->id;
        $jdf=new \App\Lib\Jdf();
        $y=$jdf->tr_num($jdf->jdate('Y'));
        $y=!empty($request->get('default_year')) ?  $request->get('default_year') : $y;
        $now=$jdf->tr_num($jdf->jdate('Y'));

        $tatal_sale=DB::table('seller_sale_statistics')->where('seller_id',$seller_id)->sum('price');
        $commission=DB::table('seller_sale_statistics')->where('seller_id',$seller_id)->sum('commision');
        $data=get_sale_report($request,$y,'seller_sale_statistics',['year'=>$y,'seller_id'=>$seller_id],'price',$now);
        $data['tatal_sale']=$tatal_sale;
        $data['commission']=$commission;
        return $data;
    }
    public function get_month_sales_statistics(Request $request)
    {
        $seller_id=$request->user()->id;
        return Order::SellerChartData($seller_id);
    }
    public function orders(Request $request)
    {
        return OrderProduct::getSellerOrders($request);
    }
    public function read_order(Request $request,$id)
    {
        $seller_id=$request->user()->id;
        $order=OrderProduct::where(['seller_id'=>$seller_id,'order_id'=>$id,'seller_read'=>'no'])->first();
        if($order)
        {
            $order->seller_read='ok';
            $order->update();
            return 'ok';
        }
        else{
            return 'error';
        }
    }
    public function edit(Request $request)
    {
        return Seller::editProfile($request);
    }
    public function profile_check_active_code(Request $request)
    {
        $seller_id = $request->user()->id;
        $encrypted=$request->get('encrypted');
        $code = $request->get('code');
        try {
            $mobile = Crypt::decryptString($encrypted);
            $seller=Seller::where(['id'=>$seller_id, 'active_code'=>$code])->first();
            if($seller){
                $seller->mobile=$mobile;
                $seller->active_code='';
                $seller->update();
                return 'ok';
            }
            else{
                return 'error';
            }
        } catch (DecryptException $e) {
           return 'error';
        }
    }
    public function profile_resend_active_code(Request $request)
    {
        $seller_id = $request->user()->id;
        $seller = Seller::where(['id' => $seller_id])->whereNotNull('active_code')->first();
        if ($seller) {
            $active_code = rand(99999, 1000000);
            $seller->active_code = $active_code;
            $seller->update($request->all());

            // $message = config('shop-info.shop_name') . "\n";
            // $message .= 'کد تایید';
            // $message .= " : " . $seller->active_code;

            // $seller->notify(new \App\Notifications\SendSms($seller->mobile, $message));
            return ['status' => 'ok'];
        } 
        else {
            return 'error';
        }
    }
    public function payemnt(Request $request)
    {
        $seller_id = $request->user()->id;
        $payment=Payment::where('seller_id', $seller_id)->orderBy('id','DESC');
        $date = $request->get('date');
        if (!empty($date)) {
            $first = getTimestamp($date, 'first');
            $last = getTimestamp($date, 'last');
            $payment = $payment->whereBetween('time', [$first, $last]);
        }
        return  $payment->paginate(10);
    }
    public function getMessageList(Request $request)
    {
        $seller_id = $request->user()->id;
        $title=$request->get('title');
        $message =Message::orderBy('id', 'DESC')->with(['from', 'to']);
        $message = $message->where(['user_id' => $seller_id, 'user_type' => 'App\Seller', 'parent_id' => 0]);
        if (!empty($title)) {
            $message = $message->where('title', 'like', '%' .  $title . '%');
        }
        $message = $message->paginate(10);
        return $message;
    }
    public function addMessage(Request $request)
    {
        return Seller::addMessage($request);
    }
    public function getMessageContent($id, Request $request)
    {
        $seller_id = $request->user()->id;
        $message=Message::with('getAnswer')->where(['id'=>$id,'user_id'=>$seller_id,'user_type'=>'App\Seller'])->first();
        if($message){
            $message->status=0;
            $message->update();
            return $message;
        }
        else{
            return 'redirect';
        }
    }
    public function addAnswer($id,Request $request)
    {
        return Seller::addAnswer($id,$request);
    }
}
