<?php if(sizeof($incredible_offers)>0): ?>
    <div class="row incredible-offers">

        <div class="col-3">

            <div style="margin-top:20px">
                <a href="<?php echo e(url('')); ?>">
                    <img src="<?php echo e(url('files/images/1000007524.jpg')); ?>" class="index-pic">
                </a>
                <a href="<?php echo e(url('')); ?>">
                    <img src="<?php echo e(url('files/images/1000007568.jpg')); ?>" class="index-pic">
                </a>
            </div>
        </div>
        <div class="col-9">
            <div class="discount-box">
                <div class="row">
                    <div class="discount-box-content">
                        <?php $__currentLoopData = $incredible_offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div <?php if($key==0): ?> style="display:block"  <?php endif; ?> id="discount_box_link_<?php echo e($value->id); ?>" class="item an">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="discount_bottom_bar"></div>
                                        <a href="<?php echo e(url('product/dkp-'.$value->getProduct->id.'/'.$value->getProduct->product_url)); ?>">
                                            <img src="<?php echo e(url('files/thumbnails/'.$value->getProduct->image_url)); ?>">
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a href="<?php echo e(url('product/dkp-'.$value->getProduct->id.'/'.$value->getProduct->product_url)); ?>">
                                            <div class="price_box">
                                                <del><?php echo e(replace_number(number_format($value->price1))); ?> تومان</del>
                                                <div class="incredible-offers-price">
                                                    <label><?php echo e(replace_number(number_format($value->price2))); ?> تومان </label>
                                                    <span class="discount-badge">
                                                    <?php
                                                        $a=($value->price2/$value->price1)*100;
                                                        $a=100-$a;
                                                        $a=round($a);
                                                        ?>
                                                        ٪<?php echo e(replace_number($a).' تخفیف'); ?>

                                                   </span>
                                                </div>
                                                <div class="discount_title"><?php echo e($value->getProduct->title); ?></div>
                                                <ul class="important_item_ul">
                                                    <?php $__currentLoopData = $value->itemValue; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($key<2): ?>
                                                            <li>
                                                                <?php echo e($item->important_item->title); ?> :
                                                                <?php echo e($item->item_value); ?>

                                                            </li>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>

                                                <?php if($value->product_number>0): ?>
                                                    <counter second="<?= $value->offers_last_time-time() ?>"></counter>
                                                <?php else: ?>

                                                <?php endif; ?>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    </div>

                    <div class="discount_left_item">
                        <?php $i=0; ?>
                        <?php $__currentLoopData = $incredible_offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div id="item_number_<?php echo e($i); ?>" <?php if($i==0): ?> class="active" <?php endif; ?> data-id="<?= $value->id ?>">
                                <?php echo e($value->getProduct->getCat->name); ?>

                                <?php $i++; ?>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                        <?php if(sizeof($incredible_offers)>=9): ?>
                            <a href="<?php echo e(url('incredible-offers')); ?>">
                                مشاهده همه شگفت انگیز ها
                            </a>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="discount_box_footer">

                    <div class="swiper-container" dir="rtl">
                        <div class="swiper-wrapper">
                            <?php $__currentLoopData = $incredible_offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="swiper-slide <?php if($key==0): ?> active <?php endif; ?> slide-amazing" data-id="<?php echo e($value->id); ?>" >
                                    <?php echo e($value->getProduct->getCat->name); ?>

                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(sizeof($incredible_offers)>4): ?>
                                <div class="swiper-slide" ></div>
                                <div class="swiper-slide" ></div>
                            <?php endif; ?>
                        </div>
                        <div class="slick-next"></div>
                        <div class="slick-prev"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/include/incredible-offers.blade.php ENDPATH**/ ?>