@extends('layouts.mobile')

@section('content')

    <?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
    <div class="message_list">
        <span class="profile_menu_title">پیام های من</span>
        <div class="search_box_div">
            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>
            <form method="get" class="search_form">
                <input type="text" name="title" class="form-control" value="{{ $req->get('title','') }}" placeholder="عنوان پیام"><button class="btn btn-primary">جست و جو</button>
            </form>
            <a href="{{ url('user/profile/messages/create') }}" class="btn btn-success" style="margin-bottom:10px;font-size:14px">
                <span class="fa fa-envelope"></span>
                <span>ارسال پیام جدید</span>
            </a>
        </div>
        @foreach($messages as $key=>$value)
            <div class="profile_item">
                <div class="profile_info_row" style="border-top:0px">
                    <div>
                        عنوان
                    </div>
                    <a href="{{ url('user/profile/messages/'.$value->id) }}">
                        <span class="fa fa-angle-left"></span>
                    </a>
                </div>
                <div class="profile_info_row">
                    <span>ارسال کننده</span>
                    <span>
                         @if($value->from)
                            <span class="form_link">
                                @if($value->from_id==$value->user_id)
                                    {{ $value->from->name }}
                                @else
                                    {{ config('shop-info.shop_name') }}
                                @endif
                            </span>
                        @endif
                    </span>
                </div>
                <div class="profile_info_row">
                    <span>دریافت کننده</span>
                    <span>
                        @if($value->to)
                            <span class="to_link">
                                 {{ $value->to->name }}
                            </span>
                        @else
                            <span class="to_link">
                                  {{ config('shop-info.shop_name') }}
                            </span>
                        @endif
                    </span>
                </div>
                <div class="profile_info_row">
                    <span>زمان ارسال</span>
                    <span>
                        {{ $jdf->jdate('H:i:s',$value->time)  }} / {{  $jdf->jdate('Y-n-j',$value->time) }}
                    </span>
                </div>
            </div>
        @endforeach
        {{ $messages->links() }}
    </div>
@endsection
