<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail;
use DB;
class Question extends Model
{
    use SoftDeletes;
    protected $fillable=['time','product_id','user_id','question_id','status','answer_count','send_email',
    'question','like','dislike'];
    public $timestamps=false;
    public function getUser()
    {
        return $this->hasone(User::class,'id','user_id')->select(['id','name'])
        ->withDefault(['name'=>'']);
    }
    public function getAnswer(){
        return $this->hasMany(Question::class,'question_id','id')->where('status',1);
    }
    public function getUserInfo()
    {
        return $this->hasone(AdditionalInfo::class,'user_id','user_id')->select(['id','user_id','email','first_name','last_name']);
    }
    public function getProduct(){
        return $this->hasone(Product::class,'id','product_id')->select(['id','product_url','title']);
    }
    public static function getData($request)
    {
        $string='?';
        $questions=self::with(['getProduct','getUserInfo','getParent'])->orderBy('id','DESC');
        if(inTrashed($request))
        {
            $questions=$questions->onlyTrashed();
            $string=create_paginate_url($string,'trashed=true');
        }
        if(array_key_exists('user_id',$request) && !empty($request['user_id']))
        {
            $questions=$questions->where('user_id',$request['user_id']);
            $string=create_paginate_url($string,'user_id='.$request['user_id']);
        }
        $questions=$questions->paginate(10);
        $questions->withPath($string);
        return $questions;
    }
    public function getParent()
    {
        return $this->hasone(Question::class,'id','question_id');
    }
    public function getQuestionAttribute($value)
    {
        return strip_tags(nl2br($value),'<br>');
    }
    public static function change_status($request)
    {
        if($request->ajax())
        {
            $question_id=$request->get('question_id');
            $question=Question::find($question_id);
            if($question){
                $status=$question->status==1 ? 0 : 1;
                $question->status=$status;
                if($question->update())
                {
                    if($question->question_id>0)
                    {
                        self::updateAnswerCount($question->question_id,$status);
                    }
                    if($question->question_id>0 &&  $status==1)
                    {
                        self::sendAnswerEmail($question);
                    }
                    return 'ok';
                }
                else{
                    return 'error';
                }
            }
            else{
                return 'error';
            }
        }
    }
    public static function sendAnswerEmail($answer){
        $question=Question::with(['getUserInfo','getProduct'])->where(['id'=>$answer->question_id,'send_email'=>'ok'])->first();
        if($question && $question->getUserInfo)
        {
            if(!empty($question->getUserInfo->email))
            {
                Mail::to($question->getUserInfo->email)->queue(new \App\Mail\SendAnswer($question,$answer));
            }
        }
    }
    public static function addAnswer($request,$id,$answer_text)
    {
        $user_id=$request->user()->id;
        $question=Question::where(['id'=>$id,'question_id'=>0])->firstOrFail();
        $question->answer_count=$question->answer_count+1;
        $question->update();

        $answer=new Question();
        $answer->time=time();
        $answer->user_id=$user_id;
        $answer->status=1;
        $answer->question_id=$id;
        $answer->question=$answer_text;
        $answer->product_id=$question->product_id;
        $answer->save();
        self::sendAnswerEmail($answer);
        return redirect()->back()->with('message','ثبت پاسخ با موفقیت انجام شد');
    }
    public static function updateAnswerCount($question_id,$status)
    {
        $question=Question::where(['id'=>$question_id,'question_id'=>0])->first();
        if($question)
        {
            if($status==1)
            {
                $question->answer_count=$question->answer_count+1;
            }
            else{
                $question->answer_count=$question->answer_count-1;
            }
            $question->update();
        }
    }
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($row){
            if(!$row->isForceDeleting())
            {
                if($row->question_id>0)
                {
                    DB::table('questions')->where('id',$row->question_id)->decrement('answer_count',1);
                }
            }
        });
        static::restoring(function ($row){
            DB::table('questions')->where('id',$row->question_id)->increment('answer_count',1);
        });
    }
}
