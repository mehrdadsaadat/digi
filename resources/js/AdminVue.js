window.Vue = require('vue');
Vue.component('pagination',require('laravel-vue-pagination'));
Vue.component('Cleave',require('vue-cleave-component'));
import IncredibleOffers from './components/IncredibleOffers';
import Counter from './components/Counter';
import OrderStep from './components/OrderStep';
import StockroomProductList from './components/StockroomProductList';
import StockroomOutputList from './components/StockroomOutputList';
import SaleReport from './components/SaleReport';
import PackageContent from './components/PackageContent';
import axios from 'axios';
import VueAxsio from 'vue-axios';
Vue.use(VueAxsio,axios);
Vue.prototype.$siteUrl='http://localhost/digikala/public';

const app = new Vue({
    el: '#app',
    components:{
        IncredibleOffers,
        Counter,
        OrderStep,
        StockroomProductList,
        StockroomOutputList,
        SaleReport,
        PackageContent
    }
});
