<?php

namespace App\Http\Controllers\Admin;

use App\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProvinceController extends CustomController
{
    protected  $model='Province';
    protected  $title='استان';
    protected $route_params='province';
    public function index(Request $request)
    {
        $province=Province::getData($request->all());
        $trash_province_count=Province::onlyTrashed()->count();
        return view('province.index',['province'=>$province,'trash_province_count'=>$trash_province_count,'req'=>$request]);

    }
    public function create()
    {
        return view('province.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,['name'=>'required'],[],['name'=>'نام استان']);
        $Province=new Province($request->all());
        $Province->saveOrFail();
        return redirect('admin/province')->with('message','ثبت استان با موفقیت انجام شد');
    }
    public function edit($id)
    {
        $province=Province::findOrFail($id);
        return view('province.edit',['province'=>$province]);
    }
    public function update($id,Request $request)
    {
        $province=Province::findOrFail($id);
        $this->validate($request,['name'=>'required'],[],['name'=>'نام استان']);
        $province->update($request->all());
        return redirect('admin/province')->with('message','ویرایش نام استان با موفقیت انجام شد');
    }
}
