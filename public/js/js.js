const site_url='http://localhost/digikala/public/';
let promo_single_count=0;
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $(".cat_item").mouseover(function () {
        const li_width=$(this).css('width');
        const ul_width=$(".index-cat-list ul").width();
        const a=li_width.replace('px','');
        const right=ul_width-$(this).offset().left-a+15;
        $('.cat_hover').css('width',li_width);
        $('.cat_hover').css('right',right);
        $('.cat_hover').css('transform','scaleX(1)');
        $('.li_div').hide();
        $('.li_div',this).show();
    });
    $('.index-cat-list').mouseleave(function () {
        $('.cat_hover').css('transform','scaleX(0)');
        $('.li_div').hide();
    });
    $(".discount_left_item div").click(function () {
        $(".discount_left_item div").removeClass('active');
        const id=$(this).attr('data-id');
        $('.discount-box-content .item').hide();
        $("#discount_box_link_"+id).show();
        $(this).addClass('active');
    });

    $(".discount_box_footer .slide-amazing").click(function () {
        const id=$(this).attr('data-id');
        $('.discount-box-content .item').hide();
        $("#discount_box_link_"+id).show();
        $(".discount_box_footer .slide-amazing").removeClass('active');
        $(this).addClass('active');
    });

    let discount_slider_count=0;
    let discount_slider_number=0;
    const discount_box_footer=$('.discount_box_footer').css('display');
    if(discount_box_footer=='none')
    {
        discount_slider_count=$('.discount_left_item div').length;
       const discount_slider=setInterval(function () {
           const discount_box_footer=$('.discount_box_footer').css('display');
           if(discount_box_footer=='none'){
               discount_slider_number++;
               $(".discount_left_item div").removeClass('active');
               $('.discount-box-content .item').hide();

               if(discount_slider_number>=discount_slider_count)
               {
                   discount_slider_number=0;
               }
               $("#item_number_"+discount_slider_number).addClass('active');
               const id=$("#item_number_"+discount_slider_number).attr('data-id');
               $("#discount_box_link_"+id).show();
           }
           else{
               clearInterval(discount_slider);
           }

        },5000);

    }
    $(document).on('click','.color_li',function () {
        const color_id=$(this).attr('data');
        const product_id=$("#product_id").val();
        change_color(color_id,product_id);
    });
    $('.send_btn').hover(function () {
        $('.send_btn .line').addClass('line2');
    },function () {
        $('.send_btn .line').removeClass('line2');
    });
    $('.show_more_important_item').click(function () {
        const more_important_item=$('.more_important_item').css('display');
        if(more_important_item=='none')
        {
            $(".more_important_item").slideDown();
            $('.show_more_important_item').text('موارد کمتر');
            $('.show_more_important_item').addClass('minus_important_item');
        }
        else{
            $(".more_important_item").slideUp();
            $('.show_more_important_item').text('موارد بیشتر');
            $('.show_more_important_item').removeClass('minus_important_item');
        }
    });

    $("#login_remember").click(function () {
       if($(this).hasClass('active'))
      {
          $(this).removeClass('active');
          $("#remember").removeAttr('checked');
      }
       else{
           $(this).addClass('active');
           $("#remember").attr('checked',true);
       }
    });
    $("#cart_btn").click(function () {
       $("#add_cart_form").submit();
    });
    $('.shipping_data_box .header_box').click(function () {
        const el=$(this).parent().find('.ordering_product_list');
        const display=el.css('display');
        if(display=='block')
        {
            el.slideUp();
        }
        else{
            el.slideDown();
        }
    });
    $('.title_box').click(function () {
        const el=$(this).parent().find('.filter_box');
        if(el.css('display')=='none'){
            el.slideDown();
            $('.fa-angle-down',this).removeClass('fa-angle-down').addClass('fa-angle-up');
        }
        else{
            el.slideUp();
            $('.fa-angle-up',this).removeClass('fa-angle-up').addClass('fa-angle-down');
        }
    });
    let search=new window.URLSearchParams(window.location.search);
    if(document.getElementById('product_status')){
        if(search.get('has_product')!=null)
        {
            if(search.get('has_product')==1){
                $("#product_status").toggles({
                    type:'Light',
                    text:{'on':'','off':''},
                    width:50,
                    direction:'rtl',
                    on:false
                });
            }
        }
        if(search.get('has_ready_to_shipment')!=null)
        {
            if(search.get('has_ready_to_shipment')==1){
                $("#send_status").toggles({
                    type:'Light',
                    text:{'on':'','off':''},
                    width:50,
                    direction:'rtl',
                    on:false
                });
            }
        }
    }

    $("#brand_search").on('keyup',function () {
        const input=$(this).val().toLowerCase();
        const li=$(this).parent().find('.product_cat_ul li');
        for (let i=0;i<li.length;i++)
        {
            if(li[i].innerText.toLowerCase().indexOf(input)>-1)
            {
                li[i].style.display='block';
            }
            else {
                li[i].style.display='none';
            }
        }
    });
    $('.remove_product_of_compare_list').click(function () {
        const product_id=$(this).attr('data-id');
        let url=window.location.href;
        url=url.replace('/dkp-'+product_id,'');
        window.location=url;
    });
    check_has_compare_list();
    $(".logout").click(function () {
        $("#logout_form").submit();
    });

    $('.expert_button').click(function () {
        const display=$(this).parent().find('.content').css('display');
        if(display=='block')
        {
            $(this).parent().find('.content').css('display','none');
            $(this).addClass('plus_btn');
        }
        else {
            $(this).parent().find('.content').css('display','block');
            $(this).removeClass('plus_btn');
        }
    });
    $('.more_content span').click(function () {
        if( $(this).parent().hasClass('show_short_content')){
            $('.tozihat .content div').css('max-height','250px');
            $(this).parent().removeClass('show_short_content');
            $(this).text('ادامه مطلب');
        }
        else {
            $('.tozihat .content div').css('max-height','none');
            $(this).text('بستن');
            $(this).parent().addClass('show_short_content');
        }
    });
    $('.item_slider').on('input',function () {

        const newValue=this.value;
        const left=(100 - (newValue)*25)+'%';
        $(this).parent().find('.rang_slider_div .active_rang_slider').css('left',left);

        const Array=['slider_step_two','slider_step_three','slider_step_four','slider_step_five','slider_step_six'];

        $(this).parent().find('.rang_slider_div .js-slider-step').removeClass('active_rang_step');
        for (let i=0;i<newValue;i++)
        {
            $(this).parent().find('.rang_slider_div .'+Array[i]).addClass('active_rang_step');
        }

        const title=$(this).parent().find('.rang_slider_div .'+Array[newValue]).attr('data-rate-title');
        $(this).parent().find('.rang_slider_div').attr('data-rate-title',title);
    });
    $('.input_add_point input[type="text"]').keyup(function () {
        const value=$(this).val();
        if(value.trim().length>2)
        {
            $(this).parent().find('button').css('display','block');
        }
        else {
            $(this).parent().find('button').css('display','none');
        }
    });

    $('.input_add_point button').click(function () {
        const value=$(this).parent().find('input[type="text"]').val();
        const name=$(this).parent().find('input[type="text"]').attr('id');
        if(value.trim().length>2)
        {
            const html='<div><span>'+value+'</span>' +
                '<span class="fa fa-close"></span>' +
                '<input type="hidden" value="'+value+'" name="'+name+'[]">' +
                '</div>';
            $("#"+name+"_input_box").append(html);
            $(this).parent().find('input[type="text"]').val('');
            $(this).hide();
        }
    });

    $(document).on('click','.score_comment_form .item_list  .fa-close',function () {
       $(this).parent().remove();
    });
    $("#comment_form").submit(function (event)
    {
        const comment_title=$("#comment_title").val();
        const comment_content=$("#comment_content").val();

        const check_title=check_comment_title(comment_title);
        const check_content=check_comment_content(comment_content);

        if (!check_title || !check_content)
        {
            event.preventDefault();
        }
    });

    const promo_single=$(".promo_single a");
    if(promo_single.length>0)
    {
        promo_single_count=promo_single.length;
        startPromoSingleSlide();
    }

    $(".cart-header-box .dropdown-menu").on({
        "click":function (e) {
            e.stopPropagation();
        }
    });
    $("#img_swiper div").click(function(){
        const n=$(this).index()+1;

        let new_value=(n*100);
        new_value=200-new_value;
        if(n==1)
        {
            new_value=200;
        }
        const newTransform="translate3d(0px,"+new_value+"px,0px)";
        document.getElementById("img_swiper").style.transform=newTransform;
        $('.swiper-slide').removeClass('img_select_border');
        $(this).addClass('img_select_border');
        const src=$(this).find('img').attr('src');
        $("#selected_img").attr('src',src);
        $("#image_zoom_rang").val(0);
        zoom_image();
    });

    const rangElement=document.getElementById('image_zoom_rang');
    if(rangElement!=null)
    {
        rangElement.addEventListener('input',function(){
            zoom_image();
        });
    }

    set_image_width();
    set_gallery_item_event();

    $('.rang_slider_plus').click(function(){
        const image_zoom_rang=parseInt($("#image_zoom_rang").val());
        if(image_zoom_rang<=95)
        {
            const new_value=image_zoom_rang+5;
            $("#image_zoom_rang").val(new_value);
            zoom_image();
        }
    });

    $('.rang_slider_minus').click(function(){
        const image_zoom_rang=parseInt($("#image_zoom_rang").val());
        if(image_zoom_rang>=5)
        {
            const new_value=image_zoom_rang-5;
            $("#image_zoom_rang").val(new_value);
            zoom_image();
        }
    });
    $('.common_question_header').click(function () {
        const display=$(this).parent().find('.small_answer').css('display');
        $('.small_answer').css('display','none');
        if(display=='none'){
            $(this).parent().find('.small_answer').slideDown();
            $(this).find('.fa').removeClass('fa-angle-down').addClass('fa-angle-up');
        }
        else{
            $(this).parent().find('.small_answer').slideUp();
            $(this).find('.fa').removeClass('fa-angle-up').addClass('fa-angle-down');
        }
    });
    $(document).on('change','#priceItem',function(){
        const  color_id=$(this).val();
        const product_id=$("#product_id").val();
        change_color(color_id,product_id,true);
    });
});

let img_count=0;
let img=0;
function load_slider(count) {
    img_count=count;
    setInterval(next,5000)
}
function next() {

    $('.slider_bullet_div span').removeClass('active');
    if(img==(img_count-1)){
        img=-1;
    }
    img=img+1;
    $('.slide_div').hide();
    document.getElementById('slider_img_'+img).style.display='block';
    $("#slider_bullet_"+img).addClass('active');
}
function previous() {
    $('.slider_bullet_div span').removeClass('active');
    img=img-1;
    if(img==-1)
    {
        img=(img_count-1);
    }
    $('.slide_div').hide();
    document.getElementById('slider_img_'+img).style.display='block';
    $("#slider_bullet_"+img).addClass('active');
}
function change_color(color_id,product_id,update) {
    $.ajaxSetup(
        {
            'headers':{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
    const url=site_url+"site/change_color";
    $.ajax({
        url: url,
        type:"POST",
        data:"color_id="+color_id+"&product_id="+product_id,
        success:function (response) {
            if(response)
            {
                $("#warranty_box").html(response);
                $("#offers_time").click();
                if(update!=undefined){
                    $(".selectpicker").selectpicker('refresh');
                }
            }
        }
    });
}
check_has_compare_list=function() {
    const check_has_compare_list=document.getElementsByClassName('compare_product_gallery');
    if(check_has_compare_list.length>0)
    {
        $(window).scroll(function (e) {
            if($(document).scrollTop()>200)
            {
                $('.compare_product_gallery').css('border-bottom','3px solid #2196F3');
                $('.compare_product_gallery .btn-primary').hide();
                $('.compare_product_gallery .btn-dark').hide();
                $('.gallery_box').css('height','300px');
                $('.compare_add').css('height','300px');
                $('.gallery_box img').css('width','80%');
            }
            else{
                $('.compare_product_gallery').css('border-bottom','0px');
                $('.compare_product_gallery .btn-primary').show();
                $('.compare_product_gallery .btn-dark').show();
                $('.gallery_box').css('height','360px');
                $('.compare_add').css('height','360px');
                $('.gallery_box img').css('width','90%');
            }
        });
    }
};
check_comment_title=function (title) {
    if(title.trim()==""){
        $("#comment_title_error_message").show().text('عنوان نظر را وارد نمایید');
        $("#comment_title").addClass('validate_error_border');
        return false;
    }
    else {
        $("#comment_title_error_message").hide();
        $("#comment_title").removeClass('validate_error_border');
        return true;
    }
};
check_comment_content=function (content) {
    if(content.trim().length==0){
        $("#comment_content_error_message").show().text('متن نظر را وارد نمایید');
        $("#comment_content").addClass('validate_error_border');
        return false;
    }
    else {
        $("#comment_content_error_message").hide();
        $("#comment_content").removeClass('validate_error_border');
        return true;
    }
};


let promo_index=0;
startPromoSingleSlide=function () {
    $('.promo_single_header').addClass('promo-single-bar');
    setInterval(function () {
        promo_index++;
        if(promo_index>(promo_single_count-1))
        {
            promo_index=0;
        }
        $('.promo_single a').removeClass('active');
        $("a[data-swiper-slide-index='"+promo_index+"']").addClass('active');
    },7000);
};
zoom_image=function(){
    const value=parseFloat($("#image_zoom_rang").val());
    const nw=parseFloat($("#selected_img").attr('nw'));
    let new_width=nw+value;
    new_width=new_width+'%';
    $("#selected_img").attr('width',new_width);

    const scrollWidth=document.getElementById('gallery_item').scrollWidth;
    const offsetWidth=document.getElementById('gallery_item').offsetWidth;

    const scrollHeight=document.getElementById('gallery_item').scrollHeight;
    const offsetHeight=document.getElementById('gallery_item').offsetHeight;

    const a=(scrollWidth-offsetWidth)/2;
    const b=(scrollHeight-offsetHeight)/2;

    document.getElementById('gallery_item').scroll(-a,b);
}
set_image_width=function()
{
    const selected_img=document.getElementById('selected_img');
    if(selected_img!=null)
    {
        selected_img.onload=function(){
           if(selected_img.naturalHeight>selected_img.naturalWidth)
           {
               selected_img.setAttribute('width','40%');
               selected_img.setAttribute('nw','40');
           }
           else if(selected_img.naturalWidth>600 && selected_img.naturalHeight<720)
           {
              selected_img.setAttribute('width','65%');
              selected_img.setAttribute('nw','65');
           }
           else{
            selected_img.setAttribute('width','55%');
            selected_img.setAttribute('nw','55');
           }
        };
    }
};
let isDown=false;
let startX;
let startY;
let scrollTop;
let scrollLeft;
set_gallery_item_event=function(){
    const gallery_item=document.getElementById('gallery_item');
    if(gallery_item!=null)
    {
        gallery_item.addEventListener('mousedown',(e)=>{
            isDown=true;
            startX=e.pageX-gallery_item.offsetLeft;
            startY=e.pageY-gallery_item.offsetTop;
            scrollLeft=gallery_item.scrollLeft;
            scrollTop=gallery_item.scrollTop;
        });
        gallery_item.addEventListener('mouseup',(e)=>{
            isDown=false;
        });
        gallery_item.addEventListener('mousemove',(e)=>{
            if(!isDown) return ;
            e.preventDefault();
            const x=e.pageX - gallery_item.offsetLeft;
            const y=e.pageY - gallery_item.offsetTop;

            const x1=(x-startX)*2;
            const y1=(y-startY)*2;
            gallery_item.scrollLeft=(scrollLeft-x1);
            gallery_item.scrollTop=(scrollTop-y1);
        })
    }
}
