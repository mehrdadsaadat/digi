<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\SmsChannel;
class SendSms extends Notification implements ShouldQueue
{
    use Queueable;
    protected $mobile_number;
    protected $message;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($mobile_number,$message)
    {
       $this->mobile_number=$mobile_number;
       $this->message=$message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
       return [
          'mobile_number'=>$this->mobile_number,
          'message'=>$this->message
       ]; 
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
          

        ];
    }
}
