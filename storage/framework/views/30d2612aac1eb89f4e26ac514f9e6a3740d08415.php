<?php $__env->startSection('content'); ?>
    <div class="row slider">
        <div class="col-2">
            <div>
                <a href="<?php echo e(url('')); ?>" >
                    <img src="<?php echo e(url('files/images/1000007524.jpg')); ?>"   <?php if(sizeof($incredible_offers)==0): ?> style="height:155px" <?php endif; ?> class="index-pic">
                </a>
                <a href="<?php echo e(url('')); ?>" >
                    <img src="<?php echo e(url('files/images/1000007568.jpg')); ?>" <?php if(sizeof($incredible_offers)==0): ?> style="height:155px" <?php endif; ?> class="index-pic">
                </a>
              <?php if(sizeof($incredible_offers)>0): ?>
                <a href="<?php echo e(url('')); ?>">
                    <img src="<?php echo e(url('files/images/1000005397.jpg')); ?>" class="index-pic">
                </a>
                <a href="<?php echo e(url('')); ?>">
                    <img src="<?php echo e(url('files/images/1000005397.jpg')); ?>" class="index-pic">
                </a>
              <?php endif; ?>
            </div>
        </div>
        <div class="col-10">
            <?php if(sizeof($sliders)>0): ?>
                <div class="slider_box">
                    <div style="position:relative" >
                        <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="slide_div an" id="slider_img_<?php echo e($key); ?>" <?php if($key==0): ?> style="display:block" <?php endif; ?>>
                                <a href="<?php echo e($value->url); ?>" style='background-image:url("<?= url('files/slider/'.$value->image_url) ?>")'></a>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>


                    <div id="right-slide" onclick="previous()"></div>
                    <div id="left-slide" onclick="next()"></div>
                </div>
                <div class="slider_box_footer">
                    <div class="slider_bullet_div">
                        <?php for($i=0;$i<sizeof($sliders);$i++): ?>
                            <span id="slider_bullet_<?php echo e($i); ?>" <?php if($i==0): ?> class="active" <?php endif; ?>></span>
                        <?php endfor; ?>
                    </div>
                </div>

            <?php endif; ?>


           <?php echo $__env->make('include.incredible-offers', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>





        </div>
    </div>

    <div class="row">

        <?php if(sizeof($randomProduct)>1): ?>
            <div class="col-md-9">
                <?php echo $__env->make('include.horizontal_product_list',['title'=>'جدید ترین محصولات فروشگاه','products'=>$new_products], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-3 promo_single">
                <div class="promo_single_header">
                    <span>پیشنهاد های لحظه ای برای شما</span>
                </div>
                <?php $__currentLoopData = $randomProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a data-swiper-slide-index="<?php echo e($key); ?>" href="<?php echo e(url('product/dkp-'.$value->id.'/'.$value->product_url)); ?>" <?php if($key==0): ?> class="active" <?php endif; ?>>
                        <img src="<?php echo e(url('files/thumbnails/'.$value->image_url)); ?>">
                        <p class="title">
                            <?php if(strlen($value->title)>50): ?>
                                <?php echo e(mb_substr($value->title,0,33).'...'); ?>

                            <?php else: ?>
                                <?php echo e($value->title); ?>

                            <?php endif; ?>
                        </p>
                        <?php
                          $price1=$value->price+$value->discount_price;
                        ?>
                        <p class="discount_price">
                            <?php if(!empty($value->discount_price)): ?>
                                <del>
                                    <?php echo e(replace_number(number_format($price1))); ?>

                                </del>
                            <?php endif; ?>
                        </p>
                        <p class="price">
                            <?php echo e(replace_number(number_format($value->price)).' تومان'); ?>

                        </p>
                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php else: ?>
            <?php echo $__env->make('include.horizontal_product_list',['title'=>'جدید ترین محصولات فروشگاه','products'=>$new_products], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endif; ?>
    </div>

    <div class="row">
        <?php echo $__env->make('include.horizontal_product_list',['title'=>' پرفروش ترین محصولات فروشگاه','products'=>$best_selling_product], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/swiper.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('slick/slick.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('slick/slick-theme.css')); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('seo'); ?>
<meta name="description" content="<?php echo e(config('shop-info.description')); ?>"/>
    <meta name="keywords" content="<?php echo e(config('shop-info.keywords')); ?>"/>
    <meta property="og:site_name" content="<?php echo e(config('shop-info.shop_name')); ?>"/>
    <meta property="og:description" content="<?php echo e(config('shop-info.description')); ?>"/>
    <meta property="og:title" content="<?php echo e(config('shop-info.shop_name')); ?>"/>
    <meta property="og:locale" content="fa_IR"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(asset('js/swiper.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('slick/slick.js')); ?>"></script>
    <script>
        load_slider('<?= sizeof($sliders) ?>');
        const swiper=new Swiper('.swiper-container',{
            slidesPerView:'auto',
            spaceBetween:10,
            navigation:{
                nextEl:'.slick-next',
                prevEl:'.slick-prev'
            }
        });
        <?php
            if(sizeof($incredible_offers)<6)
            {
                ?>
                  $(".discount_box_footer .slick-next").hide();
                  $(".discount_box_footer .slick-prev").hide();
                <?php
            }
        ?>
        $('.product_list').slick({
             speed: 900,
             slidesToShow: 4,
             slidesToScroll: 3,
             rtl:true,
             infinite: false,
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.shop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/shop/index.blade.php ENDPATH**/ ?>