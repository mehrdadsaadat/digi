<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Color;
use App\Http\Controllers\Controller;
use App\ProductWarranty;
use Illuminate\Http\Request;

class PriceVariationController extends Controller
{
    public function priceVariation($id)
    {
        $category=Category::findOrFail($id);
        $priceVariation=Color::where(['type'=>2,'cat_id'=>$id])->orderBy('position','ASC')->get();
        return view('priceVariation.index',['category'=>$category,'priceVariation'=>$priceVariation]);
    }
    public function add_price_variation($cat_id,Request $request)
    {
        $price_variation=$request->get('price_variation',array());

        Color::addPriceVariation($price_variation,$cat_id);
        return redirect()->back()->with('message','ثبت معیار تعیین هزینه با موفقیت انجام شد');
    }
    public function destroy($id)
    {
        $check=ProductWarranty::where('color_id',$id)->first();
        if($check){
            return redirect()->back()->with('warring','خطا : مقدار انتخاب شده قابل حذف نمی باشد');
        }
        else{
            $price_variation=Color::where(['id'=>$id,'type'=>2])->firstOrFail();
            $price_variation->forceDelete();
            return redirect()->back()->with('message','حذف معیار تعیین هزینه با موفقیت انجام شد');
        }
    }
}
