@extends('layouts.mobile')

@section('content')
    <div class="profile_menu">

        <div class="profile_item_header order_content_header">
            <div>
                <span> جزییات پیام - {{ $message->title }}</span>
            </div>
            <a href="{{ url('user/profile/messages') }}">
                <span>بازگشت</span>
                <span class="fa fa-angle-left"></span>
            </a>
        </div>
        <div class="message_content_div">
            <?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
            @include('include.Alert')
            <div class="user_message_div">
                <div class="user_message_div_header">
                    <div>
                        @if($message->from_type=='App\\User')
                            @if($message->from)
                                @if($message->from_id==$message->user_id)
                                    <span  @if($message->from_id==$message->user_id) class="form_link" @else class="to_link" @endif >
                                           ارسال کننده : {{ $message->from->name }}
                                           </span>
                                @else
                                    {{ config('shop-info.shop_name') }}
                                @endif
                            @endif
                        @endif
                    </div>
                    <div>
                        {{ $jdf->jdate('H:i:s',$message->time)  }} / {{  $jdf->jdate('Y-n-j',$message->time) }}
                    </div>
                </div>

                <div class="message_content">
                    {!! strip_tags($message->content,'<br>') !!}
                    @if(!empty($message->file))
                        <div  class="attached-file">
                            <span>فایل ضمیمه شده : </span>
                            <a href="{{ url('/files/upload/'.$message->file) }}" target="_blank">
                                {{ $message->file }}
                            </a>
                        </div>
                    @endif
                </div>
            </div>
            @foreach($message->getAnswer as $answer)
                <div class="user_message_div">
                    <div class="user_message_div_header">
                        <div>
                            @if($answer->from_type=='App\\User')
                                @if($answer->from)
                                    @if($answer->from_id==$answer->user_id)
                                        <span  @if($answer->from_id==$answer->user_id) class="form_link" @else class="to_link" @endif >
                                           ارسال کننده : {{ $answer->from->name }}
                                           </span>
                                    @else
                                        <span class="to_link">ارسال کننده : {{ config('shop-info.shop_name') }}</span>
                                    @endif
                                @endif

                            @endif
                        </div>
                        <div>
                            {{ $jdf->jdate('H:i:s',$answer->time)  }} / {{  $jdf->jdate('Y-n-j',$answer->time) }}
                        </div>
                    </div>

                    <div class="message_content">
                        {!! strip_tags($answer->content,'<br>') !!}
                        @if(!empty($answer->file))
                            <div  class="attached-file">
                                <span>فایل ضمیمه شده : </span>
                                <a href="{{ url('/files/upload/'.$answer->file) }}" target="_blank">
                                    {{ $answer->file }}
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>

        <div class="message_form">
            <span style="color:gray;padding-top:20px;display:block;padding-bottom: 20px">ارسال پاسخ</span>

            {!! Form::open(['url' => 'user/profile/messages/addAnswer/'.$message->id,'files'=>true]) !!}

            {{ method_field('PUT') }}
            <div class="form-group center_item">
                {{ Form::label('content','محتوای پیام : ') }}
                {{ Form::textArea('content',null,['class'=>'form-control','style'=>'width:100%']) }}
            </div>

            <div class="form-group">
                @if($errors->has('content'))
                    <p class="has_error" >{{ $errors->first('content') }}</p>
                @endif
            </div>

            <div class="form-group margin-10 file-input-div">
                {{ Form::label('pic','انتخاب فایل : ') }}
                <input type="file" name="pic" id="pic" style="direction:ltr">
                @if($errors->has('pic'))
                    <span class="has_error">{{ $errors->first('pic') }}</span>
                @endif
            </div>

            <button class="btn btn-success margin-10">ارسال پاسخ</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
