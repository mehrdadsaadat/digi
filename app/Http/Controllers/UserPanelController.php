<?php

namespace App\Http\Controllers;

use App\AdditionalInfo;
use App\City;
use App\Comment;
use App\GiftCart;
use App\Http\Requests\AdditionalRequest;
use App\Http\Requests\MessageRequest;
use App\Lib\Mobile_Detect;
use App\Message;
use App\Order;
use App\OrderData;
use App\Province;
use App\User;
use Illuminate\Http\Request;
use Auth;
class UserPanelController extends Controller
{
    protected $view='';
    public function __construct()
    {
        getCatList();
        $detect=new Mobile_Detect();
        if($detect->isMobile() || $detect->isTablet()){
            $this->view='mobile.';
        }
    }
    public function gift_cart(Request $request){

        $user_id=$request->user()->id;
        $gift_carts=GiftCart::where('user_id',$user_id)->orderBy('id','DESC')->paginate(10);
        set_new_message_count($user_id);
        return view($this->view.'userPanel.gift_cart',['gift_carts'=>$gift_carts]);
    }
    public function orders(Request $request)
    {
        $user_id=$request->user()->id;
        $orders=Order::where('user_id',$user_id)->orderBy('id','DESC')->paginate();
        set_new_message_count($user_id);
        return view($this->view.'userPanel.orders',['orders'=>$orders]);
    }
    public function show_order($order_id,Request $request){
        $user_id=$request->user()->id;
        $order=Order::with(['getProductRow.getSeller','getOrderInfo','getAddress','getGiftCart'])
            ->where(['id'=>$order_id,'user_id'=>$user_id])->firstOrFail();

        $order_data=new OrderData($order->getOrderInfo,$order->getProductRow,$order->user_id);
        $order_data=$order_data->getData();
        set_new_message_count($user_id);
        return view($this->view.'userPanel.show_order',['order'=>$order,'order_data'=>$order_data]);
    }
    public function profile()
    {
        $user=Auth::user()->id;
        $orders=null;
        $additionalInfo=null;
        if($this->view!='mobile.')
        {
            $additionalInfo=AdditionalInfo::where(['user_id'=>$user])->with(['getProvince','getCity'])->first();
            $orders=Order::where(['user_id'=>$user])->orderBy('id','DESC')->limit(10)->get();
        }
        set_new_message_count($user);
        return view($this->view.'userPanel.profile',['orders'=>$orders,'additionalInfo'=>$additionalInfo]);
    }
    public function additional_info(){
        $user=Auth::user()->id;
        $province=Province::pluck('name','id')->toArray();
        $province=[''=>'انتخاب استان']+$province;
        $additionalInfo=AdditionalInfo::where(['user_id'=>$user])->first();
        $city=[''=>'انتخاب شهر'];
        if($additionalInfo && !empty($additionalInfo->city_id)){
            $city=City::where('province_id',$additionalInfo->province_id)->pluck('name','id')->toArray();
        }
        set_new_message_count($user);
        return view($this->view.'userPanel.additional_info',['additionalInfo'=>$additionalInfo,'province'=>$province,'city'=>$city]);
    }
    public function save_additional_info(AdditionalRequest $request){
        $user_id=$request->user()->id;
        $user=\App\User::findOrFail($user_id);
        return AdditionalInfo::addUserData($user,$request);
    }
    public function address(){
        $user_id=Auth::user()->id;
        set_new_message_count($user_id);
        return view($this->view.'userPanel.address');
    }
    public function personal_info(){
        if($this->view=='mobile.')
        {
            $user=Auth::user()->id;
            set_new_message_count($user);
            $additionalInfo=AdditionalInfo::where(['user_id'=>$user])->first();
            return view($this->view.'userPanel.personal-info',['additionalInfo'=>$additionalInfo]);
        }
        else{
            return redirect('/user/profile');
        }
    }
    public function favorite()
    {
        $user_id=Auth::user()->id;
        set_new_message_count($user_id);
        return view($this->view.'userPanel.favorite');
    }
    public function addMessageForm()
    {
        $user_id=Auth::user()->id;
        set_new_message_count($user_id);
        return view($this->view . 'userPanel.add_message');
    }
    public function addMessage(MessageRequest $request)
    {
        return User::addMessage($request);
    }
    public function getUserMessage(Request $request)
    {
        $id=$request->user()->id;
        set_new_message_count($id);
        $messages=Message::getMessage($id,'App\User',$request->all());
        return view($this->view . 'userPanel.messages',['messages'=>$messages,'req'=>$request]);
    }
    public function showMessageContent($id,Request $request)
    {
        $user_id=$request->user()->id;
        $message = Message::with(['getAnswer'=>function($query){
            $query->with(['to','from']);
        }])->where(['id' => $id,'parent_id'=>0,'user_id'=>$user_id,'user_type'=>'App\User'])->firstOrFail();
        if($message->status==-1){
            $message->status=0;
            $message->update();
        }
        set_new_message_count($user_id);
        return view($this->view . 'userPanel.message_content',['message'=>$message]);
    }
    public function addAnswer($id,MessageRequest $request)
    {
        $user_id = $request->user()->id;
        $message = Message::where(['id' => $id,'parent_id' => 0,'user_id'=>$user_id,'user_type'=>'App\User'])->firstOrFail();
        return User::addAnswer($request,$message,$user_id);
    }
    public function comments(Request $request)
    {
        $user_id = $request->user()->id;
        set_new_message_count($user_id);
        $comments=Comment::where('user_id',$user_id)->orderBy('id','DESC')->with(['getProduct','getScore'])
            ->paginate(10);
        return view($this->view . 'userPanel.comments',['comments'=>$comments]);
    }

}
