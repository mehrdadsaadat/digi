@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'تنظیمات فروشگاه','url'=>url('admin/setting/shop')],
    ]])

    <div class="panel">

        <div class="header">تنظیمات فروشگاه</div>

        <div class="panel_content">

            {!! Form::open(['url' => 'admin/setting/shop','files'=>true]) !!}

            <div class="form-group">
                {{ Form::label('shop_name','عنوان فروشگاه') }}
                {{ Form::text('shop_name',config('shop-info.shop_name'),['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('shop_icon','ایکون فروشگاه') }}
                {{ Form::file('shop_icon',['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('login_url','آدرس ورود به پنل مدیریت') }}
                {{ Form::text('login_url',config('shop-info.login_url'),['class'=>'form-control left']) }}
            </div>
            <p class="message_text">برچسب ها با استفاده از (،) از هم جدا شود</p>
            <div class="form-group">
                <input type="text" name="tag_list"  id="tag_list" class="form-control" placeholder="برجسب های محصول">
                <div class="btn btn-success" onclick="add_tag()">افزودن</div>
                <input type="hidden" value="{{ config('shop-info.keywords') }}" name="keywords" id="keywords">
            </div>

            <div id="tag_box">
                @php
                $keywords=config('shop-info.keywords');
                $e=explode(',',$keywords);
                $i=1;
                @endphp
                @if(is_array($e))
                    @foreach($e as $key=>$value)
                      @if(!empty($value))

                            <div class="tag_div" id="tag_div_{{ $i }}">
                                <span  class="fa fa-remove" onclick="remove_tag({{ $i }},'{{ $value }}')"></span>
                                {{ $value }}
                           </div>
                            @php $i++;  @endphp
                      @endif

                    @endforeach
                @endif
            </div>
            <div style="clear:both"></div>

            <div class="form-group" style="margin-top:10px">

                {{ Form::textarea('description',config('shop-info.description'),['class'=>'form-control','id'=>'description','placeholder'=>'توضیحات مختصر در مورد فروشگاه (حداکثر 150 کاراکتر)']) }}
            </div>
            <button class="btn btn-success">ثبت اطلاعات</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection    