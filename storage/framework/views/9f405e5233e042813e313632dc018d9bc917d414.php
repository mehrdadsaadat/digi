<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت تنوع های قیمت','url'=>url('admin/product_warranties?product_id='.$product->id)],
         ['title'=>'افزودن تنوع قیمت','url'=>url('admin/product_warranties/create?product_id='.$product->id)]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="panel">

        <div class="header">
            افزون تنوع قیمت جدید برای <?php echo e($product->title); ?>

        </div>

        <div class="panel_content">


            <?php echo $__env->make('include.warring', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo Form::open(['url' => 'admin/product_warranties?product_id='.$product->id]); ?>



            <div class="form-group">
                <?php echo e(Form::label('warranty_id','انتخاب گارانتی : ')); ?>

                <?php echo e(Form::select('warranty_id',$warranty,null,['class'=>'selectpicker auto_width','data-live-search'=>'true'])); ?>

            </div>

            <?php if(sizeof($colors)>0): ?>
                <div class="form-group">
                    <label>
                        <?php if($colors[0]->type==1): ?>
                            انتخاب رنگ :
                        <?php else: ?>
                            انتخاب سایز :
                        <?php endif; ?>
                    </label>
                    <select class="selectpicker auto_width" data-live-search="true"  name="color_id">
                        <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($value->type==1): ?>
                                <option value="color_<?php echo e($value->id); ?>" data-content="<span  style='background:#<?php echo e($value->code); ?>; <?php if($value->name=='سفید'): ?> color:#000 <?php endif; ?>' class='color_option'><?php echo e($value->name); ?></span>"></option>
                            <?php else: ?>
                                <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            <?php endif; ?>


            <div class="form-group">

                <?php echo e(Form::label('price1','قیمت محصول : ')); ?>

                <?php echo e(Form::text('price1',null,['class'=>'form-control left price_input'])); ?>

                <?php if($errors->has('price1')): ?>
                    <span class="has_error"><?php echo e($errors->first('price1')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('price2','قیمت محصول برای فروش  : ')); ?>

                <?php echo e(Form::text('price2',null,['class'=>'form-control left discount_price_input'])); ?>

                <?php if($errors->has('price2')): ?>
                    <span class="has_error"><?php echo e($errors->first('price2')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('product_number','تعداد موجودی محصول  : ')); ?>

                <?php echo e(Form::text('product_number',null,['class'=>'form-control left product_number'])); ?>

                <?php if($errors->has('product_number')): ?>
                    <span class="has_error"><?php echo e($errors->first('product_number')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('product_number_cart','تعداد سفارش در سبد خرید  : ')); ?>

                <?php echo e(Form::text('product_number_cart',null,['class'=>'form-control left product_number_cart'])); ?>

                <?php if($errors->has('product_number_cart')): ?>
                    <span class="has_error"><?php echo e($errors->first('product_number_cart')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('send_time','زمان آماده سازی محصول  : ')); ?>

                <?php echo e(Form::text('send_time',null,['class'=>'form-control left'])); ?>

                <?php if($errors->has('send_time')): ?>
                    <span class="has_error"><?php echo e($errors->first('send_time')); ?></span>
                <?php endif; ?>
            </div>

            <button class="btn btn-success">ثبت تنوع قیمت</button>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/cleave.min.js')); ?>"></script>
<script>

    var cleave1 = new Cleave('.price_input', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave2 = new Cleave('.discount_price_input', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave3 = new Cleave('.discount_price_input', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave4 = new Cleave('.product_number', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave5 = new Cleave('.product_number_cart', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave6 = new Cleave('#send_time', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/product_warranties/create.blade.php ENDPATH**/ ?>