<?php $__env->startSection('content'); ?>

    <div class="order_header">
        <img src="<?php echo e(asset('files/images/shop_icon.jpg')); ?>" class="shop_icon">
        <ul class="checkout_steps">
            <li>
                <a class="checkout_step">
                    <div class="step_item active_item" step-title="اطلاعات ارسال"></div>
                </a>
            </li>

            <li class="inactive">
                <a class="checkout_step">
                    <div class="step_item" step-title="پرداخت"></div>
                </a>
            </li>

            <li class="inactive">
                <a class="checkout_step">
                    <div class="step_item" step-title="اتمام خرید"></div>
                </a>
            </li>
        </ul>
    </div>
    <div class="page_row">
        <div class="page_content">
            <form action="<?php echo e(url('payment')); ?>" id="add_order" method="post">
                <?php echo csrf_field(); ?>
                <input type="hidden" id="address_id" name="address_id">
                <input type="hidden" id="lat" name="lat" value="0.0">
                <input type="hidden" id="lng" name="lng" value="0.0">
                <input type="hidden" id="send_type" name="send_type" value="1">
            </form>
            <mobile-address-list  :data="<?php echo e(json_encode($address)); ?>"></mobile-address-list>
        </div>
        <div class="page_aside">
            <div class="order_info" style="margin-top: 0px">
                <?php
                $total_product_price=Session::get('total_product_price',0);
                $final_price=Session::get('final_price',0);
                $discount=$total_product_price-$final_price;
                ?>
                <ul>
                    <li>
                        <span>مبلغ کل </span>
                        <span>(<?php echo e(replace_number(\App\Cart::get_product_count())); ?>) کالا</span>
                        <span class="left"><?php echo e(replace_number(number_format($total_product_price))); ?> تومان</span>
                    </li>

                    <?php if($discount>0): ?>
                        <li class="cart_discount_li">
                            <span>سود شما از خرید</span>
                            <span class="left"><?php echo e(replace_number(number_format($discount))); ?> تومان</span>
                        </li>
                    <?php endif; ?>

                    <li>
                        <span>هزینه ارسال</span>
                        <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="هزینه ارسال مرسولات می‌تواند وابسته به شهر و آدرس گیرنده متفاوت باشد. در صورتی که هر یک از مرسولات حداقل ارزشی برابر با ۱۵۰هزار تومان داشته باشد، آن مرسوله بصورت رایگان ارسال می‌شود."></span>
                        <span class="left" id="total_send_order_price">رایگان</span>
                    </li>

                </ul>
                <div class="checkout_devider"></div>
                <div class="checkout_content">
                    <p style="color:red">مبلغ قابل پرداخت : </p>
                    <p id="final_price"><?php echo e(replace_number(number_format($final_price))); ?> تومان</p>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(url('css/cedarmaps.css')); ?>" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(url('js/cedarmaps.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('js/leaflet.rotatedMarker.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('js/Map.js')); ?>"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.mobile_order', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/shipping/set_data.blade.php ENDPATH**/ ?>