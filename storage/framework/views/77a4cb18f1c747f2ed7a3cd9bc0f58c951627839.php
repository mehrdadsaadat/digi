<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(url('css/swiper.min.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div style="position: relative;padding-bottom: 50px">

        <?php if(Session::has('comment_status')): ?>
            <div class="alert <?php if(Session::get('comment_status')=='ok'): ?> alert-success <?php else: ?> alert-danger <?php endif; ?> custom-alert">
                <?php if(Session::get('comment_status')=='ok'): ?>
                    نظر شما با موفقیت ثبت شد و بعد از تایید نمایش داده خواهد شد
                <?php else: ?>
                    خطا در ثبت اطلاعات،مجددا تلاش نمایید
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="product_item_box margin">
            <div class="product_headline">
                <offer-time></offer-time>
                <h6 class="product_title">
                    <?php echo e($product->title); ?>

                    <?php if(!empty($product->ename) && $product->ename!='null'): ?> <span><?php echo e($product->ename); ?></span> <?php endif; ?>
                </h6>
            </div>
            <div class="product_options">
                <div>
                    <a class="favorite" product-id="<?php echo e($product->id); ?>">
                        <span class="fa fa-heart-o  <?php if($favorite): ?> chosen <?php endif; ?>" ></span>
                    </a>
                    <span class="fa fa-share-alt"></span>
                    <span class="fa fa-line-chart"></span>
                </div>
                <div style="display: flex;align-items: center">
                    <?php
                    $width=0;
                    if($product->score_count>0)
                    {
                        $width=$product->score/($product->score_count*6);
                    }
                    $width=$width*20;
                    ?>
                    <span><?php echo e(replace_number($product->score_count)); ?> نفر</span>
                    <div class="score">
                        <div class="gray">
                            <div class="red" style="width: <?php echo e($width); ?>%"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php if(sizeof($product->Gallery)>0): ?>

                    <div class="swiper-container" id="gallery" dir="rtl">
                        <div class="swiper-wrapper">
                            <?php $__currentLoopData = $product->Gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="swiper-slide">
                                    <img src="<?php echo e(url('files/gallery/'.$value->image_url)); ?>">
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                <?php else: ?>
                    <img src="<?php echo e(url('files/products/'.$product->image_url)); ?>" class="product_image">
                <?php endif; ?>
            </div>

            <div class="row">
                <ul class="list-inline product_data_ul">
                    <li>
                        <span>برند : </span>
                        <a href="<?php echo e(url('brand/'.$product->getBrand->brand_ename)); ?>" class="data_link">
                            <span><?php echo e($product->getBrand->brand_name); ?></span>
                        </a>
                    </li>
                    <li>
                        <span>دسته بندی : </span>
                        <a href="<?php echo e(url('search/'.$product->getCat->url)); ?>" class="data_link">
                            <span><?php echo e($product->getCat->name); ?></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="product_item_box">
            <div style="padding: 20px">
                <?php if($product->status==1): ?>
                    <div id="warranty_box">
                        <?php echo $__env->make('include.warranty',['color_id'=>0], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php if($product->fake==1): ?>
                        <p class="fake_tag">
                            <span class="fa fa-warning"></span>
                            <span>این محصول توسط تولید کننده اصلی (برند) تولید نشده است</span>
                        </p>
                        <?php endif; ?>
                    </div>
                <?php else: ?>
                    <div class="product_unavailable">
                       <span>
                           <?php if($product->status==-1): ?>
                               توقف تولید
                           <?php else: ?>
                               ناموجود
                           <?php endif; ?>
                        </span>
                        <p>
                            <?php if($product->status==-1): ?>
                                متاسفانه تولید و فروش این کالا متوقف شده است. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید.
                            <?php else: ?>
                                متاسفانه این کالا در حال حاضر موجود نیست. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید
                            <?php endif; ?>
                        </p>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <?php if($product->status==1): ?>
            <mobile-other-price :product_id="<?php echo e($product->id); ?>"></mobile-other-price>
        <?php endif; ?>

        <?php if($product->status==1): ?>
            <div class="add_product_link">
                <span>افزودن محصول به سبد خرید</span>
            </div>
        <?php endif; ?>
        <?php if($product_item_count>0): ?>
            <div class="product_item_box">
                <div style="padding: 15px">
                    <div class="item_box remove_item_box_shadow">
                        <span>مشخصات فنی</span>
                        <a id="show_more_item_product">
                            <span>بیشتر</span>
                            <span class="fa fa-angle-left"></span>
                        </a>
                    </div>
                    <?php echo $__env->make('include.show_important_item',['remove_title'=>true], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php echo $__env->make('mobile.show_product_comments', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('mobile.show_product_questions', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <?php echo $__env->make('mobile.product_item_list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
     <div id="share_div">
         <div id="share_box">
             <p>
                 با استفاده از روش‌های زیر می‌توانید این صفحه را با دوستان خود به اشتراک بگذارید.
             </p>
             <ul>
                 <li>
                     <a href="https://telegram.me/share/url?url=<?= url('product/dkp-'.$product->id.'/'.$product->product_url) ?>&ref=telegram">
                         <span class="fa fa-telegram"></span>
                     </a>
                 </li>
                 <li>
                     <a href="https://twitter.com/intent/tweet/?url=<?= url('product/dkp-'.$product->id.'/'.$product->product_url) ?>">
                         <span class="fa fa-twitter"></span>
                     </a>
                 </li>
                 <li>
                     <a href="https://www.facebook.com/sharer/sharer.php?m2w&s=100&p[url]=<?= url('product/dkp-'.$product->id.'/'.$product->product_url) ?>">
                         <span class="fa fa-facebook"></span>
                     </a>
                 </li>
                 <li>
                     <a href="https://wa.me?text=<?= url('product/dkp-'.$product->id.'/'.$product->product_url) ?>&ref=telegram">
                         <span class="fa fa-whatsapp"></span>
                     </a>
                 </li>
             </ul>

         </div>
     </div>
    <mobile-vue-chart :product_id="<?php echo e($product->id); ?>"></mobile-vue-chart>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script src="<?php echo e(url('js/swiper.min.js')); ?>" type="text/javascript"></script>
    <script>
        var sliders=new Swiper('#gallery',{
            pagination:{
                el:'.swiper-pagination',
                dynamicBullets:true
            }
        });
        var product_slider=new Swiper('.products',{
            slidesPerView:2,
            spaceBetween:10
        });
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('seo'); ?>
<meta name="description" content="<?php echo e($product->description); ?>"/>
    <meta name="keywords" content="<?php echo e($product->keywords); ?>"/>
    <meta property="og:site_name" content="<?php echo e(config('shop-info.shop_name')); ?>"/>
    <meta property="og:description" content="<?php echo e($product->description); ?>"/>
    <meta property="og:title" content="<?php echo e($product->title); ?>"/>
    <meta property="og:locale" content="fa_IR"/>
    <meta property="og:image" content="<?php echo e(url('files/products/'.$product->image_url)); ?>"/>
    <meta name="twitter:description" content="<?php echo e($product->description); ?>"/>
    <meta name="twitter:title" content="<?php echo e($product->title); ?>"/>
    <meta name="twitter:image" content="<?php echo e(url('files/products/'.$product->image_url)); ?>"/>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.mobile', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/shop/show_product.blade.php ENDPATH**/ ?>