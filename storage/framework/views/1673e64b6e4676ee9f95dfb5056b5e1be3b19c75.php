<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت کد های تخفیف','url'=>url('admin/discount')],
         ['title'=>'افزودن کد تخفیف','url'=>url('admin/discount/create')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="panel">

        <div class="header">
            افزودن کد تخفیف
        </div>

        <div class="panel_content discount_form">


            <?php echo $__env->make('include.warring', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo Form::open(['url' => 'admin/discount']); ?>


            <div class="form-group">

                <?php echo e(Form::label('code','کد تخفیف : ')); ?>

                <?php echo e(Form::text('code',null,['class'=>'form-control left'])); ?>

                <?php if($errors->has('code')): ?>
                    <span class="has_error"><?php echo e($errors->first('code')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('expiry_time','تاریخ انقضا : ')); ?>

                <?php echo e(Form::text('expiry_time',null,['class'=>'form-control','id'=>'expiry_time','style'=>'text-align:center'])); ?>

                <?php if($errors->has('expiry_time')): ?>
                    <span class="has_error"><?php echo e($errors->first('expiry_time')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <?php echo e(Form::label('cat_id','انتخاب دسته : ')); ?>

                <?php echo e(Form::select('cat_id',$cat,null,['class'=>'selectpicker','data-live-search'=>'true'])); ?>

            </div>



            <div class="form-group">

                <?php echo e(Form::label('amount','حداقل خرید : ')); ?>

                <?php echo e(Form::text('amount',null,['class'=>'form-control left amount'])); ?>

                <?php if($errors->has('amount')): ?>
                    <span class="has_error"><?php echo e($errors->first('amount')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('number_usable','حداکثر دفعات استفاده از کد تخغیف : ')); ?>

                <?php echo e(Form::text('number_usable',null,['class'=>'form-control left number_usable'])); ?>

                <?php if($errors->has('number_usable')): ?>
                    <span class="has_error"><?php echo e($errors->first('number_usable')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('amount_discount','میزان  تخفیف (بر حسب تومان)  : ')); ?>

                <?php echo e(Form::text('amount_discount',null,['class'=>'form-control left amount_discount'])); ?>

                <?php if($errors->has('amount_discount')): ?>
                    <span class="has_error"><?php echo e($errors->first('amount_discount')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('amount_percent','میزان تخفیف (بر حسب درصد)  : ')); ?>

                <?php echo e(Form::text('amount_percent',null,['class'=>'form-control left amount_percent'])); ?>

                <?php if($errors->has('amount_percent')): ?>
                    <span class="has_error"><?php echo e($errors->first('amount_percent')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label>استفاده برای پیشنهادات شگفت انگیز</label>
                <input type="checkbox" name="incredible_offers" />
            </div>

            <button class="btn btn-success">ثبت </button>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(asset('css/js-persian-cal.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(asset('js/js-persian-cal.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/cleave.min.js')); ?>"></script>

    <script>
        const expiry_time= new AMIB.persianCalendar('expiry_time');
        var cleave1 = new Cleave('.amount', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        var cleave2 = new Cleave('.number_usable', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        var cleave3 = new Cleave('.amount_discount', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        var cleave4 = new Cleave('.amount_percent', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/discount/create.blade.php ENDPATH**/ ?>