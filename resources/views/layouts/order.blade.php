<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>فروشگاه تنیس شاپ</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('head')
    <link href="{{ asset('css/shop.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/js.js') }}" type="text/javascript"></script>
</head>
<body>

<div id="app">

    @yield('content')

</div>


<div id="loading_box">
    <div class="loading_div">
        <img src="{{ asset('files/images/shop_icon.jpg') }}">
        <div class="spinner">
            <div class="b1"></div>
            <div class="b2"></div>
            <div class="b3"></div>
        </div>
    </div>
</div>

<script src="{{ asset('js/ShopVue.js') }}" type="text/javascript"></script>
@yield('footer')
</body>
</html>
