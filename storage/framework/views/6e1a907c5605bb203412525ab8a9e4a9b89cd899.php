<?php $__env->startSection('content'); ?>

    <ul class="list-inline map_ul" style="margin-bottom:5px !important">
        <li>
            <a href="<?php echo e(url('/')); ?>">فروشگاه</a>
            /
        </li>
        <?php if($category && $category->getParent->getParent->name!='-'): ?>
           <li>
               <a href="<?php echo e(url('main/'.$category->getParent->getParent->url)); ?>">
                   <?php echo e($category->getParent->getParent->name); ?>

               </a>
               /
           </li>
        <?php endif; ?>
        <?php if($category &&  $category->getParent->name!='-'): ?>
           <li>
              <a href="<?php echo e(url('search/'.$category->getParent->url)); ?>">
                 <?php echo e($category->getParent->name); ?>

              </a>
             /
           </li>
        <?php endif; ?>
        <?php if($category): ?>
           <li>
              <a href="<?php echo e(url('search/'.$category->url)); ?>">
                 <?php echo e($category->name); ?>

              </a>
             /
           </li>
        <?php endif; ?>
        <li>
            <a href="<?php echo e(url()->current()); ?>">
                <?php echo e($product->title); ?>

            </a>
        </li>
    </ul>
    <div class="content">

        <?php if(Session::has('comment_status')): ?>
          <div class="alert <?php if(Session::get('comment_status')=='ok'): ?> alert-success <?php else: ?> alert-danger <?php endif; ?>">
              <?php if(Session::get('comment_status')=='ok'): ?>
                  نظر شما با موفقیت ثبت شد و بعد از تایید نمایش داده خواهد شد
              <?php else: ?>
                  خطا در ثبت اطلاعات،مجددا تلاش نمایید
              <?php endif; ?>
          </div>
        <?php endif; ?>
        <div class="product_info">

            <div class="product_image_box">
               <offer-time></offer-time>
               <div>
                   <ul class="product_options">
                       <li data-toggle="tooltip" data-placement="left" title="افزودن به علاقه مندی ها">
                            <a class="favorite" product-id='<?php echo e($product->id); ?>'>
                               <span class="fa fa-heart-o <?php if($favorite): ?> chosen <?php endif; ?>"></span>
                           </a>
                       </li>

                       <li data-toggle="tooltip" data-placement="left" title="اشتراک گذاری">
                           <a>
                               <span data-toggle="modal" data-target="#share_box" class="fa fa-share-alt"></span>
                           </a>
                       </li>

                       <li data-toggle="tooltip" data-placement="left" title="مقایسه">
                           <a href="<?php echo e(url('compare/dkp-'.$product->id)); ?>">
                               <span class="fa fa-compress"></span>
                           </a>
                       </li>

                       <li data-toggle="tooltip" data-placement="left" title="نمودار قیمت">
                           <a>
                               <span class="fa fa-line-chart"></span>
                           </a>
                       </li>
                   </ul>

                      <div class="default_product_pic">
                          <?php if(!empty($product->image_url)): ?>
                            <img class="default_pic" src="<?php echo e(url('files/products/'.$product->image_url)); ?>" data-zoom-img="<?php echo e(url('files/products/'.$product->image_url)); ?>">
                          <?php endif; ?>
                      </div>
                      <div class="product_gallery_box">
                          <?php echo $__env->make('include.Gallery', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                      </div>



               </div>
            </div>
            <div class="product_data">

                <div id="zoom_box"></div>
                <div class="product_headline">
                    <h6 class="product_title">
                        <?php echo e($product->title); ?>

                        <?php if(!empty($product->ename) && $product->ename!='null'): ?> <span><?php echo e($product->ename); ?></span> <?php endif; ?>
                    </h6>
                </div>
                <div>
                    <ul class="list-inline product_data_ul">
                        <li>
                            <span>برند : </span>
                            <a href="<?php echo e(url('brand/'.$product->getBrand->brand_ename)); ?>" class="data_link">
                                <span><?php echo e($product->getBrand->brand_name); ?></span>
                            </a>
                        </li>
                        <li>
                            <span>دسته بندی : </span>
                            <a href="<?php echo e(url('search/'.$product->getCat->url)); ?>" class="data_link">
                                <span><?php echo e($product->getCat->name); ?></span>
                            </a>
                        </li>
                    </ul>
                    <div class="row">

                        <div class="col-8">

                            <?php if($product->status==1): ?>
                                <div id="warranty_box">
                                    <?php echo $__env->make('include.warranty',['color_id'=>0], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                </div>
                                <div class="send_btn" id="cart_btn">
                                    <span class="line"></span>
                                    <span class="title">افزودن به سبد خرید</span>
                                </div>

                                <?php if($product->fake==1): ?>
                                    <p class="fake_tag">
                                        <span class="fa fa-warning"></span>
                                        <span>این محصول توسط تولید کننده اصلی (برند) تولید نشده است</span>
                                    </p>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="product-unavailable">
                                    <span>
                                        <?php if($product->status==-1): ?>
                                            توقف تولید
                                        <?php else: ?>
                                            ناموجود
                                        <?php endif; ?>
                                    </span>
                                    <p>
                                        <?php if($product->status==-1): ?>
                                            متاسفانه تولید و فروش این کالا متوقف شده است. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید.
                                        <?php else: ?>
                                            متاسفانه این کالا در حال حاضر موجود نیست. می‌توانید از طریق لیست بالای صفحه، از محصولات مشابه این کالا دیدن نمایید
                                        <?php endif; ?>
                                    </p>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-4">
                            <?php echo $__env->make('include.show_important_item', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if($product->status==1): ?>
        <other-price :product_id="<?php echo e($product->id); ?>"></other-price>
        <?php endif; ?>
        <?php echo $__env->make('include.horizontal_product_list',['title'=>'محصولات مرتبط','products'=>$relate_products], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div id="tab_div">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#review" role="tab" aria-selected="true">
                        <span class="fa fa-camera-retro"></span>
                        <span>نقد و بررسی</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#product_items" role="tab" aria-selected="false">
                        <span class="fa fa-list-ul"></span>
                        <span>مشخصات فنی</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" id="comments" href="#product_comments" role="tab" aria-selected="false">
                        <span class="fa fa-comment-o"></span>
                        <span>نظرات کاربران</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#question"  id="questions" role="tab" aria-selected="false">
                        <span class="fa fa-question-circle"></span>
                        <span>پرسش و پاسخ</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="review" role="tabpanel" aria-labelledby="home-tab">
                    <?php echo $__env->make('include.product_review', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="tab-pane fade" id="product_items" role="tabpanel" aria-labelledby="profile-tab">
                    <?php echo $__env->make('include.product_items', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="tab-pane fade" id="product_comments" role="tabpanel" aria-labelledby="contact-tab">

                    <comment-list auth="<?php echo Auth::check() ?  'ok' : 'no' ?>" product_id="<?= $product->id ?>" product_title="<?= $product->title ?>" ></comment-list>

                </div>
                <div class="tab-pane fade" id="question" role="tabpanel" aria-labelledby="contact-tab">
                    <question-list auth="<?php echo Auth::check() ?  'ok' : 'no' ?>" product_id="<?= $product->id ?>" product_title="<?= $product->title ?>" ></question-list>
                </div>
            </div>
        </div>

        <login-box></login-box>
        <vue-chart :product_id="<?php echo e($product->id); ?>"></vue-chart>

        <?php echo $__env->make('include.share-link', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('include.gallery-modal-box', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('slick/slick.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('slick/slick-theme.css')); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('seo'); ?>
<meta name="description" content="<?php echo e($product->description); ?>"/>
    <meta name="keywords" content="<?php echo e($product->keywords); ?>"/>
    <meta property="og:site_name" content="<?php echo e(config('shop-info.shop_name')); ?>"/>
    <meta property="og:description" content="<?php echo e($product->description); ?>"/>
    <meta property="og:title" content="<?php echo e($product->title); ?>"/>
    <meta property="og:locale" content="fa_IR"/>
    <meta property="og:image" content="<?php echo e(url('files/products/'.$product->image_url)); ?>"/>
    <meta name="twitter:description" content="<?php echo e($product->description); ?>"/>
    <meta name="twitter:title" content="<?php echo e($product->title); ?>"/>
    <meta name="twitter:image" content="<?php echo e(url('files/products/'.$product->image_url)); ?>"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(asset('slick/slick.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/jquery.elevateZoom-3.0.8.min.js')); ?>"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $('.product_list').slick({
            speed: 900,
            slidesToShow: 4,
            slidesToScroll: 3,
            rtl:true,
            infinite: false,
        });
        const product_tozihat=$("#product_tozihat")[0].scrollHeight;
        if(product_tozihat<250)
        {
            $('.more_content').hide();
        }
        $('.default_pic').elevateZoom({
            zoomWindowPosition:"zoom_box",
            borderSize:1,
            zoomWindowWidth:500,
            zoomWindowHeight:450,
            cursor:'zoom-in',
            scrollZoom:true
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.shop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/shop/show_product.blade.php ENDPATH**/ ?>