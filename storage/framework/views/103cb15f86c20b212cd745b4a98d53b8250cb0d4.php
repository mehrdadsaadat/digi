<?php $__env->startSection('content'); ?>

    <div class="row">

        <div class="col-md-3">
            <?php echo $__env->make('include.user_panel_menu',['active'=>'orders'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
        <div class="col-md-9" style="padding-right: 0px">

            <div class="profile_menu">

                <span class="profile_menu_title">سفارشات من</span>

                <?php echo $__env->make('include.user_order_list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php echo e($orders->links()); ?>

            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.shop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/userPanel/orders.blade.php ENDPATH**/ ?>