<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>فروشگاه تنیس شاپ</title>
    @yield('seo')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('head')
    <link href="{{ asset('css/mobile.css?id=aghuguahgrtet') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css?id=aghuguahgrtet') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/mobile.js?id=ewguaegru') }}" type="text/javascript"></script>
    <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
</head>
<body>

<div id="app">

    @include('mobile.CatList')
    <div>
        <div class="header">
            <span class="fa fa-align-justify"></span>
            <a href="{{ url('/') }}">
                <span>فروشگاه اینترنتی {{ env('SHOP_NAME','') }}</span>
            </a>
            <span></span>
        </div>
    </div>

   <div class="navbar">

       <mobile-header-search></mobile-header-search>

       <div>
           <a href="{{ url('Cart') }}" style="position: relative">
               @if(\App\Cart::get_product_count()>0)
                   <span class="cart_product_count">{{ replace_number(\App\Cart::get_product_count()) }}</span>
                @endif
               <span class="fa fa-shopping-basket"></span>
           </a>

           @if(Auth::check())
               <a href="{{ url('user/profile') }}"><span class="fa fa-user-o"></span></a>
           @else
               <a href="{{ url('login') }}"><span class="fa fa-user-o"></span></a>
           @endif
       </div>
   </div>
   <div class="container-fluid">
       @yield('content')
   </div>

    <div id="loading_box">
        <div class="loading_div">
            <img src="{{ asset('files/images/shop_icon.jpg') }}">
            <div class="spinner">
                <div class="b1"></div>
                <div class="b2"></div>
                <div class="b3"></div>
            </div>
        </div>
    </div>


    @include('mobile.footer')
</div>


<script src="{{ asset('js/ShopVue.js') }}" type="text/javascript"></script>
@yield('footer')
</body>
</html>
<script>
    import MobileHeaderSearch from "../../js/components/MobileHeaderSearch";
    export default {
        components: {MobileHeaderSearch}
    }
</script>
