<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Lib\Mobile_Detect;
use Session;
use Illuminate\Http\Request;
use App\User;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    protected $view='';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $detect=new Mobile_Detect();
        if($detect->isMobile() || $detect->isTablet()){
            $this->view='mobile.';
        }
    }
    public function confirm()
    {
        $mobile=Session::get('mobile');
        $token=Session::get('fotger_password_token');
        if($token && $mobile)
        {
            $layout=$this->view=='mobile.' ? 'mobile-auth' : 'auth';
            $margin=$this->view=='mobile.' ? '10' : '25';
            return view('auth/passwords/confirm',['mobile'=>$mobile,'margin'=>$margin,'layout'=>$layout]);
        }
        else{
            return redirect('/');
        }
    }
    public function check_confirm_code(Request $request)
    {
        $mobile=$request->get('mobile');
        $token=Session::get('fotger_password_token');
        $forget_password_code=$request->get('forget_password_code');
        $user=User::where(['forget_password_code'=>$forget_password_code,'mobile'=>$mobile])->first();
        if($user)
        {
            $user->forget_password_code=null;
            $user->update();
            Session::forget('token');
            return redirect('/password/reset/'.$token.'?mobile='.$mobile); 
        }
        else{
            return redirect()->back()
              ->with('mobile',$mobile)->with('token',$token)
              ->with('validate_error','کد وارد شده اشتباه میباشد')
              ->withInput();
        }
    }
}
