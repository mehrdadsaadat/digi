<?php

namespace App\Http\Controllers\Admin;

use App\CategoryCommonQuestion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryCommonQuestionController extends CustomController
{
    protected  $model='CategoryCommonQuestion';
    protected  $title='دسته';
    protected  $route_params='category-common-question';
    public function index(Request $request)
    {
        $CategoryCommonQuestion=CategoryCommonQuestion::getData($request->all());
        $trash_common_question_cat_count=CategoryCommonQuestion::onlyTrashed()->count();
        return view('category_common_question.index',['CategoryCommonQuestion'=>$CategoryCommonQuestion,'trash_common_question_cat_count'=>$trash_common_question_cat_count,'req'=>$request]);
    }
    public function create()
    {
        return view('category_common_question.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,['title'=>'required','pic'=>'image'],[],['title'=>'نام دسته','pic'=>'ایکون دسته']);
        $CategoryCommonQuestion=new CategoryCommonQuestion($request->all());
        $img_url=upload_file($request,'pic','upload');
        $CategoryCommonQuestion->icon=$img_url;
        $CategoryCommonQuestion->save();
        return redirect('admin/category-common-question')->with('message','ثبت دسته با موفقیت انجام شد');
    }
    public function edit($id)
    {
        $CategoryCommonQuestion=CategoryCommonQuestion::findOrFail($id);
        return view('category_common_question.edit',['CategoryCommonQuestion'=>$CategoryCommonQuestion]);
    }
    public function update($id,Request $request)
    {
        $this->validate($request,['title'=>'required','pic'=>'image'],[],['name'=>'نام دسته','pic'=>'ایکون دسته']);
        $data=$request->all();
        $category=CategoryCommonQuestion::findOrFail($id);
        $img_url=upload_file($request,'pic','upload');
        if($img_url!=null){
            $category->img=$img_url;
        }
        $category->update($data);
        return redirect('admin/category-common-question')->with('message','ویرایش دسته با موفقیت انجام شد');

    }
}
