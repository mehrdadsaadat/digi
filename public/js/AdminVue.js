/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/axios/index.js":
/*!*************************************!*\
  !*** ./node_modules/axios/index.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./lib/axios */ "./node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/axios/lib/adapters/xhr.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/adapters/xhr.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var settle = __webpack_require__(/*! ./../core/settle */ "./node_modules/axios/lib/core/settle.js");
var buildURL = __webpack_require__(/*! ./../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");
var parseHeaders = __webpack_require__(/*! ./../helpers/parseHeaders */ "./node_modules/axios/lib/helpers/parseHeaders.js");
var isURLSameOrigin = __webpack_require__(/*! ./../helpers/isURLSameOrigin */ "./node_modules/axios/lib/helpers/isURLSameOrigin.js");
var createError = __webpack_require__(/*! ../core/createError */ "./node_modules/axios/lib/core/createError.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request.onreadystatechange = function handleLoad() {
      if (!request || request.readyState !== 4) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        status: request.status,
        statusText: request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle browser request cancellation (as opposed to a manual cancellation)
    request.onabort = function handleAbort() {
      if (!request) {
        return;
      }

      reject(createError('Request aborted', config, 'ECONNABORTED', request));

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__(/*! ./../helpers/cookies */ "./node_modules/axios/lib/helpers/cookies.js");

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ?
        cookies.read(config.xsrfCookieName) :
        undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (config.withCredentials) {
      request.withCredentials = true;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/axios.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/axios.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");
var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");
var Axios = __webpack_require__(/*! ./core/Axios */ "./node_modules/axios/lib/core/Axios.js");
var mergeConfig = __webpack_require__(/*! ./core/mergeConfig */ "./node_modules/axios/lib/core/mergeConfig.js");
var defaults = __webpack_require__(/*! ./defaults */ "./node_modules/axios/lib/defaults.js");

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(mergeConfig(axios.defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__(/*! ./cancel/Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__(/*! ./cancel/CancelToken */ "./node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__(/*! ./cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__(/*! ./helpers/spread */ "./node_modules/axios/lib/helpers/spread.js");

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/Cancel.js":
/*!*************************************************!*\
  !*** ./node_modules/axios/lib/cancel/Cancel.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/CancelToken.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/cancel/CancelToken.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__(/*! ./Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/isCancel.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/cancel/isCancel.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),

/***/ "./node_modules/axios/lib/core/Axios.js":
/*!**********************************************!*\
  !*** ./node_modules/axios/lib/core/Axios.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var buildURL = __webpack_require__(/*! ../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");
var InterceptorManager = __webpack_require__(/*! ./InterceptorManager */ "./node_modules/axios/lib/core/InterceptorManager.js");
var dispatchRequest = __webpack_require__(/*! ./dispatchRequest */ "./node_modules/axios/lib/core/dispatchRequest.js");
var mergeConfig = __webpack_require__(/*! ./mergeConfig */ "./node_modules/axios/lib/core/mergeConfig.js");

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = arguments[1] || {};
    config.url = arguments[0];
  } else {
    config = config || {};
  }

  config = mergeConfig(this.defaults, config);
  config.method = config.method ? config.method.toLowerCase() : 'get';

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

Axios.prototype.getUri = function getUri(config) {
  config = mergeConfig(this.defaults, config);
  return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),

/***/ "./node_modules/axios/lib/core/InterceptorManager.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/core/InterceptorManager.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),

/***/ "./node_modules/axios/lib/core/createError.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/createError.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__(/*! ./enhanceError */ "./node_modules/axios/lib/core/enhanceError.js");

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};


/***/ }),

/***/ "./node_modules/axios/lib/core/dispatchRequest.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/core/dispatchRequest.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var transformData = __webpack_require__(/*! ./transformData */ "./node_modules/axios/lib/core/transformData.js");
var isCancel = __webpack_require__(/*! ../cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");
var defaults = __webpack_require__(/*! ../defaults */ "./node_modules/axios/lib/defaults.js");
var isAbsoluteURL = __webpack_require__(/*! ./../helpers/isAbsoluteURL */ "./node_modules/axios/lib/helpers/isAbsoluteURL.js");
var combineURLs = __webpack_require__(/*! ./../helpers/combineURLs */ "./node_modules/axios/lib/helpers/combineURLs.js");

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers || {}
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/core/enhanceError.js":
/*!*****************************************************!*\
  !*** ./node_modules/axios/lib/core/enhanceError.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }

  error.request = request;
  error.response = response;
  error.isAxiosError = true;

  error.toJSON = function() {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: this.config,
      code: this.code
    };
  };
  return error;
};


/***/ }),

/***/ "./node_modules/axios/lib/core/mergeConfig.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/mergeConfig.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");

/**
 * Config-specific merge-function which creates a new config-object
 * by merging two configuration objects together.
 *
 * @param {Object} config1
 * @param {Object} config2
 * @returns {Object} New object resulting from merging config2 to config1
 */
module.exports = function mergeConfig(config1, config2) {
  // eslint-disable-next-line no-param-reassign
  config2 = config2 || {};
  var config = {};

  utils.forEach(['url', 'method', 'params', 'data'], function valueFromConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    }
  });

  utils.forEach(['headers', 'auth', 'proxy'], function mergeDeepProperties(prop) {
    if (utils.isObject(config2[prop])) {
      config[prop] = utils.deepMerge(config1[prop], config2[prop]);
    } else if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (utils.isObject(config1[prop])) {
      config[prop] = utils.deepMerge(config1[prop]);
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });

  utils.forEach([
    'baseURL', 'transformRequest', 'transformResponse', 'paramsSerializer',
    'timeout', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName',
    'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress', 'maxContentLength',
    'validateStatus', 'maxRedirects', 'httpAgent', 'httpsAgent', 'cancelToken',
    'socketPath'
  ], function defaultToConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });

  return config;
};


/***/ }),

/***/ "./node_modules/axios/lib/core/settle.js":
/*!***********************************************!*\
  !*** ./node_modules/axios/lib/core/settle.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__(/*! ./createError */ "./node_modules/axios/lib/core/createError.js");

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  if (!validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};


/***/ }),

/***/ "./node_modules/axios/lib/core/transformData.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/core/transformData.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};


/***/ }),

/***/ "./node_modules/axios/lib/defaults.js":
/*!********************************************!*\
  !*** ./node_modules/axios/lib/defaults.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");
var normalizeHeaderName = __webpack_require__(/*! ./helpers/normalizeHeaderName */ "./node_modules/axios/lib/helpers/normalizeHeaderName.js");

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  // Only Node.JS has a process variable that is of [[Class]] process
  if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
    // For node use HTTP adapter
    adapter = __webpack_require__(/*! ./adapters/http */ "./node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(/*! ./adapters/xhr */ "./node_modules/axios/lib/adapters/xhr.js");
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Accept');
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/axios/lib/helpers/bind.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/helpers/bind.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/buildURL.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/helpers/buildURL.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    var hashmarkIndex = url.indexOf('#');
    if (hashmarkIndex !== -1) {
      url = url.slice(0, hashmarkIndex);
    }

    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/combineURLs.js":
/*!*******************************************************!*\
  !*** ./node_modules/axios/lib/helpers/combineURLs.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/cookies.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/helpers/cookies.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
    (function standardBrowserEnv() {
      return {
        write: function write(name, value, expires, path, domain, secure) {
          var cookie = [];
          cookie.push(name + '=' + encodeURIComponent(value));

          if (utils.isNumber(expires)) {
            cookie.push('expires=' + new Date(expires).toGMTString());
          }

          if (utils.isString(path)) {
            cookie.push('path=' + path);
          }

          if (utils.isString(domain)) {
            cookie.push('domain=' + domain);
          }

          if (secure === true) {
            cookie.push('secure');
          }

          document.cookie = cookie.join('; ');
        },

        read: function read(name) {
          var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
          return (match ? decodeURIComponent(match[3]) : null);
        },

        remove: function remove(name) {
          this.write(name, '', Date.now() - 86400000);
        }
      };
    })() :

  // Non standard browser env (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return {
        write: function write() {},
        read: function read() { return null; },
        remove: function remove() {}
      };
    })()
);


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isAbsoluteURL.js":
/*!*********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isAbsoluteURL.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isURLSameOrigin.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isURLSameOrigin.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
    (function standardBrowserEnv() {
      var msie = /(msie|trident)/i.test(navigator.userAgent);
      var urlParsingNode = document.createElement('a');
      var originURL;

      /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
      function resolveURL(url) {
        var href = url;

        if (msie) {
        // IE needs attribute set twice to normalize properties
          urlParsingNode.setAttribute('href', href);
          href = urlParsingNode.href;
        }

        urlParsingNode.setAttribute('href', href);

        // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
        return {
          href: urlParsingNode.href,
          protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
          host: urlParsingNode.host,
          search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
          hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
          hostname: urlParsingNode.hostname,
          port: urlParsingNode.port,
          pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
            urlParsingNode.pathname :
            '/' + urlParsingNode.pathname
        };
      }

      originURL = resolveURL(window.location.href);

      /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
      return function isURLSameOrigin(requestURL) {
        var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
        return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
      };
    })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return function isURLSameOrigin() {
        return true;
      };
    })()
);


/***/ }),

/***/ "./node_modules/axios/lib/helpers/normalizeHeaderName.js":
/*!***************************************************************!*\
  !*** ./node_modules/axios/lib/helpers/normalizeHeaderName.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/parseHeaders.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/parseHeaders.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/spread.js":
/*!**************************************************!*\
  !*** ./node_modules/axios/lib/helpers/spread.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ }),

/***/ "./node_modules/axios/lib/utils.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/utils.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");
var isBuffer = __webpack_require__(/*! is-buffer */ "./node_modules/is-buffer/index.js");

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 * nativescript
 *  navigator.product -> 'NativeScript' or 'NS'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' ||
                                           navigator.product === 'NativeScript' ||
                                           navigator.product === 'NS')) {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Function equal to merge with the difference being that no reference
 * to original objects is kept.
 *
 * @see merge
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function deepMerge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = deepMerge(result[key], val);
    } else if (typeof val === 'object') {
      result[key] = deepMerge({}, val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  deepMerge: deepMerge,
  extend: extend,
  trim: trim
};


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Counter.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Counter.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Counter",
  data: function data() {
    return {
      h: '',
      m: '',
      s: '',
      show_second: 0
    };
  },
  props: ['second'],
  mounted: function mounted() {
    this.show_second = this.second;
    this.counter();
    setInterval(this.counter, 1000);
  },
  methods: {
    counter: function counter() {
      var second = this.show_second;
      var h = Math.floor(second / 3600);
      second = second - h * 3600;
      var m = Math.floor(second / 60);
      var s = second - m * 60;

      if (h.toString().length == 1) {
        h = "0" + h;
      }

      if (m.toString().length == 1) {
        m = "0" + m;
      }

      if (s.toString().length == 1) {
        s = "0" + s;
      }

      this.h = this.replaceNumber(h);
      this.m = this.replaceNumber(m);
      this.s = this.replaceNumber(s);
      this.show_second = this.show_second - 1;
    },
    replaceNumber: function replaceNumber(n) {
      n = n.toString();
      var find = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
      var replace = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

      for (var i = 0; i < find.length; i++) {
        n = n.replace(new RegExp(find[i], 'g'), replace[i]);
      }

      return n;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/IncredibleOffers.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/IncredibleOffers.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "IncredibleOffers",
  data: function data() {
    return {
      WarrantyList: {
        data: []
      },
      page: 1,
      formInput: {
        price1: '',
        price2: '',
        product_number: '',
        product_number_cart: ''
      },
      options: {
        numeral: true
      },
      date1: '',
      date2: '',
      select_key: -1,
      warranty_id: -1,
      send_form: true,
      show_message_box: false,
      errors: {
        price1_error: false,
        price2_error: false,
        product_number_error: false,
        product_number_cart_error: false
      },
      label: {
        price1: 'هزینه محصول',
        price2: 'هزینه محصول برای فروش',
        product_number: 'تعداد موجودی محصول',
        product_number_cart: 'تعداد قابل سفارش در سبد خرید'
      },
      search_text: '',
      server_errors: null
    };
  },
  mounted: function mounted() {
    this.getWarrantyList(1);
  },
  methods: {
    getWarrantyList: function getWarrantyList(page) {
      var _this = this;

      this.page = page;
      var url = this.$siteUrl + '/admin/ajax/getWarranty?page=' + page + "&search_text=" + this.search_text;
      this.axios.get(url).then(function (response) {
        _this.WarrantyList = response.data;
      });
    },
    getRow: function getRow(index) {
      ++index;
      var k = (this.page - 1) * 10;
      k = k + index;
      return this.replaceNumber(k);
    },
    replaceNumber: function replaceNumber(n) {
      n = n.toString();
      var find = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
      var replace = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

      for (var i = 0; i < find.length; i++) {
        n = n.replace(new RegExp(find[i], 'g'), replace[i]);
      }

      return n;
    },
    show_box: function show_box(item_id, key) {
      if (this.send_form == true) {
        this.server_errors = false;
        this.warranty_id = item_id;
        this.select_key = key;
        this.formInput.price1 = this.WarrantyList.data[key].price1;
        this.formInput.price2 = this.WarrantyList.data[key].price2;
        this.formInput.product_number = this.WarrantyList.data[key].product_number;
        this.formInput.product_number_cart = this.WarrantyList.data[key].product_number_cart;
        this.date1 = this.WarrantyList.data[this.select_key].offers_first_date;
        this.date2 = this.WarrantyList.data[this.select_key].offers_last_date;
        $("#priceBox").modal('show');
      }
    },
    add: function add() {
      var _this2 = this;

      this.date1 = $("#pcal1").val();
      this.date2 = $("#pcal2").val();

      if (this.validateForm()) {
        this.send_form = false;
        var formData = new FormData();
        formData.append('price1', this.formInput.price1);
        formData.append('price2', this.formInput.price2);
        formData.append('product_number', this.formInput.product_number);
        formData.append('product_number_cart', this.formInput.product_number_cart);
        formData.append('date1', this.date1);
        formData.append('date2', this.date2);
        var url = this.$siteUrl + "/admin/add_incredible_offers/" + this.warranty_id;
        this.axios.post(url, formData).then(function (response) {
          if (response.data == 'ok') {
            _this2.send_form = true;
            $("#priceBox").modal('hide');
            _this2.WarrantyList.data[_this2.select_key].offers = 1;
            _this2.WarrantyList.data[_this2.select_key].price1 = _this2.formInput.price1;
            _this2.WarrantyList.data[_this2.select_key].price2 = _this2.formInput.price2;
            _this2.WarrantyList.data[_this2.select_key].product_number = _this2.formInput.product_number;
            _this2.WarrantyList.data[_this2.select_key].product_number_cart = _this2.formInput.product_number_cart;
            _this2.WarrantyList.data[_this2.select_key].offers_first_date = _this2.date1;
            _this2.WarrantyList.data[_this2.select_key].offers_last_date = _this2.date2;
          } else if (response.data.error != undefined) {
            _this2.send_form = true;
          } else {
            _this2.server_errors = response.data;
            _this2.send_form = true;
          }
        });
      }
    },
    remove_offers: function remove_offers(item_id, key) {
      this.warranty_id = item_id;
      this.select_key = key;
      this.show_message_box = true;
    },
    remove_of_list: function remove_of_list() {
      var _this3 = this;

      this.show_message_box = false;
      var url = this.$siteUrl + "/admin/remove_incredible_offers/" + this.warranty_id;
      this.axios.post(url).then(function (response) {
        if (response.data != 'error') {
          _this3.WarrantyList.data[_this3.select_key].offers = 0;
          _this3.WarrantyList.data[_this3.select_key].price1 = response.data.price1;
          _this3.WarrantyList.data[_this3.select_key].price2 = response.data.price2;
          _this3.WarrantyList.data[_this3.select_key].product_number = response.data.product_number;
          _this3.WarrantyList.data[_this3.select_key].product_number_cart = response.data.product_number_cart;
        }
      });
    },
    validateForm: function validateForm() {
      var result = true;

      for (var formInputKey in this.formInput) {
        var k = formInputKey + "_error";

        if (this.formInput[formInputKey].toString().trim().length == 0) {
          var message = this.label[formInputKey] + " نمی تواند خالی باشد";
          this.errors[k] = message;
          result = false;
        } else {
          this.errors[k] = false;
        }
      }

      return result;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/OrderStep.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/OrderStep.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "OrderStep",
  props: ['steps', 'send_status', 'order_id'],
  data: function data() {
    return {
      show_box: false,
      status: 0,
      order_status: 0,
      tozihat: ""
    };
  },
  mounted: function mounted() {
    this.order_status = this.send_status;
  },
  methods: {
    change_status: function change_status(status) {
      this.tozihat = '';
      this.status = status;
      $("#statusModal").modal('show'); //this.show_box=true;
    },
    send_data: function send_data() {
      var _this = this;

      $("#statusModal").modal('hide');
      $("#loading_box").show();
      var formData = new FormData();
      formData.append('order_id', this.order_id);
      formData.append('status', this.status);
      formData.append('tozihat', this.tozihat);
      var url = this.$siteUrl + '/admin/order/change_status';
      this.axios.post(url, formData).then(function (response) {
        $("#loading_box").hide();

        if (response.data == 'ok') {
          _this.order_status = _this.status;
        } else {
          $('.error_dialog').show();
          setTimeout(function () {
            $('.error_dialog').hide();
          }, 4000);
        }
      })["catch"](function (onerror) {
        $("#loading_box").hide();
        $('.error_dialog').show();
        setTimeout(function () {
          $('.error_dialog').hide();
        }, 4000);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/PackageContent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/PackageContent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _myMixin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../myMixin */ "./resources/js/myMixin.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_myMixin__WEBPACK_IMPORTED_MODULE_0__["default"]],
  data: function data() {
    return {
      title: "",
      list: [],
      send: true,
      show_loading: false,
      tozihat: "",
      package_status: 0,
      addProduct: false
    };
  },
  props: ["package_id", "stockroom_id", "seller_tozihat", "status", "stockroom"],
  mounted: function mounted() {
    this.package_status = this.status;
    this.getList();
  },
  methods: {
    getList: function getList() {
      var _this = this;

      this.show_loading = true;
      var url = this.$siteUrl + "/admin/package/getContent/" + this.package_id;
      this.axios.get(url).then(function (response) {
        _this.show_loading = false;
        _this.list = response.data;
      })["catch"](function (error) {
        _this.show_loading = false;
        $("#server_error_box").show();
        setTimeout(function () {
          $("#server_error_box").hide();
        }, 5000);
      });
    },
    getRow: function getRow(key) {
      key = key + 1;
      return this.replaceNumber(key);
    },
    send_data: function send_data() {
      var _this2 = this;

      if (this.send) {
        this.send = false;
        var string = "";
        this.list.forEach(function (row) {
          if (parseInt(row.product_count) > 0) {
            string = string + "@" + row.warranty_id + "_" + row.product_count;
          }
        });
        var url = this.$siteUrl + "/admin/package/stockroom/add_product";
        var formData = new FormData();
        formData.append("list", string);
        formData.append("stockroom_id", this.stockroom_id);
        formData.append("addProduct", this.addProduct);
        formData.append("package_id", this.package_id);
        formData.append("tozihat", this.tozihat);
        formData.append("status", this.package_status);
        this.axios.post(url, formData).then(function (response) {
          _this2.show_loading = false;

          if (response.data == "ok") {
            window.location = _this2.$siteUrl + "/admin/stockroom/input";
          } else if (response.data == "add") {
            window.location = _this2.$siteUrl + "/admin/packages";
          } else {
            _this2.send = true;
            $("#server_error_box").show();
            setTimeout(function () {
              $("#server_error_box").hide();
            }, 5000);
          }
        })["catch"](function (error) {
          _this2.send = true;
          _this2.show_loading = false;
          $("#server_error_box").show();
          setTimeout(function () {
            $("#server_error_box").hide();
          }, 5000);
        });
      }
    }
  },
  computed: {
    getProductList: function getProductList() {
      var _this3 = this;

      return this.list.filter(function (item) {
        return item.get_product_warranty.get_product.title.indexOf(_this3.title) > -1;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/SaleReport.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/SaleReport.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var highcharts_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! highcharts-vue */ "./node_modules/highcharts-vue/dist/highcharts-vue.min.js");
/* harmony import */ var highcharts_vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(highcharts_vue__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['product_id'],
  data: function data() {
    return {
      default_year: '',
      years: [],
      chartOptions: {
        series: [{
          data: [],
          color: 'rgb(244,81,108)',
          dataLabels: {
            enabled: true,
            color: '#FFFFFF',
            formatter: function formatter() {
              return replaceNumber(number_format(this.y));
            },
            rotation: -90,
            style: {
              fontSize: '16px'
            },
            align: 'right',
            y: 10
          }
        }],
        legend: {
          enabled: false
        },
        title: {
          text: '',
          update: true
        },
        xAxis: {
          type: 'category'
        },
        chart: {
          type: 'column',
          style: {
            fontFamily: 'IRANSansWeb'
          }
        },
        yAxis: {
          title: {
            text: ''
          },
          labels: {
            useHTML: true,
            formatter: function formatter() {
              var value = this.value;

              if (value > 1000) {
                value = number_format(value);
              }

              value = replaceNumber(value);
              return '<div style="direction:rtl">' + '<span>' + value + ' تومان</span>' + '</div>';
            },
            style: {
              fontSize: '15px'
            }
          }
        },
        tooltip: {
          useHTML: true,
          formatter: function formatter() {
            return '<div style="width:210px;text-align:right">' + '<p><span> میزان فروش در</span> ' + getMonthName(this.x + 1) + ' ماه</p>' + '<div style="text-align:left;color:rgb(244,81,108);direction:rtl">' + '<span>' + replaceNumber(number_format(this.y)) + '</span>' + '<span> تومان</span>' + '</div>' + '</div>';
          },
          style: {
            fontSize: '16px'
          }
        }
      },
      chartOptions2: {
        series: [{
          data: [],
          dataLabels: {
            enabled: true,
            color: '#FFFFFF',
            formatter: function formatter() {
              return replaceNumber(number_format(this.y));
            },
            rotation: -90,
            style: {
              fontSize: '16px'
            },
            align: 'right',
            y: 10
          }
        }],
        legend: {
          enabled: false
        },
        title: {
          text: 'کمیسیون دریافت شده از فروش محصول',
          update: true
        },
        xAxis: {
          type: 'category'
        },
        chart: {
          type: 'column',
          style: {
            fontFamily: 'IRANSansWeb'
          }
        },
        yAxis: {
          title: {
            text: ''
          },
          labels: {
            useHTML: true,
            formatter: function formatter() {
              var value = this.value;

              if (value > 1000) {
                value = number_format(value);
              }

              value = replaceNumber(value);
              return '<div style="direction:rtl">' + '<span>' + value + ' تومان</span>' + '</div>';
            },
            style: {
              fontSize: '15px'
            }
          }
        },
        tooltip: {
          useHTML: true,
          formatter: function formatter() {
            return '<div style="width:250px;text-align:right">' + '<p><span> میزان دریافت کمیسیون در</span> ' + getMonthName(this.x + 1) + ' ماه</p>' + '<div style="text-align:left;color:rgb(244,81,108);direction:rtl">' + '<span>' + replaceNumber(number_format(this.y)) + '</span>' + '<span> تومان</span>' + '</div>' + '</div>';
          },
          style: {
            fontSize: '16px'
          }
        }
      },
      month: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
      url: ''
    };
  },
  components: {
    highcharts: highcharts_vue__WEBPACK_IMPORTED_MODULE_0__["Chart"]
  },
  mounted: function mounted() {
    this.getData();
  },
  methods: {
    getData: function getData() {
      var _this = this;

      if (this.product_id == undefined) {
        this.url = this.$siteUrl + '/admin/shop/get_sale_report?default_year=' + this.default_year;
      } else {
        this.url = this.$siteUrl + '/admin/product/get_sale_report?default_year=' + this.default_year + '&product_id=' + this.product_id;
      }

      $("#loading_box").show();
      var app = this;
      this.axios.get(this.url).then(function (response) {
        $("#loading_box").hide();
        _this.chartOptions['series'][0]['data'] = [];
        var sale = response.data.sale;

        if (sale != undefined) {
          sale.forEach(function (row, key) {
            if (key != 0) {
              app.chartOptions['series'][0]['data'].push([app.getMonthName(key), row]);
            }
          });
        }

        var commision = response.data.commision;

        if (commision != undefined) {
          commision.forEach(function (row, key) {
            if (key != 0) {
              app.chartOptions2['series'][0]['data'].push([app.getMonthName(key), row]);
            }
          });
        }

        _this.years = response.data.year_list;
        _this.default_year = response.data.default_year;

        _this.$nextTick(function () {
          $(this.$refs.default_year).selectpicker('refresh');
        });
      })["catch"](function (error) {
        $("#loading_box").hide();
      });
    },
    getMonthName: function getMonthName(key) {
      key = key - 1;

      if (this.month[key] != undefined) {
        return this.month[key];
      } else {
        return '';
      }
    },
    replaceNumber: function replaceNumber(n) {
      if (n != undefined) {
        n = n.toString();
        var find = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        var replace = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

        for (var i = 0; i < find.length; i++) {
          n = n.replace(new RegExp(find[i], 'g'), replace[i]);
        }

        return n;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StockroomOutputList.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StockroomOutputList.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _myMixin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../myMixin */ "./resources/js/myMixin.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StockroomOutputList",
  props: ['stockroom'],
  mixins: [_myMixin__WEBPACK_IMPORTED_MODULE_0__["default"]],
  data: function data() {
    return {
      stockroom_id: 0,
      tozihat: '',
      ProductList: {
        data: []
      },
      page: 1,
      product_count: [],
      selected_product: [],
      show_messag_box: false,
      select_id: 0,
      select_key: 0,
      msg: 'ایا از خروج این محصول از انبار مطمئن هستین؟',
      get_data: false,
      search_text: '',
      errors: [],
      show_loading: false
    };
  },
  methods: {
    getList: function getList() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.page = page;
      this.get_data = true;
      var url = this.$siteUrl + "/admin/stockroom/getInventory?page=" + page + '&search_text=' + this.search_text + "&stockroom_id=" + this.stockroom_id;
      this.axios.get(url).then(function (response) {
        for (var i = 0; i < response.data.data.length; i++) {
          _this.product_count[i] = response.data.data[i].product_count;
        }

        _this.ProductList = response.data;
        _this.get_data = false;
      })["catch"](function (error) {
        _this.get_data = false;
      });
    },
    getRow: function getRow(key) {
      ++key;
      var k = (this.page - 1) * 5;
      k = k + key;
      return this.replaceNumber(k);
    },
    checkInList: function checkInList(id) {
      var result = false;
      this.selected_product.forEach(function (row) {
        if (row.id == id) {
          result = true;
        }
      });
      return result;
    },
    add_product: function add_product(id, key) {
      this.show_messag_box = true;
      this.select_id = id;
      this.select_key = key;
    },
    add_product_to_stockroom: function add_product_to_stockroom() {
      this.show_messag_box = false;
      var n = this.product_count[this.select_key];
      var maxCount = this.ProductList.data[this.select_key].product_count;

      if (parseInt(n) > 0) {
        var count = n <= maxCount ? n : maxCount;
        var item = this.ProductList.data[this.select_key];
        item.product_number = count;
        this.selected_product.push(item);
      }
    },
    removeOfList: function removeOfList(key) {
      this.$delete(this.selected_product, key);
    },
    send_data: function send_data() {
      var _this2 = this;

      this.errors = [];
      var send = true;

      if (this.stockroom_id == 0) {
        send = false;
        this.errors.push('لطفا انبار ارسال محصول را انتخاب نمایید');
      }

      if (this.selected_product.length == 0) {
        send = false;
        this.errors.push('لطفا محصولاتی که باید به انبار اضافه شود را انتخاب نمایید');
      }

      if (send) {
        this.show_loading = true;
        var string = '';
        this.selected_product.forEach(function (row) {
          string = string + "@" + row.get_product_warranty.id + "_" + row.product_number;
        });
        var url = this.$siteUrl + "/admin/stockroom/add_product";
        var formData = new FormData();
        formData.append('list', string);
        formData.append('stockroom_id', this.stockroom_id);
        formData.append('tozihat', this.tozihat);
        formData.append('type', "output");
        this.axios.post(url, formData).then(function (response) {
          _this2.show_loading = false;

          if (response.data == 'ok') {
            window.location = _this2.$siteUrl + "/admin/stockroom/output";
          } else {
            $("#server_error_box").show();
            setTimeout(function () {
              $("#server_error_box").hide();
            }, 5000);
          }
        })["catch"](function (error) {
          _this2.show_loading = false;
          $("#server_error_box").show();
          setTimeout(function () {
            $("#server_error_box").hide();
          }, 5000);
        });
      }
    }
  },
  watch: {
    stockroom_id: function stockroom_id() {
      this.getList();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StockroomProductList.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StockroomProductList.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _myMixin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../myMixin */ "./resources/js/myMixin.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StockroomProductList",
  props: ['stockroom'],
  mixins: [_myMixin__WEBPACK_IMPORTED_MODULE_0__["default"]],
  data: function data() {
    return {
      stockroom_id: 0,
      tozihat: '',
      ProductList: {
        data: []
      },
      page: 1,
      product_count: [],
      selected_product: [],
      show_messag_box: false,
      select_id: 0,
      select_key: 0,
      msg: 'ایا از افزودن این محصول به انبار مطمئن هستین؟',
      get_data: false,
      search_text: '',
      errors: [],
      show_loading: false
    };
  },
  mounted: function mounted() {
    this.getProductWarrnty();
  },
  methods: {
    getProductWarrnty: function getProductWarrnty() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.page = page;
      this.get_data = true;
      var url = this.$siteUrl + "/admin/stockroom/getProductWarrnty?page=" + page + '&search_text=' + this.search_text;
      this.axios.get(url).then(function (response) {
        for (var i = 0; i < response.data.data.length; i++) {
          _this.product_count[i] = response.data.data[i].product_number;
        }

        _this.ProductList = response.data;
        _this.get_data = false;
      })["catch"](function (error) {
        _this.get_data = false;
      });
    },
    getRow: function getRow(key) {
      ++key;
      var k = (this.page - 1) * 5;
      k = k + key;
      return this.replaceNumber(k);
    },
    checkInList: function checkInList(id) {
      var result = false;
      this.selected_product.forEach(function (row) {
        if (row.id == id) {
          result = true;
        }
      });
      return result;
    },
    add_product: function add_product(id, key) {
      this.show_messag_box = true;
      this.select_id = id;
      this.select_key = key;
    },
    add_product_to_stockroom: function add_product_to_stockroom() {
      this.show_messag_box = false;
      var n = this.product_count[this.select_key];

      if (parseInt(n) > 0) {
        var item = this.ProductList.data[this.select_key];
        item.product_number = n;
        this.selected_product.push(item);
      }
    },
    removeOfList: function removeOfList(key) {
      this.$delete(this.selected_product, key);
    },
    send_data: function send_data() {
      var _this2 = this;

      this.errors = [];
      var send = true;

      if (this.stockroom_id == 0) {
        send = false;
        this.errors.push('لطفا انبار ارسال محصول را انتخاب نمایید');
      }

      if (this.selected_product.length == 0) {
        send = false;
        this.errors.push('لطفا محصولاتی که باید به انبار اضافه شود را انتخاب نمایید');
      }

      if (send) {
        this.show_loading = true;
        var string = '';
        this.selected_product.forEach(function (row) {
          string = string + "@" + row.id + "_" + row.product_number;
        });
        var url = this.$siteUrl + "/admin/stockroom/add_product";
        var formData = new FormData();
        formData.append('list', string);
        formData.append('stockroom_id', this.stockroom_id);
        formData.append('tozihat', this.tozihat);
        this.axios.post(url, formData).then(function (response) {
          _this2.show_loading = false;

          if (response.data == 'ok') {
            window.location = _this2.$siteUrl + "/admin/stockroom/input";
          } else {
            $("#server_error_box").show();
            setTimeout(function () {
              $("#server_error_box").hide();
            }, 5000);
          }
        })["catch"](function (error) {
          _this2.show_loading = false;
          $("#server_error_box").show();
          setTimeout(function () {
            $("#server_error_box").hide();
          }, 5000);
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/cleave.js/dist/cleave-esm.js":
/*!***************************************************!*\
  !*** ./node_modules/cleave.js/dist/cleave-esm.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

var NumeralFormatter = function (numeralDecimalMark,
                                 numeralIntegerScale,
                                 numeralDecimalScale,
                                 numeralThousandsGroupStyle,
                                 numeralPositiveOnly,
                                 stripLeadingZeroes,
                                 prefix,
                                 signBeforePrefix,
                                 delimiter) {
    var owner = this;

    owner.numeralDecimalMark = numeralDecimalMark || '.';
    owner.numeralIntegerScale = numeralIntegerScale > 0 ? numeralIntegerScale : 0;
    owner.numeralDecimalScale = numeralDecimalScale >= 0 ? numeralDecimalScale : 2;
    owner.numeralThousandsGroupStyle = numeralThousandsGroupStyle || NumeralFormatter.groupStyle.thousand;
    owner.numeralPositiveOnly = !!numeralPositiveOnly;
    owner.stripLeadingZeroes = stripLeadingZeroes !== false;
    owner.prefix = (prefix || prefix === '') ? prefix : '';
    owner.signBeforePrefix = !!signBeforePrefix;
    owner.delimiter = (delimiter || delimiter === '') ? delimiter : ',';
    owner.delimiterRE = delimiter ? new RegExp('\\' + delimiter, 'g') : '';
};

NumeralFormatter.groupStyle = {
    thousand: 'thousand',
    lakh:     'lakh',
    wan:      'wan',
    none:     'none'    
};

NumeralFormatter.prototype = {
    getRawValue: function (value) {
        return value.replace(this.delimiterRE, '').replace(this.numeralDecimalMark, '.');
    },

    format: function (value) {
        var owner = this, parts, partSign, partSignAndPrefix, partInteger, partDecimal = '';

        // strip alphabet letters
        value = value.replace(/[A-Za-z]/g, '')
            // replace the first decimal mark with reserved placeholder
            .replace(owner.numeralDecimalMark, 'M')

            // strip non numeric letters except minus and "M"
            // this is to ensure prefix has been stripped
            .replace(/[^\dM-]/g, '')

            // replace the leading minus with reserved placeholder
            .replace(/^\-/, 'N')

            // strip the other minus sign (if present)
            .replace(/\-/g, '')

            // replace the minus sign (if present)
            .replace('N', owner.numeralPositiveOnly ? '' : '-')

            // replace decimal mark
            .replace('M', owner.numeralDecimalMark);

        // strip any leading zeros
        if (owner.stripLeadingZeroes) {
            value = value.replace(/^(-)?0+(?=\d)/, '$1');
        }

        partSign = value.slice(0, 1) === '-' ? '-' : '';
        if (typeof owner.prefix != 'undefined') {
            if (owner.signBeforePrefix) {
                partSignAndPrefix = partSign + owner.prefix;
            } else {
                partSignAndPrefix = owner.prefix + partSign;
            }
        } else {
            partSignAndPrefix = partSign;
        }
        
        partInteger = value;

        if (value.indexOf(owner.numeralDecimalMark) >= 0) {
            parts = value.split(owner.numeralDecimalMark);
            partInteger = parts[0];
            partDecimal = owner.numeralDecimalMark + parts[1].slice(0, owner.numeralDecimalScale);
        }

        if(partSign === '-') {
            partInteger = partInteger.slice(1);
        }

        if (owner.numeralIntegerScale > 0) {
          partInteger = partInteger.slice(0, owner.numeralIntegerScale);
        }

        switch (owner.numeralThousandsGroupStyle) {
        case NumeralFormatter.groupStyle.lakh:
            partInteger = partInteger.replace(/(\d)(?=(\d\d)+\d$)/g, '$1' + owner.delimiter);

            break;

        case NumeralFormatter.groupStyle.wan:
            partInteger = partInteger.replace(/(\d)(?=(\d{4})+$)/g, '$1' + owner.delimiter);

            break;

        case NumeralFormatter.groupStyle.thousand:
            partInteger = partInteger.replace(/(\d)(?=(\d{3})+$)/g, '$1' + owner.delimiter);

            break;
        }

        return partSignAndPrefix + partInteger.toString() + (owner.numeralDecimalScale > 0 ? partDecimal.toString() : '');
    }
};

var NumeralFormatter_1 = NumeralFormatter;

var DateFormatter = function (datePattern, dateMin, dateMax) {
    var owner = this;

    owner.date = [];
    owner.blocks = [];
    owner.datePattern = datePattern;
    owner.dateMin = dateMin
      .split('-')
      .reverse()
      .map(function(x) {
        return parseInt(x, 10);
      });
    if (owner.dateMin.length === 2) owner.dateMin.unshift(0);

    owner.dateMax = dateMax
      .split('-')
      .reverse()
      .map(function(x) {
        return parseInt(x, 10);
      });
    if (owner.dateMax.length === 2) owner.dateMax.unshift(0);
    
    owner.initBlocks();
};

DateFormatter.prototype = {
    initBlocks: function () {
        var owner = this;
        owner.datePattern.forEach(function (value) {
            if (value === 'Y') {
                owner.blocks.push(4);
            } else {
                owner.blocks.push(2);
            }
        });
    },

    getISOFormatDate: function () {
        var owner = this,
            date = owner.date;

        return date[2] ? (
            date[2] + '-' + owner.addLeadingZero(date[1]) + '-' + owner.addLeadingZero(date[0])
        ) : '';
    },

    getBlocks: function () {
        return this.blocks;
    },

    getValidatedDate: function (value) {
        var owner = this, result = '';

        value = value.replace(/[^\d]/g, '');

        owner.blocks.forEach(function (length, index) {
            if (value.length > 0) {
                var sub = value.slice(0, length),
                    sub0 = sub.slice(0, 1),
                    rest = value.slice(length);

                switch (owner.datePattern[index]) {
                case 'd':
                    if (sub === '00') {
                        sub = '01';
                    } else if (parseInt(sub0, 10) > 3) {
                        sub = '0' + sub0;
                    } else if (parseInt(sub, 10) > 31) {
                        sub = '31';
                    }

                    break;

                case 'm':
                    if (sub === '00') {
                        sub = '01';
                    } else if (parseInt(sub0, 10) > 1) {
                        sub = '0' + sub0;
                    } else if (parseInt(sub, 10) > 12) {
                        sub = '12';
                    }

                    break;
                }

                result += sub;

                // update remaining string
                value = rest;
            }
        });

        return this.getFixedDateString(result);
    },

    getFixedDateString: function (value) {
        var owner = this, datePattern = owner.datePattern, date = [],
            dayIndex = 0, monthIndex = 0, yearIndex = 0,
            dayStartIndex = 0, monthStartIndex = 0, yearStartIndex = 0,
            day, month, year, fullYearDone = false;

        // mm-dd || dd-mm
        if (value.length === 4 && datePattern[0].toLowerCase() !== 'y' && datePattern[1].toLowerCase() !== 'y') {
            dayStartIndex = datePattern[0] === 'd' ? 0 : 2;
            monthStartIndex = 2 - dayStartIndex;
            day = parseInt(value.slice(dayStartIndex, dayStartIndex + 2), 10);
            month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);

            date = this.getFixedDate(day, month, 0);
        }

        // yyyy-mm-dd || yyyy-dd-mm || mm-dd-yyyy || dd-mm-yyyy || dd-yyyy-mm || mm-yyyy-dd
        if (value.length === 8) {
            datePattern.forEach(function (type, index) {
                switch (type) {
                case 'd':
                    dayIndex = index;
                    break;
                case 'm':
                    monthIndex = index;
                    break;
                default:
                    yearIndex = index;
                    break;
                }
            });

            yearStartIndex = yearIndex * 2;
            dayStartIndex = (dayIndex <= yearIndex) ? dayIndex * 2 : (dayIndex * 2 + 2);
            monthStartIndex = (monthIndex <= yearIndex) ? monthIndex * 2 : (monthIndex * 2 + 2);

            day = parseInt(value.slice(dayStartIndex, dayStartIndex + 2), 10);
            month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
            year = parseInt(value.slice(yearStartIndex, yearStartIndex + 4), 10);

            fullYearDone = value.slice(yearStartIndex, yearStartIndex + 4).length === 4;

            date = this.getFixedDate(day, month, year);
        }

        // mm-yy || yy-mm
        if (value.length === 4 && (datePattern[0] === 'y' || datePattern[1] === 'y')) {
            monthStartIndex = datePattern[0] === 'm' ? 0 : 2;
            yearStartIndex = 2 - monthStartIndex;
            month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
            year = parseInt(value.slice(yearStartIndex, yearStartIndex + 2), 10);

            fullYearDone = value.slice(yearStartIndex, yearStartIndex + 2).length === 2;

            date = [0, month, year];
        }

        // mm-yyyy || yyyy-mm
        if (value.length === 6 && (datePattern[0] === 'Y' || datePattern[1] === 'Y')) {
            monthStartIndex = datePattern[0] === 'm' ? 0 : 4;
            yearStartIndex = 2 - 0.5 * monthStartIndex;
            month = parseInt(value.slice(monthStartIndex, monthStartIndex + 2), 10);
            year = parseInt(value.slice(yearStartIndex, yearStartIndex + 4), 10);

            fullYearDone = value.slice(yearStartIndex, yearStartIndex + 4).length === 4;

            date = [0, month, year];
        }

        date = owner.getRangeFixedDate(date);
        owner.date = date;

        var result = date.length === 0 ? value : datePattern.reduce(function (previous, current) {
            switch (current) {
            case 'd':
                return previous + (date[0] === 0 ? '' : owner.addLeadingZero(date[0]));
            case 'm':
                return previous + (date[1] === 0 ? '' : owner.addLeadingZero(date[1]));
            case 'y':
                return previous + (fullYearDone ? owner.addLeadingZeroForYear(date[2], false) : '');
            case 'Y':
                return previous + (fullYearDone ? owner.addLeadingZeroForYear(date[2], true) : '');
            }
        }, '');

        return result;
    },

    getRangeFixedDate: function (date) {
        var owner = this,
            datePattern = owner.datePattern,
            dateMin = owner.dateMin || [],
            dateMax = owner.dateMax || [];

        if (!date.length || (dateMin.length < 3 && dateMax.length < 3)) return date;

        if (
          datePattern.find(function(x) {
            return x.toLowerCase() === 'y';
          }) &&
          date[2] === 0
        ) return date;

        if (dateMax.length && (dateMax[2] < date[2] || (
          dateMax[2] === date[2] && (dateMax[1] < date[1] || (
            dateMax[1] === date[1] && dateMax[0] < date[0]
          ))
        ))) return dateMax;

        if (dateMin.length && (dateMin[2] > date[2] || (
          dateMin[2] === date[2] && (dateMin[1] > date[1] || (
            dateMin[1] === date[1] && dateMin[0] > date[0]
          ))
        ))) return dateMin;

        return date;
    },

    getFixedDate: function (day, month, year) {
        day = Math.min(day, 31);
        month = Math.min(month, 12);
        year = parseInt((year || 0), 10);

        if ((month < 7 && month % 2 === 0) || (month > 8 && month % 2 === 1)) {
            day = Math.min(day, month === 2 ? (this.isLeapYear(year) ? 29 : 28) : 30);
        }

        return [day, month, year];
    },

    isLeapYear: function (year) {
        return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
    },

    addLeadingZero: function (number) {
        return (number < 10 ? '0' : '') + number;
    },

    addLeadingZeroForYear: function (number, fullYearMode) {
        if (fullYearMode) {
            return (number < 10 ? '000' : (number < 100 ? '00' : (number < 1000 ? '0' : ''))) + number;
        }

        return (number < 10 ? '0' : '') + number;
    }
};

var DateFormatter_1 = DateFormatter;

var TimeFormatter = function (timePattern, timeFormat) {
    var owner = this;

    owner.time = [];
    owner.blocks = [];
    owner.timePattern = timePattern;
    owner.timeFormat = timeFormat;
    owner.initBlocks();
};

TimeFormatter.prototype = {
    initBlocks: function () {
        var owner = this;
        owner.timePattern.forEach(function () {
            owner.blocks.push(2);
        });
    },

    getISOFormatTime: function () {
        var owner = this,
            time = owner.time;

        return time[2] ? (
            owner.addLeadingZero(time[0]) + ':' + owner.addLeadingZero(time[1]) + ':' + owner.addLeadingZero(time[2])
        ) : '';
    },

    getBlocks: function () {
        return this.blocks;
    },

    getTimeFormatOptions: function () {
        var owner = this;
        if (String(owner.timeFormat) === '12') {
            return {
                maxHourFirstDigit: 1,
                maxHours: 12,
                maxMinutesFirstDigit: 5,
                maxMinutes: 60
            };
        }

        return {
            maxHourFirstDigit: 2,
            maxHours: 23,
            maxMinutesFirstDigit: 5,
            maxMinutes: 60
        };
    },

    getValidatedTime: function (value) {
        var owner = this, result = '';

        value = value.replace(/[^\d]/g, '');

        var timeFormatOptions = owner.getTimeFormatOptions();

        owner.blocks.forEach(function (length, index) {
            if (value.length > 0) {
                var sub = value.slice(0, length),
                    sub0 = sub.slice(0, 1),
                    rest = value.slice(length);

                switch (owner.timePattern[index]) {

                case 'h':
                    if (parseInt(sub0, 10) > timeFormatOptions.maxHourFirstDigit) {
                        sub = '0' + sub0;
                    } else if (parseInt(sub, 10) > timeFormatOptions.maxHours) {
                        sub = timeFormatOptions.maxHours + '';
                    }

                    break;

                case 'm':
                case 's':
                    if (parseInt(sub0, 10) > timeFormatOptions.maxMinutesFirstDigit) {
                        sub = '0' + sub0;
                    } else if (parseInt(sub, 10) > timeFormatOptions.maxMinutes) {
                        sub = timeFormatOptions.maxMinutes + '';
                    }
                    break;
                }

                result += sub;

                // update remaining string
                value = rest;
            }
        });

        return this.getFixedTimeString(result);
    },

    getFixedTimeString: function (value) {
        var owner = this, timePattern = owner.timePattern, time = [],
            secondIndex = 0, minuteIndex = 0, hourIndex = 0,
            secondStartIndex = 0, minuteStartIndex = 0, hourStartIndex = 0,
            second, minute, hour;

        if (value.length === 6) {
            timePattern.forEach(function (type, index) {
                switch (type) {
                case 's':
                    secondIndex = index * 2;
                    break;
                case 'm':
                    minuteIndex = index * 2;
                    break;
                case 'h':
                    hourIndex = index * 2;
                    break;
                }
            });

            hourStartIndex = hourIndex;
            minuteStartIndex = minuteIndex;
            secondStartIndex = secondIndex;

            second = parseInt(value.slice(secondStartIndex, secondStartIndex + 2), 10);
            minute = parseInt(value.slice(minuteStartIndex, minuteStartIndex + 2), 10);
            hour = parseInt(value.slice(hourStartIndex, hourStartIndex + 2), 10);

            time = this.getFixedTime(hour, minute, second);
        }

        if (value.length === 4 && owner.timePattern.indexOf('s') < 0) {
            timePattern.forEach(function (type, index) {
                switch (type) {
                case 'm':
                    minuteIndex = index * 2;
                    break;
                case 'h':
                    hourIndex = index * 2;
                    break;
                }
            });

            hourStartIndex = hourIndex;
            minuteStartIndex = minuteIndex;

            second = 0;
            minute = parseInt(value.slice(minuteStartIndex, minuteStartIndex + 2), 10);
            hour = parseInt(value.slice(hourStartIndex, hourStartIndex + 2), 10);

            time = this.getFixedTime(hour, minute, second);
        }

        owner.time = time;

        return time.length === 0 ? value : timePattern.reduce(function (previous, current) {
            switch (current) {
            case 's':
                return previous + owner.addLeadingZero(time[2]);
            case 'm':
                return previous + owner.addLeadingZero(time[1]);
            case 'h':
                return previous + owner.addLeadingZero(time[0]);
            }
        }, '');
    },

    getFixedTime: function (hour, minute, second) {
        second = Math.min(parseInt(second || 0, 10), 60);
        minute = Math.min(minute, 60);
        hour = Math.min(hour, 60);

        return [hour, minute, second];
    },

    addLeadingZero: function (number) {
        return (number < 10 ? '0' : '') + number;
    }
};

var TimeFormatter_1 = TimeFormatter;

var PhoneFormatter = function (formatter, delimiter) {
    var owner = this;

    owner.delimiter = (delimiter || delimiter === '') ? delimiter : ' ';
    owner.delimiterRE = delimiter ? new RegExp('\\' + delimiter, 'g') : '';

    owner.formatter = formatter;
};

PhoneFormatter.prototype = {
    setFormatter: function (formatter) {
        this.formatter = formatter;
    },

    format: function (phoneNumber) {
        var owner = this;

        owner.formatter.clear();

        // only keep number and +
        phoneNumber = phoneNumber.replace(/[^\d+]/g, '');

        // strip non-leading +
        phoneNumber = phoneNumber.replace(/^\+/, 'B').replace(/\+/g, '').replace('B', '+');

        // strip delimiter
        phoneNumber = phoneNumber.replace(owner.delimiterRE, '');

        var result = '', current, validated = false;

        for (var i = 0, iMax = phoneNumber.length; i < iMax; i++) {
            current = owner.formatter.inputDigit(phoneNumber.charAt(i));

            // has ()- or space inside
            if (/[\s()-]/g.test(current)) {
                result = current;

                validated = true;
            } else {
                if (!validated) {
                    result = current;
                }
                // else: over length input
                // it turns to invalid number again
            }
        }

        // strip ()
        // e.g. US: 7161234567 returns (716) 123-4567
        result = result.replace(/[()]/g, '');
        // replace library delimiter with user customized delimiter
        result = result.replace(/[\s-]/g, owner.delimiter);

        return result;
    }
};

var PhoneFormatter_1 = PhoneFormatter;

var CreditCardDetector = {
    blocks: {
        uatp:          [4, 5, 6],
        amex:          [4, 6, 5],
        diners:        [4, 6, 4],
        discover:      [4, 4, 4, 4],
        mastercard:    [4, 4, 4, 4],
        dankort:       [4, 4, 4, 4],
        instapayment:  [4, 4, 4, 4],
        jcb15:         [4, 6, 5],
        jcb:           [4, 4, 4, 4],
        maestro:       [4, 4, 4, 4],
        visa:          [4, 4, 4, 4],
        mir:           [4, 4, 4, 4],
        unionPay:      [4, 4, 4, 4],
        general:       [4, 4, 4, 4]
    },

    re: {
        // starts with 1; 15 digits, not starts with 1800 (jcb card)
        uatp: /^(?!1800)1\d{0,14}/,

        // starts with 34/37; 15 digits
        amex: /^3[47]\d{0,13}/,

        // starts with 6011/65/644-649; 16 digits
        discover: /^(?:6011|65\d{0,2}|64[4-9]\d?)\d{0,12}/,

        // starts with 300-305/309 or 36/38/39; 14 digits
        diners: /^3(?:0([0-5]|9)|[689]\d?)\d{0,11}/,

        // starts with 51-55/2221–2720; 16 digits
        mastercard: /^(5[1-5]\d{0,2}|22[2-9]\d{0,1}|2[3-7]\d{0,2})\d{0,12}/,

        // starts with 5019/4175/4571; 16 digits
        dankort: /^(5019|4175|4571)\d{0,12}/,

        // starts with 637-639; 16 digits
        instapayment: /^63[7-9]\d{0,13}/,

        // starts with 2131/1800; 15 digits
        jcb15: /^(?:2131|1800)\d{0,11}/,

        // starts with 2131/1800/35; 16 digits
        jcb: /^(?:35\d{0,2})\d{0,12}/,

        // starts with 50/56-58/6304/67; 16 digits
        maestro: /^(?:5[0678]\d{0,2}|6304|67\d{0,2})\d{0,12}/,

        // starts with 22; 16 digits
        mir: /^220[0-4]\d{0,12}/,

        // starts with 4; 16 digits
        visa: /^4\d{0,15}/,

        // starts with 62; 16 digits
        unionPay: /^62\d{0,14}/
    },

    getStrictBlocks: function (block) {
      var total = block.reduce(function (prev, current) {
        return prev + current;
      }, 0);

      return block.concat(19 - total);
    },

    getInfo: function (value, strictMode) {
        var blocks = CreditCardDetector.blocks,
            re = CreditCardDetector.re;

        // Some credit card can have up to 19 digits number.
        // Set strictMode to true will remove the 16 max-length restrain,
        // however, I never found any website validate card number like
        // this, hence probably you don't want to enable this option.
        strictMode = !!strictMode;

        for (var key in re) {
            if (re[key].test(value)) {
                var matchedBlocks = blocks[key];
                return {
                    type: key,
                    blocks: strictMode ? this.getStrictBlocks(matchedBlocks) : matchedBlocks
                };
            }
        }

        return {
            type: 'unknown',
            blocks: strictMode ? this.getStrictBlocks(blocks.general) : blocks.general
        };
    }
};

var CreditCardDetector_1 = CreditCardDetector;

var Util = {
    noop: function () {
    },

    strip: function (value, re) {
        return value.replace(re, '');
    },

    getPostDelimiter: function (value, delimiter, delimiters) {
        // single delimiter
        if (delimiters.length === 0) {
            return value.slice(-delimiter.length) === delimiter ? delimiter : '';
        }

        // multiple delimiters
        var matchedDelimiter = '';
        delimiters.forEach(function (current) {
            if (value.slice(-current.length) === current) {
                matchedDelimiter = current;
            }
        });

        return matchedDelimiter;
    },

    getDelimiterREByDelimiter: function (delimiter) {
        return new RegExp(delimiter.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1'), 'g');
    },

    getNextCursorPosition: function (prevPos, oldValue, newValue, delimiter, delimiters) {
      // If cursor was at the end of value, just place it back.
      // Because new value could contain additional chars.
      if (oldValue.length === prevPos) {
          return newValue.length;
      }

      return prevPos + this.getPositionOffset(prevPos, oldValue, newValue, delimiter ,delimiters);
    },

    getPositionOffset: function (prevPos, oldValue, newValue, delimiter, delimiters) {
        var oldRawValue, newRawValue, lengthOffset;

        oldRawValue = this.stripDelimiters(oldValue.slice(0, prevPos), delimiter, delimiters);
        newRawValue = this.stripDelimiters(newValue.slice(0, prevPos), delimiter, delimiters);
        lengthOffset = oldRawValue.length - newRawValue.length;

        return (lengthOffset !== 0) ? (lengthOffset / Math.abs(lengthOffset)) : 0;
    },

    stripDelimiters: function (value, delimiter, delimiters) {
        var owner = this;

        // single delimiter
        if (delimiters.length === 0) {
            var delimiterRE = delimiter ? owner.getDelimiterREByDelimiter(delimiter) : '';

            return value.replace(delimiterRE, '');
        }

        // multiple delimiters
        delimiters.forEach(function (current) {
            current.split('').forEach(function (letter) {
                value = value.replace(owner.getDelimiterREByDelimiter(letter), '');
            });
        });

        return value;
    },

    headStr: function (str, length) {
        return str.slice(0, length);
    },

    getMaxLength: function (blocks) {
        return blocks.reduce(function (previous, current) {
            return previous + current;
        }, 0);
    },

    // strip prefix
    // Before type  |   After type    |     Return value
    // PEFIX-...    |   PEFIX-...     |     ''
    // PREFIX-123   |   PEFIX-123     |     123
    // PREFIX-123   |   PREFIX-23     |     23
    // PREFIX-123   |   PREFIX-1234   |     1234
    getPrefixStrippedValue: function (value, prefix, prefixLength, prevResult, delimiter, delimiters, noImmediatePrefix) {
        // No prefix
        if (prefixLength === 0) {
          return value;
        }

        // Pre result prefix string does not match pre-defined prefix
        if (prevResult.slice(0, prefixLength) !== prefix) {
          // Check if the first time user entered something
          if (noImmediatePrefix && !prevResult && value) return value;

          return '';
        }

        var prevValue = this.stripDelimiters(prevResult, delimiter, delimiters);

        // New value has issue, someone typed in between prefix letters
        // Revert to pre value
        if (value.slice(0, prefixLength) !== prefix) {
          return prevValue.slice(prefixLength);
        }

        // No issue, strip prefix for new value
        return value.slice(prefixLength);
    },

    getFirstDiffIndex: function (prev, current) {
        var index = 0;

        while (prev.charAt(index) === current.charAt(index)) {
            if (prev.charAt(index++) === '') {
                return -1;
            }
        }

        return index;
    },

    getFormattedValue: function (value, blocks, blocksLength, delimiter, delimiters, delimiterLazyShow) {
        var result = '',
            multipleDelimiters = delimiters.length > 0,
            currentDelimiter;

        // no options, normal input
        if (blocksLength === 0) {
            return value;
        }

        blocks.forEach(function (length, index) {
            if (value.length > 0) {
                var sub = value.slice(0, length),
                    rest = value.slice(length);

                if (multipleDelimiters) {
                    currentDelimiter = delimiters[delimiterLazyShow ? (index - 1) : index] || currentDelimiter;
                } else {
                    currentDelimiter = delimiter;
                }

                if (delimiterLazyShow) {
                    if (index > 0) {
                        result += currentDelimiter;
                    }

                    result += sub;
                } else {
                    result += sub;

                    if (sub.length === length && index < blocksLength - 1) {
                        result += currentDelimiter;
                    }
                }

                // update remaining string
                value = rest;
            }
        });

        return result;
    },

    // move cursor to the end
    // the first time user focuses on an input with prefix
    fixPrefixCursor: function (el, prefix, delimiter, delimiters) {
        if (!el) {
            return;
        }

        var val = el.value,
            appendix = delimiter || (delimiters[0] || ' ');

        if (!el.setSelectionRange || !prefix || (prefix.length + appendix.length) < val.length) {
            return;
        }

        var len = val.length * 2;

        // set timeout to avoid blink
        setTimeout(function () {
            el.setSelectionRange(len, len);
        }, 1);
    },

    // Check if input field is fully selected
    checkFullSelection: function(value) {
      try {
        var selection = window.getSelection() || document.getSelection() || {};
        return selection.toString().length === value.length;
      } catch (ex) {
        // Ignore
      }

      return false;
    },

    setSelection: function (element, position, doc) {
        if (element !== this.getActiveElement(doc)) {
            return;
        }

        // cursor is already in the end
        if (element && element.value.length <= position) {
          return;
        }

        if (element.createTextRange) {
            var range = element.createTextRange();

            range.move('character', position);
            range.select();
        } else {
            try {
                element.setSelectionRange(position, position);
            } catch (e) {
                // eslint-disable-next-line
                console.warn('The input element type does not support selection');
            }
        }
    },

    getActiveElement: function(parent) {
        var activeElement = parent.activeElement;
        if (activeElement && activeElement.shadowRoot) {
            return this.getActiveElement(activeElement.shadowRoot);
        }
        return activeElement;
    },

    isAndroid: function () {
        return navigator && /android/i.test(navigator.userAgent);
    },

    // On Android chrome, the keyup and keydown events
    // always return key code 229 as a composition that
    // buffers the user’s keystrokes
    // see https://github.com/nosir/cleave.js/issues/147
    isAndroidBackspaceKeydown: function (lastInputValue, currentInputValue) {
        if (!this.isAndroid() || !lastInputValue || !currentInputValue) {
            return false;
        }

        return currentInputValue === lastInputValue.slice(0, -1);
    }
};

var Util_1 = Util;

/**
 * Props Assignment
 *
 * Separate this, so react module can share the usage
 */
var DefaultProperties = {
    // Maybe change to object-assign
    // for now just keep it as simple
    assign: function (target, opts) {
        target = target || {};
        opts = opts || {};

        // credit card
        target.creditCard = !!opts.creditCard;
        target.creditCardStrictMode = !!opts.creditCardStrictMode;
        target.creditCardType = '';
        target.onCreditCardTypeChanged = opts.onCreditCardTypeChanged || (function () {});

        // phone
        target.phone = !!opts.phone;
        target.phoneRegionCode = opts.phoneRegionCode || 'AU';
        target.phoneFormatter = {};

        // time
        target.time = !!opts.time;
        target.timePattern = opts.timePattern || ['h', 'm', 's'];
        target.timeFormat = opts.timeFormat || '24';
        target.timeFormatter = {};

        // date
        target.date = !!opts.date;
        target.datePattern = opts.datePattern || ['d', 'm', 'Y'];
        target.dateMin = opts.dateMin || '';
        target.dateMax = opts.dateMax || '';
        target.dateFormatter = {};

        // numeral
        target.numeral = !!opts.numeral;
        target.numeralIntegerScale = opts.numeralIntegerScale > 0 ? opts.numeralIntegerScale : 0;
        target.numeralDecimalScale = opts.numeralDecimalScale >= 0 ? opts.numeralDecimalScale : 2;
        target.numeralDecimalMark = opts.numeralDecimalMark || '.';
        target.numeralThousandsGroupStyle = opts.numeralThousandsGroupStyle || 'thousand';
        target.numeralPositiveOnly = !!opts.numeralPositiveOnly;
        target.stripLeadingZeroes = opts.stripLeadingZeroes !== false;
        target.signBeforePrefix = !!opts.signBeforePrefix;

        // others
        target.numericOnly = target.creditCard || target.date || !!opts.numericOnly;

        target.uppercase = !!opts.uppercase;
        target.lowercase = !!opts.lowercase;

        target.prefix = (target.creditCard || target.date) ? '' : (opts.prefix || '');
        target.noImmediatePrefix = !!opts.noImmediatePrefix;
        target.prefixLength = target.prefix.length;
        target.rawValueTrimPrefix = !!opts.rawValueTrimPrefix;
        target.copyDelimiter = !!opts.copyDelimiter;

        target.initValue = (opts.initValue !== undefined && opts.initValue !== null) ? opts.initValue.toString() : '';

        target.delimiter =
            (opts.delimiter || opts.delimiter === '') ? opts.delimiter :
                (opts.date ? '/' :
                    (opts.time ? ':' :
                        (opts.numeral ? ',' :
                            (opts.phone ? ' ' :
                                ' '))));
        target.delimiterLength = target.delimiter.length;
        target.delimiterLazyShow = !!opts.delimiterLazyShow;
        target.delimiters = opts.delimiters || [];

        target.blocks = opts.blocks || [];
        target.blocksLength = target.blocks.length;

        target.root = (typeof commonjsGlobal === 'object' && commonjsGlobal) ? commonjsGlobal : window;
        target.document = opts.document || target.root.document;

        target.maxLength = 0;

        target.backspace = false;
        target.result = '';

        target.onValueChanged = opts.onValueChanged || (function () {});

        return target;
    }
};

var DefaultProperties_1 = DefaultProperties;

/**
 * Construct a new Cleave instance by passing the configuration object
 *
 * @param {String | HTMLElement} element
 * @param {Object} opts
 */
var Cleave = function (element, opts) {
    var owner = this;
    var hasMultipleElements = false;

    if (typeof element === 'string') {
        owner.element = document.querySelector(element);
        hasMultipleElements = document.querySelectorAll(element).length > 1;
    } else {
      if (typeof element.length !== 'undefined' && element.length > 0) {
        owner.element = element[0];
        hasMultipleElements = element.length > 1;
      } else {
        owner.element = element;
      }
    }

    if (!owner.element) {
        throw new Error('[cleave.js] Please check the element');
    }

    if (hasMultipleElements) {
      try {
        // eslint-disable-next-line
        console.warn('[cleave.js] Multiple input fields matched, cleave.js will only take the first one.');
      } catch (e) {
        // Old IE
      }
    }

    opts.initValue = owner.element.value;

    owner.properties = Cleave.DefaultProperties.assign({}, opts);

    owner.init();
};

Cleave.prototype = {
    init: function () {
        var owner = this, pps = owner.properties;

        // no need to use this lib
        if (!pps.numeral && !pps.phone && !pps.creditCard && !pps.time && !pps.date && (pps.blocksLength === 0 && !pps.prefix)) {
            owner.onInput(pps.initValue);

            return;
        }

        pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);

        owner.isAndroid = Cleave.Util.isAndroid();
        owner.lastInputValue = '';

        owner.onChangeListener = owner.onChange.bind(owner);
        owner.onKeyDownListener = owner.onKeyDown.bind(owner);
        owner.onFocusListener = owner.onFocus.bind(owner);
        owner.onCutListener = owner.onCut.bind(owner);
        owner.onCopyListener = owner.onCopy.bind(owner);

        owner.element.addEventListener('input', owner.onChangeListener);
        owner.element.addEventListener('keydown', owner.onKeyDownListener);
        owner.element.addEventListener('focus', owner.onFocusListener);
        owner.element.addEventListener('cut', owner.onCutListener);
        owner.element.addEventListener('copy', owner.onCopyListener);


        owner.initPhoneFormatter();
        owner.initDateFormatter();
        owner.initTimeFormatter();
        owner.initNumeralFormatter();

        // avoid touch input field if value is null
        // otherwise Firefox will add red box-shadow for <input required />
        if (pps.initValue || (pps.prefix && !pps.noImmediatePrefix)) {
            owner.onInput(pps.initValue);
        }
    },

    initNumeralFormatter: function () {
        var owner = this, pps = owner.properties;

        if (!pps.numeral) {
            return;
        }

        pps.numeralFormatter = new Cleave.NumeralFormatter(
            pps.numeralDecimalMark,
            pps.numeralIntegerScale,
            pps.numeralDecimalScale,
            pps.numeralThousandsGroupStyle,
            pps.numeralPositiveOnly,
            pps.stripLeadingZeroes,
            pps.prefix,
            pps.signBeforePrefix,
            pps.delimiter
        );
    },

    initTimeFormatter: function() {
        var owner = this, pps = owner.properties;

        if (!pps.time) {
            return;
        }

        pps.timeFormatter = new Cleave.TimeFormatter(pps.timePattern, pps.timeFormat);
        pps.blocks = pps.timeFormatter.getBlocks();
        pps.blocksLength = pps.blocks.length;
        pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);
    },

    initDateFormatter: function () {
        var owner = this, pps = owner.properties;

        if (!pps.date) {
            return;
        }

        pps.dateFormatter = new Cleave.DateFormatter(pps.datePattern, pps.dateMin, pps.dateMax);
        pps.blocks = pps.dateFormatter.getBlocks();
        pps.blocksLength = pps.blocks.length;
        pps.maxLength = Cleave.Util.getMaxLength(pps.blocks);
    },

    initPhoneFormatter: function () {
        var owner = this, pps = owner.properties;

        if (!pps.phone) {
            return;
        }

        // Cleave.AsYouTypeFormatter should be provided by
        // external google closure lib
        try {
            pps.phoneFormatter = new Cleave.PhoneFormatter(
                new pps.root.Cleave.AsYouTypeFormatter(pps.phoneRegionCode),
                pps.delimiter
            );
        } catch (ex) {
            throw new Error('[cleave.js] Please include phone-type-formatter.{country}.js lib');
        }
    },

    onKeyDown: function (event) {
        var owner = this, pps = owner.properties,
            charCode = event.which || event.keyCode,
            Util = Cleave.Util,
            currentValue = owner.element.value;

        // if we got any charCode === 8, this means, that this device correctly
        // sends backspace keys in event, so we do not need to apply any hacks
        owner.hasBackspaceSupport = owner.hasBackspaceSupport || charCode === 8;
        if (!owner.hasBackspaceSupport
          && Util.isAndroidBackspaceKeydown(owner.lastInputValue, currentValue)
        ) {
            charCode = 8;
        }

        owner.lastInputValue = currentValue;

        // hit backspace when last character is delimiter
        var postDelimiter = Util.getPostDelimiter(currentValue, pps.delimiter, pps.delimiters);
        if (charCode === 8 && postDelimiter) {
            pps.postDelimiterBackspace = postDelimiter;
        } else {
            pps.postDelimiterBackspace = false;
        }
    },

    onChange: function () {
        this.onInput(this.element.value);
    },

    onFocus: function () {
        var owner = this,
            pps = owner.properties;

        Cleave.Util.fixPrefixCursor(owner.element, pps.prefix, pps.delimiter, pps.delimiters);
    },

    onCut: function (e) {
        if (!Cleave.Util.checkFullSelection(this.element.value)) return;
        this.copyClipboardData(e);
        this.onInput('');
    },

    onCopy: function (e) {
        if (!Cleave.Util.checkFullSelection(this.element.value)) return;
        this.copyClipboardData(e);
    },

    copyClipboardData: function (e) {
        var owner = this,
            pps = owner.properties,
            Util = Cleave.Util,
            inputValue = owner.element.value,
            textToCopy = '';

        if (!pps.copyDelimiter) {
            textToCopy = Util.stripDelimiters(inputValue, pps.delimiter, pps.delimiters);
        } else {
            textToCopy = inputValue;
        }

        try {
            if (e.clipboardData) {
                e.clipboardData.setData('Text', textToCopy);
            } else {
                window.clipboardData.setData('Text', textToCopy);
            }

            e.preventDefault();
        } catch (ex) {
            //  empty
        }
    },

    onInput: function (value) {
        var owner = this, pps = owner.properties,
            Util = Cleave.Util;

        // case 1: delete one more character "4"
        // 1234*| -> hit backspace -> 123|
        // case 2: last character is not delimiter which is:
        // 12|34* -> hit backspace -> 1|34*
        // note: no need to apply this for numeral mode
        var postDelimiterAfter = Util.getPostDelimiter(value, pps.delimiter, pps.delimiters);
        if (!pps.numeral && pps.postDelimiterBackspace && !postDelimiterAfter) {
            value = Util.headStr(value, value.length - pps.postDelimiterBackspace.length);
        }

        // phone formatter
        if (pps.phone) {
            if (pps.prefix && (!pps.noImmediatePrefix || value.length)) {
                pps.result = pps.prefix + pps.phoneFormatter.format(value).slice(pps.prefix.length);
            } else {
                pps.result = pps.phoneFormatter.format(value);
            }
            owner.updateValueState();

            return;
        }

        // numeral formatter
        if (pps.numeral) {
            // Do not show prefix when noImmediatePrefix is specified
            // This mostly because we need to show user the native input placeholder
            if (pps.prefix && pps.noImmediatePrefix && value.length === 0) {
                pps.result = '';
            } else {
                pps.result = pps.numeralFormatter.format(value);
            }
            owner.updateValueState();

            return;
        }

        // date
        if (pps.date) {
            value = pps.dateFormatter.getValidatedDate(value);
        }

        // time
        if (pps.time) {
            value = pps.timeFormatter.getValidatedTime(value);
        }

        // strip delimiters
        value = Util.stripDelimiters(value, pps.delimiter, pps.delimiters);

        // strip prefix
        value = Util.getPrefixStrippedValue(
            value, pps.prefix, pps.prefixLength,
            pps.result, pps.delimiter, pps.delimiters, pps.noImmediatePrefix
        );

        // strip non-numeric characters
        value = pps.numericOnly ? Util.strip(value, /[^\d]/g) : value;

        // convert case
        value = pps.uppercase ? value.toUpperCase() : value;
        value = pps.lowercase ? value.toLowerCase() : value;

        // prevent from showing prefix when no immediate option enabled with empty input value
        if (pps.prefix && (!pps.noImmediatePrefix || value.length)) {
            value = pps.prefix + value;

            // no blocks specified, no need to do formatting
            if (pps.blocksLength === 0) {
                pps.result = value;
                owner.updateValueState();

                return;
            }
        }

        // update credit card props
        if (pps.creditCard) {
            owner.updateCreditCardPropsByValue(value);
        }

        // strip over length characters
        value = Util.headStr(value, pps.maxLength);

        // apply blocks
        pps.result = Util.getFormattedValue(
            value,
            pps.blocks, pps.blocksLength,
            pps.delimiter, pps.delimiters, pps.delimiterLazyShow
        );

        owner.updateValueState();
    },

    updateCreditCardPropsByValue: function (value) {
        var owner = this, pps = owner.properties,
            Util = Cleave.Util,
            creditCardInfo;

        // At least one of the first 4 characters has changed
        if (Util.headStr(pps.result, 4) === Util.headStr(value, 4)) {
            return;
        }

        creditCardInfo = Cleave.CreditCardDetector.getInfo(value, pps.creditCardStrictMode);

        pps.blocks = creditCardInfo.blocks;
        pps.blocksLength = pps.blocks.length;
        pps.maxLength = Util.getMaxLength(pps.blocks);

        // credit card type changed
        if (pps.creditCardType !== creditCardInfo.type) {
            pps.creditCardType = creditCardInfo.type;

            pps.onCreditCardTypeChanged.call(owner, pps.creditCardType);
        }
    },

    updateValueState: function () {
        var owner = this,
            Util = Cleave.Util,
            pps = owner.properties;

        if (!owner.element) {
            return;
        }

        var endPos = owner.element.selectionEnd;
        var oldValue = owner.element.value;
        var newValue = pps.result;

        endPos = Util.getNextCursorPosition(endPos, oldValue, newValue, pps.delimiter, pps.delimiters);

        // fix Android browser type="text" input field
        // cursor not jumping issue
        if (owner.isAndroid) {
            window.setTimeout(function () {
                owner.element.value = newValue;
                Util.setSelection(owner.element, endPos, pps.document, false);
                owner.callOnValueChanged();
            }, 1);

            return;
        }

        owner.element.value = newValue;
        Util.setSelection(owner.element, endPos, pps.document, false);
        owner.callOnValueChanged();
    },

    callOnValueChanged: function () {
        var owner = this,
            pps = owner.properties;

        pps.onValueChanged.call(owner, {
            target: {
                value: pps.result,
                rawValue: owner.getRawValue()
            }
        });
    },

    setPhoneRegionCode: function (phoneRegionCode) {
        var owner = this, pps = owner.properties;

        pps.phoneRegionCode = phoneRegionCode;
        owner.initPhoneFormatter();
        owner.onChange();
    },

    setRawValue: function (value) {
        var owner = this, pps = owner.properties;

        value = value !== undefined && value !== null ? value.toString() : '';

        if (pps.numeral) {
            value = value.replace('.', pps.numeralDecimalMark);
        }

        pps.postDelimiterBackspace = false;

        owner.element.value = value;
        owner.onInput(value);
    },

    getRawValue: function () {
        var owner = this,
            pps = owner.properties,
            Util = Cleave.Util,
            rawValue = owner.element.value;

        if (pps.rawValueTrimPrefix) {
            rawValue = Util.getPrefixStrippedValue(rawValue, pps.prefix, pps.prefixLength, pps.result, pps.delimiter, pps.delimiters);
        }

        if (pps.numeral) {
            rawValue = pps.numeralFormatter.getRawValue(rawValue);
        } else {
            rawValue = Util.stripDelimiters(rawValue, pps.delimiter, pps.delimiters);
        }

        return rawValue;
    },

    getISOFormatDate: function () {
        var owner = this,
            pps = owner.properties;

        return pps.date ? pps.dateFormatter.getISOFormatDate() : '';
    },

    getISOFormatTime: function () {
        var owner = this,
            pps = owner.properties;

        return pps.time ? pps.timeFormatter.getISOFormatTime() : '';
    },

    getFormattedValue: function () {
        return this.element.value;
    },

    destroy: function () {
        var owner = this;

        owner.element.removeEventListener('input', owner.onChangeListener);
        owner.element.removeEventListener('keydown', owner.onKeyDownListener);
        owner.element.removeEventListener('focus', owner.onFocusListener);
        owner.element.removeEventListener('cut', owner.onCutListener);
        owner.element.removeEventListener('copy', owner.onCopyListener);
    },

    toString: function () {
        return '[Cleave Object]';
    }
};

Cleave.NumeralFormatter = NumeralFormatter_1;
Cleave.DateFormatter = DateFormatter_1;
Cleave.TimeFormatter = TimeFormatter_1;
Cleave.PhoneFormatter = PhoneFormatter_1;
Cleave.CreditCardDetector = CreditCardDetector_1;
Cleave.Util = Util_1;
Cleave.DefaultProperties = DefaultProperties_1;

// for angular directive
((typeof commonjsGlobal === 'object' && commonjsGlobal) ? commonjsGlobal : window)['Cleave'] = Cleave;

// CommonJS
var Cleave_1 = Cleave;

/* harmony default export */ __webpack_exports__["default"] = (Cleave_1);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.message_box[data-v-1ac9bd27]{\n    width:483px !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/*!*************************************************!*\
  !*** ./node_modules/css-loader/lib/css-base.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "./node_modules/highcharts-vue/dist/highcharts-vue.min.js":
/*!****************************************************************!*\
  !*** ./node_modules/highcharts-vue/dist/highcharts-vue.min.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

!function(t,e){ true?module.exports=e(__webpack_require__(/*! highcharts */ "./node_modules/highcharts/highcharts.js")):undefined}("undefined"!=typeof self?self:this,function(t){return function(t){function e(n){if(r[n])return r[n].exports;var o=r[n]={i:n,l:!1,exports:{}};return t[n].call(o.exports,o,o.exports,e),o.l=!0,o.exports}var r={};return e.m=t,e.c=r,e.d=function(t,r,n){e.o(t,r)||Object.defineProperty(t,r,{configurable:!1,enumerable:!0,get:n})},e.n=function(t){var r=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(r,"a",r),r},e.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},e.p="",e(e.s=1)}([function(e,r){e.exports=t},function(t,e,r){"use strict";function n(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{};t.component(e.tagName||"highcharts",Object(o.a)(e.highcharts||i.a))}Object.defineProperty(e,"__esModule",{value:!0}),e.default=n,r.d(e,"Chart",function(){return a});var o=r(2),c=r(0),i=r.n(c),a=Object(o.a)(i.a)},function(t,e,r){"use strict";function n(t){return i(t)||c(t)||o()}function o(){throw new TypeError("Invalid attempt to spread non-iterable instance")}function c(t){if(Symbol.iterator in Object(t)||"[object Arguments]"===Object.prototype.toString.call(t))return Array.from(t)}function i(t){if(Array.isArray(t)){for(var e=0,r=new Array(t.length);e<t.length;e++)r[e]=t[e];return r}}var a=r(3),s=function(t){return{template:'<div ref="chart"></div>',render:function(t){return t("div",{ref:"chart"})},props:{constructorType:{type:String,default:"chart"},options:{type:Object,required:!0},callback:Function,updateArgs:{type:Array,default:function(){return[!0,!0]}},highcharts:{type:Object},deepCopyOnUpdate:{type:Boolean,default:!0}},watch:{options:{handler:function(t){var e;(e=this.chart).update.apply(e,[Object(a.a)(t,this.deepCopyOnUpdate)].concat(n(this.updateArgs)))},deep:!0}},mounted:function(){var e=this.highcharts||t;this.options&&e[this.constructorType]?this.chart=e[this.constructorType](this.$refs.chart,Object(a.a)(this.options,!0),this.callback?this.callback:null):this.options?console.warn("'".concat(this.constructorType,"' constructor-type is incorrect. Sometimes this error is caused by the fact, that the corresponding module wasn't imported.")):console.warn('The "options" parameter was not passed.')},beforeDestroy:function(){this.chart&&this.chart.destroy()}}};e.a=s},function(t,e,r){"use strict";function n(t,e,r){function o(o,i){!c.a.isObject(o,!r)||c.a.isClass(o)||c.a.isDOMElement(o)?t[i]=e[i]:t[i]=n(t[i]||c.a.isArray(o)?[]:{},o,r)}return c.a.isArray(e)?e.forEach(o):c.a.objectEach(e,o),t}r.d(e,"a",function(){return i});var o=r(0),c=r.n(o),i=function(t,e){return n({},t,e)}}])});

/***/ }),

/***/ "./node_modules/highcharts/highcharts.js":
/*!***********************************************!*\
  !*** ./node_modules/highcharts/highcharts.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/*
 Highcharts JS v8.0.0 (2019-12-10)

 (c) 2009-2018 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(S,M){ true&&module.exports?(M["default"]=M,module.exports=S.document?M(S):M): true?!(__WEBPACK_AMD_DEFINE_RESULT__ = (function(){return M(S)}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):(undefined)})("undefined"!==typeof window?window:this,function(S){function M(c,e,F,I){c.hasOwnProperty(e)||(c[e]=I.apply(null,F))}var J={};M(J,"parts/Globals.js",[],function(){var c="undefined"!==typeof S?S:"undefined"!==typeof window?window:{},e=c.document,
F=c.navigator&&c.navigator.userAgent||"",I=e&&e.createElementNS&&!!e.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect,G=/(edge|msie|trident)/i.test(F)&&!c.opera,H=-1!==F.indexOf("Firefox"),v=-1!==F.indexOf("Chrome"),q=H&&4>parseInt(F.split("Firefox/")[1],10);return{product:"Highcharts",version:"8.0.0",deg2rad:2*Math.PI/360,doc:e,hasBidiBug:q,hasTouch:!!c.TouchEvent,isMS:G,isWebKit:-1!==F.indexOf("AppleWebKit"),isFirefox:H,isChrome:v,isSafari:!v&&-1!==F.indexOf("Safari"),isTouchDevice:/(Mobile|Android|Windows Phone)/.test(F),
SVG_NS:"http://www.w3.org/2000/svg",chartCount:0,seriesTypes:{},symbolSizes:{},svg:I,win:c,marginNames:["plotTop","marginRight","marginBottom","plotLeft"],noop:function(){},charts:[],dateFormats:{}}});M(J,"parts/Utilities.js",[J["parts/Globals.js"]],function(c){function e(d,a){return parseInt(d,a||10)}function F(d){return"string"===typeof d}function I(d){d=Object.prototype.toString.call(d);return"[object Array]"===d||"[object Array Iterator]"===d}function G(d,a){return!!d&&"object"===typeof d&&(!a||
!I(d))}function H(d){return G(d)&&"number"===typeof d.nodeType}function v(d){var a=d&&d.constructor;return!(!G(d,!0)||H(d)||!a||!a.name||"Object"===a.name)}function q(d){return"number"===typeof d&&!isNaN(d)&&Infinity>d&&-Infinity<d}function C(d){return"undefined"!==typeof d&&null!==d}function B(d,a,b){var h;F(a)?C(b)?d.setAttribute(a,b):d&&d.getAttribute&&((h=d.getAttribute(a))||"class"!==a||(h=d.getAttribute(a+"Name"))):A(a,function(a,h){d.setAttribute(h,a)});return h}function u(d,a){var h;d||(d=
{});for(h in a)d[h]=a[h];return d}function w(){for(var d=arguments,a=d.length,b=0;b<a;b++){var l=d[b];if("undefined"!==typeof l&&null!==l)return l}}function m(d,a){var h=function(){};h.prototype=new d;u(h.prototype,a);return h}function r(d,a){return parseFloat(d.toPrecision(a||14))}function D(d,a,b,l){d=+d||0;a=+a;var h=c.defaultOptions.lang,k=(d.toString().split(".")[1]||"").split("e")[0].length,g=d.toString().split("e");if(-1===a)a=Math.min(k,20);else if(!q(a))a=2;else if(a&&g[1]&&0>g[1]){var t=
a+ +g[1];0<=t?(g[0]=(+g[0]).toExponential(t).split("e")[0],a=t):(g[0]=g[0].split(".")[0]||0,d=20>a?(g[0]*Math.pow(10,g[1])).toFixed(a):0,g[1]=0)}var x=(Math.abs(g[1]?g[0]:d)+Math.pow(10,-Math.max(a,k)-1)).toFixed(a);k=String(e(x));t=3<k.length?k.length%3:0;b=w(b,h.decimalPoint);l=w(l,h.thousandsSep);d=(0>d?"-":"")+(t?k.substr(0,t)+l:"");d+=k.substr(t).replace(/(\d{3})(?=\d)/g,"$1"+l);a&&(d+=b+x.slice(-a));g[1]&&0!==+d&&(d+="e"+g[1]);return d}function A(d,a,b){for(var h in d)Object.hasOwnProperty.call(d,
h)&&a.call(b||d[h],d[h],h,d)}c.timers=[];var f=c.charts,b=c.doc,a=c.win;c.error=function(d,b,k,l){var h=q(d),f=h?"Highcharts error #"+d+": www.highcharts.com/errors/"+d+"/":d.toString(),g=function(){if(b)throw Error(f);a.console&&console.log(f)};if("undefined"!==typeof l){var t="";h&&(f+="?");c.objectEach(l,function(d,a){t+="\n"+a+": "+d;h&&(f+=encodeURI(a)+"="+encodeURI(d))});f+=t}k?c.fireEvent(k,"displayError",{code:d,message:f,params:l},g):g()};c.Fx=function(d,a,b){this.options=a;this.elem=d;this.prop=
b};c.Fx.prototype={dSetter:function(){var d=this.paths[0],a=this.paths[1],b=[],l=this.now,f=d.length;if(1===l)b=this.toD;else if(f===a.length&&1>l)for(;f--;){var p=parseFloat(d[f]);b[f]=isNaN(p)||"A"===a[f-4]||"A"===a[f-5]?a[f]:l*parseFloat(""+(a[f]-p))+p}else b=a;this.elem.attr("d",b,null,!0)},update:function(){var d=this.elem,a=this.prop,b=this.now,l=this.options.step;if(this[a+"Setter"])this[a+"Setter"]();else d.attr?d.element&&d.attr(a,b,null,!0):d.style[a]=b+this.unit;l&&l.call(d,b,this)},run:function(d,
b,k){var h=this,f=h.options,p=function(d){return p.stopped?!1:h.step(d)},g=a.requestAnimationFrame||function(d){setTimeout(d,13)},t=function(){for(var d=0;d<c.timers.length;d++)c.timers[d]()||c.timers.splice(d--,1);c.timers.length&&g(t)};d!==b||this.elem["forceAnimate:"+this.prop]?(this.startTime=+new Date,this.start=d,this.end=b,this.unit=k,this.now=this.start,this.pos=0,p.elem=this.elem,p.prop=this.prop,p()&&1===c.timers.push(p)&&g(t)):(delete f.curAnim[this.prop],f.complete&&0===Object.keys(f.curAnim).length&&
f.complete.call(this.elem))},step:function(d){var a=+new Date,b=this.options,l=this.elem,f=b.complete,p=b.duration,g=b.curAnim;if(l.attr&&!l.element)d=!1;else if(d||a>=p+this.startTime){this.now=this.end;this.pos=1;this.update();var t=g[this.prop]=!0;A(g,function(d){!0!==d&&(t=!1)});t&&f&&f.call(l);d=!1}else this.pos=b.easing((a-this.startTime)/p),this.now=this.start+(this.end-this.start)*this.pos,this.update(),d=!0;return d},initPath:function(d,a,b){function h(d){for(n=d.length;n--;){var a="M"===
d[n]||"L"===d[n];var b=/[a-zA-Z]/.test(d[n+3]);a&&b&&d.splice(n+1,0,d[n+1],d[n+2],d[n+1],d[n+2])}}function f(d,a){for(;d.length<r;){d[0]=a[r-d.length];var b=d.slice(0,c);[].splice.apply(d,[0,0].concat(b));z&&(b=d.slice(d.length-c),[].splice.apply(d,[d.length,0].concat(b)),n--)}d[0]="M"}function k(d,a){for(var b=(r-d.length)/c;0<b&&b--;)E=d.slice().splice(d.length/N-c,c*N),E[0]=a[r-c-b*c],x&&(E[c-6]=E[c-2],E[c-5]=E[c-1]),[].splice.apply(d,[d.length/N,0].concat(E)),z&&b--}a=a||"";var g=d.startX,t=d.endX,
x=-1<a.indexOf("C"),c=x?7:3,E,n;a=a.split(" ");b=b.slice();var z=d.isArea,N=z?2:1;x&&(h(a),h(b));if(g&&t){for(n=0;n<g.length;n++)if(g[n]===t[0]){var O=n;break}else if(g[0]===t[t.length-g.length+n]){O=n;var A=!0;break}else if(g[g.length-1]===t[t.length-g.length+n]){O=g.length-n;break}"undefined"===typeof O&&(a=[])}if(a.length&&q(O)){var r=b.length+O*N*c;A?(f(a,b),k(b,a)):(f(b,a),k(a,b))}return[a,b]},fillSetter:function(){c.Fx.prototype.strokeSetter.apply(this,arguments)},strokeSetter:function(){this.elem.attr(this.prop,
c.color(this.start).tweenTo(c.color(this.end),this.pos),null,!0)}};c.merge=function(){var d,a=arguments,b={},l=function(d,a){"object"!==typeof d&&(d={});A(a,function(b,h){!G(b,!0)||v(b)||H(b)?d[h]=a[h]:d[h]=l(d[h]||{},b)});return d};!0===a[0]&&(b=a[1],a=Array.prototype.slice.call(a,2));var f=a.length;for(d=0;d<f;d++)b=l(b,a[d]);return b};c.clearTimeout=function(d){C(d)&&clearTimeout(d)};c.css=function(d,a){c.isMS&&!c.svg&&a&&"undefined"!==typeof a.opacity&&(a.filter="alpha(opacity="+100*a.opacity+
")");u(d.style,a)};c.createElement=function(d,a,f,l,y){d=b.createElement(d);var h=c.css;a&&u(d,a);y&&h(d,{padding:"0",border:"none",margin:"0"});f&&h(d,f);l&&l.appendChild(d);return d};c.datePropsToTimestamps=function(d){A(d,function(a,b){G(a)&&"function"===typeof a.getTime?d[b]=a.getTime():(G(a)||I(a))&&c.datePropsToTimestamps(a)})};c.formatSingle=function(d,a,b){var h=/\.([0-9])/,f=c.defaultOptions.lang,k=b&&b.time||c.time;b=b&&b.numberFormatter||D;/f$/.test(d)?(h=(h=d.match(h))?h[1]:-1,null!==
a&&(a=b(a,h,f.decimalPoint,-1<d.indexOf(",")?f.thousandsSep:""))):a=k.dateFormat(d,a);return a};c.format=function(d,a,b){for(var h="{",f=!1,k,g,t,x,L=[],E;d;){h=d.indexOf(h);if(-1===h)break;k=d.slice(0,h);if(f){k=k.split(":");g=k.shift().split(".");x=g.length;E=a;for(t=0;t<x;t++)E&&(E=E[g[t]]);k.length&&(E=c.formatSingle(k.join(":"),E,b));L.push(E)}else L.push(k);d=d.slice(h+1);h=(f=!f)?"}":"{"}L.push(d);return L.join("")};c.getMagnitude=function(d){return Math.pow(10,Math.floor(Math.log(d)/Math.LN10))};
c.normalizeTickInterval=function(d,a,b,l,f){var h=d;b=w(b,1);var g=d/b;a||(a=f?[1,1.2,1.5,2,2.5,3,4,5,6,8,10]:[1,2,2.5,5,10],!1===l&&(1===b?a=a.filter(function(d){return 0===d%1}):.1>=b&&(a=[1/b])));for(l=0;l<a.length&&!(h=a[l],f&&h*b>=d||!f&&g<=(a[l]+(a[l+1]||a[l]))/2);l++);return h=r(h*b,-Math.round(Math.log(.001)/Math.LN10))};c.stableSort=function(d,a){var b=d.length,h,f;for(f=0;f<b;f++)d[f].safeI=f;d.sort(function(d,b){h=a(d,b);return 0===h?d.safeI-b.safeI:h});for(f=0;f<b;f++)delete d[f].safeI};
c.timeUnits={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};Math.easeInOutSine=function(d){return-.5*(Math.cos(Math.PI*d)-1)};c.getStyle=function(d,b,f){if("width"===b)return b=Math.min(d.offsetWidth,d.scrollWidth),f=d.getBoundingClientRect&&d.getBoundingClientRect().width,f<b&&f>=b-1&&(b=Math.floor(f)),Math.max(0,b-c.getStyle(d,"padding-left")-c.getStyle(d,"padding-right"));if("height"===b)return Math.max(0,Math.min(d.offsetHeight,d.scrollHeight)-
c.getStyle(d,"padding-top")-c.getStyle(d,"padding-bottom"));a.getComputedStyle||c.error(27,!0);if(d=a.getComputedStyle(d,void 0))d=d.getPropertyValue(b),w(f,"opacity"!==b)&&(d=e(d));return d};c.inArray=function(d,a,b){return a.indexOf(d,b)};c.find=Array.prototype.find?function(d,a){return d.find(a)}:function(d,a){var b,h=d.length;for(b=0;b<h;b++)if(a(d[b],b))return d[b]};c.keys=Object.keys;c.stop=function(d,a){for(var b=c.timers.length;b--;)c.timers[b].elem!==d||a&&a!==c.timers[b].prop||(c.timers[b].stopped=
!0)};A({map:"map",each:"forEach",grep:"filter",reduce:"reduce",some:"some"},function(d,a){c[a]=function(a){return Array.prototype[d].apply(a,[].slice.call(arguments,1))}});c.addEvent=function(d,a,b,f){void 0===f&&(f={});var h=d.addEventListener||c.addEventListenerPolyfill;var l="function"===typeof d&&d.prototype?d.prototype.protoEvents=d.prototype.protoEvents||{}:d.hcEvents=d.hcEvents||{};c.Point&&d instanceof c.Point&&d.series&&d.series.chart&&(d.series.chart.runTrackerClick=!0);h&&h.call(d,a,b,
!1);l[a]||(l[a]=[]);l[a].push({fn:b,order:"number"===typeof f.order?f.order:Infinity});l[a].sort(function(d,a){return d.order-a.order});return function(){c.removeEvent(d,a,b)}};c.removeEvent=function(d,a,b){function h(a,b){var g=d.removeEventListener||c.removeEventListenerPolyfill;g&&g.call(d,a,b,!1)}function f(b){var g;if(d.nodeName){if(a){var f={};f[a]=!0}else f=b;A(f,function(d,a){if(b[a])for(g=b[a].length;g--;)h(a,b[a][g].fn)})}}var k;["protoEvents","hcEvents"].forEach(function(g,t){var l=(t=
t?d:d.prototype)&&t[g];l&&(a?(k=l[a]||[],b?(l[a]=k.filter(function(a){return b!==a.fn}),h(a,b)):(f(l),l[a]=[])):(f(l),t[g]={}))})};c.fireEvent=function(a,h,f,l){var d;f=f||{};if(b.createEvent&&(a.dispatchEvent||a.fireEvent)){var k=b.createEvent("Events");k.initEvent(h,!0,!0);u(k,f);a.dispatchEvent?a.dispatchEvent(k):a.fireEvent(h,k)}else f.target||u(f,{preventDefault:function(){f.defaultPrevented=!0},target:a,type:h}),function(b,h){void 0===b&&(b=[]);void 0===h&&(h=[]);var g=0,t=0,l=b.length+h.length;
for(d=0;d<l;d++)!1===(b[g]?h[t]?b[g].order<=h[t].order?b[g++]:h[t++]:b[g++]:h[t++]).fn.call(a,f)&&f.preventDefault()}(a.protoEvents&&a.protoEvents[h],a.hcEvents&&a.hcEvents[h]);l&&!f.defaultPrevented&&l.call(a,f)};c.animate=function(a,b,f){var d,h="",k,g;if(!G(f)){var t=arguments;f={duration:t[2],easing:t[3],complete:t[4]}}q(f.duration)||(f.duration=400);f.easing="function"===typeof f.easing?f.easing:Math[f.easing]||Math.easeInOutSine;f.curAnim=c.merge(b);A(b,function(t,l){c.stop(a,l);g=new c.Fx(a,
f,l);k=null;"d"===l?(g.paths=g.initPath(a,a.d,b.d),g.toD=b.d,d=0,k=1):a.attr?d=a.attr(l):(d=parseFloat(c.getStyle(a,l))||0,"opacity"!==l&&(h="px"));k||(k=t);k&&k.match&&k.match("px")&&(k=k.replace(/px/g,""));g.run(d,k,h)})};c.seriesType=function(a,b,f,l,y){var d=c.getOptions(),g=c.seriesTypes;d.plotOptions[a]=c.merge(d.plotOptions[b],f);g[a]=m(g[b]||function(){},l);g[a].prototype.type=a;y&&(g[a].prototype.pointClass=m(c.Point,y));return g[a]};c.uniqueKey=function(){var a=Math.random().toString(36).substring(2,
9),b=0;return function(){return"highcharts-"+a+"-"+b++}}();c.isFunction=function(a){return"function"===typeof a};a.jQuery&&(a.jQuery.fn.highcharts=function(){var a=[].slice.call(arguments);if(this[0])return a[0]?(new (c[F(a[0])?a.shift():"Chart"])(this[0],a[0],a[1]),this):f[B(this[0],"data-highcharts-chart")]});return{animObject:function(a){return G(a)?c.merge(a):{duration:a?500:0}},arrayMax:function(a){for(var d=a.length,b=a[0];d--;)a[d]>b&&(b=a[d]);return b},arrayMin:function(a){for(var d=a.length,
b=a[0];d--;)a[d]<b&&(b=a[d]);return b},attr:B,clamp:function(a,b,f){return a>b?a<f?a:f:b},correctFloat:r,defined:C,destroyObjectProperties:function(a,b){A(a,function(d,h){d&&d!==b&&d.destroy&&d.destroy();delete a[h]})},discardElement:function(a){var d=c.garbageBin;d||(d=c.createElement("div"));a&&d.appendChild(a);d.innerHTML=""},erase:function(a,b){for(var d=a.length;d--;)if(a[d]===b){a.splice(d,1);break}},extend:u,extendClass:m,isArray:I,isClass:v,isDOMElement:H,isNumber:q,isObject:G,isString:F,
numberFormat:D,objectEach:A,offset:function(d){var h=b.documentElement;d=d.parentElement||d.parentNode?d.getBoundingClientRect():{top:0,left:0};return{top:d.top+(a.pageYOffset||h.scrollTop)-(h.clientTop||0),left:d.left+(a.pageXOffset||h.scrollLeft)-(h.clientLeft||0)}},pad:function(a,b,f){return Array((b||2)+1-String(a).replace("-","").length).join(f||"0")+a},pick:w,pInt:e,relativeLength:function(a,b,f){return/%$/.test(a)?b*parseFloat(a)/100+(f||0):parseFloat(a)},setAnimation:function(a,b){b.renderer.globalAnimation=
w(a,b.options.chart.animation,!0)},splat:function(a){return I(a)?a:[a]},syncTimeout:function(a,b,f){if(0<b)return setTimeout(a,b,f);a.call(0,f);return-1},wrap:function(a,b,f){var d=a[b];a[b]=function(){var a=Array.prototype.slice.call(arguments),b=arguments,g=this;g.proceed=function(){d.apply(g,arguments.length?arguments:b)};a.unshift(d);a=f.apply(this,a);g.proceed=null;return a}}}});M(J,"parts/Color.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.isNumber,I=e.pInt,G=c.merge;
c.Color=function(e){if(!(this instanceof c.Color))return new c.Color(e);this.init(e)};c.Color.prototype={parsers:[{regex:/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,parse:function(c){return[I(c[1]),I(c[2]),I(c[3]),parseFloat(c[4],10)]}},{regex:/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,parse:function(c){return[I(c[1]),I(c[2]),I(c[3]),1]}}],names:{white:"#ffffff",black:"#000000"},init:function(e){var v,q;if((this.input=e=this.names[e&&
e.toLowerCase?e.toLowerCase():""]||e)&&e.stops)this.stops=e.stops.map(function(e){return new c.Color(e[1])});else{if(e&&e.charAt&&"#"===e.charAt()){var C=e.length;e=parseInt(e.substr(1),16);7===C?v=[(e&16711680)>>16,(e&65280)>>8,e&255,1]:4===C&&(v=[(e&3840)>>4|(e&3840)>>8,(e&240)>>4|e&240,(e&15)<<4|e&15,1])}if(!v)for(q=this.parsers.length;q--&&!v;){var H=this.parsers[q];(C=H.regex.exec(e))&&(v=H.parse(C))}}this.rgba=v||[]},get:function(c){var e=this.input,q=this.rgba;if(this.stops){var C=G(e);C.stops=
[].concat(C.stops);this.stops.forEach(function(e,q){C.stops[q]=[C.stops[q][0],e.get(c)]})}else C=q&&F(q[0])?"rgb"===c||!c&&1===q[3]?"rgb("+q[0]+","+q[1]+","+q[2]+")":"a"===c?q[3]:"rgba("+q.join(",")+")":e;return C},brighten:function(c){var e,q=this.rgba;if(this.stops)this.stops.forEach(function(e){e.brighten(c)});else if(F(c)&&0!==c)for(e=0;3>e;e++)q[e]+=I(255*c),0>q[e]&&(q[e]=0),255<q[e]&&(q[e]=255);return this},setOpacity:function(c){this.rgba[3]=c;return this},tweenTo:function(c,e){var q=this.rgba,
v=c.rgba;v.length&&q&&q.length?(c=1!==v[3]||1!==q[3],e=(c?"rgba(":"rgb(")+Math.round(v[0]+(q[0]-v[0])*(1-e))+","+Math.round(v[1]+(q[1]-v[1])*(1-e))+","+Math.round(v[2]+(q[2]-v[2])*(1-e))+(c?","+(v[3]+(q[3]-v[3])*(1-e)):"")+")"):e=c.input||"none";return e}};c.color=function(e){return new c.Color(e)}});M(J,"parts/SvgRenderer.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.animObject,I=e.attr,G=e.defined,H=e.destroyObjectProperties,v=e.erase,q=e.extend,C=e.isArray,B=e.isNumber,
u=e.isObject,w=e.isString,m=e.objectEach,r=e.pick,D=e.pInt,A=e.splat,f=c.addEvent,b=c.animate,a=c.charts,d=c.color,h=c.css,k=c.createElement,l=c.deg2rad,y=c.doc,p=c.hasTouch,g=c.isFirefox,t=c.isMS,x=c.isWebKit,L=c.merge,E=c.noop,n=c.removeEvent,z=c.stop,N=c.svg,O=c.SVG_NS,V=c.symbolSizes,T=c.win;var P=c.SVGElement=function(){return this};q(P.prototype,{opacity:1,SVG_NS:O,textProps:"direction fontSize fontWeight fontFamily fontStyle color lineHeight width textAlign textDecoration textOverflow textOutline cursor".split(" "),
init:function(a,b){this.element="span"===b?k(b):y.createElementNS(this.SVG_NS,b);this.renderer=a;c.fireEvent(this,"afterInit")},animate:function(a,d,n){var K=F(r(d,this.renderer.globalAnimation,!0));r(y.hidden,y.msHidden,y.webkitHidden,!1)&&(K.duration=0);0!==K.duration?(n&&(K.complete=n),b(this,a,K)):(this.attr(a,void 0,n),m(a,function(a,b){K.step&&K.step.call(this,a,{prop:b,pos:1})},this));return this},complexColor:function(a,b,d){var n=this.renderer,K,Q,g,z,f,h,t,l,R,x,k,N=[],p;c.fireEvent(this.renderer,
"complexColor",{args:arguments},function(){a.radialGradient?Q="radialGradient":a.linearGradient&&(Q="linearGradient");Q&&(g=a[Q],f=n.gradients,t=a.stops,x=d.radialReference,C(g)&&(a[Q]=g={x1:g[0],y1:g[1],x2:g[2],y2:g[3],gradientUnits:"userSpaceOnUse"}),"radialGradient"===Q&&x&&!G(g.gradientUnits)&&(z=g,g=L(g,n.getRadialAttr(x,z),{gradientUnits:"userSpaceOnUse"})),m(g,function(a,b){"id"!==b&&N.push(b,a)}),m(t,function(a){N.push(a)}),N=N.join(","),f[N]?k=f[N].attr("id"):(g.id=k=c.uniqueKey(),f[N]=h=
n.createElement(Q).attr(g).add(n.defs),h.radAttr=z,h.stops=[],t.forEach(function(a){0===a[1].indexOf("rgba")?(K=c.color(a[1]),l=K.get("rgb"),R=K.get("a")):(l=a[1],R=1);a=n.createElement("stop").attr({offset:a[0],"stop-color":l,"stop-opacity":R}).add(h);h.stops.push(a)})),p="url("+n.url+"#"+k+")",d.setAttribute(b,p),d.gradient=N,a.toString=function(){return p})})},applyTextOutline:function(a){var b=this.element,d;-1!==a.indexOf("contrast")&&(a=a.replace(/contrast/g,this.renderer.getContrast(b.style.fill)));
a=a.split(" ");var n=a[a.length-1];if((d=a[0])&&"none"!==d&&c.svg){this.fakeTS=!0;a=[].slice.call(b.getElementsByTagName("tspan"));this.ySetter=this.xSetter;d=d.replace(/(^[\d\.]+)(.*?)$/g,function(a,b,d){return 2*b+d});this.removeTextOutline(a);var g=b.firstChild;a.forEach(function(a,K){0===K&&(a.setAttribute("x",b.getAttribute("x")),K=b.getAttribute("y"),a.setAttribute("y",K||0),null===K&&b.setAttribute("y",0));a=a.cloneNode(1);I(a,{"class":"highcharts-text-outline",fill:n,stroke:n,"stroke-width":d,
"stroke-linejoin":"round"});b.insertBefore(a,g)})}},removeTextOutline:function(a){for(var b=a.length,d;b--;)d=a[b],"highcharts-text-outline"===d.getAttribute("class")&&v(a,this.element.removeChild(d))},symbolCustomAttribs:"x y width height r start end innerR anchorX anchorY rounded".split(" "),attr:function(a,b,d,n){var g=this.element,K,Q=this,f,h,t=this.symbolCustomAttribs;if("string"===typeof a&&"undefined"!==typeof b){var l=a;a={};a[l]=b}"string"===typeof a?Q=(this[a+"Getter"]||this._defaultGetter).call(this,
a,g):(m(a,function(b,d){f=!1;n||z(this,d);this.symbolName&&-1!==c.inArray(d,t)&&(K||(this.symbolAttr(a),K=!0),f=!0);!this.rotation||"x"!==d&&"y"!==d||(this.doTransform=!0);f||(h=this[d+"Setter"]||this._defaultSetter,h.call(this,b,d,g),!this.styledMode&&this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(d)&&this.updateShadows(d,b,h))},this),this.afterSetters());d&&d.call(this);return Q},afterSetters:function(){this.doTransform&&(this.updateTransform(),this.doTransform=!1)},updateShadows:function(a,
b,d){for(var n=this.shadows,g=n.length;g--;)d.call(n[g],"height"===a?Math.max(b-(n[g].cutHeight||0),0):"d"===a?this.d:b,a,n[g])},addClass:function(a,b){var d=b?"":this.attr("class")||"";a=(a||"").split(/ /g).reduce(function(a,b){-1===d.indexOf(b)&&a.push(b);return a},d?[d]:[]).join(" ");a!==d&&this.attr("class",a);return this},hasClass:function(a){return-1!==(this.attr("class")||"").split(" ").indexOf(a)},removeClass:function(a){return this.attr("class",(this.attr("class")||"").replace(w(a)?new RegExp(" ?"+
a+" ?"):a,""))},symbolAttr:function(a){var b=this;"x y r start end width height innerR anchorX anchorY clockwise".split(" ").forEach(function(d){b[d]=r(a[d],b[d])});b.attr({d:b.renderer.symbols[b.symbolName](b.x,b.y,b.width,b.height,b)})},clip:function(a){return this.attr("clip-path",a?"url("+this.renderer.url+"#"+a.id+")":"none")},crisp:function(a,b){b=b||a.strokeWidth||0;var d=Math.round(b)%2/2;a.x=Math.floor(a.x||this.x||0)+d;a.y=Math.floor(a.y||this.y||0)+d;a.width=Math.floor((a.width||this.width||
0)-2*d);a.height=Math.floor((a.height||this.height||0)-2*d);G(a.strokeWidth)&&(a.strokeWidth=b);return a},css:function(a){var b=this.styles,d={},n=this.element,g="",z=!b,f=["textOutline","textOverflow","width"];a&&a.color&&(a.fill=a.color);b&&m(a,function(a,n){a!==b[n]&&(d[n]=a,z=!0)});if(z){b&&(a=q(b,d));if(a)if(null===a.width||"auto"===a.width)delete this.textWidth;else if("text"===n.nodeName.toLowerCase()&&a.width)var Q=this.textWidth=D(a.width);this.styles=a;Q&&!N&&this.renderer.forExport&&delete a.width;
if(n.namespaceURI===this.SVG_NS){var t=function(a,b){return"-"+b.toLowerCase()};m(a,function(a,b){-1===f.indexOf(b)&&(g+=b.replace(/([A-Z])/g,t)+":"+a+";")});g&&I(n,"style",g)}else h(n,a);this.added&&("text"===this.element.nodeName&&this.renderer.buildText(this),a&&a.textOutline&&this.applyTextOutline(a.textOutline))}return this},getStyle:function(a){return T.getComputedStyle(this.element||this,"").getPropertyValue(a)},strokeWidth:function(){if(!this.renderer.styledMode)return this["stroke-width"]||
0;var a=this.getStyle("stroke-width"),b=0;if(a.indexOf("px")===a.length-2)b=D(a);else if(""!==a){var d=y.createElementNS(O,"rect");I(d,{width:a,"stroke-width":0});this.element.parentNode.appendChild(d);b=d.getBBox().width;d.parentNode.removeChild(d)}return b},on:function(a,b){var d=this,n=d.element;p&&"click"===a?(n.ontouchstart=function(a){d.touchEventFired=Date.now();a.preventDefault();b.call(n,a)},n.onclick=function(a){(-1===T.navigator.userAgent.indexOf("Android")||1100<Date.now()-(d.touchEventFired||
0))&&b.call(n,a)}):n["on"+a]=b;return this},setRadialReference:function(a){var b=this.renderer.gradients[this.element.gradient];this.element.radialReference=a;b&&b.radAttr&&b.animate(this.renderer.getRadialAttr(a,b.radAttr));return this},translate:function(a,b){return this.attr({translateX:a,translateY:b})},invert:function(a){this.inverted=a;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,b=this.translateY||0,d=this.scaleX,n=this.scaleY,g=this.inverted,z=this.rotation,
f=this.matrix,h=this.element;g&&(a+=this.width,b+=this.height);a=["translate("+a+","+b+")"];G(f)&&a.push("matrix("+f.join(",")+")");g?a.push("rotate(90) scale(-1,1)"):z&&a.push("rotate("+z+" "+r(this.rotationOriginX,h.getAttribute("x"),0)+" "+r(this.rotationOriginY,h.getAttribute("y")||0)+")");(G(d)||G(n))&&a.push("scale("+r(d,1)+" "+r(n,1)+")");a.length&&h.setAttribute("transform",a.join(" "))},toFront:function(){var a=this.element;a.parentNode.appendChild(a);return this},align:function(a,b,d){var n,
g={};var z=this.renderer;var f=z.alignedObjects;var K,h;if(a){if(this.alignOptions=a,this.alignByTranslate=b,!d||w(d))this.alignTo=n=d||"renderer",v(f,this),f.push(this),d=null}else a=this.alignOptions,b=this.alignByTranslate,n=this.alignTo;d=r(d,z[n],z);n=a.align;z=a.verticalAlign;f=(d.x||0)+(a.x||0);var t=(d.y||0)+(a.y||0);"right"===n?K=1:"center"===n&&(K=2);K&&(f+=(d.width-(a.width||0))/K);g[b?"translateX":"x"]=Math.round(f);"bottom"===z?h=1:"middle"===z&&(h=2);h&&(t+=(d.height-(a.height||0))/
h);g[b?"translateY":"y"]=Math.round(t);this[this.placed?"animate":"attr"](g);this.placed=!0;this.alignAttr=g;return this},getBBox:function(a,b){var d,n=this.renderer,g=this.element,z=this.styles,f=this.textStr,K,h=n.cache,t=n.cacheKeys,Q=g.namespaceURI===this.SVG_NS;b=r(b,this.rotation,0);var x=n.styledMode?g&&P.prototype.getStyle.call(g,"font-size"):z&&z.fontSize;if(G(f)){var c=f.toString();-1===c.indexOf("<")&&(c=c.replace(/[0-9]/g,"0"));c+=["",b,x,this.textWidth,z&&z.textOverflow].join()}c&&!a&&
(d=h[c]);if(!d){if(Q||n.forExport){try{(K=this.fakeTS&&function(a){[].forEach.call(g.querySelectorAll(".highcharts-text-outline"),function(b){b.style.display=a})})&&K("none"),d=g.getBBox?q({},g.getBBox()):{width:g.offsetWidth,height:g.offsetHeight},K&&K("")}catch(ca){""}if(!d||0>d.width)d={width:0,height:0}}else d=this.htmlGetBBox();n.isSVG&&(a=d.width,n=d.height,Q&&(d.height=n={"11px,17":14,"13px,20":16}[z&&z.fontSize+","+Math.round(n)]||n),b&&(z=b*l,d.width=Math.abs(n*Math.sin(z))+Math.abs(a*Math.cos(z)),
d.height=Math.abs(n*Math.cos(z))+Math.abs(a*Math.sin(z))));if(c&&0<d.height){for(;250<t.length;)delete h[t.shift()];h[c]||t.push(c);h[c]=d}}return d},show:function(a){return this.attr({visibility:a?"inherit":"visible"})},hide:function(a){a?this.attr({y:-9999}):this.attr({visibility:"hidden"});return this},fadeOut:function(a){var b=this;b.animate({opacity:0},{duration:a||150,complete:function(){b.attr({y:-9999})}})},add:function(a){var b=this.renderer,d=this.element;a&&(this.parentGroup=a);this.parentInverted=
a&&a.inverted;"undefined"!==typeof this.textStr&&b.buildText(this);this.added=!0;if(!a||a.handleZ||this.zIndex)var n=this.zIndexSetter();n||(a?a.element:b.box).appendChild(d);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var b=a.parentNode;b&&b.removeChild(a)},destroy:function(){var a=this,b=a.element||{},d=a.renderer,n=d.isSVG&&"SPAN"===b.nodeName&&a.parentGroup,g=b.ownerSVGElement,f=a.clipPath;b.onclick=b.onmouseout=b.onmouseover=b.onmousemove=b.point=null;z(a);f&&g&&([].forEach.call(g.querySelectorAll("[clip-path],[CLIP-PATH]"),
function(a){-1<a.getAttribute("clip-path").indexOf(f.element.id)&&a.removeAttribute("clip-path")}),a.clipPath=f.destroy());if(a.stops){for(g=0;g<a.stops.length;g++)a.stops[g]=a.stops[g].destroy();a.stops=null}a.safeRemoveChild(b);for(d.styledMode||a.destroyShadows();n&&n.div&&0===n.div.childNodes.length;)b=n.parentGroup,a.safeRemoveChild(n.div),delete n.div,n=b;a.alignTo&&v(d.alignedObjects,a);m(a,function(b,d){a[d]&&a[d].parentGroup===a&&a[d].destroy&&a[d].destroy();delete a[d]})},shadow:function(a,
b,d){var n=[],g,z=this.element;if(!a)this.destroyShadows();else if(!this.shadows){var f=r(a.width,3);var h=(a.opacity||.15)/f;var K=this.parentInverted?"(-1,-1)":"("+r(a.offsetX,1)+", "+r(a.offsetY,1)+")";for(g=1;g<=f;g++){var t=z.cloneNode(0);var l=2*f+1-2*g;I(t,{stroke:a.color||"#000000","stroke-opacity":h*g,"stroke-width":l,transform:"translate"+K,fill:"none"});t.setAttribute("class",(t.getAttribute("class")||"")+" highcharts-shadow");d&&(I(t,"height",Math.max(I(t,"height")-l,0)),t.cutHeight=l);
b?b.element.appendChild(t):z.parentNode&&z.parentNode.insertBefore(t,z);n.push(t)}this.shadows=n}return this},destroyShadows:function(){(this.shadows||[]).forEach(function(a){this.safeRemoveChild(a)},this);this.shadows=void 0},xGetter:function(a){"circle"===this.element.nodeName&&("x"===a?a="cx":"y"===a&&(a="cy"));return this._defaultGetter(a)},_defaultGetter:function(a){a=r(this[a+"Value"],this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));return a},
dSetter:function(a,b,d){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");this[b]!==a&&(d.setAttribute(b,a),this[b]=a)},dashstyleSetter:function(a){var b,d=this["stroke-width"];"inherit"===d&&(d=1);if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,"").split(",");for(b=a.length;b--;)a[b]=D(a[b])*
d;a=a.join(",").replace(/NaN/g,"none");this.element.setAttribute("stroke-dasharray",a)}},alignSetter:function(a){var b={left:"start",center:"middle",right:"end"};b[a]&&(this.alignValue=a,this.element.setAttribute("text-anchor",b[a]))},opacitySetter:function(a,b,d){this[b]=a;d.setAttribute(b,a)},titleSetter:function(a){var b=this.element.getElementsByTagName("title")[0];b||(b=y.createElementNS(this.SVG_NS,"title"),this.element.appendChild(b));b.firstChild&&b.removeChild(b.firstChild);b.appendChild(y.createTextNode(String(r(a,
"")).replace(/<[^>]*>/g,"").replace(/&lt;/g,"<").replace(/&gt;/g,">")))},textSetter:function(a){a!==this.textStr&&(delete this.bBox,delete this.textPxLength,this.textStr=a,this.added&&this.renderer.buildText(this))},setTextPath:function(a,b){var d=this.element,n={textAnchor:"text-anchor"},g=!1,z=this.textPathWrapper,f=!z;b=L(!0,{enabled:!0,attributes:{dy:-5,startOffset:"50%",textAnchor:"middle"}},b);var h=b.attributes;if(a&&b&&b.enabled){z&&null===z.element.parentNode?(f=!0,z=z.destroy()):z&&this.removeTextOutline.call(z.parentGroup,
[].slice.call(d.getElementsByTagName("tspan")));this.options&&this.options.padding&&(h.dx=-this.options.padding);z||(this.textPathWrapper=z=this.renderer.createElement("textPath"),g=!0);var t=z.element;(b=a.element.getAttribute("id"))||a.element.setAttribute("id",b=c.uniqueKey());if(f)for(a=d.getElementsByTagName("tspan");a.length;)a[0].setAttribute("y",0),B(h.dx)&&a[0].setAttribute("x",-h.dx),t.appendChild(a[0]);g&&z.add({element:this.text?this.text.element:d});t.setAttributeNS("http://www.w3.org/1999/xlink",
"href",this.renderer.url+"#"+b);G(h.dy)&&(t.parentNode.setAttribute("dy",h.dy),delete h.dy);G(h.dx)&&(t.parentNode.setAttribute("dx",h.dx),delete h.dx);m(h,function(a,b){t.setAttribute(n[b]||b,a)});d.removeAttribute("transform");this.removeTextOutline.call(z,[].slice.call(d.getElementsByTagName("tspan")));this.text&&!this.renderer.styledMode&&this.attr({fill:"none","stroke-width":0});this.applyTextOutline=this.updateTransform=E}else z&&(delete this.updateTransform,delete this.applyTextOutline,this.destroyTextPath(d,
a),this.updateTransform(),this.options.rotation&&this.applyTextOutline(this.options.style.textOutline));return this},destroyTextPath:function(a,b){var d=a.getElementsByTagName("text")[0];if(d){if(d.removeAttribute("dx"),d.removeAttribute("dy"),b.element.setAttribute("id",""),d.getElementsByTagName("textPath").length){for(a=this.textPathWrapper.element.childNodes;a.length;)d.appendChild(a[0]);d.removeChild(this.textPathWrapper.element)}}else if(a.getAttribute("dx")||a.getAttribute("dy"))a.removeAttribute("dx"),
a.removeAttribute("dy");this.textPathWrapper=this.textPathWrapper.destroy()},fillSetter:function(a,b,d){"string"===typeof a?d.setAttribute(b,a):a&&this.complexColor(a,b,d)},visibilitySetter:function(a,b,d){"inherit"===a?d.removeAttribute(b):this[b]!==a&&d.setAttribute(b,a);this[b]=a},zIndexSetter:function(a,b){var d=this.renderer,n=this.parentGroup,g=(n||d).element||d.box,z=this.element,f=!1;d=g===d.box;var h=this.added;var t;G(a)?(z.setAttribute("data-z-index",a),a=+a,this[b]===a&&(h=!1)):G(this[b])&&
z.removeAttribute("data-z-index");this[b]=a;if(h){(a=this.zIndex)&&n&&(n.handleZ=!0);b=g.childNodes;for(t=b.length-1;0<=t&&!f;t--){n=b[t];h=n.getAttribute("data-z-index");var l=!G(h);if(n!==z)if(0>a&&l&&!d&&!t)g.insertBefore(z,b[t]),f=!0;else if(D(h)<=a||l&&(!G(a)||0<=a))g.insertBefore(z,b[t+1]||null),f=!0}f||(g.insertBefore(z,b[d?3:0]||null),f=!0)}return f},_defaultSetter:function(a,b,d){d.setAttribute(b,a)}});P.prototype.yGetter=P.prototype.xGetter;P.prototype.translateXSetter=P.prototype.translateYSetter=
P.prototype.rotationSetter=P.prototype.verticalAlignSetter=P.prototype.rotationOriginXSetter=P.prototype.rotationOriginYSetter=P.prototype.scaleXSetter=P.prototype.scaleYSetter=P.prototype.matrixSetter=function(a,b){this[b]=a;this.doTransform=!0};P.prototype["stroke-widthSetter"]=P.prototype.strokeSetter=function(a,b,d){this[b]=a;this.stroke&&this["stroke-width"]?(P.prototype.fillSetter.call(this,this.stroke,"stroke",d),d.setAttribute("stroke-width",this["stroke-width"]),this.hasStroke=!0):"stroke-width"===
b&&0===a&&this.hasStroke?(d.removeAttribute("stroke"),this.hasStroke=!1):this.renderer.styledMode&&this["stroke-width"]&&(d.setAttribute("stroke-width",this["stroke-width"]),this.hasStroke=!0)};e=c.SVGRenderer=function(){this.init.apply(this,arguments)};q(e.prototype,{Element:P,SVG_NS:O,init:function(a,b,d,n,z,t,l){var K=this.createElement("svg").attr({version:"1.1","class":"highcharts-root"});l||K.css(this.getStyle(n));n=K.element;a.appendChild(n);I(a,"dir","ltr");-1===a.innerHTML.indexOf("xmlns")&&
I(n,"xmlns",this.SVG_NS);this.isSVG=!0;this.box=n;this.boxWrapper=K;this.alignedObjects=[];this.url=(g||x)&&y.getElementsByTagName("base").length?T.location.href.split("#")[0].replace(/<[^>]*>/g,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(y.createTextNode("Created with Highcharts 8.0.0"));this.defs=this.createElement("defs").add();this.allowHTML=t;this.forExport=z;this.styledMode=l;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=
0;this.setSize(b,d,!1);var c;g&&a.getBoundingClientRect&&(b=function(){h(a,{left:0,top:0});c=a.getBoundingClientRect();h(a,{left:Math.ceil(c.left)-c.left+"px",top:Math.ceil(c.top)-c.top+"px"})},b(),this.unSubPixelFix=f(T,"resize",b))},definition:function(a){function b(a,n){var g;A(a).forEach(function(a){var z=d.createElement(a.tagName),f={};m(a,function(a,b){"tagName"!==b&&"children"!==b&&"textContent"!==b&&(f[b]=a)});z.attr(f);z.add(n||d.defs);a.textContent&&z.element.appendChild(y.createTextNode(a.textContent));
b(a.children||[],z);g=z});return g}var d=this;return b(a)},getStyle:function(a){return this.style=q({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},a)},setStyle:function(a){this.boxWrapper.css(this.getStyle(a))},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();H(this.gradients||{});this.gradients=null;a&&(this.defs=a.destroy());this.unSubPixelFix&&
this.unSubPixelFix();return this.alignedObjects=null},createElement:function(a){var b=new this.Element;b.init(this,a);return b},draw:E,getRadialAttr:function(a,b){return{cx:a[0]-a[2]/2+b.cx*a[2],cy:a[1]-a[2]/2+b.cy*a[2],r:b.r*a[2]}},truncate:function(a,b,d,n,g,z,f){var h=this,t=a.rotation,l,K=n?1:0,x=(d||n).length,c=x,k=[],N=function(a){b.firstChild&&b.removeChild(b.firstChild);a&&b.appendChild(y.createTextNode(a))},p=function(z,t){t=t||z;if("undefined"===typeof k[t])if(b.getSubStringLength)try{k[t]=
g+b.getSubStringLength(0,n?t+1:t)}catch(ea){""}else h.getSpanWidth&&(N(f(d||n,z)),k[t]=g+h.getSpanWidth(a,b));return k[t]},R;a.rotation=0;var O=p(b.textContent.length);if(R=g+O>z){for(;K<=x;)c=Math.ceil((K+x)/2),n&&(l=f(n,c)),O=p(c,l&&l.length-1),K===x?K=x+1:O>z?x=c-1:K=c;0===x?N(""):d&&x===d.length-1||N(l||f(d||n,c))}n&&n.splice(0,c);a.actualWidth=O;a.rotation=t;return R},escapes:{"&":"&amp;","<":"&lt;",">":"&gt;","'":"&#39;",'"':"&quot;"},buildText:function(a){var b=a.element,d=this,n=d.forExport,
g=r(a.textStr,"").toString(),z=-1!==g.indexOf("<"),f=b.childNodes,t,l=I(b,"x"),x=a.styles,c=a.textWidth,k=x&&x.lineHeight,p=x&&x.textOutline,L=x&&"ellipsis"===x.textOverflow,E=x&&"nowrap"===x.whiteSpace,A=x&&x.fontSize,Q,e=f.length;x=c&&!a.added&&this.box;var V=function(a){var n;d.styledMode||(n=/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:A||d.style.fontSize||12);return k?D(k):d.fontMetrics(n,a.getAttribute("style")?a:b).h},q=function(a,b){m(d.escapes,function(d,n){b&&-1!==b.indexOf(d)||
(a=a.toString().replace(new RegExp(d,"g"),n))});return a},P=function(a,b){var d=a.indexOf("<");a=a.substring(d,a.indexOf(">")-d);d=a.indexOf(b+"=");if(-1!==d&&(d=d+b.length+1,b=a.charAt(d),'"'===b||"'"===b))return a=a.substring(d+1),a.substring(0,a.indexOf(b))},T=/<br.*?>/g;var u=[g,L,E,k,p,A,c].join();if(u!==a.textCache){for(a.textCache=u;e--;)b.removeChild(f[e]);z||p||L||c||-1!==g.indexOf(" ")&&(!E||T.test(g))?(x&&x.appendChild(b),z?(g=d.styledMode?g.replace(/<(b|strong)>/g,'<span class="highcharts-strong">').replace(/<(i|em)>/g,
'<span class="highcharts-emphasized">'):g.replace(/<(b|strong)>/g,'<span style="font-weight:bold">').replace(/<(i|em)>/g,'<span style="font-style:italic">'),g=g.replace(/<a/g,"<span").replace(/<\/(b|strong|i|em|a)>/g,"</span>").split(T)):g=[g],g=g.filter(function(a){return""!==a}),g.forEach(function(g,z){var f=0,x=0;g=g.replace(/^\s+|\s+$/g,"").replace(/<span/g,"|||<span").replace(/<\/span>/g,"</span>|||");var K=g.split("|||");K.forEach(function(g){if(""!==g||1===K.length){var k={},p=y.createElementNS(d.SVG_NS,
"tspan"),R,r;(R=P(g,"class"))&&I(p,"class",R);if(R=P(g,"style"))R=R.replace(/(;| |^)color([ :])/,"$1fill$2"),I(p,"style",R);(r=P(g,"href"))&&!n&&(I(p,"onclick",'location.href="'+r+'"'),I(p,"class","highcharts-anchor"),d.styledMode||h(p,{cursor:"pointer"}));g=q(g.replace(/<[a-zA-Z\/](.|\n)*?>/g,"")||" ");if(" "!==g){p.appendChild(y.createTextNode(g));f?k.dx=0:z&&null!==l&&(k.x=l);I(p,k);b.appendChild(p);!f&&Q&&(!N&&n&&h(p,{display:"block"}),I(p,"dy",V(p)));if(c){var m=g.replace(/([^\^])-/g,"$1- ").split(" ");
k=!E&&(1<K.length||z||1<m.length);r=0;var e=V(p);if(L)t=d.truncate(a,p,g,void 0,0,Math.max(0,c-parseInt(A||12,10)),function(a,b){return a.substring(0,b)+"\u2026"});else if(k)for(;m.length;)m.length&&!E&&0<r&&(p=y.createElementNS(O,"tspan"),I(p,{dy:e,x:l}),R&&I(p,"style",R),p.appendChild(y.createTextNode(m.join(" ").replace(/- /g,"-"))),b.appendChild(p)),d.truncate(a,p,null,m,0===r?x:0,c,function(a,b){return m.slice(0,b).join(" ").replace(/- /g,"-")}),x=a.actualWidth,r++}f++}}});Q=Q||b.childNodes.length}),
L&&t&&a.attr("title",q(a.textStr,["&lt;","&gt;"])),x&&x.removeChild(b),p&&a.applyTextOutline&&a.applyTextOutline(p)):b.appendChild(y.createTextNode(q(g)))}},getContrast:function(a){a=d(a).rgba;a[0]*=1;a[1]*=1.2;a[2]*=.5;return 459<a[0]+a[1]+a[2]?"#000000":"#FFFFFF"},button:function(a,b,d,n,g,z,h,l,x,c){var K=this.label(a,b,d,x,null,null,c,null,"button"),k=0,N=this.styledMode;K.attr(L({padding:8,r:2},g));if(!N){g=L({fill:"#f7f7f7",stroke:"#cccccc","stroke-width":1,style:{color:"#333333",cursor:"pointer",
fontWeight:"normal"}},g);var p=g.style;delete g.style;z=L(g,{fill:"#e6e6e6"},z);var O=z.style;delete z.style;h=L(g,{fill:"#e6ebf5",style:{color:"#000000",fontWeight:"bold"}},h);var R=h.style;delete h.style;l=L(g,{style:{color:"#cccccc"}},l);var E=l.style;delete l.style}f(K.element,t?"mouseover":"mouseenter",function(){3!==k&&K.setState(1)});f(K.element,t?"mouseout":"mouseleave",function(){3!==k&&K.setState(k)});K.setState=function(a){1!==a&&(K.state=k=a);K.removeClass(/highcharts-button-(normal|hover|pressed|disabled)/).addClass("highcharts-button-"+
["normal","hover","pressed","disabled"][a||0]);N||K.attr([g,z,h,l][a||0]).css([p,O,R,E][a||0])};N||K.attr(g).css(q({cursor:"default"},p));return K.on("click",function(a){3!==k&&n.call(K,a)})},crispLine:function(a,b){a[1]===a[4]&&(a[1]=a[4]=Math.round(a[1])-b%2/2);a[2]===a[5]&&(a[2]=a[5]=Math.round(a[2])+b%2/2);return a},path:function(a){var b=this.styledMode?{}:{fill:"none"};C(a)?b.d=a:u(a)&&q(b,a);return this.createElement("path").attr(b)},circle:function(a,b,d){a=u(a)?a:"undefined"===typeof a?{}:
{x:a,y:b,r:d};b=this.createElement("circle");b.xSetter=b.ySetter=function(a,b,d){d.setAttribute("c"+b,a)};return b.attr(a)},arc:function(a,b,d,n,g,z){u(a)?(n=a,b=n.y,d=n.r,a=n.x):n={innerR:n,start:g,end:z};a=this.symbol("arc",a,b,d,d,n);a.r=d;return a},rect:function(a,b,d,n,g,z){g=u(a)?a.r:g;var f=this.createElement("rect");a=u(a)?a:"undefined"===typeof a?{}:{x:a,y:b,width:Math.max(d,0),height:Math.max(n,0)};this.styledMode||("undefined"!==typeof z&&(a.strokeWidth=z,a=f.crisp(a)),a.fill="none");g&&
(a.r=g);f.rSetter=function(a,b,d){f.r=a;I(d,{rx:a,ry:a})};f.rGetter=function(){return f.r};return f.attr(a)},setSize:function(a,b,d){var n=this.alignedObjects,g=n.length;this.width=a;this.height=b;for(this.boxWrapper.animate({width:a,height:b},{step:function(){this.attr({viewBox:"0 0 "+this.attr("width")+" "+this.attr("height")})},duration:r(d,!0)?void 0:0});g--;)n[g].align()},g:function(a){var b=this.createElement("g");return a?b.attr({"class":"highcharts-"+a}):b},image:function(a,b,d,n,g,z){var h=
{preserveAspectRatio:"none"},t=function(a,b){a.setAttributeNS?a.setAttributeNS("http://www.w3.org/1999/xlink","href",b):a.setAttribute("hc-svg-href",b)},l=function(b){t(x.element,a);z.call(x,b)};1<arguments.length&&q(h,{x:b,y:d,width:n,height:g});var x=this.createElement("image").attr(h);z?(t(x.element,"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="),h=new T.Image,f(h,"load",l),h.src=a,h.complete&&l({})):t(x.element,a);return x},symbol:function(b,d,n,g,z,f){var t=this,
l=/^url\((.*?)\)$/,x=l.test(b),c=!x&&(this.symbols[b]?b:"circle"),K=c&&this.symbols[c],N=G(d)&&K&&K.call(this.symbols,Math.round(d),Math.round(n),g,z,f);if(K){var p=this.path(N);t.styledMode||p.attr("fill","none");q(p,{symbolName:c,x:d,y:n,width:g,height:z});f&&q(p,f)}else if(x){var O=b.match(l)[1];p=this.image(O);p.imgwidth=r(V[O]&&V[O].width,f&&f.width);p.imgheight=r(V[O]&&V[O].height,f&&f.height);var R=function(){p.attr({width:p.width,height:p.height})};["width","height"].forEach(function(a){p[a+
"Setter"]=function(a,b){var d={},n=this["img"+b],g="width"===b?"translateX":"translateY";this[b]=a;G(n)&&(f&&"within"===f.backgroundSize&&this.width&&this.height&&(n=Math.round(n*Math.min(this.width/this.imgwidth,this.height/this.imgheight))),this.element&&this.element.setAttribute(b,n),this.alignByTranslate||(d[g]=((this[b]||0)-n)/2,this.attr(d)))}});G(d)&&p.attr({x:d,y:n});p.isImg=!0;G(p.imgwidth)&&G(p.imgheight)?R():(p.attr({width:0,height:0}),k("img",{onload:function(){var b=a[t.chartIndex];0===
this.width&&(h(this,{position:"absolute",top:"-999em"}),y.body.appendChild(this));V[O]={width:this.width,height:this.height};p.imgwidth=this.width;p.imgheight=this.height;p.element&&R();this.parentNode&&this.parentNode.removeChild(this);t.imgCount--;if(!t.imgCount&&b&&b.onload)b.onload()},src:O}),this.imgCount++)}return p},symbols:{circle:function(a,b,d,n){return this.arc(a+d/2,b+n/2,d/2,n/2,{start:.5*Math.PI,end:2.5*Math.PI,open:!1})},square:function(a,b,d,n){return["M",a,b,"L",a+d,b,a+d,b+n,a,b+
n,"Z"]},triangle:function(a,b,d,n){return["M",a+d/2,b,"L",a+d,b+n,a,b+n,"Z"]},"triangle-down":function(a,b,d,n){return["M",a,b,"L",a+d,b,a+d/2,b+n,"Z"]},diamond:function(a,b,d,n){return["M",a+d/2,b,"L",a+d,b+n/2,a+d/2,b+n,a,b+n/2,"Z"]},arc:function(a,b,d,n,g){var z=g.start,f=g.r||d,h=g.r||n||d,t=g.end-.001;d=g.innerR;n=r(g.open,.001>Math.abs(g.end-g.start-2*Math.PI));var l=Math.cos(z),x=Math.sin(z),c=Math.cos(t);t=Math.sin(t);z=r(g.longArc,.001>g.end-z-Math.PI?0:1);f=["M",a+f*l,b+h*x,"A",f,h,0,z,
r(g.clockwise,1),a+f*c,b+h*t];G(d)&&f.push(n?"M":"L",a+d*c,b+d*t,"A",d,d,0,z,G(g.clockwise)?1-g.clockwise:0,a+d*l,b+d*x);f.push(n?"":"Z");return f},callout:function(a,b,d,n,g){var z=Math.min(g&&g.r||0,d,n),f=z+6,h=g&&g.anchorX;g=g&&g.anchorY;var t=["M",a+z,b,"L",a+d-z,b,"C",a+d,b,a+d,b,a+d,b+z,"L",a+d,b+n-z,"C",a+d,b+n,a+d,b+n,a+d-z,b+n,"L",a+z,b+n,"C",a,b+n,a,b+n,a,b+n-z,"L",a,b+z,"C",a,b,a,b,a+z,b];h&&h>d?g>b+f&&g<b+n-f?t.splice(13,3,"L",a+d,g-6,a+d+6,g,a+d,g+6,a+d,b+n-z):t.splice(13,3,"L",a+d,
n/2,h,g,a+d,n/2,a+d,b+n-z):h&&0>h?g>b+f&&g<b+n-f?t.splice(33,3,"L",a,g+6,a-6,g,a,g-6,a,b+z):t.splice(33,3,"L",a,n/2,h,g,a,n/2,a,b+z):g&&g>n&&h>a+f&&h<a+d-f?t.splice(23,3,"L",h+6,b+n,h,b+n+6,h-6,b+n,a+z,b+n):g&&0>g&&h>a+f&&h<a+d-f&&t.splice(3,3,"L",h-6,b,h,b-6,h+6,b,d-z,b);return t}},clipRect:function(a,b,d,n){var g=c.uniqueKey()+"-",z=this.createElement("clipPath").attr({id:g}).add(this.defs);a=this.rect(a,b,d,n,0).add(z);a.id=g;a.clipPath=z;a.count=0;return a},text:function(a,b,d,n){var g={};if(n&&
(this.allowHTML||!this.forExport))return this.html(a,b,d);g.x=Math.round(b||0);d&&(g.y=Math.round(d));G(a)&&(g.text=a);a=this.createElement("text").attr(g);n||(a.xSetter=function(a,b,d){var n=d.getElementsByTagName("tspan"),g=d.getAttribute(b),z;for(z=0;z<n.length;z++){var f=n[z];f.getAttribute(b)===g&&f.setAttribute(b,a)}d.setAttribute(b,a)});return a},fontMetrics:function(a,b){a=!this.styledMode&&/px/.test(a)||!T.getComputedStyle?a||b&&b.style&&b.style.fontSize||this.style&&this.style.fontSize:
b&&P.prototype.getStyle.call(b,"font-size");a=/px/.test(a)?D(a):12;b=24>a?a+3:Math.round(1.2*a);return{h:b,b:Math.round(.8*b),f:a}},rotCorr:function(a,b,d){var n=a;b&&d&&(n=Math.max(n*Math.cos(b*l),4));return{x:-a/3*Math.sin(b*l),y:n}},label:function(a,b,d,g,z,f,h,t,l){var x=this,c=x.styledMode,k=x.g("button"!==l&&"label"),p=k.text=x.text("",0,0,h).attr({zIndex:1}),N,K,O=0,E=3,r=0,A,y,m,R,e,D={},V,T,da=/^url\((.*?)\)$/.test(g),u=c||da,v=function(){return c?N.strokeWidth()%2/2:(V?parseInt(V,10):0)%
2/2};l&&k.addClass("highcharts-"+l);var w=function(){var a=p.element.style,b={};K=("undefined"===typeof A||"undefined"===typeof y||e)&&G(p.textStr)&&p.getBBox();k.width=(A||K.width||0)+2*E+r;k.height=(y||K.height||0)+2*E;T=E+Math.min(x.fontMetrics(a&&a.fontSize,p).b,K?K.height:Infinity);u&&(N||(k.box=N=x.symbols[g]||da?x.symbol(g):x.rect(),N.addClass(("button"===l?"":"highcharts-label-box")+(l?" highcharts-"+l+"-box":"")),N.add(k),a=v(),b.x=a,b.y=(t?-T:0)+a),b.width=Math.round(k.width),b.height=Math.round(k.height),
N.attr(q(b,D)),D={})};var Q=function(){var a=r+E;var b=t?0:T;G(A)&&K&&("center"===e||"right"===e)&&(a+={center:.5,right:1}[e]*(A-K.width));if(a!==p.x||b!==p.y)p.attr("x",a),p.hasBoxWidthChanged&&(K=p.getBBox(!0),w()),"undefined"!==typeof b&&p.attr("y",b);p.x=a;p.y=b};var U=function(a,b){N?N.attr(a,b):D[a]=b};k.onAdd=function(){p.add(k);k.attr({text:a||0===a?a:"",x:b,y:d});N&&G(z)&&k.attr({anchorX:z,anchorY:f})};k.widthSetter=function(a){A=B(a)?a:null};k.heightSetter=function(a){y=a};k["text-alignSetter"]=
function(a){e=a};k.paddingSetter=function(a){G(a)&&a!==E&&(E=k.padding=a,Q())};k.paddingLeftSetter=function(a){G(a)&&a!==r&&(r=a,Q())};k.alignSetter=function(a){a={left:0,center:.5,right:1}[a];a!==O&&(O=a,K&&k.attr({x:m}))};k.textSetter=function(a){"undefined"!==typeof a&&p.attr({text:a});w();Q()};k["stroke-widthSetter"]=function(a,b){a&&(u=!0);V=this["stroke-width"]=a;U(b,a)};c?k.rSetter=function(a,b){U(b,a)}:k.strokeSetter=k.fillSetter=k.rSetter=function(a,b){"r"!==b&&("fill"===b&&a&&(u=!0),k[b]=
a);U(b,a)};k.anchorXSetter=function(a,b){z=k.anchorX=a;U(b,Math.round(a)-v()-m)};k.anchorYSetter=function(a,b){f=k.anchorY=a;U(b,a-R)};k.xSetter=function(a){k.x=a;O&&(a-=O*((A||K.width)+2*E),k["forceAnimate:x"]=!0);m=Math.round(a);k.attr("translateX",m)};k.ySetter=function(a){R=k.y=Math.round(a);k.attr("translateY",R)};var C=k.css;h={css:function(a){if(a){var b={};a=L(a);k.textProps.forEach(function(d){"undefined"!==typeof a[d]&&(b[d]=a[d],delete a[d])});p.css(b);"width"in b&&w();"fontSize"in b&&
(w(),Q())}return C.call(k,a)},getBBox:function(){return{width:K.width+2*E,height:K.height+2*E,x:K.x-E,y:K.y-E}},destroy:function(){n(k.element,"mouseenter");n(k.element,"mouseleave");p&&(p=p.destroy());N&&(N=N.destroy());P.prototype.destroy.call(k);k=x=w=Q=U=null}};c||(h.shadow=function(a){a&&(w(),N&&N.shadow(a));return k});return q(k,h)}});c.Renderer=e});M(J,"parts/Html.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.attr,I=e.defined,G=e.extend,H=e.pick,v=e.pInt,q=c.createElement,
C=c.css,B=c.isFirefox,u=c.isMS,w=c.isWebKit,m=c.SVGElement;e=c.SVGRenderer;var r=c.win;G(m.prototype,{htmlCss:function(c){var r="SPAN"===this.element.tagName&&c&&"width"in c,f=H(r&&c.width,void 0);if(r){delete c.width;this.textWidth=f;var b=!0}c&&"ellipsis"===c.textOverflow&&(c.whiteSpace="nowrap",c.overflow="hidden");this.styles=G(this.styles,c);C(this.element,c);b&&this.htmlUpdateTransform();return this},htmlGetBBox:function(){var c=this.element;return{x:c.offsetLeft,y:c.offsetTop,width:c.offsetWidth,
height:c.offsetHeight}},htmlUpdateTransform:function(){if(this.added){var c=this.renderer,r=this.element,f=this.translateX||0,b=this.translateY||0,a=this.x||0,d=this.y||0,h=this.textAlign||"left",k={left:0,center:.5,right:1}[h],l=this.styles,y=l&&l.whiteSpace;C(r,{marginLeft:f,marginTop:b});!c.styledMode&&this.shadows&&this.shadows.forEach(function(a){C(a,{marginLeft:f+1,marginTop:b+1})});this.inverted&&[].forEach.call(r.childNodes,function(a){c.invertChild(a,r)});if("SPAN"===r.tagName){l=this.rotation;
var p=this.textWidth&&v(this.textWidth),g=[l,h,r.innerHTML,this.textWidth,this.textAlign].join(),t;(t=p!==this.oldTextWidth)&&!(t=p>this.oldTextWidth)&&((t=this.textPxLength)||(C(r,{width:"",whiteSpace:y||"nowrap"}),t=r.offsetWidth),t=t>p);t&&(/[ \-]/.test(r.textContent||r.innerText)||"ellipsis"===r.style.textOverflow)?(C(r,{width:p+"px",display:"block",whiteSpace:y||"normal"}),this.oldTextWidth=p,this.hasBoxWidthChanged=!0):this.hasBoxWidthChanged=!1;g!==this.cTT&&(y=c.fontMetrics(r.style.fontSize,
r).b,!I(l)||l===(this.oldRotation||0)&&h===this.oldAlign||this.setSpanRotation(l,k,y),this.getSpanCorrection(!I(l)&&this.textPxLength||r.offsetWidth,y,k,l,h));C(r,{left:a+(this.xCorr||0)+"px",top:d+(this.yCorr||0)+"px"});this.cTT=g;this.oldRotation=l;this.oldAlign=h}}else this.alignOnAdd=!0},setSpanRotation:function(c,r,f){var b={},a=this.renderer.getTransformKey();b[a]=b.transform="rotate("+c+"deg)";b[a+(B?"Origin":"-origin")]=b.transformOrigin=100*r+"% "+f+"px";C(this.element,b)},getSpanCorrection:function(c,
r,f){this.xCorr=-c*f;this.yCorr=-r}});G(e.prototype,{getTransformKey:function(){return u&&!/Edge/.test(r.navigator.userAgent)?"-ms-transform":w?"-webkit-transform":B?"MozTransform":r.opera?"-o-transform":""},html:function(c,r,f){var b=this.createElement("span"),a=b.element,d=b.renderer,h=d.isSVG,k=function(a,b){["opacity","visibility"].forEach(function(d){a[d+"Setter"]=function(g,f,h){var t=a.div?a.div.style:b;m.prototype[d+"Setter"].call(this,g,f,h);t&&(t[f]=g)}});a.addedSetters=!0};b.textSetter=
function(d){d!==a.innerHTML&&(delete this.bBox,delete this.oldTextWidth);this.textStr=d;a.innerHTML=H(d,"");b.doTransform=!0};h&&k(b,b.element.style);b.xSetter=b.ySetter=b.alignSetter=b.rotationSetter=function(a,d){"align"===d&&(d="textAlign");b[d]=a;b.doTransform=!0};b.afterSetters=function(){this.doTransform&&(this.htmlUpdateTransform(),this.doTransform=!1)};b.attr({text:c,x:Math.round(r),y:Math.round(f)}).css({position:"absolute"});d.styledMode||b.css({fontFamily:this.style.fontFamily,fontSize:this.style.fontSize});
a.style.whiteSpace="nowrap";b.css=b.htmlCss;h&&(b.add=function(f){var h=d.box.parentNode,l=[];if(this.parentGroup=f){var g=f.div;if(!g){for(;f;)l.push(f),f=f.parentGroup;l.reverse().forEach(function(a){function d(b,d){a[d]=b;"translateX"===d?t.left=b+"px":t.top=b+"px";a.doTransform=!0}var f=F(a.element,"class");g=a.div=a.div||q("div",f?{className:f}:void 0,{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px",display:a.display,opacity:a.opacity,pointerEvents:a.styles&&a.styles.pointerEvents},
g||h);var t=g.style;G(a,{classSetter:function(a){return function(b){this.element.setAttribute("class",b);a.className=b}}(g),on:function(){l[0].div&&b.on.apply({element:l[0].div},arguments);return a},translateXSetter:d,translateYSetter:d});a.addedSetters||k(a)})}}else g=h;g.appendChild(a);b.added=!0;b.alignOnAdd&&b.htmlUpdateTransform();return b});return b}})});M(J,"parts/Time.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.defined,I=e.extend,G=e.isObject,H=e.objectEach,v=
e.pad,q=e.pick,C=e.splat,B=c.merge,u=c.timeUnits,w=c.win;c.Time=function(c){this.update(c,!1)};c.Time.prototype={defaultOptions:{Date:void 0,getTimezoneOffset:void 0,timezone:void 0,timezoneOffset:0,useUTC:!0},update:function(c){var r=q(c&&c.useUTC,!0),m=this;this.options=c=B(!0,this.options||{},c);this.Date=c.Date||w.Date||Date;this.timezoneOffset=(this.useUTC=r)&&c.timezoneOffset;this.getTimezoneOffset=this.timezoneOffsetFunction();(this.variableTimezone=!(r&&!c.getTimezoneOffset&&!c.timezone))||
this.timezoneOffset?(this.get=function(c,f){var b=f.getTime(),a=b-m.getTimezoneOffset(f);f.setTime(a);c=f["getUTC"+c]();f.setTime(b);return c},this.set=function(c,f,b){if("Milliseconds"===c||"Seconds"===c||"Minutes"===c&&0===f.getTimezoneOffset()%60)f["set"+c](b);else{var a=m.getTimezoneOffset(f);a=f.getTime()-a;f.setTime(a);f["setUTC"+c](b);c=m.getTimezoneOffset(f);a=f.getTime()+c;f.setTime(a)}}):r?(this.get=function(c,f){return f["getUTC"+c]()},this.set=function(c,f,b){return f["setUTC"+c](b)}):
(this.get=function(c,f){return f["get"+c]()},this.set=function(c,f,b){return f["set"+c](b)})},makeTime:function(m,r,e,A,f,b){if(this.useUTC){var a=this.Date.UTC.apply(0,arguments);var d=this.getTimezoneOffset(a);a+=d;var h=this.getTimezoneOffset(a);d!==h?a+=h-d:d-36E5!==this.getTimezoneOffset(a-36E5)||c.isSafari||(a-=36E5)}else a=(new this.Date(m,r,q(e,1),q(A,0),q(f,0),q(b,0))).getTime();return a},timezoneOffsetFunction:function(){var m=this,r=this.options,e=w.moment;if(!this.useUTC)return function(c){return 6E4*
(new Date(c)).getTimezoneOffset()};if(r.timezone){if(e)return function(c){return 6E4*-e.tz(c,r.timezone).utcOffset()};c.error(25)}return this.useUTC&&r.getTimezoneOffset?function(c){return 6E4*r.getTimezoneOffset(c)}:function(){return 6E4*(m.timezoneOffset||0)}},dateFormat:function(m,r,e){if(!F(r)||isNaN(r))return c.defaultOptions.lang.invalidDate||"";m=q(m,"%Y-%m-%d %H:%M:%S");var A=this,f=new this.Date(r),b=this.get("Hours",f),a=this.get("Day",f),d=this.get("Date",f),h=this.get("Month",f),k=this.get("FullYear",
f),l=c.defaultOptions.lang,y=l.weekdays,p=l.shortWeekdays;f=I({a:p?p[a]:y[a].substr(0,3),A:y[a],d:v(d),e:v(d,2," "),w:a,b:l.shortMonths[h],B:l.months[h],m:v(h+1),o:h+1,y:k.toString().substr(2,2),Y:k,H:v(b),k:b,I:v(b%12||12),l:b%12||12,M:v(A.get("Minutes",f)),p:12>b?"AM":"PM",P:12>b?"am":"pm",S:v(f.getSeconds()),L:v(Math.floor(r%1E3),3)},c.dateFormats);H(f,function(a,b){for(;-1!==m.indexOf("%"+b);)m=m.replace("%"+b,"function"===typeof a?a.call(A,r):a)});return e?m.substr(0,1).toUpperCase()+m.substr(1):
m},resolveDTLFormat:function(c){return G(c,!0)?c:(c=C(c),{main:c[0],from:c[1],to:c[2]})},getTimeTicks:function(c,r,e,A){var f=this,b=[],a={};var d=new f.Date(r);var h=c.unitRange,k=c.count||1,l;A=q(A,1);if(F(r)){f.set("Milliseconds",d,h>=u.second?0:k*Math.floor(f.get("Milliseconds",d)/k));h>=u.second&&f.set("Seconds",d,h>=u.minute?0:k*Math.floor(f.get("Seconds",d)/k));h>=u.minute&&f.set("Minutes",d,h>=u.hour?0:k*Math.floor(f.get("Minutes",d)/k));h>=u.hour&&f.set("Hours",d,h>=u.day?0:k*Math.floor(f.get("Hours",
d)/k));h>=u.day&&f.set("Date",d,h>=u.month?1:Math.max(1,k*Math.floor(f.get("Date",d)/k)));if(h>=u.month){f.set("Month",d,h>=u.year?0:k*Math.floor(f.get("Month",d)/k));var y=f.get("FullYear",d)}h>=u.year&&f.set("FullYear",d,y-y%k);h===u.week&&(y=f.get("Day",d),f.set("Date",d,f.get("Date",d)-y+A+(y<A?-7:0)));y=f.get("FullYear",d);A=f.get("Month",d);var p=f.get("Date",d),g=f.get("Hours",d);r=d.getTime();f.variableTimezone&&(l=e-r>4*u.month||f.getTimezoneOffset(r)!==f.getTimezoneOffset(e));r=d.getTime();
for(d=1;r<e;)b.push(r),r=h===u.year?f.makeTime(y+d*k,0):h===u.month?f.makeTime(y,A+d*k):!l||h!==u.day&&h!==u.week?l&&h===u.hour&&1<k?f.makeTime(y,A,p,g+d*k):r+h*k:f.makeTime(y,A,p+d*k*(h===u.day?1:7)),d++;b.push(r);h<=u.hour&&1E4>b.length&&b.forEach(function(b){0===b%18E5&&"000000000"===f.dateFormat("%H%M%S%L",b)&&(a[b]="day")})}b.info=I(c,{higherRanks:a,totalRange:h*k});return b}}});M(J,"parts/Options.js",[J["parts/Globals.js"]],function(c){var e=c.color,F=c.merge;c.defaultOptions={colors:"#7cb5ec #434348 #90ed7d #f7a35c #8085e9 #f15c80 #e4d354 #2b908f #f45b5b #91e8e1".split(" "),
symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",months:"January February March April May June July August September October November December".split(" "),shortMonths:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),weekdays:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),decimalPoint:".",numericSymbols:"kMGTPE".split(""),resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{},time:c.Time.prototype.defaultOptions,
chart:{styledMode:!1,borderRadius:0,colorCount:10,defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],resetZoomButton:{theme:{zIndex:6},position:{align:"right",x:-10,y:10}},width:null,height:null,borderColor:"#335cad",backgroundColor:"#ffffff",plotBorderColor:"#cccccc"},title:{text:"Chart title",align:"center",margin:15,widthAdjust:-44},subtitle:{text:"",align:"center",widthAdjust:-44},caption:{margin:15,text:"",align:"left",verticalAlign:"bottom"},plotOptions:{},labels:{style:{position:"absolute",
color:"#333333"}},legend:{enabled:!0,align:"center",alignColumns:!0,layout:"horizontal",labelFormatter:function(){return this.name},borderColor:"#999999",borderRadius:0,navigation:{activeColor:"#003399",inactiveColor:"#cccccc"},itemStyle:{color:"#333333",cursor:"pointer",fontSize:"12px",fontWeight:"bold",textOverflow:"ellipsis"},itemHoverStyle:{color:"#000000"},itemHiddenStyle:{color:"#cccccc"},shadow:!1,itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},squareSymbol:!0,symbolPadding:5,
verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",backgroundColor:"#ffffff",opacity:.5,textAlign:"center"}},tooltip:{enabled:!0,animation:c.svg,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",second:"%A, %b %e, %H:%M:%S",minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",
padding:8,snap:c.isTouchDevice?25:10,headerFormat:'<span style="font-size: 10px">{point.key}</span><br/>',pointFormat:'<span style="color:{point.color}">\u25cf</span> {series.name}: <b>{point.y}</b><br/>',backgroundColor:e("#f7f7f7").setOpacity(.85).get(),borderWidth:1,shadow:!0,style:{color:"#333333",cursor:"default",fontSize:"12px",pointerEvents:"none",whiteSpace:"nowrap"}},credits:{enabled:!0,href:"https://www.highcharts.com?credits",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",
color:"#999999",fontSize:"9px"},text:"Highcharts.com"}};c.setOptions=function(e){c.defaultOptions=F(!0,c.defaultOptions,e);(e.time||e.global)&&c.time.update(F(c.defaultOptions.global,c.defaultOptions.time,e.global,e.time));return c.defaultOptions};c.getOptions=function(){return c.defaultOptions};c.defaultPlotOptions=c.defaultOptions.plotOptions;c.time=new c.Time(F(c.defaultOptions.global,c.defaultOptions.time));c.dateFormat=function(e,G,H){return c.time.dateFormat(e,G,H)};""});M(J,"parts/Tick.js",
[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.clamp,I=e.correctFloat,G=e.defined,H=e.destroyObjectProperties,v=e.extend,q=e.isNumber,C=e.objectEach,B=e.pick,u=c.fireEvent,w=c.merge,m=c.deg2rad;c.Tick=function(c,e,m,f,b){this.axis=c;this.pos=e;this.type=m||"";this.isNewLabel=this.isNew=!0;this.parameters=b||{};this.tickmarkOffset=this.parameters.tickmarkOffset;this.options=this.parameters.options;m||f||this.addLabel()};c.Tick.prototype={addLabel:function(){var c=this,e=c.axis,
m=e.options,f=e.chart,b=e.categories,a=e.names,d=c.pos,h=B(c.options&&c.options.labels,m.labels),k=e.tickPositions,l=d===k[0],y=d===k[k.length-1];a=this.parameters.category||(b?B(b[d],a[d],d):d);var p=c.label;b=(!h.step||1===h.step)&&1===e.tickInterval;k=k.info;var g,t;if(e.isDatetimeAxis&&k){var x=f.time.resolveDTLFormat(m.dateTimeLabelFormats[!m.grid&&k.higherRanks[d]||k.unitName]);var L=x.main}c.isFirst=l;c.isLast=y;c.formatCtx={axis:e,chart:f,isFirst:l,isLast:y,dateTimeLabelFormat:L,tickPositionInfo:k,
value:e.isLog?I(e.lin2log(a)):a,pos:d};m=e.labelFormatter.call(c.formatCtx,this.formatCtx);if(t=x&&x.list)c.shortenLabel=function(){for(g=0;g<t.length;g++)if(p.attr({text:e.labelFormatter.call(v(c.formatCtx,{dateTimeLabelFormat:t[g]}))}),p.getBBox().width<e.getSlotWidth(c)-2*B(h.padding,5))return;p.attr({text:""})};b&&e._addedPlotLB&&e.isXAxis&&c.moveLabel(m,h);G(p)||c.movedLabel?p&&p.textStr!==m&&!b&&(!p.textWidth||h.style&&h.style.width||p.styles.width||p.css({width:null}),p.attr({text:m}),p.textPxLength=
p.getBBox().width):(c.label=p=c.createLabel({x:0,y:0},m,h),c.rotation=0)},moveLabel:function(c,e){var r=this,f=r.label,b=!1,a=r.axis,d=a.reversed,h=a.chart.inverted;f&&f.textStr===c?(r.movedLabel=f,b=!0,delete r.label):C(a.ticks,function(a){b||a.isNew||a===r||!a.label||a.label.textStr!==c||(r.movedLabel=a.label,b=!0,a.labelPos=r.movedLabel.xy,delete a.label)});if(!b&&(r.labelPos||f)){var k=r.labelPos||f.xy;f=h?k.x:d?0:a.width+a.left;a=h?d?a.width+a.left:0:k.y;r.movedLabel=r.createLabel({x:f,y:a},
c,e);r.movedLabel&&r.movedLabel.attr({opacity:0})}},createLabel:function(c,e,m){var f=this.axis,b=f.chart;if(c=G(e)&&m.enabled?b.renderer.text(e,c.x,c.y,m.useHTML).add(f.labelGroup):null)b.styledMode||c.css(w(m.style)),c.textPxLength=c.getBBox().width;return c},replaceMovedLabel:function(){var c=this.label,e=this.axis,m=e.reversed,f=this.axis.chart.inverted;if(c&&!this.isNew){var b=f?c.xy.x:m?e.left:e.width+e.left;m=f?m?e.width+e.top:e.top:c.xy.y;c.animate({x:b,y:m,opacity:0},void 0,c.destroy);delete this.label}e.isDirty=
!0;this.label=this.movedLabel;delete this.movedLabel},getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?"height":"width"]:0},handleOverflow:function(c){var e=this.axis,r=e.options.labels,f=c.x,b=e.chart.chartWidth,a=e.chart.spacing,d=B(e.labelLeft,Math.min(e.pos,a[3]));a=B(e.labelRight,Math.max(e.isRadial?0:e.pos+e.len,b-a[1]));var h=this.label,k=this.rotation,l={left:0,center:.5,right:1}[e.labelAlign||h.attr("align")],y=h.getBBox().width,p=e.getSlotWidth(this),g=p,t=
1,x,L={};if(k||"justify"!==B(r.overflow,"justify"))0>k&&f-l*y<d?x=Math.round(f/Math.cos(k*m)-d):0<k&&f+l*y>a&&(x=Math.round((b-f)/Math.cos(k*m)));else if(b=f+(1-l)*y,f-l*y<d?g=c.x+g*(1-l)-d:b>a&&(g=a-c.x+g*l,t=-1),g=Math.min(p,g),g<p&&"center"===e.labelAlign&&(c.x+=t*(p-g-l*(p-Math.min(y,g)))),y>g||e.autoRotation&&(h.styles||{}).width)x=g;x&&(this.shortenLabel?this.shortenLabel():(L.width=Math.floor(x),(r.style||{}).textOverflow||(L.textOverflow="ellipsis"),h.css(L)))},getPosition:function(c,e,m,
f){var b=this.axis,a=b.chart,d=f&&a.oldChartHeight||a.chartHeight;c={x:c?I(b.translate(e+m,null,null,f)+b.transB):b.left+b.offset+(b.opposite?(f&&a.oldChartWidth||a.chartWidth)-b.right-b.left:0),y:c?d-b.bottom+b.offset-(b.opposite?b.height:0):I(d-b.translate(e+m,null,null,f)-b.transB)};c.y=F(c.y,-1E5,1E5);u(this,"afterGetPosition",{pos:c});return c},getLabelPosition:function(c,e,A,f,b,a,d,h){var k=this.axis,l=k.transA,r=k.isLinked&&k.linkedParent?k.linkedParent.reversed:k.reversed,p=k.staggerLines,
g=k.tickRotCorr||{x:0,y:0},t=b.y,x=f||k.reserveSpaceDefault?0:-k.labelOffset*("center"===k.labelAlign?.5:1),L={};G(t)||(t=0===k.side?A.rotation?-8:-A.getBBox().height:2===k.side?g.y+8:Math.cos(A.rotation*m)*(g.y-A.getBBox(!1,0).height/2));c=c+b.x+x+g.x-(a&&f?a*l*(r?-1:1):0);e=e+t-(a&&!f?a*l*(r?1:-1):0);p&&(A=d/(h||1)%p,k.opposite&&(A=p-A-1),e+=k.labelOffset/p*A);L.x=c;L.y=Math.round(e);u(this,"afterGetLabelPosition",{pos:L,tickmarkOffset:a,index:d});return L},getMarkPath:function(c,e,m,f,b,a){return a.crispLine(["M",
c,e,"L",c+(b?0:-m),e+(b?m:0)],f)},renderGridLine:function(c,e,m){var f=this.axis,b=f.options,a=this.gridLine,d={},h=this.pos,k=this.type,l=B(this.tickmarkOffset,f.tickmarkOffset),r=f.chart.renderer,p=k?k+"Grid":"grid",g=b[p+"LineWidth"],t=b[p+"LineColor"];b=b[p+"LineDashStyle"];a||(f.chart.styledMode||(d.stroke=t,d["stroke-width"]=g,b&&(d.dashstyle=b)),k||(d.zIndex=1),c&&(e=0),this.gridLine=a=r.path().attr(d).addClass("highcharts-"+(k?k+"-":"")+"grid-line").add(f.gridGroup));if(a&&(m=f.getPlotLinePath({value:h+
l,lineWidth:a.strokeWidth()*m,force:"pass",old:c})))a[c||this.isNew?"attr":"animate"]({d:m,opacity:e})},renderMark:function(c,e,m){var f=this.axis,b=f.options,a=f.chart.renderer,d=this.type,h=d?d+"Tick":"tick",k=f.tickSize(h),l=this.mark,r=!l,p=c.x;c=c.y;var g=B(b[h+"Width"],!d&&f.isXAxis?1:0);b=b[h+"Color"];k&&(f.opposite&&(k[0]=-k[0]),r&&(this.mark=l=a.path().addClass("highcharts-"+(d?d+"-":"")+"tick").add(f.axisGroup),f.chart.styledMode||l.attr({stroke:b,"stroke-width":g})),l[r?"attr":"animate"]({d:this.getMarkPath(p,
c,k[0],l.strokeWidth()*m,f.horiz,a),opacity:e}))},renderLabel:function(c,e,m,f){var b=this.axis,a=b.horiz,d=b.options,h=this.label,k=d.labels,l=k.step;b=B(this.tickmarkOffset,b.tickmarkOffset);var y=!0,p=c.x;c=c.y;h&&q(p)&&(h.xy=c=this.getLabelPosition(p,c,h,a,k,b,f,l),this.isFirst&&!this.isLast&&!B(d.showFirstLabel,1)||this.isLast&&!this.isFirst&&!B(d.showLastLabel,1)?y=!1:!a||k.step||k.rotation||e||0===m||this.handleOverflow(c),l&&f%l&&(y=!1),y&&q(c.y)?(c.opacity=m,h[this.isNewLabel?"attr":"animate"](c),
this.isNewLabel=!1):(h.attr("y",-9999),this.isNewLabel=!0))},render:function(e,m,q){var f=this.axis,b=f.horiz,a=this.pos,d=B(this.tickmarkOffset,f.tickmarkOffset);a=this.getPosition(b,a,d,m);d=a.x;var h=a.y;f=b&&d===f.pos+f.len||!b&&h===f.pos?-1:1;q=B(q,1);this.isActive=!0;this.renderGridLine(m,q,f);this.renderMark(a,q,f);this.renderLabel(a,m,q,e);this.isNew=!1;c.fireEvent(this,"afterRender")},destroy:function(){H(this,this.axis)}}});M(J,"parts/Axis.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],
function(c,e){var F=e.animObject,I=e.arrayMax,G=e.arrayMin,H=e.clamp,v=e.correctFloat,q=e.defined,C=e.destroyObjectProperties,B=e.extend,u=e.isArray,w=e.isNumber,m=e.isString,r=e.objectEach,D=e.pick,A=e.relativeLength,f=e.splat,b=e.syncTimeout,a=c.addEvent,d=c.color,h=c.defaultOptions,k=c.deg2rad,l=c.fireEvent,y=c.format,p=c.getMagnitude,g=c.merge,t=c.normalizeTickInterval,x=c.removeEvent,L=c.seriesTypes,E=c.Tick;e=function(){this.init.apply(this,arguments)};B(e.prototype,{defaultOptions:{dateTimeLabelFormats:{millisecond:{main:"%H:%M:%S.%L",
range:!1},second:{main:"%H:%M:%S",range:!1},minute:{main:"%H:%M",range:!1},hour:{main:"%H:%M",range:!1},day:{main:"%e. %b"},week:{main:"%e. %b"},month:{main:"%b '%y"},year:{main:"%Y"}},endOnTick:!1,labels:{enabled:!0,indentation:10,x:0,style:{color:"#666666",cursor:"default",fontSize:"11px"}},maxPadding:.01,minorTickLength:2,minorTickPosition:"outside",minPadding:.01,showEmpty:!0,startOfWeek:1,startOnTick:!1,tickLength:10,tickPixelInterval:100,tickmarkPlacement:"between",tickPosition:"outside",title:{align:"middle",
style:{color:"#666666"}},type:"linear",minorGridLineColor:"#f2f2f2",minorGridLineWidth:1,minorTickColor:"#999999",lineColor:"#ccd6eb",lineWidth:1,gridLineColor:"#e6e6e6",tickColor:"#ccd6eb"},defaultYAxisOptions:{endOnTick:!0,maxPadding:.05,minPadding:.05,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8},startOnTick:!0,title:{rotation:270,text:"Values"},stackLabels:{allowOverlap:!1,enabled:!1,crop:!0,overflow:"justify",formatter:function(){var a=this.axis.chart.numberFormatter;return a(this.total,
-1)},style:{color:"#000000",fontSize:"11px",fontWeight:"bold",textOutline:"1px contrast"}},gridLineWidth:1,lineWidth:0},defaultLeftAxisOptions:{labels:{x:-15},title:{rotation:270}},defaultRightAxisOptions:{labels:{x:15},title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0},margin:15,title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],x:0},margin:15,title:{rotation:0}},init:function(b,d){var n=d.isX,g=this;g.chart=b;g.horiz=b.inverted&&!g.isZAxis?!n:n;g.isXAxis=
n;g.coll=g.coll||(n?"xAxis":"yAxis");l(this,"init",{userOptions:d});g.opposite=d.opposite;g.side=d.side||(g.horiz?g.opposite?0:2:g.opposite?1:3);g.setOptions(d);var z=this.options,h=z.type;g.labelFormatter=z.labels.formatter||g.defaultLabelFormatter;g.userOptions=d;g.minPixelPadding=0;g.reversed=z.reversed;g.visible=!1!==z.visible;g.zoomEnabled=!1!==z.zoomEnabled;g.hasNames="category"===h||!0===z.categories;g.categories=z.categories||g.hasNames;g.names||(g.names=[],g.names.keys={});g.plotLinesAndBandsGroups=
{};g.isLog="logarithmic"===h;g.isDatetimeAxis="datetime"===h;g.positiveValuesOnly=g.isLog&&!g.allowNegativeLog;g.isLinked=q(z.linkedTo);g.ticks={};g.labelEdge=[];g.minorTicks={};g.plotLinesAndBands=[];g.alternateBands={};g.len=0;g.minRange=g.userMinRange=z.minRange||z.maxZoom;g.range=z.range;g.offset=z.offset||0;g.stacks={};g.oldStacks={};g.stacksTouched=0;g.max=null;g.min=null;g.crosshair=D(z.crosshair,f(b.options.tooltip.crosshairs)[n?0:1],!1);d=g.options.events;-1===b.axes.indexOf(g)&&(n?b.axes.splice(b.xAxis.length,
0,g):b.axes.push(g),b[g.coll].push(g));g.series=g.series||[];b.inverted&&!g.isZAxis&&n&&"undefined"===typeof g.reversed&&(g.reversed=!0);r(d,function(b,d){c.isFunction(b)&&a(g,d,b)});g.lin2log=z.linearToLogConverter||g.lin2log;g.isLog&&(g.val2lin=g.log2lin,g.lin2val=g.lin2log);l(this,"afterInit")},setOptions:function(a){this.options=g(this.defaultOptions,"yAxis"===this.coll&&this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,this.defaultLeftAxisOptions][this.side],
g(h[this.coll],a));l(this,"afterSetOptions",{userOptions:a})},defaultLabelFormatter:function(){var a=this.axis,b=this.value,d=a.chart.time,g=a.categories,f=this.dateTimeLabelFormat,c=h.lang,t=c.numericSymbols;c=c.numericSymbolMagnitude||1E3;var l=t&&t.length,x=a.options.labels.format;a=a.isLog?Math.abs(b):a.tickInterval;var k=this.chart,p=k.numberFormatter;if(x)var e=y(x,this,k);else if(g)e=b;else if(f)e=d.dateFormat(f,b);else if(l&&1E3<=a)for(;l--&&"undefined"===typeof e;)d=Math.pow(c,l+1),a>=d&&
0===10*b%d&&null!==t[l]&&0!==b&&(e=p(b/d,-1)+t[l]);"undefined"===typeof e&&(e=1E4<=Math.abs(b)?p(b,-1):p(b,-1,void 0,""));return e},getSeriesExtremes:function(){var a=this,b=a.chart,d;l(this,"getSeriesExtremes",null,function(){a.hasVisibleSeries=!1;a.dataMin=a.dataMax=a.threshold=null;a.softThreshold=!a.isXAxis;a.buildStacks&&a.buildStacks();a.series.forEach(function(g){if(g.visible||!b.options.chart.ignoreHiddenSeries){var n=g.options,z=n.threshold;a.hasVisibleSeries=!0;a.positiveValuesOnly&&0>=
z&&(z=null);if(a.isXAxis){if(n=g.xData,n.length){d=g.getXExtremes(n);var f=d.min;var h=d.max;w(f)||f instanceof Date||(n=n.filter(w),d=g.getXExtremes(n),f=d.min,h=d.max);n.length&&(a.dataMin=Math.min(D(a.dataMin,f),f),a.dataMax=Math.max(D(a.dataMax,h),h))}}else if(g.getExtremes(),h=g.dataMax,f=g.dataMin,q(f)&&q(h)&&(a.dataMin=Math.min(D(a.dataMin,f),f),a.dataMax=Math.max(D(a.dataMax,h),h)),q(z)&&(a.threshold=z),!n.softThreshold||a.positiveValuesOnly)a.softThreshold=!1}})});l(this,"afterGetSeriesExtremes")},
translate:function(a,b,d,g,f,h){var n=this.linkedParent||this,z=1,c=0,t=g?n.oldTransA:n.transA;g=g?n.oldMin:n.min;var l=n.minPixelPadding;f=(n.isOrdinal||n.isBroken||n.isLog&&f)&&n.lin2val;t||(t=n.transA);d&&(z*=-1,c=n.len);n.reversed&&(z*=-1,c-=z*(n.sector||n.len));b?(a=(a*z+c-l)/t+g,f&&(a=n.lin2val(a))):(f&&(a=n.val2lin(a)),a=w(g)?z*(a-g)*t+c+z*l+(w(h)?t*h:0):void 0);return a},toPixels:function(a,b){return this.translate(a,!1,!this.horiz,null,!0)+(b?0:this.pos)},toValue:function(a,b){return this.translate(a-
(b?0:this.pos),!0,!this.horiz,null,!0)},getPlotLinePath:function(a){var b=this,d=b.chart,g=b.left,n=b.top,f=a.old,h=a.value,c=a.translatedValue,t=a.lineWidth,x=a.force,k,p,e,E,m=f&&d.oldChartHeight||d.chartHeight,L=f&&d.oldChartWidth||d.chartWidth,y,r=b.transB,q=function(a,b,d){if("pass"!==x&&a<b||a>d)x?a=H(a,b,d):y=!0;return a};a={value:h,lineWidth:t,old:f,force:x,acrossPanes:a.acrossPanes,translatedValue:c};l(this,"getPlotLinePath",a,function(a){c=D(c,b.translate(h,null,null,f));c=H(c,-1E5,1E5);
k=e=Math.round(c+r);p=E=Math.round(m-c-r);w(c)?b.horiz?(p=n,E=m-b.bottom,k=e=q(k,g,g+b.width)):(k=g,e=L-b.right,p=E=q(p,n,n+b.height)):(y=!0,x=!1);a.path=y&&!x?null:d.renderer.crispLine(["M",k,p,"L",e,E],t||1)});return a.path},getLinearTickPositions:function(a,b,d){var g=v(Math.floor(b/a)*a);d=v(Math.ceil(d/a)*a);var n=[],f;v(g+a)===g&&(f=20);if(this.single)return[b];for(b=g;b<=d;){n.push(b);b=v(b+a,f);if(b===z)break;var z=b}return n},getMinorTickInterval:function(){var a=this.options;return!0===
a.minorTicks?D(a.minorTickInterval,"auto"):!1===a.minorTicks?null:a.minorTickInterval},getMinorTickPositions:function(){var a=this,b=a.options,d=a.tickPositions,g=a.minorTickInterval,f=[],h=a.pointRangePadding||0,c=a.min-h;h=a.max+h;var t=h-c;if(t&&t/g<a.len/3)if(a.isLog)this.paddedTicks.forEach(function(b,d,n){d&&f.push.apply(f,a.getLogTickPositions(g,n[d-1],n[d],!0))});else if(a.isDatetimeAxis&&"auto"===this.getMinorTickInterval())f=f.concat(a.getTimeTicks(a.normalizeTimeTickInterval(g),c,h,b.startOfWeek));
else for(b=c+(d[0]-c)%g;b<=h&&b!==f[0];b+=g)f.push(b);0!==f.length&&a.trimTicks(f);return f},adjustForMinRange:function(){var a=this.options,b=this.min,d=this.max,g,f,h,c,t;this.isXAxis&&"undefined"===typeof this.minRange&&!this.isLog&&(q(a.min)||q(a.max)?this.minRange=null:(this.series.forEach(function(a){c=a.xData;for(f=t=a.xIncrement?1:c.length-1;0<f;f--)if(h=c[f]-c[f-1],"undefined"===typeof g||h<g)g=h}),this.minRange=Math.min(5*g,this.dataMax-this.dataMin)));if(d-b<this.minRange){var l=this.dataMax-
this.dataMin>=this.minRange;var x=this.minRange;var k=(x-d+b)/2;k=[b-k,D(a.min,b-k)];l&&(k[2]=this.isLog?this.log2lin(this.dataMin):this.dataMin);b=I(k);d=[b+x,D(a.max,b+x)];l&&(d[2]=this.isLog?this.log2lin(this.dataMax):this.dataMax);d=G(d);d-b<x&&(k[0]=d-x,k[1]=D(a.min,d-x),b=I(k))}this.min=b;this.max=d},getClosest:function(){var a;this.categories?a=1:this.series.forEach(function(b){var d=b.closestPointRange,g=b.visible||!b.chart.options.chart.ignoreHiddenSeries;!b.noSharedTooltip&&q(d)&&g&&(a=
q(a)?Math.min(a,d):d)});return a},nameToX:function(a){var b=u(this.categories),d=b?this.categories:this.names,g=a.options.x;a.series.requireSorting=!1;q(g)||(g=!1===this.options.uniqueNames?a.series.autoIncrement():b?d.indexOf(a.name):D(d.keys[a.name],-1));if(-1===g){if(!b)var n=d.length}else n=g;"undefined"!==typeof n&&(this.names[n]=a.name,this.names.keys[a.name]=n);return n},updateNames:function(){var a=this,b=this.names;0<b.length&&(Object.keys(b.keys).forEach(function(a){delete b.keys[a]}),b.length=
0,this.minRange=this.userMinRange,(this.series||[]).forEach(function(b){b.xIncrement=null;if(!b.points||b.isDirtyData)a.max=Math.max(a.max,b.xData.length-1),b.processData(),b.generatePoints();b.data.forEach(function(d,g){if(d&&d.options&&"undefined"!==typeof d.name){var n=a.nameToX(d);"undefined"!==typeof n&&n!==d.x&&(d.x=n,b.xData[g]=n)}})}))},setAxisTranslation:function(a){var b=this,d=b.max-b.min,g=b.axisPointRange||0,n=0,f=0,h=b.linkedParent,c=!!b.categories,t=b.transA,x=b.isXAxis;if(x||c||g){var k=
b.getClosest();h?(n=h.minPointOffset,f=h.pointRangePadding):b.series.forEach(function(a){var d=c?1:x?D(a.options.pointRange,k,0):b.axisPointRange||0,h=a.options.pointPlacement;g=Math.max(g,d);if(!b.single||c)a=L.xrange&&a instanceof L.xrange?!x:x,n=Math.max(n,a&&m(h)?0:d/2),f=Math.max(f,a&&"on"===h?0:d)});h=b.ordinalSlope&&k?b.ordinalSlope/k:1;b.minPointOffset=n*=h;b.pointRangePadding=f*=h;b.pointRange=Math.min(g,b.single&&c?1:d);x&&(b.closestPointRange=k)}a&&(b.oldTransA=t);b.translationSlope=b.transA=
t=b.staticScale||b.len/(d+f||1);b.transB=b.horiz?b.left:b.bottom;b.minPixelPadding=t*n;l(this,"afterSetAxisTranslation")},minFromRange:function(){return this.max-this.range},setTickInterval:function(a){var b=this,d=b.chart,g=b.options,n=b.isLog,f=b.isDatetimeAxis,h=b.isXAxis,x=b.isLinked,k=g.maxPadding,e=g.minPadding,E=g.tickInterval,m=g.tickPixelInterval,L=b.categories,y=w(b.threshold)?b.threshold:null,r=b.softThreshold;f||L||x||this.getTickAmount();var A=D(b.userMin,g.min);var u=D(b.userMax,g.max);
if(x){b.linkedParent=d[b.coll][g.linkedTo];var C=b.linkedParent.getExtremes();b.min=D(C.min,C.dataMin);b.max=D(C.max,C.dataMax);g.type!==b.linkedParent.options.type&&c.error(11,1,d)}else{if(!r&&q(y))if(b.dataMin>=y)C=y,e=0;else if(b.dataMax<=y){var B=y;k=0}b.min=D(A,C,b.dataMin);b.max=D(u,B,b.dataMax)}n&&(b.positiveValuesOnly&&!a&&0>=Math.min(b.min,D(b.dataMin,b.min))&&c.error(10,1,d),b.min=v(b.log2lin(b.min),16),b.max=v(b.log2lin(b.max),16));b.range&&q(b.max)&&(b.userMin=b.min=A=Math.max(b.dataMin,
b.minFromRange()),b.userMax=u=b.max,b.range=null);l(b,"foundExtremes");b.beforePadding&&b.beforePadding();b.adjustForMinRange();!(L||b.axisPointRange||b.usePercentage||x)&&q(b.min)&&q(b.max)&&(d=b.max-b.min)&&(!q(A)&&e&&(b.min-=d*e),!q(u)&&k&&(b.max+=d*k));w(b.userMin)||(w(g.softMin)&&g.softMin<b.min&&(b.min=A=g.softMin),w(g.floor)&&(b.min=Math.max(b.min,g.floor)));w(b.userMax)||(w(g.softMax)&&g.softMax>b.max&&(b.max=u=g.softMax),w(g.ceiling)&&(b.max=Math.min(b.max,g.ceiling)));r&&q(b.dataMin)&&(y=
y||0,!q(A)&&b.min<y&&b.dataMin>=y?b.min=b.options.minRange?Math.min(y,b.max-b.minRange):y:!q(u)&&b.max>y&&b.dataMax<=y&&(b.max=b.options.minRange?Math.max(y,b.min+b.minRange):y));b.tickInterval=b.min===b.max||"undefined"===typeof b.min||"undefined"===typeof b.max?1:x&&!E&&m===b.linkedParent.options.tickPixelInterval?E=b.linkedParent.tickInterval:D(E,this.tickAmount?(b.max-b.min)/Math.max(this.tickAmount-1,1):void 0,L?1:(b.max-b.min)*m/Math.max(b.len,m));h&&!a&&b.series.forEach(function(a){a.processData(b.min!==
b.oldMin||b.max!==b.oldMax)});b.setAxisTranslation(!0);b.beforeSetTickPositions&&b.beforeSetTickPositions();b.postProcessTickInterval&&(b.tickInterval=b.postProcessTickInterval(b.tickInterval));b.pointRange&&!E&&(b.tickInterval=Math.max(b.pointRange,b.tickInterval));a=D(g.minTickInterval,b.isDatetimeAxis&&b.closestPointRange);!E&&b.tickInterval<a&&(b.tickInterval=a);f||n||E||(b.tickInterval=t(b.tickInterval,null,p(b.tickInterval),D(g.allowDecimals,!(.5<b.tickInterval&&5>b.tickInterval&&1E3<b.max&&
9999>b.max)),!!this.tickAmount));this.tickAmount||(b.tickInterval=b.unsquish());this.setTickPositions()},setTickPositions:function(){var a=this.options,b=a.tickPositions;var d=this.getMinorTickInterval();var g=a.tickPositioner,f=a.startOnTick,h=a.endOnTick;this.tickmarkOffset=this.categories&&"between"===a.tickmarkPlacement&&1===this.tickInterval?.5:0;this.minorTickInterval="auto"===d&&this.tickInterval?this.tickInterval/5:d;this.single=this.min===this.max&&q(this.min)&&!this.tickAmount&&(parseInt(this.min,
10)===this.min||!1!==a.allowDecimals);this.tickPositions=d=b&&b.slice();!d&&(!this.ordinalPositions&&(this.max-this.min)/this.tickInterval>Math.max(2*this.len,200)?(d=[this.min,this.max],c.error(19,!1,this.chart)):d=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,!0):this.isLog?this.getLogTickPositions(this.tickInterval,this.min,this.max):this.getLinearTickPositions(this.tickInterval,
this.min,this.max),d.length>this.len&&(d=[d[0],d.pop()],d[0]===d[1]&&(d.length=1)),this.tickPositions=d,g&&(g=g.apply(this,[this.min,this.max])))&&(this.tickPositions=d=g);this.paddedTicks=d.slice(0);this.trimTicks(d,f,h);this.isLinked||(this.single&&2>d.length&&!this.categories&&(this.min-=.5,this.max+=.5),b||g||this.adjustTickAmount());l(this,"afterSetTickPositions")},trimTicks:function(a,b,d){var g=a[0],n=a[a.length-1],f=this.minPointOffset||0;l(this,"trimTicks");if(!this.isLinked){if(b&&-Infinity!==
g)this.min=g;else for(;this.min-f>a[0];)a.shift();if(d)this.max=n;else for(;this.max+f<a[a.length-1];)a.pop();0===a.length&&q(g)&&!this.options.tickPositions&&a.push((n+g)/2)}},alignToOthers:function(){var a={},b,d=this.options;!1===this.chart.options.chart.alignTicks||!1===d.alignTicks||!1===d.startOnTick||!1===d.endOnTick||this.isLog||this.chart[this.coll].forEach(function(d){var g=d.options;g=[d.horiz?g.left:g.top,g.width,g.height,g.pane].join();d.series.length&&(a[g]?b=!0:a[g]=1)});return b},
getTickAmount:function(){var a=this.options,b=a.tickAmount,d=a.tickPixelInterval;!q(a.tickInterval)&&this.len<d&&!this.isRadial&&!this.isLog&&a.startOnTick&&a.endOnTick&&(b=2);!b&&this.alignToOthers()&&(b=Math.ceil(this.len/d)+1);4>b&&(this.finalTickAmt=b,b=5);this.tickAmount=b},adjustTickAmount:function(){var a=this.options,b=this.tickInterval,d=this.tickPositions,g=this.tickAmount,f=this.finalTickAmt,h=d&&d.length,c=D(this.threshold,this.softThreshold?0:null),t;if(this.hasData()){if(h<g){for(t=
this.min;d.length<g;)d.length%2||t===c?d.push(v(d[d.length-1]+b)):d.unshift(v(d[0]-b));this.transA*=(h-1)/(g-1);this.min=a.startOnTick?d[0]:Math.min(this.min,d[0]);this.max=a.endOnTick?d[d.length-1]:Math.max(this.max,d[d.length-1])}else h>g&&(this.tickInterval*=2,this.setTickPositions());if(q(f)){for(b=a=d.length;b--;)(3===f&&1===b%2||2>=f&&0<b&&b<a-1)&&d.splice(b,1);this.finalTickAmt=void 0}}},setScale:function(){var a=this.series.some(function(a){return a.isDirtyData||a.isDirty||a.xAxis&&a.xAxis.isDirty}),
b;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();(b=this.len!==this.oldAxisLength)||a||this.isLinked||this.forceRedraw||this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax||this.alignToOthers()?(this.resetStacks&&this.resetStacks(),this.forceRedraw=!1,this.getSeriesExtremes(),this.setTickInterval(),this.oldUserMin=this.userMin,this.oldUserMax=this.userMax,this.isDirty||(this.isDirty=b||this.min!==this.oldMin||this.max!==this.oldMax)):this.cleanStacks&&
this.cleanStacks();l(this,"afterSetScale")},setExtremes:function(a,b,d,g,f){var n=this,h=n.chart;d=D(d,!0);n.series.forEach(function(a){delete a.kdTree});f=B(f,{min:a,max:b});l(n,"setExtremes",f,function(){n.userMin=a;n.userMax=b;n.eventArgs=f;d&&h.redraw(g)})},zoom:function(a,b){var d=this.dataMin,g=this.dataMax,n=this.options,f=Math.min(d,D(n.min,d)),h=Math.max(g,D(n.max,g));a={newMin:a,newMax:b};l(this,"zoom",a,function(a){var b=a.newMin,n=a.newMax;if(b!==this.min||n!==this.max)this.allowZoomOutside||
(q(d)&&(b<f&&(b=f),b>h&&(b=h)),q(g)&&(n<f&&(n=f),n>h&&(n=h))),this.displayBtn="undefined"!==typeof b||"undefined"!==typeof n,this.setExtremes(b,n,!1,void 0,{trigger:"zoom"});a.zoomed=!0});return a.zoomed},setAxisSize:function(){var a=this.chart,b=this.options,d=b.offsets||[0,0,0,0],g=this.horiz,f=this.width=Math.round(A(D(b.width,a.plotWidth-d[3]+d[1]),a.plotWidth)),h=this.height=Math.round(A(D(b.height,a.plotHeight-d[0]+d[2]),a.plotHeight)),c=this.top=Math.round(A(D(b.top,a.plotTop+d[0]),a.plotHeight,
a.plotTop));b=this.left=Math.round(A(D(b.left,a.plotLeft+d[3]),a.plotWidth,a.plotLeft));this.bottom=a.chartHeight-h-c;this.right=a.chartWidth-f-b;this.len=Math.max(g?f:h,0);this.pos=g?b:c},getExtremes:function(){var a=this.isLog;return{min:a?v(this.lin2log(this.min)):this.min,max:a?v(this.lin2log(this.max)):this.max,dataMin:this.dataMin,dataMax:this.dataMax,userMin:this.userMin,userMax:this.userMax}},getThreshold:function(a){var b=this.isLog,d=b?this.lin2log(this.min):this.min;b=b?this.lin2log(this.max):
this.max;null===a||-Infinity===a?a=d:Infinity===a?a=b:d>a?a=d:b<a&&(a=b);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){var b=(D(a,0)-90*this.side+720)%360;a={align:"center"};l(this,"autoLabelAlign",a,function(a){15<b&&165>b?a.align="right":195<b&&345>b&&(a.align="left")});return a.align},tickSize:function(a){var b=this.options,d=b[a+"Length"],g=D(b[a+"Width"],"tick"===a&&this.isXAxis&&!this.categories?1:0);if(g&&d){"inside"===b[a+"Position"]&&(d=-d);var f=[d,g]}a={tickSize:f};l(this,
"afterTickSize",a);return a.tickSize},labelMetrics:function(){var a=this.tickPositions&&this.tickPositions[0]||0;return this.chart.renderer.fontMetrics(this.options.labels.style&&this.options.labels.style.fontSize,this.ticks[a]&&this.ticks[a].label)},unsquish:function(){var a=this.options.labels,b=this.horiz,d=this.tickInterval,g=d,f=this.len/(((this.categories?1:0)+this.max-this.min)/d),h,c=a.rotation,t=this.labelMetrics(),l,x=Number.MAX_VALUE,p,e=this.max-this.min,E=function(a){var b=a/(f||1);b=
1<b?Math.ceil(b):1;b*d>e&&Infinity!==a&&Infinity!==f&&e&&(b=Math.ceil(e/d));return v(b*d)};b?(p=!a.staggerLines&&!a.step&&(q(c)?[c]:f<D(a.autoRotationLimit,80)&&a.autoRotation))&&p.forEach(function(a){if(a===c||a&&-90<=a&&90>=a){l=E(Math.abs(t.h/Math.sin(k*a)));var b=l+Math.abs(a/360);b<x&&(x=b,h=a,g=l)}}):a.step||(g=E(t.h));this.autoRotation=p;this.labelRotation=D(h,c);return g},getSlotWidth:function(a){var b=this.chart,d=this.horiz,g=this.options.labels,f=Math.max(this.tickPositions.length-(this.categories?
0:1),1),n=b.margin[3];return a&&a.slotWidth||d&&2>(g.step||0)&&!g.rotation&&(this.staggerLines||1)*this.len/f||!d&&(g.style&&parseInt(g.style.width,10)||n&&n-b.spacing[3]||.33*b.chartWidth)},renderUnsquish:function(){var a=this.chart,b=a.renderer,d=this.tickPositions,g=this.ticks,f=this.options.labels,h=f&&f.style||{},c=this.horiz,t=this.getSlotWidth(),l=Math.max(1,Math.round(t-2*(f.padding||5))),x={},k=this.labelMetrics(),p=f.style&&f.style.textOverflow,e=0;m(f.rotation)||(x.rotation=f.rotation||
0);d.forEach(function(a){a=g[a];a.movedLabel&&a.replaceMovedLabel();a&&a.label&&a.label.textPxLength>e&&(e=a.label.textPxLength)});this.maxLabelLength=e;if(this.autoRotation)e>l&&e>k.h?x.rotation=this.labelRotation:this.labelRotation=0;else if(t){var E=l;if(!p){var L="clip";for(l=d.length;!c&&l--;){var y=d[l];if(y=g[y].label)y.styles&&"ellipsis"===y.styles.textOverflow?y.css({textOverflow:"clip"}):y.textPxLength>t&&y.css({width:t+"px"}),y.getBBox().height>this.len/d.length-(k.h-k.f)&&(y.specificTextOverflow=
"ellipsis")}}}x.rotation&&(E=e>.5*a.chartHeight?.33*a.chartHeight:e,p||(L="ellipsis"));if(this.labelAlign=f.align||this.autoLabelAlign(this.labelRotation))x.align=this.labelAlign;d.forEach(function(a){var b=(a=g[a])&&a.label,d=h.width,f={};b&&(b.attr(x),a.shortenLabel?a.shortenLabel():E&&!d&&"nowrap"!==h.whiteSpace&&(E<b.textPxLength||"SPAN"===b.element.tagName)?(f.width=E,p||(f.textOverflow=b.specificTextOverflow||L),b.css(f)):b.styles&&b.styles.width&&!f.width&&!d&&b.css({width:null}),delete b.specificTextOverflow,
a.rotation=x.rotation)},this);this.tickRotCorr=b.rotCorr(k.b,this.labelRotation||0,0!==this.side)},hasData:function(){return this.series.some(function(a){return a.hasData()})||this.options.showEmpty&&q(this.min)&&q(this.max)},addTitle:function(a){var b=this.chart.renderer,d=this.horiz,f=this.opposite,h=this.options.title,n,c=this.chart.styledMode;this.axisTitle||((n=h.textAlign)||(n=(d?{low:"left",middle:"center",high:"right"}:{low:f?"right":"left",middle:"center",high:f?"left":"right"})[h.align]),
this.axisTitle=b.text(h.text,0,0,h.useHTML).attr({zIndex:7,rotation:h.rotation||0,align:n}).addClass("highcharts-axis-title"),c||this.axisTitle.css(g(h.style)),this.axisTitle.add(this.axisGroup),this.axisTitle.isNew=!0);c||h.style.width||this.isRadial||this.axisTitle.css({width:this.len});this.axisTitle[a?"show":"hide"](a)},generateTick:function(a){var b=this.ticks;b[a]?b[a].addLabel():b[a]=new E(this,a)},getOffset:function(){var a=this,b=a.chart,d=b.renderer,g=a.options,f=a.tickPositions,h=a.ticks,
c=a.horiz,t=a.side,x=b.inverted&&!a.isZAxis?[1,0,3,2][t]:t,k,p=0,e=0,E=g.title,m=g.labels,L=0,y=b.axisOffset;b=b.clipOffset;var A=[-1,1,1,-1][t],u=g.className,v=a.axisParent;var w=a.hasData();a.showAxis=k=w||D(g.showEmpty,!0);a.staggerLines=a.horiz&&m.staggerLines;a.axisGroup||(a.gridGroup=d.g("grid").attr({zIndex:g.gridZIndex||1}).addClass("highcharts-"+this.coll.toLowerCase()+"-grid "+(u||"")).add(v),a.axisGroup=d.g("axis").attr({zIndex:g.zIndex||2}).addClass("highcharts-"+this.coll.toLowerCase()+
" "+(u||"")).add(v),a.labelGroup=d.g("axis-labels").attr({zIndex:m.zIndex||7}).addClass("highcharts-"+a.coll.toLowerCase()+"-labels "+(u||"")).add(v));w||a.isLinked?(f.forEach(function(b,d){a.generateTick(b,d)}),a.renderUnsquish(),a.reserveSpaceDefault=0===t||2===t||{1:"left",3:"right"}[t]===a.labelAlign,D(m.reserveSpace,"center"===a.labelAlign?!0:null,a.reserveSpaceDefault)&&f.forEach(function(a){L=Math.max(h[a].getLabelSize(),L)}),a.staggerLines&&(L*=a.staggerLines),a.labelOffset=L*(a.opposite?
-1:1)):r(h,function(a,b){a.destroy();delete h[b]});if(E&&E.text&&!1!==E.enabled&&(a.addTitle(k),k&&!1!==E.reserveSpace)){a.titleOffset=p=a.axisTitle.getBBox()[c?"height":"width"];var C=E.offset;e=q(C)?0:D(E.margin,c?5:10)}a.renderLine();a.offset=A*D(g.offset,y[t]?y[t]+(g.margin||0):0);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};d=0===t?-a.labelMetrics().h:2===t?a.tickRotCorr.y:0;e=Math.abs(L)+e;L&&(e=e-d+A*(c?D(m.y,a.tickRotCorr.y+8*A):m.x));a.axisTitleMargin=D(C,e);a.getMaxLabelDimensions&&(a.maxLabelDimensions=
a.getMaxLabelDimensions(h,f));c=this.tickSize("tick");y[t]=Math.max(y[t],a.axisTitleMargin+p+A*a.offset,e,f&&f.length&&c?c[0]+A*a.offset:0);g=g.offset?0:2*Math.floor(a.axisLine.strokeWidth()/2);b[x]=Math.max(b[x],g);l(this,"afterGetOffset")},getLinePath:function(a){var b=this.chart,d=this.opposite,g=this.offset,f=this.horiz,h=this.left+(d?this.width:0)+g;g=b.chartHeight-this.bottom-(d?this.height:0)+g;d&&(a*=-1);return b.renderer.crispLine(["M",f?this.left:h,f?g:this.top,"L",f?b.chartWidth-this.right:
h,f?g:b.chartHeight-this.bottom],a)},renderLine:function(){this.axisLine||(this.axisLine=this.chart.renderer.path().addClass("highcharts-axis-line").add(this.axisGroup),this.chart.styledMode||this.axisLine.attr({stroke:this.options.lineColor,"stroke-width":this.options.lineWidth,zIndex:7}))},getTitlePosition:function(){var a=this.horiz,b=this.left,d=this.top,g=this.len,f=this.options.title,h=a?b:d,c=this.opposite,t=this.offset,x=f.x||0,k=f.y||0,p=this.axisTitle,e=this.chart.renderer.fontMetrics(f.style&&
f.style.fontSize,p);p=Math.max(p.getBBox(null,0).height-e.h-1,0);g={low:h+(a?0:g),middle:h+g/2,high:h+(a?g:0)}[f.align];b=(a?d+this.height:b)+(a?1:-1)*(c?-1:1)*this.axisTitleMargin+[-p,p,e.f,-p][this.side];a={x:a?g+x:b+(c?this.width:0)+t+x,y:a?b+k-(c?this.height:0)+t:g+k};l(this,"afterGetTitlePosition",{titlePosition:a});return a},renderMinorTick:function(a){var b=this.chart.hasRendered&&w(this.oldMin),d=this.minorTicks;d[a]||(d[a]=new E(this,a,"minor"));b&&d[a].isNew&&d[a].render(null,!0);d[a].render(null,
!1,1)},renderTick:function(a,b){var d=this.isLinked,g=this.ticks,f=this.chart.hasRendered&&w(this.oldMin);if(!d||a>=this.min&&a<=this.max)g[a]||(g[a]=new E(this,a)),f&&g[a].isNew&&g[a].render(b,!0,-1),g[a].render(b)},render:function(){var a=this,d=a.chart,g=a.options,f=a.isLog,h=a.isLinked,t=a.tickPositions,x=a.axisTitle,k=a.ticks,p=a.minorTicks,e=a.alternateBands,m=g.stackLabels,L=g.alternateGridColor,y=a.tickmarkOffset,q=a.axisLine,A=a.showAxis,u=F(d.renderer.globalAnimation),D,v;a.labelEdge.length=
0;a.overlap=!1;[k,p,e].forEach(function(a){r(a,function(a){a.isActive=!1})});if(a.hasData()||h)a.minorTickInterval&&!a.categories&&a.getMinorTickPositions().forEach(function(b){a.renderMinorTick(b)}),t.length&&(t.forEach(function(b,d){a.renderTick(b,d)}),y&&(0===a.min||a.single)&&(k[-1]||(k[-1]=new E(a,-1,null,!0)),k[-1].render(-1))),L&&t.forEach(function(b,g){v="undefined"!==typeof t[g+1]?t[g+1]+y:a.max-y;0===g%2&&b<a.max&&v<=a.max+(d.polar?-y:y)&&(e[b]||(e[b]=new c.PlotLineOrBand(a)),D=b+y,e[b].options=
{from:f?a.lin2log(D):D,to:f?a.lin2log(v):v,color:L},e[b].render(),e[b].isActive=!0)}),a._addedPlotLB||((g.plotLines||[]).concat(g.plotBands||[]).forEach(function(b){a.addPlotBandOrLine(b)}),a._addedPlotLB=!0);[k,p,e].forEach(function(a){var g,f=[],h=u.duration;r(a,function(a,b){a.isActive||(a.render(b,!1,0),a.isActive=!1,f.push(b))});b(function(){for(g=f.length;g--;)a[f[g]]&&!a[f[g]].isActive&&(a[f[g]].destroy(),delete a[f[g]])},a!==e&&d.hasRendered&&h?h:0)});q&&(q[q.isPlaced?"animate":"attr"]({d:this.getLinePath(q.strokeWidth())}),
q.isPlaced=!0,q[A?"show":"hide"](A));x&&A&&(g=a.getTitlePosition(),w(g.y)?(x[x.isNew?"attr":"animate"](g),x.isNew=!1):(x.attr("y",-9999),x.isNew=!0));m&&m.enabled&&a.renderStackTotals();a.isDirty=!1;l(this,"afterRender")},redraw:function(){this.visible&&(this.render(),this.plotLinesAndBands.forEach(function(a){a.render()}));this.series.forEach(function(a){a.isDirty=!0})},keepProps:"extKey hcEvents names series userMax userMin".split(" "),destroy:function(a){var b=this,d=b.stacks,g=b.plotLinesAndBands,
f;l(this,"destroy",{keepEvents:a});a||x(b);r(d,function(a,b){C(a);d[b]=null});[b.ticks,b.minorTicks,b.alternateBands].forEach(function(a){C(a)});if(g)for(a=g.length;a--;)g[a].destroy();"stackTotalGroup axisLine axisTitle axisGroup gridGroup labelGroup cross scrollbar".split(" ").forEach(function(a){b[a]&&(b[a]=b[a].destroy())});for(f in b.plotLinesAndBandsGroups)b.plotLinesAndBandsGroups[f]=b.plotLinesAndBandsGroups[f].destroy();r(b,function(a,d){-1===b.keepProps.indexOf(d)&&delete b[d]})},drawCrosshair:function(a,
b){var g=this.crosshair,f=D(g.snap,!0),h,c=this.cross;l(this,"drawCrosshair",{e:a,point:b});a||(a=this.cross&&this.cross.e);if(this.crosshair&&!1!==(q(b)||!f)){f?q(b)&&(h=D("colorAxis"!==this.coll?b.crosshairPos:null,this.isXAxis?b.plotX:this.len-b.plotY)):h=a&&(this.horiz?a.chartX-this.pos:this.len-a.chartY+this.pos);if(q(h)){var t={value:b&&(this.isXAxis?b.x:D(b.stackY,b.y)),translatedValue:h};this.chart.polar&&B(t,{isCrosshair:!0,chartX:a&&a.chartX,chartY:a&&a.chartY,point:b});t=this.getPlotLinePath(t)||
null}if(!q(t)){this.hideCrosshair();return}f=this.categories&&!this.isRadial;c||(this.cross=c=this.chart.renderer.path().addClass("highcharts-crosshair highcharts-crosshair-"+(f?"category ":"thin ")+g.className).attr({zIndex:D(g.zIndex,2)}).add(),this.chart.styledMode||(c.attr({stroke:g.color||(f?d("#ccd6eb").setOpacity(.25).get():"#cccccc"),"stroke-width":D(g.width,1)}).css({"pointer-events":"none"}),g.dashStyle&&c.attr({dashstyle:g.dashStyle})));c.show().attr({d:t});f&&!g.width&&c.attr({"stroke-width":this.transA});
this.cross.e=a}else this.hideCrosshair();l(this,"afterDrawCrosshair",{e:a,point:b})},hideCrosshair:function(){this.cross&&this.cross.hide();l(this,"afterHideCrosshair")}});return c.Axis=e});M(J,"parts/DateTimeAxis.js",[J["parts/Globals.js"]],function(c){var e=c.Axis,F=c.getMagnitude,I=c.normalizeTickInterval,G=c.timeUnits;e.prototype.getTimeTicks=function(){return this.chart.time.getTimeTicks.apply(this.chart.time,arguments)};e.prototype.normalizeTimeTickInterval=function(c,e){var q=e||[["millisecond",
[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1,2]],["week",[1,2]],["month",[1,2,3,4,6]],["year",null]];e=q[q.length-1];var v=G[e[0]],B=e[1],u;for(u=0;u<q.length&&!(e=q[u],v=G[e[0]],B=e[1],q[u+1]&&c<=(v*B[B.length-1]+G[q[u+1][0]])/2);u++);v===G.year&&c<5*v&&(B=[1,2,5]);c=I(c/v,B,"year"===e[0]?Math.max(F(c/v),1):1);return{unitRange:v,count:c,unitName:e[0]}}});M(J,"parts/LogarithmicAxis.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],
function(c,e){var F=e.pick;e=c.Axis;var I=c.getMagnitude,G=c.normalizeTickInterval;e.prototype.getLogTickPositions=function(c,e,q,C){var v=this.options,u=this.len,w=[];C||(this._minorAutoInterval=null);if(.5<=c)c=Math.round(c),w=this.getLinearTickPositions(c,e,q);else if(.08<=c){u=Math.floor(e);var m,r;for(v=.3<c?[1,2,4]:.15<c?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];u<q+1&&!r;u++){var D=v.length;for(m=0;m<D&&!r;m++){var A=this.log2lin(this.lin2log(u)*v[m]);A>e&&(!C||f<=q)&&"undefined"!==typeof f&&w.push(f);
f>q&&(r=!0);var f=A}}}else e=this.lin2log(e),q=this.lin2log(q),c=C?this.getMinorTickInterval():v.tickInterval,c=F("auto"===c?null:c,this._minorAutoInterval,v.tickPixelInterval/(C?5:1)*(q-e)/((C?u/this.tickPositions.length:u)||1)),c=G(c,null,I(c)),w=this.getLinearTickPositions(c,e,q).map(this.log2lin),C||(this._minorAutoInterval=c/5);C||(this.tickInterval=c);return w};e.prototype.log2lin=function(c){return Math.log(c)/Math.LN10};e.prototype.lin2log=function(c){return Math.pow(10,c)}});M(J,"parts/PlotLineOrBand.js",
[J["parts/Globals.js"],J["parts/Axis.js"],J["parts/Utilities.js"]],function(c,e,F){var I=F.arrayMax,G=F.arrayMin,H=F.defined,v=F.destroyObjectProperties,q=F.erase,C=F.extend,B=F.objectEach,u=F.pick,w=c.merge;c.PlotLineOrBand=function(c,e){this.axis=c;e&&(this.options=e,this.id=e.id)};c.PlotLineOrBand.prototype={render:function(){c.fireEvent(this,"render");var e=this,r=e.axis,q=r.horiz,A=e.options,f=A.label,b=e.label,a=A.to,d=A.from,h=A.value,k=H(d)&&H(a),l=H(h),y=e.svgElem,p=!y,g=[],t=A.color,x=u(A.zIndex,
0),L=A.events;g={"class":"highcharts-plot-"+(k?"band ":"line ")+(A.className||"")};var E={},n=r.chart.renderer,z=k?"bands":"lines";r.isLog&&(d=r.log2lin(d),a=r.log2lin(a),h=r.log2lin(h));r.chart.styledMode||(l?(g.stroke=t||"#999999",g["stroke-width"]=u(A.width,1),A.dashStyle&&(g.dashstyle=A.dashStyle)):k&&(g.fill=t||"#e6ebf5",A.borderWidth&&(g.stroke=A.borderColor,g["stroke-width"]=A.borderWidth)));E.zIndex=x;z+="-"+x;(t=r.plotLinesAndBandsGroups[z])||(r.plotLinesAndBandsGroups[z]=t=n.g("plot-"+z).attr(E).add());
p&&(e.svgElem=y=n.path().attr(g).add(t));if(l)g=r.getPlotLinePath({value:h,lineWidth:y.strokeWidth(),acrossPanes:A.acrossPanes});else if(k)g=r.getPlotBandPath(d,a,A);else return;(p||!y.d)&&g&&g.length?(y.attr({d:g}),L&&B(L,function(a,b){y.on(b,function(a){L[b].apply(e,[a])})})):y&&(g?(y.show(!0),y.animate({d:g})):y.d&&(y.hide(),b&&(e.label=b=b.destroy())));f&&(H(f.text)||H(f.formatter))&&g&&g.length&&0<r.width&&0<r.height&&!g.isFlat?(f=w({align:q&&k&&"center",x:q?!k&&4:10,verticalAlign:!q&&k&&"middle",
y:q?k?16:10:k?6:-4,rotation:q&&!k&&90},f),this.renderLabel(f,g,k,x)):b&&b.hide();return e},renderLabel:function(c,e,q,A){var f=this.label,b=this.axis.chart.renderer;f||(f={align:c.textAlign||c.align,rotation:c.rotation,"class":"highcharts-plot-"+(q?"band":"line")+"-label "+(c.className||"")},f.zIndex=A,A=this.getLabelText(c),this.label=f=b.text(A,0,0,c.useHTML).attr(f).add(),this.axis.chart.styledMode||f.css(c.style));b=e.xBounds||[e[1],e[4],q?e[6]:e[1]];e=e.yBounds||[e[2],e[5],q?e[7]:e[2]];q=G(b);
A=G(e);f.align(c,!1,{x:q,y:A,width:I(b)-q,height:I(e)-A});f.show(!0)},getLabelText:function(c){return H(c.formatter)?c.formatter.call(this):c.text},destroy:function(){q(this.axis.plotLinesAndBands,this);delete this.axis;v(this)}};C(e.prototype,{getPlotBandPath:function(c,e){var m=this.getPlotLinePath({value:e,force:!0,acrossPanes:this.options.acrossPanes}),r=this.getPlotLinePath({value:c,force:!0,acrossPanes:this.options.acrossPanes}),f=[],b=this.horiz,a=1;c=c<this.min&&e<this.min||c>this.max&&e>
this.max;if(r&&m){if(c){var d=r.toString()===m.toString();a=0}for(c=0;c<r.length;c+=6)b&&m[c+1]===r[c+1]?(m[c+1]+=a,m[c+4]+=a):b||m[c+2]!==r[c+2]||(m[c+2]+=a,m[c+5]+=a),f.push("M",r[c+1],r[c+2],"L",r[c+4],r[c+5],m[c+4],m[c+5],m[c+1],m[c+2],"z"),f.isFlat=d}return f},addPlotBand:function(c){return this.addPlotBandOrLine(c,"plotBands")},addPlotLine:function(c){return this.addPlotBandOrLine(c,"plotLines")},addPlotBandOrLine:function(e,r){var m=(new c.PlotLineOrBand(this,e)).render(),q=this.userOptions;
if(m){if(r){var f=q[r]||[];f.push(e);q[r]=f}this.plotLinesAndBands.push(m)}return m},removePlotBandOrLine:function(c){for(var e=this.plotLinesAndBands,m=this.options,A=this.userOptions,f=e.length;f--;)e[f].id===c&&e[f].destroy();[m.plotLines||[],A.plotLines||[],m.plotBands||[],A.plotBands||[]].forEach(function(b){for(f=b.length;f--;)b[f].id===c&&q(b,b[f])})},removePlotBand:function(c){this.removePlotBandOrLine(c)},removePlotLine:function(c){this.removePlotBandOrLine(c)}})});M(J,"parts/Tooltip.js",
[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.clamp,I=e.defined,G=e.discardElement,H=e.extend,v=e.isNumber,q=e.isString,C=e.pick,B=e.splat,u=e.syncTimeout;"";var w=c.doc,m=c.format,r=c.merge,D=c.timeUnits;c.Tooltip=function(){this.init.apply(this,arguments)};c.Tooltip.prototype={init:function(c,f){this.chart=c;this.options=f;this.crosshairs=[];this.now={x:0,y:0};this.isHidden=!0;this.split=f.split&&!c.inverted&&!c.polar;this.shared=f.shared||this.split;this.outside=C(f.outside,
!(!c.scrollablePixelsX&&!c.scrollablePixelsY))},cleanSplit:function(c){this.chart.series.forEach(function(f){var b=f&&f.tt;b&&(!b.isActive||c?f.tt=b.destroy():b.isActive=!1)})},applyFilter:function(){var c=this.chart;c.renderer.definition({tagName:"filter",id:"drop-shadow-"+c.index,opacity:.5,children:[{tagName:"feGaussianBlur","in":"SourceAlpha",stdDeviation:1},{tagName:"feOffset",dx:1,dy:1},{tagName:"feComponentTransfer",children:[{tagName:"feFuncA",type:"linear",slope:.3}]},{tagName:"feMerge",
children:[{tagName:"feMergeNode"},{tagName:"feMergeNode","in":"SourceGraphic"}]}]});c.renderer.definition({tagName:"style",textContent:".highcharts-tooltip-"+c.index+"{filter:url(#drop-shadow-"+c.index+")}"})},getLabel:function(){var e=this,f=this.chart.renderer,b=this.chart.styledMode,a=this.options,d="tooltip"+(I(a.className)?" "+a.className:""),h;if(!this.label){this.outside&&(this.container=h=c.doc.createElement("div"),h.className="highcharts-tooltip-container",c.css(h,{position:"absolute",top:"1px",
pointerEvents:a.style&&a.style.pointerEvents,zIndex:3}),c.doc.body.appendChild(h),this.renderer=f=new c.Renderer(h,0,0,{},void 0,void 0,f.styledMode));this.split?this.label=f.g(d):(this.label=f.label("",0,0,a.shape||"callout",null,null,a.useHTML,null,d).attr({padding:a.padding,r:a.borderRadius}),b||this.label.attr({fill:a.backgroundColor,"stroke-width":a.borderWidth}).css(a.style).shadow(a.shadow));b&&(this.applyFilter(),this.label.addClass("highcharts-tooltip-"+this.chart.index));if(e.outside&&!e.split){var k=
{x:this.label.xSetter,y:this.label.ySetter};this.label.xSetter=function(a,b){k[b].call(this.label,e.distance);h.style.left=a+"px"};this.label.ySetter=function(a,b){k[b].call(this.label,e.distance);h.style.top=a+"px"}}this.label.attr({zIndex:8}).add()}return this.label},update:function(c){this.destroy();r(!0,this.chart.options.tooltip.userOptions,c);this.init(this.chart,r(!0,this.options,c))},destroy:function(){this.label&&(this.label=this.label.destroy());this.split&&this.tt&&(this.cleanSplit(this.chart,
!0),this.tt=this.tt.destroy());this.renderer&&(this.renderer=this.renderer.destroy(),G(this.container));c.clearTimeout(this.hideTimer);c.clearTimeout(this.tooltipTimeout)},move:function(e,f,b,a){var d=this,h=d.now,k=!1!==d.options.animation&&!d.isHidden&&(1<Math.abs(e-h.x)||1<Math.abs(f-h.y)),l=d.followPointer||1<d.len;H(h,{x:k?(2*h.x+e)/3:e,y:k?(h.y+f)/2:f,anchorX:l?void 0:k?(2*h.anchorX+b)/3:b,anchorY:l?void 0:k?(h.anchorY+a)/2:a});d.getLabel().attr(h);k&&(c.clearTimeout(this.tooltipTimeout),this.tooltipTimeout=
setTimeout(function(){d&&d.move(e,f,b,a)},32))},hide:function(e){var f=this;c.clearTimeout(this.hideTimer);e=C(e,this.options.hideDelay,500);this.isHidden||(this.hideTimer=u(function(){f.getLabel()[e?"fadeOut":"hide"]();f.isHidden=!0},e))},getAnchor:function(c,f){var b=this.chart,a=b.pointer,d=b.inverted,h=b.plotTop,k=b.plotLeft,l=0,e=0,p,g;c=B(c);this.followPointer&&f?("undefined"===typeof f.chartX&&(f=a.normalize(f)),c=[f.chartX-b.plotLeft,f.chartY-h]):c[0].tooltipPos?c=c[0].tooltipPos:(c.forEach(function(a){p=
a.series.yAxis;g=a.series.xAxis;l+=a.plotX+(!d&&g?g.left-k:0);e+=(a.plotLow?(a.plotLow+a.plotHigh)/2:a.plotY)+(!d&&p?p.top-h:0)}),l/=c.length,e/=c.length,c=[d?b.plotWidth-e:l,this.shared&&!d&&1<c.length&&f?f.chartY-h:d?b.plotHeight-l:e]);return c.map(Math.round)},getPosition:function(c,f,b){var a=this.chart,d=this.distance,h={},k=a.inverted&&b.h||0,l,e=this.outside,p=e?w.documentElement.clientWidth-2*d:a.chartWidth,g=e?Math.max(w.body.scrollHeight,w.documentElement.scrollHeight,w.body.offsetHeight,
w.documentElement.offsetHeight,w.documentElement.clientHeight):a.chartHeight,t=a.pointer.getChartPosition(),x=a.containerScaling,L=function(a){return x?a*x.scaleX:a},E=function(a){return x?a*x.scaleY:a},n=function(h){var n="x"===h;return[h,n?p:g,n?c:f].concat(e?[n?L(c):E(f),n?t.left-d+L(b.plotX+a.plotLeft):t.top-d+E(b.plotY+a.plotTop),0,n?p:g]:[n?c:f,n?b.plotX+a.plotLeft:b.plotY+a.plotTop,n?a.plotLeft:a.plotTop,n?a.plotLeft+a.plotWidth:a.plotTop+a.plotHeight])},z=n("y"),m=n("x"),r=!this.followPointer&&
C(b.ttBelow,!a.inverted===!!b.negative),q=function(a,b,g,f,c,t,n){var l="y"===a?E(d):L(d),x=(g-f)/2,e=f<c-d,p=c+d+f<b,z=c-l-g+x;c=c+l-x;if(r&&p)h[a]=c;else if(!r&&e)h[a]=z;else if(e)h[a]=Math.min(n-f,0>z-k?z:z-k);else if(p)h[a]=Math.max(t,c+k+g>b?c:c+k);else return!1},A=function(a,b,g,f,c){var t;c<d||c>b-d?t=!1:h[a]=c<g/2?1:c>b-f/2?b-f-2:c-g/2;return t},u=function(a){var b=z;z=m;m=b;l=a},v=function(){!1!==q.apply(0,z)?!1!==A.apply(0,m)||l||(u(!0),v()):l?h.x=h.y=0:(u(!0),v())};(a.inverted||1<this.len)&&
u();v();return h},defaultFormatter:function(c){var f=this.points||B(this);var b=[c.tooltipFooterHeaderFormatter(f[0])];b=b.concat(c.bodyFormatter(f));b.push(c.tooltipFooterHeaderFormatter(f[0],!0));return b},refresh:function(e,f){var b=this.chart,a=this.options,d=e,h={},k=[],l=a.formatter||this.defaultFormatter;h=this.shared;var y=b.styledMode;if(a.enabled){c.clearTimeout(this.hideTimer);this.followPointer=B(d)[0].series.tooltipOptions.followPointer;var p=this.getAnchor(d,f);f=p[0];var g=p[1];!h||
d.series&&d.series.noSharedTooltip?h=d.getLabelConfig():(b.pointer.applyInactiveState(d),d.forEach(function(a){a.setState("hover");k.push(a.getLabelConfig())}),h={x:d[0].category,y:d[0].y},h.points=k,d=d[0]);this.len=k.length;b=l.call(h,this);l=d.series;this.distance=C(l.tooltipOptions.distance,16);!1===b?this.hide():(this.split?this.renderSplit(b,B(e)):(e=this.getLabel(),a.style.width&&!y||e.css({width:this.chart.spacingBox.width}),e.attr({text:b&&b.join?b.join(""):b}),e.removeClass(/highcharts-color-[\d]+/g).addClass("highcharts-color-"+
C(d.colorIndex,l.colorIndex)),y||e.attr({stroke:a.borderColor||d.color||l.color||"#666666"}),this.updatePosition({plotX:f,plotY:g,negative:d.negative,ttBelow:d.ttBelow,h:p[2]||0})),this.isHidden&&this.label&&this.label.attr({opacity:1}).show(),this.isHidden=!1);c.fireEvent(this,"refresh")}},renderSplit:function(e,f){function b(a,b,d,g,c){void 0===c&&(c=!0);d?(b=D?0:I,a=F(a-g/2,K.left,K.right-g)):(b-=B,a=c?a-g-v:a+v,a=F(a,c?a:K.left,K.right));return{x:a,y:b}}var a=this,d=a.chart,h=a.chart,k=h.chartWidth,
l=h.chartHeight,y=h.plotHeight,p=h.plotLeft,g=h.plotTop,t=h.plotWidth,x=h.pointer,L=h.renderer,E=h.scrollablePixelsX;E=void 0===E?0:E;var n=h.scrollablePixelsY,z=void 0===n?0:n;n=h.scrollingContainer;n=void 0===n?{scrollLeft:0,scrollTop:0}:n;var m=n.scrollLeft,r=n.scrollTop,u=h.styledMode,v=a.distance,w=a.options,A=a.options.positioner,K={left:E?p:0,right:E?p+t-E:k,top:z?g:0,bottom:z?g+y-z:l},R=a.getLabel(),D=!(!d.xAxis[0]||!d.xAxis[0].opposite),B=g,G=0,I=y-z;q(e)&&(e=[!1,e]);e=e.slice(0,f.length+
1).reduce(function(d,c,h){if(!1!==c&&""!==c){h=f[h-1]||{isHeader:!0,plotX:f[0].plotX,plotY:y,series:{}};var t=h.isHeader,n=t?a:h.series,l=n.tt,x=h.isHeader;var e=h.series;var k="highcharts-color-"+C(h.colorIndex,e.colorIndex,"none");l||(l={padding:w.padding,r:w.borderRadius},u||(l.fill=w.backgroundColor,l["stroke-width"]=w.borderWidth),l=L.label(null,null,null,w[x?"headerShape":"shape"]||"callout",null,null,w.useHTML).addClass(x?"highcharts-tooltip-header ":"highcharts-tooltip-box "+k).attr(l).add(R));
l.isActive=!0;l.attr({text:c});u||l.css(w.style).shadow(w.shadow).attr({stroke:w.borderColor||h.color||e.color||"#333333"});c=n.tt=l;x=c.getBBox();n=x.width+c.strokeWidth();t&&(G=x.height,I+=G,D&&(B-=G));e=h.plotX;e=void 0===e?0:e;k=h.plotY;k=void 0===k?0:k;var E=h.series;h.isHeader?(e=p+e-m,k=g+(y-z)/2):(l=E.xAxis,E=E.yAxis,e=l.pos+F(e,-v,l.len+v)-m,k=E.pos+F(k,0,E.len)-r);e=F(e,K.left-v,K.right+v);k=F(k,K.top,K.bottom);x=x.height+1;l=A?A.call(a,n,x,h):b(e,k,t,n);d.push({align:A?0:void 0,anchorX:e,
anchorY:k,boxWidth:n,point:h,rank:C(l.rank,t?1:0),size:x,target:l.y,tt:c,x:l.x})}return d},[]);!A&&e.some(function(a){return 0>a.x})&&(e=e.map(function(a){var d=b(a.anchorX,a.anchorY,a.point.isHeader,a.boxWidth,!1);return H(a,{target:d.y,x:d.x})}));a.cleanSplit();c.distribute(e,I,void 0);e.forEach(function(a){var b=a.pos;a.tt.attr({visibility:"undefined"===typeof b?"hidden":"inherit",x:a.x,y:b+B,anchorX:a.anchorX,anchorY:a.anchorY})});e=a.container;d=a.renderer;a.outside&&e&&d&&(h=R.getBBox(),d.setSize(h.width+
h.x,h.height+h.y,!1),x=x.getChartPosition(),e.style.left=x.left+"px",e.style.top=x.top+"px")},updatePosition:function(e){var f=this.chart,b=f.pointer,a=this.getLabel(),d=e.plotX+f.plotLeft,h=e.plotY+f.plotTop;b=b.getChartPosition();e=(this.options.positioner||this.getPosition).call(this,a.width,a.height,e);if(this.outside){var k=(this.options.borderWidth||0)+2*this.distance;this.renderer.setSize(a.width+k,a.height+k,!1);if(f=f.containerScaling)c.css(this.container,{transform:"scale("+f.scaleX+", "+
f.scaleY+")"}),d*=f.scaleX,h*=f.scaleY;d+=b.left-e.x;h+=b.top-e.y}this.move(Math.round(e.x),Math.round(e.y||0),d,h)},getDateFormat:function(c,f,b,a){var d=this.chart.time,h=d.dateFormat("%m-%d %H:%M:%S.%L",f),e={millisecond:15,second:12,minute:9,hour:6,day:3},l="millisecond";for(y in D){if(c===D.week&&+d.dateFormat("%w",f)===b&&"00:00:00.000"===h.substr(6)){var y="week";break}if(D[y]>c){y=l;break}if(e[y]&&h.substr(e[y])!=="01-01 00:00:00.000".substr(e[y]))break;"week"!==y&&(l=y)}if(y)var p=d.resolveDTLFormat(a[y]).main;
return p},getXDateFormat:function(c,f,b){f=f.dateTimeLabelFormats;var a=b&&b.closestPointRange;return(a?this.getDateFormat(a,c.x,b.options.startOfWeek,f):f.day)||f.year},tooltipFooterHeaderFormatter:function(e,f){var b=f?"footer":"header",a=e.series,d=a.tooltipOptions,h=d.xDateFormat,k=a.xAxis,l=k&&"datetime"===k.options.type&&v(e.key),y=d[b+"Format"];f={isFooter:f,labelConfig:e};c.fireEvent(this,"headerFormatter",f,function(b){l&&!h&&(h=this.getXDateFormat(e,d,k));l&&h&&(e.point&&e.point.tooltipDateKeys||
["key"]).forEach(function(a){y=y.replace("{point."+a+"}","{point."+a+":"+h+"}")});a.chart.styledMode&&(y=this.styledModeFormat(y));b.text=m(y,{point:e,series:a},this.chart)});return f.text},bodyFormatter:function(c){return c.map(function(c){var b=c.series.tooltipOptions;return(b[(c.point.formatPrefix||"point")+"Formatter"]||c.point.tooltipFormatter).call(c.point,b[(c.point.formatPrefix||"point")+"Format"]||"")})},styledModeFormat:function(c){return c.replace('style="font-size: 10px"','class="highcharts-header"').replace(/style="color:{(point|series)\.color}"/g,
'class="highcharts-color-{$1.colorIndex}"')}}});M(J,"parts/Pointer.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.attr,I=e.defined,G=e.extend,H=e.isNumber,v=e.isObject,q=e.objectEach,C=e.offset,B=e.pick,u=e.splat,w=c.addEvent,m=c.charts,r=c.color,D=c.css,A=c.find,f=c.fireEvent,b=c.Tooltip;c.Pointer=function(a,b){this.init(a,b)};c.Pointer.prototype={init:function(a,d){this.options=d;this.chart=a;this.runChartClick=d.chart.events&&!!d.chart.events.click;this.pinchDown=[];
this.lastValidTouch={};b&&(a.tooltip=new b(a,d.tooltip),this.followTouchMove=B(d.tooltip.followTouchMove,!0));this.setDOMEvents()},zoomOption:function(a){var b=this.chart,c=b.options.chart,f=c.zoomType||"";b=b.inverted;/touch/.test(a.type)&&(f=B(c.pinchType,f));this.zoomX=a=/x/.test(f);this.zoomY=f=/y/.test(f);this.zoomHor=a&&!b||f&&b;this.zoomVert=f&&!b||a&&b;this.hasZoom=a||f},getChartPosition:function(){var a=this.chart;a=a.scrollingContainer||a.container;return this.chartPosition||(this.chartPosition=
C(a))},normalize:function(a,b){var d=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:a;b||(b=this.getChartPosition());var c=d.pageX-b.left;b=d.pageY-b.top;if(d=this.chart.containerScaling)c/=d.scaleX,b/=d.scaleY;return G(a,{chartX:Math.round(c),chartY:Math.round(b)})},getCoordinates:function(a){var b={xAxis:[],yAxis:[]};this.chart.axes.forEach(function(d){b[d.isXAxis?"xAxis":"yAxis"].push({axis:d,value:d.toValue(a[d.horiz?"chartX":"chartY"])})});return b},findNearestKDPoint:function(a,
b,c){var d;a.forEach(function(a){var f=!(a.noSharedTooltip&&b)&&0>a.options.findNearestPointBy.indexOf("y");a=a.searchPoint(c,f);if((f=v(a,!0))&&!(f=!v(d,!0))){f=d.distX-a.distX;var h=d.dist-a.dist,g=(a.series.group&&a.series.group.zIndex)-(d.series.group&&d.series.group.zIndex);f=0<(0!==f&&b?f:0!==h?h:0!==g?g:d.series.index>a.series.index?-1:1)}f&&(d=a)});return d},getPointFromEvent:function(a){a=a.target;for(var b;a&&!b;)b=a.point,a=a.parentNode;return b},getChartCoordinatesFromPoint:function(a,
b){var d=a.series,c=d.xAxis;d=d.yAxis;var f=B(a.clientX,a.plotX),e=a.shapeArgs;if(c&&d)return b?{chartX:c.len+c.pos-f,chartY:d.len+d.pos-a.plotY}:{chartX:f+c.pos,chartY:a.plotY+d.pos};if(e&&e.x&&e.y)return{chartX:e.x,chartY:e.y}},getHoverData:function(a,b,c,f,e,y){var d,g=[];f=!(!f||!a);var h=b&&!b.stickyTracking?[b]:c.filter(function(a){return a.visible&&!(!e&&a.directTouch)&&B(a.options.enableMouseTracking,!0)&&a.stickyTracking});b=(d=f||!y?a:this.findNearestKDPoint(h,e,y))&&d.series;d&&(e&&!b.noSharedTooltip?
(h=c.filter(function(a){return a.visible&&!(!e&&a.directTouch)&&B(a.options.enableMouseTracking,!0)&&!a.noSharedTooltip}),h.forEach(function(a){var b=A(a.points,function(a){return a.x===d.x&&!a.isNull});v(b)&&(a.chart.isBoosting&&(b=a.getPoint(b)),g.push(b))})):g.push(d));return{hoverPoint:d,hoverSeries:b,hoverPoints:g}},runPointActions:function(a,b){var d=this.chart,f=d.tooltip&&d.tooltip.options.enabled?d.tooltip:void 0,e=f?f.shared:!1,y=b||d.hoverPoint,p=y&&y.series||d.hoverSeries;p=this.getHoverData(y,
p,d.series,(!a||"touchmove"!==a.type)&&(!!b||p&&p.directTouch&&this.isDirectTouch),e,a);y=p.hoverPoint;var g=p.hoverPoints;b=(p=p.hoverSeries)&&p.tooltipOptions.followPointer;e=e&&p&&!p.noSharedTooltip;if(y&&(y!==d.hoverPoint||f&&f.isHidden)){(d.hoverPoints||[]).forEach(function(a){-1===g.indexOf(a)&&a.setState()});if(d.hoverSeries!==p)p.onMouseOver();this.applyInactiveState(g);(g||[]).forEach(function(a){a.setState("hover")});d.hoverPoint&&d.hoverPoint.firePointEvent("mouseOut");if(!y.series)return;
y.firePointEvent("mouseOver");d.hoverPoints=g;d.hoverPoint=y;f&&f.refresh(e?g:y,a)}else b&&f&&!f.isHidden&&(y=f.getAnchor([{}],a),f.updatePosition({plotX:y[0],plotY:y[1]}));this.unDocMouseMove||(this.unDocMouseMove=w(d.container.ownerDocument,"mousemove",function(a){var b=m[c.hoverChartIndex];if(b)b.pointer.onDocumentMouseMove(a)}));d.axes.forEach(function(b){var d=B(b.crosshair.snap,!0),f=d?c.find(g,function(a){return a.series[b.coll]===b}):void 0;f||!d?b.drawCrosshair(a,f):b.hideCrosshair()})},
applyInactiveState:function(a){var b=[],c;(a||[]).forEach(function(a){c=a.series;b.push(c);c.linkedParent&&b.push(c.linkedParent);c.linkedSeries&&(b=b.concat(c.linkedSeries));c.navigatorSeries&&b.push(c.navigatorSeries)});this.chart.series.forEach(function(a){-1===b.indexOf(a)?a.setState("inactive",!0):a.options.inactiveOtherPoints&&a.setAllPointsToState("inactive")})},reset:function(a,b){var d=this.chart,c=d.hoverSeries,f=d.hoverPoint,e=d.hoverPoints,p=d.tooltip,g=p&&p.shared?e:f;a&&g&&u(g).forEach(function(b){b.series.isCartesian&&
"undefined"===typeof b.plotX&&(a=!1)});if(a)p&&g&&u(g).length&&(p.refresh(g),p.shared&&e?e.forEach(function(a){a.setState(a.state,!0);a.series.isCartesian&&(a.series.xAxis.crosshair&&a.series.xAxis.drawCrosshair(null,a),a.series.yAxis.crosshair&&a.series.yAxis.drawCrosshair(null,a))}):f&&(f.setState(f.state,!0),d.axes.forEach(function(a){a.crosshair&&f.series[a.coll]===a&&a.drawCrosshair(null,f)})));else{if(f)f.onMouseOut();e&&e.forEach(function(a){a.setState()});if(c)c.onMouseOut();p&&p.hide(b);
this.unDocMouseMove&&(this.unDocMouseMove=this.unDocMouseMove());d.axes.forEach(function(a){a.hideCrosshair()});this.hoverX=d.hoverPoints=d.hoverPoint=null}},scaleGroups:function(a,b){var d=this.chart,c;d.series.forEach(function(f){c=a||f.getPlotBox();f.xAxis&&f.xAxis.zoomEnabled&&f.group&&(f.group.attr(c),f.markerGroup&&(f.markerGroup.attr(c),f.markerGroup.clip(b?d.clipRect:null)),f.dataLabelsGroup&&f.dataLabelsGroup.attr(c))});d.clipRect.attr(b||d.clipBox)},dragStart:function(a){var b=this.chart;
b.mouseIsDown=a.type;b.cancelClick=!1;b.mouseDownX=this.mouseDownX=a.chartX;b.mouseDownY=this.mouseDownY=a.chartY},drag:function(a){var b=this.chart,c=b.options.chart,f=a.chartX,e=a.chartY,m=this.zoomHor,p=this.zoomVert,g=b.plotLeft,t=b.plotTop,x=b.plotWidth,L=b.plotHeight,E=this.selectionMarker,n=this.mouseDownX,z=this.mouseDownY,q=v(c.panning)?c.panning&&c.panning.enabled:c.panning,u=c.panKey&&a[c.panKey+"Key"];if(!E||!E.touch)if(f<g?f=g:f>g+x&&(f=g+x),e<t?e=t:e>t+L&&(e=t+L),this.hasDragged=Math.sqrt(Math.pow(n-
f,2)+Math.pow(z-e,2)),10<this.hasDragged){var w=b.isInsidePlot(n-g,z-t);b.hasCartesianSeries&&(this.zoomX||this.zoomY)&&w&&!u&&!E&&(this.selectionMarker=E=b.renderer.rect(g,t,m?1:x,p?1:L,0).attr({"class":"highcharts-selection-marker",zIndex:7}).add(),b.styledMode||E.attr({fill:c.selectionMarkerFill||r("#335cad").setOpacity(.25).get()}));E&&m&&(f-=n,E.attr({width:Math.abs(f),x:(0<f?0:f)+n}));E&&p&&(f=e-z,E.attr({height:Math.abs(f),y:(0<f?0:f)+z}));w&&!E&&q&&b.pan(a,c.panning)}},drop:function(a){var b=
this,c=this.chart,e=this.hasPinched;if(this.selectionMarker){var l={originalEvent:a,xAxis:[],yAxis:[]},m=this.selectionMarker,p=m.attr?m.attr("x"):m.x,g=m.attr?m.attr("y"):m.y,t=m.attr?m.attr("width"):m.width,x=m.attr?m.attr("height"):m.height,L;if(this.hasDragged||e)c.axes.forEach(function(d){if(d.zoomEnabled&&I(d.min)&&(e||b[{xAxis:"zoomX",yAxis:"zoomY"}[d.coll]])){var c=d.horiz,f="touchend"===a.type?d.minPixelPadding:0,h=d.toValue((c?p:g)+f);c=d.toValue((c?p+t:g+x)-f);l[d.coll].push({axis:d,min:Math.min(h,
c),max:Math.max(h,c)});L=!0}}),L&&f(c,"selection",l,function(a){c.zoom(G(a,e?{animation:!1}:null))});H(c.index)&&(this.selectionMarker=this.selectionMarker.destroy());e&&this.scaleGroups()}c&&H(c.index)&&(D(c.container,{cursor:c._cursor}),c.cancelClick=10<this.hasDragged,c.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=[])},onContainerMouseDown:function(a){a=this.normalize(a);2!==a.button&&(this.zoomOption(a),a.preventDefault&&a.preventDefault(),this.dragStart(a))},onDocumentMouseUp:function(a){m[c.hoverChartIndex]&&
m[c.hoverChartIndex].pointer.drop(a)},onDocumentMouseMove:function(a){var b=this.chart,c=this.chartPosition;a=this.normalize(a,c);!c||this.inClass(a.target,"highcharts-tracker")||b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)||this.reset()},onContainerMouseLeave:function(a){var b=m[c.hoverChartIndex];b&&(a.relatedTarget||a.toElement)&&(b.pointer.reset(),b.pointer.chartPosition=void 0)},onContainerMouseMove:function(a){var b=this.chart;I(c.hoverChartIndex)&&m[c.hoverChartIndex]&&m[c.hoverChartIndex].mouseIsDown||
(c.hoverChartIndex=b.index);a=this.normalize(a);a.preventDefault||(a.returnValue=!1);"mousedown"===b.mouseIsDown&&this.drag(a);!this.inClass(a.target,"highcharts-tracker")&&!b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)||b.openMenu||this.runPointActions(a)},inClass:function(a,b){for(var d;a;){if(d=F(a,"class")){if(-1!==d.indexOf(b))return!0;if(-1!==d.indexOf("highcharts-container"))return!1}a=a.parentNode}},onTrackerMouseOut:function(a){var b=this.chart.hoverSeries;a=a.relatedTarget||a.toElement;
this.isDirectTouch=!1;if(!(!b||!a||b.stickyTracking||this.inClass(a,"highcharts-tooltip")||this.inClass(a,"highcharts-series-"+b.index)&&this.inClass(a,"highcharts-tracker")))b.onMouseOut()},onContainerClick:function(a){var b=this.chart,c=b.hoverPoint,e=b.plotLeft,l=b.plotTop;a=this.normalize(a);b.cancelClick||(c&&this.inClass(a.target,"highcharts-tracker")?(f(c.series,"click",G(a,{point:c})),b.hoverPoint&&c.firePointEvent("click",a)):(G(a,this.getCoordinates(a)),b.isInsidePlot(a.chartX-e,a.chartY-
l)&&f(b,"click",a)))},setDOMEvents:function(){var a=this,b=a.chart.container,f=b.ownerDocument;b.onmousedown=function(b){a.onContainerMouseDown(b)};b.onmousemove=function(b){a.onContainerMouseMove(b)};b.onclick=function(b){a.onContainerClick(b)};this.unbindContainerMouseLeave=w(b,"mouseleave",a.onContainerMouseLeave);c.unbindDocumentMouseUp||(c.unbindDocumentMouseUp=w(f,"mouseup",a.onDocumentMouseUp));c.hasTouch&&(w(b,"touchstart",function(b){a.onContainerTouchStart(b)}),w(b,"touchmove",function(b){a.onContainerTouchMove(b)}),
c.unbindDocumentTouchEnd||(c.unbindDocumentTouchEnd=w(f,"touchend",a.onDocumentTouchEnd)))},destroy:function(){var a=this;a.unDocMouseMove&&a.unDocMouseMove();this.unbindContainerMouseLeave();c.chartCount||(c.unbindDocumentMouseUp&&(c.unbindDocumentMouseUp=c.unbindDocumentMouseUp()),c.unbindDocumentTouchEnd&&(c.unbindDocumentTouchEnd=c.unbindDocumentTouchEnd()));clearInterval(a.tooltipTimeout);q(a,function(b,c){a[c]=null})}}});M(J,"parts/TouchPointer.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],
function(c,e){var F=e.extend,I=e.pick,G=c.charts,H=c.noop;F(c.Pointer.prototype,{pinchTranslate:function(c,e,C,B,u,w){this.zoomHor&&this.pinchTranslateDirection(!0,c,e,C,B,u,w);this.zoomVert&&this.pinchTranslateDirection(!1,c,e,C,B,u,w)},pinchTranslateDirection:function(c,e,C,B,u,w,m,r){var q=this.chart,v=c?"x":"y",f=c?"X":"Y",b="chart"+f,a=c?"width":"height",d=q["plot"+(c?"Left":"Top")],h,k,l=r||1,y=q.inverted,p=q.bounds[c?"h":"v"],g=1===e.length,t=e[0][b],x=C[0][b],L=!g&&e[1][b],E=!g&&C[1][b];C=
function(){!g&&20<Math.abs(t-L)&&(l=r||Math.abs(x-E)/Math.abs(t-L));k=(d-x)/l+t;h=q["plot"+(c?"Width":"Height")]/l};C();e=k;if(e<p.min){e=p.min;var n=!0}else e+h>p.max&&(e=p.max-h,n=!0);n?(x-=.8*(x-m[v][0]),g||(E-=.8*(E-m[v][1])),C()):m[v]=[x,E];y||(w[v]=k-d,w[a]=h);w=y?1/l:l;u[a]=h;u[v]=e;B[y?c?"scaleY":"scaleX":"scale"+f]=l;B["translate"+f]=w*d+(x-w*t)},pinch:function(c){var e=this,v=e.chart,B=e.pinchDown,u=c.touches,w=u.length,m=e.lastValidTouch,r=e.hasZoom,D=e.selectionMarker,A={},f=1===w&&(e.inClass(c.target,
"highcharts-tracker")&&v.runTrackerClick||e.runChartClick),b={};1<w&&(e.initiated=!0);r&&e.initiated&&!f&&c.preventDefault();[].map.call(u,function(a){return e.normalize(a)});"touchstart"===c.type?([].forEach.call(u,function(a,b){B[b]={chartX:a.chartX,chartY:a.chartY}}),m.x=[B[0].chartX,B[1]&&B[1].chartX],m.y=[B[0].chartY,B[1]&&B[1].chartY],v.axes.forEach(function(a){if(a.zoomEnabled){var b=v.bounds[a.horiz?"h":"v"],c=a.minPixelPadding,f=a.toPixels(Math.min(I(a.options.min,a.dataMin),a.dataMin)),
e=a.toPixels(Math.max(I(a.options.max,a.dataMax),a.dataMax)),m=Math.max(f,e);b.min=Math.min(a.pos,Math.min(f,e)-c);b.max=Math.max(a.pos+a.len,m+c)}}),e.res=!0):e.followTouchMove&&1===w?this.runPointActions(e.normalize(c)):B.length&&(D||(e.selectionMarker=D=F({destroy:H,touch:!0},v.plotBox)),e.pinchTranslate(B,u,A,D,b,m),e.hasPinched=r,e.scaleGroups(A,b),e.res&&(e.res=!1,this.reset(!1,0)))},touch:function(e,q){var v=this.chart,B;if(v.index!==c.hoverChartIndex)this.onContainerMouseLeave({relatedTarget:!0});
c.hoverChartIndex=v.index;if(1===e.touches.length)if(e=this.normalize(e),(B=v.isInsidePlot(e.chartX-v.plotLeft,e.chartY-v.plotTop))&&!v.openMenu){q&&this.runPointActions(e);if("touchmove"===e.type){q=this.pinchDown;var u=q[0]?4<=Math.sqrt(Math.pow(q[0].chartX-e.chartX,2)+Math.pow(q[0].chartY-e.chartY,2)):!1}I(u,!0)&&this.pinch(e)}else q&&this.reset();else 2===e.touches.length&&this.pinch(e)},onContainerTouchStart:function(c){this.zoomOption(c);this.touch(c,!0)},onContainerTouchMove:function(c){this.touch(c)},
onDocumentTouchEnd:function(e){G[c.hoverChartIndex]&&G[c.hoverChartIndex].pointer.drop(e)}})});M(J,"parts/MSPointer.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.extend,I=e.objectEach;e=e.wrap;var G=c.addEvent,H=c.charts,v=c.css,q=c.doc,C=c.noop,B=c.Pointer,u=c.removeEvent,w=c.win;if(!c.hasTouch&&(w.PointerEvent||w.MSPointerEvent)){var m={},r=!!w.PointerEvent,D=function(){var c=[];c.item=function(b){return this[b]};I(m,function(b){c.push({pageX:b.pageX,pageY:b.pageY,target:b.target})});
return c},A=function(f,b,a,d){"touch"!==f.pointerType&&f.pointerType!==f.MSPOINTER_TYPE_TOUCH||!H[c.hoverChartIndex]||(d(f),d=H[c.hoverChartIndex].pointer,d[b]({type:a,target:f.currentTarget,preventDefault:C,touches:D()}))};F(B.prototype,{onContainerPointerDown:function(c){A(c,"onContainerTouchStart","touchstart",function(b){m[b.pointerId]={pageX:b.pageX,pageY:b.pageY,target:b.currentTarget}})},onContainerPointerMove:function(c){A(c,"onContainerTouchMove","touchmove",function(b){m[b.pointerId]={pageX:b.pageX,
pageY:b.pageY};m[b.pointerId].target||(m[b.pointerId].target=b.currentTarget)})},onDocumentPointerUp:function(c){A(c,"onDocumentTouchEnd","touchend",function(b){delete m[b.pointerId]})},batchMSEvents:function(c){c(this.chart.container,r?"pointerdown":"MSPointerDown",this.onContainerPointerDown);c(this.chart.container,r?"pointermove":"MSPointerMove",this.onContainerPointerMove);c(q,r?"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});e(B.prototype,"init",function(c,b,a){c.call(this,b,a);this.hasZoom&&
v(b.container,{"-ms-touch-action":"none","touch-action":"none"})});e(B.prototype,"setDOMEvents",function(c){c.apply(this);(this.hasZoom||this.followTouchMove)&&this.batchMSEvents(G)});e(B.prototype,"destroy",function(c){this.batchMSEvents(u);c.call(this)})}});M(J,"parts/Legend.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.defined,I=e.discardElement,G=e.isNumber,H=e.pick,v=e.relativeLength,q=e.setAnimation,C=e.syncTimeout;e=e.wrap;var B=c.addEvent,u=c.css,w=c.fireEvent,
m=c.isFirefox,r=c.marginNames,D=c.merge,A=c.stableSort,f=c.win;c.Legend=function(b,a){this.init(b,a)};c.Legend.prototype={init:function(b,a){this.chart=b;this.setOptions(a);a.enabled&&(this.render(),B(this.chart,"endResize",function(){this.legend.positionCheckboxes()}),this.proximate?this.unchartrender=B(this.chart,"render",function(){this.legend.proximatePositions();this.legend.positionItems()}):this.unchartrender&&this.unchartrender())},setOptions:function(b){var a=H(b.padding,8);this.options=b;
this.chart.styledMode||(this.itemStyle=b.itemStyle,this.itemHiddenStyle=D(this.itemStyle,b.itemHiddenStyle));this.itemMarginTop=b.itemMarginTop||0;this.itemMarginBottom=b.itemMarginBottom||0;this.padding=a;this.initialItemY=a-5;this.symbolWidth=H(b.symbolWidth,16);this.pages=[];this.proximate="proximate"===b.layout&&!this.chart.inverted},update:function(b,a){var d=this.chart;this.setOptions(D(!0,this.options,b));this.destroy();d.isDirtyLegend=d.isDirtyBox=!0;H(a,!0)&&d.redraw();w(this,"afterUpdate")},
colorizeItem:function(b,a){b.legendGroup[a?"removeClass":"addClass"]("highcharts-legend-item-hidden");if(!this.chart.styledMode){var d=this.options,c=b.legendItem,f=b.legendLine,e=b.legendSymbol,m=this.itemHiddenStyle.color;d=a?d.itemStyle.color:m;var p=a?b.color||m:m,g=b.options&&b.options.marker,t={fill:p};c&&c.css({fill:d,color:d});f&&f.attr({stroke:p});e&&(g&&e.isMarker&&(t=b.pointAttribs(),a||(t.stroke=t.fill=m)),e.attr(t))}w(this,"afterColorizeItem",{item:b,visible:a})},positionItems:function(){this.allItems.forEach(this.positionItem,
this);this.chart.isResizing||this.positionCheckboxes()},positionItem:function(b){var a=this.options,d=a.symbolPadding;a=!a.rtl;var c=b._legendItemPos,f=c[0];c=c[1];var e=b.checkbox;if((b=b.legendGroup)&&b.element)b[F(b.translateY)?"animate":"attr"]({translateX:a?f:this.legendWidth-f-2*d-4,translateY:c});e&&(e.x=f,e.y=c)},destroyItem:function(b){var a=b.checkbox;["legendItem","legendLine","legendSymbol","legendGroup"].forEach(function(a){b[a]&&(b[a]=b[a].destroy())});a&&I(b.checkbox)},destroy:function(){function b(a){this[a]&&
(this[a]=this[a].destroy())}this.getAllItems().forEach(function(a){["legendItem","legendGroup"].forEach(b,a)});"clipRect up down pager nav box title group".split(" ").forEach(b,this);this.display=null},positionCheckboxes:function(){var b=this.group&&this.group.alignAttr,a=this.clipHeight||this.legendHeight,d=this.titleHeight;if(b){var c=b.translateY;this.allItems.forEach(function(f){var e=f.checkbox;if(e){var h=c+d+e.y+(this.scrollOffset||0)+3;u(e,{left:b.translateX+f.checkboxOffset+e.x-20+"px",top:h+
"px",display:this.proximate||h>c-6&&h<c+a-6?"":"none"})}},this)}},renderTitle:function(){var b=this.options,a=this.padding,d=b.title,c=0;d.text&&(this.title||(this.title=this.chart.renderer.label(d.text,a-3,a-4,null,null,null,b.useHTML,null,"legend-title").attr({zIndex:1}),this.chart.styledMode||this.title.css(d.style),this.title.add(this.group)),d.width||this.title.css({width:this.maxLegendWidth+"px"}),b=this.title.getBBox(),c=b.height,this.offsetWidth=b.width,this.contentGroup.attr({translateY:c}));
this.titleHeight=c},setText:function(b){var a=this.options;b.legendItem.attr({text:a.labelFormat?c.format(a.labelFormat,b,this.chart):a.labelFormatter.call(b)})},renderItem:function(b){var a=this.chart,d=a.renderer,c=this.options,f=this.symbolWidth,e=c.symbolPadding,m=this.itemStyle,p=this.itemHiddenStyle,g="horizontal"===c.layout?H(c.itemDistance,20):0,t=!c.rtl,x=b.legendItem,L=!b.series,E=!L&&b.series.drawLegendSymbol?b.series:b,n=E.options;n=this.createCheckboxForItem&&n&&n.showCheckbox;g=f+e+
g+(n?20:0);var z=c.useHTML,r=b.options.className;x||(b.legendGroup=d.g("legend-item").addClass("highcharts-"+E.type+"-series highcharts-color-"+b.colorIndex+(r?" "+r:"")+(L?" highcharts-series-"+b.index:"")).attr({zIndex:1}).add(this.scrollGroup),b.legendItem=x=d.text("",t?f+e:-e,this.baseline||0,z),a.styledMode||x.css(D(b.visible?m:p)),x.attr({align:t?"left":"right",zIndex:2}).add(b.legendGroup),this.baseline||(this.fontMetrics=d.fontMetrics(a.styledMode?12:m.fontSize,x),this.baseline=this.fontMetrics.f+
3+this.itemMarginTop,x.attr("y",this.baseline)),this.symbolHeight=c.symbolHeight||this.fontMetrics.f,E.drawLegendSymbol(this,b),this.setItemEvents&&this.setItemEvents(b,x,z));n&&!b.checkbox&&this.createCheckboxForItem(b);this.colorizeItem(b,b.visible);!a.styledMode&&m.width||x.css({width:(c.itemWidth||this.widthOption||a.spacingBox.width)-g});this.setText(b);a=x.getBBox();b.itemWidth=b.checkboxOffset=c.itemWidth||b.legendItemWidth||a.width+g;this.maxItemWidth=Math.max(this.maxItemWidth,b.itemWidth);
this.totalItemWidth+=b.itemWidth;this.itemHeight=b.itemHeight=Math.round(b.legendItemHeight||a.height||this.symbolHeight)},layoutItem:function(b){var a=this.options,d=this.padding,c="horizontal"===a.layout,f=b.itemHeight,e=this.itemMarginBottom,m=this.itemMarginTop,p=c?H(a.itemDistance,20):0,g=this.maxLegendWidth;a=a.alignColumns&&this.totalItemWidth>g?this.maxItemWidth:b.itemWidth;c&&this.itemX-d+a>g&&(this.itemX=d,this.lastLineHeight&&(this.itemY+=m+this.lastLineHeight+e),this.lastLineHeight=0);
this.lastItemY=m+this.itemY+e;this.lastLineHeight=Math.max(f,this.lastLineHeight);b._legendItemPos=[this.itemX,this.itemY];c?this.itemX+=a:(this.itemY+=m+f+e,this.lastLineHeight=f);this.offsetWidth=this.widthOption||Math.max((c?this.itemX-d-(b.checkbox?0:p):a)+d,this.offsetWidth)},getAllItems:function(){var b=[];this.chart.series.forEach(function(a){var d=a&&a.options;a&&H(d.showInLegend,F(d.linkedTo)?!1:void 0,!0)&&(b=b.concat(a.legendItems||("point"===d.legendType?a.data:a)))});w(this,"afterGetAllItems",
{allItems:b});return b},getAlignment:function(){var b=this.options;return this.proximate?b.align.charAt(0)+"tv":b.floating?"":b.align.charAt(0)+b.verticalAlign.charAt(0)+b.layout.charAt(0)},adjustMargins:function(b,a){var d=this.chart,c=this.options,f=this.getAlignment();f&&[/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/].forEach(function(e,h){e.test(f)&&!F(b[h])&&(d[r[h]]=Math.max(d[r[h]],d.legend[(h+1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][h]*c[h%2?"x":"y"]+H(c.margin,12)+a[h]+
(d.titleOffset[h]||0)))})},proximatePositions:function(){var b=this.chart,a=[],d="left"===this.options.align;this.allItems.forEach(function(f){var e=d;if(f.yAxis&&f.points){f.xAxis.options.reversed&&(e=!e);var h=c.find(e?f.points:f.points.slice(0).reverse(),function(a){return G(a.plotY)});e=this.itemMarginTop+f.legendItem.getBBox().height+this.itemMarginBottom;var m=f.yAxis.top-b.plotTop;f.visible?(h=h?h.plotY:f.yAxis.height,h+=m-.3*e):h=m+f.yAxis.height;a.push({target:h,size:e,item:f})}},this);c.distribute(a,
b.plotHeight);a.forEach(function(a){a.item._legendItemPos[1]=b.plotTop-b.spacing[0]+a.pos})},render:function(){var b=this.chart,a=b.renderer,d=this.group,c,f=this.box,e=this.options,m=this.padding;this.itemX=m;this.itemY=this.initialItemY;this.lastItemY=this.offsetWidth=0;this.widthOption=v(e.width,b.spacingBox.width-m);var p=b.spacingBox.width-2*m-e.x;-1<["rm","lm"].indexOf(this.getAlignment().substring(0,2))&&(p/=2);this.maxLegendWidth=this.widthOption||p;d||(this.group=d=a.g("legend").attr({zIndex:7}).add(),
this.contentGroup=a.g().attr({zIndex:1}).add(d),this.scrollGroup=a.g().add(this.contentGroup));this.renderTitle();p=this.getAllItems();A(p,function(a,b){return(a.options&&a.options.legendIndex||0)-(b.options&&b.options.legendIndex||0)});e.reversed&&p.reverse();this.allItems=p;this.display=c=!!p.length;this.itemHeight=this.totalItemWidth=this.maxItemWidth=this.lastLineHeight=0;p.forEach(this.renderItem,this);p.forEach(this.layoutItem,this);p=(this.widthOption||this.offsetWidth)+m;var g=this.lastItemY+
this.lastLineHeight+this.titleHeight;g=this.handleOverflow(g);g+=m;f||(this.box=f=a.rect().addClass("highcharts-legend-box").attr({r:e.borderRadius}).add(d),f.isNew=!0);b.styledMode||f.attr({stroke:e.borderColor,"stroke-width":e.borderWidth||0,fill:e.backgroundColor||"none"}).shadow(e.shadow);0<p&&0<g&&(f[f.isNew?"attr":"animate"](f.crisp.call({},{x:0,y:0,width:p,height:g},f.strokeWidth())),f.isNew=!1);f[c?"show":"hide"]();b.styledMode&&"none"===d.getStyle("display")&&(p=g=0);this.legendWidth=p;this.legendHeight=
g;c&&(a=b.spacingBox,f=a.y,/(lth|ct|rth)/.test(this.getAlignment())&&0<b.titleOffset[0]?f+=b.titleOffset[0]:/(lbh|cb|rbh)/.test(this.getAlignment())&&0<b.titleOffset[2]&&(f-=b.titleOffset[2]),f!==a.y&&(a=D(a,{y:f})),d.align(D(e,{width:p,height:g,verticalAlign:this.proximate?"top":e.verticalAlign}),!0,a));this.proximate||this.positionItems();w(this,"afterRender")},handleOverflow:function(b){var a=this,d=this.chart,c=d.renderer,f=this.options,e=f.y,m=this.padding;e=d.spacingBox.height+("top"===f.verticalAlign?
-e:e)-m;var p=f.maxHeight,g,t=this.clipRect,x=f.navigation,L=H(x.animation,!0),E=x.arrowSize||12,n=this.nav,z=this.pages,r,q=this.allItems,u=function(b){"number"===typeof b?t.attr({height:b}):t&&(a.clipRect=t.destroy(),a.contentGroup.clip());a.contentGroup.div&&(a.contentGroup.div.style.clip=b?"rect("+m+"px,9999px,"+(m+b)+"px,0)":"auto")},w=function(b){a[b]=c.circle(0,0,1.3*E).translate(E/2,E/2).add(n);d.styledMode||a[b].attr("fill","rgba(0,0,0,0.0001)");return a[b]};"horizontal"!==f.layout||"middle"===
f.verticalAlign||f.floating||(e/=2);p&&(e=Math.min(e,p));z.length=0;b>e&&!1!==x.enabled?(this.clipHeight=g=Math.max(e-20-this.titleHeight-m,0),this.currentPage=H(this.currentPage,1),this.fullHeight=b,q.forEach(function(a,b){var d=a._legendItemPos[1],c=Math.round(a.legendItem.getBBox().height),f=z.length;if(!f||d-z[f-1]>g&&(r||d)!==z[f-1])z.push(r||d),f++;a.pageIx=f-1;r&&(q[b-1].pageIx=f-1);b===q.length-1&&d+c-z[f-1]>g&&d!==r&&(z.push(d),a.pageIx=f);d!==r&&(r=d)}),t||(t=a.clipRect=c.clipRect(0,m,9999,
0),a.contentGroup.clip(t)),u(g),n||(this.nav=n=c.g().attr({zIndex:1}).add(this.group),this.up=c.symbol("triangle",0,0,E,E).add(n),w("upTracker").on("click",function(){a.scroll(-1,L)}),this.pager=c.text("",15,10).addClass("highcharts-legend-navigation"),d.styledMode||this.pager.css(x.style),this.pager.add(n),this.down=c.symbol("triangle-down",0,0,E,E).add(n),w("downTracker").on("click",function(){a.scroll(1,L)})),a.scroll(0),b=e):n&&(u(),this.nav=n.destroy(),this.scrollGroup.attr({translateY:1}),this.clipHeight=
0);return b},scroll:function(b,a){var d=this,f=this.chart,e=this.pages,l=e.length,m=this.currentPage+b;b=this.clipHeight;var p=this.options.navigation,g=this.pager,t=this.padding;m>l&&(m=l);0<m&&("undefined"!==typeof a&&q(a,f),this.nav.attr({translateX:t,translateY:b+this.padding+7+this.titleHeight,visibility:"visible"}),[this.up,this.upTracker].forEach(function(a){a.attr({"class":1===m?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"})}),g.attr({text:m+"/"+l}),[this.down,this.downTracker].forEach(function(a){a.attr({x:18+
this.pager.getBBox().width,"class":m===l?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"})},this),f.styledMode||(this.up.attr({fill:1===m?p.inactiveColor:p.activeColor}),this.upTracker.css({cursor:1===m?"default":"pointer"}),this.down.attr({fill:m===l?p.inactiveColor:p.activeColor}),this.downTracker.css({cursor:m===l?"default":"pointer"})),this.scrollOffset=-e[m-1]+this.initialItemY,this.scrollGroup.animate({translateY:this.scrollOffset}),this.currentPage=m,this.positionCheckboxes(),
a=c.animObject(H(a,f.renderer.globalAnimation,!0)),C(function(){w(d,"afterScroll",{currentPage:m})},a.duration||0))}};c.LegendSymbolMixin={drawRectangle:function(b,a){var d=b.symbolHeight,c=b.options.squareSymbol;a.legendSymbol=this.chart.renderer.rect(c?(b.symbolWidth-d)/2:0,b.baseline-d+1,c?d:b.symbolWidth,d,H(b.options.symbolRadius,d/2)).addClass("highcharts-point").attr({zIndex:3}).add(a.legendGroup)},drawLineMarker:function(b){var a=this.options,d=a.marker,c=b.symbolWidth,f=b.symbolHeight,e=
f/2,m=this.chart.renderer,p=this.legendGroup;b=b.baseline-Math.round(.3*b.fontMetrics.b);var g={};this.chart.styledMode||(g={"stroke-width":a.lineWidth||0},a.dashStyle&&(g.dashstyle=a.dashStyle));this.legendLine=m.path(["M",0,b,"L",c,b]).addClass("highcharts-graph").attr(g).add(p);d&&!1!==d.enabled&&c&&(a=Math.min(H(d.radius,e),e),0===this.symbol.indexOf("url")&&(d=D(d,{width:f,height:f}),a=0),this.legendSymbol=d=m.symbol(this.symbol,c/2-a,b-a,2*a,2*a,d).addClass("highcharts-point").add(p),d.isMarker=
!0)}};(/Trident\/7\.0/.test(f.navigator&&f.navigator.userAgent)||m)&&e(c.Legend.prototype,"positionItem",function(b,a){var d=this,c=function(){a._legendItemPos&&b.call(d,a)};c();d.bubbleLegend||setTimeout(c)})});M(J,"parts/Chart.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.animObject,I=e.attr,G=e.defined,H=e.discardElement,v=e.erase,q=e.extend,C=e.isArray,B=e.isNumber,u=e.isObject,w=e.isString,m=e.numberFormat,r=e.objectEach,D=e.pick,A=e.pInt,f=e.relativeLength,b=e.setAnimation,
a=e.splat,d=e.syncTimeout,h=c.addEvent,k=c.animate,l=c.doc,y=c.Axis,p=c.createElement,g=c.defaultOptions,t=c.charts,x=c.css,L=c.find,E=c.fireEvent,n=c.Legend,z=c.marginNames,N=c.merge,O=c.Pointer,V=c.removeEvent,T=c.seriesTypes,P=c.win,Q=c.Chart=function(){this.getArgs.apply(this,arguments)};c.chart=function(a,b,d){return new Q(a,b,d)};q(Q.prototype,{callbacks:[],getArgs:function(){var a=[].slice.call(arguments);if(w(a[0])||a[0].nodeName)this.renderTo=a.shift();this.init(a[0],a[1])},init:function(a,
b){var d,f=a.series,e=a.plotOptions||{};E(this,"init",{args:arguments},function(){a.series=null;d=N(g,a);r(d.plotOptions,function(a,b){u(a)&&(a.tooltip=e[b]&&N(e[b].tooltip)||void 0)});d.tooltip.userOptions=a.chart&&a.chart.forExport&&a.tooltip.userOptions||a.tooltip;d.series=a.series=f;this.userOptions=a;var n=d.chart,x=n.events;this.margin=[];this.spacing=[];this.bounds={h:{},v:{}};this.labelCollectors=[];this.callback=b;this.isResizing=0;this.options=d;this.axes=[];this.series=[];this.time=a.time&&
Object.keys(a.time).length?new c.Time(a.time):c.time;this.numberFormatter=n.numberFormatter||m;this.styledMode=n.styledMode;this.hasCartesianSeries=n.showAxes;var l=this;l.index=t.length;t.push(l);c.chartCount++;x&&r(x,function(a,b){c.isFunction(a)&&h(l,b,a)});l.xAxis=[];l.yAxis=[];l.pointCount=l.colorCounter=l.symbolCounter=0;E(l,"afterInit");l.firstRender()})},initSeries:function(a){var b=this.options.chart;b=a.type||b.type||b.defaultSeriesType;var d=T[b];d||c.error(17,!0,this,{missingModuleFor:b});
b=new d;b.init(this,a);return b},setSeriesData:function(){this.getSeriesOrderByLinks().forEach(function(a){a.points||a.data||!a.enabledDataSorting||a.setData(a.options.data,!1)})},getSeriesOrderByLinks:function(){return this.series.concat().sort(function(a,b){return a.linkedSeries.length||b.linkedSeries.length?b.linkedSeries.length-a.linkedSeries.length:0})},orderSeries:function(a){var b=this.series;for(a=a||0;a<b.length;a++)b[a]&&(b[a].index=a,b[a].name=b[a].getName())},isInsidePlot:function(a,b,
d){var g=d?b:a;a=d?a:b;return 0<=g&&g<=this.plotWidth&&0<=a&&a<=this.plotHeight},redraw:function(a){E(this,"beforeRedraw");var d=this.axes,g=this.series,c=this.pointer,f=this.legend,e=this.userOptions.legend,h=this.isDirtyLegend,t=this.hasCartesianSeries,n=this.isDirtyBox,x=this.renderer,l=x.isHidden(),p=[];this.setResponsive&&this.setResponsive(!1);b(a,this);l&&this.temporaryDisplay();this.layOutTitles();for(a=g.length;a--;){var k=g[a];if(k.options.stacking){var m=!0;if(k.isDirty){var z=!0;break}}}if(z)for(a=
g.length;a--;)k=g[a],k.options.stacking&&(k.isDirty=!0);g.forEach(function(a){a.isDirty&&("point"===a.options.legendType?(a.updateTotals&&a.updateTotals(),h=!0):e&&(e.labelFormatter||e.labelFormat)&&(h=!0));a.isDirtyData&&E(a,"updatedData")});h&&f&&f.options.enabled&&(f.render(),this.isDirtyLegend=!1);m&&this.getStacks();t&&d.forEach(function(a){a.updateNames();a.setScale()});this.getMargins();t&&(d.forEach(function(a){a.isDirty&&(n=!0)}),d.forEach(function(a){var b=a.min+","+a.max;a.extKey!==b&&
(a.extKey=b,p.push(function(){E(a,"afterSetExtremes",q(a.eventArgs,a.getExtremes()));delete a.eventArgs}));(n||m)&&a.redraw()}));n&&this.drawChartBox();E(this,"predraw");g.forEach(function(a){(n||a.isDirty)&&a.visible&&a.redraw();a.isDirtyData=!1});c&&c.reset(!0);x.draw();E(this,"redraw");E(this,"render");l&&this.temporaryDisplay(!0);p.forEach(function(a){a.call()})},get:function(a){function b(b){return b.id===a||b.options&&b.options.id===a}var d=this.series,g;var c=L(this.axes,b)||L(this.series,
b);for(g=0;!c&&g<d.length;g++)c=L(d[g].points||[],b);return c},getAxes:function(){var b=this,d=this.options,g=d.xAxis=a(d.xAxis||{});d=d.yAxis=a(d.yAxis||{});E(this,"getAxes");g.forEach(function(a,b){a.index=b;a.isX=!0});d.forEach(function(a,b){a.index=b});g.concat(d).forEach(function(a){new y(b,a)});E(this,"afterGetAxes")},getSelectedPoints:function(){var a=[];this.series.forEach(function(b){a=a.concat((b[b.hasGroupedData?"points":"data"]||[]).filter(function(a){return D(a.selectedStaging,a.selected)}))});
return a},getSelectedSeries:function(){return this.series.filter(function(a){return a.selected})},setTitle:function(a,b,d){this.applyDescription("title",a);this.applyDescription("subtitle",b);this.applyDescription("caption",void 0);this.layOutTitles(d)},applyDescription:function(a,b){var d=this,g="title"===a?{color:"#333333",fontSize:this.options.isStock?"16px":"18px"}:{color:"#666666"};g=this.options[a]=N(!this.styledMode&&{style:g},this.options[a],b);var c=this[a];c&&b&&(this[a]=c=c.destroy());
g&&!c&&(c=this.renderer.text(g.text,0,0,g.useHTML).attr({align:g.align,"class":"highcharts-"+a,zIndex:g.zIndex||4}).add(),c.update=function(b){d[{title:"setTitle",subtitle:"setSubtitle",caption:"setCaption"}[a]](b)},this.styledMode||c.css(g.style),this[a]=c)},layOutTitles:function(a){var b=[0,0,0],d=this.renderer,g=this.spacingBox;["title","subtitle","caption"].forEach(function(a){var c=this[a],f=this.options[a],e=f.verticalAlign||"top";a="title"===a?-3:"top"===e?b[0]+2:0;if(c){if(!this.styledMode)var h=
f.style.fontSize;h=d.fontMetrics(h,c).b;c.css({width:(f.width||g.width+(f.widthAdjust||0))+"px"});var t=Math.round(c.getBBox(f.useHTML).height);c.align(q({y:"bottom"===e?h:a+h,height:t},f),!1,"spacingBox");f.floating||("top"===e?b[0]=Math.ceil(b[0]+t):"bottom"===e&&(b[2]=Math.ceil(b[2]+t)))}},this);b[0]&&"top"===(this.options.title.verticalAlign||"top")&&(b[0]+=this.options.title.margin);b[2]&&"bottom"===this.options.caption.verticalAlign&&(b[2]+=this.options.caption.margin);var c=!this.titleOffset||
this.titleOffset.join(",")!==b.join(",");this.titleOffset=b;E(this,"afterLayOutTitles");!this.isDirtyBox&&c&&(this.isDirtyBox=this.isDirtyLegend=c,this.hasRendered&&D(a,!0)&&this.isDirtyBox&&this.redraw())},getChartSize:function(){var a=this.options.chart,b=a.width;a=a.height;var d=this.renderTo;G(b)||(this.containerWidth=c.getStyle(d,"width"));G(a)||(this.containerHeight=c.getStyle(d,"height"));this.chartWidth=Math.max(0,b||this.containerWidth||600);this.chartHeight=Math.max(0,f(a,this.chartWidth)||
(1<this.containerHeight?this.containerHeight:400))},temporaryDisplay:function(a){var b=this.renderTo;if(a)for(;b&&b.style;)b.hcOrigStyle&&(c.css(b,b.hcOrigStyle),delete b.hcOrigStyle),b.hcOrigDetached&&(l.body.removeChild(b),b.hcOrigDetached=!1),b=b.parentNode;else for(;b&&b.style;){l.body.contains(b)||b.parentNode||(b.hcOrigDetached=!0,l.body.appendChild(b));if("none"===c.getStyle(b,"display",!1)||b.hcOricDetached)b.hcOrigStyle={display:b.style.display,height:b.style.height,overflow:b.style.overflow},
a={display:"block",overflow:"hidden"},b!==this.renderTo&&(a.height=0),c.css(b,a),b.offsetWidth||b.style.setProperty("display","block","important");b=b.parentNode;if(b===l.body)break}},setClassName:function(a){this.container.className="highcharts-container "+(a||"")},getContainer:function(){var a=this.options,b=a.chart;var d=this.renderTo;var g=c.uniqueKey(),f,e;d||(this.renderTo=d=b.renderTo);w(d)&&(this.renderTo=d=l.getElementById(d));d||c.error(13,!0,this);var h=A(I(d,"data-highcharts-chart"));
B(h)&&t[h]&&t[h].hasRendered&&t[h].destroy();I(d,"data-highcharts-chart",this.index);d.innerHTML="";b.skipClone||d.offsetWidth||this.temporaryDisplay();this.getChartSize();h=this.chartWidth;var n=this.chartHeight;x(d,{overflow:"hidden"});this.styledMode||(f=q({position:"relative",overflow:"hidden",width:h+"px",height:n+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},b.style));this.container=d=p("div",{id:g},f,d);this._cursor=d.style.cursor;this.renderer=
new (c[b.renderer]||c.Renderer)(d,h,n,null,b.forExport,a.exporting&&a.exporting.allowHTML,this.styledMode);this.setClassName(b.className);if(this.styledMode)for(e in a.defs)this.renderer.definition(a.defs[e]);else this.renderer.setStyle(b.style);this.renderer.chartIndex=this.index;E(this,"afterGetContainer")},getMargins:function(a){var b=this.spacing,d=this.margin,g=this.titleOffset;this.resetMargins();g[0]&&!G(d[0])&&(this.plotTop=Math.max(this.plotTop,g[0]+b[0]));g[2]&&!G(d[2])&&(this.marginBottom=
Math.max(this.marginBottom,g[2]+b[2]));this.legend&&this.legend.display&&this.legend.adjustMargins(d,b);E(this,"getMargins");a||this.getAxisMargins()},getAxisMargins:function(){var a=this,b=a.axisOffset=[0,0,0,0],d=a.colorAxis,g=a.margin,c=function(a){a.forEach(function(a){a.visible&&a.getOffset()})};a.hasCartesianSeries?c(a.axes):d&&d.length&&c(d);z.forEach(function(d,c){G(g[c])||(a[d]+=b[c])});a.setChartSize()},reflow:function(a){var b=this,g=b.options.chart,f=b.renderTo,e=G(g.width)&&G(g.height),
h=g.width||c.getStyle(f,"width");g=g.height||c.getStyle(f,"height");f=a?a.target:P;if(!e&&!b.isPrinting&&h&&g&&(f===P||f===l)){if(h!==b.containerWidth||g!==b.containerHeight)c.clearTimeout(b.reflowTimeout),b.reflowTimeout=d(function(){b.container&&b.setSize(void 0,void 0,!1)},a?100:0);b.containerWidth=h;b.containerHeight=g}},setReflow:function(a){var b=this;!1===a||this.unbindReflow?!1===a&&this.unbindReflow&&(this.unbindReflow=this.unbindReflow()):(this.unbindReflow=h(P,"resize",function(a){b.options&&
b.reflow(a)}),h(this,"destroy",this.unbindReflow))},setSize:function(a,g,c){var f=this,e=f.renderer;f.isResizing+=1;b(c,f);f.oldChartHeight=f.chartHeight;f.oldChartWidth=f.chartWidth;"undefined"!==typeof a&&(f.options.chart.width=a);"undefined"!==typeof g&&(f.options.chart.height=g);f.getChartSize();if(!f.styledMode){var h=e.globalAnimation;(h?k:x)(f.container,{width:f.chartWidth+"px",height:f.chartHeight+"px"},h)}f.setChartSize(!0);e.setSize(f.chartWidth,f.chartHeight,c);f.axes.forEach(function(a){a.isDirty=
!0;a.setScale()});f.isDirtyLegend=!0;f.isDirtyBox=!0;f.layOutTitles();f.getMargins();f.redraw(c);f.oldChartHeight=null;E(f,"resize");d(function(){f&&E(f,"endResize",null,function(){--f.isResizing})},F(h).duration||0)},setChartSize:function(a){var b=this.inverted,d=this.renderer,g=this.chartWidth,c=this.chartHeight,f=this.options.chart,e=this.spacing,h=this.clipOffset,t,n,x,l;this.plotLeft=t=Math.round(this.plotLeft);this.plotTop=n=Math.round(this.plotTop);this.plotWidth=x=Math.max(0,Math.round(g-
t-this.marginRight));this.plotHeight=l=Math.max(0,Math.round(c-n-this.marginBottom));this.plotSizeX=b?l:x;this.plotSizeY=b?x:l;this.plotBorderWidth=f.plotBorderWidth||0;this.spacingBox=d.spacingBox={x:e[3],y:e[0],width:g-e[3]-e[1],height:c-e[0]-e[2]};this.plotBox=d.plotBox={x:t,y:n,width:x,height:l};g=2*Math.floor(this.plotBorderWidth/2);b=Math.ceil(Math.max(g,h[3])/2);d=Math.ceil(Math.max(g,h[0])/2);this.clipBox={x:b,y:d,width:Math.floor(this.plotSizeX-Math.max(g,h[1])/2-b),height:Math.max(0,Math.floor(this.plotSizeY-
Math.max(g,h[2])/2-d))};a||this.axes.forEach(function(a){a.setAxisSize();a.setAxisTranslation()});E(this,"afterSetChartSize",{skipAxes:a})},resetMargins:function(){E(this,"resetMargins");var a=this,b=a.options.chart;["margin","spacing"].forEach(function(d){var g=b[d],c=u(g)?g:[g,g,g,g];["Top","Right","Bottom","Left"].forEach(function(g,f){a[d][f]=D(b[d+g],c[f])})});z.forEach(function(b,d){a[b]=D(a.margin[d],a.spacing[d])});a.axisOffset=[0,0,0,0];a.clipOffset=[0,0,0,0]},drawChartBox:function(){var a=
this.options.chart,b=this.renderer,d=this.chartWidth,g=this.chartHeight,c=this.chartBackground,f=this.plotBackground,e=this.plotBorder,h=this.styledMode,t=this.plotBGImage,n=a.backgroundColor,x=a.plotBackgroundColor,l=a.plotBackgroundImage,p,k=this.plotLeft,m=this.plotTop,z=this.plotWidth,L=this.plotHeight,r=this.plotBox,q=this.clipRect,y=this.clipBox,N="animate";c||(this.chartBackground=c=b.rect().addClass("highcharts-background").add(),N="attr");if(h)var u=p=c.strokeWidth();else{u=a.borderWidth||
0;p=u+(a.shadow?8:0);n={fill:n||"none"};if(u||c["stroke-width"])n.stroke=a.borderColor,n["stroke-width"]=u;c.attr(n).shadow(a.shadow)}c[N]({x:p/2,y:p/2,width:d-p-u%2,height:g-p-u%2,r:a.borderRadius});N="animate";f||(N="attr",this.plotBackground=f=b.rect().addClass("highcharts-plot-background").add());f[N](r);h||(f.attr({fill:x||"none"}).shadow(a.plotShadow),l&&(t?(l!==t.attr("href")&&t.attr("href",l),t.animate(r)):this.plotBGImage=b.image(l,k,m,z,L).add()));q?q.animate({width:y.width,height:y.height}):
this.clipRect=b.clipRect(y);N="animate";e||(N="attr",this.plotBorder=e=b.rect().addClass("highcharts-plot-border").attr({zIndex:1}).add());h||e.attr({stroke:a.plotBorderColor,"stroke-width":a.plotBorderWidth||0,fill:"none"});e[N](e.crisp({x:k,y:m,width:z,height:L},-e.strokeWidth()));this.isDirtyBox=!1;E(this,"afterDrawChartBox")},propFromSeries:function(){var a=this,b=a.options.chart,d,g=a.options.series,c,f;["inverted","angular","polar"].forEach(function(e){d=T[b.type||b.defaultSeriesType];f=b[e]||
d&&d.prototype[e];for(c=g&&g.length;!f&&c--;)(d=T[g[c].type])&&d.prototype[e]&&(f=!0);a[e]=f})},linkSeries:function(){var a=this,b=a.series;b.forEach(function(a){a.linkedSeries.length=0});b.forEach(function(b){var d=b.options.linkedTo;w(d)&&(d=":previous"===d?a.series[b.index-1]:a.get(d))&&d.linkedParent!==b&&(d.linkedSeries.push(b),b.linkedParent=d,d.enabledDataSorting&&b.setDataSortingOptions(),b.visible=D(b.options.visible,d.options.visible,b.visible))});E(this,"afterLinkSeries")},renderSeries:function(){this.series.forEach(function(a){a.translate();
a.render()})},renderLabels:function(){var a=this,b=a.options.labels;b.items&&b.items.forEach(function(d){var g=q(b.style,d.style),c=A(g.left)+a.plotLeft,f=A(g.top)+a.plotTop+12;delete g.left;delete g.top;a.renderer.text(d.html,c,f).attr({zIndex:2}).css(g).add()})},render:function(){var a=this.axes,b=this.colorAxis,d=this.renderer,g=this.options,c=0,f=function(a){a.forEach(function(a){a.visible&&a.render()})};this.setTitle();this.legend=new n(this,g.legend);this.getStacks&&this.getStacks();this.getMargins(!0);
this.setChartSize();g=this.plotWidth;a.some(function(a){if(a.horiz&&a.visible&&a.options.labels.enabled&&a.series.length)return c=21,!0});var e=this.plotHeight=Math.max(this.plotHeight-c,0);a.forEach(function(a){a.setScale()});this.getAxisMargins();var h=1.1<g/this.plotWidth;var t=1.05<e/this.plotHeight;if(h||t)a.forEach(function(a){(a.horiz&&h||!a.horiz&&t)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries?f(a):b&&b.length&&f(b);this.seriesGroup||(this.seriesGroup=
d.g("series-group").attr({zIndex:3}).add());this.renderSeries();this.renderLabels();this.addCredits();this.setResponsive&&this.setResponsive();this.updateContainerScaling();this.hasRendered=!0},addCredits:function(a){var b=this;a=N(!0,this.options.credits,a);a.enabled&&!this.credits&&(this.credits=this.renderer.text(a.text+(this.mapCredits||""),0,0).addClass("highcharts-credits").on("click",function(){a.href&&(P.location.href=a.href)}).attr({align:a.position.align,zIndex:8}),b.styledMode||this.credits.css(a.style),
this.credits.add().align(a.position),this.credits.update=function(a){b.credits=b.credits.destroy();b.addCredits(a)})},updateContainerScaling:function(){var a=this.container;if(a.offsetWidth&&a.offsetHeight&&a.getBoundingClientRect){var b=a.getBoundingClientRect(),d=b.width/a.offsetWidth;a=b.height/a.offsetHeight;1!==d||1!==a?this.containerScaling={scaleX:d,scaleY:a}:delete this.containerScaling}},destroy:function(){var a=this,b=a.axes,d=a.series,g=a.container,f,e=g&&g.parentNode;E(a,"destroy");a.renderer.forExport?
v(t,a):t[a.index]=void 0;c.chartCount--;a.renderTo.removeAttribute("data-highcharts-chart");V(a);for(f=b.length;f--;)b[f]=b[f].destroy();this.scroller&&this.scroller.destroy&&this.scroller.destroy();for(f=d.length;f--;)d[f]=d[f].destroy();"title subtitle chartBackground plotBackground plotBGImage plotBorder seriesGroup clipRect credits pointer rangeSelector legend resetZoomButton tooltip renderer".split(" ").forEach(function(b){var d=a[b];d&&d.destroy&&(a[b]=d.destroy())});g&&(g.innerHTML="",V(g),
e&&H(g));r(a,function(b,d){delete a[d]})},firstRender:function(){var a=this,b=a.options;if(!a.isReadyToRender||a.isReadyToRender()){a.getContainer();a.resetMargins();a.setChartSize();a.propFromSeries();a.getAxes();(C(b.series)?b.series:[]).forEach(function(b){a.initSeries(b)});a.linkSeries();a.setSeriesData();E(a,"beforeRender");O&&(a.pointer=new O(a,b));a.render();if(!a.renderer.imgCount&&a.onload)a.onload();a.temporaryDisplay(!0)}},onload:function(){this.callbacks.concat([this.callback]).forEach(function(a){a&&
"undefined"!==typeof this.index&&a.apply(this,[this])},this);E(this,"load");E(this,"render");G(this.index)&&this.setReflow(this.options.chart.reflow);this.onload=null}})});M(J,"parts/ScrollablePlotArea.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.pick,I=c.addEvent;e=c.Chart;"";I(e,"afterSetChartSize",function(e){var G=this.options.chart.scrollablePlotArea,v=G&&G.minWidth;G=G&&G.minHeight;if(!this.renderer.forExport){if(v){if(this.scrollablePixelsX=v=Math.max(0,v-this.chartWidth)){this.plotWidth+=
v;this.inverted?(this.clipBox.height+=v,this.plotBox.height+=v):(this.clipBox.width+=v,this.plotBox.width+=v);var q={1:{name:"right",value:v}}}}else G&&(this.scrollablePixelsY=v=Math.max(0,G-this.chartHeight))&&(this.plotHeight+=v,this.inverted?(this.clipBox.width+=v,this.plotBox.width+=v):(this.clipBox.height+=v,this.plotBox.height+=v),q={2:{name:"bottom",value:v}});q&&!e.skipAxes&&this.axes.forEach(function(e){q[e.side]?e.getPlotLinePath=function(){var v=q[e.side].name,u=this[v];this[v]=u-q[e.side].value;
var w=c.Axis.prototype.getPlotLinePath.apply(this,arguments);this[v]=u;return w}:(e.setAxisSize(),e.setAxisTranslation())})}});I(e,"render",function(){this.scrollablePixelsX||this.scrollablePixelsY?(this.setUpScrolling&&this.setUpScrolling(),this.applyFixed()):this.fixedDiv&&this.applyFixed()});e.prototype.setUpScrolling=function(){var e={WebkitOverflowScrolling:"touch",overflowX:"hidden",overflowY:"hidden"};this.scrollablePixelsX&&(e.overflowX="auto");this.scrollablePixelsY&&(e.overflowY="auto");
this.scrollingContainer=c.createElement("div",{className:"highcharts-scrolling"},e,this.renderTo);this.innerContainer=c.createElement("div",{className:"highcharts-inner-container"},null,this.scrollingContainer);this.innerContainer.appendChild(this.container);this.setUpScrolling=null};e.prototype.moveFixedElements=function(){var c=this.container,e=this.fixedRenderer,v=".highcharts-contextbutton .highcharts-credits .highcharts-legend .highcharts-legend-checkbox .highcharts-navigator-series .highcharts-navigator-xaxis .highcharts-navigator-yaxis .highcharts-navigator .highcharts-reset-zoom .highcharts-scrollbar .highcharts-subtitle .highcharts-title".split(" "),
q;this.scrollablePixelsX&&!this.inverted?q=".highcharts-yaxis":this.scrollablePixelsX&&this.inverted?q=".highcharts-xaxis":this.scrollablePixelsY&&!this.inverted?q=".highcharts-xaxis":this.scrollablePixelsY&&this.inverted&&(q=".highcharts-yaxis");v.push(q,q+"-labels");v.forEach(function(q){[].forEach.call(c.querySelectorAll(q),function(c){(c.namespaceURI===e.SVG_NS?e.box:e.box.parentNode).appendChild(c);c.style.pointerEvents="auto"})})};e.prototype.applyFixed=function(){var e,H=!this.fixedDiv,v=this.options.chart.scrollablePlotArea;
H?(this.fixedDiv=c.createElement("div",{className:"highcharts-fixed"},{position:"absolute",overflow:"hidden",pointerEvents:"none",zIndex:2},null,!0),this.renderTo.insertBefore(this.fixedDiv,this.renderTo.firstChild),this.renderTo.style.overflow="visible",this.fixedRenderer=e=new c.Renderer(this.fixedDiv,this.chartWidth,this.chartHeight),this.scrollableMask=e.path().attr({fill:this.options.chart.backgroundColor||"#fff","fill-opacity":F(v.opacity,.85),zIndex:-1}).addClass("highcharts-scrollable-mask").add(),
this.moveFixedElements(),I(this,"afterShowResetZoom",this.moveFixedElements),I(this,"afterLayOutTitles",this.moveFixedElements)):this.fixedRenderer.setSize(this.chartWidth,this.chartHeight);e=this.chartWidth+(this.scrollablePixelsX||0);var q=this.chartHeight+(this.scrollablePixelsY||0);c.stop(this.container);this.container.style.width=e+"px";this.container.style.height=q+"px";this.renderer.boxWrapper.attr({width:e,height:q,viewBox:[0,0,e,q].join(" ")});this.chartBackground.attr({width:e,height:q});
this.scrollablePixelsY&&(this.scrollingContainer.style.height=this.chartHeight+"px");H&&(v.scrollPositionX&&(this.scrollingContainer.scrollLeft=this.scrollablePixelsX*v.scrollPositionX),v.scrollPositionY&&(this.scrollingContainer.scrollTop=this.scrollablePixelsY*v.scrollPositionY));q=this.axisOffset;H=this.plotTop-q[0]-1;v=this.plotLeft-q[3]-1;e=this.plotTop+this.plotHeight+q[2]+1;q=this.plotLeft+this.plotWidth+q[1]+1;var C=this.plotLeft+this.plotWidth-(this.scrollablePixelsX||0),B=this.plotTop+this.plotHeight-
(this.scrollablePixelsY||0);H=this.scrollablePixelsX?["M",0,H,"L",this.plotLeft-1,H,"L",this.plotLeft-1,e,"L",0,e,"Z","M",C,H,"L",this.chartWidth,H,"L",this.chartWidth,e,"L",C,e,"Z"]:this.scrollablePixelsY?["M",v,0,"L",v,this.plotTop-1,"L",q,this.plotTop-1,"L",q,0,"Z","M",v,B,"L",v,this.chartHeight,"L",q,this.chartHeight,"L",q,B,"Z"]:["M",0,0];"adjustHeight"!==this.redrawTrigger&&this.scrollableMask.attr({d:H})}});M(J,"parts/Point.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=
e.animObject,I=e.defined,G=e.erase,H=e.extend,v=e.isArray,q=e.isNumber,C=e.isObject,B=e.syncTimeout,u=e.pick,w,m=c.fireEvent,r=c.format,D=c.uniqueKey,A=c.removeEvent;c.Point=w=function(){};c.Point.prototype={init:function(c,b,a){this.series=c;this.applyOptions(b,a);this.id=I(this.id)?this.id:D();this.resolveColor();c.chart.pointCount++;m(this,"afterInit");return this},resolveColor:function(){var c=this.series;var b=c.chart.options.chart.colorCount;var a=c.chart.styledMode;a||this.options.color||(this.color=
c.color);c.options.colorByPoint?(a||(b=c.options.colors||c.chart.options.colors,this.color=this.color||b[c.colorCounter],b=b.length),a=c.colorCounter,c.colorCounter++,c.colorCounter===b&&(c.colorCounter=0)):a=c.colorIndex;this.colorIndex=u(this.colorIndex,a)},applyOptions:function(c,b){var a=this.series,d=a.options.pointValKey||a.pointValKey;c=w.prototype.optionsToObject.call(this,c);H(this,c);this.options=this.options?H(this.options,c):c;c.group&&delete this.group;c.dataLabels&&delete this.dataLabels;
d&&(this.y=this[d]);this.formatPrefix=(this.isNull=u(this.isValid&&!this.isValid(),null===this.x||!q(this.y)))?"null":"point";this.selected&&(this.state="select");"name"in this&&"undefined"===typeof b&&a.xAxis&&a.xAxis.hasNames&&(this.x=a.xAxis.nameToX(this));"undefined"===typeof this.x&&a&&(this.x="undefined"===typeof b?a.autoIncrement(this):b);return this},setNestedProperty:function(c,b,a){a.split(".").reduce(function(a,c,f,e){a[c]=e.length-1===f?b:C(a[c],!0)?a[c]:{};return a[c]},c);return c},optionsToObject:function(f){var b=
{},a=this.series,d=a.options.keys,e=d||a.pointArrayMap||["y"],k=e.length,l=0,m=0;if(q(f)||null===f)b[e[0]]=f;else if(v(f))for(!d&&f.length>k&&(a=typeof f[0],"string"===a?b.name=f[0]:"number"===a&&(b.x=f[0]),l++);m<k;)d&&"undefined"===typeof f[l]||(0<e[m].indexOf(".")?c.Point.prototype.setNestedProperty(b,f[l],e[m]):b[e[m]]=f[l]),l++,m++;else"object"===typeof f&&(b=f,f.dataLabels&&(a._hasPointLabels=!0),f.marker&&(a._hasPointMarkers=!0));return b},getClassName:function(){return"highcharts-point"+(this.selected?
" highcharts-point-select":"")+(this.negative?" highcharts-negative":"")+(this.isNull?" highcharts-null-point":"")+("undefined"!==typeof this.colorIndex?" highcharts-color-"+this.colorIndex:"")+(this.options.className?" "+this.options.className:"")+(this.zone&&this.zone.className?" "+this.zone.className.replace("highcharts-negative",""):"")},getZone:function(){var c=this.series,b=c.zones;c=c.zoneAxis||"y";var a=0,d;for(d=b[a];this[c]>=d.value;)d=b[++a];this.nonZonedColor||(this.nonZonedColor=this.color);
this.color=d&&d.color&&!this.options.color?d.color:this.nonZonedColor;return d},hasNewShapeType:function(){return(this.graphic&&(this.graphic.symbolName||this.graphic.element.nodeName))!==this.shapeType},destroy:function(){function c(){e&&(b.setState(),G(e,b),e.length||(d.hoverPoints=null));if(b===d.hoverPoint)b.onMouseOut();if(b.graphic||b.dataLabel||b.dataLabels)A(b),b.destroyElements();for(l in b)b[l]=null}var b=this,a=b.series,d=a.chart;a=a.options.dataSorting;var e=d.hoverPoints,k=F(b.series.chart.renderer.globalAnimation),
l;a&&a.enabled?(this.animateBeforeDestroy(),B(c,k.duration)):c();d.pointCount--;b.legendItem&&d.legend.destroyItem(b)},animateBeforeDestroy:function(){var c=this,b={x:c.startXPos,opacity:0},a,d=c.getGraphicalProps();d.singular.forEach(function(d){a="dataLabel"===d;c[d]=c[d].animate(a?{x:c[d].startXPos,y:c[d].startYPos,opacity:0}:b)});d.plural.forEach(function(a){c[a].forEach(function(a){a.element&&a.animate(H({x:c.startXPos},a.startYPos?{x:a.startXPos,y:a.startYPos}:{}))})})},destroyElements:function(c){var b=
this;c=b.getGraphicalProps(c);c.singular.forEach(function(a){b[a]=b[a].destroy()});c.plural.forEach(function(a){b[a].forEach(function(a){a.element&&a.destroy()});delete b[a]})},getGraphicalProps:function(c){var b=this,a=[],d,f={singular:[],plural:[]};c=c||{graphic:1,dataLabel:1};c.graphic&&a.push("graphic","shadowGroup");c.dataLabel&&a.push("dataLabel","dataLabelUpper","connector");for(d=a.length;d--;){var e=a[d];b[e]&&f.singular.push(e)}["dataLabel","connector"].forEach(function(a){var d=a+"s";c[a]&&
b[d]&&f.plural.push(d)});return f},getLabelConfig:function(){return{x:this.category,y:this.y,color:this.color,colorIndex:this.colorIndex,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(c){var b=this.series,a=b.tooltipOptions,d=u(a.valueDecimals,""),f=a.valuePrefix||"",e=a.valueSuffix||"";b.chart.styledMode&&(c=b.chart.tooltip.styledModeFormat(c));(b.pointArrayMap||["y"]).forEach(function(a){a="{point."+
a;if(f||e)c=c.replace(RegExp(a+"}","g"),f+a+"}"+e);c=c.replace(RegExp(a+"}","g"),a+":,."+d+"f}")});return r(c,{point:this,series:this.series},b.chart)},firePointEvent:function(c,b,a){var d=this,f=this.series.options;(f.point.events[c]||d.options&&d.options.events&&d.options.events[c])&&this.importEvents();"click"===c&&f.allowPointSelect&&(a=function(a){d.select&&d.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});m(this,c,b,a)},visible:!0}});M(J,"parts/Series.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],
function(c,e){var F=e.animObject,I=e.arrayMax,G=e.arrayMin,H=e.clamp,v=e.correctFloat,q=e.defined,C=e.erase,B=e.extend,u=e.isArray,w=e.isNumber,m=e.isString,r=e.objectEach,D=e.pick,A=e.splat,f=e.syncTimeout,b=c.addEvent,a=c.defaultOptions,d=c.defaultPlotOptions,h=c.fireEvent,k=c.merge,l=c.removeEvent,y=c.SVGElement,p=c.win;c.Series=c.seriesType("line",null,{lineWidth:2,allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},marker:{enabledThreshold:2,lineColor:"#ffffff",lineWidth:0,
radius:4,states:{normal:{animation:!0},hover:{animation:{duration:50},enabled:!0,radiusPlus:2,lineWidthPlus:1},select:{fillColor:"#cccccc",lineColor:"#000000",lineWidth:2}}},point:{events:{}},dataLabels:{align:"center",formatter:function(){var a=this.series.chart.numberFormatter;return null===this.y?"":a(this.y,-1)},padding:5,style:{fontSize:"11px",fontWeight:"bold",color:"contrast",textOutline:"1px contrast"},verticalAlign:"bottom",x:0,y:0},cropThreshold:300,opacity:1,pointRange:0,softThreshold:!0,
states:{normal:{animation:!0},hover:{animation:{duration:50},lineWidthPlus:1,marker:{},halo:{size:10,opacity:.25}},select:{animation:{duration:0}},inactive:{animation:{duration:50},opacity:.2}},stickyTracking:!0,turboThreshold:1E3,findNearestPointBy:"x"},{axisTypes:["xAxis","yAxis"],coll:"series",colorCounter:0,cropShoulder:1,directTouch:!1,eventsToUnbind:[],isCartesian:!0,parallelArrays:["x","y"],pointClass:c.Point,requireSorting:!0,sorted:!0,init:function(a,d){h(this,"init",{options:d});var g=this,
f=a.series,e;this.eventOptions=this.eventOptions||{};g.chart=a;g.options=d=g.setOptions(d);g.linkedSeries=[];g.bindAxes();B(g,{name:d.name,state:"",visible:!1!==d.visible,selected:!0===d.selected});var t=d.events;r(t,function(a,d){c.isFunction(a)&&g.eventOptions[d]!==a&&(c.isFunction(g.eventOptions[d])&&l(g,d,g.eventOptions[d]),g.eventOptions[d]=a,b(g,d,a))});if(t&&t.click||d.point&&d.point.events&&d.point.events.click||d.allowPointSelect)a.runTrackerClick=!0;g.getColor();g.getSymbol();g.parallelArrays.forEach(function(a){g[a+
"Data"]||(g[a+"Data"]=[])});g.isCartesian&&(a.hasCartesianSeries=!0);f.length&&(e=f[f.length-1]);g._i=D(e&&e._i,-1)+1;a.orderSeries(this.insert(f));d.dataSorting&&d.dataSorting.enabled?g.setDataSortingOptions():g.points||g.data||g.setData(d.data,!1);h(this,"afterInit")},insert:function(a){var b=this.options.index,d;if(w(b)){for(d=a.length;d--;)if(b>=D(a[d].options.index,a[d]._i)){a.splice(d+1,0,this);break}-1===d&&a.unshift(this);d+=1}else a.push(this);return D(d,a.length-1)},bindAxes:function(){var a=
this,b=a.options,d=a.chart,f;h(this,"bindAxes",null,function(){(a.axisTypes||[]).forEach(function(g){d[g].forEach(function(d){f=d.options;if(b[g]===f.index||"undefined"!==typeof b[g]&&b[g]===f.id||"undefined"===typeof b[g]&&0===f.index)a.insert(d.series),a[g]=d,d.isDirty=!0});a[g]||a.optionalAxis===g||c.error(18,!0,d)})})},updateParallelArrays:function(a,b){var d=a.series,c=arguments,g=w(b)?function(c){var g="y"===c&&d.toYData?d.toYData(a):a[c];d[c+"Data"][b]=g}:function(a){Array.prototype[b].apply(d[a+
"Data"],Array.prototype.slice.call(c,2))};d.parallelArrays.forEach(g)},hasData:function(){return this.visible&&"undefined"!==typeof this.dataMax&&"undefined"!==typeof this.dataMin||this.visible&&this.yData&&0<this.yData.length},autoIncrement:function(){var a=this.options,b=this.xIncrement,d,c=a.pointIntervalUnit,f=this.chart.time;b=D(b,a.pointStart,0);this.pointInterval=d=D(this.pointInterval,a.pointInterval,1);c&&(a=new f.Date(b),"day"===c?f.set("Date",a,f.get("Date",a)+d):"month"===c?f.set("Month",
a,f.get("Month",a)+d):"year"===c&&f.set("FullYear",a,f.get("FullYear",a)+d),d=a.getTime()-b);this.xIncrement=b+d;return b},setDataSortingOptions:function(){var a=this.options;B(this,{requireSorting:!1,sorted:!1,enabledDataSorting:!0,allowDG:!1});q(a.pointRange)||(a.pointRange=1)},setOptions:function(b){var d=this.chart,c=d.options,g=c.plotOptions,f=d.userOptions||{};b=k(b);d=d.styledMode;var e={plotOptions:g,userOptions:b};h(this,"setOptions",e);var l=e.plotOptions[this.type],p=f.plotOptions||{};
this.userOptions=e.userOptions;f=k(l,g.series,f.plotOptions&&f.plotOptions[this.type],b);this.tooltipOptions=k(a.tooltip,a.plotOptions.series&&a.plotOptions.series.tooltip,a.plotOptions[this.type].tooltip,c.tooltip.userOptions,g.series&&g.series.tooltip,g[this.type].tooltip,b.tooltip);this.stickyTracking=D(b.stickyTracking,p[this.type]&&p[this.type].stickyTracking,p.series&&p.series.stickyTracking,this.tooltipOptions.shared&&!this.noSharedTooltip?!0:f.stickyTracking);null===l.marker&&delete f.marker;
this.zoneAxis=f.zoneAxis;c=this.zones=(f.zones||[]).slice();!f.negativeColor&&!f.negativeFillColor||f.zones||(g={value:f[this.zoneAxis+"Threshold"]||f.threshold||0,className:"highcharts-negative"},d||(g.color=f.negativeColor,g.fillColor=f.negativeFillColor),c.push(g));c.length&&q(c[c.length-1].value)&&c.push(d?{}:{color:this.color,fillColor:this.fillColor});h(this,"afterSetOptions",{options:f});return f},getName:function(){return D(this.options.name,"Series "+(this.index+1))},getCyclic:function(a,
b,d){var c=this.chart,g=this.userOptions,f=a+"Index",e=a+"Counter",h=d?d.length:D(c.options.chart[a+"Count"],c[a+"Count"]);if(!b){var t=D(g[f],g["_"+f]);q(t)||(c.series.length||(c[e]=0),g["_"+f]=t=c[e]%h,c[e]+=1);d&&(b=d[t])}"undefined"!==typeof t&&(this[f]=t);this[a]=b},getColor:function(){this.chart.styledMode?this.getCyclic("color"):this.options.colorByPoint?this.options.color=null:this.getCyclic("color",this.options.color||d[this.type].color,this.chart.options.colors)},getSymbol:function(){this.getCyclic("symbol",
this.options.marker.symbol,this.chart.options.symbols)},findPointIndex:function(a,b){var d=a.id,g=a.x,f=this.points,e,h=this.options.dataSorting;if(d)var t=this.chart.get(d);else if(this.linkedParent||this.enabledDataSorting){var l=h&&h.matchByName?"name":"index";t=c.find(f,function(b){return!b.touched&&b[l]===a[l]});if(!t)return}if(t){var p=t&&t.index;"undefined"!==typeof p&&(e=!0)}"undefined"===typeof p&&w(g)&&(p=this.xData.indexOf(g,b));-1!==p&&"undefined"!==typeof p&&this.cropped&&(p=p>=this.cropStart?
p-this.cropStart:p);!e&&f[p]&&f[p].touched&&(p=void 0);return p},drawLegendSymbol:c.LegendSymbolMixin.drawLineMarker,updateData:function(a,b){var d=this.options,c=d.dataSorting,g=this.points,f=[],e,h,t,l=this.requireSorting,p=a.length===g.length,m=!0;this.xIncrement=null;a.forEach(function(a,b){var h=q(a)&&this.pointClass.prototype.optionsToObject.call({series:this},a)||{};var n=h.x;if(h.id||w(n)){if(n=this.findPointIndex(h,t),-1===n||"undefined"===typeof n?f.push(a):g[n]&&a!==d.data[n]?(g[n].update(a,
!1,null,!1),g[n].touched=!0,l&&(t=n+1)):g[n]&&(g[n].touched=!0),!p||b!==n||c&&c.enabled||this.hasDerivedData)e=!0}else f.push(a)},this);if(e)for(a=g.length;a--;)(h=g[a])&&!h.touched&&h.remove&&h.remove(!1,b);else!p||c&&c.enabled?m=!1:(a.forEach(function(a,b){g[b].update&&a!==g[b].y&&g[b].update(a,!1,null,!1)}),f.length=0);g.forEach(function(a){a&&(a.touched=!1)});if(!m)return!1;f.forEach(function(a){this.addPoint(a,!1,null,null,!1)},this);null===this.xIncrement&&this.xData&&this.xData.length&&(this.xIncrement=
I(this.xData),this.autoIncrement());return!0},setData:function(a,b,d,f){var g=this,e=g.points,h=e&&e.length||0,t,l=g.options,p=g.chart,x=l.dataSorting,k=null,r=g.xAxis;k=l.turboThreshold;var L=this.xData,q=this.yData,y=(t=g.pointArrayMap)&&t.length,v=l.keys,A=0,B=1,C;a=a||[];t=a.length;b=D(b,!0);x&&x.enabled&&(a=this.sortData(a));!1!==f&&t&&h&&!g.cropped&&!g.hasGroupedData&&g.visible&&!g.isSeriesBoosting&&(C=this.updateData(a,d));if(!C){g.xIncrement=null;g.colorCounter=0;this.parallelArrays.forEach(function(a){g[a+
"Data"].length=0});if(k&&t>k)if(k=g.getFirstValidPoint(a),w(k))for(d=0;d<t;d++)L[d]=this.autoIncrement(),q[d]=a[d];else if(u(k))if(y)for(d=0;d<t;d++)f=a[d],L[d]=f[0],q[d]=f.slice(1,y+1);else for(v&&(A=v.indexOf("x"),B=v.indexOf("y"),A=0<=A?A:0,B=0<=B?B:1),d=0;d<t;d++)f=a[d],L[d]=f[A],q[d]=f[B];else c.error(12,!1,p);else for(d=0;d<t;d++)"undefined"!==typeof a[d]&&(f={series:g},g.pointClass.prototype.applyOptions.apply(f,[a[d]]),g.updateParallelArrays(f,d));q&&m(q[0])&&c.error(14,!0,p);g.data=[];g.options.data=
g.userOptions.data=a;for(d=h;d--;)e[d]&&e[d].destroy&&e[d].destroy();r&&(r.minRange=r.userMinRange);g.isDirty=p.isDirtyBox=!0;g.isDirtyData=!!e;d=!1}"point"===l.legendType&&(this.processData(),this.generatePoints());b&&p.redraw(d)},sortData:function(a){var b=this,d=b.options.dataSorting.sortKey||"y",c=function(a,b){return q(b)&&a.pointClass.prototype.optionsToObject.call({series:a},b)||{}};a.forEach(function(d,g){a[g]=c(b,d);a[g].index=g},this);a.concat().sort(function(a,b){return w(b[d])?b[d]-a[d]:
-1}).forEach(function(a,b){a.x=b},this);b.linkedSeries&&b.linkedSeries.forEach(function(b){var d=b.options,g=d.data;d.dataSorting&&d.dataSorting.enabled||!g||(g.forEach(function(d,f){g[f]=c(b,d);a[f]&&(g[f].x=a[f].x,g[f].index=f)}),b.setData(g,!1))});return a},processData:function(a){var b=this.xData,d=this.yData,g=b.length;var f=0;var e=this.xAxis,h=this.options;var l=h.cropThreshold;var p=this.getExtremesFromAll||h.getExtremesFromAll,m=this.isCartesian;h=e&&e.val2lin;var k=e&&e.isLog,r=this.requireSorting;
if(m&&!this.isDirty&&!e.isDirty&&!this.yAxis.isDirty&&!a)return!1;if(e){a=e.getExtremes();var q=a.min;var y=a.max}if(m&&this.sorted&&!p&&(!l||g>l||this.forceCrop))if(b[g-1]<q||b[0]>y)b=[],d=[];else if(this.yData&&(b[0]<q||b[g-1]>y)){f=this.cropData(this.xData,this.yData,q,y);b=f.xData;d=f.yData;f=f.start;var u=!0}for(l=b.length||1;--l;)if(g=k?h(b[l])-h(b[l-1]):b[l]-b[l-1],0<g&&("undefined"===typeof w||g<w))var w=g;else 0>g&&r&&(c.error(15,!1,this.chart),r=!1);this.cropped=u;this.cropStart=f;this.processedXData=
b;this.processedYData=d;this.closestPointRange=this.basePointRange=w},cropData:function(a,b,d,c,f){var g=a.length,e=0,h=g,t;f=D(f,this.cropShoulder);for(t=0;t<g;t++)if(a[t]>=d){e=Math.max(0,t-f);break}for(d=t;d<g;d++)if(a[d]>c){h=d+f;break}return{xData:a.slice(e,h),yData:b.slice(e,h),start:e,end:h}},generatePoints:function(){var a=this.options,b=a.data,d=this.data,c,f=this.processedXData,e=this.processedYData,l=this.pointClass,p=f.length,m=this.cropStart||0,k=this.hasGroupedData;a=a.keys;var r=[],
q;d||k||(d=[],d.length=b.length,d=this.data=d);a&&k&&(this.options.keys=!1);for(q=0;q<p;q++){var y=m+q;if(k){var u=(new l).init(this,[f[q]].concat(A(e[q])));u.dataGroup=this.groupMap[q];u.dataGroup.options&&(u.options=u.dataGroup.options,B(u,u.dataGroup.options),delete u.dataLabels)}else(u=d[y])||"undefined"===typeof b[y]||(d[y]=u=(new l).init(this,b[y],f[q]));u&&(u.index=y,r[q]=u)}this.options.keys=a;if(d&&(p!==(c=d.length)||k))for(q=0;q<c;q++)q!==m||k||(q+=p),d[q]&&(d[q].destroyElements(),d[q].plotX=
void 0);this.data=d;this.points=r;h(this,"afterGeneratePoints")},getXExtremes:function(a){return{min:G(a),max:I(a)}},getExtremes:function(a){var b=this.xAxis,d=this.yAxis,c=this.processedXData||this.xData,g=[],f=0,e=0;var l=0;var p=this.requireSorting?this.cropShoulder:0,m=d?d.positiveValuesOnly:!1,k;a=a||this.stackedYData||this.processedYData||[];d=a.length;b&&(l=b.getExtremes(),e=l.min,l=l.max);for(k=0;k<d;k++){var r=c[k];var q=a[k];var y=(w(q)||u(q))&&(q.length||0<q||!m);r=this.getExtremesFromAll||
this.options.getExtremesFromAll||this.cropped||!b||(c[k+p]||r)>=e&&(c[k-p]||r)<=l;if(y&&r)if(y=q.length)for(;y--;)w(q[y])&&(g[f++]=q[y]);else g[f++]=q}this.dataMin=G(g);this.dataMax=I(g);h(this,"afterGetExtremes")},getFirstValidPoint:function(a){for(var b=null,d=a.length,c=0;null===b&&c<d;)b=a[c],c++;return b},translate:function(){this.processedXData||this.processData();this.generatePoints();var a=this.options,b=a.stacking,d=this.xAxis,c=d.categories,f=this.enabledDataSorting,e=this.yAxis,l=this.points,
p=l.length,k=!!this.modifyValue,m,r=this.pointPlacementToXValue(),y=w(r),A=a.threshold,B=a.startFromThreshold?A:0,C,G=this.zoneAxis||"y",I=Number.MAX_VALUE;for(m=0;m<p;m++){var F=l[m],J=F.x;var W=F.y;var M=F.low,Z=b&&e.stacks[(this.negStacks&&W<(B?0:A)?"-":"")+this.stackKey];e.positiveValuesOnly&&null!==W&&0>=W&&(F.isNull=!0);F.plotX=C=v(H(d.translate(J,0,0,0,1,r,"flags"===this.type),-1E5,1E5));if(b&&this.visible&&Z&&Z[J]){var S=this.getStackIndicator(S,J,this.index);if(!F.isNull){var Y=Z[J];var ba=
Y.points[S.key]}}u(ba)&&(M=ba[0],W=ba[1],M===B&&S.key===Z[J].base&&(M=D(w(A)&&A,e.min)),e.positiveValuesOnly&&0>=M&&(M=null),F.total=F.stackTotal=Y.total,F.percentage=Y.total&&F.y/Y.total*100,F.stackY=W,this.irregularWidths||Y.setOffset(this.pointXOffset||0,this.barW||0));F.yBottom=q(M)?H(e.translate(M,0,1,0,1),-1E5,1E5):null;k&&(W=this.modifyValue(W,F));F.plotY=W="number"===typeof W&&Infinity!==W?H(e.translate(W,0,1,0,1),-1E5,1E5):void 0;F.isInside="undefined"!==typeof W&&0<=W&&W<=e.len&&0<=C&&C<=
d.len;F.clientX=y?v(d.translate(J,0,0,0,1,r)):C;F.negative=F[G]<(a[G+"Threshold"]||A||0);F.category=c&&"undefined"!==typeof c[F.x]?c[F.x]:F.x;if(!F.isNull&&!1!==F.visible){"undefined"!==typeof ca&&(I=Math.min(I,Math.abs(C-ca)));var ca=C}F.zone=this.zones.length&&F.getZone();!F.graphic&&this.group&&f&&(F.isNew=!0)}this.closestPointRangePx=I;h(this,"afterTranslate")},getValidPoints:function(a,b,d){var c=this.chart;return(a||this.points||[]).filter(function(a){return b&&!c.isInsidePlot(a.plotX,a.plotY,
c.inverted)?!1:!1!==a.visible&&(d||!a.isNull)})},getClipBox:function(a,b){var d=this.options,c=this.chart,g=c.inverted,f=this.xAxis,e=f&&this.yAxis;a&&!1===d.clip&&e?a=g?{y:-c.chartWidth+e.len+e.pos,height:c.chartWidth,width:c.chartHeight,x:-c.chartHeight+f.len+f.pos}:{y:-e.pos,height:c.chartHeight,width:c.chartWidth,x:-f.pos}:(a=this.clipBox||c.clipBox,b&&(a.width=c.plotSizeX,a.x=0));return b?{width:a.width,x:a.x}:a},setClip:function(a){var b=this.chart,d=this.options,c=b.renderer,g=b.inverted,f=
this.clipBox,e=this.getClipBox(a),h=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,e.height,d.xAxis,d.yAxis].join(),l=b[h],p=b[h+"m"];l||(a&&(e.width=0,g&&(e.x=b.plotSizeX+(!1!==d.clip?0:b.plotTop)),b[h+"m"]=p=c.clipRect(g?b.plotSizeX+99:-99,g?-b.plotLeft:-b.plotTop,99,g?b.chartWidth:b.chartHeight)),b[h]=l=c.clipRect(e),l.count={length:0});a&&!l.count[this.index]&&(l.count[this.index]=!0,l.count.length+=1);if(!1!==d.clip||a)this.group.clip(a||f?l:b.clipRect),this.markerGroup.clip(p),
this.sharedClipKey=h;a||(l.count[this.index]&&(delete l.count[this.index],--l.count.length),0===l.count.length&&h&&b[h]&&(f||(b[h]=b[h].destroy()),b[h+"m"]&&(b[h+"m"]=b[h+"m"].destroy())))},animate:function(a){var b=this.chart,d=F(this.options.animation);if(a)this.setClip(d);else{var c=this.sharedClipKey;a=b[c];var g=this.getClipBox(d,!0);a&&a.animate(g,d);b[c+"m"]&&b[c+"m"].animate({width:g.width+99,x:g.x-(b.inverted?0:99)},d);this.animate=null}},afterAnimate:function(){this.setClip();h(this,"afterAnimate");
this.finishedAnimating=!0},drawPoints:function(){var a=this.points,b=this.chart,d,c,f=this.options.marker,e=this[this.specialGroup]||this.markerGroup,h=this.xAxis,l=D(f.enabled,!h||h.isRadial?!0:null,this.closestPointRangePx>=f.enabledThreshold*f.radius);if(!1!==f.enabled||this._hasPointMarkers)for(d=0;d<a.length;d++){var p=a[d];var m=(c=p.graphic)?"animate":"attr";var k=p.marker||{};var r=!!p.marker;if((l&&"undefined"===typeof k.enabled||k.enabled)&&!p.isNull&&!1!==p.visible){var q=D(k.symbol,this.symbol);
var y=this.markerAttribs(p,p.selected&&"select");this.enabledDataSorting&&(p.startXPos=h.reversed?-y.width:h.width);var u=!1!==p.isInside;c?c[u?"show":"hide"](u).animate(y):u&&(0<y.width||p.hasImage)&&(p.graphic=c=b.renderer.symbol(q,y.x,y.y,y.width,y.height,r?k:f).add(e),this.enabledDataSorting&&b.hasRendered&&(c.attr({x:p.startXPos}),m="animate"));c&&"animate"===m&&c[u?"show":"hide"](u).animate(y);if(c&&!b.styledMode)c[m](this.pointAttribs(p,p.selected&&"select"));c&&c.addClass(p.getClassName(),
!0)}else c&&(p.graphic=c.destroy())}},markerAttribs:function(a,b){var d=this.options.marker,c=a.marker||{},g=c.symbol||d.symbol,f=D(c.radius,d.radius);b&&(d=d.states[b],b=c.states&&c.states[b],f=D(b&&b.radius,d&&d.radius,f+(d&&d.radiusPlus||0)));a.hasImage=g&&0===g.indexOf("url");a.hasImage&&(f=0);a={x:Math.floor(a.plotX)-f,y:a.plotY-f};f&&(a.width=a.height=2*f);return a},pointAttribs:function(a,b){var d=this.options.marker,c=a&&a.options,g=c&&c.marker||{},f=this.color,e=c&&c.color,h=a&&a.color;c=
D(g.lineWidth,d.lineWidth);var l=a&&a.zone&&a.zone.color;a=1;f=e||l||h||f;e=g.fillColor||d.fillColor||f;f=g.lineColor||d.lineColor||f;b=b||"normal";d=d.states[b];b=g.states&&g.states[b]||{};c=D(b.lineWidth,d.lineWidth,c+D(b.lineWidthPlus,d.lineWidthPlus,0));e=b.fillColor||d.fillColor||e;f=b.lineColor||d.lineColor||f;a=D(b.opacity,d.opacity,a);return{stroke:f,"stroke-width":c,fill:e,opacity:a}},destroy:function(a){var b=this,d=b.chart,g=/AppleWebKit\/533/.test(p.navigator.userAgent),f,e,l=b.data||
[],k,m;h(b,"destroy");this.removeEvents(a);(b.axisTypes||[]).forEach(function(a){(m=b[a])&&m.series&&(C(m.series,b),m.isDirty=m.forceRedraw=!0)});b.legendItem&&b.chart.legend.destroyItem(b);for(e=l.length;e--;)(k=l[e])&&k.destroy&&k.destroy();b.points=null;c.clearTimeout(b.animationTimeout);r(b,function(a,b){a instanceof y&&!a.survive&&(f=g&&"group"===b?"hide":"destroy",a[f]())});d.hoverSeries===b&&(d.hoverSeries=null);C(d.series,b);d.orderSeries();r(b,function(d,c){a&&"hcEvents"===c||delete b[c]})},
getGraphPath:function(a,b,d){var c=this,f=c.options,g=f.step,e,h=[],l=[],p;a=a||c.points;(e=a.reversed)&&a.reverse();(g={right:1,center:2}[g]||g&&3)&&e&&(g=4-g);a=this.getValidPoints(a,!1,!(f.connectNulls&&!b&&!d));a.forEach(function(e,n){var t=e.plotX,m=e.plotY,k=a[n-1];(e.leftCliff||k&&k.rightCliff)&&!d&&(p=!0);e.isNull&&!q(b)&&0<n?p=!f.connectNulls:e.isNull&&!b?p=!0:(0===n||p?n=["M",e.plotX,e.plotY]:c.getPointSpline?n=c.getPointSpline(a,e,n):g?(n=1===g?["L",k.plotX,m]:2===g?["L",(k.plotX+t)/2,
k.plotY,"L",(k.plotX+t)/2,m]:["L",t,k.plotY],n.push("L",t,m)):n=["L",t,m],l.push(e.x),g&&(l.push(e.x),2===g&&l.push(e.x)),h.push.apply(h,n),p=!1)});h.xMap=l;return c.graphPath=h},drawGraph:function(){var a=this,b=this.options,d=(this.gappedPath||this.getGraphPath).call(this),c=this.chart.styledMode,f=[["graph","highcharts-graph"]];c||f[0].push(b.lineColor||this.color||"#cccccc",b.dashStyle);f=a.getZonesGraphs(f);f.forEach(function(f,g){var e=f[0],h=a[e],l=h?"animate":"attr";h?(h.endX=a.preventGraphAnimation?
null:d.xMap,h.animate({d:d})):d.length&&(a[e]=h=a.chart.renderer.path(d).addClass(f[1]).attr({zIndex:1}).add(a.group));h&&!c&&(e={stroke:f[2],"stroke-width":b.lineWidth,fill:a.fillGraph&&a.color||"none"},f[3]?e.dashstyle=f[3]:"square"!==b.linecap&&(e["stroke-linecap"]=e["stroke-linejoin"]="round"),h[l](e).shadow(2>g&&b.shadow));h&&(h.startX=d.xMap,h.isArea=d.isArea)})},getZonesGraphs:function(a){this.zones.forEach(function(b,d){d=["zone-graph-"+d,"highcharts-graph highcharts-zone-graph-"+d+" "+(b.className||
"")];this.chart.styledMode||d.push(b.color||this.color,b.dashStyle||this.options.dashStyle);a.push(d)},this);return a},applyZones:function(){var a=this,b=this.chart,d=b.renderer,c=this.zones,f,e,h=this.clips||[],l,p=this.graph,k=this.area,m=Math.max(b.chartWidth,b.chartHeight),r=this[(this.zoneAxis||"y")+"Axis"],q=b.inverted,y,u,w,v=!1;if(c.length&&(p||k)&&r&&"undefined"!==typeof r.min){var A=r.reversed;var C=r.horiz;p&&!this.showLine&&p.hide();k&&k.hide();var B=r.getExtremes();c.forEach(function(c,
g){f=A?C?b.plotWidth:0:C?0:r.toPixels(B.min)||0;f=H(D(e,f),0,m);e=H(Math.round(r.toPixels(D(c.value,B.max),!0)||0),0,m);v&&(f=e=r.toPixels(B.max));y=Math.abs(f-e);u=Math.min(f,e);w=Math.max(f,e);r.isXAxis?(l={x:q?w:u,y:0,width:y,height:m},C||(l.x=b.plotHeight-l.x)):(l={x:0,y:q?w:u,width:m,height:y},C&&(l.y=b.plotWidth-l.y));q&&d.isVML&&(l=r.isXAxis?{x:0,y:A?u:w,height:l.width,width:b.chartWidth}:{x:l.y-b.plotLeft-b.spacingBox.x,y:0,width:l.height,height:b.chartHeight});h[g]?h[g].animate(l):h[g]=d.clipRect(l);
p&&a["zone-graph-"+g].clip(h[g]);k&&a["zone-area-"+g].clip(h[g]);v=c.value>B.max;a.resetZones&&0===e&&(e=void 0)});this.clips=h}else a.visible&&(p&&p.show(!0),k&&k.show(!0))},invertGroups:function(a){function d(){["group","markerGroup"].forEach(function(b){c[b]&&(f.renderer.isVML&&c[b].attr({width:c.yAxis.len,height:c.xAxis.len}),c[b].width=c.yAxis.len,c[b].height=c.xAxis.len,c[b].invert(c.isRadialSeries?!1:a))})}var c=this,f=c.chart;c.xAxis&&(c.eventsToUnbind.push(b(f,"resize",d)),d(),c.invertGroups=
d)},plotGroup:function(a,b,d,c,f){var g=this[a],e=!g;e&&(this[a]=g=this.chart.renderer.g().attr({zIndex:c||.1}).add(f));g.addClass("highcharts-"+b+" highcharts-series-"+this.index+" highcharts-"+this.type+"-series "+(q(this.colorIndex)?"highcharts-color-"+this.colorIndex+" ":"")+(this.options.className||"")+(g.hasClass("highcharts-tracker")?" highcharts-tracker":""),!0);g.attr({visibility:d})[e?"attr":"animate"](this.getPlotBox());return g},getPlotBox:function(){var a=this.chart,b=this.xAxis,d=this.yAxis;
a.inverted&&(b=d,d=this.xAxis);return{translateX:b?b.left:a.plotLeft,translateY:d?d.top:a.plotTop,scaleX:1,scaleY:1}},removeEvents:function(a){a?this.eventsToUnbind.length&&(this.eventsToUnbind.forEach(function(a){a()}),this.eventsToUnbind.length=0):l(this)},render:function(){var a=this,b=a.chart,d=a.options,c=!!a.animate&&b.renderer.isSVG&&F(d.animation).duration,e=a.visible?"inherit":"hidden",l=d.zIndex,p=a.hasRendered,k=b.seriesGroup,m=b.inverted;h(this,"render");var r=a.plotGroup("group","series",
e,l,k);a.markerGroup=a.plotGroup("markerGroup","markers",e,l,k);c&&a.animate(!0);r.inverted=a.isCartesian||a.invertable?m:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());a.visible&&a.drawPoints();a.drawDataLabels&&a.drawDataLabels();a.redrawPoints&&a.redrawPoints();a.drawTracker&&!1!==a.options.enableMouseTracking&&a.drawTracker();a.invertGroups(m);!1===d.clip||a.sharedClipKey||p||r.clip(b.clipRect);c&&a.animate();p||(a.animationTimeout=f(function(){a.afterAnimate()},c||0));a.isDirty=!1;a.hasRendered=
!0;h(a,"afterRender")},redraw:function(){var a=this.chart,b=this.isDirty||this.isDirtyData,d=this.group,c=this.xAxis,f=this.yAxis;d&&(a.inverted&&d.attr({width:a.plotWidth,height:a.plotHeight}),d.animate({translateX:D(c&&c.left,a.plotLeft),translateY:D(f&&f.top,a.plotTop)}));this.translate();this.render();b&&delete this.kdTree},kdAxisArray:["clientX","plotY"],searchPoint:function(a,b){var d=this.xAxis,c=this.yAxis,f=this.chart.inverted;return this.searchKDTree({clientX:f?d.len-a.chartY+d.pos:a.chartX-
d.pos,plotY:f?c.len-a.chartX+c.pos:a.chartY-c.pos},b,a)},buildKDTree:function(a){function b(a,c,f){var g;if(g=a&&a.length){var e=d.kdAxisArray[c%f];a.sort(function(a,b){return a[e]-b[e]});g=Math.floor(g/2);return{point:a[g],left:b(a.slice(0,g),c+1,f),right:b(a.slice(g+1),c+1,f)}}}this.buildingKdTree=!0;var d=this,c=-1<d.options.findNearestPointBy.indexOf("y")?2:1;delete d.kdTree;f(function(){d.kdTree=b(d.getValidPoints(null,!d.directTouch),c,c);d.buildingKdTree=!1},d.options.kdNow||a&&"touchstart"===
a.type?0:1)},searchKDTree:function(a,b,d){function c(a,b,d,l){var p=b.point,n=f.kdAxisArray[d%l],t=p;var k=q(a[g])&&q(p[g])?Math.pow(a[g]-p[g],2):null;var m=q(a[e])&&q(p[e])?Math.pow(a[e]-p[e],2):null;m=(k||0)+(m||0);p.dist=q(m)?Math.sqrt(m):Number.MAX_VALUE;p.distX=q(k)?Math.sqrt(k):Number.MAX_VALUE;n=a[n]-p[n];m=0>n?"left":"right";k=0>n?"right":"left";b[m]&&(m=c(a,b[m],d+1,l),t=m[h]<t[h]?m:p);b[k]&&Math.sqrt(n*n)<t[h]&&(a=c(a,b[k],d+1,l),t=a[h]<t[h]?a:t);return t}var f=this,g=this.kdAxisArray[0],
e=this.kdAxisArray[1],h=b?"distX":"dist";b=-1<f.options.findNearestPointBy.indexOf("y")?2:1;this.kdTree||this.buildingKdTree||this.buildKDTree(d);if(this.kdTree)return c(a,this.kdTree,b,b)},pointPlacementToXValue:function(){var a=this.xAxis,b=this.options.pointPlacement;"between"===b&&(b=a.reversed?-.5:.5);w(b)&&(b*=D(this.options.pointRange||a.pointRange));return b}});""});M(J,"parts/Stacking.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.correctFloat,I=e.defined,G=e.destroyObjectProperties,
H=e.objectEach,v=e.pick;e=c.Axis;var q=c.Chart,C=c.format,B=c.Series;c.StackItem=function(c,e,m,r,q){var u=c.chart.inverted;this.axis=c;this.isNegative=m;this.options=e=e||{};this.x=r;this.total=null;this.points={};this.stack=q;this.rightCliff=this.leftCliff=0;this.alignOptions={align:e.align||(u?m?"left":"right":"center"),verticalAlign:e.verticalAlign||(u?"middle":m?"bottom":"top"),y:e.y,x:e.x};this.textAlign=e.textAlign||(u?m?"right":"left":"center")};c.StackItem.prototype={destroy:function(){G(this,
this.axis)},render:function(c){var e=this.axis.chart,m=this.options,r=m.format;r=r?C(r,this,e):m.formatter.call(this);this.label?this.label.attr({text:r,visibility:"hidden"}):(this.label=e.renderer.label(r,null,null,m.shape,null,null,m.useHTML,!1,"stack-labels"),r={text:r,align:this.textAlign,rotation:m.rotation,padding:v(m.padding,0),visibility:"hidden"},this.label.attr(r),e.styledMode||this.label.css(m.style),this.label.added||this.label.add(c));this.label.labelrank=e.plotHeight},setOffset:function(c,
e,m,r,q){var u=this.axis,f=u.chart;r=u.translate(u.usePercentage?100:r?r:this.total,0,0,0,1);m=u.translate(m?m:0);m=I(r)&&Math.abs(r-m);c=v(q,f.xAxis[0].translate(this.x))+c;u=I(r)&&this.getStackBox(f,this,c,r,e,m,u);e=this.label;c=this.isNegative;q="justify"===v(this.options.overflow,"justify");if(e&&u){m=e.getBBox();var b=f.inverted?c?m.width:0:m.width/2,a=f.inverted?m.height/2:c?-4:m.height+4;this.alignOptions.x=v(this.options.x,0);e.align(this.alignOptions,null,u);r=e.alignAttr;e.show();r.y-=
a;q&&(r.x-=b,B.prototype.justifyDataLabel.call(this.axis,e,this.alignOptions,r,m,u),r.x+=b);r.x=e.alignAttr.x;e.attr({x:r.x,y:r.y});v(!q&&this.options.crop,!0)&&((f=f.isInsidePlot(e.x+(f.inverted?0:-m.width/2),e.y)&&f.isInsidePlot(e.x+(f.inverted?c?-m.width:m.width:m.width/2),e.y+m.height))||e.hide())}},getStackBox:function(c,e,m,r,q,v,f){var b=e.axis.reversed,a=c.inverted;c=f.height+f.pos-(a?c.plotLeft:c.plotTop);e=e.isNegative&&!b||!e.isNegative&&b;return{x:a?e?r:r-v:m,y:a?c-m-q:e?c-r-v:c-r,width:a?
v:q,height:a?q:v}}};q.prototype.getStacks=function(){var c=this,e=c.inverted;c.yAxis.forEach(function(c){c.stacks&&c.hasVisibleSeries&&(c.oldStacks=c.stacks)});c.series.forEach(function(m){var r=m.xAxis&&m.xAxis.options||{};!m.options.stacking||!0!==m.visible&&!1!==c.options.chart.ignoreHiddenSeries||(m.stackKey=[m.type,v(m.options.stack,""),e?r.top:r.left,e?r.height:r.width].join())})};e.prototype.buildStacks=function(){var e=this.series,q=v(this.options.reversedStacks,!0),m=e.length,r;if(!this.isXAxis){this.usePercentage=
!1;for(r=m;r--;){var D=e[q?r:m-r-1];D.setStackedPoints()}for(r=0;r<m;r++)e[r].modifyStacks();c.fireEvent(this,"afterBuildStacks")}};e.prototype.renderStackTotals=function(){var c=this.chart,e=c.renderer,m=this.stacks,r=this.stackTotalGroup;r||(this.stackTotalGroup=r=e.g("stack-labels").attr({visibility:"visible",zIndex:6}).add());r.translate(c.plotLeft,c.plotTop);H(m,function(c){H(c,function(c){c.render(r)})})};e.prototype.resetStacks=function(){var c=this,e=c.stacks;c.isXAxis||H(e,function(e){H(e,
function(m,q){m.touched<c.stacksTouched?(m.destroy(),delete e[q]):(m.total=null,m.cumulative=null)})})};e.prototype.cleanStacks=function(){if(!this.isXAxis){if(this.oldStacks)var c=this.stacks=this.oldStacks;H(c,function(c){H(c,function(c){c.cumulative=c.total})})}};B.prototype.setStackedPoints=function(){if(this.options.stacking&&(!0===this.visible||!1===this.chart.options.chart.ignoreHiddenSeries)){var e=this.processedXData,q=this.processedYData,m=[],r=q.length,D=this.options,A=D.threshold,f=v(D.startFromThreshold&&
A,0),b=D.stack;D=D.stacking;var a=this.stackKey,d="-"+a,h=this.negStacks,k=this.yAxis,l=k.stacks,y=k.oldStacks,p,g;k.stacksTouched+=1;for(g=0;g<r;g++){var t=e[g];var x=q[g];var L=this.getStackIndicator(L,t,this.index);var E=L.key;var n=(p=h&&x<(f?0:A))?d:a;l[n]||(l[n]={});l[n][t]||(y[n]&&y[n][t]?(l[n][t]=y[n][t],l[n][t].total=null):l[n][t]=new c.StackItem(k,k.options.stackLabels,p,t,b));n=l[n][t];null!==x?(n.points[E]=n.points[this.index]=[v(n.cumulative,f)],I(n.cumulative)||(n.base=E),n.touched=
k.stacksTouched,0<L.index&&!1===this.singleStacks&&(n.points[E][0]=n.points[this.index+","+t+",0"][0])):n.points[E]=n.points[this.index]=null;"percent"===D?(p=p?a:d,h&&l[p]&&l[p][t]?(p=l[p][t],n.total=p.total=Math.max(p.total,n.total)+Math.abs(x)||0):n.total=F(n.total+(Math.abs(x)||0))):n.total=F(n.total+(x||0));n.cumulative=v(n.cumulative,f)+(x||0);null!==x&&(n.points[E].push(n.cumulative),m[g]=n.cumulative)}"percent"===D&&(k.usePercentage=!0);this.stackedYData=m;k.oldStacks={}}};B.prototype.modifyStacks=
function(){var c=this,e=c.stackKey,m=c.yAxis.stacks,r=c.processedXData,q,v=c.options.stacking;c[v+"Stacker"]&&[e,"-"+e].forEach(function(f){for(var b=r.length,a,d;b--;)if(a=r[b],q=c.getStackIndicator(q,a,c.index,f),d=(a=m[f]&&m[f][a])&&a.points[q.key])c[v+"Stacker"](d,a,b)})};B.prototype.percentStacker=function(c,e,m){e=e.total?100/e.total:0;c[0]=F(c[0]*e);c[1]=F(c[1]*e);this.stackedYData[m]=c[1]};B.prototype.getStackIndicator=function(c,e,m,r){!I(c)||c.x!==e||r&&c.key!==r?c={x:e,index:0,key:r}:c.index++;
c.key=[m,e,c.index].join();return c}});M(J,"parts/Dynamics.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.defined,I=e.erase,G=e.extend,H=e.isArray,v=e.isNumber,q=e.isObject,C=e.isString,B=e.objectEach,u=e.pick,w=e.relativeLength,m=e.setAnimation,r=e.splat,D=c.addEvent,A=c.animate,f=c.Axis;e=c.Chart;var b=c.createElement,a=c.css,d=c.fireEvent,h=c.merge,k=c.Point,l=c.Series,y=c.seriesTypes;c.cleanRecursively=function(a,b){var d={};B(a,function(f,g){if(q(a[g],!0)&&!a.nodeType&&
b[g])f=c.cleanRecursively(a[g],b[g]),Object.keys(f).length&&(d[g]=f);else if(q(a[g])||a[g]!==b[g])d[g]=a[g]});return d};G(e.prototype,{addSeries:function(a,b,c){var f,g=this;a&&(b=u(b,!0),d(g,"addSeries",{options:a},function(){f=g.initSeries(a);g.isDirtyLegend=!0;g.linkSeries();f.enabledDataSorting&&f.setData(a.data,!1);d(g,"afterAddSeries",{series:f});b&&g.redraw(c)}));return f},addAxis:function(a,b,d,c){return this.createAxis(b?"xAxis":"yAxis",{axis:a,redraw:d,animation:c})},addColorAxis:function(a,
b,d){return this.createAxis("colorAxis",{axis:a,redraw:b,animation:d})},createAxis:function(a,b){var d=this.options,g="colorAxis"===a,e=b.redraw,l=b.animation;b=h(b.axis,{index:this[a].length,isX:"xAxis"===a});var p=g?new c.ColorAxis(this,b):new f(this,b);d[a]=r(d[a]||{});d[a].push(b);g&&(this.isDirtyLegend=!0,this.axes.forEach(function(a){a.series=[]}),this.series.forEach(function(a){a.bindAxes();a.isDirtyData=!0}));u(e,!0)&&this.redraw(l);return p},showLoading:function(d){var c=this,f=c.options,
e=c.loadingDiv,h=f.loading,l=function(){e&&a(e,{left:c.plotLeft+"px",top:c.plotTop+"px",width:c.plotWidth+"px",height:c.plotHeight+"px"})};e||(c.loadingDiv=e=b("div",{className:"highcharts-loading highcharts-loading-hidden"},null,c.container),c.loadingSpan=b("span",{className:"highcharts-loading-inner"},null,e),D(c,"redraw",l));e.className="highcharts-loading";c.loadingSpan.innerHTML=u(d,f.lang.loading,"");c.styledMode||(a(e,G(h.style,{zIndex:10})),a(c.loadingSpan,h.labelStyle),c.loadingShown||(a(e,
{opacity:0,display:""}),A(e,{opacity:h.style.opacity||.5},{duration:h.showDuration||0})));c.loadingShown=!0;l()},hideLoading:function(){var b=this.options,d=this.loadingDiv;d&&(d.className="highcharts-loading highcharts-loading-hidden",this.styledMode||A(d,{opacity:0},{duration:b.loading.hideDuration||100,complete:function(){a(d,{display:"none"})}}));this.loadingShown=!1},propsRequireDirtyBox:"backgroundColor borderColor borderWidth borderRadius plotBackgroundColor plotBackgroundImage plotBorderColor plotBorderWidth plotShadow shadow".split(" "),
propsRequireReflow:"margin marginTop marginRight marginBottom marginLeft spacing spacingTop spacingRight spacingBottom spacingLeft".split(" "),propsRequireUpdateSeries:"chart.inverted chart.polar chart.ignoreHiddenSeries chart.type colors plotOptions time tooltip".split(" "),collectionsWithUpdate:["xAxis","yAxis","zAxis","series"],update:function(a,b,f,e){var g=this,l={credits:"addCredits",title:"setTitle",subtitle:"setSubtitle",caption:"setCaption"},p,k,m,t=a.isResponsiveOptions,q=[];d(g,"update",
{options:a});t||g.setResponsive(!1,!0);a=c.cleanRecursively(a,g.options);h(!0,g.userOptions,a);if(p=a.chart){h(!0,g.options.chart,p);"className"in p&&g.setClassName(p.className);"reflow"in p&&g.setReflow(p.reflow);if("inverted"in p||"polar"in p||"type"in p){g.propFromSeries();var x=!0}"alignTicks"in p&&(x=!0);B(p,function(a,b){-1!==g.propsRequireUpdateSeries.indexOf("chart."+b)&&(k=!0);-1!==g.propsRequireDirtyBox.indexOf(b)&&(g.isDirtyBox=!0);t||-1===g.propsRequireReflow.indexOf(b)||(m=!0)});!g.styledMode&&
"style"in p&&g.renderer.setStyle(p.style)}!g.styledMode&&a.colors&&(this.options.colors=a.colors);a.plotOptions&&h(!0,this.options.plotOptions,a.plotOptions);a.time&&this.time===c.time&&(this.time=new c.Time(a.time));B(a,function(a,b){if(g[b]&&"function"===typeof g[b].update)g[b].update(a,!1);else if("function"===typeof g[l[b]])g[l[b]](a);"chart"!==b&&-1!==g.propsRequireUpdateSeries.indexOf(b)&&(k=!0)});this.collectionsWithUpdate.forEach(function(b){if(a[b]){if("series"===b){var d=[];g[b].forEach(function(a,
b){a.options.isInternal||d.push(u(a.options.index,b))})}r(a[b]).forEach(function(a,c){(c=F(a.id)&&g.get(a.id)||g[b][d?d[c]:c])&&c.coll===b&&(c.update(a,!1),f&&(c.touched=!0));!c&&f&&g.collectionsWithInit[b]&&(g.collectionsWithInit[b][0].apply(g,[a].concat(g.collectionsWithInit[b][1]||[]).concat([!1])).touched=!0)});f&&g[b].forEach(function(a){a.touched||a.options.isInternal?delete a.touched:q.push(a)})}});q.forEach(function(a){a.remove&&a.remove(!1)});x&&g.axes.forEach(function(a){a.update({},!1)});
k&&g.getSeriesOrderByLinks().forEach(function(a){a.chart&&a.update({},!1)},this);a.loading&&h(!0,g.options.loading,a.loading);x=p&&p.width;p=p&&p.height;C(p)&&(p=w(p,x||g.chartWidth));m||v(x)&&x!==g.chartWidth||v(p)&&p!==g.chartHeight?g.setSize(x,p,e):u(b,!0)&&g.redraw(e);d(g,"afterUpdate",{options:a,redraw:b,animation:e})},setSubtitle:function(a,b){this.applyDescription("subtitle",a);this.layOutTitles(b)},setCaption:function(a,b){this.applyDescription("caption",a);this.layOutTitles(b)}});e.prototype.collectionsWithInit=
{xAxis:[e.prototype.addAxis,[!0]],yAxis:[e.prototype.addAxis,[!1]],series:[e.prototype.addSeries]};G(k.prototype,{update:function(a,b,d,c){function f(){g.applyOptions(a);null===g.y&&h&&(g.graphic=h.destroy());q(a,!0)&&(h&&h.element&&a&&a.marker&&"undefined"!==typeof a.marker.symbol&&(g.graphic=h.destroy()),a&&a.dataLabels&&g.dataLabel&&(g.dataLabel=g.dataLabel.destroy()),g.connector&&(g.connector=g.connector.destroy()));l=g.index;e.updateParallelArrays(g,l);k.data[l]=q(k.data[l],!0)||q(a,!0)?g.options:
u(a,k.data[l]);e.isDirty=e.isDirtyData=!0;!e.fixedBox&&e.hasCartesianSeries&&(p.isDirtyBox=!0);"point"===k.legendType&&(p.isDirtyLegend=!0);b&&p.redraw(d)}var g=this,e=g.series,h=g.graphic,l,p=e.chart,k=e.options;b=u(b,!0);!1===c?f():g.firePointEvent("update",{options:a},f)},remove:function(a,b){this.series.removePoint(this.series.data.indexOf(this),a,b)}});G(l.prototype,{addPoint:function(a,b,c,f,e){var g=this.options,h=this.data,l=this.chart,p=this.xAxis;p=p&&p.hasNames&&p.names;var k=g.data,m=
this.xData,t;b=u(b,!0);var r={series:this};this.pointClass.prototype.applyOptions.apply(r,[a]);var q=r.x;var x=m.length;if(this.requireSorting&&q<m[x-1])for(t=!0;x&&m[x-1]>q;)x--;this.updateParallelArrays(r,"splice",x,0,0);this.updateParallelArrays(r,x);p&&r.name&&(p[q]=r.name);k.splice(x,0,a);t&&(this.data.splice(x,0,null),this.processData());"point"===g.legendType&&this.generatePoints();c&&(h[0]&&h[0].remove?h[0].remove(!1):(h.shift(),this.updateParallelArrays(r,"shift"),k.shift()));!1!==e&&d(this,
"addPoint",{point:r});this.isDirtyData=this.isDirty=!0;b&&l.redraw(f)},removePoint:function(a,b,d){var c=this,f=c.data,g=f[a],e=c.points,h=c.chart,l=function(){e&&e.length===f.length&&e.splice(a,1);f.splice(a,1);c.options.data.splice(a,1);c.updateParallelArrays(g||{series:c},"splice",a,1);g&&g.destroy();c.isDirty=!0;c.isDirtyData=!0;b&&h.redraw()};m(d,h);b=u(b,!0);g?g.firePointEvent("remove",null,l):l()},remove:function(a,b,c,f){function g(){e.destroy(f);e.remove=null;h.isDirtyLegend=h.isDirtyBox=
!0;h.linkSeries();u(a,!0)&&h.redraw(b)}var e=this,h=e.chart;!1!==c?d(e,"remove",null,g):g()},update:function(a,b){a=c.cleanRecursively(a,this.userOptions);d(this,"update",{options:a});var f=this,g=f.chart,e=f.userOptions,l=f.initialType||f.type,p=a.type||e.type||g.options.chart.type,k=!(this.hasDerivedData||a.dataGrouping||p&&p!==this.type||"undefined"!==typeof a.pointStart||a.pointInterval||a.pointIntervalUnit||a.keys),m=y[l].prototype,r,q=["group","markerGroup","dataLabelsGroup","transformGroup"],
v=["eventOptions","navigatorSeries","baseSeries"],w=f.finishedAnimating&&{animation:!1},A={};k&&(v.push("data","isDirtyData","points","processedXData","processedYData","xIncrement","_hasPointMarkers","_hasPointLabels","mapMap","mapData","minY","maxY","minX","maxX"),!1!==a.visible&&v.push("area","graph"),f.parallelArrays.forEach(function(a){v.push(a+"Data")}),a.data&&(a.dataSorting&&G(f.options.dataSorting,a.dataSorting),this.setData(a.data,!1)));a=h(e,w,{index:"undefined"===typeof e.index?f.index:
e.index,pointStart:u(e.pointStart,f.xData[0])},!k&&{data:f.options.data},a);k&&a.data&&(a.data=f.options.data);v=q.concat(v);v.forEach(function(a){v[a]=f[a];delete f[a]});f.remove(!1,null,!1,!0);for(r in m)f[r]=void 0;y[p||l]?G(f,y[p||l].prototype):c.error(17,!0,g,{missingModuleFor:p||l});v.forEach(function(a){f[a]=v[a]});f.init(g,a);if(k&&this.points){var D=f.options;!1===D.visible?(A.graphic=1,A.dataLabel=1):f._hasPointLabels||(p=D.marker,m=D.dataLabels,p&&(!1===p.enabled||"symbol"in p)&&(A.graphic=
1),m&&!1===m.enabled&&(A.dataLabel=1));this.points.forEach(function(a){a&&a.series&&(a.resolveColor(),Object.keys(A).length&&a.destroyElements(A),!1===D.showInLegend&&a.legendItem&&g.legend.destroyItem(a))},this)}a.zIndex!==e.zIndex&&q.forEach(function(b){f[b]&&f[b].attr({zIndex:a.zIndex})});f.initialType=l;g.linkSeries();d(this,"afterUpdate");u(b,!0)&&g.redraw(k?void 0:!1)},setName:function(a){this.name=this.options.name=this.userOptions.name=a;this.chart.isDirtyLegend=!0}});G(f.prototype,{update:function(a,
b){var d=this.chart,c=a&&a.events||{};a=h(this.userOptions,a);d.options[this.coll].indexOf&&(d.options[this.coll][d.options[this.coll].indexOf(this.userOptions)]=a);B(d.options[this.coll].events,function(a,b){"undefined"===typeof c[b]&&(c[b]=void 0)});this.destroy(!0);this.init(d,G(a,{events:c}));d.isDirtyBox=!0;u(b,!0)&&d.redraw()},remove:function(a){for(var b=this.chart,d=this.coll,c=this.series,f=c.length;f--;)c[f]&&c[f].remove(!1);I(b.axes,this);I(b[d],this);H(b.options[d])?b.options[d].splice(this.options.index,
1):delete b.options[d];b[d].forEach(function(a,b){a.options.index=a.userOptions.index=b});this.destroy();b.isDirtyBox=!0;u(a,!0)&&b.redraw()},setTitle:function(a,b){this.update({title:a},b)},setCategories:function(a,b){this.update({categories:a},b)}})});M(J,"parts/AreaSeries.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.objectEach,I=e.pick,G=c.color,H=c.Series;e=c.seriesType;e("area","line",{softThreshold:!1,threshold:0},{singleStacks:!1,getStackPoints:function(c){var e=
[],v=[],B=this.xAxis,u=this.yAxis,w=u.stacks[this.stackKey],m={},r=this.index,D=u.series,A=D.length,f=I(u.options.reversedStacks,!0)?1:-1,b;c=c||this.points;if(this.options.stacking){for(b=0;b<c.length;b++)c[b].leftNull=c[b].rightNull=void 0,m[c[b].x]=c[b];F(w,function(a,b){null!==a.total&&v.push(b)});v.sort(function(a,b){return a-b});var a=D.map(function(a){return a.visible});v.forEach(function(d,c){var h=0,l,q;if(m[d]&&!m[d].isNull)e.push(m[d]),[-1,1].forEach(function(e){var g=1===e?"rightNull":
"leftNull",h=0,p=w[v[c+e]];if(p)for(b=r;0<=b&&b<A;)l=p.points[b],l||(b===r?m[d][g]=!0:a[b]&&(q=w[d].points[b])&&(h-=q[1]-q[0])),b+=f;m[d][1===e?"rightCliff":"leftCliff"]=h});else{for(b=r;0<=b&&b<A;){if(l=w[d].points[b]){h=l[1];break}b+=f}h=u.translate(h,0,1,0,1);e.push({isNull:!0,plotX:B.translate(d,0,0,0,1),x:d,plotY:h,yBottom:h})}})}return e},getGraphPath:function(c){var e=H.prototype.getGraphPath,v=this.options,B=v.stacking,u=this.yAxis,w,m=[],r=[],D=this.index,A=u.stacks[this.stackKey],f=v.threshold,
b=Math.round(u.getThreshold(v.threshold));v=I(v.connectNulls,"percent"===B);var a=function(a,d,e){var g=c[a];a=B&&A[g.x].points[D];var l=g[e+"Null"]||0;e=g[e+"Cliff"]||0;g=!0;if(e||l){var p=(l?a[0]:a[1])+e;var k=a[0]+e;g=!!l}else!B&&c[d]&&c[d].isNull&&(p=k=f);"undefined"!==typeof p&&(r.push({plotX:h,plotY:null===p?b:u.getThreshold(p),isNull:g,isCliff:!0}),m.push({plotX:h,plotY:null===k?b:u.getThreshold(k),doCurve:!1}))};c=c||this.points;B&&(c=this.getStackPoints(c));for(w=0;w<c.length;w++){B||(c[w].leftCliff=
c[w].rightCliff=c[w].leftNull=c[w].rightNull=void 0);var d=c[w].isNull;var h=I(c[w].rectPlotX,c[w].plotX);var k=I(c[w].yBottom,b);if(!d||v)v||a(w,w-1,"left"),d&&!B&&v||(r.push(c[w]),m.push({x:w,plotX:h,plotY:k})),v||a(w,w+1,"right")}w=e.call(this,r,!0,!0);m.reversed=!0;d=e.call(this,m,!0,!0);d.length&&(d[0]="L");d=w.concat(d);e=e.call(this,r,!1,v);d.xMap=w.xMap;this.areaPath=d;return e},drawGraph:function(){this.areaPath=[];H.prototype.drawGraph.apply(this);var c=this,e=this.areaPath,C=this.options,
B=[["area","highcharts-area",this.color,C.fillColor]];this.zones.forEach(function(e,q){B.push(["zone-area-"+q,"highcharts-area highcharts-zone-area-"+q+" "+e.className,e.color||c.color,e.fillColor||C.fillColor])});B.forEach(function(q){var v=q[0],m=c[v],r=m?"animate":"attr",u={};m?(m.endX=c.preventGraphAnimation?null:e.xMap,m.animate({d:e})):(u.zIndex=0,m=c[v]=c.chart.renderer.path(e).addClass(q[1]).add(c.group),m.isArea=!0);c.chart.styledMode||(u.fill=I(q[3],G(q[2]).setOpacity(I(C.fillOpacity,.75)).get()));
m[r](u);m.startX=e.xMap;m.shiftUnit=C.step?2:1})},drawLegendSymbol:c.LegendSymbolMixin.drawRectangle});""});M(J,"parts/SplineSeries.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.pick;c=c.seriesType;c("spline","line",{},{getPointSpline:function(c,e,H){var v=e.plotX,q=e.plotY,C=c[H-1];H=c[H+1];if(C&&!C.isNull&&!1!==C.doCurve&&!e.isCliff&&H&&!H.isNull&&!1!==H.doCurve&&!e.isCliff){c=C.plotY;var B=H.plotX;H=H.plotY;var u=0;var w=(1.5*v+C.plotX)/2.5;var m=(1.5*q+c)/2.5;B=(1.5*
v+B)/2.5;var r=(1.5*q+H)/2.5;B!==w&&(u=(r-m)*(B-v)/(B-w)+q-r);m+=u;r+=u;m>c&&m>q?(m=Math.max(c,q),r=2*q-m):m<c&&m<q&&(m=Math.min(c,q),r=2*q-m);r>H&&r>q?(r=Math.max(H,q),m=2*q-r):r<H&&r<q&&(r=Math.min(H,q),m=2*q-r);e.rightContX=B;e.rightContY=r}e=["C",F(C.rightContX,C.plotX),F(C.rightContY,C.plotY),F(w,v),F(m,q),v,q];C.rightContX=C.rightContY=null;return e}});""});M(J,"parts/AreaSplineSeries.js",[J["parts/Globals.js"]],function(c){var e=c.seriesTypes.area.prototype,F=c.seriesType;F("areaspline","spline",
c.defaultPlotOptions.area,{getStackPoints:e.getStackPoints,getGraphPath:e.getGraphPath,drawGraph:e.drawGraph,drawLegendSymbol:c.LegendSymbolMixin.drawRectangle});""});M(J,"parts/ColumnSeries.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.animObject,I=e.clamp,G=e.defined,H=e.extend,v=e.isNumber,q=e.pick,C=c.color,B=c.merge,u=c.Series;e=c.seriesType;var w=c.svg;e("column","line",{borderRadius:0,crisp:!0,groupPadding:.2,marker:null,pointPadding:.1,minPointLength:0,cropThreshold:50,
pointRange:null,states:{hover:{halo:!1,brightness:.1},select:{color:"#cccccc",borderColor:"#000000"}},dataLabels:{align:null,verticalAlign:null,y:null},softThreshold:!1,startFromThreshold:!0,stickyTracking:!1,tooltip:{distance:6},threshold:0,borderColor:"#ffffff"},{cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){u.prototype.init.apply(this,arguments);var c=this,e=c.chart;e.hasRendered&&e.series.forEach(function(e){e.type===c.type&&(e.isDirty=!0)})},
getColumnMetrics:function(){var c=this,e=c.options,v=c.xAxis,u=c.yAxis,f=v.options.reversedStacks;f=v.reversed&&!f||!v.reversed&&f;var b,a={},d=0;!1===e.grouping?d=1:c.chart.series.forEach(function(f){var e=f.yAxis,g=f.options;if(f.type===c.type&&(f.visible||!c.chart.options.chart.ignoreHiddenSeries)&&u.len===e.len&&u.pos===e.pos){if(g.stacking){b=f.stackKey;"undefined"===typeof a[b]&&(a[b]=d++);var h=a[b]}else!1!==g.grouping&&(h=d++);f.columnIndex=h}});var h=Math.min(Math.abs(v.transA)*(v.ordinalSlope||
e.pointRange||v.closestPointRange||v.tickInterval||1),v.len),k=h*e.groupPadding,l=(h-2*k)/(d||1);e=Math.min(e.maxPointWidth||v.len,q(e.pointWidth,l*(1-2*e.pointPadding)));c.columnMetrics={width:e,offset:(l-e)/2+(k+((c.columnIndex||0)+(f?1:0))*l-h/2)*(f?-1:1)};return c.columnMetrics},crispCol:function(c,e,q,v){var f=this.chart,b=this.borderWidth,a=-(b%2?.5:0);b=b%2?.5:1;f.inverted&&f.renderer.isVML&&(b+=1);this.options.crisp&&(q=Math.round(c+q)+a,c=Math.round(c)+a,q-=c);v=Math.round(e+v)+b;a=.5>=Math.abs(e)&&
.5<v;e=Math.round(e)+b;v-=e;a&&v&&(--e,v+=1);return{x:c,y:e,width:q,height:v}},translate:function(){var c=this,e=c.chart,v=c.options,w=c.dense=2>c.closestPointRange*c.xAxis.transA;w=c.borderWidth=q(v.borderWidth,w?0:1);var f=c.yAxis,b=v.threshold,a=c.translatedThreshold=f.getThreshold(b),d=q(v.minPointLength,5),h=c.getColumnMetrics(),k=h.width,l=c.barW=Math.max(k,1+2*w),y=c.pointXOffset=h.offset,p=c.dataMin,g=c.dataMax;e.inverted&&(a-=.5);v.pointPadding&&(l=Math.ceil(l));u.prototype.translate.apply(c);
c.points.forEach(function(h){var m=q(h.yBottom,a),t=999+Math.abs(m),r=k;t=I(h.plotY,-t,f.len+t);var n=h.plotX+y,z=l,v=Math.min(t,m),u=Math.max(t,m)-v;if(d&&Math.abs(u)<d){u=d;var w=!f.reversed&&!h.negative||f.reversed&&h.negative;h.y===b&&c.dataMax<=b&&f.min<b&&p!==g&&(w=!w);v=Math.abs(v-a)>d?m-d:a-(w?d:0)}G(h.options.pointWidth)&&(r=z=Math.ceil(h.options.pointWidth),n-=Math.round((r-k)/2));h.barX=n;h.pointWidth=r;h.tooltipPos=e.inverted?[f.len+f.pos-e.plotLeft-t,c.xAxis.len-n-z/2,u]:[n+z/2,t+f.pos-
e.plotTop,u];h.shapeType=c.pointClass.prototype.shapeType||"rect";h.shapeArgs=c.crispCol.apply(c,h.isNull?[n,a,z,0]:[n,v,z,u])})},getSymbol:c.noop,drawLegendSymbol:c.LegendSymbolMixin.drawRectangle,drawGraph:function(){this.group[this.dense?"addClass":"removeClass"]("highcharts-dense-data")},pointAttribs:function(c,e){var m=this.options,r=this.pointAttrToOptions||{};var f=r.stroke||"borderColor";var b=r["stroke-width"]||"borderWidth",a=c&&c.color||this.color,d=c&&c[f]||m[f]||this.color||a,h=c&&c[b]||
m[b]||this[b]||0;r=c&&c.options.dashStyle||m.dashStyle;var k=q(c&&c.opacity,m.opacity,1);if(c&&this.zones.length){var l=c.getZone();a=c.options.color||l&&(l.color||c.nonZonedColor)||this.color;l&&(d=l.borderColor||d,r=l.dashStyle||r,h=l.borderWidth||h)}e&&c&&(c=B(m.states[e],c.options.states&&c.options.states[e]||{}),e=c.brightness,a=c.color||"undefined"!==typeof e&&C(a).brighten(c.brightness).get()||a,d=c[f]||d,h=c[b]||h,r=c.dashStyle||r,k=q(c.opacity,k));f={fill:a,stroke:d,"stroke-width":h,opacity:k};
r&&(f.dashstyle=r);return f},drawPoints:function(){var c=this,e=this.chart,q=c.options,u=e.renderer,f=q.animationLimit||250,b;c.points.forEach(function(a){var d=a.graphic,h=!!d,k=d&&e.pointCount<f?"animate":"attr";if(v(a.plotY)&&null!==a.y){b=a.shapeArgs;d&&a.hasNewShapeType()&&(d=d.destroy());c.enabledDataSorting&&(a.startXPos=c.xAxis.reversed?-(b?b.width:0):c.xAxis.width);d||(a.graphic=d=u[a.shapeType](b).add(a.group||c.group))&&c.enabledDataSorting&&e.hasRendered&&e.pointCount<f&&(d.attr({x:a.startXPos}),
h=!0,k="animate");if(d&&h)d[k](B(b));if(q.borderRadius)d[k]({r:q.borderRadius});e.styledMode||d[k](c.pointAttribs(a,a.selected&&"select")).shadow(!1!==a.allowShadow&&q.shadow,null,q.stacking&&!q.borderRadius);d.addClass(a.getClassName(),!0)}else d&&(a.graphic=d.destroy())})},animate:function(c){var e=this,m=this.yAxis,q=e.options,f=this.chart.inverted,b={},a=f?"translateX":"translateY";if(w)if(c)b.scaleY=.001,c=I(m.toPixels(q.threshold),m.pos,m.pos+m.len),f?b.translateX=c-m.len:b.translateY=c,e.clipBox&&
e.setClip(),e.group.attr(b);else{var d=e.group.attr(a);e.group.animate({scaleY:1},H(F(e.options.animation),{step:function(c,f){b[a]=d+f.pos*(m.pos-d);e.group.attr(b)}}));e.animate=null}},remove:function(){var c=this,e=c.chart;e.hasRendered&&e.series.forEach(function(e){e.type===c.type&&(e.isDirty=!0)});u.prototype.remove.apply(c,arguments)}});""});M(J,"parts/BarSeries.js",[J["parts/Globals.js"]],function(c){c=c.seriesType;c("bar","column",null,{inverted:!0});""});M(J,"parts/ScatterSeries.js",[J["parts/Globals.js"]],
function(c){var e=c.Series,F=c.seriesType;F("scatter","line",{lineWidth:0,findNearestPointBy:"xy",jitter:{x:0,y:0},marker:{enabled:!0},tooltip:{headerFormat:'<span style="color:{point.color}">\u25cf</span> <span style="font-size: 10px"> {series.name}</span><br/>',pointFormat:"x: <b>{point.x}</b><br/>y: <b>{point.y}</b><br/>"}},{sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],takeOrdinalPosition:!1,drawGraph:function(){this.options.lineWidth&&
e.prototype.drawGraph.call(this)},applyJitter:function(){var c=this,e=this.options.jitter,F=this.points.length;e&&this.points.forEach(function(v,q){["x","y"].forEach(function(C,B){var u="plot"+C.toUpperCase();if(e[C]&&!v.isNull){var w=c[C+"Axis"];var m=e[C]*w.transA;if(w&&!w.isLog){var r=Math.max(0,v[u]-m);w=Math.min(w.len,v[u]+m);B=1E4*Math.sin(q+B*F);v[u]=r+(w-r)*(B-Math.floor(B));"x"===C&&(v.clientX=v.plotX)}}})})}});c.addEvent(e,"afterTranslate",function(){this.applyJitter&&this.applyJitter()});
""});M(J,"mixins/centered-series.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.isNumber,I=e.pick,G=e.relativeLength,H=c.deg2rad;c.CenteredSeriesMixin={getCenter:function(){var c=this.options,e=this.chart,C=2*(c.slicedOffset||0),B=e.plotWidth-2*C;e=e.plotHeight-2*C;var u=c.center;u=[I(u[0],"50%"),I(u[1],"50%"),c.size||"100%",c.innerSize||0];var w=Math.min(B,e),m;for(m=0;4>m;++m){var r=u[m];c=2>m||2===m&&/%$/.test(r);u[m]=G(r,[B,e,w,u[2]][m])+(c?C:0)}u[3]>u[2]&&(u[3]=u[2]);
return u},getStartAndEndRadians:function(c,e){c=F(c)?c:0;e=F(e)&&e>c&&360>e-c?e:c+360;return{start:H*(c+-90),end:H*(e+-90)}}}});M(J,"parts/PieSeries.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.clamp,I=e.defined,G=e.isNumber,H=e.pick,v=e.relativeLength,q=e.setAnimation,C=c.addEvent;e=c.CenteredSeriesMixin;var B=e.getStartAndEndRadians,u=c.merge,w=c.noop,m=c.Point,r=c.Series,D=c.seriesType,A=c.fireEvent;D("pie","line",{center:[null,null],clip:!1,colorByPoint:!0,dataLabels:{allowOverlap:!0,
connectorPadding:5,connectorShape:"fixedOffset",crookDistance:"70%",distance:30,enabled:!0,formatter:function(){return this.point.isNull?void 0:this.point.name},softConnector:!0,x:0},fillColor:void 0,ignoreHiddenPoint:!0,inactiveOtherPoints:!0,legendType:"point",marker:null,size:null,showInLegend:!1,slicedOffset:10,stickyTracking:!1,tooltip:{followPointer:!0},borderColor:"#ffffff",borderWidth:1,lineWidth:void 0,states:{hover:{brightness:.1}}},{isCartesian:!1,requireSorting:!1,directTouch:!0,noSharedTooltip:!0,
trackerGroups:["group","dataLabelsGroup"],axisTypes:[],pointAttribs:c.seriesTypes.column.prototype.pointAttribs,animate:function(c){var b=this,a=b.points,d=b.startAngleRad;c||(a.forEach(function(a){var c=a.graphic,f=a.shapeArgs;c&&f&&(c.attr({r:H(a.startR,b.center&&b.center[3]/2),start:d,end:d}),c.animate({r:f.r,start:f.start,end:f.end},b.options.animation))}),b.animate=null)},hasData:function(){return!!this.processedXData.length},updateTotals:function(){var c,b=0,a=this.points,d=a.length,e=this.options.ignoreHiddenPoint;
for(c=0;c<d;c++){var k=a[c];b+=e&&!k.visible?0:k.isNull?0:k.y}this.total=b;for(c=0;c<d;c++)k=a[c],k.percentage=0<b&&(k.visible||!e)?k.y/b*100:0,k.total=b},generatePoints:function(){r.prototype.generatePoints.call(this);this.updateTotals()},getX:function(c,b,a){var d=this.center,f=this.radii?this.radii[a.index]:d[2]/2;c=Math.asin(F((c-d[1])/(f+a.labelDistance),-1,1));return d[0]+(b?-1:1)*Math.cos(c)*(f+a.labelDistance)+(0<a.labelDistance?(b?-1:1)*this.options.dataLabels.padding:0)},translate:function(c){this.generatePoints();
var b=0,a=this.options,d=a.slicedOffset,f=d+(a.borderWidth||0),e=B(a.startAngle,a.endAngle),l=this.startAngleRad=e.start;e=(this.endAngleRad=e.end)-l;var m=this.points,p=a.dataLabels.distance;a=a.ignoreHiddenPoint;var g,t=m.length;c||(this.center=c=this.getCenter());for(g=0;g<t;g++){var q=m[g];var r=l+b*e;if(!a||q.visible)b+=q.percentage/100;var u=l+b*e;q.shapeType="arc";q.shapeArgs={x:c[0],y:c[1],r:c[2]/2,innerR:c[3]/2,start:Math.round(1E3*r)/1E3,end:Math.round(1E3*u)/1E3};q.labelDistance=H(q.options.dataLabels&&
q.options.dataLabels.distance,p);q.labelDistance=v(q.labelDistance,q.shapeArgs.r);this.maxLabelDistance=Math.max(this.maxLabelDistance||0,q.labelDistance);u=(u+r)/2;u>1.5*Math.PI?u-=2*Math.PI:u<-Math.PI/2&&(u+=2*Math.PI);q.slicedTranslation={translateX:Math.round(Math.cos(u)*d),translateY:Math.round(Math.sin(u)*d)};var n=Math.cos(u)*c[2]/2;var z=Math.sin(u)*c[2]/2;q.tooltipPos=[c[0]+.7*n,c[1]+.7*z];q.half=u<-Math.PI/2||u>Math.PI/2?1:0;q.angle=u;r=Math.min(f,q.labelDistance/5);q.labelPosition={natural:{x:c[0]+
n+Math.cos(u)*q.labelDistance,y:c[1]+z+Math.sin(u)*q.labelDistance},"final":{},alignment:0>q.labelDistance?"center":q.half?"right":"left",connectorPosition:{breakAt:{x:c[0]+n+Math.cos(u)*r,y:c[1]+z+Math.sin(u)*r},touchingSliceAt:{x:c[0]+n,y:c[1]+z}}}}A(this,"afterTranslate")},drawEmpty:function(){var c=this.options;if(0===this.total){var b=this.center[0];var a=this.center[1];this.graph||(this.graph=this.chart.renderer.circle(b,a,0).addClass("highcharts-graph").add(this.group));this.graph.animate({"stroke-width":c.borderWidth,
cx:b,cy:a,r:this.center[2]/2,fill:c.fillColor||"none",stroke:c.color||"#cccccc"})}else this.graph&&(this.graph=this.graph.destroy())},redrawPoints:function(){var c=this,b=c.chart,a=b.renderer,d,e,k,l,m=c.options.shadow;this.drawEmpty();!m||c.shadowGroup||b.styledMode||(c.shadowGroup=a.g("shadow").attr({zIndex:-1}).add(c.group));c.points.forEach(function(f){var g={};e=f.graphic;if(!f.isNull&&e){l=f.shapeArgs;d=f.getTranslate();if(!b.styledMode){var h=f.shadowGroup;m&&!h&&(h=f.shadowGroup=a.g("shadow").add(c.shadowGroup));
h&&h.attr(d);k=c.pointAttribs(f,f.selected&&"select")}f.delayedRendering?(e.setRadialReference(c.center).attr(l).attr(d),b.styledMode||e.attr(k).attr({"stroke-linejoin":"round"}).shadow(m,h),f.delayedRendering=!1):(e.setRadialReference(c.center),b.styledMode||u(!0,g,k),u(!0,g,l,d),e.animate(g));e.attr({visibility:f.visible?"inherit":"hidden"});e.addClass(f.getClassName())}else e&&(f.graphic=e.destroy())})},drawPoints:function(){var c=this.chart.renderer;this.points.forEach(function(b){b.graphic||
(b.graphic=c[b.shapeType](b.shapeArgs).add(b.series.group),b.delayedRendering=!0)})},searchPoint:w,sortByAngle:function(c,b){c.sort(function(a,d){return"undefined"!==typeof a.angle&&(d.angle-a.angle)*b})},drawLegendSymbol:c.LegendSymbolMixin.drawRectangle,getCenter:e.getCenter,getSymbol:w,drawGraph:null},{init:function(){m.prototype.init.apply(this,arguments);var c=this;c.name=H(c.name,"Slice");var b=function(a){c.slice("select"===a.type)};C(c,"select",b);C(c,"unselect",b);return c},isValid:function(){return G(this.y)&&
0<=this.y},setVisible:function(c,b){var a=this,d=a.series,f=d.chart,e=d.options.ignoreHiddenPoint;b=H(b,e);c!==a.visible&&(a.visible=a.options.visible=c="undefined"===typeof c?!a.visible:c,d.options.data[d.data.indexOf(a)]=a.options,["graphic","dataLabel","connector","shadowGroup"].forEach(function(b){if(a[b])a[b][c?"show":"hide"](!0)}),a.legendItem&&f.legend.colorizeItem(a,c),c||"hover"!==a.state||a.setState(""),e&&(d.isDirty=!0),b&&f.redraw())},slice:function(c,b,a){var d=this.series;q(a,d.chart);
H(b,!0);this.sliced=this.options.sliced=I(c)?c:!this.sliced;d.options.data[d.data.indexOf(this)]=this.options;this.graphic.animate(this.getTranslate());this.shadowGroup&&this.shadowGroup.animate(this.getTranslate())},getTranslate:function(){return this.sliced?this.slicedTranslation:{translateX:0,translateY:0}},haloPath:function(c){var b=this.shapeArgs;return this.sliced||!this.visible?[]:this.series.chart.renderer.symbols.arc(b.x,b.y,b.r+c,b.r+c,{innerR:b.r-1,start:b.start,end:b.end})},connectorShapes:{fixedOffset:function(c,
b,a){var d=b.breakAt;b=b.touchingSliceAt;return["M",c.x,c.y].concat(a.softConnector?["C",c.x+("left"===c.alignment?-5:5),c.y,2*d.x-b.x,2*d.y-b.y,d.x,d.y]:["L",d.x,d.y]).concat(["L",b.x,b.y])},straight:function(c,b){b=b.touchingSliceAt;return["M",c.x,c.y,"L",b.x,b.y]},crookedLine:function(c,b,a){b=b.touchingSliceAt;var d=this.series,e=d.center[0],f=d.chart.plotWidth,l=d.chart.plotLeft;d=c.alignment;var m=this.shapeArgs.r;a=v(a.crookDistance,1);a="left"===d?e+m+(f+l-e-m)*(1-a):l+(e-m)*a;e=["L",a,c.y];
if("left"===d?a>c.x||a<b.x:a<c.x||a>b.x)e=[];return["M",c.x,c.y].concat(e).concat(["L",b.x,b.y])}},getConnectorPath:function(){var c=this.labelPosition,b=this.series.options.dataLabels,a=b.connectorShape,d=this.connectorShapes;d[a]&&(a=d[a]);return a.call(this,{x:c.final.x,y:c.final.y,alignment:c.alignment},c.connectorPosition,b)}});""});M(J,"parts/DataLabels.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.animObject,I=e.arrayMax,G=e.clamp,H=e.defined,v=e.extend,q=e.isArray,
C=e.objectEach,B=e.pick,u=e.relativeLength,w=e.splat,m=c.format,r=c.merge;e=c.noop;var D=c.Series,A=c.seriesTypes,f=c.stableSort;c.distribute=function(b,a,d){function e(a,b){return a.target-b.target}var k,l=!0,m=b,p=[];var g=0;var t=m.reducedLen||a;for(k=b.length;k--;)g+=b[k].size;if(g>t){f(b,function(a,b){return(b.rank||0)-(a.rank||0)});for(g=k=0;g<=t;)g+=b[k].size,k++;p=b.splice(k-1,b.length)}f(b,e);for(b=b.map(function(a){return{size:a.size,targets:[a.target],align:B(a.align,.5)}});l;){for(k=b.length;k--;)l=
b[k],g=(Math.min.apply(0,l.targets)+Math.max.apply(0,l.targets))/2,l.pos=G(g-l.size*l.align,0,a-l.size);k=b.length;for(l=!1;k--;)0<k&&b[k-1].pos+b[k-1].size>b[k].pos&&(b[k-1].size+=b[k].size,b[k-1].targets=b[k-1].targets.concat(b[k].targets),b[k-1].align=.5,b[k-1].pos+b[k-1].size>a&&(b[k-1].pos=a-b[k-1].size),b.splice(k,1),l=!0)}m.push.apply(m,p);k=0;b.some(function(b){var e=0;if(b.targets.some(function(){m[k].pos=b.pos+e;if(Math.abs(m[k].pos-m[k].target)>d)return m.slice(0,k+1).forEach(function(a){delete a.pos}),
m.reducedLen=(m.reducedLen||a)-.1*a,m.reducedLen>.1*a&&c.distribute(m,a,d),!0;e+=m[k].size;k++}))return!0});f(m,e)};D.prototype.drawDataLabels=function(){function b(a,b){var d=b.filter;return d?(b=d.operator,a=a[d.property],d=d.value,">"===b&&a>d||"<"===b&&a<d||">="===b&&a>=d||"<="===b&&a<=d||"=="===b&&a==d||"==="===b&&a===d?!0:!1):!0}function a(a,b){var d=[],c;if(q(a)&&!q(b))d=a.map(function(a){return r(a,b)});else if(q(b)&&!q(a))d=b.map(function(b){return r(a,b)});else if(q(a)||q(b))for(c=Math.max(a.length,
b.length);c--;)d[c]=r(a[c],b[c]);else d=r(a,b);return d}var d=this,e=d.chart,f=d.options,l=f.dataLabels,y=d.points,p,g=d.hasRendered||0,t=F(f.animation).duration,x=Math.min(t,200),u=!e.renderer.forExport&&B(l.defer,0<x),v=e.renderer;l=a(a(e.options.plotOptions&&e.options.plotOptions.series&&e.options.plotOptions.series.dataLabels,e.options.plotOptions&&e.options.plotOptions[d.type]&&e.options.plotOptions[d.type].dataLabels),l);c.fireEvent(this,"drawDataLabels");if(q(l)||l.enabled||d._hasPointLabels){var n=
d.plotGroup("dataLabelsGroup","data-labels",u&&!g?"hidden":"inherit",l.zIndex||6);u&&(n.attr({opacity:+g}),g||setTimeout(function(){var a=d.dataLabelsGroup;a&&(d.visible&&n.show(!0),a[f.animation?"animate":"attr"]({opacity:1},{duration:x}))},t-x));y.forEach(function(c){p=w(a(l,c.dlOptions||c.options&&c.options.dataLabels));p.forEach(function(a,g){var h=a.enabled&&(!c.isNull||c.dataLabelOnNull)&&b(c,a),l=c.dataLabels?c.dataLabels[g]:c.dataLabel,p=c.connectors?c.connectors[g]:c.connector,k=B(a.distance,
c.labelDistance),t=!l;if(h){var q=c.getLabelConfig();var r=B(a[c.formatPrefix+"Format"],a.format);q=H(r)?m(r,q,e):(a[c.formatPrefix+"Formatter"]||a.formatter).call(q,a);r=a.style;var y=a.rotation;e.styledMode||(r.color=B(a.color,r.color,d.color,"#000000"),"contrast"===r.color?(c.contrastColor=v.getContrast(c.color||d.color),r.color=!H(k)&&a.inside||0>k||f.stacking?c.contrastColor:"#000000"):delete c.contrastColor,f.cursor&&(r.cursor=f.cursor));var x={r:a.borderRadius||0,rotation:y,padding:a.padding,
zIndex:1};e.styledMode||(x.fill=a.backgroundColor,x.stroke=a.borderColor,x["stroke-width"]=a.borderWidth);C(x,function(a,b){"undefined"===typeof a&&delete x[b]})}!l||h&&H(q)?h&&H(q)&&(l?x.text=q:(c.dataLabels=c.dataLabels||[],l=c.dataLabels[g]=y?v.text(q,0,-9999).addClass("highcharts-data-label"):v.label(q,0,-9999,a.shape,null,null,a.useHTML,null,"data-label"),g||(c.dataLabel=l),l.addClass(" highcharts-data-label-color-"+c.colorIndex+" "+(a.className||"")+(a.useHTML?" highcharts-tracker":""))),l.options=
a,l.attr(x),e.styledMode||l.css(r).shadow(a.shadow),l.added||l.add(n),a.textPath&&!a.useHTML&&(l.setTextPath(c.getDataLabelPath&&c.getDataLabelPath(l)||c.graphic,a.textPath),c.dataLabelPath&&!a.textPath.enabled&&(c.dataLabelPath=c.dataLabelPath.destroy())),d.alignDataLabel(c,l,a,null,t)):(c.dataLabel=c.dataLabel&&c.dataLabel.destroy(),c.dataLabels&&(1===c.dataLabels.length?delete c.dataLabels:delete c.dataLabels[g]),g||delete c.dataLabel,p&&(c.connector=c.connector.destroy(),c.connectors&&(1===c.connectors.length?
delete c.connectors:delete c.connectors[g])))})})}c.fireEvent(this,"afterDrawDataLabels")};D.prototype.alignDataLabel=function(b,a,c,e,f){var d=this,h=this.chart,p=this.isCartesian&&h.inverted,g=this.enabledDataSorting,k=B(b.dlBox&&b.dlBox.centerX,b.plotX,-9999),m=B(b.plotY,-9999),q=a.getBBox(),r=c.rotation,n=c.align,u=h.isInsidePlot(k,Math.round(m),p),w="justify"===B(c.overflow,g?"none":"justify"),A=this.visible&&(b.series.forceDL||g&&!w||u||e&&h.isInsidePlot(k,p?e.x+1:e.y+e.height-1,p));var C=function(c){g&&
d.xAxis&&!w&&d.setDataLabelStartPos(b,a,f,u,c)};if(A){var D=h.renderer.fontMetrics(h.styledMode?void 0:c.style.fontSize,a).b;e=v({x:p?this.yAxis.len-m:k,y:Math.round(p?this.xAxis.len-k:m),width:0,height:0},e);v(c,{width:q.width,height:q.height});r?(w=!1,k=h.renderer.rotCorr(D,r),k={x:e.x+c.x+e.width/2+k.x,y:e.y+c.y+{top:0,middle:.5,bottom:1}[c.verticalAlign]*e.height},C(k),a[f?"attr":"animate"](k).attr({align:n}),C=(r+720)%360,C=180<C&&360>C,"left"===n?k.y-=C?q.height:0:"center"===n?(k.x-=q.width/
2,k.y-=q.height/2):"right"===n&&(k.x-=q.width,k.y-=C?0:q.height),a.placed=!0,a.alignAttr=k):(C(e),a.align(c,null,e),k=a.alignAttr);w&&0<=e.height?this.justifyDataLabel(a,c,k,q,e,f):B(c.crop,!0)&&(A=h.isInsidePlot(k.x,k.y)&&h.isInsidePlot(k.x+q.width,k.y+q.height));if(c.shape&&!r)a[f?"attr":"animate"]({anchorX:p?h.plotWidth-b.plotY:b.plotX,anchorY:p?h.plotHeight-b.plotX:b.plotY})}f&&g&&(a.placed=!1);A||g&&!w||(a.hide(!0),a.placed=!1)};D.prototype.setDataLabelStartPos=function(b,a,c,e,f){var d=this.chart,
h=d.inverted,p=this.xAxis,g=p.reversed,k=h?a.height/2:a.width/2;b=(b=b.pointWidth)?b/2:0;p=h?f.x:g?-k-b:p.width-k+b;f=h?g?this.yAxis.height-k+b:-k-b:f.y;a.startXPos=p;a.startYPos=f;e?"hidden"===a.visibility&&(a.show(),a.attr({opacity:0}).animate({opacity:1})):a.attr({opacity:1}).animate({opacity:0},void 0,a.hide);d.hasRendered&&(c&&a.attr({x:a.startXPos,y:a.startYPos}),a.placed=!0)};D.prototype.justifyDataLabel=function(b,a,c,e,f,l){var d=this.chart,h=a.align,g=a.verticalAlign,k=b.box?0:b.padding||
0;var m=c.x+k;if(0>m){"right"===h?(a.align="left",a.inside=!0):a.x=-m;var q=!0}m=c.x+e.width-k;m>d.plotWidth&&("left"===h?(a.align="right",a.inside=!0):a.x=d.plotWidth-m,q=!0);m=c.y+k;0>m&&("bottom"===g?(a.verticalAlign="top",a.inside=!0):a.y=-m,q=!0);m=c.y+e.height-k;m>d.plotHeight&&("top"===g?(a.verticalAlign="bottom",a.inside=!0):a.y=d.plotHeight-m,q=!0);q&&(b.placed=!l,b.align(a,null,f));return q};A.pie&&(A.pie.prototype.dataLabelPositioners={radialDistributionY:function(b){return b.top+b.distributeBox.pos},
radialDistributionX:function(b,a,c,e){return b.getX(c<a.top+2||c>a.bottom-2?e:c,a.half,a)},justify:function(b,a,c){return c[0]+(b.half?-1:1)*(a+b.labelDistance)},alignToPlotEdges:function(b,a,c,e){b=b.getBBox().width;return a?b+e:c-b-e},alignToConnectors:function(b,a,c,e){var d=0,f;b.forEach(function(a){f=a.dataLabel.getBBox().width;f>d&&(d=f)});return a?d+e:c-d-e}},A.pie.prototype.drawDataLabels=function(){var b=this,a=b.data,d,e=b.chart,f=b.options.dataLabels,l=f.connectorPadding,m,p=e.plotWidth,
g=e.plotHeight,q=e.plotLeft,x=Math.round(e.chartWidth/3),u,v=b.center,n=v[2]/2,z=v[1],w,A,C,F,G=[[],[]],J,K,M,S,U=[0,0,0,0],X=b.dataLabelPositioners,aa;b.visible&&(f.enabled||b._hasPointLabels)&&(a.forEach(function(a){a.dataLabel&&a.visible&&a.dataLabel.shortened&&(a.dataLabel.attr({width:"auto"}).css({width:"auto",textOverflow:"clip"}),a.dataLabel.shortened=!1)}),D.prototype.drawDataLabels.apply(b),a.forEach(function(a){a.dataLabel&&(a.visible?(G[a.half].push(a),a.dataLabel._pos=null,!H(f.style.width)&&
!H(a.options.dataLabels&&a.options.dataLabels.style&&a.options.dataLabels.style.width)&&a.dataLabel.getBBox().width>x&&(a.dataLabel.css({width:.7*x}),a.dataLabel.shortened=!0)):(a.dataLabel=a.dataLabel.destroy(),a.dataLabels&&1===a.dataLabels.length&&delete a.dataLabels))}),G.forEach(function(a,h){var k=a.length,m=[],r;if(k){b.sortByAngle(a,h-.5);if(0<b.maxLabelDistance){var t=Math.max(0,z-n-b.maxLabelDistance);var x=Math.min(z+n+b.maxLabelDistance,e.plotHeight);a.forEach(function(a){0<a.labelDistance&&
a.dataLabel&&(a.top=Math.max(0,z-n-a.labelDistance),a.bottom=Math.min(z+n+a.labelDistance,e.plotHeight),r=a.dataLabel.getBBox().height||21,a.distributeBox={target:a.labelPosition.natural.y-a.top+r/2,size:r,rank:a.y},m.push(a.distributeBox))});t=x+r-t;c.distribute(m,t,t/5)}for(S=0;S<k;S++){d=a[S];C=d.labelPosition;w=d.dataLabel;M=!1===d.visible?"hidden":"inherit";K=t=C.natural.y;m&&H(d.distributeBox)&&("undefined"===typeof d.distributeBox.pos?M="hidden":(F=d.distributeBox.size,K=X.radialDistributionY(d)));
delete d.positionIndex;if(f.justify)J=X.justify(d,n,v);else switch(f.alignTo){case "connectors":J=X.alignToConnectors(a,h,p,q);break;case "plotEdges":J=X.alignToPlotEdges(w,h,p,q);break;default:J=X.radialDistributionX(b,d,K,t)}w._attr={visibility:M,align:C.alignment};w._pos={x:J+f.x+({left:l,right:-l}[C.alignment]||0),y:K+f.y-10};C.final.x=J;C.final.y=K;B(f.crop,!0)&&(A=w.getBBox().width,t=null,J-A<l&&1===h?(t=Math.round(A-J+l),U[3]=Math.max(t,U[3])):J+A>p-l&&0===h&&(t=Math.round(J+A-p+l),U[1]=Math.max(t,
U[1])),0>K-F/2?U[0]=Math.max(Math.round(-K+F/2),U[0]):K+F/2>g&&(U[2]=Math.max(Math.round(K+F/2-g),U[2])),w.sideOverflow=t)}}}),0===I(U)||this.verifyDataLabelOverflow(U))&&(this.placeDataLabels(),this.points.forEach(function(a){aa=r(f,a.options.dataLabels);if(m=B(aa.connectorWidth,1)){var c;u=a.connector;if((w=a.dataLabel)&&w._pos&&a.visible&&0<a.labelDistance){M=w._attr.visibility;if(c=!u)a.connector=u=e.renderer.path().addClass("highcharts-data-label-connector  highcharts-color-"+a.colorIndex+(a.className?
" "+a.className:"")).add(b.dataLabelsGroup),e.styledMode||u.attr({"stroke-width":m,stroke:aa.connectorColor||a.color||"#666666"});u[c?"attr":"animate"]({d:a.getConnectorPath()});u.attr("visibility",M)}else u&&(a.connector=u.destroy())}}))},A.pie.prototype.placeDataLabels=function(){this.points.forEach(function(b){var a=b.dataLabel,c;a&&b.visible&&((c=a._pos)?(a.sideOverflow&&(a._attr.width=Math.max(a.getBBox().width-a.sideOverflow,0),a.css({width:a._attr.width+"px",textOverflow:(this.options.dataLabels.style||
{}).textOverflow||"ellipsis"}),a.shortened=!0),a.attr(a._attr),a[a.moved?"animate":"attr"](c),a.moved=!0):a&&a.attr({y:-9999}));delete b.distributeBox},this)},A.pie.prototype.alignDataLabel=e,A.pie.prototype.verifyDataLabelOverflow=function(b){var a=this.center,c=this.options,e=c.center,f=c.minSize||80,l=null!==c.size;if(!l){if(null!==e[0])var m=Math.max(a[2]-Math.max(b[1],b[3]),f);else m=Math.max(a[2]-b[1]-b[3],f),a[0]+=(b[3]-b[1])/2;null!==e[1]?m=G(m,f,a[2]-Math.max(b[0],b[2])):(m=G(m,f,a[2]-b[0]-
b[2]),a[1]+=(b[0]-b[2])/2);m<a[2]?(a[2]=m,a[3]=Math.min(u(c.innerSize||0,m),m),this.translate(a),this.drawDataLabels&&this.drawDataLabels()):l=!0}return l});A.column&&(A.column.prototype.alignDataLabel=function(b,a,c,e,f){var d=this.chart.inverted,h=b.series,p=b.dlBox||b.shapeArgs,g=B(b.below,b.plotY>B(this.translatedThreshold,h.yAxis.len)),k=B(c.inside,!!this.options.stacking);p&&(e=r(p),0>e.y&&(e.height+=e.y,e.y=0),p=e.y+e.height-h.yAxis.len,0<p&&(e.height-=p),d&&(e={x:h.yAxis.len-e.y-e.height,
y:h.xAxis.len-e.x-e.width,width:e.height,height:e.width}),k||(d?(e.x+=g?0:e.width,e.width=0):(e.y+=g?e.height:0,e.height=0)));c.align=B(c.align,!d||k?"center":g?"right":"left");c.verticalAlign=B(c.verticalAlign,d||k?"middle":g?"top":"bottom");D.prototype.alignDataLabel.call(this,b,a,c,e,f);e&&(0>=e.height&&e.y===this.chart.plotHeight||0>=e.width&&0===e.x)&&(a.hide(!0),a.placed=!1);c.inside&&b.contrastColor&&a.css({color:b.contrastColor})})});M(J,"modules/overlapping-datalabels.src.js",[J["parts/Globals.js"],
J["parts/Utilities.js"]],function(c,e){var F=e.isArray,I=e.objectEach,G=e.pick;e=c.Chart;var H=c.addEvent,v=c.fireEvent;H(e,"render",function(){var c=[];(this.labelCollectors||[]).forEach(function(e){c=c.concat(e())});(this.yAxis||[]).forEach(function(e){e.options.stackLabels&&!e.options.stackLabels.allowOverlap&&I(e.stacks,function(e){I(e,function(e){c.push(e.label)})})});(this.series||[]).forEach(function(e){var q=e.options.dataLabels;e.visible&&(!1!==q.enabled||e._hasPointLabels)&&e.points.forEach(function(e){e.visible&&
(F(e.dataLabels)?e.dataLabels:e.dataLabel?[e.dataLabel]:[]).forEach(function(q){var m=q.options;q.labelrank=G(m.labelrank,e.labelrank,e.shapeArgs&&e.shapeArgs.height);m.allowOverlap||c.push(q)})})});this.hideOverlappingLabels(c)});e.prototype.hideOverlappingLabels=function(c){var e=this,q=c.length,u=e.renderer,w,m,r,D=!1;var A=function(a){var b=a.box?0:a.padding||0;var c=0;if(a&&(!a.alignAttr||a.placed)){var e=a.alignAttr||{x:a.attr("x"),y:a.attr("y")};var f=a.parentGroup;a.width||(c=a.getBBox(),
a.width=c.width,a.height=c.height,c=u.fontMetrics(null,a.element).h);return{x:e.x+(f.translateX||0)+b,y:e.y+(f.translateY||0)+b-c,width:a.width-2*b,height:a.height-2*b}}};for(m=0;m<q;m++)if(w=c[m])w.oldOpacity=w.opacity,w.newOpacity=1,w.absoluteBox=A(w);c.sort(function(a,b){return(b.labelrank||0)-(a.labelrank||0)});for(m=0;m<q;m++){var f=(A=c[m])&&A.absoluteBox;for(w=m+1;w<q;++w){var b=(r=c[w])&&r.absoluteBox;!f||!b||A===r||0===A.newOpacity||0===r.newOpacity||b.x>f.x+f.width||b.x+b.width<f.x||b.y>
f.y+f.height||b.y+b.height<f.y||((A.labelrank<r.labelrank?A:r).newOpacity=0)}}c.forEach(function(a){var b;if(a){var c=a.newOpacity;a.oldOpacity!==c&&(a.alignAttr&&a.placed?(c?a.show(!0):b=function(){a.hide(!0);a.placed=!1},D=!0,a.alignAttr.opacity=c,a[a.isOld?"animate":"attr"](a.alignAttr,null,b),v(e,"afterHideOverlappingLabel")):a.attr({opacity:c}));a.isOld=!0}});D&&v(e,"afterHideAllOverlappingLabels")}});M(J,"parts/Interaction.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=
e.defined,I=e.extend,G=e.isArray,H=e.isObject,v=e.objectEach,q=e.pick,C=c.addEvent;e=c.Chart;var B=c.createElement,u=c.css,w=c.defaultOptions,m=c.defaultPlotOptions,r=c.fireEvent,D=c.hasTouch,A=c.Legend,f=c.merge,b=c.Point,a=c.Series,d=c.seriesTypes,h=c.svg;var k=c.TrackerMixin={drawTrackerPoint:function(){var a=this,b=a.chart,c=b.pointer,d=function(a){var b=c.getPointFromEvent(a);"undefined"!==typeof b&&(c.isDirectTouch=!0,b.onMouseOver(a))},e;a.points.forEach(function(a){e=G(a.dataLabels)?a.dataLabels:
a.dataLabel?[a.dataLabel]:[];a.graphic&&(a.graphic.element.point=a);e.forEach(function(b){b.div?b.div.point=a:b.element.point=a})});a._hasTracking||(a.trackerGroups.forEach(function(e){if(a[e]){a[e].addClass("highcharts-tracker").on("mouseover",d).on("mouseout",function(a){c.onTrackerMouseOut(a)});if(D)a[e].on("touchstart",d);!b.styledMode&&a.options.cursor&&a[e].css(u).css({cursor:a.options.cursor})}}),a._hasTracking=!0);r(this,"afterDrawTracker")},drawTrackerGraph:function(){var a=this,b=a.options,
c=b.trackByArea,d=[].concat(c?a.areaPath:a.graphPath),e=d.length,f=a.chart,k=f.pointer,m=f.renderer,n=f.options.tooltip.snap,q=a.tracker,u,v=function(){if(f.hoverSeries!==a)a.onMouseOver()},w="rgba(192,192,192,"+(h?.0001:.002)+")";if(e&&!c)for(u=e+1;u--;)"M"===d[u]&&d.splice(u+1,0,d[u+1]-n,d[u+2],"L"),(u&&"M"===d[u]||u===e)&&d.splice(u,0,"L",d[u-2]+n,d[u-1]);q?q.attr({d:d}):a.graph&&(a.tracker=m.path(d).attr({visibility:a.visible?"visible":"hidden",zIndex:2}).addClass(c?"highcharts-tracker-area":
"highcharts-tracker-line").add(a.group),f.styledMode||a.tracker.attr({"stroke-linejoin":"round",stroke:w,fill:c?w:"none","stroke-width":a.graph.strokeWidth()+(c?0:2*n)}),[a.tracker,a.markerGroup].forEach(function(a){a.addClass("highcharts-tracker").on("mouseover",v).on("mouseout",function(a){k.onTrackerMouseOut(a)});b.cursor&&!f.styledMode&&a.css({cursor:b.cursor});if(D)a.on("touchstart",v)}));r(this,"afterDrawTracker")}};d.column&&(d.column.prototype.drawTracker=k.drawTrackerPoint);d.pie&&(d.pie.prototype.drawTracker=
k.drawTrackerPoint);d.scatter&&(d.scatter.prototype.drawTracker=k.drawTrackerPoint);I(A.prototype,{setItemEvents:function(a,c,d){var e=this,h=e.chart.renderer.boxWrapper,l=a instanceof b,p="highcharts-legend-"+(l?"point":"series")+"-active",k=e.chart.styledMode;(d?c:a.legendGroup).on("mouseover",function(){a.visible&&e.allItems.forEach(function(b){a!==b&&b.setState("inactive",!l)});a.setState("hover");a.visible&&h.addClass(p);k||c.css(e.options.itemHoverStyle)}).on("mouseout",function(){e.chart.styledMode||
c.css(f(a.visible?e.itemStyle:e.itemHiddenStyle));e.allItems.forEach(function(b){a!==b&&b.setState("",!l)});h.removeClass(p);a.setState()}).on("click",function(b){var c=function(){a.setVisible&&a.setVisible();e.allItems.forEach(function(b){a!==b&&b.setState(a.visible?"inactive":"",!l)})};h.removeClass(p);b={browserEvent:b};a.firePointEvent?a.firePointEvent("legendItemClick",b,c):r(a,"legendItemClick",b,c)})},createCheckboxForItem:function(a){a.checkbox=B("input",{type:"checkbox",className:"highcharts-legend-checkbox",
checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);C(a.checkbox,"click",function(b){r(a.series||a,"checkboxClick",{checked:b.target.checked,item:a},function(){a.select()})})}});I(e.prototype,{showResetZoom:function(){function a(){b.zoomOut()}var b=this,c=w.lang,d=b.options.chart.resetZoomButton,e=d.theme,f=e.states,h="chart"===d.relativeTo||"spaceBox"===d.relativeTo?null:"plotBox";r(this,"beforeShowResetZoom",null,function(){b.resetZoomButton=b.renderer.button(c.resetZoom,
null,null,a,e,f&&f.hover).attr({align:d.position.align,title:c.resetZoomTitle}).addClass("highcharts-reset-zoom").add().align(d.position,!1,h)});r(this,"afterShowResetZoom")},zoomOut:function(){r(this,"selection",{resetSelection:!0},this.zoom)},zoom:function(a){var b=this,c,d=b.pointer,e=!1,f=b.inverted?d.mouseDownX:d.mouseDownY;!a||a.resetSelection?(b.axes.forEach(function(a){c=a.zoom()}),d.initiated=!1):a.xAxis.concat(a.yAxis).forEach(function(a){var g=a.axis,h=b.inverted?g.left:g.top,l=b.inverted?
h+g.width:h+g.height,p=g.isXAxis,k=!1;if(!p&&f>=h&&f<=l||p||!F(f))k=!0;d[p?"zoomX":"zoomY"]&&k&&(c=g.zoom(a.min,a.max),g.displayBtn&&(e=!0))});var h=b.resetZoomButton;e&&!h?b.showResetZoom():!e&&H(h)&&(b.resetZoomButton=h.destroy());c&&b.redraw(q(b.options.chart.animation,a&&a.animation,100>b.pointCount))},pan:function(a,b){var c=this,d=c.hoverPoints,e=c.options.chart,f;b="object"===typeof b?b:{enabled:b,type:"x"};e&&e.panning&&(e.panning=b);var h=b.type;r(this,"pan",{originalEvent:a},function(){d&&
d.forEach(function(a){a.setState()});var b=[1];"xy"===h?b=[1,0]:"y"===h&&(b=[0]);b.forEach(function(b){var d=c[b?"xAxis":"yAxis"][0],e=d.options,g=d.horiz,h=a[g?"chartX":"chartY"];g=g?"mouseDownX":"mouseDownY";var l=c[g],k=(d.pointRange||0)/2,p=d.reversed&&!c.inverted||!d.reversed&&c.inverted?-1:1,m=d.getExtremes(),n=d.toValue(l-h,!0)+k*p;p=d.toValue(l+d.len-h,!0)-k*p;var q=p<n;l=q?p:n;n=q?n:p;p=Math.min(m.dataMin,k?m.min:d.toValue(d.toPixels(m.min)-d.minPixelPadding));k=Math.max(m.dataMax,k?m.max:
d.toValue(d.toPixels(m.max)+d.minPixelPadding));if(!e.ordinal){b&&(e=p-l,0<e&&(n+=e,l=p),e=n-k,0<e&&(n=k,l-=e));if(d.series.length&&l!==m.min&&n!==m.max&&b||d.panningState&&l>=d.panningState.startMin&&n<=d.panningState.startMax)d.setExtremes(l,n,!1,!1,{trigger:"pan"}),f=!0;c[g]=h}});f&&c.redraw(!1);u(c.container,{cursor:"move"})})}});I(b.prototype,{select:function(a,b){var c=this,d=c.series,e=d.chart;this.selectedStaging=a=q(a,!c.selected);c.firePointEvent(a?"select":"unselect",{accumulate:b},function(){c.selected=
c.options.selected=a;d.options.data[d.data.indexOf(c)]=c.options;c.setState(a&&"select");b||e.getSelectedPoints().forEach(function(a){var b=a.series;a.selected&&a!==c&&(a.selected=a.options.selected=!1,b.options.data[b.data.indexOf(a)]=a.options,a.setState(e.hoverPoints&&b.options.inactiveOtherPoints?"inactive":""),a.firePointEvent("unselect"))})});delete this.selectedStaging},onMouseOver:function(a){var b=this.series.chart,c=b.pointer;a=a?c.normalize(a):c.getChartCoordinatesFromPoint(this,b.inverted);
c.runPointActions(a,this)},onMouseOut:function(){var a=this.series.chart;this.firePointEvent("mouseOut");this.series.options.inactiveOtherPoints||(a.hoverPoints||[]).forEach(function(a){a.setState()});a.hoverPoints=a.hoverPoint=null},importEvents:function(){if(!this.hasImportedEvents){var a=this,b=f(a.series.options.point,a.options).events;a.events=b;v(b,function(b,d){c.isFunction(b)&&C(a,d,b)});this.hasImportedEvents=!0}},setState:function(a,b){var c=this.series,d=this.state,e=c.options.states[a||
"normal"]||{},f=m[c.type].marker&&c.options.marker,h=f&&!1===f.enabled,l=f&&f.states&&f.states[a||"normal"]||{},k=!1===l.enabled,u=c.stateMarkerGraphic,v=this.marker||{},y=c.chart,w=c.halo,A,B=f&&c.markerAttribs;a=a||"";if(!(a===this.state&&!b||this.selected&&"select"!==a||!1===e.enabled||a&&(k||h&&!1===l.enabled)||a&&v.states&&v.states[a]&&!1===v.states[a].enabled)){this.state=a;B&&(A=c.markerAttribs(this,a));if(this.graphic){d&&this.graphic.removeClass("highcharts-point-"+d);a&&this.graphic.addClass("highcharts-point-"+
a);if(!y.styledMode){var C=c.pointAttribs(this,a);var D=q(y.options.chart.animation,e.animation);c.options.inactiveOtherPoints&&((this.dataLabels||[]).forEach(function(a){a&&a.animate({opacity:C.opacity},D)}),this.connector&&this.connector.animate({opacity:C.opacity},D));this.graphic.animate(C,D)}A&&this.graphic.animate(A,q(y.options.chart.animation,l.animation,f.animation));u&&u.hide()}else{if(a&&l){d=v.symbol||c.symbol;u&&u.currentSymbol!==d&&(u=u.destroy());if(A)if(u)u[b?"animate":"attr"]({x:A.x,
y:A.y});else d&&(c.stateMarkerGraphic=u=y.renderer.symbol(d,A.x,A.y,A.width,A.height).add(c.markerGroup),u.currentSymbol=d);!y.styledMode&&u&&u.attr(c.pointAttribs(this,a))}u&&(u[a&&this.isInside?"show":"hide"](),u.element.point=this)}a=e.halo;e=(u=this.graphic||u)&&u.visibility||"inherit";a&&a.size&&u&&"hidden"!==e&&!this.isCluster?(w||(c.halo=w=y.renderer.path().add(u.parentGroup)),w.show()[b?"animate":"attr"]({d:this.haloPath(a.size)}),w.attr({"class":"highcharts-halo highcharts-color-"+q(this.colorIndex,
c.colorIndex)+(this.className?" "+this.className:""),visibility:e,zIndex:-1}),w.point=this,y.styledMode||w.attr(I({fill:this.color||c.color,"fill-opacity":a.opacity},a.attributes))):w&&w.point&&w.point.haloPath&&w.animate({d:w.point.haloPath(0)},null,w.hide);r(this,"afterSetState")}},haloPath:function(a){return this.series.chart.renderer.symbols.circle(Math.floor(this.plotX)-a,this.plotY-a,2*a,2*a)}});I(a.prototype,{onMouseOver:function(){var a=this.chart,b=a.hoverSeries;if(b&&b!==this)b.onMouseOut();
this.options.events.mouseOver&&r(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,b=this.chart,c=b.tooltip,d=b.hoverPoint;b.hoverSeries=null;if(d)d.onMouseOut();this&&a.events.mouseOut&&r(this,"mouseOut");!c||this.stickyTracking||c.shared&&!this.noSharedTooltip||c.hide();b.series.forEach(function(a){a.setState("",!0)})},setState:function(a,b){var c=this,d=c.options,e=c.graph,f=d.inactiveOtherPoints,h=d.states,l=d.lineWidth,k=d.opacity,m=q(h[a||
"normal"]&&h[a||"normal"].animation,c.chart.options.chart.animation);d=0;a=a||"";if(c.state!==a&&([c.group,c.markerGroup,c.dataLabelsGroup].forEach(function(b){b&&(c.state&&b.removeClass("highcharts-series-"+c.state),a&&b.addClass("highcharts-series-"+a))}),c.state=a,!c.chart.styledMode)){if(h[a]&&!1===h[a].enabled)return;a&&(l=h[a].lineWidth||l+(h[a].lineWidthPlus||0),k=q(h[a].opacity,k));if(e&&!e.dashstyle)for(h={"stroke-width":l},e.animate(h,m);c["zone-graph-"+d];)c["zone-graph-"+d].attr(h),d+=
1;f||[c.group,c.markerGroup,c.dataLabelsGroup,c.labelBySeries].forEach(function(a){a&&a.animate({opacity:k},m)})}b&&f&&c.points&&c.setAllPointsToState(a)},setAllPointsToState:function(a){this.points.forEach(function(b){b.setState&&b.setState(a)})},setVisible:function(a,b){var c=this,d=c.chart,e=c.legendItem,f=d.options.chart.ignoreHiddenSeries,h=c.visible;var k=(c.visible=a=c.options.visible=c.userOptions.visible="undefined"===typeof a?!h:a)?"show":"hide";["group","dataLabelsGroup","markerGroup",
"tracker","tt"].forEach(function(a){if(c[a])c[a][k]()});if(d.hoverSeries===c||(d.hoverPoint&&d.hoverPoint.series)===c)c.onMouseOut();e&&d.legend.colorizeItem(c,a);c.isDirty=!0;c.options.stacking&&d.series.forEach(function(a){a.options.stacking&&a.visible&&(a.isDirty=!0)});c.linkedSeries.forEach(function(b){b.setVisible(a,!1)});f&&(d.isDirtyBox=!0);r(c,k);!1!==b&&d.redraw()},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=this.options.selected=
"undefined"===typeof a?!this.selected:a;this.checkbox&&(this.checkbox.checked=a);r(this,a?"select":"unselect")},drawTracker:k.drawTrackerGraph})});M(J,"parts/Responsive.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.isArray,I=e.isObject,G=e.objectEach,H=e.pick,v=e.splat;e=c.Chart;e.prototype.setResponsive=function(e,v){var q=this.options.responsive,u=[],w=this.currentResponsive;!v&&q&&q.rules&&q.rules.forEach(function(e){"undefined"===typeof e._id&&(e._id=c.uniqueKey());
this.matchResponsiveRule(e,u)},this);v=c.merge.apply(0,u.map(function(e){return c.find(q.rules,function(c){return c._id===e}).chartOptions}));v.isResponsiveOptions=!0;u=u.toString()||void 0;u!==(w&&w.ruleIds)&&(w&&this.update(w.undoOptions,e,!0),u?(w=this.currentOptions(v),w.isResponsiveOptions=!0,this.currentResponsive={ruleIds:u,mergedOptions:v,undoOptions:w},this.update(v,e,!0)):this.currentResponsive=void 0)};e.prototype.matchResponsiveRule=function(c,e){var q=c.condition;(q.callback||function(){return this.chartWidth<=
H(q.maxWidth,Number.MAX_VALUE)&&this.chartHeight<=H(q.maxHeight,Number.MAX_VALUE)&&this.chartWidth>=H(q.minWidth,0)&&this.chartHeight>=H(q.minHeight,0)}).call(this)&&e.push(c._id)};e.prototype.currentOptions=function(c){function e(c,m,r,u){var w;G(c,function(c,b){if(!u&&-1<q.collectionsWithUpdate.indexOf(b))for(c=v(c),r[b]=[],w=0;w<c.length;w++)m[b][w]&&(r[b][w]={},e(c[w],m[b][w],r[b][w],u+1));else I(c)?(r[b]=F(c)?[]:{},e(c,m[b]||{},r[b],u+1)):r[b]="undefined"===typeof m[b]?null:m[b]})}var q=this,
u={};e(c,this.options,u,0);return u}});M(J,"masters/highcharts.src.js",[J["parts/Globals.js"],J["parts/Utilities.js"]],function(c,e){var F=e.extend;F(c,{animObject:e.animObject,arrayMax:e.arrayMax,arrayMin:e.arrayMin,attr:e.attr,correctFloat:e.correctFloat,defined:e.defined,destroyObjectProperties:e.destroyObjectProperties,discardElement:e.discardElement,erase:e.erase,extend:e.extend,extendClass:e.extendClass,isArray:e.isArray,isClass:e.isClass,isDOMElement:e.isDOMElement,isNumber:e.isNumber,isObject:e.isObject,
isString:e.isString,numberFormat:e.numberFormat,objectEach:e.objectEach,offset:e.offset,pad:e.pad,pick:e.pick,pInt:e.pInt,relativeLength:e.relativeLength,setAnimation:e.setAnimation,splat:e.splat,syncTimeout:e.syncTimeout,wrap:e.wrap});return c});J["masters/highcharts.src.js"]._modules=J;return J["masters/highcharts.src.js"]});
//# sourceMappingURL=highcharts.js.map

/***/ }),

/***/ "./node_modules/is-buffer/index.js":
/*!*****************************************!*\
  !*** ./node_modules/is-buffer/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

module.exports = function isBuffer (obj) {
  return obj != null && obj.constructor != null &&
    typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}


/***/ }),

/***/ "./node_modules/laravel-vue-pagination/dist/laravel-vue-pagination.common.js":
/*!***********************************************************************************!*\
  !*** ./node_modules/laravel-vue-pagination/dist/laravel-vue-pagination.common.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "0d58":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("ce10");
var enumBugKeys = __webpack_require__("e11e");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "11e9":
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__("52a7");
var createDesc = __webpack_require__("4630");
var toIObject = __webpack_require__("6821");
var toPrimitive = __webpack_require__("6a99");
var has = __webpack_require__("69a8");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__("9e1e") ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),

/***/ "1495":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var anObject = __webpack_require__("cb7c");
var getKeys = __webpack_require__("0d58");

module.exports = __webpack_require__("9e1e") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "230e":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var document = __webpack_require__("7726").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "2aba":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var hide = __webpack_require__("32e9");
var has = __webpack_require__("69a8");
var SRC = __webpack_require__("ca5a")('src');
var $toString = __webpack_require__("fa5b");
var TO_STRING = 'toString';
var TPL = ('' + $toString).split(TO_STRING);

__webpack_require__("8378").inspectSource = function (it) {
  return $toString.call(it);
};

(module.exports = function (O, key, val, safe) {
  var isFunction = typeof val == 'function';
  if (isFunction) has(val, 'name') || hide(val, 'name', key);
  if (O[key] === val) return;
  if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
  if (O === global) {
    O[key] = val;
  } else if (!safe) {
    delete O[key];
    hide(O, key, val);
  } else if (O[key]) {
    O[key] = val;
  } else {
    hide(O, key, val);
  }
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, TO_STRING, function toString() {
  return typeof this == 'function' && this[SRC] || $toString.call(this);
});


/***/ }),

/***/ "2aeb":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("cb7c");
var dPs = __webpack_require__("1495");
var enumBugKeys = __webpack_require__("e11e");
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("230e")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("fab2").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "2d00":
/***/ (function(module, exports) {

module.exports = false;


/***/ }),

/***/ "2d95":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "32e9":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var createDesc = __webpack_require__("4630");
module.exports = __webpack_require__("9e1e") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "4588":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "4630":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "52a7":
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "5537":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("8378");
var global = __webpack_require__("7726");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__("2d00") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "5ca1":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var core = __webpack_require__("8378");
var hide = __webpack_require__("32e9");
var redefine = __webpack_require__("2aba");
var ctx = __webpack_require__("9b43");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
  var key, own, out, exp;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    // export native or passed
    out = (own ? target : source)[key];
    // bind timers to global for call from export context
    exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // extend global
    if (target) redefine(target, key, out, type & $export.U);
    // export
    if (exports[key] != out) hide(exports, key, exp);
    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
  }
};
global.core = core;
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "5dbc":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var setPrototypeOf = __webpack_require__("8b97").set;
module.exports = function (that, target, C) {
  var S = target.constructor;
  var P;
  if (S !== C && typeof S == 'function' && (P = S.prototype) !== C.prototype && isObject(P) && setPrototypeOf) {
    setPrototypeOf(that, P);
  } return that;
};


/***/ }),

/***/ "613b":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("5537")('keys');
var uid = __webpack_require__("ca5a");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "626a":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("2d95");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "6821":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("626a");
var defined = __webpack_require__("be13");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "69a8":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "6a99":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("d3f4");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "7726":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "77f1":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("4588");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "79e5":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "8378":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.6.5' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "86cc":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("cb7c");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var toPrimitive = __webpack_require__("6a99");
var dP = Object.defineProperty;

exports.f = __webpack_require__("9e1e") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "8b97":
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__("d3f4");
var anObject = __webpack_require__("cb7c");
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__("9b43")(Function.call, __webpack_require__("11e9").f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),

/***/ "9093":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__("ce10");
var hiddenKeys = __webpack_require__("e11e").concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),

/***/ "9b43":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("d8e8");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "9def":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("4588");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "9e1e":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("79e5")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "aa77":
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__("5ca1");
var defined = __webpack_require__("be13");
var fails = __webpack_require__("79e5");
var spaces = __webpack_require__("fdef");
var space = '[' + spaces + ']';
var non = '\u200b\u0085';
var ltrim = RegExp('^' + space + space + '*');
var rtrim = RegExp(space + space + '*$');

var exporter = function (KEY, exec, ALIAS) {
  var exp = {};
  var FORCE = fails(function () {
    return !!spaces[KEY]() || non[KEY]() != non;
  });
  var fn = exp[KEY] = FORCE ? exec(trim) : spaces[KEY];
  if (ALIAS) exp[ALIAS] = fn;
  $export($export.P + $export.F * FORCE, 'String', exp);
};

// 1 -> String#trimLeft
// 2 -> String#trimRight
// 3 -> String#trim
var trim = exporter.trim = function (string, TYPE) {
  string = String(defined(string));
  if (TYPE & 1) string = string.replace(ltrim, '');
  if (TYPE & 2) string = string.replace(rtrim, '');
  return string;
};

module.exports = exporter;


/***/ }),

/***/ "be13":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "c366":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("6821");
var toLength = __webpack_require__("9def");
var toAbsoluteIndex = __webpack_require__("77f1");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "c5f6":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__("7726");
var has = __webpack_require__("69a8");
var cof = __webpack_require__("2d95");
var inheritIfRequired = __webpack_require__("5dbc");
var toPrimitive = __webpack_require__("6a99");
var fails = __webpack_require__("79e5");
var gOPN = __webpack_require__("9093").f;
var gOPD = __webpack_require__("11e9").f;
var dP = __webpack_require__("86cc").f;
var $trim = __webpack_require__("aa77").trim;
var NUMBER = 'Number';
var $Number = global[NUMBER];
var Base = $Number;
var proto = $Number.prototype;
// Opera ~12 has broken Object#toString
var BROKEN_COF = cof(__webpack_require__("2aeb")(proto)) == NUMBER;
var TRIM = 'trim' in String.prototype;

// 7.1.3 ToNumber(argument)
var toNumber = function (argument) {
  var it = toPrimitive(argument, false);
  if (typeof it == 'string' && it.length > 2) {
    it = TRIM ? it.trim() : $trim(it, 3);
    var first = it.charCodeAt(0);
    var third, radix, maxCode;
    if (first === 43 || first === 45) {
      third = it.charCodeAt(2);
      if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
    } else if (first === 48) {
      switch (it.charCodeAt(1)) {
        case 66: case 98: radix = 2; maxCode = 49; break; // fast equal /^0b[01]+$/i
        case 79: case 111: radix = 8; maxCode = 55; break; // fast equal /^0o[0-7]+$/i
        default: return +it;
      }
      for (var digits = it.slice(2), i = 0, l = digits.length, code; i < l; i++) {
        code = digits.charCodeAt(i);
        // parseInt parses a string to a first unavailable symbol
        // but ToNumber should return NaN if a string contains unavailable symbols
        if (code < 48 || code > maxCode) return NaN;
      } return parseInt(digits, radix);
    }
  } return +it;
};

if (!$Number(' 0o1') || !$Number('0b1') || $Number('+0x1')) {
  $Number = function Number(value) {
    var it = arguments.length < 1 ? 0 : value;
    var that = this;
    return that instanceof $Number
      // check on 1..constructor(foo) case
      && (BROKEN_COF ? fails(function () { proto.valueOf.call(that); }) : cof(that) != NUMBER)
        ? inheritIfRequired(new Base(toNumber(it)), that, $Number) : toNumber(it);
  };
  for (var keys = __webpack_require__("9e1e") ? gOPN(Base) : (
    // ES3:
    'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' +
    // ES6 (in case, if modules with ES6 Number statics required before):
    'EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,' +
    'MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger'
  ).split(','), j = 0, key; keys.length > j; j++) {
    if (has(Base, key = keys[j]) && !has($Number, key)) {
      dP($Number, key, gOPD(Base, key));
    }
  }
  $Number.prototype = proto;
  proto.constructor = $Number;
  __webpack_require__("2aba")(global, NUMBER, $Number);
}


/***/ }),

/***/ "c69a":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("9e1e") && !__webpack_require__("79e5")(function () {
  return Object.defineProperty(__webpack_require__("230e")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "ca5a":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "cb7c":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "ce10":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("69a8");
var toIObject = __webpack_require__("6821");
var arrayIndexOf = __webpack_require__("c366")(false);
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "d3f4":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "d8e8":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "e11e":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "fa5b":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("5537")('native-function-to-string', Function.toString);


/***/ }),

/***/ "fab2":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("7726").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var i
  if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js(\?.*)?$/))) {
    __webpack_require__.p = i[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"0fa2e68e-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/LaravelVuePagination.vue?vue&type=template&id=7f71b5a7&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('renderless-laravel-vue-pagination',{attrs:{"data":_vm.data,"limit":_vm.limit,"show-disabled":_vm.showDisabled,"size":_vm.size,"align":_vm.align},on:{"pagination-change-page":_vm.onPaginationChangePage},scopedSlots:_vm._u([{key:"default",fn:function(ref){
var data = ref.data;
var limit = ref.limit;
var showDisabled = ref.showDisabled;
var size = ref.size;
var align = ref.align;
var computed = ref.computed;
var prevButtonEvents = ref.prevButtonEvents;
var nextButtonEvents = ref.nextButtonEvents;
var pageButtonEvents = ref.pageButtonEvents;
return (computed.total > computed.perPage)?_c('ul',{staticClass:"pagination",class:{
            'pagination-sm': size == 'small',
            'pagination-lg': size == 'large',
            'justify-content-center': align == 'center',
            'justify-content-end': align == 'right'
        }},[(computed.prevPageUrl || showDisabled)?_c('li',{staticClass:"page-item pagination-prev-nav",class:{'disabled': !computed.prevPageUrl}},[_c('a',_vm._g({staticClass:"page-link",attrs:{"href":"#","aria-label":"Previous","tabindex":!computed.prevPageUrl && -1}},prevButtonEvents),[_vm._t("prev-nav",[_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("«")]),_c('span',{staticClass:"sr-only"},[_vm._v("Previous")])])],2)]):_vm._e(),_vm._l((computed.pageRange),function(page,key){return _c('li',{key:key,staticClass:"page-item pagination-page-nav",class:{ 'active': page == computed.currentPage }},[_c('a',_vm._g({staticClass:"page-link",attrs:{"href":"#"}},pageButtonEvents(page)),[_vm._v("\n                "+_vm._s(page)+"\n                "),(page == computed.currentPage)?_c('span',{staticClass:"sr-only"},[_vm._v("(current)")]):_vm._e()])])}),(computed.nextPageUrl || showDisabled)?_c('li',{staticClass:"page-item pagination-next-nav",class:{'disabled': !computed.nextPageUrl}},[_c('a',_vm._g({staticClass:"page-link",attrs:{"href":"#","aria-label":"Next","tabindex":!computed.nextPageUrl && -1}},nextButtonEvents),[_vm._t("next-nav",[_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("»")]),_c('span',{staticClass:"sr-only"},[_vm._v("Next")])])],2)]):_vm._e()],2):_vm._e()}}],null,true)})}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/LaravelVuePagination.vue?vue&type=template&id=7f71b5a7&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.number.constructor.js
var es6_number_constructor = __webpack_require__("c5f6");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/RenderlessLaravelVuePagination.vue?vue&type=script&lang=js&

/* harmony default export */ var RenderlessLaravelVuePaginationvue_type_script_lang_js_ = ({
  props: {
    data: {
      type: Object,
      default: function _default() {}
    },
    limit: {
      type: Number,
      default: 0
    },
    showDisabled: {
      type: Boolean,
      default: false
    },
    size: {
      type: String,
      default: 'default',
      validator: function validator(value) {
        return ['small', 'default', 'large'].indexOf(value) !== -1;
      }
    },
    align: {
      type: String,
      default: 'left',
      validator: function validator(value) {
        return ['left', 'center', 'right'].indexOf(value) !== -1;
      }
    }
  },
  computed: {
    isApiResource: function isApiResource() {
      return !!this.data.meta;
    },
    currentPage: function currentPage() {
      return this.isApiResource ? this.data.meta.current_page : this.data.current_page;
    },
    firstPageUrl: function firstPageUrl() {
      return this.isApiResource ? this.data.links.first : null;
    },
    from: function from() {
      return this.isApiResource ? this.data.meta.from : this.data.from;
    },
    lastPage: function lastPage() {
      return this.isApiResource ? this.data.meta.last_page : this.data.last_page;
    },
    lastPageUrl: function lastPageUrl() {
      return this.isApiResource ? this.data.links.last : null;
    },
    nextPageUrl: function nextPageUrl() {
      return this.isApiResource ? this.data.links.next : this.data.next_page_url;
    },
    perPage: function perPage() {
      return this.isApiResource ? this.data.meta.per_page : this.data.per_page;
    },
    prevPageUrl: function prevPageUrl() {
      return this.isApiResource ? this.data.links.prev : this.data.prev_page_url;
    },
    to: function to() {
      return this.isApiResource ? this.data.meta.to : this.data.to;
    },
    total: function total() {
      return this.isApiResource ? this.data.meta.total : this.data.total;
    },
    pageRange: function pageRange() {
      if (this.limit === -1) {
        return 0;
      }

      if (this.limit === 0) {
        return this.lastPage;
      }

      var current = this.currentPage;
      var last = this.lastPage;
      var delta = this.limit;
      var left = current - delta;
      var right = current + delta + 1;
      var range = [];
      var pages = [];
      var l;

      for (var i = 1; i <= last; i++) {
        if (i === 1 || i === last || i >= left && i < right) {
          range.push(i);
        }
      }

      range.forEach(function (i) {
        if (l) {
          if (i - l === 2) {
            pages.push(l + 1);
          } else if (i - l !== 1) {
            pages.push('...');
          }
        }

        pages.push(i);
        l = i;
      });
      return pages;
    }
  },
  methods: {
    previousPage: function previousPage() {
      this.selectPage(this.currentPage - 1);
    },
    nextPage: function nextPage() {
      this.selectPage(this.currentPage + 1);
    },
    selectPage: function selectPage(page) {
      if (page === '...') {
        return;
      }

      this.$emit('pagination-change-page', page);
    }
  },
  render: function render() {
    var _this = this;

    return this.$scopedSlots.default({
      data: this.data,
      limit: this.limit,
      showDisabled: this.showDisabled,
      size: this.size,
      align: this.align,
      computed: {
        isApiResource: this.isApiResource,
        currentPage: this.currentPage,
        firstPageUrl: this.firstPageUrl,
        from: this.from,
        lastPage: this.lastPage,
        lastPageUrl: this.lastPageUrl,
        nextPageUrl: this.nextPageUrl,
        perPage: this.perPage,
        prevPageUrl: this.prevPageUrl,
        to: this.to,
        total: this.total,
        pageRange: this.pageRange
      },
      prevButtonEvents: {
        click: function click(e) {
          e.preventDefault();

          _this.previousPage();
        }
      },
      nextButtonEvents: {
        click: function click(e) {
          e.preventDefault();

          _this.nextPage();
        }
      },
      pageButtonEvents: function pageButtonEvents(page) {
        return {
          click: function click(e) {
            e.preventDefault();

            _this.selectPage(page);
          }
        };
      }
    });
  }
});
// CONCATENATED MODULE: ./src/RenderlessLaravelVuePagination.vue?vue&type=script&lang=js&
 /* harmony default export */ var src_RenderlessLaravelVuePaginationvue_type_script_lang_js_ = (RenderlessLaravelVuePaginationvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/RenderlessLaravelVuePagination.vue
var RenderlessLaravelVuePagination_render, RenderlessLaravelVuePagination_staticRenderFns




/* normalize component */

var component = normalizeComponent(
  src_RenderlessLaravelVuePaginationvue_type_script_lang_js_,
  RenderlessLaravelVuePagination_render,
  RenderlessLaravelVuePagination_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var RenderlessLaravelVuePagination = (component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/LaravelVuePagination.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var LaravelVuePaginationvue_type_script_lang_js_ = ({
  props: {
    data: {
      type: Object,
      default: function _default() {}
    },
    limit: {
      type: Number,
      default: 0
    },
    showDisabled: {
      type: Boolean,
      default: false
    },
    size: {
      type: String,
      default: 'default',
      validator: function validator(value) {
        return ['small', 'default', 'large'].indexOf(value) !== -1;
      }
    },
    align: {
      type: String,
      default: 'left',
      validator: function validator(value) {
        return ['left', 'center', 'right'].indexOf(value) !== -1;
      }
    }
  },
  methods: {
    onPaginationChangePage: function onPaginationChangePage(page) {
      this.$emit('pagination-change-page', page);
    }
  },
  components: {
    RenderlessLaravelVuePagination: RenderlessLaravelVuePagination
  }
});
// CONCATENATED MODULE: ./src/LaravelVuePagination.vue?vue&type=script&lang=js&
 /* harmony default export */ var src_LaravelVuePaginationvue_type_script_lang_js_ = (LaravelVuePaginationvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/LaravelVuePagination.vue





/* normalize component */

var LaravelVuePagination_component = normalizeComponent(
  src_LaravelVuePaginationvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var LaravelVuePagination = (LaravelVuePagination_component.exports);
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (LaravelVuePagination);



/***/ }),

/***/ "fdef":
/***/ (function(module, exports) {

module.exports = '\x09\x0A\x0B\x0C\x0D\x20\xA0\u1680\u180E\u2000\u2001\u2002\u2003' +
  '\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';


/***/ })

/******/ })["default"];
//# sourceMappingURL=laravel-vue-pagination.common.js.map

/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/setimmediate/setImmediate.js":
/*!***************************************************!*\
  !*** ./node_modules/setimmediate/setImmediate.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! ./../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--5-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--5-2!../../../node_modules/vue-loader/lib??vue-loader-options!./IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./node_modules/timers-browserify/main.js":
/*!************************************************!*\
  !*** ./node_modules/timers-browserify/main.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var scope = (typeof global !== "undefined" && global) ||
            (typeof self !== "undefined" && self) ||
            window;
var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, scope, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, scope, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(scope, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__(/*! setimmediate */ "./node_modules/setimmediate/setImmediate.js");
// On some exotic environments, it's not clear which object `setimmediate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/vue-axios/dist/vue-axios.min.js":
/*!******************************************************!*\
  !*** ./node_modules/vue-axios/dist/vue-axios.min.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var _typeof="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(o){return typeof o}:function(o){return o&&"function"==typeof Symbol&&o.constructor===Symbol&&o!==Symbol.prototype?"symbol":typeof o};!function(){function o(e,t){if(!o.installed){if(o.installed=!0,!t)return void console.error("You have to install axios");e.axios=t,Object.defineProperties(e.prototype,{axios:{get:function(){return t}},$http:{get:function(){return t}}})}}"object"==( false?undefined:_typeof(exports))?module.exports=o: true?!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function(){return o}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):undefined}();

/***/ }),

/***/ "./node_modules/vue-cleave-component/dist/vue-cleave.min.js":
/*!******************************************************************!*\
  !*** ./node_modules/vue-cleave-component/dist/vue-cleave.min.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

!function(e,t){ true?module.exports=t(__webpack_require__(/*! cleave.js */ "./node_modules/cleave.js/dist/cleave-esm.js")):undefined}("undefined"!=typeof self?self:this,function(e){return function(e){var t={};function n(r){if(t[r])return t[r].exports;var o=t[r]={i:r,l:!1,exports:{}};return e[r].call(o.exports,o,o.exports,n),o.l=!0,o.exports}return n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var o in e)n.d(r,o,function(t){return e[t]}.bind(null,o));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=1)}([function(t,n){t.exports=e},function(e,t,n){"use strict";n.r(t);var r=n(0),o=n.n(r);function u(){return(u=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var r in n)Object.prototype.hasOwnProperty.call(n,r)&&(e[r]=n[r])}return e}).apply(this,arguments)}var a={name:"cleave",render:function(e){return e("input",{attrs:{type:"text",value:this.value},on:{blur:this.onBlur}})},props:{value:{default:null,required:!0,validator:function(e){return null===e||"string"==typeof e||e instanceof String||"number"==typeof e}},options:{type:Object,default:function(){return{}}},raw:{type:Boolean,default:!0}},data:function(){return{cleave:null,onValueChangedFn:null}},mounted:function(){this.cleave||(this.cleave=new o.a(this.$el,this.getOptions(this.options)))},methods:{getOptions:function(e){return this.onValueChangedFn=e.onValueChanged,u({},e,{onValueChanged:this.onValueChanged})},onValueChanged:function(e){var t=this.raw?e.target.rawValue:e.target.value;this.$emit("input",t),"function"==typeof this.onValueChangedFn&&this.onValueChangedFn.call(this,e)},onBlur:function(e){this.$emit("blur",this.value)}},watch:{options:{deep:!0,handler:function(e){this.cleave.destroy(),this.cleave=new o.a(this.$el,this.getOptions(e)),this.cleave.setRawValue(this.value)}},value:function(e){this.cleave&&(this.raw&&e===this.cleave.getRawValue()||(this.raw||e!==this.$el.value)&&this.cleave.setRawValue(e))}},beforeDestroy:function(){this.cleave&&(this.cleave.destroy(),this.cleave=null,this.onValueChangedFn=null)}};n.d(t,"plugin",function(){return i}),n.d(t,"component",function(){return a});var i=function(e,t){var n="cleave";"string"==typeof t&&(n=t),e.component(n,a)};a.install=i;t.default=a}]).default});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Counter.vue?vue&type=template&id=68e92161&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Counter.vue?vue&type=template&id=68e92161&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.show_second > 0
    ? _c("div", [
        _c("div", { staticClass: "c-counter" }, [
          _c("span", [_vm._v(_vm._s(_vm.h))]),
          _vm._v(":"),
          _c("span", [_vm._v(_vm._s(_vm.m))]),
          _vm._v(":"),
          _c("span", [_vm._v(_vm._s(_vm.s))])
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "discount_counter_title" }, [
          _vm._v("زمان باقی ماننده تا پایان سفارش")
        ])
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/IncredibleOffers.vue?vue&type=template&id=1ac9bd27&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/IncredibleOffers.vue?vue&type=template&id=1ac9bd27&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "search_form" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.search_text,
              expression: "search_text"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", placeholder: "نام محصول ..." },
          domProps: { value: _vm.search_text },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.search_text = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "btn btn-primary",
            on: {
              click: function($event) {
                return _vm.getWarrantyList(1)
              }
            }
          },
          [_vm._v("جست و جو")]
        )
      ]),
      _vm._v(" "),
      _c("table", { staticClass: "table table-bordered" }, [
        _vm._m(0),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.WarrantyList.data, function(item, key) {
            return _c("tr", [
              _c("td", [_vm._v(_vm._s(_vm.getRow(key)))]),
              _vm._v(" "),
              _c("td", [
                _c("img", {
                  staticClass: "product_pic",
                  attrs: {
                    src:
                      _vm.$siteUrl +
                      "/files/thumbnails/" +
                      item.get_product.image_url
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { "font-size": "14px" } }, [
                _vm._v(
                  "\n                    " +
                    _vm._s(item.get_product.title) +
                    "\n                "
                )
              ]),
              _vm._v(" "),
              _c("td"),
              _vm._v(" "),
              _c("td", { staticStyle: { "font-size": "14px" } }, [
                _vm._v(
                  "\n                    " +
                    _vm._s(item.get_warranty.name) +
                    "\n                "
                )
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "100px" } }, [
                item.get_color.id > 0
                  ? _c(
                      "span",
                      {
                        staticClass: "color_td",
                        style: [
                          item.get_color.id > 0
                            ? { background: "#" + item.get_color.code }
                            : {}
                        ]
                      },
                      [
                        item.get_color.id > 0
                          ? _c(
                              "span",
                              {
                                style: [
                                  item.get_color.name == "سفید"
                                    ? { color: "black" }
                                    : { color: "white" }
                                ]
                              },
                              [
                                _vm._v(
                                  "\n                          " +
                                    _vm._s(item.get_color.name) +
                                    "\n                       "
                                )
                              ]
                            )
                          : _vm._e()
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "100px" } }, [
                _c(
                  "p",
                  {
                    staticClass: "select_item",
                    on: {
                      click: function($event) {
                        return _vm.show_box(item.id, key)
                      }
                    }
                  },
                  [_vm._v("انتخاب")]
                ),
                _vm._v(" "),
                item.offers == 1
                  ? _c(
                      "p",
                      {
                        staticClass: "remove_item",
                        on: {
                          click: function($event) {
                            return _vm.remove_offers(item.id, key)
                          }
                        }
                      },
                      [_vm._v("حذف")]
                    )
                  : _vm._e()
              ])
            ])
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("pagination", {
        attrs: { data: _vm.WarrantyList },
        on: { "pagination-change-page": _vm.getWarrantyList }
      }),
      _vm._v(" "),
      _vm.show_message_box
        ? _c(
            "div",
            { staticClass: "message_div", staticStyle: { display: "block" } },
            [
              _c("div", { staticClass: "message_box" }, [
                _c("p", { attrs: { id: "msg" } }, [
                  _vm._v(
                    "آیا از حذف این محصول از لیست پیشنهاد شگفت انگیز مطمین هستین ؟‌"
                  )
                ]),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "alert alert-success",
                    on: {
                      click: function($event) {
                        return _vm.remove_of_list()
                      }
                    }
                  },
                  [_vm._v("بله")]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "alert alert-danger",
                    on: {
                      click: function($event) {
                        _vm.show_message_box = !_vm.show_message_box
                      }
                    }
                  },
                  [_vm._v("خیر")]
                )
              ])
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: { id: "priceBox", role: "dialog" }
        },
        [
          _c("div", { staticClass: "modal-dialog modal-lg" }, [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(1),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _vm.server_errors
                  ? _c("div", { staticClass: "alert alert-warning" }, [
                      _c(
                        "ul",
                        { staticClass: "list-inline" },
                        _vm._l(_vm.server_errors, function(error) {
                          return _c("li", [
                            _vm._v(
                              "\n                              " +
                                _vm._s(error[0])
                            )
                          ])
                        }),
                        0
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("label", [_vm._v("هزینه محصول : ")]),
                    _vm._v(" "),
                    _c("cleave", {
                      staticClass: "form-control left",
                      attrs: { options: _vm.options },
                      model: {
                        value: _vm.formInput.price1,
                        callback: function($$v) {
                          _vm.$set(_vm.formInput, "price1", $$v)
                        },
                        expression: "formInput.price1"
                      }
                    }),
                    _vm._v(" "),
                    _vm.errors.price1_error
                      ? _c("span", { staticClass: "has_error" }, [
                          _vm._v(_vm._s(_vm.errors.price1_error))
                        ])
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("label", [_vm._v("هزینه محصول برای فروش : ")]),
                    _vm._v(" "),
                    _c("cleave", {
                      staticClass: "form-control left",
                      attrs: { options: _vm.options },
                      model: {
                        value: _vm.formInput.price2,
                        callback: function($$v) {
                          _vm.$set(_vm.formInput, "price2", $$v)
                        },
                        expression: "formInput.price2"
                      }
                    }),
                    _vm._v(" "),
                    _vm.errors.price2_error
                      ? _c("span", { staticClass: "has_error" }, [
                          _vm._v(_vm._s(_vm.errors.price2_error))
                        ])
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("label", [_vm._v("تعداد موجودی محصول : ")]),
                    _vm._v(" "),
                    _c("cleave", {
                      staticClass: "form-control left",
                      attrs: { options: _vm.options },
                      model: {
                        value: _vm.formInput.product_number,
                        callback: function($$v) {
                          _vm.$set(_vm.formInput, "product_number", $$v)
                        },
                        expression: "formInput.product_number"
                      }
                    }),
                    _vm._v(" "),
                    _vm.errors.product_number_error
                      ? _c("span", { staticClass: "has_error" }, [
                          _vm._v(_vm._s(_vm.errors.product_number_error))
                        ])
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("label", [_vm._v("تعداد قابل سفارش در سبد خرید : ")]),
                    _vm._v(" "),
                    _c("cleave", {
                      staticClass: "form-control left",
                      attrs: { options: _vm.options },
                      model: {
                        value: _vm.formInput.product_number_cart,
                        callback: function($$v) {
                          _vm.$set(_vm.formInput, "product_number_cart", $$v)
                        },
                        expression: "formInput.product_number_cart"
                      }
                    }),
                    _vm._v(" "),
                    _vm.errors.product_number_cart_error
                      ? _c("span", { staticClass: "has_error" }, [
                          _vm._v(_vm._s(_vm.errors.product_number_cart_error))
                        ])
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("تاریخ شروع : ")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.date1,
                        expression: "date1"
                      }
                    ],
                    staticClass: "form-control",
                    staticStyle: { "text-align": "center" },
                    attrs: { type: "text", id: "pcal1" },
                    domProps: { value: _vm.date1 },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.date1 = $event.target.value
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", [_vm._v("تاریخ پایان : ")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.date2,
                        expression: "date2"
                      }
                    ],
                    staticClass: "form-control",
                    staticStyle: { "text-align": "center" },
                    attrs: { type: "text", id: "pcal2" },
                    domProps: { value: _vm.date2 },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.date2 = $event.target.value
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "modal-footer" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    on: {
                      click: function($event) {
                        return _vm.add()
                      }
                    }
                  },
                  [_vm._v("افزودن")]
                )
              ])
            ])
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ردیف")]),
        _vm._v(" "),
        _c("th", [_vm._v("تصویر")]),
        _vm._v(" "),
        _c("th", [_vm._v("عنوان محصول")]),
        _vm._v(" "),
        _c("th", [_vm._v("فروشنده")]),
        _vm._v(" "),
        _c("th", [_vm._v("گارانتی")]),
        _vm._v(" "),
        _c("th", [_vm._v("رنگ")]),
        _vm._v(" "),
        _c("th", [_vm._v("عملیات")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c("h5", [_vm._v("  افزودن به لیست پیشنهاد شگفت انگیز ")]),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("\n                       ×\n                   ")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/OrderStep.vue?vue&type=template&id=2121af42&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/OrderStep.vue?vue&type=template&id=2121af42&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "swiper-container order_steps" }, [
      _c(
        "div",
        { staticClass: "swiper-wrapper" },
        _vm._l(_vm.steps, function(step, key) {
          return key > -1
            ? _c("div", { staticClass: "swiper-slide" }, [
                _c(
                  "div",
                  {
                    class: [
                      _vm.order_status < key
                        ? "step_div step_inactive"
                        : "step_div"
                    ],
                    on: {
                      click: function($event) {
                        return _vm.change_status(key)
                      }
                    }
                  },
                  [
                    _c("img", {
                      style: [key == 6 ? { width: "65%" } : {}],
                      attrs: {
                        src: _vm.$siteUrl + "/files/images/step" + key + ".svg"
                      }
                    }),
                    _vm._v(" "),
                    _c("span", [_vm._v(_vm._s(step))])
                  ]
                ),
                _vm._v(" "),
                key < 6
                  ? _c("hr", {
                      class: [_vm.order_status > key ? "hr_active" : ""]
                    })
                  : _c("div", { staticStyle: { "min-width": "66px" } })
              ])
            : _vm._e()
        }),
        0
      ),
      _vm._v(" "),
      _c("div", { staticClass: "swiper-button-next" }),
      _vm._v(" "),
      _c("div", { staticClass: "swiper-button-prev" })
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "statusModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "exampleModalLabel",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _vm._v(
                  "\n                        آیا از تغییر وضعیت مرسوله به حالت " +
                    _vm._s(_vm.steps[_vm.status]) +
                    " مطمین هستین ؟\n\n                        "
                ),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.tozihat,
                      expression: "tozihat"
                    }
                  ],
                  staticClass: "textarea",
                  attrs: { placeholder: "توضیحات" },
                  domProps: { value: _vm.tozihat },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.tozihat = $event.target.value
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "modal-footer" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    attrs: { type: "button" },
                    on: {
                      click: function($event) {
                        return _vm.send_data()
                      }
                    }
                  },
                  [_vm._v("تایید")]
                )
              ])
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _vm._m(1)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        { staticClass: "modal-title", attrs: { id: "exampleModalLabel" } },
        [_vm._v("تغییر وضعیت مرسوله")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "error_dialog" }, [
      _c("span", [_vm._v("خطا در ارسال اطلاعات - مجددا تلاش نمایید")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/PackageContent.vue?vue&type=template&id=61fde6fe&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/PackageContent.vue?vue&type=template&id=61fde6fe& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.show_loading
      ? _c("div", { staticClass: "loading_box2" }, [_vm._m(0)])
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "search_form order_search" }, [
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.title,
            expression: "title"
          }
        ],
        staticClass: "form-control",
        staticStyle: { "margin-left": "20px" },
        attrs: { type: "text", placeholder: "عنوان محصول" },
        domProps: { value: _vm.title },
        on: {
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.title = $event.target.value
          }
        }
      })
    ]),
    _vm._v(" "),
    this.seller_tozihat.trim().length > 0
      ? _c("div", { staticClass: "tozihat" }, [
          _c("span", [_vm._v("توضیحات فروشنده")]),
          _vm._v(" "),
          _c("span", { domProps: { innerHTML: _vm._s(this.seller_tozihat) } })
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("textarea", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.tozihat,
            expression: "tozihat"
          }
        ],
        staticClass: "form-control",
        staticStyle: { width: "100%", height: "170px" },
        attrs: { placeholder: "توضیحات" },
        domProps: { value: _vm.tozihat },
        on: {
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.tozihat = $event.target.value
          }
        }
      })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("label", [_vm._v("وضعیت محموله")]),
      _vm._v(" "),
      _c(
        "select",
        {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.package_status,
              expression: "package_status"
            }
          ],
          staticClass: "selectpicker auto_width",
          on: {
            change: function($event) {
              var $$selectedVal = Array.prototype.filter
                .call($event.target.options, function(o) {
                  return o.selected
                })
                .map(function(o) {
                  var val = "_value" in o ? o._value : o.value
                  return val
                })
              _vm.package_status = $event.target.multiple
                ? $$selectedVal
                : $$selectedVal[0]
            }
          }
        },
        [
          _c("option", { attrs: { value: "-2" } }, [_vm._v("ارسال ناقص")]),
          _vm._v(" "),
          _c("option", { attrs: { value: "-1" } }, [_vm._v("عدم ارسال")]),
          _vm._v(" "),
          _c("option", { attrs: { value: "1" } }, [_vm._v("ارسال شده")]),
          _vm._v(" "),
          _c("option", { attrs: { value: "0" } }, [_vm._v("آماده ارسال")])
        ]
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("label", [_vm._v("انبار ارسال محموله")]),
      _vm._v(" "),
      _c("span", [_vm._v(_vm._s(_vm.stockroom))])
    ]),
    _vm._v(" "),
    _c("p", [_vm._v("محصولات انتخاب شده")]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-bordered" }, [
      _vm._m(1),
      _vm._v(" "),
      _c(
        "tbody",
        [
          _vm._l(_vm.getProductList, function(row, key) {
            return _c("tr", { key: key }, [
              _c("td", [_vm._v(_vm._s(_vm.getRow(key)))]),
              _vm._v(" "),
              _c("td", [
                _c("img", {
                  staticClass: "product_pic stockroom_product",
                  attrs: {
                    src:
                      _vm.$siteUrl +
                      "/files/thumbnails/" +
                      row.get_product_warranty.get_product.image_url
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", [
                _c("span", [
                  _vm._v(_vm._s(row.get_product_warranty.get_product.title))
                ])
              ]),
              _vm._v(" "),
              _c("td", [
                _vm._v(_vm._s(row.get_product_warranty.get_warranty.name))
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "150px" } }, [
                row.get_product_warranty.get_color.id > 0
                  ? _c(
                      "span",
                      {
                        staticClass: "color_td",
                        style: [
                          {
                            background:
                              "#" + row.get_product_warranty.get_color.code
                          }
                        ]
                      },
                      [
                        _c("span", { staticStyle: { color: "white" } }, [
                          _vm._v(
                            _vm._s(row.get_product_warranty.get_color.name)
                          )
                        ])
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "140px" } }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: row.product_count,
                      expression: "row.product_count"
                    }
                  ],
                  staticClass: "form-control",
                  staticStyle: { width: "70px", "text-align": "center" },
                  attrs: { type: "text", placeholder: "تعداد" },
                  domProps: { value: row.product_count },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(row, "product_count", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                row.real_count != null && row.real_count != row.product_count
                  ? _c(
                      "div",
                      {
                        staticStyle: {
                          color: "red",
                          "font-size": "14px",
                          "padding-top": "10px"
                        }
                      },
                      [
                        _c("span", [_vm._v("تعداد دریافت شده توسط انباردار")]),
                        _vm._v(" :\n            "),
                        _c("span", [
                          _vm._v(_vm._s(_vm.replaceNumber(row.real_count)))
                        ])
                      ]
                    )
                  : _vm._e()
              ])
            ])
          }),
          _vm._v(" "),
          _vm.list.length == 0
            ? _c("tr", [
                _c("td", { attrs: { colspan: "6" } }, [
                  _vm._v("رکوردی یافت نشد")
                ])
              ])
            : _vm._e()
        ],
        2
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.addProduct,
            expression: "addProduct"
          }
        ],
        attrs: { type: "checkbox" },
        domProps: {
          checked: Array.isArray(_vm.addProduct)
            ? _vm._i(_vm.addProduct, null) > -1
            : _vm.addProduct
        },
        on: {
          change: function($event) {
            var $$a = _vm.addProduct,
              $$el = $event.target,
              $$c = $$el.checked ? true : false
            if (Array.isArray($$a)) {
              var $$v = null,
                $$i = _vm._i($$a, $$v)
              if ($$el.checked) {
                $$i < 0 && (_vm.addProduct = $$a.concat([$$v]))
              } else {
                $$i > -1 &&
                  (_vm.addProduct = $$a
                    .slice(0, $$i)
                    .concat($$a.slice($$i + 1)))
              }
            } else {
              _vm.addProduct = $$c
            }
          }
        }
      }),
      _vm._v(" افزودن محصولات به انبار\n  ")
    ]),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn btn-success",
        staticStyle: { "border-radius": "0px" },
        on: {
          click: function($event) {
            return _vm.send_data()
          }
        }
      },
      [_vm._v("ثبت نهایی")]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "spinner" }, [
      _c("div", { staticClass: "b1" }),
      _vm._v(" "),
      _c("div", { staticClass: "b2" }),
      _vm._v(" "),
      _c("div", { staticClass: "b3" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ردیف")]),
        _vm._v(" "),
        _c("th", [_vm._v("تصویر محصول")]),
        _vm._v(" "),
        _c("th", [_vm._v("عنوان محصول")]),
        _vm._v(" "),
        _c("th", [_vm._v("گارانتی")]),
        _vm._v(" "),
        _c("th", [_vm._v("رنگ")]),
        _vm._v(" "),
        _c("th", [_vm._v("تعداد")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/SaleReport.vue?vue&type=template&id=49057786&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/SaleReport.vue?vue&type=template&id=49057786& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("p", [
        _c("span", [_vm._v("میزان فروش در سال ")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.default_year,
                expression: "default_year"
              }
            ],
            ref: "default_year",
            staticClass: "selectpicker auto_width years_tag",
            on: {
              change: [
                function($event) {
                  var $$selectedVal = Array.prototype.filter
                    .call($event.target.options, function(o) {
                      return o.selected
                    })
                    .map(function(o) {
                      var val = "_value" in o ? o._value : o.value
                      return val
                    })
                  _vm.default_year = $event.target.multiple
                    ? $$selectedVal
                    : $$selectedVal[0]
                },
                function($event) {
                  return _vm.getData()
                }
              ]
            }
          },
          _vm._l(_vm.years, function(year, key) {
            return _c("option", { key: key, domProps: { value: year } }, [
              _vm._v(_vm._s(_vm.replaceNumber(year)))
            ])
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("highcharts", { attrs: { options: _vm.chartOptions } }),
      _vm._v(" "),
      _c(
        "div",
        { staticStyle: { "padding-top": "50px" } },
        [_c("highcharts", { attrs: { options: _vm.chartOptions2 } })],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StockroomOutputList.vue?vue&type=template&id=9d976a16&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StockroomOutputList.vue?vue&type=template&id=9d976a16& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.show_loading
      ? _c("div", { staticClass: "loading_box2" }, [_vm._m(0)])
      : _vm._e(),
    _vm._v(" "),
    _vm.errors.length > 0
      ? _c("div", { staticClass: "alert alert-warning" }, [
          _c(
            "ul",
            { staticClass: "error_ul" },
            _vm._l(_vm.errors, function(msg, key) {
              return _c("li", { key: key }, [
                _vm._v("\n                " + _vm._s(msg) + "\n            ")
              ])
            }),
            0
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticStyle: { "padding-bottom": "20px" } }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "stockroom" } }, [_vm._v("انتخاب انبار")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.stockroom_id,
                expression: "stockroom_id"
              }
            ],
            staticClass: "selectpicker",
            attrs: { id: "stockroom" },
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.stockroom_id = $event.target.multiple
                  ? $$selectedVal
                  : $$selectedVal[0]
              }
            }
          },
          [
            _c("option", { attrs: { value: "0" } }, [_vm._v("انتخاب انبار")]),
            _vm._v(" "),
            _vm._l(_vm.stockroom, function(row) {
              return _c(
                "option",
                { key: row.id, domProps: { value: row.id } },
                [_vm._v(_vm._s(row.name))]
              )
            })
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("textarea", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.tozihat,
              expression: "tozihat"
            }
          ],
          staticClass: "form-control",
          staticStyle: { width: "100%", height: "170px" },
          attrs: { placeholder: "توضیحات" },
          domProps: { value: _vm.tozihat },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.tozihat = $event.target.value
            }
          }
        })
      ]),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "alert alert-success select_product_btn",
          attrs: { "data-toggle": "modal", "data-target": ".product_list" }
        },
        [_vm._v("\n            افزودن محصول\n        ")]
      )
    ]),
    _vm._v(" "),
    _c(
      "p",
      { staticStyle: { "margin-top": "30px", "margin-bottom": "20px" } },
      [_vm._v("محصولات انتخاب شده")]
    ),
    _vm._v(" "),
    _c("table", { staticClass: "table table-bordered" }, [
      _vm._m(1),
      _vm._v(" "),
      _c(
        "tbody",
        [
          _vm._l(_vm.selected_product, function(item, key) {
            return _c("tr", { key: key }, [
              _c("td", [
                _vm._v(
                  "\n                    " +
                    _vm._s(_vm.getRow(key)) +
                    "\n                 "
                )
              ]),
              _vm._v(" "),
              _c("td", [
                _c("img", {
                  staticClass: "product_pic stockroom_product",
                  attrs: {
                    src:
                      _vm.$siteUrl +
                      "/files/thumbnails/" +
                      item.get_product_warranty.get_product.image_url
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", [
                _c("span", [
                  _vm._v(_vm._s(item.get_product_warranty.get_product.title))
                ])
              ]),
              _vm._v(" "),
              _c("td", [
                _c("span", [
                  _vm._v(
                    _vm._s(item.get_product_warranty.get_seller.brand_name)
                  )
                ])
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { "font-size": "14px" } }, [
                _c("span", [
                  _vm._v(_vm._s(item.get_product_warranty.get_warranty.name))
                ])
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "150px" } }, [
                item.get_product_warranty.get_color.id > 0
                  ? _c(
                      "span",
                      {
                        staticClass: "color_td",
                        style: [
                          {
                            background:
                              "#" + item.get_product_warranty.get_color.code
                          }
                        ]
                      },
                      [
                        _c("span", { staticStyle: { color: "white" } }, [
                          _vm._v(
                            _vm._s(item.get_product_warranty.get_color.name)
                          )
                        ])
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "70px" } }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.selected_product[key].product_number,
                      expression: "selected_product[key].product_number"
                    }
                  ],
                  staticClass: "form-control",
                  staticStyle: { width: "70px", "text-align": "center" },
                  attrs: { type: "text", placeholder: "تعداد" },
                  domProps: { value: _vm.selected_product[key].product_number },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.selected_product[key],
                        "product_number",
                        $event.target.value
                      )
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "100px" } }, [
                _c(
                  "span",
                  {
                    staticClass: "remove_item",
                    on: {
                      click: function($event) {
                        return _vm.removeOfList(key)
                      }
                    }
                  },
                  [_vm._v("حذف")]
                )
              ])
            ])
          }),
          _vm._v(" "),
          _vm.selected_product.length == 0
            ? _c("tr", [
                _c("td", { attrs: { colspan: "8" } }, [
                  _vm._v("هیچ محصولی انتخاب نشده")
                ])
              ])
            : _vm._e()
        ],
        2
      )
    ]),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn btn-success",
        staticStyle: { "border-radius": "0px" },
        on: {
          click: function($event) {
            return _vm.send_data()
          }
        }
      },
      [_vm._v("\n        ثبت نهایی\n    ")]
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "modal fade product_list", attrs: { role: "modal" } },
      [
        _c("div", { staticClass: "modal-dialog modal-lg" }, [
          _c(
            "div",
            { staticClass: "modal-content" },
            [
              _vm.get_data
                ? _c("div", { staticClass: "loading_box2" }, [_vm._m(2)])
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "box_header" }, [
                _c("div", { staticClass: "input_div" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.search_text,
                        expression: "search_text"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", placeholder: "نام محصول .." },
                    domProps: { value: _vm.search_text },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.search_text = $event.target.value
                      }
                    }
                  }),
                  _c(
                    "a",
                    {
                      staticClass: "btn btn-primary",
                      staticStyle: { color: "white" },
                      on: {
                        click: function($event) {
                          return _vm.getList(1)
                        }
                      }
                    },
                    [_vm._v("جست و جو")]
                  )
                ]),
                _vm._v(" "),
                _vm._m(3)
              ]),
              _vm._v(" "),
              _c("table", { staticClass: "table table-bordered" }, [
                _c(
                  "tbody",
                  _vm._l(_vm.ProductList.data, function(item, key) {
                    return _c("tr", { key: key }, [
                      _c("td", [
                        _vm._v(
                          "\n                                " +
                            _vm._s(_vm.getRow(key)) +
                            "\n                            "
                        )
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c("img", {
                          staticClass: "product_pic stockroom_product_pic",
                          attrs: {
                            src:
                              _vm.$siteUrl +
                              "/files/thumbnails/" +
                              item.get_product_warranty.get_product.image_url
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c("span", [
                          _vm._v(
                            _vm._s(item.get_product_warranty.get_product.title)
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c("span", [
                          _vm._v(
                            _vm._s(
                              item.get_product_warranty.get_seller.brand_name
                            )
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("td", { staticStyle: { "font-size": "14px" } }, [
                        _c("span", [
                          _vm._v(
                            _vm._s(item.get_product_warranty.get_warranty.name)
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("td", { staticStyle: { width: "150px" } }, [
                        item.get_product_warranty.get_color.id > 0
                          ? _c(
                              "span",
                              {
                                staticClass: "color_td",
                                style: [
                                  {
                                    background:
                                      "#" +
                                      item.get_product_warranty.get_color.code
                                  }
                                ]
                              },
                              [
                                _c(
                                  "span",
                                  { staticStyle: { color: "white" } },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        item.get_product_warranty.get_color.name
                                      )
                                    )
                                  ]
                                )
                              ]
                            )
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("td", { staticStyle: { width: "70px" } }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product_count[key],
                              expression: "product_count[key]"
                            }
                          ],
                          staticClass: "form-control",
                          staticStyle: {
                            width: "70px",
                            "text-align": "center"
                          },
                          attrs: { type: "text", placeholder: "تعداد" },
                          domProps: { value: _vm.product_count[key] },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product_count,
                                key,
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("td", { staticStyle: { width: "130px" } }, [
                        _vm.checkInList(item.id)
                          ? _c("span", { staticStyle: { color: "#ef5661" } }, [
                              _vm._v("اضافه شد")
                            ])
                          : _c(
                              "span",
                              {
                                staticClass: "select_item",
                                on: {
                                  click: function($event) {
                                    return _vm.add_product(
                                      item.get_product_warranty.id,
                                      key
                                    )
                                  }
                                }
                              },
                              [_vm._v("افزودن به لیست")]
                            )
                      ])
                    ])
                  }),
                  0
                )
              ]),
              _vm._v(" "),
              _c("pagination", {
                attrs: { data: _vm.ProductList },
                on: { "pagination-change-page": _vm.getList }
              })
            ],
            1
          )
        ])
      ]
    ),
    _vm._v(" "),
    _vm.show_messag_box
      ? _c(
          "div",
          { staticClass: "message_div", staticStyle: { display: "block" } },
          [
            _c("div", { staticClass: "message_box" }, [
              _c("p", { attrs: { id: "msg" } }, [_vm._v(_vm._s(_vm.msg))]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "alert alert-success",
                  on: {
                    click: function($event) {
                      return _vm.add_product_to_stockroom()
                    }
                  }
                },
                [_vm._v("بله")]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "alert alert-danger",
                  on: {
                    click: function($event) {
                      _vm.show_messag_box = false
                    }
                  }
                },
                [_vm._v("خیر")]
              )
            ])
          ]
        )
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "spinner" }, [
      _c("div", { staticClass: "b1" }),
      _vm._v(" "),
      _c("div", { staticClass: "b2" }),
      _vm._v(" "),
      _c("div", { staticClass: "b3" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ردیف")]),
        _vm._v(" "),
        _c("th", [_vm._v("تصویر محصول")]),
        _vm._v(" "),
        _c("th", [_vm._v("عنوان محصول")]),
        _vm._v(" "),
        _c("th", [_vm._v("فروشنده")]),
        _vm._v(" "),
        _c("th", [_vm._v("گارانتی")]),
        _vm._v(" "),
        _c("th", [_vm._v("رنگ")]),
        _vm._v(" "),
        _c("th", [_vm._v("تعداد")]),
        _vm._v(" "),
        _c("th", [_vm._v("عملیات")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "spinner" }, [
      _c("div", { staticClass: "b1" }),
      _vm._v(" "),
      _c("div", { staticClass: "b2" }),
      _vm._v(" "),
      _c("div", { staticClass: "b3" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StockroomProductList.vue?vue&type=template&id=43098f67&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/StockroomProductList.vue?vue&type=template&id=43098f67& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.show_loading
      ? _c("div", { staticClass: "loading_box2" }, [_vm._m(0)])
      : _vm._e(),
    _vm._v(" "),
    _vm.errors.length > 0
      ? _c("div", { staticClass: "alert alert-warning" }, [
          _c(
            "ul",
            { staticClass: "error_ul" },
            _vm._l(_vm.errors, function(msg, key) {
              return _c("li", { key: key }, [
                _vm._v("\n                " + _vm._s(msg) + "\n            ")
              ])
            }),
            0
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticStyle: { "padding-bottom": "20px" } }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "stockroom" } }, [_vm._v("انتخاب انبار")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.stockroom_id,
                expression: "stockroom_id"
              }
            ],
            staticClass: "selectpicker auto_width",
            attrs: { id: "stockroom" },
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.stockroom_id = $event.target.multiple
                  ? $$selectedVal
                  : $$selectedVal[0]
              }
            }
          },
          [
            _c("option", { attrs: { value: "0" } }, [_vm._v("انتخاب انبار")]),
            _vm._v(" "),
            _vm._l(_vm.stockroom, function(row) {
              return _c(
                "option",
                { key: row.id, domProps: { value: row.id } },
                [_vm._v(_vm._s(row.name))]
              )
            })
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("textarea", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.tozihat,
              expression: "tozihat"
            }
          ],
          staticClass: "form-control",
          staticStyle: { width: "100%", height: "170px" },
          attrs: { placeholder: "توضیحات" },
          domProps: { value: _vm.tozihat },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.tozihat = $event.target.value
            }
          }
        })
      ]),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "alert alert-success select_product_btn",
          attrs: { "data-toggle": "modal", "data-target": ".product_list" }
        },
        [_vm._v("\n            افزودن محصول\n        ")]
      )
    ]),
    _vm._v(" "),
    _c(
      "p",
      { staticStyle: { "margin-top": "30px", "margin-bottom": "20px" } },
      [_vm._v("محصولات انتخاب شده")]
    ),
    _vm._v(" "),
    _c("table", { staticClass: "table table-bordered" }, [
      _vm._m(1),
      _vm._v(" "),
      _c(
        "tbody",
        [
          _vm._l(_vm.selected_product, function(item, key) {
            return _c("tr", { key: key }, [
              _c("td", [
                _vm._v(
                  "\n                    " +
                    _vm._s(_vm.getRow(key)) +
                    "\n                 "
                )
              ]),
              _vm._v(" "),
              _c("td", [
                _c("img", {
                  staticClass: "product_pic stockroom_product",
                  attrs: {
                    src:
                      _vm.$siteUrl +
                      "/files/thumbnails/" +
                      item.get_product.image_url
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", [_c("span", [_vm._v(_vm._s(item.get_product.title))])]),
              _vm._v(" "),
              _c("td", [
                _c("span", [_vm._v(_vm._s(item.get_seller.brand_name))])
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { "font-size": "14px" } }, [
                _c("span", [_vm._v(_vm._s(item.get_warranty.name))])
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "150px" } }, [
                item.get_color.id > 0 && item.get_color.type == 1
                  ? _c(
                      "span",
                      {
                        staticClass: "color_td",
                        style: [{ background: "#" + item.get_color.code }]
                      },
                      [
                        _c("span", { staticStyle: { color: "white" } }, [
                          _vm._v(_vm._s(item.get_color.name))
                        ])
                      ]
                    )
                  : _c("span", { staticClass: "color_td" }, [
                      _c("span", [
                        _vm._v(
                          "\n                                سایز : " +
                            _vm._s(item.get_color.name) +
                            "\n                            "
                        )
                      ])
                    ])
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "70px" } }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.selected_product[key].product_number,
                      expression: "selected_product[key].product_number"
                    }
                  ],
                  staticClass: "form-control",
                  staticStyle: { width: "70px", "text-align": "center" },
                  attrs: { type: "text", placeholder: "تعداد" },
                  domProps: { value: _vm.selected_product[key].product_number },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.selected_product[key],
                        "product_number",
                        $event.target.value
                      )
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", { staticStyle: { width: "100px" } }, [
                _c(
                  "span",
                  {
                    staticClass: "remove_item",
                    on: {
                      click: function($event) {
                        return _vm.removeOfList(key)
                      }
                    }
                  },
                  [_vm._v("حذف")]
                )
              ])
            ])
          }),
          _vm._v(" "),
          _vm.selected_product.length == 0
            ? _c("tr", [
                _c("td", { attrs: { colspan: "8" } }, [
                  _vm._v("هیچ محصولی انتخاب نشده")
                ])
              ])
            : _vm._e()
        ],
        2
      )
    ]),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn btn-success",
        staticStyle: { "border-radius": "0px" },
        on: {
          click: function($event) {
            return _vm.send_data()
          }
        }
      },
      [_vm._v("\n        ثبت نهایی\n    ")]
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "modal fade product_list", attrs: { role: "modal" } },
      [
        _c("div", { staticClass: "modal-dialog modal-lg" }, [
          _c(
            "div",
            { staticClass: "modal-content" },
            [
              _vm.get_data
                ? _c("div", { staticClass: "loading_box2" }, [_vm._m(2)])
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "box_header" }, [
                _c("div", { staticClass: "input_div" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.search_text,
                        expression: "search_text"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", placeholder: "نام محصول .." },
                    domProps: { value: _vm.search_text },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.search_text = $event.target.value
                      }
                    }
                  }),
                  _c(
                    "a",
                    {
                      staticClass: "btn btn-primary",
                      staticStyle: { color: "white" },
                      on: {
                        click: function($event) {
                          return _vm.getProductWarrnty(1)
                        }
                      }
                    },
                    [_vm._v("جست و جو")]
                  )
                ]),
                _vm._v(" "),
                _vm._m(3)
              ]),
              _vm._v(" "),
              _c("table", { staticClass: "table table-bordered" }, [
                _c(
                  "tbody",
                  _vm._l(_vm.ProductList.data, function(item, key) {
                    return _c("tr", { key: key }, [
                      _c("td", [
                        _vm._v(
                          "\n                                " +
                            _vm._s(_vm.getRow(key)) +
                            "\n                            "
                        )
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c("img", {
                          staticClass: "product_pic stockroom_product_pic",
                          attrs: {
                            src:
                              _vm.$siteUrl +
                              "/files/thumbnails/" +
                              item.get_product.image_url
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c("span", [_vm._v(_vm._s(item.get_product.title))])
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c("span", [_vm._v(_vm._s(item.get_seller.brand_name))])
                      ]),
                      _vm._v(" "),
                      _c("td", { staticStyle: { "font-size": "14px" } }, [
                        _c("span", [_vm._v(_vm._s(item.get_warranty.name))])
                      ]),
                      _vm._v(" "),
                      _c("td", { staticStyle: { width: "150px" } }, [
                        item.get_color.id > 0 && item.get_color.type == 1
                          ? _c(
                              "span",
                              {
                                staticClass: "color_td",
                                style: [
                                  { background: "#" + item.get_color.code }
                                ]
                              },
                              [
                                _c(
                                  "span",
                                  { staticStyle: { color: "white" } },
                                  [_vm._v(_vm._s(item.get_color.name))]
                                )
                              ]
                            )
                          : _c("span", { staticClass: "color_td" }, [
                              _c("span", [
                                _vm._v(
                                  "\n                                      سایز : " +
                                    _vm._s(item.get_color.name) +
                                    "\n                                     "
                                )
                              ])
                            ])
                      ]),
                      _vm._v(" "),
                      _c("td", { staticStyle: { width: "70px" } }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product_count[key],
                              expression: "product_count[key]"
                            }
                          ],
                          staticClass: "form-control",
                          staticStyle: {
                            width: "70px",
                            "text-align": "center"
                          },
                          attrs: { type: "text", placeholder: "تعداد" },
                          domProps: { value: _vm.product_count[key] },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product_count,
                                key,
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("td", { staticStyle: { width: "130px" } }, [
                        _vm.checkInList(item.id)
                          ? _c("span", { staticStyle: { color: "#ef5661" } }, [
                              _vm._v("اضافه شد")
                            ])
                          : _c(
                              "span",
                              {
                                staticClass: "select_item",
                                on: {
                                  click: function($event) {
                                    return _vm.add_product(item.id, key)
                                  }
                                }
                              },
                              [_vm._v("افزودن به لیست")]
                            )
                      ])
                    ])
                  }),
                  0
                )
              ]),
              _vm._v(" "),
              _c("pagination", {
                attrs: { data: _vm.ProductList },
                on: { "pagination-change-page": _vm.getProductWarrnty }
              })
            ],
            1
          )
        ])
      ]
    ),
    _vm._v(" "),
    _vm.show_messag_box
      ? _c(
          "div",
          { staticClass: "message_div", staticStyle: { display: "block" } },
          [
            _c("div", { staticClass: "message_box" }, [
              _c("p", { attrs: { id: "msg" } }, [_vm._v(_vm._s(_vm.msg))]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "alert alert-success",
                  on: {
                    click: function($event) {
                      return _vm.add_product_to_stockroom()
                    }
                  }
                },
                [_vm._v("بله")]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "alert alert-danger",
                  on: {
                    click: function($event) {
                      _vm.show_messag_box = false
                    }
                  }
                },
                [_vm._v("خیر")]
              )
            ])
          ]
        )
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "spinner" }, [
      _c("div", { staticClass: "b1" }),
      _vm._v(" "),
      _c("div", { staticClass: "b2" }),
      _vm._v(" "),
      _c("div", { staticClass: "b3" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ردیف")]),
        _vm._v(" "),
        _c("th", [_vm._v("تصویر محصول")]),
        _vm._v(" "),
        _c("th", [_vm._v("عنوان محصول")]),
        _vm._v(" "),
        _c("th", [_vm._v("فروشنده")]),
        _vm._v(" "),
        _c("th", [_vm._v("گارانتی")]),
        _vm._v(" "),
        _c("th", [_vm._v("رنگ")]),
        _vm._v(" "),
        _c("th", [_vm._v("تعداد")]),
        _vm._v(" "),
        _c("th", [_vm._v("عملیات")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "spinner" }, [
      _c("div", { staticClass: "b1" }),
      _vm._v(" "),
      _c("div", { staticClass: "b2" }),
      _vm._v(" "),
      _c("div", { staticClass: "b3" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./node_modules/vue/dist/vue.common.dev.js":
/*!*************************************************!*\
  !*** ./node_modules/vue/dist/vue.common.dev.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global, setImmediate) {/*!
 * Vue.js v2.6.10
 * (c) 2014-2019 Evan You
 * Released under the MIT License.
 */


/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Generate a string containing static keys from compiler modules.
 */
function genStaticKeys (modules) {
  return modules.reduce(function (keys, m) {
    return keys.concat(m.staticKeys || [])
  }, []).join(',')
}

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var SSR_ATTR = 'data-server-rendered';

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;

var supportsPassive = false;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
        /* istanbul ignore next */
        supportsPassive = true;
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

{
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm) {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.target) {
    Dep.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if (!config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
Dep.target = null;
var targetStack = [];

function pushTarget (target) {
  targetStack.push(target);
  Dep.target = target;
}

function popTarget () {
  targetStack.pop();
  Dep.target = targetStack[targetStack.length - 1];
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      protoAugment(value, arrayMethods);
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.target) {
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if (customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if (isUndef(target) || isPrimitive(target)
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
    warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if (isUndef(target) || isPrimitive(target)
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
    warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
{
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
      warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
    assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if (warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if (isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var isUsingMicroTask = false;

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
  isUsingMicroTask = true;
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
  isUsingMicroTask = true;
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Techinically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

var mark;
var measure;

{
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

{
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
      warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

function mergeVNodeHook (def, hookKey, hook) {
  if (def instanceof VNode) {
    def = def.data.hook || (def.data.hook = {});
  }
  var invoker;
  var oldHook = def[hookKey];

  function wrappedHook () {
    hook.apply(this, arguments);
    // important: remove merged hook to ensure it's called only once
    // and prevent memory leak
    remove(invoker.fns, wrappedHook);
  }

  if (isUndef(oldHook)) {
    // no existing hook
    invoker = createFnInvoker([wrappedHook]);
  } else {
    /* istanbul ignore if */
    if (isDef(oldHook.fns) && isTrue(oldHook.merged)) {
      // already a merged invoker
      invoker = oldHook;
      invoker.fns.push(wrappedHook);
    } else {
      // existing plain hook
      invoker = createFnInvoker([oldHook, wrappedHook]);
    }
  }

  invoker.merged = true;
  def[hookKey] = invoker;
}

/*  */

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    return
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  return res
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      }
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      (slots.default || (slots.default = [])).push(child);
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i);
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i);
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length));
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i);
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if (!isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    nodes = scopedSlotFn(props) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
      warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
      warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if (key !== '' && key !== null) {
      // null is a speical value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag);

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
    warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if (isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  }
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack becaues all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if (vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if (Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
      warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                "timeout (" + (res.timeout) + "ms)"
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function mountComponent (
  vm,
  el,
  hydrating
) {
  vm.$el = el;
  if (!vm.$options.render) {
    vm.$options.render = createEmptyVNode;
    {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  callHook(vm, 'beforeMount');

  var updateComponent;
  /* istanbul ignore if */
  if (config.performance && mark) {
    updateComponent = function () {
      var name = vm._name;
      var id = vm._uid;
      var startTag = "vue-perf-start:" + id;
      var endTag = "vue-perf-end:" + id;

      mark(startTag);
      var vnode = vm._render();
      mark(endTag);
      measure(("vue " + name + " render"), startTag, endTag);

      mark(startTag);
      vm._update(vnode, hydrating);
      mark(endTag);
      measure(("vue " + name + " patch"), startTag, endTag);
    };
  } else {
    updateComponent = function () {
      vm._update(vm._render(), hydrating);
    };
  }

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before () {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;

  // manually mounted instance, call mounted on self
  // mounted is called for render-created child components in its inserted hook
  if (vm.$vnode == null) {
    vm._isMounted = true;
    callHook(vm, 'mounted');
  }
  return vm
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }

  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if (has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if (!config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression = expOrFn.toString();
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
      warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    }
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
    warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
      warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if (getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if (sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.target) {
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if (config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    {
      initProxy(vm);
    }
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    initInjections(vm); // resolve injections before data/props
    initState(vm);
    initProvide(vm); // resolve provide after data/props
    callHook(vm, 'created');

    /* istanbul ignore if */
    if (config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if (!(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if (name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if (type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.10';

/*  */

// these are reserved for web because they are directly compiled away
// during template compilation
var isReservedAttr = makeMap('style,class');

// attributes that should be using props for binding
var acceptValue = makeMap('input,textarea,option,select,progress');
var mustUseProp = function (tag, type, attr) {
  return (
    (attr === 'value' && acceptValue(tag)) && type !== 'button' ||
    (attr === 'selected' && tag === 'option') ||
    (attr === 'checked' && tag === 'input') ||
    (attr === 'muted' && tag === 'video')
  )
};

var isEnumeratedAttr = makeMap('contenteditable,draggable,spellcheck');

var isValidContentEditableValue = makeMap('events,caret,typing,plaintext-only');

var convertEnumeratedValue = function (key, value) {
  return isFalsyAttrValue(value) || value === 'false'
    ? 'false'
    // allow arbitrary string value for contenteditable
    : key === 'contenteditable' && isValidContentEditableValue(value)
      ? value
      : 'true'
};

var isBooleanAttr = makeMap(
  'allowfullscreen,async,autofocus,autoplay,checked,compact,controls,declare,' +
  'default,defaultchecked,defaultmuted,defaultselected,defer,disabled,' +
  'enabled,formnovalidate,hidden,indeterminate,inert,ismap,itemscope,loop,multiple,' +
  'muted,nohref,noresize,noshade,novalidate,nowrap,open,pauseonexit,readonly,' +
  'required,reversed,scoped,seamless,selected,sortable,translate,' +
  'truespeed,typemustmatch,visible'
);

var xlinkNS = 'http://www.w3.org/1999/xlink';

var isXlink = function (name) {
  return name.charAt(5) === ':' && name.slice(0, 5) === 'xlink'
};

var getXlinkProp = function (name) {
  return isXlink(name) ? name.slice(6, name.length) : ''
};

var isFalsyAttrValue = function (val) {
  return val == null || val === false
};

/*  */

function genClassForVnode (vnode) {
  var data = vnode.data;
  var parentNode = vnode;
  var childNode = vnode;
  while (isDef(childNode.componentInstance)) {
    childNode = childNode.componentInstance._vnode;
    if (childNode && childNode.data) {
      data = mergeClassData(childNode.data, data);
    }
  }
  while (isDef(parentNode = parentNode.parent)) {
    if (parentNode && parentNode.data) {
      data = mergeClassData(data, parentNode.data);
    }
  }
  return renderClass(data.staticClass, data.class)
}

function mergeClassData (child, parent) {
  return {
    staticClass: concat(child.staticClass, parent.staticClass),
    class: isDef(child.class)
      ? [child.class, parent.class]
      : parent.class
  }
}

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var namespaceMap = {
  svg: 'http://www.w3.org/2000/svg',
  math: 'http://www.w3.org/1998/Math/MathML'
};

var isHTMLTag = makeMap(
  'html,body,base,head,link,meta,style,title,' +
  'address,article,aside,footer,header,h1,h2,h3,h4,h5,h6,hgroup,nav,section,' +
  'div,dd,dl,dt,figcaption,figure,picture,hr,img,li,main,ol,p,pre,ul,' +
  'a,b,abbr,bdi,bdo,br,cite,code,data,dfn,em,i,kbd,mark,q,rp,rt,rtc,ruby,' +
  's,samp,small,span,strong,sub,sup,time,u,var,wbr,area,audio,map,track,video,' +
  'embed,object,param,source,canvas,script,noscript,del,ins,' +
  'caption,col,colgroup,table,thead,tbody,td,th,tr,' +
  'button,datalist,fieldset,form,input,label,legend,meter,optgroup,option,' +
  'output,progress,select,textarea,' +
  'details,dialog,menu,menuitem,summary,' +
  'content,element,shadow,template,blockquote,iframe,tfoot'
);

// this map is intentionally selective, only covering SVG elements that may
// contain child elements.
var isSVG = makeMap(
  'svg,animate,circle,clippath,cursor,defs,desc,ellipse,filter,font-face,' +
  'foreignObject,g,glyph,image,line,marker,mask,missing-glyph,path,pattern,' +
  'polygon,polyline,rect,switch,symbol,text,textpath,tspan,use,view',
  true
);

var isPreTag = function (tag) { return tag === 'pre'; };

var isReservedTag = function (tag) {
  return isHTMLTag(tag) || isSVG(tag)
};

function getTagNamespace (tag) {
  if (isSVG(tag)) {
    return 'svg'
  }
  // basic support for MathML
  // note it doesn't support other MathML elements being component roots
  if (tag === 'math') {
    return 'math'
  }
}

var unknownElementCache = Object.create(null);
function isUnknownElement (tag) {
  /* istanbul ignore if */
  if (!inBrowser) {
    return true
  }
  if (isReservedTag(tag)) {
    return false
  }
  tag = tag.toLowerCase();
  /* istanbul ignore if */
  if (unknownElementCache[tag] != null) {
    return unknownElementCache[tag]
  }
  var el = document.createElement(tag);
  if (tag.indexOf('-') > -1) {
    // http://stackoverflow.com/a/28210364/1070244
    return (unknownElementCache[tag] = (
      el.constructor === window.HTMLUnknownElement ||
      el.constructor === window.HTMLElement
    ))
  } else {
    return (unknownElementCache[tag] = /HTMLUnknownElement/.test(el.toString()))
  }
}

var isTextInputType = makeMap('text,number,password,search,email,tel,url');

/*  */

/**
 * Query an element selector if it's not an element already.
 */
function query (el) {
  if (typeof el === 'string') {
    var selected = document.querySelector(el);
    if (!selected) {
      warn(
        'Cannot find element: ' + el
      );
      return document.createElement('div')
    }
    return selected
  } else {
    return el
  }
}

/*  */

function createElement$1 (tagName, vnode) {
  var elm = document.createElement(tagName);
  if (tagName !== 'select') {
    return elm
  }
  // false or null will remove the attribute but undefined will not
  if (vnode.data && vnode.data.attrs && vnode.data.attrs.multiple !== undefined) {
    elm.setAttribute('multiple', 'multiple');
  }
  return elm
}

function createElementNS (namespace, tagName) {
  return document.createElementNS(namespaceMap[namespace], tagName)
}

function createTextNode (text) {
  return document.createTextNode(text)
}

function createComment (text) {
  return document.createComment(text)
}

function insertBefore (parentNode, newNode, referenceNode) {
  parentNode.insertBefore(newNode, referenceNode);
}

function removeChild (node, child) {
  node.removeChild(child);
}

function appendChild (node, child) {
  node.appendChild(child);
}

function parentNode (node) {
  return node.parentNode
}

function nextSibling (node) {
  return node.nextSibling
}

function tagName (node) {
  return node.tagName
}

function setTextContent (node, text) {
  node.textContent = text;
}

function setStyleScope (node, scopeId) {
  node.setAttribute(scopeId, '');
}

var nodeOps = /*#__PURE__*/Object.freeze({
  createElement: createElement$1,
  createElementNS: createElementNS,
  createTextNode: createTextNode,
  createComment: createComment,
  insertBefore: insertBefore,
  removeChild: removeChild,
  appendChild: appendChild,
  parentNode: parentNode,
  nextSibling: nextSibling,
  tagName: tagName,
  setTextContent: setTextContent,
  setStyleScope: setStyleScope
});

/*  */

var ref = {
  create: function create (_, vnode) {
    registerRef(vnode);
  },
  update: function update (oldVnode, vnode) {
    if (oldVnode.data.ref !== vnode.data.ref) {
      registerRef(oldVnode, true);
      registerRef(vnode);
    }
  },
  destroy: function destroy (vnode) {
    registerRef(vnode, true);
  }
};

function registerRef (vnode, isRemoval) {
  var key = vnode.data.ref;
  if (!isDef(key)) { return }

  var vm = vnode.context;
  var ref = vnode.componentInstance || vnode.elm;
  var refs = vm.$refs;
  if (isRemoval) {
    if (Array.isArray(refs[key])) {
      remove(refs[key], ref);
    } else if (refs[key] === ref) {
      refs[key] = undefined;
    }
  } else {
    if (vnode.data.refInFor) {
      if (!Array.isArray(refs[key])) {
        refs[key] = [ref];
      } else if (refs[key].indexOf(ref) < 0) {
        // $flow-disable-line
        refs[key].push(ref);
      }
    } else {
      refs[key] = ref;
    }
  }
}

/**
 * Virtual DOM patching algorithm based on Snabbdom by
 * Simon Friis Vindum (@paldepind)
 * Licensed under the MIT License
 * https://github.com/paldepind/snabbdom/blob/master/LICENSE
 *
 * modified by Evan You (@yyx990803)
 *
 * Not type-checking this because this file is perf-critical and the cost
 * of making flow understand it is not worth it.
 */

var emptyNode = new VNode('', {}, []);

var hooks = ['create', 'activate', 'update', 'remove', 'destroy'];

function sameVnode (a, b) {
  return (
    a.key === b.key && (
      (
        a.tag === b.tag &&
        a.isComment === b.isComment &&
        isDef(a.data) === isDef(b.data) &&
        sameInputType(a, b)
      ) || (
        isTrue(a.isAsyncPlaceholder) &&
        a.asyncFactory === b.asyncFactory &&
        isUndef(b.asyncFactory.error)
      )
    )
  )
}

function sameInputType (a, b) {
  if (a.tag !== 'input') { return true }
  var i;
  var typeA = isDef(i = a.data) && isDef(i = i.attrs) && i.type;
  var typeB = isDef(i = b.data) && isDef(i = i.attrs) && i.type;
  return typeA === typeB || isTextInputType(typeA) && isTextInputType(typeB)
}

function createKeyToOldIdx (children, beginIdx, endIdx) {
  var i, key;
  var map = {};
  for (i = beginIdx; i <= endIdx; ++i) {
    key = children[i].key;
    if (isDef(key)) { map[key] = i; }
  }
  return map
}

function createPatchFunction (backend) {
  var i, j;
  var cbs = {};

  var modules = backend.modules;
  var nodeOps = backend.nodeOps;

  for (i = 0; i < hooks.length; ++i) {
    cbs[hooks[i]] = [];
    for (j = 0; j < modules.length; ++j) {
      if (isDef(modules[j][hooks[i]])) {
        cbs[hooks[i]].push(modules[j][hooks[i]]);
      }
    }
  }

  function emptyNodeAt (elm) {
    return new VNode(nodeOps.tagName(elm).toLowerCase(), {}, [], undefined, elm)
  }

  function createRmCb (childElm, listeners) {
    function remove$$1 () {
      if (--remove$$1.listeners === 0) {
        removeNode(childElm);
      }
    }
    remove$$1.listeners = listeners;
    return remove$$1
  }

  function removeNode (el) {
    var parent = nodeOps.parentNode(el);
    // element may have already been removed due to v-html / v-text
    if (isDef(parent)) {
      nodeOps.removeChild(parent, el);
    }
  }

  function isUnknownElement$$1 (vnode, inVPre) {
    return (
      !inVPre &&
      !vnode.ns &&
      !(
        config.ignoredElements.length &&
        config.ignoredElements.some(function (ignore) {
          return isRegExp(ignore)
            ? ignore.test(vnode.tag)
            : ignore === vnode.tag
        })
      ) &&
      config.isUnknownElement(vnode.tag)
    )
  }

  var creatingElmInVPre = 0;

  function createElm (
    vnode,
    insertedVnodeQueue,
    parentElm,
    refElm,
    nested,
    ownerArray,
    index
  ) {
    if (isDef(vnode.elm) && isDef(ownerArray)) {
      // This vnode was used in a previous render!
      // now it's used as a new node, overwriting its elm would cause
      // potential patch errors down the road when it's used as an insertion
      // reference node. Instead, we clone the node on-demand before creating
      // associated DOM element for it.
      vnode = ownerArray[index] = cloneVNode(vnode);
    }

    vnode.isRootInsert = !nested; // for transition enter check
    if (createComponent(vnode, insertedVnodeQueue, parentElm, refElm)) {
      return
    }

    var data = vnode.data;
    var children = vnode.children;
    var tag = vnode.tag;
    if (isDef(tag)) {
      {
        if (data && data.pre) {
          creatingElmInVPre++;
        }
        if (isUnknownElement$$1(vnode, creatingElmInVPre)) {
          warn(
            'Unknown custom element: <' + tag + '> - did you ' +
            'register the component correctly? For recursive components, ' +
            'make sure to provide the "name" option.',
            vnode.context
          );
        }
      }

      vnode.elm = vnode.ns
        ? nodeOps.createElementNS(vnode.ns, tag)
        : nodeOps.createElement(tag, vnode);
      setScope(vnode);

      /* istanbul ignore if */
      {
        createChildren(vnode, children, insertedVnodeQueue);
        if (isDef(data)) {
          invokeCreateHooks(vnode, insertedVnodeQueue);
        }
        insert(parentElm, vnode.elm, refElm);
      }

      if (data && data.pre) {
        creatingElmInVPre--;
      }
    } else if (isTrue(vnode.isComment)) {
      vnode.elm = nodeOps.createComment(vnode.text);
      insert(parentElm, vnode.elm, refElm);
    } else {
      vnode.elm = nodeOps.createTextNode(vnode.text);
      insert(parentElm, vnode.elm, refElm);
    }
  }

  function createComponent (vnode, insertedVnodeQueue, parentElm, refElm) {
    var i = vnode.data;
    if (isDef(i)) {
      var isReactivated = isDef(vnode.componentInstance) && i.keepAlive;
      if (isDef(i = i.hook) && isDef(i = i.init)) {
        i(vnode, false /* hydrating */);
      }
      // after calling the init hook, if the vnode is a child component
      // it should've created a child instance and mounted it. the child
      // component also has set the placeholder vnode's elm.
      // in that case we can just return the element and be done.
      if (isDef(vnode.componentInstance)) {
        initComponent(vnode, insertedVnodeQueue);
        insert(parentElm, vnode.elm, refElm);
        if (isTrue(isReactivated)) {
          reactivateComponent(vnode, insertedVnodeQueue, parentElm, refElm);
        }
        return true
      }
    }
  }

  function initComponent (vnode, insertedVnodeQueue) {
    if (isDef(vnode.data.pendingInsert)) {
      insertedVnodeQueue.push.apply(insertedVnodeQueue, vnode.data.pendingInsert);
      vnode.data.pendingInsert = null;
    }
    vnode.elm = vnode.componentInstance.$el;
    if (isPatchable(vnode)) {
      invokeCreateHooks(vnode, insertedVnodeQueue);
      setScope(vnode);
    } else {
      // empty component root.
      // skip all element-related modules except for ref (#3455)
      registerRef(vnode);
      // make sure to invoke the insert hook
      insertedVnodeQueue.push(vnode);
    }
  }

  function reactivateComponent (vnode, insertedVnodeQueue, parentElm, refElm) {
    var i;
    // hack for #4339: a reactivated component with inner transition
    // does not trigger because the inner node's created hooks are not called
    // again. It's not ideal to involve module-specific logic in here but
    // there doesn't seem to be a better way to do it.
    var innerNode = vnode;
    while (innerNode.componentInstance) {
      innerNode = innerNode.componentInstance._vnode;
      if (isDef(i = innerNode.data) && isDef(i = i.transition)) {
        for (i = 0; i < cbs.activate.length; ++i) {
          cbs.activate[i](emptyNode, innerNode);
        }
        insertedVnodeQueue.push(innerNode);
        break
      }
    }
    // unlike a newly created component,
    // a reactivated keep-alive component doesn't insert itself
    insert(parentElm, vnode.elm, refElm);
  }

  function insert (parent, elm, ref$$1) {
    if (isDef(parent)) {
      if (isDef(ref$$1)) {
        if (nodeOps.parentNode(ref$$1) === parent) {
          nodeOps.insertBefore(parent, elm, ref$$1);
        }
      } else {
        nodeOps.appendChild(parent, elm);
      }
    }
  }

  function createChildren (vnode, children, insertedVnodeQueue) {
    if (Array.isArray(children)) {
      {
        checkDuplicateKeys(children);
      }
      for (var i = 0; i < children.length; ++i) {
        createElm(children[i], insertedVnodeQueue, vnode.elm, null, true, children, i);
      }
    } else if (isPrimitive(vnode.text)) {
      nodeOps.appendChild(vnode.elm, nodeOps.createTextNode(String(vnode.text)));
    }
  }

  function isPatchable (vnode) {
    while (vnode.componentInstance) {
      vnode = vnode.componentInstance._vnode;
    }
    return isDef(vnode.tag)
  }

  function invokeCreateHooks (vnode, insertedVnodeQueue) {
    for (var i$1 = 0; i$1 < cbs.create.length; ++i$1) {
      cbs.create[i$1](emptyNode, vnode);
    }
    i = vnode.data.hook; // Reuse variable
    if (isDef(i)) {
      if (isDef(i.create)) { i.create(emptyNode, vnode); }
      if (isDef(i.insert)) { insertedVnodeQueue.push(vnode); }
    }
  }

  // set scope id attribute for scoped CSS.
  // this is implemented as a special case to avoid the overhead
  // of going through the normal attribute patching process.
  function setScope (vnode) {
    var i;
    if (isDef(i = vnode.fnScopeId)) {
      nodeOps.setStyleScope(vnode.elm, i);
    } else {
      var ancestor = vnode;
      while (ancestor) {
        if (isDef(i = ancestor.context) && isDef(i = i.$options._scopeId)) {
          nodeOps.setStyleScope(vnode.elm, i);
        }
        ancestor = ancestor.parent;
      }
    }
    // for slot content they should also get the scopeId from the host instance.
    if (isDef(i = activeInstance) &&
      i !== vnode.context &&
      i !== vnode.fnContext &&
      isDef(i = i.$options._scopeId)
    ) {
      nodeOps.setStyleScope(vnode.elm, i);
    }
  }

  function addVnodes (parentElm, refElm, vnodes, startIdx, endIdx, insertedVnodeQueue) {
    for (; startIdx <= endIdx; ++startIdx) {
      createElm(vnodes[startIdx], insertedVnodeQueue, parentElm, refElm, false, vnodes, startIdx);
    }
  }

  function invokeDestroyHook (vnode) {
    var i, j;
    var data = vnode.data;
    if (isDef(data)) {
      if (isDef(i = data.hook) && isDef(i = i.destroy)) { i(vnode); }
      for (i = 0; i < cbs.destroy.length; ++i) { cbs.destroy[i](vnode); }
    }
    if (isDef(i = vnode.children)) {
      for (j = 0; j < vnode.children.length; ++j) {
        invokeDestroyHook(vnode.children[j]);
      }
    }
  }

  function removeVnodes (parentElm, vnodes, startIdx, endIdx) {
    for (; startIdx <= endIdx; ++startIdx) {
      var ch = vnodes[startIdx];
      if (isDef(ch)) {
        if (isDef(ch.tag)) {
          removeAndInvokeRemoveHook(ch);
          invokeDestroyHook(ch);
        } else { // Text node
          removeNode(ch.elm);
        }
      }
    }
  }

  function removeAndInvokeRemoveHook (vnode, rm) {
    if (isDef(rm) || isDef(vnode.data)) {
      var i;
      var listeners = cbs.remove.length + 1;
      if (isDef(rm)) {
        // we have a recursively passed down rm callback
        // increase the listeners count
        rm.listeners += listeners;
      } else {
        // directly removing
        rm = createRmCb(vnode.elm, listeners);
      }
      // recursively invoke hooks on child component root node
      if (isDef(i = vnode.componentInstance) && isDef(i = i._vnode) && isDef(i.data)) {
        removeAndInvokeRemoveHook(i, rm);
      }
      for (i = 0; i < cbs.remove.length; ++i) {
        cbs.remove[i](vnode, rm);
      }
      if (isDef(i = vnode.data.hook) && isDef(i = i.remove)) {
        i(vnode, rm);
      } else {
        rm();
      }
    } else {
      removeNode(vnode.elm);
    }
  }

  function updateChildren (parentElm, oldCh, newCh, insertedVnodeQueue, removeOnly) {
    var oldStartIdx = 0;
    var newStartIdx = 0;
    var oldEndIdx = oldCh.length - 1;
    var oldStartVnode = oldCh[0];
    var oldEndVnode = oldCh[oldEndIdx];
    var newEndIdx = newCh.length - 1;
    var newStartVnode = newCh[0];
    var newEndVnode = newCh[newEndIdx];
    var oldKeyToIdx, idxInOld, vnodeToMove, refElm;

    // removeOnly is a special flag used only by <transition-group>
    // to ensure removed elements stay in correct relative positions
    // during leaving transitions
    var canMove = !removeOnly;

    {
      checkDuplicateKeys(newCh);
    }

    while (oldStartIdx <= oldEndIdx && newStartIdx <= newEndIdx) {
      if (isUndef(oldStartVnode)) {
        oldStartVnode = oldCh[++oldStartIdx]; // Vnode has been moved left
      } else if (isUndef(oldEndVnode)) {
        oldEndVnode = oldCh[--oldEndIdx];
      } else if (sameVnode(oldStartVnode, newStartVnode)) {
        patchVnode(oldStartVnode, newStartVnode, insertedVnodeQueue, newCh, newStartIdx);
        oldStartVnode = oldCh[++oldStartIdx];
        newStartVnode = newCh[++newStartIdx];
      } else if (sameVnode(oldEndVnode, newEndVnode)) {
        patchVnode(oldEndVnode, newEndVnode, insertedVnodeQueue, newCh, newEndIdx);
        oldEndVnode = oldCh[--oldEndIdx];
        newEndVnode = newCh[--newEndIdx];
      } else if (sameVnode(oldStartVnode, newEndVnode)) { // Vnode moved right
        patchVnode(oldStartVnode, newEndVnode, insertedVnodeQueue, newCh, newEndIdx);
        canMove && nodeOps.insertBefore(parentElm, oldStartVnode.elm, nodeOps.nextSibling(oldEndVnode.elm));
        oldStartVnode = oldCh[++oldStartIdx];
        newEndVnode = newCh[--newEndIdx];
      } else if (sameVnode(oldEndVnode, newStartVnode)) { // Vnode moved left
        patchVnode(oldEndVnode, newStartVnode, insertedVnodeQueue, newCh, newStartIdx);
        canMove && nodeOps.insertBefore(parentElm, oldEndVnode.elm, oldStartVnode.elm);
        oldEndVnode = oldCh[--oldEndIdx];
        newStartVnode = newCh[++newStartIdx];
      } else {
        if (isUndef(oldKeyToIdx)) { oldKeyToIdx = createKeyToOldIdx(oldCh, oldStartIdx, oldEndIdx); }
        idxInOld = isDef(newStartVnode.key)
          ? oldKeyToIdx[newStartVnode.key]
          : findIdxInOld(newStartVnode, oldCh, oldStartIdx, oldEndIdx);
        if (isUndef(idxInOld)) { // New element
          createElm(newStartVnode, insertedVnodeQueue, parentElm, oldStartVnode.elm, false, newCh, newStartIdx);
        } else {
          vnodeToMove = oldCh[idxInOld];
          if (sameVnode(vnodeToMove, newStartVnode)) {
            patchVnode(vnodeToMove, newStartVnode, insertedVnodeQueue, newCh, newStartIdx);
            oldCh[idxInOld] = undefined;
            canMove && nodeOps.insertBefore(parentElm, vnodeToMove.elm, oldStartVnode.elm);
          } else {
            // same key but different element. treat as new element
            createElm(newStartVnode, insertedVnodeQueue, parentElm, oldStartVnode.elm, false, newCh, newStartIdx);
          }
        }
        newStartVnode = newCh[++newStartIdx];
      }
    }
    if (oldStartIdx > oldEndIdx) {
      refElm = isUndef(newCh[newEndIdx + 1]) ? null : newCh[newEndIdx + 1].elm;
      addVnodes(parentElm, refElm, newCh, newStartIdx, newEndIdx, insertedVnodeQueue);
    } else if (newStartIdx > newEndIdx) {
      removeVnodes(parentElm, oldCh, oldStartIdx, oldEndIdx);
    }
  }

  function checkDuplicateKeys (children) {
    var seenKeys = {};
    for (var i = 0; i < children.length; i++) {
      var vnode = children[i];
      var key = vnode.key;
      if (isDef(key)) {
        if (seenKeys[key]) {
          warn(
            ("Duplicate keys detected: '" + key + "'. This may cause an update error."),
            vnode.context
          );
        } else {
          seenKeys[key] = true;
        }
      }
    }
  }

  function findIdxInOld (node, oldCh, start, end) {
    for (var i = start; i < end; i++) {
      var c = oldCh[i];
      if (isDef(c) && sameVnode(node, c)) { return i }
    }
  }

  function patchVnode (
    oldVnode,
    vnode,
    insertedVnodeQueue,
    ownerArray,
    index,
    removeOnly
  ) {
    if (oldVnode === vnode) {
      return
    }

    if (isDef(vnode.elm) && isDef(ownerArray)) {
      // clone reused vnode
      vnode = ownerArray[index] = cloneVNode(vnode);
    }

    var elm = vnode.elm = oldVnode.elm;

    if (isTrue(oldVnode.isAsyncPlaceholder)) {
      if (isDef(vnode.asyncFactory.resolved)) {
        hydrate(oldVnode.elm, vnode, insertedVnodeQueue);
      } else {
        vnode.isAsyncPlaceholder = true;
      }
      return
    }

    // reuse element for static trees.
    // note we only do this if the vnode is cloned -
    // if the new node is not cloned it means the render functions have been
    // reset by the hot-reload-api and we need to do a proper re-render.
    if (isTrue(vnode.isStatic) &&
      isTrue(oldVnode.isStatic) &&
      vnode.key === oldVnode.key &&
      (isTrue(vnode.isCloned) || isTrue(vnode.isOnce))
    ) {
      vnode.componentInstance = oldVnode.componentInstance;
      return
    }

    var i;
    var data = vnode.data;
    if (isDef(data) && isDef(i = data.hook) && isDef(i = i.prepatch)) {
      i(oldVnode, vnode);
    }

    var oldCh = oldVnode.children;
    var ch = vnode.children;
    if (isDef(data) && isPatchable(vnode)) {
      for (i = 0; i < cbs.update.length; ++i) { cbs.update[i](oldVnode, vnode); }
      if (isDef(i = data.hook) && isDef(i = i.update)) { i(oldVnode, vnode); }
    }
    if (isUndef(vnode.text)) {
      if (isDef(oldCh) && isDef(ch)) {
        if (oldCh !== ch) { updateChildren(elm, oldCh, ch, insertedVnodeQueue, removeOnly); }
      } else if (isDef(ch)) {
        {
          checkDuplicateKeys(ch);
        }
        if (isDef(oldVnode.text)) { nodeOps.setTextContent(elm, ''); }
        addVnodes(elm, null, ch, 0, ch.length - 1, insertedVnodeQueue);
      } else if (isDef(oldCh)) {
        removeVnodes(elm, oldCh, 0, oldCh.length - 1);
      } else if (isDef(oldVnode.text)) {
        nodeOps.setTextContent(elm, '');
      }
    } else if (oldVnode.text !== vnode.text) {
      nodeOps.setTextContent(elm, vnode.text);
    }
    if (isDef(data)) {
      if (isDef(i = data.hook) && isDef(i = i.postpatch)) { i(oldVnode, vnode); }
    }
  }

  function invokeInsertHook (vnode, queue, initial) {
    // delay insert hooks for component root nodes, invoke them after the
    // element is really inserted
    if (isTrue(initial) && isDef(vnode.parent)) {
      vnode.parent.data.pendingInsert = queue;
    } else {
      for (var i = 0; i < queue.length; ++i) {
        queue[i].data.hook.insert(queue[i]);
      }
    }
  }

  var hydrationBailed = false;
  // list of modules that can skip create hook during hydration because they
  // are already rendered on the client or has no need for initialization
  // Note: style is excluded because it relies on initial clone for future
  // deep updates (#7063).
  var isRenderedModule = makeMap('attrs,class,staticClass,staticStyle,key');

  // Note: this is a browser-only function so we can assume elms are DOM nodes.
  function hydrate (elm, vnode, insertedVnodeQueue, inVPre) {
    var i;
    var tag = vnode.tag;
    var data = vnode.data;
    var children = vnode.children;
    inVPre = inVPre || (data && data.pre);
    vnode.elm = elm;

    if (isTrue(vnode.isComment) && isDef(vnode.asyncFactory)) {
      vnode.isAsyncPlaceholder = true;
      return true
    }
    // assert node match
    {
      if (!assertNodeMatch(elm, vnode, inVPre)) {
        return false
      }
    }
    if (isDef(data)) {
      if (isDef(i = data.hook) && isDef(i = i.init)) { i(vnode, true /* hydrating */); }
      if (isDef(i = vnode.componentInstance)) {
        // child component. it should have hydrated its own tree.
        initComponent(vnode, insertedVnodeQueue);
        return true
      }
    }
    if (isDef(tag)) {
      if (isDef(children)) {
        // empty element, allow client to pick up and populate children
        if (!elm.hasChildNodes()) {
          createChildren(vnode, children, insertedVnodeQueue);
        } else {
          // v-html and domProps: innerHTML
          if (isDef(i = data) && isDef(i = i.domProps) && isDef(i = i.innerHTML)) {
            if (i !== elm.innerHTML) {
              /* istanbul ignore if */
              if (typeof console !== 'undefined' &&
                !hydrationBailed
              ) {
                hydrationBailed = true;
                console.warn('Parent: ', elm);
                console.warn('server innerHTML: ', i);
                console.warn('client innerHTML: ', elm.innerHTML);
              }
              return false
            }
          } else {
            // iterate and compare children lists
            var childrenMatch = true;
            var childNode = elm.firstChild;
            for (var i$1 = 0; i$1 < children.length; i$1++) {
              if (!childNode || !hydrate(childNode, children[i$1], insertedVnodeQueue, inVPre)) {
                childrenMatch = false;
                break
              }
              childNode = childNode.nextSibling;
            }
            // if childNode is not null, it means the actual childNodes list is
            // longer than the virtual children list.
            if (!childrenMatch || childNode) {
              /* istanbul ignore if */
              if (typeof console !== 'undefined' &&
                !hydrationBailed
              ) {
                hydrationBailed = true;
                console.warn('Parent: ', elm);
                console.warn('Mismatching childNodes vs. VNodes: ', elm.childNodes, children);
              }
              return false
            }
          }
        }
      }
      if (isDef(data)) {
        var fullInvoke = false;
        for (var key in data) {
          if (!isRenderedModule(key)) {
            fullInvoke = true;
            invokeCreateHooks(vnode, insertedVnodeQueue);
            break
          }
        }
        if (!fullInvoke && data['class']) {
          // ensure collecting deps for deep class bindings for future updates
          traverse(data['class']);
        }
      }
    } else if (elm.data !== vnode.text) {
      elm.data = vnode.text;
    }
    return true
  }

  function assertNodeMatch (node, vnode, inVPre) {
    if (isDef(vnode.tag)) {
      return vnode.tag.indexOf('vue-component') === 0 || (
        !isUnknownElement$$1(vnode, inVPre) &&
        vnode.tag.toLowerCase() === (node.tagName && node.tagName.toLowerCase())
      )
    } else {
      return node.nodeType === (vnode.isComment ? 8 : 3)
    }
  }

  return function patch (oldVnode, vnode, hydrating, removeOnly) {
    if (isUndef(vnode)) {
      if (isDef(oldVnode)) { invokeDestroyHook(oldVnode); }
      return
    }

    var isInitialPatch = false;
    var insertedVnodeQueue = [];

    if (isUndef(oldVnode)) {
      // empty mount (likely as component), create new root element
      isInitialPatch = true;
      createElm(vnode, insertedVnodeQueue);
    } else {
      var isRealElement = isDef(oldVnode.nodeType);
      if (!isRealElement && sameVnode(oldVnode, vnode)) {
        // patch existing root node
        patchVnode(oldVnode, vnode, insertedVnodeQueue, null, null, removeOnly);
      } else {
        if (isRealElement) {
          // mounting to a real element
          // check if this is server-rendered content and if we can perform
          // a successful hydration.
          if (oldVnode.nodeType === 1 && oldVnode.hasAttribute(SSR_ATTR)) {
            oldVnode.removeAttribute(SSR_ATTR);
            hydrating = true;
          }
          if (isTrue(hydrating)) {
            if (hydrate(oldVnode, vnode, insertedVnodeQueue)) {
              invokeInsertHook(vnode, insertedVnodeQueue, true);
              return oldVnode
            } else {
              warn(
                'The client-side rendered virtual DOM tree is not matching ' +
                'server-rendered content. This is likely caused by incorrect ' +
                'HTML markup, for example nesting block-level elements inside ' +
                '<p>, or missing <tbody>. Bailing hydration and performing ' +
                'full client-side render.'
              );
            }
          }
          // either not server-rendered, or hydration failed.
          // create an empty node and replace it
          oldVnode = emptyNodeAt(oldVnode);
        }

        // replacing existing element
        var oldElm = oldVnode.elm;
        var parentElm = nodeOps.parentNode(oldElm);

        // create new node
        createElm(
          vnode,
          insertedVnodeQueue,
          // extremely rare edge case: do not insert if old element is in a
          // leaving transition. Only happens when combining transition +
          // keep-alive + HOCs. (#4590)
          oldElm._leaveCb ? null : parentElm,
          nodeOps.nextSibling(oldElm)
        );

        // update parent placeholder node element, recursively
        if (isDef(vnode.parent)) {
          var ancestor = vnode.parent;
          var patchable = isPatchable(vnode);
          while (ancestor) {
            for (var i = 0; i < cbs.destroy.length; ++i) {
              cbs.destroy[i](ancestor);
            }
            ancestor.elm = vnode.elm;
            if (patchable) {
              for (var i$1 = 0; i$1 < cbs.create.length; ++i$1) {
                cbs.create[i$1](emptyNode, ancestor);
              }
              // #6513
              // invoke insert hooks that may have been merged by create hooks.
              // e.g. for directives that uses the "inserted" hook.
              var insert = ancestor.data.hook.insert;
              if (insert.merged) {
                // start at index 1 to avoid re-invoking component mounted hook
                for (var i$2 = 1; i$2 < insert.fns.length; i$2++) {
                  insert.fns[i$2]();
                }
              }
            } else {
              registerRef(ancestor);
            }
            ancestor = ancestor.parent;
          }
        }

        // destroy old node
        if (isDef(parentElm)) {
          removeVnodes(parentElm, [oldVnode], 0, 0);
        } else if (isDef(oldVnode.tag)) {
          invokeDestroyHook(oldVnode);
        }
      }
    }

    invokeInsertHook(vnode, insertedVnodeQueue, isInitialPatch);
    return vnode.elm
  }
}

/*  */

var directives = {
  create: updateDirectives,
  update: updateDirectives,
  destroy: function unbindDirectives (vnode) {
    updateDirectives(vnode, emptyNode);
  }
};

function updateDirectives (oldVnode, vnode) {
  if (oldVnode.data.directives || vnode.data.directives) {
    _update(oldVnode, vnode);
  }
}

function _update (oldVnode, vnode) {
  var isCreate = oldVnode === emptyNode;
  var isDestroy = vnode === emptyNode;
  var oldDirs = normalizeDirectives$1(oldVnode.data.directives, oldVnode.context);
  var newDirs = normalizeDirectives$1(vnode.data.directives, vnode.context);

  var dirsWithInsert = [];
  var dirsWithPostpatch = [];

  var key, oldDir, dir;
  for (key in newDirs) {
    oldDir = oldDirs[key];
    dir = newDirs[key];
    if (!oldDir) {
      // new directive, bind
      callHook$1(dir, 'bind', vnode, oldVnode);
      if (dir.def && dir.def.inserted) {
        dirsWithInsert.push(dir);
      }
    } else {
      // existing directive, update
      dir.oldValue = oldDir.value;
      dir.oldArg = oldDir.arg;
      callHook$1(dir, 'update', vnode, oldVnode);
      if (dir.def && dir.def.componentUpdated) {
        dirsWithPostpatch.push(dir);
      }
    }
  }

  if (dirsWithInsert.length) {
    var callInsert = function () {
      for (var i = 0; i < dirsWithInsert.length; i++) {
        callHook$1(dirsWithInsert[i], 'inserted', vnode, oldVnode);
      }
    };
    if (isCreate) {
      mergeVNodeHook(vnode, 'insert', callInsert);
    } else {
      callInsert();
    }
  }

  if (dirsWithPostpatch.length) {
    mergeVNodeHook(vnode, 'postpatch', function () {
      for (var i = 0; i < dirsWithPostpatch.length; i++) {
        callHook$1(dirsWithPostpatch[i], 'componentUpdated', vnode, oldVnode);
      }
    });
  }

  if (!isCreate) {
    for (key in oldDirs) {
      if (!newDirs[key]) {
        // no longer present, unbind
        callHook$1(oldDirs[key], 'unbind', oldVnode, oldVnode, isDestroy);
      }
    }
  }
}

var emptyModifiers = Object.create(null);

function normalizeDirectives$1 (
  dirs,
  vm
) {
  var res = Object.create(null);
  if (!dirs) {
    // $flow-disable-line
    return res
  }
  var i, dir;
  for (i = 0; i < dirs.length; i++) {
    dir = dirs[i];
    if (!dir.modifiers) {
      // $flow-disable-line
      dir.modifiers = emptyModifiers;
    }
    res[getRawDirName(dir)] = dir;
    dir.def = resolveAsset(vm.$options, 'directives', dir.name, true);
  }
  // $flow-disable-line
  return res
}

function getRawDirName (dir) {
  return dir.rawName || ((dir.name) + "." + (Object.keys(dir.modifiers || {}).join('.')))
}

function callHook$1 (dir, hook, vnode, oldVnode, isDestroy) {
  var fn = dir.def && dir.def[hook];
  if (fn) {
    try {
      fn(vnode.elm, dir, vnode, oldVnode, isDestroy);
    } catch (e) {
      handleError(e, vnode.context, ("directive " + (dir.name) + " " + hook + " hook"));
    }
  }
}

var baseModules = [
  ref,
  directives
];

/*  */

function updateAttrs (oldVnode, vnode) {
  var opts = vnode.componentOptions;
  if (isDef(opts) && opts.Ctor.options.inheritAttrs === false) {
    return
  }
  if (isUndef(oldVnode.data.attrs) && isUndef(vnode.data.attrs)) {
    return
  }
  var key, cur, old;
  var elm = vnode.elm;
  var oldAttrs = oldVnode.data.attrs || {};
  var attrs = vnode.data.attrs || {};
  // clone observed objects, as the user probably wants to mutate it
  if (isDef(attrs.__ob__)) {
    attrs = vnode.data.attrs = extend({}, attrs);
  }

  for (key in attrs) {
    cur = attrs[key];
    old = oldAttrs[key];
    if (old !== cur) {
      setAttr(elm, key, cur);
    }
  }
  // #4391: in IE9, setting type can reset value for input[type=radio]
  // #6666: IE/Edge forces progress value down to 1 before setting a max
  /* istanbul ignore if */
  if ((isIE || isEdge) && attrs.value !== oldAttrs.value) {
    setAttr(elm, 'value', attrs.value);
  }
  for (key in oldAttrs) {
    if (isUndef(attrs[key])) {
      if (isXlink(key)) {
        elm.removeAttributeNS(xlinkNS, getXlinkProp(key));
      } else if (!isEnumeratedAttr(key)) {
        elm.removeAttribute(key);
      }
    }
  }
}

function setAttr (el, key, value) {
  if (el.tagName.indexOf('-') > -1) {
    baseSetAttr(el, key, value);
  } else if (isBooleanAttr(key)) {
    // set attribute for blank value
    // e.g. <option disabled>Select one</option>
    if (isFalsyAttrValue(value)) {
      el.removeAttribute(key);
    } else {
      // technically allowfullscreen is a boolean attribute for <iframe>,
      // but Flash expects a value of "true" when used on <embed> tag
      value = key === 'allowfullscreen' && el.tagName === 'EMBED'
        ? 'true'
        : key;
      el.setAttribute(key, value);
    }
  } else if (isEnumeratedAttr(key)) {
    el.setAttribute(key, convertEnumeratedValue(key, value));
  } else if (isXlink(key)) {
    if (isFalsyAttrValue(value)) {
      el.removeAttributeNS(xlinkNS, getXlinkProp(key));
    } else {
      el.setAttributeNS(xlinkNS, key, value);
    }
  } else {
    baseSetAttr(el, key, value);
  }
}

function baseSetAttr (el, key, value) {
  if (isFalsyAttrValue(value)) {
    el.removeAttribute(key);
  } else {
    // #7138: IE10 & 11 fires input event when setting placeholder on
    // <textarea>... block the first input event and remove the blocker
    // immediately.
    /* istanbul ignore if */
    if (
      isIE && !isIE9 &&
      el.tagName === 'TEXTAREA' &&
      key === 'placeholder' && value !== '' && !el.__ieph
    ) {
      var blocker = function (e) {
        e.stopImmediatePropagation();
        el.removeEventListener('input', blocker);
      };
      el.addEventListener('input', blocker);
      // $flow-disable-line
      el.__ieph = true; /* IE placeholder patched */
    }
    el.setAttribute(key, value);
  }
}

var attrs = {
  create: updateAttrs,
  update: updateAttrs
};

/*  */

function updateClass (oldVnode, vnode) {
  var el = vnode.elm;
  var data = vnode.data;
  var oldData = oldVnode.data;
  if (
    isUndef(data.staticClass) &&
    isUndef(data.class) && (
      isUndef(oldData) || (
        isUndef(oldData.staticClass) &&
        isUndef(oldData.class)
      )
    )
  ) {
    return
  }

  var cls = genClassForVnode(vnode);

  // handle transition classes
  var transitionClass = el._transitionClasses;
  if (isDef(transitionClass)) {
    cls = concat(cls, stringifyClass(transitionClass));
  }

  // set the class
  if (cls !== el._prevClass) {
    el.setAttribute('class', cls);
    el._prevClass = cls;
  }
}

var klass = {
  create: updateClass,
  update: updateClass
};

/*  */

var validDivisionCharRE = /[\w).+\-_$\]]/;

function parseFilters (exp) {
  var inSingle = false;
  var inDouble = false;
  var inTemplateString = false;
  var inRegex = false;
  var curly = 0;
  var square = 0;
  var paren = 0;
  var lastFilterIndex = 0;
  var c, prev, i, expression, filters;

  for (i = 0; i < exp.length; i++) {
    prev = c;
    c = exp.charCodeAt(i);
    if (inSingle) {
      if (c === 0x27 && prev !== 0x5C) { inSingle = false; }
    } else if (inDouble) {
      if (c === 0x22 && prev !== 0x5C) { inDouble = false; }
    } else if (inTemplateString) {
      if (c === 0x60 && prev !== 0x5C) { inTemplateString = false; }
    } else if (inRegex) {
      if (c === 0x2f && prev !== 0x5C) { inRegex = false; }
    } else if (
      c === 0x7C && // pipe
      exp.charCodeAt(i + 1) !== 0x7C &&
      exp.charCodeAt(i - 1) !== 0x7C &&
      !curly && !square && !paren
    ) {
      if (expression === undefined) {
        // first filter, end of expression
        lastFilterIndex = i + 1;
        expression = exp.slice(0, i).trim();
      } else {
        pushFilter();
      }
    } else {
      switch (c) {
        case 0x22: inDouble = true; break         // "
        case 0x27: inSingle = true; break         // '
        case 0x60: inTemplateString = true; break // `
        case 0x28: paren++; break                 // (
        case 0x29: paren--; break                 // )
        case 0x5B: square++; break                // [
        case 0x5D: square--; break                // ]
        case 0x7B: curly++; break                 // {
        case 0x7D: curly--; break                 // }
      }
      if (c === 0x2f) { // /
        var j = i - 1;
        var p = (void 0);
        // find first non-whitespace prev char
        for (; j >= 0; j--) {
          p = exp.charAt(j);
          if (p !== ' ') { break }
        }
        if (!p || !validDivisionCharRE.test(p)) {
          inRegex = true;
        }
      }
    }
  }

  if (expression === undefined) {
    expression = exp.slice(0, i).trim();
  } else if (lastFilterIndex !== 0) {
    pushFilter();
  }

  function pushFilter () {
    (filters || (filters = [])).push(exp.slice(lastFilterIndex, i).trim());
    lastFilterIndex = i + 1;
  }

  if (filters) {
    for (i = 0; i < filters.length; i++) {
      expression = wrapFilter(expression, filters[i]);
    }
  }

  return expression
}

function wrapFilter (exp, filter) {
  var i = filter.indexOf('(');
  if (i < 0) {
    // _f: resolveFilter
    return ("_f(\"" + filter + "\")(" + exp + ")")
  } else {
    var name = filter.slice(0, i);
    var args = filter.slice(i + 1);
    return ("_f(\"" + name + "\")(" + exp + (args !== ')' ? ',' + args : args))
  }
}

/*  */



/* eslint-disable no-unused-vars */
function baseWarn (msg, range) {
  console.error(("[Vue compiler]: " + msg));
}
/* eslint-enable no-unused-vars */

function pluckModuleFunction (
  modules,
  key
) {
  return modules
    ? modules.map(function (m) { return m[key]; }).filter(function (_) { return _; })
    : []
}

function addProp (el, name, value, range, dynamic) {
  (el.props || (el.props = [])).push(rangeSetItem({ name: name, value: value, dynamic: dynamic }, range));
  el.plain = false;
}

function addAttr (el, name, value, range, dynamic) {
  var attrs = dynamic
    ? (el.dynamicAttrs || (el.dynamicAttrs = []))
    : (el.attrs || (el.attrs = []));
  attrs.push(rangeSetItem({ name: name, value: value, dynamic: dynamic }, range));
  el.plain = false;
}

// add a raw attr (use this in preTransforms)
function addRawAttr (el, name, value, range) {
  el.attrsMap[name] = value;
  el.attrsList.push(rangeSetItem({ name: name, value: value }, range));
}

function addDirective (
  el,
  name,
  rawName,
  value,
  arg,
  isDynamicArg,
  modifiers,
  range
) {
  (el.directives || (el.directives = [])).push(rangeSetItem({
    name: name,
    rawName: rawName,
    value: value,
    arg: arg,
    isDynamicArg: isDynamicArg,
    modifiers: modifiers
  }, range));
  el.plain = false;
}

function prependModifierMarker (symbol, name, dynamic) {
  return dynamic
    ? ("_p(" + name + ",\"" + symbol + "\")")
    : symbol + name // mark the event as captured
}

function addHandler (
  el,
  name,
  value,
  modifiers,
  important,
  warn,
  range,
  dynamic
) {
  modifiers = modifiers || emptyObject;
  // warn prevent and passive modifier
  /* istanbul ignore if */
  if (
    warn &&
    modifiers.prevent && modifiers.passive
  ) {
    warn(
      'passive and prevent can\'t be used together. ' +
      'Passive handler can\'t prevent default event.',
      range
    );
  }

  // normalize click.right and click.middle since they don't actually fire
  // this is technically browser-specific, but at least for now browsers are
  // the only target envs that have right/middle clicks.
  if (modifiers.right) {
    if (dynamic) {
      name = "(" + name + ")==='click'?'contextmenu':(" + name + ")";
    } else if (name === 'click') {
      name = 'contextmenu';
      delete modifiers.right;
    }
  } else if (modifiers.middle) {
    if (dynamic) {
      name = "(" + name + ")==='click'?'mouseup':(" + name + ")";
    } else if (name === 'click') {
      name = 'mouseup';
    }
  }

  // check capture modifier
  if (modifiers.capture) {
    delete modifiers.capture;
    name = prependModifierMarker('!', name, dynamic);
  }
  if (modifiers.once) {
    delete modifiers.once;
    name = prependModifierMarker('~', name, dynamic);
  }
  /* istanbul ignore if */
  if (modifiers.passive) {
    delete modifiers.passive;
    name = prependModifierMarker('&', name, dynamic);
  }

  var events;
  if (modifiers.native) {
    delete modifiers.native;
    events = el.nativeEvents || (el.nativeEvents = {});
  } else {
    events = el.events || (el.events = {});
  }

  var newHandler = rangeSetItem({ value: value.trim(), dynamic: dynamic }, range);
  if (modifiers !== emptyObject) {
    newHandler.modifiers = modifiers;
  }

  var handlers = events[name];
  /* istanbul ignore if */
  if (Array.isArray(handlers)) {
    important ? handlers.unshift(newHandler) : handlers.push(newHandler);
  } else if (handlers) {
    events[name] = important ? [newHandler, handlers] : [handlers, newHandler];
  } else {
    events[name] = newHandler;
  }

  el.plain = false;
}

function getRawBindingAttr (
  el,
  name
) {
  return el.rawAttrsMap[':' + name] ||
    el.rawAttrsMap['v-bind:' + name] ||
    el.rawAttrsMap[name]
}

function getBindingAttr (
  el,
  name,
  getStatic
) {
  var dynamicValue =
    getAndRemoveAttr(el, ':' + name) ||
    getAndRemoveAttr(el, 'v-bind:' + name);
  if (dynamicValue != null) {
    return parseFilters(dynamicValue)
  } else if (getStatic !== false) {
    var staticValue = getAndRemoveAttr(el, name);
    if (staticValue != null) {
      return JSON.stringify(staticValue)
    }
  }
}

// note: this only removes the attr from the Array (attrsList) so that it
// doesn't get processed by processAttrs.
// By default it does NOT remove it from the map (attrsMap) because the map is
// needed during codegen.
function getAndRemoveAttr (
  el,
  name,
  removeFromMap
) {
  var val;
  if ((val = el.attrsMap[name]) != null) {
    var list = el.attrsList;
    for (var i = 0, l = list.length; i < l; i++) {
      if (list[i].name === name) {
        list.splice(i, 1);
        break
      }
    }
  }
  if (removeFromMap) {
    delete el.attrsMap[name];
  }
  return val
}

function getAndRemoveAttrByRegex (
  el,
  name
) {
  var list = el.attrsList;
  for (var i = 0, l = list.length; i < l; i++) {
    var attr = list[i];
    if (name.test(attr.name)) {
      list.splice(i, 1);
      return attr
    }
  }
}

function rangeSetItem (
  item,
  range
) {
  if (range) {
    if (range.start != null) {
      item.start = range.start;
    }
    if (range.end != null) {
      item.end = range.end;
    }
  }
  return item
}

/*  */

/**
 * Cross-platform code generation for component v-model
 */
function genComponentModel (
  el,
  value,
  modifiers
) {
  var ref = modifiers || {};
  var number = ref.number;
  var trim = ref.trim;

  var baseValueExpression = '$$v';
  var valueExpression = baseValueExpression;
  if (trim) {
    valueExpression =
      "(typeof " + baseValueExpression + " === 'string'" +
      "? " + baseValueExpression + ".trim()" +
      ": " + baseValueExpression + ")";
  }
  if (number) {
    valueExpression = "_n(" + valueExpression + ")";
  }
  var assignment = genAssignmentCode(value, valueExpression);

  el.model = {
    value: ("(" + value + ")"),
    expression: JSON.stringify(value),
    callback: ("function (" + baseValueExpression + ") {" + assignment + "}")
  };
}

/**
 * Cross-platform codegen helper for generating v-model value assignment code.
 */
function genAssignmentCode (
  value,
  assignment
) {
  var res = parseModel(value);
  if (res.key === null) {
    return (value + "=" + assignment)
  } else {
    return ("$set(" + (res.exp) + ", " + (res.key) + ", " + assignment + ")")
  }
}

/**
 * Parse a v-model expression into a base path and a final key segment.
 * Handles both dot-path and possible square brackets.
 *
 * Possible cases:
 *
 * - test
 * - test[key]
 * - test[test1[key]]
 * - test["a"][key]
 * - xxx.test[a[a].test1[key]]
 * - test.xxx.a["asa"][test1[key]]
 *
 */

var len, str, chr, index$1, expressionPos, expressionEndPos;



function parseModel (val) {
  // Fix https://github.com/vuejs/vue/pull/7730
  // allow v-model="obj.val " (trailing whitespace)
  val = val.trim();
  len = val.length;

  if (val.indexOf('[') < 0 || val.lastIndexOf(']') < len - 1) {
    index$1 = val.lastIndexOf('.');
    if (index$1 > -1) {
      return {
        exp: val.slice(0, index$1),
        key: '"' + val.slice(index$1 + 1) + '"'
      }
    } else {
      return {
        exp: val,
        key: null
      }
    }
  }

  str = val;
  index$1 = expressionPos = expressionEndPos = 0;

  while (!eof()) {
    chr = next();
    /* istanbul ignore if */
    if (isStringStart(chr)) {
      parseString(chr);
    } else if (chr === 0x5B) {
      parseBracket(chr);
    }
  }

  return {
    exp: val.slice(0, expressionPos),
    key: val.slice(expressionPos + 1, expressionEndPos)
  }
}

function next () {
  return str.charCodeAt(++index$1)
}

function eof () {
  return index$1 >= len
}

function isStringStart (chr) {
  return chr === 0x22 || chr === 0x27
}

function parseBracket (chr) {
  var inBracket = 1;
  expressionPos = index$1;
  while (!eof()) {
    chr = next();
    if (isStringStart(chr)) {
      parseString(chr);
      continue
    }
    if (chr === 0x5B) { inBracket++; }
    if (chr === 0x5D) { inBracket--; }
    if (inBracket === 0) {
      expressionEndPos = index$1;
      break
    }
  }
}

function parseString (chr) {
  var stringQuote = chr;
  while (!eof()) {
    chr = next();
    if (chr === stringQuote) {
      break
    }
  }
}

/*  */

var warn$1;

// in some cases, the event used has to be determined at runtime
// so we used some reserved tokens during compile.
var RANGE_TOKEN = '__r';
var CHECKBOX_RADIO_TOKEN = '__c';

function model (
  el,
  dir,
  _warn
) {
  warn$1 = _warn;
  var value = dir.value;
  var modifiers = dir.modifiers;
  var tag = el.tag;
  var type = el.attrsMap.type;

  {
    // inputs with type="file" are read only and setting the input's
    // value will throw an error.
    if (tag === 'input' && type === 'file') {
      warn$1(
        "<" + (el.tag) + " v-model=\"" + value + "\" type=\"file\">:\n" +
        "File inputs are read only. Use a v-on:change listener instead.",
        el.rawAttrsMap['v-model']
      );
    }
  }

  if (el.component) {
    genComponentModel(el, value, modifiers);
    // component v-model doesn't need extra runtime
    return false
  } else if (tag === 'select') {
    genSelect(el, value, modifiers);
  } else if (tag === 'input' && type === 'checkbox') {
    genCheckboxModel(el, value, modifiers);
  } else if (tag === 'input' && type === 'radio') {
    genRadioModel(el, value, modifiers);
  } else if (tag === 'input' || tag === 'textarea') {
    genDefaultModel(el, value, modifiers);
  } else if (!config.isReservedTag(tag)) {
    genComponentModel(el, value, modifiers);
    // component v-model doesn't need extra runtime
    return false
  } else {
    warn$1(
      "<" + (el.tag) + " v-model=\"" + value + "\">: " +
      "v-model is not supported on this element type. " +
      'If you are working with contenteditable, it\'s recommended to ' +
      'wrap a library dedicated for that purpose inside a custom component.',
      el.rawAttrsMap['v-model']
    );
  }

  // ensure runtime directive metadata
  return true
}

function genCheckboxModel (
  el,
  value,
  modifiers
) {
  var number = modifiers && modifiers.number;
  var valueBinding = getBindingAttr(el, 'value') || 'null';
  var trueValueBinding = getBindingAttr(el, 'true-value') || 'true';
  var falseValueBinding = getBindingAttr(el, 'false-value') || 'false';
  addProp(el, 'checked',
    "Array.isArray(" + value + ")" +
    "?_i(" + value + "," + valueBinding + ")>-1" + (
      trueValueBinding === 'true'
        ? (":(" + value + ")")
        : (":_q(" + value + "," + trueValueBinding + ")")
    )
  );
  addHandler(el, 'change',
    "var $$a=" + value + "," +
        '$$el=$event.target,' +
        "$$c=$$el.checked?(" + trueValueBinding + "):(" + falseValueBinding + ");" +
    'if(Array.isArray($$a)){' +
      "var $$v=" + (number ? '_n(' + valueBinding + ')' : valueBinding) + "," +
          '$$i=_i($$a,$$v);' +
      "if($$el.checked){$$i<0&&(" + (genAssignmentCode(value, '$$a.concat([$$v])')) + ")}" +
      "else{$$i>-1&&(" + (genAssignmentCode(value, '$$a.slice(0,$$i).concat($$a.slice($$i+1))')) + ")}" +
    "}else{" + (genAssignmentCode(value, '$$c')) + "}",
    null, true
  );
}

function genRadioModel (
  el,
  value,
  modifiers
) {
  var number = modifiers && modifiers.number;
  var valueBinding = getBindingAttr(el, 'value') || 'null';
  valueBinding = number ? ("_n(" + valueBinding + ")") : valueBinding;
  addProp(el, 'checked', ("_q(" + value + "," + valueBinding + ")"));
  addHandler(el, 'change', genAssignmentCode(value, valueBinding), null, true);
}

function genSelect (
  el,
  value,
  modifiers
) {
  var number = modifiers && modifiers.number;
  var selectedVal = "Array.prototype.filter" +
    ".call($event.target.options,function(o){return o.selected})" +
    ".map(function(o){var val = \"_value\" in o ? o._value : o.value;" +
    "return " + (number ? '_n(val)' : 'val') + "})";

  var assignment = '$event.target.multiple ? $$selectedVal : $$selectedVal[0]';
  var code = "var $$selectedVal = " + selectedVal + ";";
  code = code + " " + (genAssignmentCode(value, assignment));
  addHandler(el, 'change', code, null, true);
}

function genDefaultModel (
  el,
  value,
  modifiers
) {
  var type = el.attrsMap.type;

  // warn if v-bind:value conflicts with v-model
  // except for inputs with v-bind:type
  {
    var value$1 = el.attrsMap['v-bind:value'] || el.attrsMap[':value'];
    var typeBinding = el.attrsMap['v-bind:type'] || el.attrsMap[':type'];
    if (value$1 && !typeBinding) {
      var binding = el.attrsMap['v-bind:value'] ? 'v-bind:value' : ':value';
      warn$1(
        binding + "=\"" + value$1 + "\" conflicts with v-model on the same element " +
        'because the latter already expands to a value binding internally',
        el.rawAttrsMap[binding]
      );
    }
  }

  var ref = modifiers || {};
  var lazy = ref.lazy;
  var number = ref.number;
  var trim = ref.trim;
  var needCompositionGuard = !lazy && type !== 'range';
  var event = lazy
    ? 'change'
    : type === 'range'
      ? RANGE_TOKEN
      : 'input';

  var valueExpression = '$event.target.value';
  if (trim) {
    valueExpression = "$event.target.value.trim()";
  }
  if (number) {
    valueExpression = "_n(" + valueExpression + ")";
  }

  var code = genAssignmentCode(value, valueExpression);
  if (needCompositionGuard) {
    code = "if($event.target.composing)return;" + code;
  }

  addProp(el, 'value', ("(" + value + ")"));
  addHandler(el, event, code, null, true);
  if (trim || number) {
    addHandler(el, 'blur', '$forceUpdate()');
  }
}

/*  */

// normalize v-model event tokens that can only be determined at runtime.
// it's important to place the event as the first in the array because
// the whole point is ensuring the v-model callback gets called before
// user-attached handlers.
function normalizeEvents (on) {
  /* istanbul ignore if */
  if (isDef(on[RANGE_TOKEN])) {
    // IE input[type=range] only supports `change` event
    var event = isIE ? 'change' : 'input';
    on[event] = [].concat(on[RANGE_TOKEN], on[event] || []);
    delete on[RANGE_TOKEN];
  }
  // This was originally intended to fix #4521 but no longer necessary
  // after 2.5. Keeping it for backwards compat with generated code from < 2.4
  /* istanbul ignore if */
  if (isDef(on[CHECKBOX_RADIO_TOKEN])) {
    on.change = [].concat(on[CHECKBOX_RADIO_TOKEN], on.change || []);
    delete on[CHECKBOX_RADIO_TOKEN];
  }
}

var target$1;

function createOnceHandler$1 (event, handler, capture) {
  var _target = target$1; // save current target element in closure
  return function onceHandler () {
    var res = handler.apply(null, arguments);
    if (res !== null) {
      remove$2(event, onceHandler, capture, _target);
    }
  }
}

// #9446: Firefox <= 53 (in particular, ESR 52) has incorrect Event.timeStamp
// implementation and does not fire microtasks in between event propagation, so
// safe to exclude.
var useMicrotaskFix = isUsingMicroTask && !(isFF && Number(isFF[1]) <= 53);

function add$1 (
  name,
  handler,
  capture,
  passive
) {
  // async edge case #6566: inner click event triggers patch, event handler
  // attached to outer element during patch, and triggered again. This
  // happens because browsers fire microtask ticks between event propagation.
  // the solution is simple: we save the timestamp when a handler is attached,
  // and the handler would only fire if the event passed to it was fired
  // AFTER it was attached.
  if (useMicrotaskFix) {
    var attachedTimestamp = currentFlushTimestamp;
    var original = handler;
    handler = original._wrapper = function (e) {
      if (
        // no bubbling, should always fire.
        // this is just a safety net in case event.timeStamp is unreliable in
        // certain weird environments...
        e.target === e.currentTarget ||
        // event is fired after handler attachment
        e.timeStamp >= attachedTimestamp ||
        // bail for environments that have buggy event.timeStamp implementations
        // #9462 iOS 9 bug: event.timeStamp is 0 after history.pushState
        // #9681 QtWebEngine event.timeStamp is negative value
        e.timeStamp <= 0 ||
        // #9448 bail if event is fired in another document in a multi-page
        // electron/nw.js app, since event.timeStamp will be using a different
        // starting reference
        e.target.ownerDocument !== document
      ) {
        return original.apply(this, arguments)
      }
    };
  }
  target$1.addEventListener(
    name,
    handler,
    supportsPassive
      ? { capture: capture, passive: passive }
      : capture
  );
}

function remove$2 (
  name,
  handler,
  capture,
  _target
) {
  (_target || target$1).removeEventListener(
    name,
    handler._wrapper || handler,
    capture
  );
}

function updateDOMListeners (oldVnode, vnode) {
  if (isUndef(oldVnode.data.on) && isUndef(vnode.data.on)) {
    return
  }
  var on = vnode.data.on || {};
  var oldOn = oldVnode.data.on || {};
  target$1 = vnode.elm;
  normalizeEvents(on);
  updateListeners(on, oldOn, add$1, remove$2, createOnceHandler$1, vnode.context);
  target$1 = undefined;
}

var events = {
  create: updateDOMListeners,
  update: updateDOMListeners
};

/*  */

var svgContainer;

function updateDOMProps (oldVnode, vnode) {
  if (isUndef(oldVnode.data.domProps) && isUndef(vnode.data.domProps)) {
    return
  }
  var key, cur;
  var elm = vnode.elm;
  var oldProps = oldVnode.data.domProps || {};
  var props = vnode.data.domProps || {};
  // clone observed objects, as the user probably wants to mutate it
  if (isDef(props.__ob__)) {
    props = vnode.data.domProps = extend({}, props);
  }

  for (key in oldProps) {
    if (!(key in props)) {
      elm[key] = '';
    }
  }

  for (key in props) {
    cur = props[key];
    // ignore children if the node has textContent or innerHTML,
    // as these will throw away existing DOM nodes and cause removal errors
    // on subsequent patches (#3360)
    if (key === 'textContent' || key === 'innerHTML') {
      if (vnode.children) { vnode.children.length = 0; }
      if (cur === oldProps[key]) { continue }
      // #6601 work around Chrome version <= 55 bug where single textNode
      // replaced by innerHTML/textContent retains its parentNode property
      if (elm.childNodes.length === 1) {
        elm.removeChild(elm.childNodes[0]);
      }
    }

    if (key === 'value' && elm.tagName !== 'PROGRESS') {
      // store value as _value as well since
      // non-string values will be stringified
      elm._value = cur;
      // avoid resetting cursor position when value is the same
      var strCur = isUndef(cur) ? '' : String(cur);
      if (shouldUpdateValue(elm, strCur)) {
        elm.value = strCur;
      }
    } else if (key === 'innerHTML' && isSVG(elm.tagName) && isUndef(elm.innerHTML)) {
      // IE doesn't support innerHTML for SVG elements
      svgContainer = svgContainer || document.createElement('div');
      svgContainer.innerHTML = "<svg>" + cur + "</svg>";
      var svg = svgContainer.firstChild;
      while (elm.firstChild) {
        elm.removeChild(elm.firstChild);
      }
      while (svg.firstChild) {
        elm.appendChild(svg.firstChild);
      }
    } else if (
      // skip the update if old and new VDOM state is the same.
      // `value` is handled separately because the DOM value may be temporarily
      // out of sync with VDOM state due to focus, composition and modifiers.
      // This  #4521 by skipping the unnecesarry `checked` update.
      cur !== oldProps[key]
    ) {
      // some property updates can throw
      // e.g. `value` on <progress> w/ non-finite value
      try {
        elm[key] = cur;
      } catch (e) {}
    }
  }
}

// check platforms/web/util/attrs.js acceptValue


function shouldUpdateValue (elm, checkVal) {
  return (!elm.composing && (
    elm.tagName === 'OPTION' ||
    isNotInFocusAndDirty(elm, checkVal) ||
    isDirtyWithModifiers(elm, checkVal)
  ))
}

function isNotInFocusAndDirty (elm, checkVal) {
  // return true when textbox (.number and .trim) loses focus and its value is
  // not equal to the updated value
  var notInFocus = true;
  // #6157
  // work around IE bug when accessing document.activeElement in an iframe
  try { notInFocus = document.activeElement !== elm; } catch (e) {}
  return notInFocus && elm.value !== checkVal
}

function isDirtyWithModifiers (elm, newVal) {
  var value = elm.value;
  var modifiers = elm._vModifiers; // injected by v-model runtime
  if (isDef(modifiers)) {
    if (modifiers.number) {
      return toNumber(value) !== toNumber(newVal)
    }
    if (modifiers.trim) {
      return value.trim() !== newVal.trim()
    }
  }
  return value !== newVal
}

var domProps = {
  create: updateDOMProps,
  update: updateDOMProps
};

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// merge static and dynamic style data on the same vnode
function normalizeStyleData (data) {
  var style = normalizeStyleBinding(data.style);
  // static style is pre-processed into an object during compilation
  // and is always a fresh object, so it's safe to merge into it
  return data.staticStyle
    ? extend(data.staticStyle, style)
    : style
}

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/**
 * parent component style should be after child's
 * so that parent component's style could override it
 */
function getStyle (vnode, checkChild) {
  var res = {};
  var styleData;

  if (checkChild) {
    var childNode = vnode;
    while (childNode.componentInstance) {
      childNode = childNode.componentInstance._vnode;
      if (
        childNode && childNode.data &&
        (styleData = normalizeStyleData(childNode.data))
      ) {
        extend(res, styleData);
      }
    }
  }

  if ((styleData = normalizeStyleData(vnode.data))) {
    extend(res, styleData);
  }

  var parentNode = vnode;
  while ((parentNode = parentNode.parent)) {
    if (parentNode.data && (styleData = normalizeStyleData(parentNode.data))) {
      extend(res, styleData);
    }
  }
  return res
}

/*  */

var cssVarRE = /^--/;
var importantRE = /\s*!important$/;
var setProp = function (el, name, val) {
  /* istanbul ignore if */
  if (cssVarRE.test(name)) {
    el.style.setProperty(name, val);
  } else if (importantRE.test(val)) {
    el.style.setProperty(hyphenate(name), val.replace(importantRE, ''), 'important');
  } else {
    var normalizedName = normalize(name);
    if (Array.isArray(val)) {
      // Support values array created by autoprefixer, e.g.
      // {display: ["-webkit-box", "-ms-flexbox", "flex"]}
      // Set them one by one, and the browser will only set those it can recognize
      for (var i = 0, len = val.length; i < len; i++) {
        el.style[normalizedName] = val[i];
      }
    } else {
      el.style[normalizedName] = val;
    }
  }
};

var vendorNames = ['Webkit', 'Moz', 'ms'];

var emptyStyle;
var normalize = cached(function (prop) {
  emptyStyle = emptyStyle || document.createElement('div').style;
  prop = camelize(prop);
  if (prop !== 'filter' && (prop in emptyStyle)) {
    return prop
  }
  var capName = prop.charAt(0).toUpperCase() + prop.slice(1);
  for (var i = 0; i < vendorNames.length; i++) {
    var name = vendorNames[i] + capName;
    if (name in emptyStyle) {
      return name
    }
  }
});

function updateStyle (oldVnode, vnode) {
  var data = vnode.data;
  var oldData = oldVnode.data;

  if (isUndef(data.staticStyle) && isUndef(data.style) &&
    isUndef(oldData.staticStyle) && isUndef(oldData.style)
  ) {
    return
  }

  var cur, name;
  var el = vnode.elm;
  var oldStaticStyle = oldData.staticStyle;
  var oldStyleBinding = oldData.normalizedStyle || oldData.style || {};

  // if static style exists, stylebinding already merged into it when doing normalizeStyleData
  var oldStyle = oldStaticStyle || oldStyleBinding;

  var style = normalizeStyleBinding(vnode.data.style) || {};

  // store normalized style under a different key for next diff
  // make sure to clone it if it's reactive, since the user likely wants
  // to mutate it.
  vnode.data.normalizedStyle = isDef(style.__ob__)
    ? extend({}, style)
    : style;

  var newStyle = getStyle(vnode, true);

  for (name in oldStyle) {
    if (isUndef(newStyle[name])) {
      setProp(el, name, '');
    }
  }
  for (name in newStyle) {
    cur = newStyle[name];
    if (cur !== oldStyle[name]) {
      // ie9 setting to null has no effect, must use empty string
      setProp(el, name, cur == null ? '' : cur);
    }
  }
}

var style = {
  create: updateStyle,
  update: updateStyle
};

/*  */

var whitespaceRE = /\s+/;

/**
 * Add class with compatibility for SVG since classList is not supported on
 * SVG elements in IE
 */
function addClass (el, cls) {
  /* istanbul ignore if */
  if (!cls || !(cls = cls.trim())) {
    return
  }

  /* istanbul ignore else */
  if (el.classList) {
    if (cls.indexOf(' ') > -1) {
      cls.split(whitespaceRE).forEach(function (c) { return el.classList.add(c); });
    } else {
      el.classList.add(cls);
    }
  } else {
    var cur = " " + (el.getAttribute('class') || '') + " ";
    if (cur.indexOf(' ' + cls + ' ') < 0) {
      el.setAttribute('class', (cur + cls).trim());
    }
  }
}

/**
 * Remove class with compatibility for SVG since classList is not supported on
 * SVG elements in IE
 */
function removeClass (el, cls) {
  /* istanbul ignore if */
  if (!cls || !(cls = cls.trim())) {
    return
  }

  /* istanbul ignore else */
  if (el.classList) {
    if (cls.indexOf(' ') > -1) {
      cls.split(whitespaceRE).forEach(function (c) { return el.classList.remove(c); });
    } else {
      el.classList.remove(cls);
    }
    if (!el.classList.length) {
      el.removeAttribute('class');
    }
  } else {
    var cur = " " + (el.getAttribute('class') || '') + " ";
    var tar = ' ' + cls + ' ';
    while (cur.indexOf(tar) >= 0) {
      cur = cur.replace(tar, ' ');
    }
    cur = cur.trim();
    if (cur) {
      el.setAttribute('class', cur);
    } else {
      el.removeAttribute('class');
    }
  }
}

/*  */

function resolveTransition (def$$1) {
  if (!def$$1) {
    return
  }
  /* istanbul ignore else */
  if (typeof def$$1 === 'object') {
    var res = {};
    if (def$$1.css !== false) {
      extend(res, autoCssTransition(def$$1.name || 'v'));
    }
    extend(res, def$$1);
    return res
  } else if (typeof def$$1 === 'string') {
    return autoCssTransition(def$$1)
  }
}

var autoCssTransition = cached(function (name) {
  return {
    enterClass: (name + "-enter"),
    enterToClass: (name + "-enter-to"),
    enterActiveClass: (name + "-enter-active"),
    leaveClass: (name + "-leave"),
    leaveToClass: (name + "-leave-to"),
    leaveActiveClass: (name + "-leave-active")
  }
});

var hasTransition = inBrowser && !isIE9;
var TRANSITION = 'transition';
var ANIMATION = 'animation';

// Transition property/event sniffing
var transitionProp = 'transition';
var transitionEndEvent = 'transitionend';
var animationProp = 'animation';
var animationEndEvent = 'animationend';
if (hasTransition) {
  /* istanbul ignore if */
  if (window.ontransitionend === undefined &&
    window.onwebkittransitionend !== undefined
  ) {
    transitionProp = 'WebkitTransition';
    transitionEndEvent = 'webkitTransitionEnd';
  }
  if (window.onanimationend === undefined &&
    window.onwebkitanimationend !== undefined
  ) {
    animationProp = 'WebkitAnimation';
    animationEndEvent = 'webkitAnimationEnd';
  }
}

// binding to window is necessary to make hot reload work in IE in strict mode
var raf = inBrowser
  ? window.requestAnimationFrame
    ? window.requestAnimationFrame.bind(window)
    : setTimeout
  : /* istanbul ignore next */ function (fn) { return fn(); };

function nextFrame (fn) {
  raf(function () {
    raf(fn);
  });
}

function addTransitionClass (el, cls) {
  var transitionClasses = el._transitionClasses || (el._transitionClasses = []);
  if (transitionClasses.indexOf(cls) < 0) {
    transitionClasses.push(cls);
    addClass(el, cls);
  }
}

function removeTransitionClass (el, cls) {
  if (el._transitionClasses) {
    remove(el._transitionClasses, cls);
  }
  removeClass(el, cls);
}

function whenTransitionEnds (
  el,
  expectedType,
  cb
) {
  var ref = getTransitionInfo(el, expectedType);
  var type = ref.type;
  var timeout = ref.timeout;
  var propCount = ref.propCount;
  if (!type) { return cb() }
  var event = type === TRANSITION ? transitionEndEvent : animationEndEvent;
  var ended = 0;
  var end = function () {
    el.removeEventListener(event, onEnd);
    cb();
  };
  var onEnd = function (e) {
    if (e.target === el) {
      if (++ended >= propCount) {
        end();
      }
    }
  };
  setTimeout(function () {
    if (ended < propCount) {
      end();
    }
  }, timeout + 1);
  el.addEventListener(event, onEnd);
}

var transformRE = /\b(transform|all)(,|$)/;

function getTransitionInfo (el, expectedType) {
  var styles = window.getComputedStyle(el);
  // JSDOM may return undefined for transition properties
  var transitionDelays = (styles[transitionProp + 'Delay'] || '').split(', ');
  var transitionDurations = (styles[transitionProp + 'Duration'] || '').split(', ');
  var transitionTimeout = getTimeout(transitionDelays, transitionDurations);
  var animationDelays = (styles[animationProp + 'Delay'] || '').split(', ');
  var animationDurations = (styles[animationProp + 'Duration'] || '').split(', ');
  var animationTimeout = getTimeout(animationDelays, animationDurations);

  var type;
  var timeout = 0;
  var propCount = 0;
  /* istanbul ignore if */
  if (expectedType === TRANSITION) {
    if (transitionTimeout > 0) {
      type = TRANSITION;
      timeout = transitionTimeout;
      propCount = transitionDurations.length;
    }
  } else if (expectedType === ANIMATION) {
    if (animationTimeout > 0) {
      type = ANIMATION;
      timeout = animationTimeout;
      propCount = animationDurations.length;
    }
  } else {
    timeout = Math.max(transitionTimeout, animationTimeout);
    type = timeout > 0
      ? transitionTimeout > animationTimeout
        ? TRANSITION
        : ANIMATION
      : null;
    propCount = type
      ? type === TRANSITION
        ? transitionDurations.length
        : animationDurations.length
      : 0;
  }
  var hasTransform =
    type === TRANSITION &&
    transformRE.test(styles[transitionProp + 'Property']);
  return {
    type: type,
    timeout: timeout,
    propCount: propCount,
    hasTransform: hasTransform
  }
}

function getTimeout (delays, durations) {
  /* istanbul ignore next */
  while (delays.length < durations.length) {
    delays = delays.concat(delays);
  }

  return Math.max.apply(null, durations.map(function (d, i) {
    return toMs(d) + toMs(delays[i])
  }))
}

// Old versions of Chromium (below 61.0.3163.100) formats floating pointer numbers
// in a locale-dependent way, using a comma instead of a dot.
// If comma is not replaced with a dot, the input will be rounded down (i.e. acting
// as a floor function) causing unexpected behaviors
function toMs (s) {
  return Number(s.slice(0, -1).replace(',', '.')) * 1000
}

/*  */

function enter (vnode, toggleDisplay) {
  var el = vnode.elm;

  // call leave callback now
  if (isDef(el._leaveCb)) {
    el._leaveCb.cancelled = true;
    el._leaveCb();
  }

  var data = resolveTransition(vnode.data.transition);
  if (isUndef(data)) {
    return
  }

  /* istanbul ignore if */
  if (isDef(el._enterCb) || el.nodeType !== 1) {
    return
  }

  var css = data.css;
  var type = data.type;
  var enterClass = data.enterClass;
  var enterToClass = data.enterToClass;
  var enterActiveClass = data.enterActiveClass;
  var appearClass = data.appearClass;
  var appearToClass = data.appearToClass;
  var appearActiveClass = data.appearActiveClass;
  var beforeEnter = data.beforeEnter;
  var enter = data.enter;
  var afterEnter = data.afterEnter;
  var enterCancelled = data.enterCancelled;
  var beforeAppear = data.beforeAppear;
  var appear = data.appear;
  var afterAppear = data.afterAppear;
  var appearCancelled = data.appearCancelled;
  var duration = data.duration;

  // activeInstance will always be the <transition> component managing this
  // transition. One edge case to check is when the <transition> is placed
  // as the root node of a child component. In that case we need to check
  // <transition>'s parent for appear check.
  var context = activeInstance;
  var transitionNode = activeInstance.$vnode;
  while (transitionNode && transitionNode.parent) {
    context = transitionNode.context;
    transitionNode = transitionNode.parent;
  }

  var isAppear = !context._isMounted || !vnode.isRootInsert;

  if (isAppear && !appear && appear !== '') {
    return
  }

  var startClass = isAppear && appearClass
    ? appearClass
    : enterClass;
  var activeClass = isAppear && appearActiveClass
    ? appearActiveClass
    : enterActiveClass;
  var toClass = isAppear && appearToClass
    ? appearToClass
    : enterToClass;

  var beforeEnterHook = isAppear
    ? (beforeAppear || beforeEnter)
    : beforeEnter;
  var enterHook = isAppear
    ? (typeof appear === 'function' ? appear : enter)
    : enter;
  var afterEnterHook = isAppear
    ? (afterAppear || afterEnter)
    : afterEnter;
  var enterCancelledHook = isAppear
    ? (appearCancelled || enterCancelled)
    : enterCancelled;

  var explicitEnterDuration = toNumber(
    isObject(duration)
      ? duration.enter
      : duration
  );

  if (explicitEnterDuration != null) {
    checkDuration(explicitEnterDuration, 'enter', vnode);
  }

  var expectsCSS = css !== false && !isIE9;
  var userWantsControl = getHookArgumentsLength(enterHook);

  var cb = el._enterCb = once(function () {
    if (expectsCSS) {
      removeTransitionClass(el, toClass);
      removeTransitionClass(el, activeClass);
    }
    if (cb.cancelled) {
      if (expectsCSS) {
        removeTransitionClass(el, startClass);
      }
      enterCancelledHook && enterCancelledHook(el);
    } else {
      afterEnterHook && afterEnterHook(el);
    }
    el._enterCb = null;
  });

  if (!vnode.data.show) {
    // remove pending leave element on enter by injecting an insert hook
    mergeVNodeHook(vnode, 'insert', function () {
      var parent = el.parentNode;
      var pendingNode = parent && parent._pending && parent._pending[vnode.key];
      if (pendingNode &&
        pendingNode.tag === vnode.tag &&
        pendingNode.elm._leaveCb
      ) {
        pendingNode.elm._leaveCb();
      }
      enterHook && enterHook(el, cb);
    });
  }

  // start enter transition
  beforeEnterHook && beforeEnterHook(el);
  if (expectsCSS) {
    addTransitionClass(el, startClass);
    addTransitionClass(el, activeClass);
    nextFrame(function () {
      removeTransitionClass(el, startClass);
      if (!cb.cancelled) {
        addTransitionClass(el, toClass);
        if (!userWantsControl) {
          if (isValidDuration(explicitEnterDuration)) {
            setTimeout(cb, explicitEnterDuration);
          } else {
            whenTransitionEnds(el, type, cb);
          }
        }
      }
    });
  }

  if (vnode.data.show) {
    toggleDisplay && toggleDisplay();
    enterHook && enterHook(el, cb);
  }

  if (!expectsCSS && !userWantsControl) {
    cb();
  }
}

function leave (vnode, rm) {
  var el = vnode.elm;

  // call enter callback now
  if (isDef(el._enterCb)) {
    el._enterCb.cancelled = true;
    el._enterCb();
  }

  var data = resolveTransition(vnode.data.transition);
  if (isUndef(data) || el.nodeType !== 1) {
    return rm()
  }

  /* istanbul ignore if */
  if (isDef(el._leaveCb)) {
    return
  }

  var css = data.css;
  var type = data.type;
  var leaveClass = data.leaveClass;
  var leaveToClass = data.leaveToClass;
  var leaveActiveClass = data.leaveActiveClass;
  var beforeLeave = data.beforeLeave;
  var leave = data.leave;
  var afterLeave = data.afterLeave;
  var leaveCancelled = data.leaveCancelled;
  var delayLeave = data.delayLeave;
  var duration = data.duration;

  var expectsCSS = css !== false && !isIE9;
  var userWantsControl = getHookArgumentsLength(leave);

  var explicitLeaveDuration = toNumber(
    isObject(duration)
      ? duration.leave
      : duration
  );

  if (isDef(explicitLeaveDuration)) {
    checkDuration(explicitLeaveDuration, 'leave', vnode);
  }

  var cb = el._leaveCb = once(function () {
    if (el.parentNode && el.parentNode._pending) {
      el.parentNode._pending[vnode.key] = null;
    }
    if (expectsCSS) {
      removeTransitionClass(el, leaveToClass);
      removeTransitionClass(el, leaveActiveClass);
    }
    if (cb.cancelled) {
      if (expectsCSS) {
        removeTransitionClass(el, leaveClass);
      }
      leaveCancelled && leaveCancelled(el);
    } else {
      rm();
      afterLeave && afterLeave(el);
    }
    el._leaveCb = null;
  });

  if (delayLeave) {
    delayLeave(performLeave);
  } else {
    performLeave();
  }

  function performLeave () {
    // the delayed leave may have already been cancelled
    if (cb.cancelled) {
      return
    }
    // record leaving element
    if (!vnode.data.show && el.parentNode) {
      (el.parentNode._pending || (el.parentNode._pending = {}))[(vnode.key)] = vnode;
    }
    beforeLeave && beforeLeave(el);
    if (expectsCSS) {
      addTransitionClass(el, leaveClass);
      addTransitionClass(el, leaveActiveClass);
      nextFrame(function () {
        removeTransitionClass(el, leaveClass);
        if (!cb.cancelled) {
          addTransitionClass(el, leaveToClass);
          if (!userWantsControl) {
            if (isValidDuration(explicitLeaveDuration)) {
              setTimeout(cb, explicitLeaveDuration);
            } else {
              whenTransitionEnds(el, type, cb);
            }
          }
        }
      });
    }
    leave && leave(el, cb);
    if (!expectsCSS && !userWantsControl) {
      cb();
    }
  }
}

// only used in dev mode
function checkDuration (val, name, vnode) {
  if (typeof val !== 'number') {
    warn(
      "<transition> explicit " + name + " duration is not a valid number - " +
      "got " + (JSON.stringify(val)) + ".",
      vnode.context
    );
  } else if (isNaN(val)) {
    warn(
      "<transition> explicit " + name + " duration is NaN - " +
      'the duration expression might be incorrect.',
      vnode.context
    );
  }
}

function isValidDuration (val) {
  return typeof val === 'number' && !isNaN(val)
}

/**
 * Normalize a transition hook's argument length. The hook may be:
 * - a merged hook (invoker) with the original in .fns
 * - a wrapped component method (check ._length)
 * - a plain function (.length)
 */
function getHookArgumentsLength (fn) {
  if (isUndef(fn)) {
    return false
  }
  var invokerFns = fn.fns;
  if (isDef(invokerFns)) {
    // invoker
    return getHookArgumentsLength(
      Array.isArray(invokerFns)
        ? invokerFns[0]
        : invokerFns
    )
  } else {
    return (fn._length || fn.length) > 1
  }
}

function _enter (_, vnode) {
  if (vnode.data.show !== true) {
    enter(vnode);
  }
}

var transition = inBrowser ? {
  create: _enter,
  activate: _enter,
  remove: function remove$$1 (vnode, rm) {
    /* istanbul ignore else */
    if (vnode.data.show !== true) {
      leave(vnode, rm);
    } else {
      rm();
    }
  }
} : {};

var platformModules = [
  attrs,
  klass,
  events,
  domProps,
  style,
  transition
];

/*  */

// the directive module should be applied last, after all
// built-in modules have been applied.
var modules = platformModules.concat(baseModules);

var patch = createPatchFunction({ nodeOps: nodeOps, modules: modules });

/**
 * Not type checking this file because flow doesn't like attaching
 * properties to Elements.
 */

/* istanbul ignore if */
if (isIE9) {
  // http://www.matts411.com/post/internet-explorer-9-oninput/
  document.addEventListener('selectionchange', function () {
    var el = document.activeElement;
    if (el && el.vmodel) {
      trigger(el, 'input');
    }
  });
}

var directive = {
  inserted: function inserted (el, binding, vnode, oldVnode) {
    if (vnode.tag === 'select') {
      // #6903
      if (oldVnode.elm && !oldVnode.elm._vOptions) {
        mergeVNodeHook(vnode, 'postpatch', function () {
          directive.componentUpdated(el, binding, vnode);
        });
      } else {
        setSelected(el, binding, vnode.context);
      }
      el._vOptions = [].map.call(el.options, getValue);
    } else if (vnode.tag === 'textarea' || isTextInputType(el.type)) {
      el._vModifiers = binding.modifiers;
      if (!binding.modifiers.lazy) {
        el.addEventListener('compositionstart', onCompositionStart);
        el.addEventListener('compositionend', onCompositionEnd);
        // Safari < 10.2 & UIWebView doesn't fire compositionend when
        // switching focus before confirming composition choice
        // this also fixes the issue where some browsers e.g. iOS Chrome
        // fires "change" instead of "input" on autocomplete.
        el.addEventListener('change', onCompositionEnd);
        /* istanbul ignore if */
        if (isIE9) {
          el.vmodel = true;
        }
      }
    }
  },

  componentUpdated: function componentUpdated (el, binding, vnode) {
    if (vnode.tag === 'select') {
      setSelected(el, binding, vnode.context);
      // in case the options rendered by v-for have changed,
      // it's possible that the value is out-of-sync with the rendered options.
      // detect such cases and filter out values that no longer has a matching
      // option in the DOM.
      var prevOptions = el._vOptions;
      var curOptions = el._vOptions = [].map.call(el.options, getValue);
      if (curOptions.some(function (o, i) { return !looseEqual(o, prevOptions[i]); })) {
        // trigger change event if
        // no matching option found for at least one value
        var needReset = el.multiple
          ? binding.value.some(function (v) { return hasNoMatchingOption(v, curOptions); })
          : binding.value !== binding.oldValue && hasNoMatchingOption(binding.value, curOptions);
        if (needReset) {
          trigger(el, 'change');
        }
      }
    }
  }
};

function setSelected (el, binding, vm) {
  actuallySetSelected(el, binding, vm);
  /* istanbul ignore if */
  if (isIE || isEdge) {
    setTimeout(function () {
      actuallySetSelected(el, binding, vm);
    }, 0);
  }
}

function actuallySetSelected (el, binding, vm) {
  var value = binding.value;
  var isMultiple = el.multiple;
  if (isMultiple && !Array.isArray(value)) {
    warn(
      "<select multiple v-model=\"" + (binding.expression) + "\"> " +
      "expects an Array value for its binding, but got " + (Object.prototype.toString.call(value).slice(8, -1)),
      vm
    );
    return
  }
  var selected, option;
  for (var i = 0, l = el.options.length; i < l; i++) {
    option = el.options[i];
    if (isMultiple) {
      selected = looseIndexOf(value, getValue(option)) > -1;
      if (option.selected !== selected) {
        option.selected = selected;
      }
    } else {
      if (looseEqual(getValue(option), value)) {
        if (el.selectedIndex !== i) {
          el.selectedIndex = i;
        }
        return
      }
    }
  }
  if (!isMultiple) {
    el.selectedIndex = -1;
  }
}

function hasNoMatchingOption (value, options) {
  return options.every(function (o) { return !looseEqual(o, value); })
}

function getValue (option) {
  return '_value' in option
    ? option._value
    : option.value
}

function onCompositionStart (e) {
  e.target.composing = true;
}

function onCompositionEnd (e) {
  // prevent triggering an input event for no reason
  if (!e.target.composing) { return }
  e.target.composing = false;
  trigger(e.target, 'input');
}

function trigger (el, type) {
  var e = document.createEvent('HTMLEvents');
  e.initEvent(type, true, true);
  el.dispatchEvent(e);
}

/*  */

// recursively search for possible transition defined inside the component root
function locateNode (vnode) {
  return vnode.componentInstance && (!vnode.data || !vnode.data.transition)
    ? locateNode(vnode.componentInstance._vnode)
    : vnode
}

var show = {
  bind: function bind (el, ref, vnode) {
    var value = ref.value;

    vnode = locateNode(vnode);
    var transition$$1 = vnode.data && vnode.data.transition;
    var originalDisplay = el.__vOriginalDisplay =
      el.style.display === 'none' ? '' : el.style.display;
    if (value && transition$$1) {
      vnode.data.show = true;
      enter(vnode, function () {
        el.style.display = originalDisplay;
      });
    } else {
      el.style.display = value ? originalDisplay : 'none';
    }
  },

  update: function update (el, ref, vnode) {
    var value = ref.value;
    var oldValue = ref.oldValue;

    /* istanbul ignore if */
    if (!value === !oldValue) { return }
    vnode = locateNode(vnode);
    var transition$$1 = vnode.data && vnode.data.transition;
    if (transition$$1) {
      vnode.data.show = true;
      if (value) {
        enter(vnode, function () {
          el.style.display = el.__vOriginalDisplay;
        });
      } else {
        leave(vnode, function () {
          el.style.display = 'none';
        });
      }
    } else {
      el.style.display = value ? el.__vOriginalDisplay : 'none';
    }
  },

  unbind: function unbind (
    el,
    binding,
    vnode,
    oldVnode,
    isDestroy
  ) {
    if (!isDestroy) {
      el.style.display = el.__vOriginalDisplay;
    }
  }
};

var platformDirectives = {
  model: directive,
  show: show
};

/*  */

var transitionProps = {
  name: String,
  appear: Boolean,
  css: Boolean,
  mode: String,
  type: String,
  enterClass: String,
  leaveClass: String,
  enterToClass: String,
  leaveToClass: String,
  enterActiveClass: String,
  leaveActiveClass: String,
  appearClass: String,
  appearActiveClass: String,
  appearToClass: String,
  duration: [Number, String, Object]
};

// in case the child is also an abstract component, e.g. <keep-alive>
// we want to recursively retrieve the real component to be rendered
function getRealChild (vnode) {
  var compOptions = vnode && vnode.componentOptions;
  if (compOptions && compOptions.Ctor.options.abstract) {
    return getRealChild(getFirstComponentChild(compOptions.children))
  } else {
    return vnode
  }
}

function extractTransitionData (comp) {
  var data = {};
  var options = comp.$options;
  // props
  for (var key in options.propsData) {
    data[key] = comp[key];
  }
  // events.
  // extract listeners and pass them directly to the transition methods
  var listeners = options._parentListeners;
  for (var key$1 in listeners) {
    data[camelize(key$1)] = listeners[key$1];
  }
  return data
}

function placeholder (h, rawChild) {
  if (/\d-keep-alive$/.test(rawChild.tag)) {
    return h('keep-alive', {
      props: rawChild.componentOptions.propsData
    })
  }
}

function hasParentTransition (vnode) {
  while ((vnode = vnode.parent)) {
    if (vnode.data.transition) {
      return true
    }
  }
}

function isSameChild (child, oldChild) {
  return oldChild.key === child.key && oldChild.tag === child.tag
}

var isNotTextNode = function (c) { return c.tag || isAsyncPlaceholder(c); };

var isVShowDirective = function (d) { return d.name === 'show'; };

var Transition = {
  name: 'transition',
  props: transitionProps,
  abstract: true,

  render: function render (h) {
    var this$1 = this;

    var children = this.$slots.default;
    if (!children) {
      return
    }

    // filter out text nodes (possible whitespaces)
    children = children.filter(isNotTextNode);
    /* istanbul ignore if */
    if (!children.length) {
      return
    }

    // warn multiple elements
    if (children.length > 1) {
      warn(
        '<transition> can only be used on a single element. Use ' +
        '<transition-group> for lists.',
        this.$parent
      );
    }

    var mode = this.mode;

    // warn invalid mode
    if (mode && mode !== 'in-out' && mode !== 'out-in'
    ) {
      warn(
        'invalid <transition> mode: ' + mode,
        this.$parent
      );
    }

    var rawChild = children[0];

    // if this is a component root node and the component's
    // parent container node also has transition, skip.
    if (hasParentTransition(this.$vnode)) {
      return rawChild
    }

    // apply transition data to child
    // use getRealChild() to ignore abstract components e.g. keep-alive
    var child = getRealChild(rawChild);
    /* istanbul ignore if */
    if (!child) {
      return rawChild
    }

    if (this._leaving) {
      return placeholder(h, rawChild)
    }

    // ensure a key that is unique to the vnode type and to this transition
    // component instance. This key will be used to remove pending leaving nodes
    // during entering.
    var id = "__transition-" + (this._uid) + "-";
    child.key = child.key == null
      ? child.isComment
        ? id + 'comment'
        : id + child.tag
      : isPrimitive(child.key)
        ? (String(child.key).indexOf(id) === 0 ? child.key : id + child.key)
        : child.key;

    var data = (child.data || (child.data = {})).transition = extractTransitionData(this);
    var oldRawChild = this._vnode;
    var oldChild = getRealChild(oldRawChild);

    // mark v-show
    // so that the transition module can hand over the control to the directive
    if (child.data.directives && child.data.directives.some(isVShowDirective)) {
      child.data.show = true;
    }

    if (
      oldChild &&
      oldChild.data &&
      !isSameChild(child, oldChild) &&
      !isAsyncPlaceholder(oldChild) &&
      // #6687 component root is a comment node
      !(oldChild.componentInstance && oldChild.componentInstance._vnode.isComment)
    ) {
      // replace old child transition data with fresh one
      // important for dynamic transitions!
      var oldData = oldChild.data.transition = extend({}, data);
      // handle transition mode
      if (mode === 'out-in') {
        // return placeholder node and queue update when leave finishes
        this._leaving = true;
        mergeVNodeHook(oldData, 'afterLeave', function () {
          this$1._leaving = false;
          this$1.$forceUpdate();
        });
        return placeholder(h, rawChild)
      } else if (mode === 'in-out') {
        if (isAsyncPlaceholder(child)) {
          return oldRawChild
        }
        var delayedLeave;
        var performLeave = function () { delayedLeave(); };
        mergeVNodeHook(data, 'afterEnter', performLeave);
        mergeVNodeHook(data, 'enterCancelled', performLeave);
        mergeVNodeHook(oldData, 'delayLeave', function (leave) { delayedLeave = leave; });
      }
    }

    return rawChild
  }
};

/*  */

var props = extend({
  tag: String,
  moveClass: String
}, transitionProps);

delete props.mode;

var TransitionGroup = {
  props: props,

  beforeMount: function beforeMount () {
    var this$1 = this;

    var update = this._update;
    this._update = function (vnode, hydrating) {
      var restoreActiveInstance = setActiveInstance(this$1);
      // force removing pass
      this$1.__patch__(
        this$1._vnode,
        this$1.kept,
        false, // hydrating
        true // removeOnly (!important, avoids unnecessary moves)
      );
      this$1._vnode = this$1.kept;
      restoreActiveInstance();
      update.call(this$1, vnode, hydrating);
    };
  },

  render: function render (h) {
    var tag = this.tag || this.$vnode.data.tag || 'span';
    var map = Object.create(null);
    var prevChildren = this.prevChildren = this.children;
    var rawChildren = this.$slots.default || [];
    var children = this.children = [];
    var transitionData = extractTransitionData(this);

    for (var i = 0; i < rawChildren.length; i++) {
      var c = rawChildren[i];
      if (c.tag) {
        if (c.key != null && String(c.key).indexOf('__vlist') !== 0) {
          children.push(c);
          map[c.key] = c
          ;(c.data || (c.data = {})).transition = transitionData;
        } else {
          var opts = c.componentOptions;
          var name = opts ? (opts.Ctor.options.name || opts.tag || '') : c.tag;
          warn(("<transition-group> children must be keyed: <" + name + ">"));
        }
      }
    }

    if (prevChildren) {
      var kept = [];
      var removed = [];
      for (var i$1 = 0; i$1 < prevChildren.length; i$1++) {
        var c$1 = prevChildren[i$1];
        c$1.data.transition = transitionData;
        c$1.data.pos = c$1.elm.getBoundingClientRect();
        if (map[c$1.key]) {
          kept.push(c$1);
        } else {
          removed.push(c$1);
        }
      }
      this.kept = h(tag, null, kept);
      this.removed = removed;
    }

    return h(tag, null, children)
  },

  updated: function updated () {
    var children = this.prevChildren;
    var moveClass = this.moveClass || ((this.name || 'v') + '-move');
    if (!children.length || !this.hasMove(children[0].elm, moveClass)) {
      return
    }

    // we divide the work into three loops to avoid mixing DOM reads and writes
    // in each iteration - which helps prevent layout thrashing.
    children.forEach(callPendingCbs);
    children.forEach(recordPosition);
    children.forEach(applyTranslation);

    // force reflow to put everything in position
    // assign to this to avoid being removed in tree-shaking
    // $flow-disable-line
    this._reflow = document.body.offsetHeight;

    children.forEach(function (c) {
      if (c.data.moved) {
        var el = c.elm;
        var s = el.style;
        addTransitionClass(el, moveClass);
        s.transform = s.WebkitTransform = s.transitionDuration = '';
        el.addEventListener(transitionEndEvent, el._moveCb = function cb (e) {
          if (e && e.target !== el) {
            return
          }
          if (!e || /transform$/.test(e.propertyName)) {
            el.removeEventListener(transitionEndEvent, cb);
            el._moveCb = null;
            removeTransitionClass(el, moveClass);
          }
        });
      }
    });
  },

  methods: {
    hasMove: function hasMove (el, moveClass) {
      /* istanbul ignore if */
      if (!hasTransition) {
        return false
      }
      /* istanbul ignore if */
      if (this._hasMove) {
        return this._hasMove
      }
      // Detect whether an element with the move class applied has
      // CSS transitions. Since the element may be inside an entering
      // transition at this very moment, we make a clone of it and remove
      // all other transition classes applied to ensure only the move class
      // is applied.
      var clone = el.cloneNode();
      if (el._transitionClasses) {
        el._transitionClasses.forEach(function (cls) { removeClass(clone, cls); });
      }
      addClass(clone, moveClass);
      clone.style.display = 'none';
      this.$el.appendChild(clone);
      var info = getTransitionInfo(clone);
      this.$el.removeChild(clone);
      return (this._hasMove = info.hasTransform)
    }
  }
};

function callPendingCbs (c) {
  /* istanbul ignore if */
  if (c.elm._moveCb) {
    c.elm._moveCb();
  }
  /* istanbul ignore if */
  if (c.elm._enterCb) {
    c.elm._enterCb();
  }
}

function recordPosition (c) {
  c.data.newPos = c.elm.getBoundingClientRect();
}

function applyTranslation (c) {
  var oldPos = c.data.pos;
  var newPos = c.data.newPos;
  var dx = oldPos.left - newPos.left;
  var dy = oldPos.top - newPos.top;
  if (dx || dy) {
    c.data.moved = true;
    var s = c.elm.style;
    s.transform = s.WebkitTransform = "translate(" + dx + "px," + dy + "px)";
    s.transitionDuration = '0s';
  }
}

var platformComponents = {
  Transition: Transition,
  TransitionGroup: TransitionGroup
};

/*  */

// install platform specific utils
Vue.config.mustUseProp = mustUseProp;
Vue.config.isReservedTag = isReservedTag;
Vue.config.isReservedAttr = isReservedAttr;
Vue.config.getTagNamespace = getTagNamespace;
Vue.config.isUnknownElement = isUnknownElement;

// install platform runtime directives & components
extend(Vue.options.directives, platformDirectives);
extend(Vue.options.components, platformComponents);

// install platform patch function
Vue.prototype.__patch__ = inBrowser ? patch : noop;

// public mount method
Vue.prototype.$mount = function (
  el,
  hydrating
) {
  el = el && inBrowser ? query(el) : undefined;
  return mountComponent(this, el, hydrating)
};

// devtools global hook
/* istanbul ignore next */
if (inBrowser) {
  setTimeout(function () {
    if (config.devtools) {
      if (devtools) {
        devtools.emit('init', Vue);
      } else {
        console[console.info ? 'info' : 'log'](
          'Download the Vue Devtools extension for a better development experience:\n' +
          'https://github.com/vuejs/vue-devtools'
        );
      }
    }
    if (config.productionTip !== false &&
      typeof console !== 'undefined'
    ) {
      console[console.info ? 'info' : 'log'](
        "You are running Vue in development mode.\n" +
        "Make sure to turn on production mode when deploying for production.\n" +
        "See more tips at https://vuejs.org/guide/deployment.html"
      );
    }
  }, 0);
}

/*  */

var defaultTagRE = /\{\{((?:.|\r?\n)+?)\}\}/g;
var regexEscapeRE = /[-.*+?^${}()|[\]\/\\]/g;

var buildRegex = cached(function (delimiters) {
  var open = delimiters[0].replace(regexEscapeRE, '\\$&');
  var close = delimiters[1].replace(regexEscapeRE, '\\$&');
  return new RegExp(open + '((?:.|\\n)+?)' + close, 'g')
});



function parseText (
  text,
  delimiters
) {
  var tagRE = delimiters ? buildRegex(delimiters) : defaultTagRE;
  if (!tagRE.test(text)) {
    return
  }
  var tokens = [];
  var rawTokens = [];
  var lastIndex = tagRE.lastIndex = 0;
  var match, index, tokenValue;
  while ((match = tagRE.exec(text))) {
    index = match.index;
    // push text token
    if (index > lastIndex) {
      rawTokens.push(tokenValue = text.slice(lastIndex, index));
      tokens.push(JSON.stringify(tokenValue));
    }
    // tag token
    var exp = parseFilters(match[1].trim());
    tokens.push(("_s(" + exp + ")"));
    rawTokens.push({ '@binding': exp });
    lastIndex = index + match[0].length;
  }
  if (lastIndex < text.length) {
    rawTokens.push(tokenValue = text.slice(lastIndex));
    tokens.push(JSON.stringify(tokenValue));
  }
  return {
    expression: tokens.join('+'),
    tokens: rawTokens
  }
}

/*  */

function transformNode (el, options) {
  var warn = options.warn || baseWarn;
  var staticClass = getAndRemoveAttr(el, 'class');
  if (staticClass) {
    var res = parseText(staticClass, options.delimiters);
    if (res) {
      warn(
        "class=\"" + staticClass + "\": " +
        'Interpolation inside attributes has been removed. ' +
        'Use v-bind or the colon shorthand instead. For example, ' +
        'instead of <div class="{{ val }}">, use <div :class="val">.',
        el.rawAttrsMap['class']
      );
    }
  }
  if (staticClass) {
    el.staticClass = JSON.stringify(staticClass);
  }
  var classBinding = getBindingAttr(el, 'class', false /* getStatic */);
  if (classBinding) {
    el.classBinding = classBinding;
  }
}

function genData (el) {
  var data = '';
  if (el.staticClass) {
    data += "staticClass:" + (el.staticClass) + ",";
  }
  if (el.classBinding) {
    data += "class:" + (el.classBinding) + ",";
  }
  return data
}

var klass$1 = {
  staticKeys: ['staticClass'],
  transformNode: transformNode,
  genData: genData
};

/*  */

function transformNode$1 (el, options) {
  var warn = options.warn || baseWarn;
  var staticStyle = getAndRemoveAttr(el, 'style');
  if (staticStyle) {
    /* istanbul ignore if */
    {
      var res = parseText(staticStyle, options.delimiters);
      if (res) {
        warn(
          "style=\"" + staticStyle + "\": " +
          'Interpolation inside attributes has been removed. ' +
          'Use v-bind or the colon shorthand instead. For example, ' +
          'instead of <div style="{{ val }}">, use <div :style="val">.',
          el.rawAttrsMap['style']
        );
      }
    }
    el.staticStyle = JSON.stringify(parseStyleText(staticStyle));
  }

  var styleBinding = getBindingAttr(el, 'style', false /* getStatic */);
  if (styleBinding) {
    el.styleBinding = styleBinding;
  }
}

function genData$1 (el) {
  var data = '';
  if (el.staticStyle) {
    data += "staticStyle:" + (el.staticStyle) + ",";
  }
  if (el.styleBinding) {
    data += "style:(" + (el.styleBinding) + "),";
  }
  return data
}

var style$1 = {
  staticKeys: ['staticStyle'],
  transformNode: transformNode$1,
  genData: genData$1
};

/*  */

var decoder;

var he = {
  decode: function decode (html) {
    decoder = decoder || document.createElement('div');
    decoder.innerHTML = html;
    return decoder.textContent
  }
};

/*  */

var isUnaryTag = makeMap(
  'area,base,br,col,embed,frame,hr,img,input,isindex,keygen,' +
  'link,meta,param,source,track,wbr'
);

// Elements that you can, intentionally, leave open
// (and which close themselves)
var canBeLeftOpenTag = makeMap(
  'colgroup,dd,dt,li,options,p,td,tfoot,th,thead,tr,source'
);

// HTML5 tags https://html.spec.whatwg.org/multipage/indices.html#elements-3
// Phrasing Content https://html.spec.whatwg.org/multipage/dom.html#phrasing-content
var isNonPhrasingTag = makeMap(
  'address,article,aside,base,blockquote,body,caption,col,colgroup,dd,' +
  'details,dialog,div,dl,dt,fieldset,figcaption,figure,footer,form,' +
  'h1,h2,h3,h4,h5,h6,head,header,hgroup,hr,html,legend,li,menuitem,meta,' +
  'optgroup,option,param,rp,rt,source,style,summary,tbody,td,tfoot,th,thead,' +
  'title,tr,track'
);

/**
 * Not type-checking this file because it's mostly vendor code.
 */

// Regular Expressions for parsing tags and attributes
var attribute = /^\s*([^\s"'<>\/=]+)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/;
var dynamicArgAttribute = /^\s*((?:v-[\w-]+:|@|:|#)\[[^=]+\][^\s"'<>\/=]*)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/;
var ncname = "[a-zA-Z_][\\-\\.0-9_a-zA-Z" + (unicodeRegExp.source) + "]*";
var qnameCapture = "((?:" + ncname + "\\:)?" + ncname + ")";
var startTagOpen = new RegExp(("^<" + qnameCapture));
var startTagClose = /^\s*(\/?)>/;
var endTag = new RegExp(("^<\\/" + qnameCapture + "[^>]*>"));
var doctype = /^<!DOCTYPE [^>]+>/i;
// #7298: escape - to avoid being pased as HTML comment when inlined in page
var comment = /^<!\--/;
var conditionalComment = /^<!\[/;

// Special Elements (can contain anything)
var isPlainTextElement = makeMap('script,style,textarea', true);
var reCache = {};

var decodingMap = {
  '&lt;': '<',
  '&gt;': '>',
  '&quot;': '"',
  '&amp;': '&',
  '&#10;': '\n',
  '&#9;': '\t',
  '&#39;': "'"
};
var encodedAttr = /&(?:lt|gt|quot|amp|#39);/g;
var encodedAttrWithNewLines = /&(?:lt|gt|quot|amp|#39|#10|#9);/g;

// #5992
var isIgnoreNewlineTag = makeMap('pre,textarea', true);
var shouldIgnoreFirstNewline = function (tag, html) { return tag && isIgnoreNewlineTag(tag) && html[0] === '\n'; };

function decodeAttr (value, shouldDecodeNewlines) {
  var re = shouldDecodeNewlines ? encodedAttrWithNewLines : encodedAttr;
  return value.replace(re, function (match) { return decodingMap[match]; })
}

function parseHTML (html, options) {
  var stack = [];
  var expectHTML = options.expectHTML;
  var isUnaryTag$$1 = options.isUnaryTag || no;
  var canBeLeftOpenTag$$1 = options.canBeLeftOpenTag || no;
  var index = 0;
  var last, lastTag;
  while (html) {
    last = html;
    // Make sure we're not in a plaintext content element like script/style
    if (!lastTag || !isPlainTextElement(lastTag)) {
      var textEnd = html.indexOf('<');
      if (textEnd === 0) {
        // Comment:
        if (comment.test(html)) {
          var commentEnd = html.indexOf('-->');

          if (commentEnd >= 0) {
            if (options.shouldKeepComment) {
              options.comment(html.substring(4, commentEnd), index, index + commentEnd + 3);
            }
            advance(commentEnd + 3);
            continue
          }
        }

        // http://en.wikipedia.org/wiki/Conditional_comment#Downlevel-revealed_conditional_comment
        if (conditionalComment.test(html)) {
          var conditionalEnd = html.indexOf(']>');

          if (conditionalEnd >= 0) {
            advance(conditionalEnd + 2);
            continue
          }
        }

        // Doctype:
        var doctypeMatch = html.match(doctype);
        if (doctypeMatch) {
          advance(doctypeMatch[0].length);
          continue
        }

        // End tag:
        var endTagMatch = html.match(endTag);
        if (endTagMatch) {
          var curIndex = index;
          advance(endTagMatch[0].length);
          parseEndTag(endTagMatch[1], curIndex, index);
          continue
        }

        // Start tag:
        var startTagMatch = parseStartTag();
        if (startTagMatch) {
          handleStartTag(startTagMatch);
          if (shouldIgnoreFirstNewline(startTagMatch.tagName, html)) {
            advance(1);
          }
          continue
        }
      }

      var text = (void 0), rest = (void 0), next = (void 0);
      if (textEnd >= 0) {
        rest = html.slice(textEnd);
        while (
          !endTag.test(rest) &&
          !startTagOpen.test(rest) &&
          !comment.test(rest) &&
          !conditionalComment.test(rest)
        ) {
          // < in plain text, be forgiving and treat it as text
          next = rest.indexOf('<', 1);
          if (next < 0) { break }
          textEnd += next;
          rest = html.slice(textEnd);
        }
        text = html.substring(0, textEnd);
      }

      if (textEnd < 0) {
        text = html;
      }

      if (text) {
        advance(text.length);
      }

      if (options.chars && text) {
        options.chars(text, index - text.length, index);
      }
    } else {
      var endTagLength = 0;
      var stackedTag = lastTag.toLowerCase();
      var reStackedTag = reCache[stackedTag] || (reCache[stackedTag] = new RegExp('([\\s\\S]*?)(</' + stackedTag + '[^>]*>)', 'i'));
      var rest$1 = html.replace(reStackedTag, function (all, text, endTag) {
        endTagLength = endTag.length;
        if (!isPlainTextElement(stackedTag) && stackedTag !== 'noscript') {
          text = text
            .replace(/<!\--([\s\S]*?)-->/g, '$1') // #7298
            .replace(/<!\[CDATA\[([\s\S]*?)]]>/g, '$1');
        }
        if (shouldIgnoreFirstNewline(stackedTag, text)) {
          text = text.slice(1);
        }
        if (options.chars) {
          options.chars(text);
        }
        return ''
      });
      index += html.length - rest$1.length;
      html = rest$1;
      parseEndTag(stackedTag, index - endTagLength, index);
    }

    if (html === last) {
      options.chars && options.chars(html);
      if (!stack.length && options.warn) {
        options.warn(("Mal-formatted tag at end of template: \"" + html + "\""), { start: index + html.length });
      }
      break
    }
  }

  // Clean up any remaining tags
  parseEndTag();

  function advance (n) {
    index += n;
    html = html.substring(n);
  }

  function parseStartTag () {
    var start = html.match(startTagOpen);
    if (start) {
      var match = {
        tagName: start[1],
        attrs: [],
        start: index
      };
      advance(start[0].length);
      var end, attr;
      while (!(end = html.match(startTagClose)) && (attr = html.match(dynamicArgAttribute) || html.match(attribute))) {
        attr.start = index;
        advance(attr[0].length);
        attr.end = index;
        match.attrs.push(attr);
      }
      if (end) {
        match.unarySlash = end[1];
        advance(end[0].length);
        match.end = index;
        return match
      }
    }
  }

  function handleStartTag (match) {
    var tagName = match.tagName;
    var unarySlash = match.unarySlash;

    if (expectHTML) {
      if (lastTag === 'p' && isNonPhrasingTag(tagName)) {
        parseEndTag(lastTag);
      }
      if (canBeLeftOpenTag$$1(tagName) && lastTag === tagName) {
        parseEndTag(tagName);
      }
    }

    var unary = isUnaryTag$$1(tagName) || !!unarySlash;

    var l = match.attrs.length;
    var attrs = new Array(l);
    for (var i = 0; i < l; i++) {
      var args = match.attrs[i];
      var value = args[3] || args[4] || args[5] || '';
      var shouldDecodeNewlines = tagName === 'a' && args[1] === 'href'
        ? options.shouldDecodeNewlinesForHref
        : options.shouldDecodeNewlines;
      attrs[i] = {
        name: args[1],
        value: decodeAttr(value, shouldDecodeNewlines)
      };
      if (options.outputSourceRange) {
        attrs[i].start = args.start + args[0].match(/^\s*/).length;
        attrs[i].end = args.end;
      }
    }

    if (!unary) {
      stack.push({ tag: tagName, lowerCasedTag: tagName.toLowerCase(), attrs: attrs, start: match.start, end: match.end });
      lastTag = tagName;
    }

    if (options.start) {
      options.start(tagName, attrs, unary, match.start, match.end);
    }
  }

  function parseEndTag (tagName, start, end) {
    var pos, lowerCasedTagName;
    if (start == null) { start = index; }
    if (end == null) { end = index; }

    // Find the closest opened tag of the same type
    if (tagName) {
      lowerCasedTagName = tagName.toLowerCase();
      for (pos = stack.length - 1; pos >= 0; pos--) {
        if (stack[pos].lowerCasedTag === lowerCasedTagName) {
          break
        }
      }
    } else {
      // If no tag name is provided, clean shop
      pos = 0;
    }

    if (pos >= 0) {
      // Close all the open elements, up the stack
      for (var i = stack.length - 1; i >= pos; i--) {
        if (i > pos || !tagName &&
          options.warn
        ) {
          options.warn(
            ("tag <" + (stack[i].tag) + "> has no matching end tag."),
            { start: stack[i].start, end: stack[i].end }
          );
        }
        if (options.end) {
          options.end(stack[i].tag, start, end);
        }
      }

      // Remove the open elements from the stack
      stack.length = pos;
      lastTag = pos && stack[pos - 1].tag;
    } else if (lowerCasedTagName === 'br') {
      if (options.start) {
        options.start(tagName, [], true, start, end);
      }
    } else if (lowerCasedTagName === 'p') {
      if (options.start) {
        options.start(tagName, [], false, start, end);
      }
      if (options.end) {
        options.end(tagName, start, end);
      }
    }
  }
}

/*  */

var onRE = /^@|^v-on:/;
var dirRE = /^v-|^@|^:/;
var forAliasRE = /([\s\S]*?)\s+(?:in|of)\s+([\s\S]*)/;
var forIteratorRE = /,([^,\}\]]*)(?:,([^,\}\]]*))?$/;
var stripParensRE = /^\(|\)$/g;
var dynamicArgRE = /^\[.*\]$/;

var argRE = /:(.*)$/;
var bindRE = /^:|^\.|^v-bind:/;
var modifierRE = /\.[^.\]]+(?=[^\]]*$)/g;

var slotRE = /^v-slot(:|$)|^#/;

var lineBreakRE = /[\r\n]/;
var whitespaceRE$1 = /\s+/g;

var invalidAttributeRE = /[\s"'<>\/=]/;

var decodeHTMLCached = cached(he.decode);

var emptySlotScopeToken = "_empty_";

// configurable state
var warn$2;
var delimiters;
var transforms;
var preTransforms;
var postTransforms;
var platformIsPreTag;
var platformMustUseProp;
var platformGetTagNamespace;
var maybeComponent;

function createASTElement (
  tag,
  attrs,
  parent
) {
  return {
    type: 1,
    tag: tag,
    attrsList: attrs,
    attrsMap: makeAttrsMap(attrs),
    rawAttrsMap: {},
    parent: parent,
    children: []
  }
}

/**
 * Convert HTML string to AST.
 */
function parse (
  template,
  options
) {
  warn$2 = options.warn || baseWarn;

  platformIsPreTag = options.isPreTag || no;
  platformMustUseProp = options.mustUseProp || no;
  platformGetTagNamespace = options.getTagNamespace || no;
  var isReservedTag = options.isReservedTag || no;
  maybeComponent = function (el) { return !!el.component || !isReservedTag(el.tag); };

  transforms = pluckModuleFunction(options.modules, 'transformNode');
  preTransforms = pluckModuleFunction(options.modules, 'preTransformNode');
  postTransforms = pluckModuleFunction(options.modules, 'postTransformNode');

  delimiters = options.delimiters;

  var stack = [];
  var preserveWhitespace = options.preserveWhitespace !== false;
  var whitespaceOption = options.whitespace;
  var root;
  var currentParent;
  var inVPre = false;
  var inPre = false;
  var warned = false;

  function warnOnce (msg, range) {
    if (!warned) {
      warned = true;
      warn$2(msg, range);
    }
  }

  function closeElement (element) {
    trimEndingWhitespace(element);
    if (!inVPre && !element.processed) {
      element = processElement(element, options);
    }
    // tree management
    if (!stack.length && element !== root) {
      // allow root elements with v-if, v-else-if and v-else
      if (root.if && (element.elseif || element.else)) {
        {
          checkRootConstraints(element);
        }
        addIfCondition(root, {
          exp: element.elseif,
          block: element
        });
      } else {
        warnOnce(
          "Component template should contain exactly one root element. " +
          "If you are using v-if on multiple elements, " +
          "use v-else-if to chain them instead.",
          { start: element.start }
        );
      }
    }
    if (currentParent && !element.forbidden) {
      if (element.elseif || element.else) {
        processIfConditions(element, currentParent);
      } else {
        if (element.slotScope) {
          // scoped slot
          // keep it in the children list so that v-else(-if) conditions can
          // find it as the prev node.
          var name = element.slotTarget || '"default"'
          ;(currentParent.scopedSlots || (currentParent.scopedSlots = {}))[name] = element;
        }
        currentParent.children.push(element);
        element.parent = currentParent;
      }
    }

    // final children cleanup
    // filter out scoped slots
    element.children = element.children.filter(function (c) { return !(c).slotScope; });
    // remove trailing whitespace node again
    trimEndingWhitespace(element);

    // check pre state
    if (element.pre) {
      inVPre = false;
    }
    if (platformIsPreTag(element.tag)) {
      inPre = false;
    }
    // apply post-transforms
    for (var i = 0; i < postTransforms.length; i++) {
      postTransforms[i](element, options);
    }
  }

  function trimEndingWhitespace (el) {
    // remove trailing whitespace node
    if (!inPre) {
      var lastNode;
      while (
        (lastNode = el.children[el.children.length - 1]) &&
        lastNode.type === 3 &&
        lastNode.text === ' '
      ) {
        el.children.pop();
      }
    }
  }

  function checkRootConstraints (el) {
    if (el.tag === 'slot' || el.tag === 'template') {
      warnOnce(
        "Cannot use <" + (el.tag) + "> as component root element because it may " +
        'contain multiple nodes.',
        { start: el.start }
      );
    }
    if (el.attrsMap.hasOwnProperty('v-for')) {
      warnOnce(
        'Cannot use v-for on stateful component root element because ' +
        'it renders multiple elements.',
        el.rawAttrsMap['v-for']
      );
    }
  }

  parseHTML(template, {
    warn: warn$2,
    expectHTML: options.expectHTML,
    isUnaryTag: options.isUnaryTag,
    canBeLeftOpenTag: options.canBeLeftOpenTag,
    shouldDecodeNewlines: options.shouldDecodeNewlines,
    shouldDecodeNewlinesForHref: options.shouldDecodeNewlinesForHref,
    shouldKeepComment: options.comments,
    outputSourceRange: options.outputSourceRange,
    start: function start (tag, attrs, unary, start$1, end) {
      // check namespace.
      // inherit parent ns if there is one
      var ns = (currentParent && currentParent.ns) || platformGetTagNamespace(tag);

      // handle IE svg bug
      /* istanbul ignore if */
      if (isIE && ns === 'svg') {
        attrs = guardIESVGBug(attrs);
      }

      var element = createASTElement(tag, attrs, currentParent);
      if (ns) {
        element.ns = ns;
      }

      {
        if (options.outputSourceRange) {
          element.start = start$1;
          element.end = end;
          element.rawAttrsMap = element.attrsList.reduce(function (cumulated, attr) {
            cumulated[attr.name] = attr;
            return cumulated
          }, {});
        }
        attrs.forEach(function (attr) {
          if (invalidAttributeRE.test(attr.name)) {
            warn$2(
              "Invalid dynamic argument expression: attribute names cannot contain " +
              "spaces, quotes, <, >, / or =.",
              {
                start: attr.start + attr.name.indexOf("["),
                end: attr.start + attr.name.length
              }
            );
          }
        });
      }

      if (isForbiddenTag(element) && !isServerRendering()) {
        element.forbidden = true;
        warn$2(
          'Templates should only be responsible for mapping the state to the ' +
          'UI. Avoid placing tags with side-effects in your templates, such as ' +
          "<" + tag + ">" + ', as they will not be parsed.',
          { start: element.start }
        );
      }

      // apply pre-transforms
      for (var i = 0; i < preTransforms.length; i++) {
        element = preTransforms[i](element, options) || element;
      }

      if (!inVPre) {
        processPre(element);
        if (element.pre) {
          inVPre = true;
        }
      }
      if (platformIsPreTag(element.tag)) {
        inPre = true;
      }
      if (inVPre) {
        processRawAttrs(element);
      } else if (!element.processed) {
        // structural directives
        processFor(element);
        processIf(element);
        processOnce(element);
      }

      if (!root) {
        root = element;
        {
          checkRootConstraints(root);
        }
      }

      if (!unary) {
        currentParent = element;
        stack.push(element);
      } else {
        closeElement(element);
      }
    },

    end: function end (tag, start, end$1) {
      var element = stack[stack.length - 1];
      // pop stack
      stack.length -= 1;
      currentParent = stack[stack.length - 1];
      if (options.outputSourceRange) {
        element.end = end$1;
      }
      closeElement(element);
    },

    chars: function chars (text, start, end) {
      if (!currentParent) {
        {
          if (text === template) {
            warnOnce(
              'Component template requires a root element, rather than just text.',
              { start: start }
            );
          } else if ((text = text.trim())) {
            warnOnce(
              ("text \"" + text + "\" outside root element will be ignored."),
              { start: start }
            );
          }
        }
        return
      }
      // IE textarea placeholder bug
      /* istanbul ignore if */
      if (isIE &&
        currentParent.tag === 'textarea' &&
        currentParent.attrsMap.placeholder === text
      ) {
        return
      }
      var children = currentParent.children;
      if (inPre || text.trim()) {
        text = isTextTag(currentParent) ? text : decodeHTMLCached(text);
      } else if (!children.length) {
        // remove the whitespace-only node right after an opening tag
        text = '';
      } else if (whitespaceOption) {
        if (whitespaceOption === 'condense') {
          // in condense mode, remove the whitespace node if it contains
          // line break, otherwise condense to a single space
          text = lineBreakRE.test(text) ? '' : ' ';
        } else {
          text = ' ';
        }
      } else {
        text = preserveWhitespace ? ' ' : '';
      }
      if (text) {
        if (!inPre && whitespaceOption === 'condense') {
          // condense consecutive whitespaces into single space
          text = text.replace(whitespaceRE$1, ' ');
        }
        var res;
        var child;
        if (!inVPre && text !== ' ' && (res = parseText(text, delimiters))) {
          child = {
            type: 2,
            expression: res.expression,
            tokens: res.tokens,
            text: text
          };
        } else if (text !== ' ' || !children.length || children[children.length - 1].text !== ' ') {
          child = {
            type: 3,
            text: text
          };
        }
        if (child) {
          if (options.outputSourceRange) {
            child.start = start;
            child.end = end;
          }
          children.push(child);
        }
      }
    },
    comment: function comment (text, start, end) {
      // adding anyting as a sibling to the root node is forbidden
      // comments should still be allowed, but ignored
      if (currentParent) {
        var child = {
          type: 3,
          text: text,
          isComment: true
        };
        if (options.outputSourceRange) {
          child.start = start;
          child.end = end;
        }
        currentParent.children.push(child);
      }
    }
  });
  return root
}

function processPre (el) {
  if (getAndRemoveAttr(el, 'v-pre') != null) {
    el.pre = true;
  }
}

function processRawAttrs (el) {
  var list = el.attrsList;
  var len = list.length;
  if (len) {
    var attrs = el.attrs = new Array(len);
    for (var i = 0; i < len; i++) {
      attrs[i] = {
        name: list[i].name,
        value: JSON.stringify(list[i].value)
      };
      if (list[i].start != null) {
        attrs[i].start = list[i].start;
        attrs[i].end = list[i].end;
      }
    }
  } else if (!el.pre) {
    // non root node in pre blocks with no attributes
    el.plain = true;
  }
}

function processElement (
  element,
  options
) {
  processKey(element);

  // determine whether this is a plain element after
  // removing structural attributes
  element.plain = (
    !element.key &&
    !element.scopedSlots &&
    !element.attrsList.length
  );

  processRef(element);
  processSlotContent(element);
  processSlotOutlet(element);
  processComponent(element);
  for (var i = 0; i < transforms.length; i++) {
    element = transforms[i](element, options) || element;
  }
  processAttrs(element);
  return element
}

function processKey (el) {
  var exp = getBindingAttr(el, 'key');
  if (exp) {
    {
      if (el.tag === 'template') {
        warn$2(
          "<template> cannot be keyed. Place the key on real elements instead.",
          getRawBindingAttr(el, 'key')
        );
      }
      if (el.for) {
        var iterator = el.iterator2 || el.iterator1;
        var parent = el.parent;
        if (iterator && iterator === exp && parent && parent.tag === 'transition-group') {
          warn$2(
            "Do not use v-for index as key on <transition-group> children, " +
            "this is the same as not using keys.",
            getRawBindingAttr(el, 'key'),
            true /* tip */
          );
        }
      }
    }
    el.key = exp;
  }
}

function processRef (el) {
  var ref = getBindingAttr(el, 'ref');
  if (ref) {
    el.ref = ref;
    el.refInFor = checkInFor(el);
  }
}

function processFor (el) {
  var exp;
  if ((exp = getAndRemoveAttr(el, 'v-for'))) {
    var res = parseFor(exp);
    if (res) {
      extend(el, res);
    } else {
      warn$2(
        ("Invalid v-for expression: " + exp),
        el.rawAttrsMap['v-for']
      );
    }
  }
}



function parseFor (exp) {
  var inMatch = exp.match(forAliasRE);
  if (!inMatch) { return }
  var res = {};
  res.for = inMatch[2].trim();
  var alias = inMatch[1].trim().replace(stripParensRE, '');
  var iteratorMatch = alias.match(forIteratorRE);
  if (iteratorMatch) {
    res.alias = alias.replace(forIteratorRE, '').trim();
    res.iterator1 = iteratorMatch[1].trim();
    if (iteratorMatch[2]) {
      res.iterator2 = iteratorMatch[2].trim();
    }
  } else {
    res.alias = alias;
  }
  return res
}

function processIf (el) {
  var exp = getAndRemoveAttr(el, 'v-if');
  if (exp) {
    el.if = exp;
    addIfCondition(el, {
      exp: exp,
      block: el
    });
  } else {
    if (getAndRemoveAttr(el, 'v-else') != null) {
      el.else = true;
    }
    var elseif = getAndRemoveAttr(el, 'v-else-if');
    if (elseif) {
      el.elseif = elseif;
    }
  }
}

function processIfConditions (el, parent) {
  var prev = findPrevElement(parent.children);
  if (prev && prev.if) {
    addIfCondition(prev, {
      exp: el.elseif,
      block: el
    });
  } else {
    warn$2(
      "v-" + (el.elseif ? ('else-if="' + el.elseif + '"') : 'else') + " " +
      "used on element <" + (el.tag) + "> without corresponding v-if.",
      el.rawAttrsMap[el.elseif ? 'v-else-if' : 'v-else']
    );
  }
}

function findPrevElement (children) {
  var i = children.length;
  while (i--) {
    if (children[i].type === 1) {
      return children[i]
    } else {
      if (children[i].text !== ' ') {
        warn$2(
          "text \"" + (children[i].text.trim()) + "\" between v-if and v-else(-if) " +
          "will be ignored.",
          children[i]
        );
      }
      children.pop();
    }
  }
}

function addIfCondition (el, condition) {
  if (!el.ifConditions) {
    el.ifConditions = [];
  }
  el.ifConditions.push(condition);
}

function processOnce (el) {
  var once$$1 = getAndRemoveAttr(el, 'v-once');
  if (once$$1 != null) {
    el.once = true;
  }
}

// handle content being passed to a component as slot,
// e.g. <template slot="xxx">, <div slot-scope="xxx">
function processSlotContent (el) {
  var slotScope;
  if (el.tag === 'template') {
    slotScope = getAndRemoveAttr(el, 'scope');
    /* istanbul ignore if */
    if (slotScope) {
      warn$2(
        "the \"scope\" attribute for scoped slots have been deprecated and " +
        "replaced by \"slot-scope\" since 2.5. The new \"slot-scope\" attribute " +
        "can also be used on plain elements in addition to <template> to " +
        "denote scoped slots.",
        el.rawAttrsMap['scope'],
        true
      );
    }
    el.slotScope = slotScope || getAndRemoveAttr(el, 'slot-scope');
  } else if ((slotScope = getAndRemoveAttr(el, 'slot-scope'))) {
    /* istanbul ignore if */
    if (el.attrsMap['v-for']) {
      warn$2(
        "Ambiguous combined usage of slot-scope and v-for on <" + (el.tag) + "> " +
        "(v-for takes higher priority). Use a wrapper <template> for the " +
        "scoped slot to make it clearer.",
        el.rawAttrsMap['slot-scope'],
        true
      );
    }
    el.slotScope = slotScope;
  }

  // slot="xxx"
  var slotTarget = getBindingAttr(el, 'slot');
  if (slotTarget) {
    el.slotTarget = slotTarget === '""' ? '"default"' : slotTarget;
    el.slotTargetDynamic = !!(el.attrsMap[':slot'] || el.attrsMap['v-bind:slot']);
    // preserve slot as an attribute for native shadow DOM compat
    // only for non-scoped slots.
    if (el.tag !== 'template' && !el.slotScope) {
      addAttr(el, 'slot', slotTarget, getRawBindingAttr(el, 'slot'));
    }
  }

  // 2.6 v-slot syntax
  {
    if (el.tag === 'template') {
      // v-slot on <template>
      var slotBinding = getAndRemoveAttrByRegex(el, slotRE);
      if (slotBinding) {
        {
          if (el.slotTarget || el.slotScope) {
            warn$2(
              "Unexpected mixed usage of different slot syntaxes.",
              el
            );
          }
          if (el.parent && !maybeComponent(el.parent)) {
            warn$2(
              "<template v-slot> can only appear at the root level inside " +
              "the receiving the component",
              el
            );
          }
        }
        var ref = getSlotName(slotBinding);
        var name = ref.name;
        var dynamic = ref.dynamic;
        el.slotTarget = name;
        el.slotTargetDynamic = dynamic;
        el.slotScope = slotBinding.value || emptySlotScopeToken; // force it into a scoped slot for perf
      }
    } else {
      // v-slot on component, denotes default slot
      var slotBinding$1 = getAndRemoveAttrByRegex(el, slotRE);
      if (slotBinding$1) {
        {
          if (!maybeComponent(el)) {
            warn$2(
              "v-slot can only be used on components or <template>.",
              slotBinding$1
            );
          }
          if (el.slotScope || el.slotTarget) {
            warn$2(
              "Unexpected mixed usage of different slot syntaxes.",
              el
            );
          }
          if (el.scopedSlots) {
            warn$2(
              "To avoid scope ambiguity, the default slot should also use " +
              "<template> syntax when there are other named slots.",
              slotBinding$1
            );
          }
        }
        // add the component's children to its default slot
        var slots = el.scopedSlots || (el.scopedSlots = {});
        var ref$1 = getSlotName(slotBinding$1);
        var name$1 = ref$1.name;
        var dynamic$1 = ref$1.dynamic;
        var slotContainer = slots[name$1] = createASTElement('template', [], el);
        slotContainer.slotTarget = name$1;
        slotContainer.slotTargetDynamic = dynamic$1;
        slotContainer.children = el.children.filter(function (c) {
          if (!c.slotScope) {
            c.parent = slotContainer;
            return true
          }
        });
        slotContainer.slotScope = slotBinding$1.value || emptySlotScopeToken;
        // remove children as they are returned from scopedSlots now
        el.children = [];
        // mark el non-plain so data gets generated
        el.plain = false;
      }
    }
  }
}

function getSlotName (binding) {
  var name = binding.name.replace(slotRE, '');
  if (!name) {
    if (binding.name[0] !== '#') {
      name = 'default';
    } else {
      warn$2(
        "v-slot shorthand syntax requires a slot name.",
        binding
      );
    }
  }
  return dynamicArgRE.test(name)
    // dynamic [name]
    ? { name: name.slice(1, -1), dynamic: true }
    // static name
    : { name: ("\"" + name + "\""), dynamic: false }
}

// handle <slot/> outlets
function processSlotOutlet (el) {
  if (el.tag === 'slot') {
    el.slotName = getBindingAttr(el, 'name');
    if (el.key) {
      warn$2(
        "`key` does not work on <slot> because slots are abstract outlets " +
        "and can possibly expand into multiple elements. " +
        "Use the key on a wrapping element instead.",
        getRawBindingAttr(el, 'key')
      );
    }
  }
}

function processComponent (el) {
  var binding;
  if ((binding = getBindingAttr(el, 'is'))) {
    el.component = binding;
  }
  if (getAndRemoveAttr(el, 'inline-template') != null) {
    el.inlineTemplate = true;
  }
}

function processAttrs (el) {
  var list = el.attrsList;
  var i, l, name, rawName, value, modifiers, syncGen, isDynamic;
  for (i = 0, l = list.length; i < l; i++) {
    name = rawName = list[i].name;
    value = list[i].value;
    if (dirRE.test(name)) {
      // mark element as dynamic
      el.hasBindings = true;
      // modifiers
      modifiers = parseModifiers(name.replace(dirRE, ''));
      // support .foo shorthand syntax for the .prop modifier
      if (modifiers) {
        name = name.replace(modifierRE, '');
      }
      if (bindRE.test(name)) { // v-bind
        name = name.replace(bindRE, '');
        value = parseFilters(value);
        isDynamic = dynamicArgRE.test(name);
        if (isDynamic) {
          name = name.slice(1, -1);
        }
        if (
          value.trim().length === 0
        ) {
          warn$2(
            ("The value for a v-bind expression cannot be empty. Found in \"v-bind:" + name + "\"")
          );
        }
        if (modifiers) {
          if (modifiers.prop && !isDynamic) {
            name = camelize(name);
            if (name === 'innerHtml') { name = 'innerHTML'; }
          }
          if (modifiers.camel && !isDynamic) {
            name = camelize(name);
          }
          if (modifiers.sync) {
            syncGen = genAssignmentCode(value, "$event");
            if (!isDynamic) {
              addHandler(
                el,
                ("update:" + (camelize(name))),
                syncGen,
                null,
                false,
                warn$2,
                list[i]
              );
              if (hyphenate(name) !== camelize(name)) {
                addHandler(
                  el,
                  ("update:" + (hyphenate(name))),
                  syncGen,
                  null,
                  false,
                  warn$2,
                  list[i]
                );
              }
            } else {
              // handler w/ dynamic event name
              addHandler(
                el,
                ("\"update:\"+(" + name + ")"),
                syncGen,
                null,
                false,
                warn$2,
                list[i],
                true // dynamic
              );
            }
          }
        }
        if ((modifiers && modifiers.prop) || (
          !el.component && platformMustUseProp(el.tag, el.attrsMap.type, name)
        )) {
          addProp(el, name, value, list[i], isDynamic);
        } else {
          addAttr(el, name, value, list[i], isDynamic);
        }
      } else if (onRE.test(name)) { // v-on
        name = name.replace(onRE, '');
        isDynamic = dynamicArgRE.test(name);
        if (isDynamic) {
          name = name.slice(1, -1);
        }
        addHandler(el, name, value, modifiers, false, warn$2, list[i], isDynamic);
      } else { // normal directives
        name = name.replace(dirRE, '');
        // parse arg
        var argMatch = name.match(argRE);
        var arg = argMatch && argMatch[1];
        isDynamic = false;
        if (arg) {
          name = name.slice(0, -(arg.length + 1));
          if (dynamicArgRE.test(arg)) {
            arg = arg.slice(1, -1);
            isDynamic = true;
          }
        }
        addDirective(el, name, rawName, value, arg, isDynamic, modifiers, list[i]);
        if (name === 'model') {
          checkForAliasModel(el, value);
        }
      }
    } else {
      // literal attribute
      {
        var res = parseText(value, delimiters);
        if (res) {
          warn$2(
            name + "=\"" + value + "\": " +
            'Interpolation inside attributes has been removed. ' +
            'Use v-bind or the colon shorthand instead. For example, ' +
            'instead of <div id="{{ val }}">, use <div :id="val">.',
            list[i]
          );
        }
      }
      addAttr(el, name, JSON.stringify(value), list[i]);
      // #6887 firefox doesn't update muted state if set via attribute
      // even immediately after element creation
      if (!el.component &&
          name === 'muted' &&
          platformMustUseProp(el.tag, el.attrsMap.type, name)) {
        addProp(el, name, 'true', list[i]);
      }
    }
  }
}

function checkInFor (el) {
  var parent = el;
  while (parent) {
    if (parent.for !== undefined) {
      return true
    }
    parent = parent.parent;
  }
  return false
}

function parseModifiers (name) {
  var match = name.match(modifierRE);
  if (match) {
    var ret = {};
    match.forEach(function (m) { ret[m.slice(1)] = true; });
    return ret
  }
}

function makeAttrsMap (attrs) {
  var map = {};
  for (var i = 0, l = attrs.length; i < l; i++) {
    if (
      map[attrs[i].name] && !isIE && !isEdge
    ) {
      warn$2('duplicate attribute: ' + attrs[i].name, attrs[i]);
    }
    map[attrs[i].name] = attrs[i].value;
  }
  return map
}

// for script (e.g. type="x/template") or style, do not decode content
function isTextTag (el) {
  return el.tag === 'script' || el.tag === 'style'
}

function isForbiddenTag (el) {
  return (
    el.tag === 'style' ||
    (el.tag === 'script' && (
      !el.attrsMap.type ||
      el.attrsMap.type === 'text/javascript'
    ))
  )
}

var ieNSBug = /^xmlns:NS\d+/;
var ieNSPrefix = /^NS\d+:/;

/* istanbul ignore next */
function guardIESVGBug (attrs) {
  var res = [];
  for (var i = 0; i < attrs.length; i++) {
    var attr = attrs[i];
    if (!ieNSBug.test(attr.name)) {
      attr.name = attr.name.replace(ieNSPrefix, '');
      res.push(attr);
    }
  }
  return res
}

function checkForAliasModel (el, value) {
  var _el = el;
  while (_el) {
    if (_el.for && _el.alias === value) {
      warn$2(
        "<" + (el.tag) + " v-model=\"" + value + "\">: " +
        "You are binding v-model directly to a v-for iteration alias. " +
        "This will not be able to modify the v-for source array because " +
        "writing to the alias is like modifying a function local variable. " +
        "Consider using an array of objects and use v-model on an object property instead.",
        el.rawAttrsMap['v-model']
      );
    }
    _el = _el.parent;
  }
}

/*  */

function preTransformNode (el, options) {
  if (el.tag === 'input') {
    var map = el.attrsMap;
    if (!map['v-model']) {
      return
    }

    var typeBinding;
    if (map[':type'] || map['v-bind:type']) {
      typeBinding = getBindingAttr(el, 'type');
    }
    if (!map.type && !typeBinding && map['v-bind']) {
      typeBinding = "(" + (map['v-bind']) + ").type";
    }

    if (typeBinding) {
      var ifCondition = getAndRemoveAttr(el, 'v-if', true);
      var ifConditionExtra = ifCondition ? ("&&(" + ifCondition + ")") : "";
      var hasElse = getAndRemoveAttr(el, 'v-else', true) != null;
      var elseIfCondition = getAndRemoveAttr(el, 'v-else-if', true);
      // 1. checkbox
      var branch0 = cloneASTElement(el);
      // process for on the main node
      processFor(branch0);
      addRawAttr(branch0, 'type', 'checkbox');
      processElement(branch0, options);
      branch0.processed = true; // prevent it from double-processed
      branch0.if = "(" + typeBinding + ")==='checkbox'" + ifConditionExtra;
      addIfCondition(branch0, {
        exp: branch0.if,
        block: branch0
      });
      // 2. add radio else-if condition
      var branch1 = cloneASTElement(el);
      getAndRemoveAttr(branch1, 'v-for', true);
      addRawAttr(branch1, 'type', 'radio');
      processElement(branch1, options);
      addIfCondition(branch0, {
        exp: "(" + typeBinding + ")==='radio'" + ifConditionExtra,
        block: branch1
      });
      // 3. other
      var branch2 = cloneASTElement(el);
      getAndRemoveAttr(branch2, 'v-for', true);
      addRawAttr(branch2, ':type', typeBinding);
      processElement(branch2, options);
      addIfCondition(branch0, {
        exp: ifCondition,
        block: branch2
      });

      if (hasElse) {
        branch0.else = true;
      } else if (elseIfCondition) {
        branch0.elseif = elseIfCondition;
      }

      return branch0
    }
  }
}

function cloneASTElement (el) {
  return createASTElement(el.tag, el.attrsList.slice(), el.parent)
}

var model$1 = {
  preTransformNode: preTransformNode
};

var modules$1 = [
  klass$1,
  style$1,
  model$1
];

/*  */

function text (el, dir) {
  if (dir.value) {
    addProp(el, 'textContent', ("_s(" + (dir.value) + ")"), dir);
  }
}

/*  */

function html (el, dir) {
  if (dir.value) {
    addProp(el, 'innerHTML', ("_s(" + (dir.value) + ")"), dir);
  }
}

var directives$1 = {
  model: model,
  text: text,
  html: html
};

/*  */

var baseOptions = {
  expectHTML: true,
  modules: modules$1,
  directives: directives$1,
  isPreTag: isPreTag,
  isUnaryTag: isUnaryTag,
  mustUseProp: mustUseProp,
  canBeLeftOpenTag: canBeLeftOpenTag,
  isReservedTag: isReservedTag,
  getTagNamespace: getTagNamespace,
  staticKeys: genStaticKeys(modules$1)
};

/*  */

var isStaticKey;
var isPlatformReservedTag;

var genStaticKeysCached = cached(genStaticKeys$1);

/**
 * Goal of the optimizer: walk the generated template AST tree
 * and detect sub-trees that are purely static, i.e. parts of
 * the DOM that never needs to change.
 *
 * Once we detect these sub-trees, we can:
 *
 * 1. Hoist them into constants, so that we no longer need to
 *    create fresh nodes for them on each re-render;
 * 2. Completely skip them in the patching process.
 */
function optimize (root, options) {
  if (!root) { return }
  isStaticKey = genStaticKeysCached(options.staticKeys || '');
  isPlatformReservedTag = options.isReservedTag || no;
  // first pass: mark all non-static nodes.
  markStatic$1(root);
  // second pass: mark static roots.
  markStaticRoots(root, false);
}

function genStaticKeys$1 (keys) {
  return makeMap(
    'type,tag,attrsList,attrsMap,plain,parent,children,attrs,start,end,rawAttrsMap' +
    (keys ? ',' + keys : '')
  )
}

function markStatic$1 (node) {
  node.static = isStatic(node);
  if (node.type === 1) {
    // do not make component slot content static. this avoids
    // 1. components not able to mutate slot nodes
    // 2. static slot content fails for hot-reloading
    if (
      !isPlatformReservedTag(node.tag) &&
      node.tag !== 'slot' &&
      node.attrsMap['inline-template'] == null
    ) {
      return
    }
    for (var i = 0, l = node.children.length; i < l; i++) {
      var child = node.children[i];
      markStatic$1(child);
      if (!child.static) {
        node.static = false;
      }
    }
    if (node.ifConditions) {
      for (var i$1 = 1, l$1 = node.ifConditions.length; i$1 < l$1; i$1++) {
        var block = node.ifConditions[i$1].block;
        markStatic$1(block);
        if (!block.static) {
          node.static = false;
        }
      }
    }
  }
}

function markStaticRoots (node, isInFor) {
  if (node.type === 1) {
    if (node.static || node.once) {
      node.staticInFor = isInFor;
    }
    // For a node to qualify as a static root, it should have children that
    // are not just static text. Otherwise the cost of hoisting out will
    // outweigh the benefits and it's better off to just always render it fresh.
    if (node.static && node.children.length && !(
      node.children.length === 1 &&
      node.children[0].type === 3
    )) {
      node.staticRoot = true;
      return
    } else {
      node.staticRoot = false;
    }
    if (node.children) {
      for (var i = 0, l = node.children.length; i < l; i++) {
        markStaticRoots(node.children[i], isInFor || !!node.for);
      }
    }
    if (node.ifConditions) {
      for (var i$1 = 1, l$1 = node.ifConditions.length; i$1 < l$1; i$1++) {
        markStaticRoots(node.ifConditions[i$1].block, isInFor);
      }
    }
  }
}

function isStatic (node) {
  if (node.type === 2) { // expression
    return false
  }
  if (node.type === 3) { // text
    return true
  }
  return !!(node.pre || (
    !node.hasBindings && // no dynamic bindings
    !node.if && !node.for && // not v-if or v-for or v-else
    !isBuiltInTag(node.tag) && // not a built-in
    isPlatformReservedTag(node.tag) && // not a component
    !isDirectChildOfTemplateFor(node) &&
    Object.keys(node).every(isStaticKey)
  ))
}

function isDirectChildOfTemplateFor (node) {
  while (node.parent) {
    node = node.parent;
    if (node.tag !== 'template') {
      return false
    }
    if (node.for) {
      return true
    }
  }
  return false
}

/*  */

var fnExpRE = /^([\w$_]+|\([^)]*?\))\s*=>|^function\s*(?:[\w$]+)?\s*\(/;
var fnInvokeRE = /\([^)]*?\);*$/;
var simplePathRE = /^[A-Za-z_$][\w$]*(?:\.[A-Za-z_$][\w$]*|\['[^']*?']|\["[^"]*?"]|\[\d+]|\[[A-Za-z_$][\w$]*])*$/;

// KeyboardEvent.keyCode aliases
var keyCodes = {
  esc: 27,
  tab: 9,
  enter: 13,
  space: 32,
  up: 38,
  left: 37,
  right: 39,
  down: 40,
  'delete': [8, 46]
};

// KeyboardEvent.key aliases
var keyNames = {
  // #7880: IE11 and Edge use `Esc` for Escape key name.
  esc: ['Esc', 'Escape'],
  tab: 'Tab',
  enter: 'Enter',
  // #9112: IE11 uses `Spacebar` for Space key name.
  space: [' ', 'Spacebar'],
  // #7806: IE11 uses key names without `Arrow` prefix for arrow keys.
  up: ['Up', 'ArrowUp'],
  left: ['Left', 'ArrowLeft'],
  right: ['Right', 'ArrowRight'],
  down: ['Down', 'ArrowDown'],
  // #9112: IE11 uses `Del` for Delete key name.
  'delete': ['Backspace', 'Delete', 'Del']
};

// #4868: modifiers that prevent the execution of the listener
// need to explicitly return null so that we can determine whether to remove
// the listener for .once
var genGuard = function (condition) { return ("if(" + condition + ")return null;"); };

var modifierCode = {
  stop: '$event.stopPropagation();',
  prevent: '$event.preventDefault();',
  self: genGuard("$event.target !== $event.currentTarget"),
  ctrl: genGuard("!$event.ctrlKey"),
  shift: genGuard("!$event.shiftKey"),
  alt: genGuard("!$event.altKey"),
  meta: genGuard("!$event.metaKey"),
  left: genGuard("'button' in $event && $event.button !== 0"),
  middle: genGuard("'button' in $event && $event.button !== 1"),
  right: genGuard("'button' in $event && $event.button !== 2")
};

function genHandlers (
  events,
  isNative
) {
  var prefix = isNative ? 'nativeOn:' : 'on:';
  var staticHandlers = "";
  var dynamicHandlers = "";
  for (var name in events) {
    var handlerCode = genHandler(events[name]);
    if (events[name] && events[name].dynamic) {
      dynamicHandlers += name + "," + handlerCode + ",";
    } else {
      staticHandlers += "\"" + name + "\":" + handlerCode + ",";
    }
  }
  staticHandlers = "{" + (staticHandlers.slice(0, -1)) + "}";
  if (dynamicHandlers) {
    return prefix + "_d(" + staticHandlers + ",[" + (dynamicHandlers.slice(0, -1)) + "])"
  } else {
    return prefix + staticHandlers
  }
}

function genHandler (handler) {
  if (!handler) {
    return 'function(){}'
  }

  if (Array.isArray(handler)) {
    return ("[" + (handler.map(function (handler) { return genHandler(handler); }).join(',')) + "]")
  }

  var isMethodPath = simplePathRE.test(handler.value);
  var isFunctionExpression = fnExpRE.test(handler.value);
  var isFunctionInvocation = simplePathRE.test(handler.value.replace(fnInvokeRE, ''));

  if (!handler.modifiers) {
    if (isMethodPath || isFunctionExpression) {
      return handler.value
    }
    return ("function($event){" + (isFunctionInvocation ? ("return " + (handler.value)) : handler.value) + "}") // inline statement
  } else {
    var code = '';
    var genModifierCode = '';
    var keys = [];
    for (var key in handler.modifiers) {
      if (modifierCode[key]) {
        genModifierCode += modifierCode[key];
        // left/right
        if (keyCodes[key]) {
          keys.push(key);
        }
      } else if (key === 'exact') {
        var modifiers = (handler.modifiers);
        genModifierCode += genGuard(
          ['ctrl', 'shift', 'alt', 'meta']
            .filter(function (keyModifier) { return !modifiers[keyModifier]; })
            .map(function (keyModifier) { return ("$event." + keyModifier + "Key"); })
            .join('||')
        );
      } else {
        keys.push(key);
      }
    }
    if (keys.length) {
      code += genKeyFilter(keys);
    }
    // Make sure modifiers like prevent and stop get executed after key filtering
    if (genModifierCode) {
      code += genModifierCode;
    }
    var handlerCode = isMethodPath
      ? ("return " + (handler.value) + "($event)")
      : isFunctionExpression
        ? ("return (" + (handler.value) + ")($event)")
        : isFunctionInvocation
          ? ("return " + (handler.value))
          : handler.value;
    return ("function($event){" + code + handlerCode + "}")
  }
}

function genKeyFilter (keys) {
  return (
    // make sure the key filters only apply to KeyboardEvents
    // #9441: can't use 'keyCode' in $event because Chrome autofill fires fake
    // key events that do not have keyCode property...
    "if(!$event.type.indexOf('key')&&" +
    (keys.map(genFilterCode).join('&&')) + ")return null;"
  )
}

function genFilterCode (key) {
  var keyVal = parseInt(key, 10);
  if (keyVal) {
    return ("$event.keyCode!==" + keyVal)
  }
  var keyCode = keyCodes[key];
  var keyName = keyNames[key];
  return (
    "_k($event.keyCode," +
    (JSON.stringify(key)) + "," +
    (JSON.stringify(keyCode)) + "," +
    "$event.key," +
    "" + (JSON.stringify(keyName)) +
    ")"
  )
}

/*  */

function on (el, dir) {
  if (dir.modifiers) {
    warn("v-on without argument does not support modifiers.");
  }
  el.wrapListeners = function (code) { return ("_g(" + code + "," + (dir.value) + ")"); };
}

/*  */

function bind$1 (el, dir) {
  el.wrapData = function (code) {
    return ("_b(" + code + ",'" + (el.tag) + "'," + (dir.value) + "," + (dir.modifiers && dir.modifiers.prop ? 'true' : 'false') + (dir.modifiers && dir.modifiers.sync ? ',true' : '') + ")")
  };
}

/*  */

var baseDirectives = {
  on: on,
  bind: bind$1,
  cloak: noop
};

/*  */





var CodegenState = function CodegenState (options) {
  this.options = options;
  this.warn = options.warn || baseWarn;
  this.transforms = pluckModuleFunction(options.modules, 'transformCode');
  this.dataGenFns = pluckModuleFunction(options.modules, 'genData');
  this.directives = extend(extend({}, baseDirectives), options.directives);
  var isReservedTag = options.isReservedTag || no;
  this.maybeComponent = function (el) { return !!el.component || !isReservedTag(el.tag); };
  this.onceId = 0;
  this.staticRenderFns = [];
  this.pre = false;
};



function generate (
  ast,
  options
) {
  var state = new CodegenState(options);
  var code = ast ? genElement(ast, state) : '_c("div")';
  return {
    render: ("with(this){return " + code + "}"),
    staticRenderFns: state.staticRenderFns
  }
}

function genElement (el, state) {
  if (el.parent) {
    el.pre = el.pre || el.parent.pre;
  }

  if (el.staticRoot && !el.staticProcessed) {
    return genStatic(el, state)
  } else if (el.once && !el.onceProcessed) {
    return genOnce(el, state)
  } else if (el.for && !el.forProcessed) {
    return genFor(el, state)
  } else if (el.if && !el.ifProcessed) {
    return genIf(el, state)
  } else if (el.tag === 'template' && !el.slotTarget && !state.pre) {
    return genChildren(el, state) || 'void 0'
  } else if (el.tag === 'slot') {
    return genSlot(el, state)
  } else {
    // component or element
    var code;
    if (el.component) {
      code = genComponent(el.component, el, state);
    } else {
      var data;
      if (!el.plain || (el.pre && state.maybeComponent(el))) {
        data = genData$2(el, state);
      }

      var children = el.inlineTemplate ? null : genChildren(el, state, true);
      code = "_c('" + (el.tag) + "'" + (data ? ("," + data) : '') + (children ? ("," + children) : '') + ")";
    }
    // module transforms
    for (var i = 0; i < state.transforms.length; i++) {
      code = state.transforms[i](el, code);
    }
    return code
  }
}

// hoist static sub-trees out
function genStatic (el, state) {
  el.staticProcessed = true;
  // Some elements (templates) need to behave differently inside of a v-pre
  // node.  All pre nodes are static roots, so we can use this as a location to
  // wrap a state change and reset it upon exiting the pre node.
  var originalPreState = state.pre;
  if (el.pre) {
    state.pre = el.pre;
  }
  state.staticRenderFns.push(("with(this){return " + (genElement(el, state)) + "}"));
  state.pre = originalPreState;
  return ("_m(" + (state.staticRenderFns.length - 1) + (el.staticInFor ? ',true' : '') + ")")
}

// v-once
function genOnce (el, state) {
  el.onceProcessed = true;
  if (el.if && !el.ifProcessed) {
    return genIf(el, state)
  } else if (el.staticInFor) {
    var key = '';
    var parent = el.parent;
    while (parent) {
      if (parent.for) {
        key = parent.key;
        break
      }
      parent = parent.parent;
    }
    if (!key) {
      state.warn(
        "v-once can only be used inside v-for that is keyed. ",
        el.rawAttrsMap['v-once']
      );
      return genElement(el, state)
    }
    return ("_o(" + (genElement(el, state)) + "," + (state.onceId++) + "," + key + ")")
  } else {
    return genStatic(el, state)
  }
}

function genIf (
  el,
  state,
  altGen,
  altEmpty
) {
  el.ifProcessed = true; // avoid recursion
  return genIfConditions(el.ifConditions.slice(), state, altGen, altEmpty)
}

function genIfConditions (
  conditions,
  state,
  altGen,
  altEmpty
) {
  if (!conditions.length) {
    return altEmpty || '_e()'
  }

  var condition = conditions.shift();
  if (condition.exp) {
    return ("(" + (condition.exp) + ")?" + (genTernaryExp(condition.block)) + ":" + (genIfConditions(conditions, state, altGen, altEmpty)))
  } else {
    return ("" + (genTernaryExp(condition.block)))
  }

  // v-if with v-once should generate code like (a)?_m(0):_m(1)
  function genTernaryExp (el) {
    return altGen
      ? altGen(el, state)
      : el.once
        ? genOnce(el, state)
        : genElement(el, state)
  }
}

function genFor (
  el,
  state,
  altGen,
  altHelper
) {
  var exp = el.for;
  var alias = el.alias;
  var iterator1 = el.iterator1 ? ("," + (el.iterator1)) : '';
  var iterator2 = el.iterator2 ? ("," + (el.iterator2)) : '';

  if (state.maybeComponent(el) &&
    el.tag !== 'slot' &&
    el.tag !== 'template' &&
    !el.key
  ) {
    state.warn(
      "<" + (el.tag) + " v-for=\"" + alias + " in " + exp + "\">: component lists rendered with " +
      "v-for should have explicit keys. " +
      "See https://vuejs.org/guide/list.html#key for more info.",
      el.rawAttrsMap['v-for'],
      true /* tip */
    );
  }

  el.forProcessed = true; // avoid recursion
  return (altHelper || '_l') + "((" + exp + ")," +
    "function(" + alias + iterator1 + iterator2 + "){" +
      "return " + ((altGen || genElement)(el, state)) +
    '})'
}

function genData$2 (el, state) {
  var data = '{';

  // directives first.
  // directives may mutate the el's other properties before they are generated.
  var dirs = genDirectives(el, state);
  if (dirs) { data += dirs + ','; }

  // key
  if (el.key) {
    data += "key:" + (el.key) + ",";
  }
  // ref
  if (el.ref) {
    data += "ref:" + (el.ref) + ",";
  }
  if (el.refInFor) {
    data += "refInFor:true,";
  }
  // pre
  if (el.pre) {
    data += "pre:true,";
  }
  // record original tag name for components using "is" attribute
  if (el.component) {
    data += "tag:\"" + (el.tag) + "\",";
  }
  // module data generation functions
  for (var i = 0; i < state.dataGenFns.length; i++) {
    data += state.dataGenFns[i](el);
  }
  // attributes
  if (el.attrs) {
    data += "attrs:" + (genProps(el.attrs)) + ",";
  }
  // DOM props
  if (el.props) {
    data += "domProps:" + (genProps(el.props)) + ",";
  }
  // event handlers
  if (el.events) {
    data += (genHandlers(el.events, false)) + ",";
  }
  if (el.nativeEvents) {
    data += (genHandlers(el.nativeEvents, true)) + ",";
  }
  // slot target
  // only for non-scoped slots
  if (el.slotTarget && !el.slotScope) {
    data += "slot:" + (el.slotTarget) + ",";
  }
  // scoped slots
  if (el.scopedSlots) {
    data += (genScopedSlots(el, el.scopedSlots, state)) + ",";
  }
  // component v-model
  if (el.model) {
    data += "model:{value:" + (el.model.value) + ",callback:" + (el.model.callback) + ",expression:" + (el.model.expression) + "},";
  }
  // inline-template
  if (el.inlineTemplate) {
    var inlineTemplate = genInlineTemplate(el, state);
    if (inlineTemplate) {
      data += inlineTemplate + ",";
    }
  }
  data = data.replace(/,$/, '') + '}';
  // v-bind dynamic argument wrap
  // v-bind with dynamic arguments must be applied using the same v-bind object
  // merge helper so that class/style/mustUseProp attrs are handled correctly.
  if (el.dynamicAttrs) {
    data = "_b(" + data + ",\"" + (el.tag) + "\"," + (genProps(el.dynamicAttrs)) + ")";
  }
  // v-bind data wrap
  if (el.wrapData) {
    data = el.wrapData(data);
  }
  // v-on data wrap
  if (el.wrapListeners) {
    data = el.wrapListeners(data);
  }
  return data
}

function genDirectives (el, state) {
  var dirs = el.directives;
  if (!dirs) { return }
  var res = 'directives:[';
  var hasRuntime = false;
  var i, l, dir, needRuntime;
  for (i = 0, l = dirs.length; i < l; i++) {
    dir = dirs[i];
    needRuntime = true;
    var gen = state.directives[dir.name];
    if (gen) {
      // compile-time directive that manipulates AST.
      // returns true if it also needs a runtime counterpart.
      needRuntime = !!gen(el, dir, state.warn);
    }
    if (needRuntime) {
      hasRuntime = true;
      res += "{name:\"" + (dir.name) + "\",rawName:\"" + (dir.rawName) + "\"" + (dir.value ? (",value:(" + (dir.value) + "),expression:" + (JSON.stringify(dir.value))) : '') + (dir.arg ? (",arg:" + (dir.isDynamicArg ? dir.arg : ("\"" + (dir.arg) + "\""))) : '') + (dir.modifiers ? (",modifiers:" + (JSON.stringify(dir.modifiers))) : '') + "},";
    }
  }
  if (hasRuntime) {
    return res.slice(0, -1) + ']'
  }
}

function genInlineTemplate (el, state) {
  var ast = el.children[0];
  if (el.children.length !== 1 || ast.type !== 1) {
    state.warn(
      'Inline-template components must have exactly one child element.',
      { start: el.start }
    );
  }
  if (ast && ast.type === 1) {
    var inlineRenderFns = generate(ast, state.options);
    return ("inlineTemplate:{render:function(){" + (inlineRenderFns.render) + "},staticRenderFns:[" + (inlineRenderFns.staticRenderFns.map(function (code) { return ("function(){" + code + "}"); }).join(',')) + "]}")
  }
}

function genScopedSlots (
  el,
  slots,
  state
) {
  // by default scoped slots are considered "stable", this allows child
  // components with only scoped slots to skip forced updates from parent.
  // but in some cases we have to bail-out of this optimization
  // for example if the slot contains dynamic names, has v-if or v-for on them...
  var needsForceUpdate = el.for || Object.keys(slots).some(function (key) {
    var slot = slots[key];
    return (
      slot.slotTargetDynamic ||
      slot.if ||
      slot.for ||
      containsSlotChild(slot) // is passing down slot from parent which may be dynamic
    )
  });

  // #9534: if a component with scoped slots is inside a conditional branch,
  // it's possible for the same component to be reused but with different
  // compiled slot content. To avoid that, we generate a unique key based on
  // the generated code of all the slot contents.
  var needsKey = !!el.if;

  // OR when it is inside another scoped slot or v-for (the reactivity may be
  // disconnected due to the intermediate scope variable)
  // #9438, #9506
  // TODO: this can be further optimized by properly analyzing in-scope bindings
  // and skip force updating ones that do not actually use scope variables.
  if (!needsForceUpdate) {
    var parent = el.parent;
    while (parent) {
      if (
        (parent.slotScope && parent.slotScope !== emptySlotScopeToken) ||
        parent.for
      ) {
        needsForceUpdate = true;
        break
      }
      if (parent.if) {
        needsKey = true;
      }
      parent = parent.parent;
    }
  }

  var generatedSlots = Object.keys(slots)
    .map(function (key) { return genScopedSlot(slots[key], state); })
    .join(',');

  return ("scopedSlots:_u([" + generatedSlots + "]" + (needsForceUpdate ? ",null,true" : "") + (!needsForceUpdate && needsKey ? (",null,false," + (hash(generatedSlots))) : "") + ")")
}

function hash(str) {
  var hash = 5381;
  var i = str.length;
  while(i) {
    hash = (hash * 33) ^ str.charCodeAt(--i);
  }
  return hash >>> 0
}

function containsSlotChild (el) {
  if (el.type === 1) {
    if (el.tag === 'slot') {
      return true
    }
    return el.children.some(containsSlotChild)
  }
  return false
}

function genScopedSlot (
  el,
  state
) {
  var isLegacySyntax = el.attrsMap['slot-scope'];
  if (el.if && !el.ifProcessed && !isLegacySyntax) {
    return genIf(el, state, genScopedSlot, "null")
  }
  if (el.for && !el.forProcessed) {
    return genFor(el, state, genScopedSlot)
  }
  var slotScope = el.slotScope === emptySlotScopeToken
    ? ""
    : String(el.slotScope);
  var fn = "function(" + slotScope + "){" +
    "return " + (el.tag === 'template'
      ? el.if && isLegacySyntax
        ? ("(" + (el.if) + ")?" + (genChildren(el, state) || 'undefined') + ":undefined")
        : genChildren(el, state) || 'undefined'
      : genElement(el, state)) + "}";
  // reverse proxy v-slot without scope on this.$slots
  var reverseProxy = slotScope ? "" : ",proxy:true";
  return ("{key:" + (el.slotTarget || "\"default\"") + ",fn:" + fn + reverseProxy + "}")
}

function genChildren (
  el,
  state,
  checkSkip,
  altGenElement,
  altGenNode
) {
  var children = el.children;
  if (children.length) {
    var el$1 = children[0];
    // optimize single v-for
    if (children.length === 1 &&
      el$1.for &&
      el$1.tag !== 'template' &&
      el$1.tag !== 'slot'
    ) {
      var normalizationType = checkSkip
        ? state.maybeComponent(el$1) ? ",1" : ",0"
        : "";
      return ("" + ((altGenElement || genElement)(el$1, state)) + normalizationType)
    }
    var normalizationType$1 = checkSkip
      ? getNormalizationType(children, state.maybeComponent)
      : 0;
    var gen = altGenNode || genNode;
    return ("[" + (children.map(function (c) { return gen(c, state); }).join(',')) + "]" + (normalizationType$1 ? ("," + normalizationType$1) : ''))
  }
}

// determine the normalization needed for the children array.
// 0: no normalization needed
// 1: simple normalization needed (possible 1-level deep nested array)
// 2: full normalization needed
function getNormalizationType (
  children,
  maybeComponent
) {
  var res = 0;
  for (var i = 0; i < children.length; i++) {
    var el = children[i];
    if (el.type !== 1) {
      continue
    }
    if (needsNormalization(el) ||
        (el.ifConditions && el.ifConditions.some(function (c) { return needsNormalization(c.block); }))) {
      res = 2;
      break
    }
    if (maybeComponent(el) ||
        (el.ifConditions && el.ifConditions.some(function (c) { return maybeComponent(c.block); }))) {
      res = 1;
    }
  }
  return res
}

function needsNormalization (el) {
  return el.for !== undefined || el.tag === 'template' || el.tag === 'slot'
}

function genNode (node, state) {
  if (node.type === 1) {
    return genElement(node, state)
  } else if (node.type === 3 && node.isComment) {
    return genComment(node)
  } else {
    return genText(node)
  }
}

function genText (text) {
  return ("_v(" + (text.type === 2
    ? text.expression // no need for () because already wrapped in _s()
    : transformSpecialNewlines(JSON.stringify(text.text))) + ")")
}

function genComment (comment) {
  return ("_e(" + (JSON.stringify(comment.text)) + ")")
}

function genSlot (el, state) {
  var slotName = el.slotName || '"default"';
  var children = genChildren(el, state);
  var res = "_t(" + slotName + (children ? ("," + children) : '');
  var attrs = el.attrs || el.dynamicAttrs
    ? genProps((el.attrs || []).concat(el.dynamicAttrs || []).map(function (attr) { return ({
        // slot props are camelized
        name: camelize(attr.name),
        value: attr.value,
        dynamic: attr.dynamic
      }); }))
    : null;
  var bind$$1 = el.attrsMap['v-bind'];
  if ((attrs || bind$$1) && !children) {
    res += ",null";
  }
  if (attrs) {
    res += "," + attrs;
  }
  if (bind$$1) {
    res += (attrs ? '' : ',null') + "," + bind$$1;
  }
  return res + ')'
}

// componentName is el.component, take it as argument to shun flow's pessimistic refinement
function genComponent (
  componentName,
  el,
  state
) {
  var children = el.inlineTemplate ? null : genChildren(el, state, true);
  return ("_c(" + componentName + "," + (genData$2(el, state)) + (children ? ("," + children) : '') + ")")
}

function genProps (props) {
  var staticProps = "";
  var dynamicProps = "";
  for (var i = 0; i < props.length; i++) {
    var prop = props[i];
    var value = transformSpecialNewlines(prop.value);
    if (prop.dynamic) {
      dynamicProps += (prop.name) + "," + value + ",";
    } else {
      staticProps += "\"" + (prop.name) + "\":" + value + ",";
    }
  }
  staticProps = "{" + (staticProps.slice(0, -1)) + "}";
  if (dynamicProps) {
    return ("_d(" + staticProps + ",[" + (dynamicProps.slice(0, -1)) + "])")
  } else {
    return staticProps
  }
}

// #3895, #4268
function transformSpecialNewlines (text) {
  return text
    .replace(/\u2028/g, '\\u2028')
    .replace(/\u2029/g, '\\u2029')
}

/*  */



// these keywords should not appear inside expressions, but operators like
// typeof, instanceof and in are allowed
var prohibitedKeywordRE = new RegExp('\\b' + (
  'do,if,for,let,new,try,var,case,else,with,await,break,catch,class,const,' +
  'super,throw,while,yield,delete,export,import,return,switch,default,' +
  'extends,finally,continue,debugger,function,arguments'
).split(',').join('\\b|\\b') + '\\b');

// these unary operators should not be used as property/method names
var unaryOperatorsRE = new RegExp('\\b' + (
  'delete,typeof,void'
).split(',').join('\\s*\\([^\\)]*\\)|\\b') + '\\s*\\([^\\)]*\\)');

// strip strings in expressions
var stripStringRE = /'(?:[^'\\]|\\.)*'|"(?:[^"\\]|\\.)*"|`(?:[^`\\]|\\.)*\$\{|\}(?:[^`\\]|\\.)*`|`(?:[^`\\]|\\.)*`/g;

// detect problematic expressions in a template
function detectErrors (ast, warn) {
  if (ast) {
    checkNode(ast, warn);
  }
}

function checkNode (node, warn) {
  if (node.type === 1) {
    for (var name in node.attrsMap) {
      if (dirRE.test(name)) {
        var value = node.attrsMap[name];
        if (value) {
          var range = node.rawAttrsMap[name];
          if (name === 'v-for') {
            checkFor(node, ("v-for=\"" + value + "\""), warn, range);
          } else if (onRE.test(name)) {
            checkEvent(value, (name + "=\"" + value + "\""), warn, range);
          } else {
            checkExpression(value, (name + "=\"" + value + "\""), warn, range);
          }
        }
      }
    }
    if (node.children) {
      for (var i = 0; i < node.children.length; i++) {
        checkNode(node.children[i], warn);
      }
    }
  } else if (node.type === 2) {
    checkExpression(node.expression, node.text, warn, node);
  }
}

function checkEvent (exp, text, warn, range) {
  var stipped = exp.replace(stripStringRE, '');
  var keywordMatch = stipped.match(unaryOperatorsRE);
  if (keywordMatch && stipped.charAt(keywordMatch.index - 1) !== '$') {
    warn(
      "avoid using JavaScript unary operator as property name: " +
      "\"" + (keywordMatch[0]) + "\" in expression " + (text.trim()),
      range
    );
  }
  checkExpression(exp, text, warn, range);
}

function checkFor (node, text, warn, range) {
  checkExpression(node.for || '', text, warn, range);
  checkIdentifier(node.alias, 'v-for alias', text, warn, range);
  checkIdentifier(node.iterator1, 'v-for iterator', text, warn, range);
  checkIdentifier(node.iterator2, 'v-for iterator', text, warn, range);
}

function checkIdentifier (
  ident,
  type,
  text,
  warn,
  range
) {
  if (typeof ident === 'string') {
    try {
      new Function(("var " + ident + "=_"));
    } catch (e) {
      warn(("invalid " + type + " \"" + ident + "\" in expression: " + (text.trim())), range);
    }
  }
}

function checkExpression (exp, text, warn, range) {
  try {
    new Function(("return " + exp));
  } catch (e) {
    var keywordMatch = exp.replace(stripStringRE, '').match(prohibitedKeywordRE);
    if (keywordMatch) {
      warn(
        "avoid using JavaScript keyword as property name: " +
        "\"" + (keywordMatch[0]) + "\"\n  Raw expression: " + (text.trim()),
        range
      );
    } else {
      warn(
        "invalid expression: " + (e.message) + " in\n\n" +
        "    " + exp + "\n\n" +
        "  Raw expression: " + (text.trim()) + "\n",
        range
      );
    }
  }
}

/*  */

var range = 2;

function generateCodeFrame (
  source,
  start,
  end
) {
  if ( start === void 0 ) start = 0;
  if ( end === void 0 ) end = source.length;

  var lines = source.split(/\r?\n/);
  var count = 0;
  var res = [];
  for (var i = 0; i < lines.length; i++) {
    count += lines[i].length + 1;
    if (count >= start) {
      for (var j = i - range; j <= i + range || end > count; j++) {
        if (j < 0 || j >= lines.length) { continue }
        res.push(("" + (j + 1) + (repeat$1(" ", 3 - String(j + 1).length)) + "|  " + (lines[j])));
        var lineLength = lines[j].length;
        if (j === i) {
          // push underline
          var pad = start - (count - lineLength) + 1;
          var length = end > count ? lineLength - pad : end - start;
          res.push("   |  " + repeat$1(" ", pad) + repeat$1("^", length));
        } else if (j > i) {
          if (end > count) {
            var length$1 = Math.min(end - count, lineLength);
            res.push("   |  " + repeat$1("^", length$1));
          }
          count += lineLength + 1;
        }
      }
      break
    }
  }
  return res.join('\n')
}

function repeat$1 (str, n) {
  var result = '';
  if (n > 0) {
    while (true) { // eslint-disable-line
      if (n & 1) { result += str; }
      n >>>= 1;
      if (n <= 0) { break }
      str += str;
    }
  }
  return result
}

/*  */



function createFunction (code, errors) {
  try {
    return new Function(code)
  } catch (err) {
    errors.push({ err: err, code: code });
    return noop
  }
}

function createCompileToFunctionFn (compile) {
  var cache = Object.create(null);

  return function compileToFunctions (
    template,
    options,
    vm
  ) {
    options = extend({}, options);
    var warn$$1 = options.warn || warn;
    delete options.warn;

    /* istanbul ignore if */
    {
      // detect possible CSP restriction
      try {
        new Function('return 1');
      } catch (e) {
        if (e.toString().match(/unsafe-eval|CSP/)) {
          warn$$1(
            'It seems you are using the standalone build of Vue.js in an ' +
            'environment with Content Security Policy that prohibits unsafe-eval. ' +
            'The template compiler cannot work in this environment. Consider ' +
            'relaxing the policy to allow unsafe-eval or pre-compiling your ' +
            'templates into render functions.'
          );
        }
      }
    }

    // check cache
    var key = options.delimiters
      ? String(options.delimiters) + template
      : template;
    if (cache[key]) {
      return cache[key]
    }

    // compile
    var compiled = compile(template, options);

    // check compilation errors/tips
    {
      if (compiled.errors && compiled.errors.length) {
        if (options.outputSourceRange) {
          compiled.errors.forEach(function (e) {
            warn$$1(
              "Error compiling template:\n\n" + (e.msg) + "\n\n" +
              generateCodeFrame(template, e.start, e.end),
              vm
            );
          });
        } else {
          warn$$1(
            "Error compiling template:\n\n" + template + "\n\n" +
            compiled.errors.map(function (e) { return ("- " + e); }).join('\n') + '\n',
            vm
          );
        }
      }
      if (compiled.tips && compiled.tips.length) {
        if (options.outputSourceRange) {
          compiled.tips.forEach(function (e) { return tip(e.msg, vm); });
        } else {
          compiled.tips.forEach(function (msg) { return tip(msg, vm); });
        }
      }
    }

    // turn code into functions
    var res = {};
    var fnGenErrors = [];
    res.render = createFunction(compiled.render, fnGenErrors);
    res.staticRenderFns = compiled.staticRenderFns.map(function (code) {
      return createFunction(code, fnGenErrors)
    });

    // check function generation errors.
    // this should only happen if there is a bug in the compiler itself.
    // mostly for codegen development use
    /* istanbul ignore if */
    {
      if ((!compiled.errors || !compiled.errors.length) && fnGenErrors.length) {
        warn$$1(
          "Failed to generate render function:\n\n" +
          fnGenErrors.map(function (ref) {
            var err = ref.err;
            var code = ref.code;

            return ((err.toString()) + " in\n\n" + code + "\n");
        }).join('\n'),
          vm
        );
      }
    }

    return (cache[key] = res)
  }
}

/*  */

function createCompilerCreator (baseCompile) {
  return function createCompiler (baseOptions) {
    function compile (
      template,
      options
    ) {
      var finalOptions = Object.create(baseOptions);
      var errors = [];
      var tips = [];

      var warn = function (msg, range, tip) {
        (tip ? tips : errors).push(msg);
      };

      if (options) {
        if (options.outputSourceRange) {
          // $flow-disable-line
          var leadingSpaceLength = template.match(/^\s*/)[0].length;

          warn = function (msg, range, tip) {
            var data = { msg: msg };
            if (range) {
              if (range.start != null) {
                data.start = range.start + leadingSpaceLength;
              }
              if (range.end != null) {
                data.end = range.end + leadingSpaceLength;
              }
            }
            (tip ? tips : errors).push(data);
          };
        }
        // merge custom modules
        if (options.modules) {
          finalOptions.modules =
            (baseOptions.modules || []).concat(options.modules);
        }
        // merge custom directives
        if (options.directives) {
          finalOptions.directives = extend(
            Object.create(baseOptions.directives || null),
            options.directives
          );
        }
        // copy other options
        for (var key in options) {
          if (key !== 'modules' && key !== 'directives') {
            finalOptions[key] = options[key];
          }
        }
      }

      finalOptions.warn = warn;

      var compiled = baseCompile(template.trim(), finalOptions);
      {
        detectErrors(compiled.ast, warn);
      }
      compiled.errors = errors;
      compiled.tips = tips;
      return compiled
    }

    return {
      compile: compile,
      compileToFunctions: createCompileToFunctionFn(compile)
    }
  }
}

/*  */

// `createCompilerCreator` allows creating compilers that use alternative
// parser/optimizer/codegen, e.g the SSR optimizing compiler.
// Here we just export a default compiler using the default parts.
var createCompiler = createCompilerCreator(function baseCompile (
  template,
  options
) {
  var ast = parse(template.trim(), options);
  if (options.optimize !== false) {
    optimize(ast, options);
  }
  var code = generate(ast, options);
  return {
    ast: ast,
    render: code.render,
    staticRenderFns: code.staticRenderFns
  }
});

/*  */

var ref$1 = createCompiler(baseOptions);
var compile = ref$1.compile;
var compileToFunctions = ref$1.compileToFunctions;

/*  */

// check whether current browser encodes a char inside attribute values
var div;
function getShouldDecode (href) {
  div = div || document.createElement('div');
  div.innerHTML = href ? "<a href=\"\n\"/>" : "<div a=\"\n\"/>";
  return div.innerHTML.indexOf('&#10;') > 0
}

// #3663: IE encodes newlines inside attribute values while other browsers don't
var shouldDecodeNewlines = inBrowser ? getShouldDecode(false) : false;
// #6828: chrome encodes content in a[href]
var shouldDecodeNewlinesForHref = inBrowser ? getShouldDecode(true) : false;

/*  */

var idToTemplate = cached(function (id) {
  var el = query(id);
  return el && el.innerHTML
});

var mount = Vue.prototype.$mount;
Vue.prototype.$mount = function (
  el,
  hydrating
) {
  el = el && query(el);

  /* istanbul ignore if */
  if (el === document.body || el === document.documentElement) {
    warn(
      "Do not mount Vue to <html> or <body> - mount to normal elements instead."
    );
    return this
  }

  var options = this.$options;
  // resolve template/el and convert to render function
  if (!options.render) {
    var template = options.template;
    if (template) {
      if (typeof template === 'string') {
        if (template.charAt(0) === '#') {
          template = idToTemplate(template);
          /* istanbul ignore if */
          if (!template) {
            warn(
              ("Template element not found or is empty: " + (options.template)),
              this
            );
          }
        }
      } else if (template.nodeType) {
        template = template.innerHTML;
      } else {
        {
          warn('invalid template option:' + template, this);
        }
        return this
      }
    } else if (el) {
      template = getOuterHTML(el);
    }
    if (template) {
      /* istanbul ignore if */
      if (config.performance && mark) {
        mark('compile');
      }

      var ref = compileToFunctions(template, {
        outputSourceRange: "development" !== 'production',
        shouldDecodeNewlines: shouldDecodeNewlines,
        shouldDecodeNewlinesForHref: shouldDecodeNewlinesForHref,
        delimiters: options.delimiters,
        comments: options.comments
      }, this);
      var render = ref.render;
      var staticRenderFns = ref.staticRenderFns;
      options.render = render;
      options.staticRenderFns = staticRenderFns;

      /* istanbul ignore if */
      if (config.performance && mark) {
        mark('compile end');
        measure(("vue " + (this._name) + " compile"), 'compile', 'compile end');
      }
    }
  }
  return mount.call(this, el, hydrating)
};

/**
 * Get outerHTML of elements, taking care
 * of SVG elements in IE as well.
 */
function getOuterHTML (el) {
  if (el.outerHTML) {
    return el.outerHTML
  } else {
    var container = document.createElement('div');
    container.appendChild(el.cloneNode(true));
    return container.innerHTML
  }
}

Vue.compile = compileToFunctions;

module.exports = Vue;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! ./../../timers-browserify/main.js */ "./node_modules/timers-browserify/main.js").setImmediate))

/***/ }),

/***/ "./node_modules/vue/dist/vue.common.js":
/*!*********************************************!*\
  !*** ./node_modules/vue/dist/vue.common.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

if (false) {} else {
  module.exports = __webpack_require__(/*! ./vue.common.dev.js */ "./node_modules/vue/dist/vue.common.dev.js")
}


/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./resources/js/AdminVue.js":
/*!**********************************!*\
  !*** ./resources/js/AdminVue.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_IncredibleOffers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/IncredibleOffers */ "./resources/js/components/IncredibleOffers.vue");
/* harmony import */ var _components_Counter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/Counter */ "./resources/js/components/Counter.vue");
/* harmony import */ var _components_OrderStep__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/OrderStep */ "./resources/js/components/OrderStep.vue");
/* harmony import */ var _components_StockroomProductList__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/StockroomProductList */ "./resources/js/components/StockroomProductList.vue");
/* harmony import */ var _components_StockroomOutputList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/StockroomOutputList */ "./resources/js/components/StockroomOutputList.vue");
/* harmony import */ var _components_SaleReport__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/SaleReport */ "./resources/js/components/SaleReport.vue");
/* harmony import */ var _components_PackageContent__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/PackageContent */ "./resources/js/components/PackageContent.vue");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var vue_axios__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vue-axios */ "./node_modules/vue-axios/dist/vue-axios.min.js");
/* harmony import */ var vue_axios__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(vue_axios__WEBPACK_IMPORTED_MODULE_8__);
window.Vue = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
Vue.component('pagination', __webpack_require__(/*! laravel-vue-pagination */ "./node_modules/laravel-vue-pagination/dist/laravel-vue-pagination.common.js"));
Vue.component('Cleave', __webpack_require__(/*! vue-cleave-component */ "./node_modules/vue-cleave-component/dist/vue-cleave.min.js"));









Vue.use(vue_axios__WEBPACK_IMPORTED_MODULE_8___default.a, axios__WEBPACK_IMPORTED_MODULE_7___default.a);
Vue.prototype.$siteUrl = 'http://localhost/digikala/public';
var app = new Vue({
  el: '#app',
  components: {
    IncredibleOffers: _components_IncredibleOffers__WEBPACK_IMPORTED_MODULE_0__["default"],
    Counter: _components_Counter__WEBPACK_IMPORTED_MODULE_1__["default"],
    OrderStep: _components_OrderStep__WEBPACK_IMPORTED_MODULE_2__["default"],
    StockroomProductList: _components_StockroomProductList__WEBPACK_IMPORTED_MODULE_3__["default"],
    StockroomOutputList: _components_StockroomOutputList__WEBPACK_IMPORTED_MODULE_4__["default"],
    SaleReport: _components_SaleReport__WEBPACK_IMPORTED_MODULE_5__["default"],
    PackageContent: _components_PackageContent__WEBPACK_IMPORTED_MODULE_6__["default"]
  }
});

/***/ }),

/***/ "./resources/js/components/Counter.vue":
/*!*********************************************!*\
  !*** ./resources/js/components/Counter.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Counter_vue_vue_type_template_id_68e92161_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Counter.vue?vue&type=template&id=68e92161&scoped=true& */ "./resources/js/components/Counter.vue?vue&type=template&id=68e92161&scoped=true&");
/* harmony import */ var _Counter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Counter.vue?vue&type=script&lang=js& */ "./resources/js/components/Counter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Counter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Counter_vue_vue_type_template_id_68e92161_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Counter_vue_vue_type_template_id_68e92161_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "68e92161",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Counter.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Counter.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/components/Counter.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Counter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Counter.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Counter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Counter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Counter.vue?vue&type=template&id=68e92161&scoped=true&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/Counter.vue?vue&type=template&id=68e92161&scoped=true& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Counter_vue_vue_type_template_id_68e92161_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Counter.vue?vue&type=template&id=68e92161&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Counter.vue?vue&type=template&id=68e92161&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Counter_vue_vue_type_template_id_68e92161_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Counter_vue_vue_type_template_id_68e92161_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/IncredibleOffers.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/IncredibleOffers.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _IncredibleOffers_vue_vue_type_template_id_1ac9bd27_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./IncredibleOffers.vue?vue&type=template&id=1ac9bd27&scoped=true& */ "./resources/js/components/IncredibleOffers.vue?vue&type=template&id=1ac9bd27&scoped=true&");
/* harmony import */ var _IncredibleOffers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./IncredibleOffers.vue?vue&type=script&lang=js& */ "./resources/js/components/IncredibleOffers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _IncredibleOffers_vue_vue_type_style_index_0_id_1ac9bd27_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css& */ "./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _IncredibleOffers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _IncredibleOffers_vue_vue_type_template_id_1ac9bd27_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _IncredibleOffers_vue_vue_type_template_id_1ac9bd27_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1ac9bd27",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/IncredibleOffers.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/IncredibleOffers.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/IncredibleOffers.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./IncredibleOffers.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/IncredibleOffers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css& ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_style_index_0_id_1ac9bd27_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--5-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--5-2!../../../node_modules/vue-loader/lib??vue-loader-options!./IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/IncredibleOffers.vue?vue&type=style&index=0&id=1ac9bd27&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_style_index_0_id_1ac9bd27_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_style_index_0_id_1ac9bd27_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_style_index_0_id_1ac9bd27_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_style_index_0_id_1ac9bd27_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_style_index_0_id_1ac9bd27_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/IncredibleOffers.vue?vue&type=template&id=1ac9bd27&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/IncredibleOffers.vue?vue&type=template&id=1ac9bd27&scoped=true& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_template_id_1ac9bd27_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./IncredibleOffers.vue?vue&type=template&id=1ac9bd27&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/IncredibleOffers.vue?vue&type=template&id=1ac9bd27&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_template_id_1ac9bd27_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IncredibleOffers_vue_vue_type_template_id_1ac9bd27_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/OrderStep.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/OrderStep.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _OrderStep_vue_vue_type_template_id_2121af42_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OrderStep.vue?vue&type=template&id=2121af42&scoped=true& */ "./resources/js/components/OrderStep.vue?vue&type=template&id=2121af42&scoped=true&");
/* harmony import */ var _OrderStep_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OrderStep.vue?vue&type=script&lang=js& */ "./resources/js/components/OrderStep.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _OrderStep_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _OrderStep_vue_vue_type_template_id_2121af42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _OrderStep_vue_vue_type_template_id_2121af42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2121af42",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/OrderStep.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/OrderStep.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/OrderStep.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderStep_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./OrderStep.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/OrderStep.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderStep_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/OrderStep.vue?vue&type=template&id=2121af42&scoped=true&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/OrderStep.vue?vue&type=template&id=2121af42&scoped=true& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderStep_vue_vue_type_template_id_2121af42_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./OrderStep.vue?vue&type=template&id=2121af42&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/OrderStep.vue?vue&type=template&id=2121af42&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderStep_vue_vue_type_template_id_2121af42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OrderStep_vue_vue_type_template_id_2121af42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/PackageContent.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/PackageContent.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PackageContent_vue_vue_type_template_id_61fde6fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PackageContent.vue?vue&type=template&id=61fde6fe& */ "./resources/js/components/PackageContent.vue?vue&type=template&id=61fde6fe&");
/* harmony import */ var _PackageContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PackageContent.vue?vue&type=script&lang=js& */ "./resources/js/components/PackageContent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PackageContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PackageContent_vue_vue_type_template_id_61fde6fe___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PackageContent_vue_vue_type_template_id_61fde6fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/PackageContent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/PackageContent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/PackageContent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PackageContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./PackageContent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/PackageContent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PackageContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/PackageContent.vue?vue&type=template&id=61fde6fe&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/PackageContent.vue?vue&type=template&id=61fde6fe& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PackageContent_vue_vue_type_template_id_61fde6fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./PackageContent.vue?vue&type=template&id=61fde6fe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/PackageContent.vue?vue&type=template&id=61fde6fe&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PackageContent_vue_vue_type_template_id_61fde6fe___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PackageContent_vue_vue_type_template_id_61fde6fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/SaleReport.vue":
/*!************************************************!*\
  !*** ./resources/js/components/SaleReport.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SaleReport_vue_vue_type_template_id_49057786___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SaleReport.vue?vue&type=template&id=49057786& */ "./resources/js/components/SaleReport.vue?vue&type=template&id=49057786&");
/* harmony import */ var _SaleReport_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SaleReport.vue?vue&type=script&lang=js& */ "./resources/js/components/SaleReport.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SaleReport_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SaleReport_vue_vue_type_template_id_49057786___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SaleReport_vue_vue_type_template_id_49057786___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/SaleReport.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/SaleReport.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/SaleReport.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleReport_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./SaleReport.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/SaleReport.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleReport_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/SaleReport.vue?vue&type=template&id=49057786&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/SaleReport.vue?vue&type=template&id=49057786& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleReport_vue_vue_type_template_id_49057786___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./SaleReport.vue?vue&type=template&id=49057786& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/SaleReport.vue?vue&type=template&id=49057786&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleReport_vue_vue_type_template_id_49057786___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SaleReport_vue_vue_type_template_id_49057786___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/StockroomOutputList.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/StockroomOutputList.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StockroomOutputList_vue_vue_type_template_id_9d976a16___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StockroomOutputList.vue?vue&type=template&id=9d976a16& */ "./resources/js/components/StockroomOutputList.vue?vue&type=template&id=9d976a16&");
/* harmony import */ var _StockroomOutputList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StockroomOutputList.vue?vue&type=script&lang=js& */ "./resources/js/components/StockroomOutputList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StockroomOutputList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StockroomOutputList_vue_vue_type_template_id_9d976a16___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StockroomOutputList_vue_vue_type_template_id_9d976a16___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/StockroomOutputList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/StockroomOutputList.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/StockroomOutputList.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomOutputList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./StockroomOutputList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StockroomOutputList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomOutputList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/StockroomOutputList.vue?vue&type=template&id=9d976a16&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/StockroomOutputList.vue?vue&type=template&id=9d976a16& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomOutputList_vue_vue_type_template_id_9d976a16___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./StockroomOutputList.vue?vue&type=template&id=9d976a16& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StockroomOutputList.vue?vue&type=template&id=9d976a16&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomOutputList_vue_vue_type_template_id_9d976a16___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomOutputList_vue_vue_type_template_id_9d976a16___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/StockroomProductList.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/StockroomProductList.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StockroomProductList_vue_vue_type_template_id_43098f67___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StockroomProductList.vue?vue&type=template&id=43098f67& */ "./resources/js/components/StockroomProductList.vue?vue&type=template&id=43098f67&");
/* harmony import */ var _StockroomProductList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StockroomProductList.vue?vue&type=script&lang=js& */ "./resources/js/components/StockroomProductList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StockroomProductList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StockroomProductList_vue_vue_type_template_id_43098f67___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StockroomProductList_vue_vue_type_template_id_43098f67___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/StockroomProductList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/StockroomProductList.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/StockroomProductList.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomProductList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./StockroomProductList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StockroomProductList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomProductList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/StockroomProductList.vue?vue&type=template&id=43098f67&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/StockroomProductList.vue?vue&type=template&id=43098f67& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomProductList_vue_vue_type_template_id_43098f67___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./StockroomProductList.vue?vue&type=template&id=43098f67& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/StockroomProductList.vue?vue&type=template&id=43098f67&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomProductList_vue_vue_type_template_id_43098f67___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StockroomProductList_vue_vue_type_template_id_43098f67___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/myMixin.js":
/*!*********************************!*\
  !*** ./resources/js/myMixin.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    replaceNumber: function replaceNumber(n) {
      if (n != undefined) {
        n = n.toString();
        var find = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        var replace = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

        for (var i = 0; i < find.length; i++) {
          n = n.replace(new RegExp(find[i], 'g'), replace[i]);
        }

        return n;
      }
    },
    check_mobile_number: function check_mobile_number() {
      if (isNaN(this.mobile)) {
        return true;
      } else {
        if (this.mobile.toString().trim().length == 11) {
          if (this.mobile.toString().charAt(0) == '0' && this.mobile.toString().charAt(1) == '9') {
            return false;
          } else {
            return true;
          }
        } else if (mthis.obile.toString().trim().length == 10) {
          if (this.mobile.toString().charAt(0) == '9') {
            return false;
          } else {
            return true;
          }
        } else {
          return true;
        }
      }
    },
    number_format: function number_format(num) {
      num = num.toString();
      var format = '';
      var counter = 0;

      for (var i = num.length - 1; i >= 0; i--) {
        format += num[i];
        counter++;

        if (counter == 3) {
          format += ",";
          counter = 0;
        }
      }

      return format.split('').reverse().join('');
    },
    gregorian_to_jalali: function gregorian_to_jalali(gy, gm, gd) {
      var g_d_m, jy, jm, jd, gy2, days;
      g_d_m = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];

      if (gy > 1600) {
        jy = 979;
        gy -= 1600;
      } else {
        jy = 0;
        gy -= 621;
      }

      gy2 = gm > 2 ? gy + 1 : gy;
      days = 365 * gy + parseInt((gy2 + 3) / 4) - parseInt((gy2 + 99) / 100) + parseInt((gy2 + 399) / 400) - 80 + gd + g_d_m[gm - 1];
      jy += 33 * parseInt(days / 12053);
      days %= 12053;
      jy += 4 * parseInt(days / 1461);
      days %= 1461;

      if (days > 365) {
        jy += parseInt((days - 1) / 365);
        days = (days - 1) % 365;
      }

      jm = days < 186 ? 1 + parseInt(days / 31) : 7 + parseInt((days - 186) / 30);
      jd = 1 + (days < 186 ? days % 31 : (days - 186) % 30);
      return [jy, jm, jd];
    },
    show_mobile_box: function show_mobile_box() {
      this.$nextTick(function () {
        $('body').css('overflow-y', 'hidden');
        var width = $(window).width();
        var right = "-" + width + "px";
        $('.mobile_data_box').css({
          'right': right
        });
        setTimeout(function () {
          $('.mobile_data_box').css('right', '0px');
        }, 50);
      });
    },
    get_request_url: function get_request_url(url, page) {
      var url_params = url.split('?');

      if (url_params[1] == undefined) {
        url = url + "?page=" + page;
      } else {
        url = url + "&page=" + page;
      }

      return url;
    },
    setRangSlider: function setRangSlider(price) {
      var app = this;
      var slider = document.querySelector('.price_rang_slider');

      if (this.noUiSlider == null) {
        if (parseInt(price) > 0) {
          this.noUiSlider = noUiSlider.create(slider, {
            start: [0, price],
            connect: true,
            direction: 'rtl',
            range: {
              'min': 0,
              'max': price
            },
            format: {
              from: function from(value) {
                return parseInt(value);
              },
              to: function to(value) {
                return parseInt(value);
              }
            }
          });
        }
      }

      if (slider.noUiSlider != undefined) {
        slider.noUiSlider.on('update', function (values, handle) {
          app.min_price = values[0];
          app.max_price = values[1];
          $("#min_price").text(app.replaceNumber(app.number_format(values[0])));
          $("#max_price").text(app.replaceNumber(app.number_format(values[1])));
        });
        var search = new window.URLSearchParams(window.location.search);
        var min = search.get('price[min]') != null ? parseInt(search.get('price[min]')) : 0;

        if (search.get('price[max]') != null) {
          slider.noUiSlider.updateOptions({
            start: [min, parseInt(search.get('price[max]'))]
          });
        }

        if (search.get('price[min]') != null && search.get('price[max]') == null) {
          slider.noUiSlider.updateOptions({
            start: [parseInt(search.get('price[min]')), slider.noUiSlider.get()[1]]
          });
        }
      }
    },
    check_has_off: function check_has_off(product) {
      if (product.get_first_product_price != null) {
        var last_time = product.get_first_product_price.offers_last_time;
        var time = Math.floor(Date.now() / 1000);

        if (product.get_first_product_price.offers == 1 && last_time - time > 0) {
          return last_time - time;
        } else {
          return false;
        }
      } else {
        return false;
      }
    },
    getDiscountValue: function getDiscountValue(price1, price2) {
      var a = price2 / price1 * 100;
      a = 100 - a;
      a = Math.round(a);
      return a;
    },
    set_filter_event: function set_filter_event(el, page_url) {
      var data = $(el).attr('data');
      data = data.split('_');

      if ($('.check_box', el).hasClass('active')) {
        $('.check_box', el).removeClass('active');
        this.remove_url_query_string(data[0], data[2], page_url);
        this.remove_filter_tag(data[0], data[2], page_url);
      } else {
        $('.check_box', el).addClass('active');
        this.add_url_query_string(data[0], data[2], page_url);
        this.add_filter_tag(data, data[0], data[2], page_url);
      }
    },
    setPageUrl: function setPageUrl(url) {
      window.history.pushState('data', 'title', url);
    },
    remove_url_query_string: function remove_url_query_string(key, value, page_url) {
      var url = page_url == undefined ? window.location.href : page_url;
      var check = url.split(key);
      var params = url.split('?');
      var h = 0;

      if (params[1] != undefined) {
        if (params[1].indexOf('&') > -1) {
          var vars = params[1].split('&');

          for (var i in vars) {
            var k = vars[i].split('=')[0];
            var v = vars[i].split('=')[1];
            var n = k.indexOf(key);

            if (n > -1 && v != value) {
              k = k.replace(key, '');
              k = k.replace('[', '');
              k = k.replace(']', '');
              var new_string = key + "[" + h + "]=" + v;
              var old_string = key + "[" + k + "]=" + v;
              url = url.replace(old_string, new_string);
              h++;
            } else if (n > -1) {
              url = url.replace('&' + k + "=" + v, '');
              url = url.replace('?' + k + "=" + v, '');
            }
          }
        } else {
          url = url.replace('?' + key + "[0]" + "=" + value, '');
        }
      }

      var url_params = url.split('?');

      if (url_params[1] == undefined) {
        url = url.replace('&', '?');
      }

      this.changed_url(url);
    },
    check_search_params: function check_search_params(page_url) {
      var url = page_url == undefined ? window.location.href : page_url;
      var params = url.split('?');

      if (params[1] != undefined) {
        if (params[1].indexOf('&') > -1) {
          var vars = params[1].split('&');

          for (var i in vars) {
            var k = vars[i].split('=')[0];
            var v = vars[i].split('=')[1];
            k = k.split('[');
            this.add_active_filter(k, v);
          }
        } else {
          var _k = params[1].split('=')[0];
          var _v = params[1].split('=')[1];
          _k = _k.split('[');
          this.add_active_filter(_k, _v);
        }
      }
    },
    add_active_filter: function add_active_filter(k, v) {
      if (k.length > 1) {
        var data = "";
        var filter_key = k[0];

        if (k.length == 3) {
          data = k[0] + "[" + k[1] + "_param_" + v;
          data = "'" + data + "'";
          filter_key = k[0] + "[" + k[1];
        } else {
          data = k[0] + "_param_" + v;
        }

        $('li[data=' + data + '] .check_box').addClass('active');
        $('li[data=' + data + ']').parent().parent().slideDown();

        if ($('li[data=' + data + ']').parent().parent().parent().parent().find('.title_box').find('span').hasClass('fa-plus-circle')) {
          $('li[data=' + data + ']').parent().parent().parent().parent().find('.title_box').find('span').removeClass('fa-plus-circle').addClass('fa-minus-circle');
        }

        if ($('li[data=' + data + ']').length == 1) {
          this.add_filter_tag(data, filter_key, v);
        }
      } else {
        if (k == "has_product") {
          this.set_enable_product_status_toggle();
        } else if (k == "has_ready_to_shipment") {
          this.set_enable_send_status_toggle();
        }
      }
    },
    remove_all_filter: function remove_all_filter(page_url) {
      var url = page_url == undefined ? window.location.href : page_url;
      url = url.split('?')[0];
      $('.selected_filter_item').remove();
      $("#filter_div").hide();
      $('.filter_box .list-inline li').find('.check_box').removeClass('active');

      if ($('#product_status .toggle-slide .toggle-off').hasClass('active')) {
        $("#product_status").click();
      }

      if ($('#send_status .toggle-slide .toggle-off').hasClass('active')) {
        $("#send_status").click();
      }

      if (this.noUiSlider) {
        this.noUiSlider.reset();
      }

      if (page_url == undefined) {
        this.setPageUrl(url);
        this.getProduct(1);
      } else {
        this.search_url = url;
      }
    },
    remove_url_params: function remove_url_params(key, value, page_url) {
      var params = new window.URLSearchParams(window.location.search);

      if (page_url != undefined) {
        var search_url_params = this.search_url.split('?');

        if (search_url_params[1] != undefined) {
          search_url_params = '?' + search_url_params[1];
          params = new window.URLSearchParams(search_url_params);
        }
      }

      var url = page_url == undefined ? window.location.href : page_url;

      if (params.get(key) != null) {
        value = encodeURIComponent(value);
        url = url.replace('&' + key + "=" + value, '');
        url = url.replace('?' + key + "=" + value, '');
        this.remove_filter_tag(key, value);
        var url_params = url.split('?');

        if (url_params[1] == undefined) {
          url = url.replace('&', '?');
        }

        if (page_url == undefined) {
          this.setPageUrl(url);
          this.getProduct(1);
        } else {
          this.search_url = url;
        }
      }
    },
    set_enable_send_status_toggle: function set_enable_send_status_toggle() {
      if (!$("#selected_filter_box").find('div').hasClass('send_status_filter')) {
        $("#filter_div").show();
        var html = '<div class="selected_filter_item send_status_filter">' + '<span>فقط کالاهای آماده ارسال</span> <span class="fa fa-close"></span>' + '</div>';
        $("#selected_filter_box").append(html);
      }
    },
    set_enable_product_status_toggle: function set_enable_product_status_toggle() {
      if (!$("#selected_filter_box").find('div').hasClass('product_status_filter')) {
        $("#filter_div").show();
        var html = '<div class="selected_filter_item product_status_filter">' + '<span>فقط کالاهای موجود</span> <span class="fa fa-close"></span>' + '</div>';
        $("#selected_filter_box").append(html);
      }
    },
    set_product_sort: function set_product_sort() {
      var params = new window.URLSearchParams(window.location.search);
      var url = window.location.href;

      if (params.get("sortby") != null) {
        var sortby = parseInt(params.get("sortby"));

        if (sortby >= 21 && sortby <= 25) {
          this.sort = sortby;
        }
      }
    },
    getLabel: function getLabel(key, key2) {
      key2 = key2 + 1;
      var a = "score" + key2;

      if (this.list.data[key]['get_score'][a] != undefined) {
        return this.scoreLabel[this.list.data[key]['get_score'][a]];
      } else {
        return 'معمولی';
      }
    },
    getWidth: function getWidth(key, key2) {
      key2 = key2 + 1;
      var a = "score" + key2;

      if (this.list.data[key]['get_score'][a] != undefined) {
        return this.list.data[key]['get_score'][a] * 25;
      } else {
        return 50;
      }
    },
    getDate: function getDate(time) {
      time = time * 1000;
      var date = new Date(time);
      var jalali = this.gregorian_to_jalali(date.getFullYear(), date.getMonth() + 1, date.getDate());
      var r = this.replaceNumber(jalali[2]) + " " + this.monthName[jalali[1] - 1] + " " + this.replaceNumber(jalali[0]);
      return r;
    },
    like: function like(element, row_id, table_name, redirect) {
      var _this = this;

      if (this.send) {
        $("#loading_box").show();
        this.send = false;
        var url = this.$siteUrl + "/user/like";
        var formData = new FormData();
        formData.append('row_id', row_id);
        formData.append('table_name', table_name);
        this.axios.post(url, formData).then(function (response) {
          _this.send = true;
          $("#loading_box").hide();

          if (response.data == "add") {
            element.like = element.like + 1;
          } else if (response.data == "remove") {
            element.like = element.like - 1;
          }
        })["catch"](function (error) {
          _this.send = true;
          $("#loading_box").hide();

          if (error.response.status == 401) {
            if (redirect != undefined) {
              _this.$refs.loginBox.show_box();
            } else {
              $("#login_box").modal('show');
            }
          }
        });
      }
    },
    dislike: function dislike(element, row_id, table_name, redirect) {
      var _this2 = this;

      if (this.send) {
        $("#loading_box").show();
        this.send = false;
        var url = this.$siteUrl + "/user/dislike";
        var formData = new FormData();
        formData.append('row_id', row_id);
        formData.append('table_name', table_name);
        this.axios.post(url, formData).then(function (response) {
          _this2.send = true;
          $("#loading_box").hide();

          if (response.data == "add") {
            element.dislike = element.dislike + 1;
          } else if (response.data == "remove") {
            element.dislike = element.dislike - 1;
          }
        })["catch"](function (error) {
          _this2.send = true;
          $("#loading_box").hide();

          if (error.response.status == 401) {
            if (redirect != undefined) {
              _this2.$refs.loginBox.show_box();
            } else {
              $("#login_box").modal('show');
            }
          }
        });
      }
    },
    hide_transition_box: function hide_transition_box() {
      this.show_box = false;
      $('body').css('overflow-y', 'auto');
    },
    showModalBox: function showModalBox() {
      this.$refs.data.setTitle('افزودن آدرس جدید');
      $("#myModal").modal('show');
    },
    updateRow: function updateRow(address) {
      this.$refs.data.setUpdateData(address, 'ویرایش آدرس');

      if (address['lat'] != "0.0") {
        updateMap(address['lat'], address['lng']);
      }
    },
    remove_address: function remove_address(address) {
      this.remove_address_id = address.id;
      this.show_dialog_box = true;
    },
    delete_address: function delete_address(paginate) {
      var _this3 = this;

      var string = paginate == undefined ? '' : "?paginate=ok";
      $("#loading_box").show();
      this.show_dialog_box = false;
      var url = this.$siteUrl + "/user/removeAddress/" + this.remove_address_id + string;
      this.axios["delete"](url).then(function (response) {
        $("#loading_box").hide();

        if (response.data != "error") {
          _this3.AddressLists = response.data;
        }
      })["catch"](function (error) {
        $("#loading_box").hide();
      });
    },
    login: function login() {
      var _this4 = this;

      this.validate_login_mobile();
      this.validate_login_password();

      if (!this.password_error && !this.mobile_error && this.send_data) {
        this.send_data = false;
        var formData = new FormData();
        formData.append('mobile', this.mobile);
        formData.append('password', this.password);
        formData.append('remember', this.checked);
        var url = this.$siteUrl + "/vue_login";
        $("#loading_box").show();
        this.serverError = false;
        this.axios.post(url, formData).then(function (response) {
          $("#loading_box").hide();
          _this4.send_data = true;

          if (response.data.status == 'ok') {
            $("#login_box").modal('hide');
            window.location = window.location.href;
          } else {
            _this4.serverError = response.data.status;
          }
        })["catch"](function (error) {
          _this4.send_data = true;
          _this4.serverError = 'خطا در ارسال اطلاعات مجددا تلاش نمایید';
        });
      }
    },
    validate_login_mobile: function validate_login_mobile() {
      if (this.mobile.toString().trim() == "") {
        this.mobile_error = true;
        this.mobile_error_text = 'لطفا شماره موبایل خود را وارد نمایید';
      } else if (this.check_mobile_number()) {
        this.mobile_error = true;
        this.mobile_error_text = 'شماره موبایل وارد شده معتبر نمی باشد';
      } else {
        this.mobile_error = false;
        this.mobile_error_text = false;
      }
    },
    validate_login_password: function validate_login_password() {
      if (this.password.toString().trim() == "") {
        this.password_error = true;
        this.password_error_text = 'لطفا کلمه عبور خود را وارد نمایید';
      } else {
        this.password_error = false;
        this.password_error_text = false;
      }
    },
    add_question: function add_question(answer_id) {
      var _this5 = this;

      var question_id = answer_id == undefined ? 0 : answer_id;

      if (this.Question.trim() != "") {
        if (this.send) {
          this.send = false;
          $("#loading_box").show();
          var url = this.$siteUrl + "/user/addQestion";
          var formData = new FormData();
          formData.append('product_id', this.product_id);
          formData.append('question', this.Question);
          formData.append('send_email', this.send_email);
          formData.append('question_id', question_id);
          this.axios.post(url, formData).then(function (reponse) {
            $("#loading_box").hide();
            _this5.send = true;

            if (reponse.data == 'ok') {
              if (question_id > 0) {
                _this5.save_answer = true;
              } else {
                _this5.save_question = true;
              }

              _this5.Question = '';
            }
          })["catch"](function (error) {
            $("#loading_box").hide();
            _this5.send = true;
          });
        }
      }
    },
    check_changes_cart: function check_changes_cart() {
      var app = this;
      this.CartProduct.product.forEach(function (row) {
        if (row.initial_product_count != row.product_count) {
          if (row.product_count == 0) {
            var msg = 'وضعیت محصول ';
            msg = msg + row.product_title + ' ';
            msg = msg + 'به ناموجود تغییر کرده است';
            app.changes_cart.push(msg);
          } else {
            var _msg = 'تعداد محصول ';
            _msg = _msg + row.product_title + ' ';
            _msg = _msg + 'از ' + app.replaceNumber(app.number_format(row.initial_product_count));
            _msg = _msg + ' به ' + app.replaceNumber(app.number_format(row.product_count)) + ' تغییر یافت';
            app.changes_cart.push(_msg);
          }
        } else if (row.initial_amount != undefined) {
          if (row.initial_amount != row.int_price / row.product_count) {
            var _msg2 = 'قیمت محصول ';
            _msg2 = _msg2 + row.product_title + ' ';
            _msg2 = _msg2 + 'از ' + app.replaceNumber(app.number_format(row.initial_amount)) + ' تومان ';
            _msg2 = _msg2 + 'به ' + app.replaceNumber(app.number_format(row.int_price / row.product_count)) + ' تومان تغییر پیدا کرد ';
            app.changes_cart.push(_msg2);
          }
        }
      });
    },
    change_send_address: function change_send_address() {}
  }
});

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi ./resources/js/AdminVue.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/js/AdminVue.js */"./resources/js/AdminVue.js");


/***/ })

/******/ });