<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\OrderData;
use App\OrderInfo;
use Illuminate\Http\Request;
use DB;
use App\Stockroom;
use App\OrderProduct;
use Auth;
class OrdersController extends CustomController
{
    protected  $model='Order';
    protected  $title='سفارشات';
    protected $route_params='orders';
    public function index(Request $request)
    {
        $time=time()-30*60;
        Order::where(['pay_status'=>'awaiting_payemnt'])->where('created_at','<=',$time)->update(['pay_status'=>'canceled']);
        $orders=Order::getData($request->all());
        $trash_orders_count=Order::onlyTrashed()->count();
        return view('orders.index',[
            'orders'=>$orders,'trash_orders_count'=>$trash_orders_count,'req'=>$request
        ]);
    }
    public function show($order_id)
    {
        $order=Order::with(['getProductRow.getSeller','getOrderInfo.getEvent.getUser','getAddress','getGiftCart'])
            ->where(['id'=>$order_id])->firstOrFail();
        if( $order->order_read=='no'){
            $order->order_read='ok';
            $order->update();
        }
        $order_discount=DB::table('order_discount')->where('order_id',$order->id)->get();
        $order_data=new OrderData($order->getOrderInfo,$order->getProductRow,$order->user_id);
        $order_data=$order_data->getData();
        return view('orders.show',['order'=>$order,'order_data'=>$order_data,'order_discount'=>$order_discount]);
    }
    public function change_status(Request $request)
    {
        $order_id=$request->get('order_id');
        $status=$request->get('status');
        $tozihat=$request->get('tozihat');
        $orderInfo=OrderInfo::where('id',$order_id)->first();
        $user_id=Auth::user()->id;

        if($orderInfo){
            DB::beginTransaction();
            $old_status=$orderInfo->send_status;
            $orderInfo->send_status=$status;
            try{
                $orderInfo->update();
                set_order_product_status($orderInfo,$status);
                set_submission_event($old_status,$status,$order_id,$tozihat,$user_id);
                DB::commit();
                return 'ok';
            }
            catch (\Exception $exception){
                DB::rollBack();
                return 'error';
            }
        }
        else{
            return 'error';
        }
    }
    public function submission(Request $request)
    {
        $submission=OrderInfo::getData($request->all(),0,'DESC');
        return view('orders.submission',[
            'label'=>'مدیریت مرسوله ها' ,
            'label_url'=>'submission',
            'submission'=>$submission,
            'req'=>$request
        ]);
    }
    public function submission_info($id){

        $submission_info=OrderInfo::with(['getOrder.getAddress','getEvent.getUser'])->has('getOrder')->where('id',$id)->firstOrFail();
        $order_data=new OrderData($submission_info->getOrder->getOrderInfo,$submission_info->getOrder->getProductRow,$submission_info->getOrder->user_id);
        $order_data=$order_data->getData($id);
        return view('orders.submission_info',['submission_info'=>$submission_info,'order_data'=>$order_data]);
    }
    public function submission_approved(Request $request){
        $submission=OrderInfo::getData($request->all(),1);
        return view('orders.submission',[
            'label'=>'مرسوله های تایید شده' ,
            'label_url'=>'submission/approved',
            'submission'=>$submission,
            'req'=>$request
        ]);
    }
    public function items_today(Request $request){
        $submission=OrderInfo::getData($request->all(),2);
        return view('orders.submission',[
            'label'=>'مرسوله های ارسالی امروز' ,
            'label_url'=>'submission/items/today',
            'submission'=>$submission,
            'req'=>$request
        ]);
    }
    public function submission_ready(Request $request){
        $submission=OrderInfo::getData($request->all(),3);
        return view('orders.submission',[
            'label'=>'مرسوله های آماده ارسال' ,
            'label_url'=>'submission/ready',
            'submission'=>$submission,
            'req'=>$request
        ]);
    }
    public function posting_send(Request $request){
        $submission=OrderInfo::getData($request->all(),4);
        return view('orders.submission',[
            'label'=>'مرسوله های ارسال شده به پست' ,
            'label_url'=>'posting/send',
            'submission'=>$submission,
            'req'=>$request
        ]);
    }
    public function posting_receive(Request $request){
        $submission=OrderInfo::getData($request->all(),5);
        return view('orders.submission',[
            'label'=>'مرسوله های آماده دریافت از پست' ,
            'label_url'=>'posting/receive',
            'submission'=>$submission,
            'req'=>$request
        ]);
    }
    public function delivered_shipping(Request $request){
        $submission=OrderInfo::getData($request->all(),6);
        return view('orders.submission',[
            'label'=>'مرسوله های تحویل داده شده' ,
            'label_url'=>'delivered/shipping',
            'submission'=>$submission,
            'req'=>$request
        ]);
    }
    public function submission_foctor($submission_id)
    {
        $submission_info=OrderInfo::with('getOrder.getAddress')->has('getOrder')->where('id',$submission_id)->firstOrFail();
        $order_data=new OrderData($submission_info->getOrder->getOrderInfo,$submission_info->getOrder->getProductRow,$submission_info->getOrder->user_id);
        $order_data=$order_data->getData($submission_id);
        $order_discount=DB::table('order_discount')->where('order_id',$submission_info->order_id)->get();

        return view('orders.factor',['submission_info'=>$submission_info,'order_data'=>$order_data,'order_discount'=>$order_discount]);
    }
    public function return_product($id)
    {
        $stockroom=['0'=>'انتخاب انبار']+Stockroom::pluck('name','id')->toArray();
        $orderProduct=OrderProduct::with(['getProduct','getColor','getWarranty','getSeller','getOrder'])
        ->whereHas('getOrder')->where(['send_status'=>6,'id'=>$id])->firstOrFail();
        return view('orders.return_product',['stockroom'=>$stockroom,'orderProduct'=>$orderProduct]);
    }
    public function add_return_product($id,Request $request)
    {
        $orderProduct=OrderProduct::with(['getProduct'])
        ->whereHas('getOrder')->where(['send_status'=>6,'id'=>$id])->firstOrFail();
        $count=$request->get('count',1);
        $result=OrderProduct::setReturnProduct($count,$request,$orderProduct);
        if($result=='ok')
        {
            $message='ثبت درخواست با موفقیت انجام شد';
            $name='message';
        }
        else
        {
            $message='خطا در ثبت اطلاعات مجددا تلاش نمایید';
            $name='warring';
        }
        return redirect('admin/orders/return-product')->with($name,$message);
    }
    public function return_product_list(Request $request)
    {
        $return_product_list=OrderProduct::getList($request->all());
        return view('orders.return_product_list',['return_product_list'=>$return_product_list,'req'=>$request]);
    }
    public function remove_return_product(Request $request)
    {
        $id=$request->get('id');
        $orderProduct=OrderProduct::with(['getProduct'])
        ->whereHas('getOrder')->where(['send_status'=>-1,'id'=>$id])->firstOrFail();
        $result=OrderProduct::RemoveReturnProduct($request,$orderProduct);
        if($result=='ok')
        {
            $message='ثبت درخواست با موفقیت انجام شد';
            $name='message';
        }
        else
        {
            $message='خطا در ثبت اطلاعات مجددا تلاش نمایید';
            $name='warring';
        }
        return redirect('admin/orders/return-product')->with($name,$message);
    }
}
