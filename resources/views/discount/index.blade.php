@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت کد های تخفیف','url'=>url('admin/discount')]]])
    <div class="panel">

        <div class="header">
            مدیریت کد های تخفیف

            @include('include.item_table',['count'=>$trash_discount_count,'route'=>'admin/discount','title'=>'کد تخفیف'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php use App\Lib\Jdf;$i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; $jdf=new Jdf(); ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="string" class="form-control" value="{{ $req->get('string','') }}" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>کد تخفیف</th>
                        <th>میزان تخفیف</th>
                        <th>تاریخ انقضا</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($discount as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="discount_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->code }}</td>
                            <td>
                                @if(!empty($value->amount_discount))
                                    {{ replace_number(number_format($value->amount_discount)) }} تومان
                                @else
                                    {{ replace_number($value->amount_percent) }}  درصد
                                @endif
                            </td>
                            <td>{{ $jdf->jdate('Y-n-j',$value->expiry_time) }}</td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/discount/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی کد تخفیف' onclick="restore_row('{{ url('admin/discount/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این کد تخفیف مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف کد تخفیف' onclick="del_row('{{ url('admin/discount/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این کد تخفیف مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف کد تخفیف برای همیشه' onclick="del_row('{{ url('admin/discount/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این کد تخفیف مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($discount)==0)
                        <tr>
                            <td colspan="6">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $discount->links() }}
        </div>
    </div>

@endsection
