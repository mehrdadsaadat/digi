@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[ ['title'=>'مدیریت کمیسیون ها','url'=>url('admin/commissions')] ]])
    <div class="panel">

        <div class="header">
           مدیریت کمیسیون ها
            @include('include.item_table',['count'=>$trash_commission_count,'route'=>'admin/commissions','title'=>'کمیسیون'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
              @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                <input type="hidden" name="trashed" value="true">
              @endif
              <div class="form-group">
                    {{ Form::select('cat_id',$category,$req->get('cat_id',''),['class'=>'selectpicker auto_width','data-live-search'=>'true']) }}
              </div>
              <div class="form-group">
                {{ Form::select('brand_id',$brand,$req->get('brand_id',''),['class'=>'selectpicker auto_width','data-live-search'=>'true']) }}
             </div>
              <button class="btn btn-primary">جست و جو</button>
            </form>

            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>دسته</th>
                        <th>برند</th>
                        <th>درصد کمیسیون</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($commissions as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="commissions_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->getCategory->name }}</td>
                            <td>{{ $value->getBrand->brand_name }}</td>
                            <td>{{ replace_number($value->percentage).'٪' }}</td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/commissions/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی کمیسیون' onclick="restore_row('{{ url('admin/commissions/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این کمیسیون مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف کمیسیون' onclick="del_row('{{ url('admin/commissions/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این کمیسیون مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف کمیسیون برای همیشه' onclick="del_row('{{ url('admin/commissions/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این کمیسیون مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($commissions)==0)
                        <tr>
                            <td colspan="6">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $commissions->links() }}
        </div>
    </div>

@endsection
