<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت محصولات','url'=>url('admin/products')],
         ['title'=>'ویرایش محصول','url'=>url('admin/products/'.$product->id.'/edit')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php
    use App\Product;$status=Product::ProductStatus();
    $jdf=new App\Lib\Jdf();
    ?>
    <div class="panel">

        <div class="header">
            ویرایش محصول - <?php echo e($product->title); ?>

        </div>

        <div class="panel_content">
            <?php echo Form::model($product,['url' => 'admin/products/'.$product->id,'files'=>true]); ?>


            <?php echo e(method_field('PUT')); ?>

            <div class="form-group">

                <?php echo e(Form::label('title','عنوان محصول : ')); ?>

                <?php echo e(Form::text('title',null,['class'=>'form-control total_width_input'])); ?>

                <?php if($errors->has('title')): ?>
                    <span class="has_error"><?php echo e($errors->first('title')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::textarea('tozihat',null,['class'=>'form-control ckeditor'])); ?>

            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        <?php echo e(Form::label('ename','نام لاتین محصول : ')); ?>

                        <?php echo e(Form::text('ename',null,['class'=>'form-control left'])); ?>

                    </div>

                    <div class="form-group">
                        <?php echo e(Form::label('cat_id','انتخاب دسته : ')); ?>

                        <?php echo e(Form::select('cat_id',$catList,null,['class'=>'selectpicker','data-live-search'=>'true'])); ?>

                        <?php if($errors->has('cat_id')): ?>
                            <span class="has_error"><?php echo e($errors->first('cat_id')); ?></span>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <?php echo e(Form::label('brand_id','انتخاب برند : ')); ?>

                        <?php echo e(Form::select('brand_id',$brand,null,['class'=>'selectpicker','data-live-search'=>'true'])); ?>

                        <?php if($errors->has('brand_id')): ?>
                            <span class="has_error"><?php echo e($errors->first('brand_id')); ?></span>
                        <?php endif; ?>
                    </div>



                    <div class="form-group">
                        <?php echo e(Form::label('status','وضعیت محصول : ')); ?>

                        <?php echo e(Form::select('status',$status,null,['class'=>'selectpicker','data-live-search'=>'true'])); ?>


                    </div>
                </div>
                <div class="col-md-6">

                    <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                    <div class="choice_pic_box" onclick="select_file()">

                        <span class="title">انتخاب تصویر محصول</span>
                        <img src="<?php echo e(url('files/products/'.$product->image_url)); ?>" id="output" class="pic_tag">
                    </div>
                    <?php if($errors->has('pic')): ?>
                        <span class="has_error"><?php echo e($errors->first('pic')); ?></span>
                    <?php endif; ?>
                </div>
            </div>

            <div class="form-group">
                <textarea name="reject_message" class="form-control" <?php if($product->status==-3): ?> style="display:block" <?php endif; ?> id="reject_message" placeholder="دلیل شما برای رد کردن محصول"> </textarea>
            </div>

            <p class="message_text">برچسب ها با استفاده از (،) از هم جدا شود</p>
            <div class="form-group">
                <input type="text" name="tag_list"  id="tag_list" class="form-control" placeholder="برجسب های محصول">
                <div class="btn btn-success" onclick="add_tag()">افزودن</div>
                <input type="hidden" value="<?php echo e($product->keywords); ?>" name="keywords" id="keywords">
            </div>


            <div id="tag_box">
                <?php
                $keywords=$product->keywords;
                $e=explode(',',$keywords);
                $i=1;
                ?>
                <?php if(is_array($e)): ?>
                    <?php $__currentLoopData = $e; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if(!empty($value)): ?>

                            <div class="tag_div" id="tag_div_<?php echo e($i); ?>">
                                <span  class="fa fa-remove" onclick="remove_tag(<?php echo e($i); ?>,'<?php echo e($value); ?>')"></span>
                                <?php echo e($value); ?>

                           </div>
                            <?php $i++;  ?>
                      <?php endif; ?>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
            <div style="clear:both"></div>

            <div class="form-group">

                <?php echo e(Form::label('description','توضیحات مختصر در مورد محصول (حداکثر 150 کاراکتر) : ',['style'=>'color:red;width:100%'])); ?>

                <?php echo e(Form::textarea('description',null,['class'=>'form-control','id'=>'description'])); ?>

                <?php if($errors->has('description')): ?>
                    <span class="has_error"><?php echo e($errors->first('description')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

            <div class="form-group">
                <label>
                    <input type="checkbox" <?php if($product->use_for_gift_cart=='yes'): ?> checked="checked" <?php endif; ?> name="use_for_gift_cart" id="use_for_gift_cart">
                    استفاده به عنوان کارت هدیه
                </label>
            </div>

            <div class="form-group">
                <label>
                   <?php echo e(Form::checkbox('fake')); ?>

                   کالای غیر اصل
                </label>
            </div>

            <button class="btn btn-primary">ویرایش محصول</button>

             <?php $__currentLoopData = $reject_message; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <div class="alert alert-danger reject_message">
                <div>
                    <span>ثبت شده توسط : </span>
                    <?php if($message->getUser): ?>
                      <a target="_blank" href="<?php echo e(url('admin/users/'.$message->getUser->id)); ?>"><?php echo e($message->getUser->name); ?></a>
                    <?php endif; ?>
                    <span> در تاریخ </span>
                    <span><?php echo e($jdf->jdate('d F Y',$message->time)); ?></span>
                </div>
                <div style="font-size:14px;padding-top:20px;padding-bottom:10px">
                    <?php echo e($message->tozihat); ?>

                </div>
            </div>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <?php echo Form::close(); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<script type="text/javascript" src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>
<script>
    $("#status").change(function(){
        const value=$(this).val();
        if(value==-3)
        {
            $("#reject_message").show();
        }
        else
        {
            $("#reject_message").hide();
        }
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/product/edit.blade.php ENDPATH**/ ?>