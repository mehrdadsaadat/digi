@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت صفحات','url'=>url('admin/pages')],
         ['title'=>'افزودن صفحه جدید','url'=>url('admin/pages/create')]
    ]])

    <?php
    use App\Product;$status=Product::ProductStatus();
    ?>
    <div class="panel">

        <div class="header">افزودن صفحه جدید</div>

        <div class="panel_content">
            {!! Form::open(['url' => 'admin/pages']) !!}

            <div class="form-group">

                {{ Form::label('title','عنوان صفحه : ') }}
                {{ Form::text('title',null,['class'=>'form-control total_width_input']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::textarea('content',null,['class'=>'form-control ckeditor']) }}
                @if($errors->has('content'))
                   <span class="has_error">{{ $errors->first('content') }}</span>
                @endif
            </div>


        

            <p class="message_text">برچسب ها با استفاده از (،) از هم جدا شود</p>
            <div class="form-group">
                <input type="text" name="tag_list" id="tag_list" class="form-control" placeholder="برجسب های محصول">
                <div class="btn btn-success" onclick="add_tag()">افزودن</div>
                <input type="hidden" name="keywords" id="keywords">
            </div>

            <div id="tag_box">

            </div>
            <div style="clear:both"></div>

            <div class="form-group">

                {{ Form::label('description','توضیحات مختصر در مورد محصول (حداکثر 150 کاراکتر) : ',['style'=>'color:red;width:100%']) }}
                {{ Form::textarea('description',null,['class'=>'form-control','id'=>'description']) }}
                @if($errors->has('description'))
                    <span class="has_error">{{ $errors->first('description') }}</span>
                @endif
            </div>



            <button class="btn btn-success">ثبت صفحه</button>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('footer')
<script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
@endsection
