<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <?php echo $__env->yieldContent('seo'); ?>
    <title><?php echo e(config('shop-info.shop_name')); ?></title>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <?php echo $__env->yieldContent('head'); ?>
    <link href="<?php echo e(asset('css/shop.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/main.css')); ?>" rel="stylesheet">
    <script src="<?php echo e(asset('js/app.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/js.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/main.js')); ?>" type="text/javascript"></script>
</head>
<body>

<div id="app">
    <div class="header">

        <a href="<?php echo e(url('')); ?>">
            <img src="<?php echo e(asset(config('shop-info.shop_icon'))); ?>" class="shop_logo">
        </a>
        <div class="header_row">

            <header-search></header-search>

            <div class="header_action">

                <div class="dropdown">
                    <div class="index_auth_div" role="button" data-toggle="dropdown">
                        <span>
                            <?php if(Auth::check()): ?>
                                <?php if(!empty(Auth::user()->name)): ?>
                                    <?php echo e(Auth::user()->name); ?>

                                <?php else: ?>
                                    <?php echo e(replace_number(Auth::user()->mobile)); ?>

                                <?php endif; ?>
                            <?php else: ?>
                                ورود / ثبت‌نام
                            <?php endif; ?>
                        </span>
                        <span class="fa fa-angle-down"></span>
                    </div>
                    <div class="dropdown-menu header-auth-box" aria-labelledby="dropdownMenuButton">

                        <?php if(Auth::check()): ?>
                            <?php if(Auth::user()->role_id>0 || Auth::user()->role=='admin'): ?>
                                <a class="dropdown-item admin" href="<?php echo e(url('admin')); ?>">
                                    پنل مدیریت
                                </a>
                            <?php endif; ?>
                        <?php else: ?>
                            <a class="btn btn-primary" href="<?php echo e(url('login')); ?>">ورود به دیجی کالا</a>
                            <div class="register-link">
                                <span>کاربر جدید هستید ؟ </span>
                                <a class="link" href="<?php echo e(url('register')); ?>">ثبت نام</a>
                            </div>
                            <div class="dropdown-divider"></div>

                        <?php endif; ?>
                         <a class="dropdown-item profile" href="<?php echo e(url('user/profile')); ?>">
                                پروفایل
                         </a>
                        <a class="dropdown-item orders" href="<?php echo e(url('user/profile/orders')); ?>">
                                پیگیری سفارش
                         </a>

                        <?php if(Auth::check()): ?>
                          <form method="post" action="<?php echo e(url('logout')); ?>" id="logout_form"><?php echo csrf_field(); ?></form>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item logout" >
                               خروج از حساب کاربری
                          </a>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="header_divider"></div>

                <div class="cart-header-box">
                    <div class="btn-cart" data-toggle="dropdown">
                        <span id="cart-product-count" data-counter="<?php echo e(replace_number(App\Cart::get_product_count())); ?>">سبد خرید</span>
                    </div>

                    <?php if(App\Cart::get_product_count()>0): ?>
                        <div class="dropdown cart">
                            <div class="dropdown-menu">
                                <header-cart></header-cart>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>


        </div>

    </div>

    <?php echo $__env->make('include.CategoryList',['catList'=>$calList], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="container-fluid">
        <?php echo $__env->yieldContent('content'); ?>
    </div>

    <div id="loading_box">
        <div class="loading_div">
            <img src="<?php echo e(asset('files/images/shop_icon.jpg')); ?>">
            <div class="spinner">
                <div class="b1"></div>
                <div class="b2"></div>
                <div class="b3"></div>
            </div>
        </div>
    </div>

    <footer class="c-footer">
        <nav>
            <a href="">
                <div class="c-footer_feature_item-1">تحویل اکسپرس</div>
            </a>
            <a href="">
                <div class="c-footer_feature_item-2">پشتیبانی ۲۴ ساعته</div>
            </a>
            <a href="">
                <div class="c-footer_feature_item-3">پرداخت در محل</div>
            </a>
            <a href="">
                <div class="c-footer_feature_item-4">۷ روز ضمانت بازگشت</div>
            </a>
            <a href="">
                <div class="c-footer_feature_item-5">ضمانت اصل بودن کالا</div>
            </a>
        </nav>

        <div class="row">
            <div class="col-md-3">
                <h6>راهنمای خرید از <?php echo e(env('SHOP_NAME','')); ?></h6>
                <ul>
                    <li>
                        <a href="">نحوه ثبت سفارش</a>
                    </li>
                    <li>
                        <a href="">رویه ارسال سفارش</a>
                    </li>
                    <li>
                        <a href="">شیوه های پرداخت</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h6>خدمات مشتریان</h6>
                <ul>
                    <li>
                        <a href="<?php echo e(url('faq')); ?>">پاسخ به پرسش‌های متداول</a>
                    </li>
                    <li>
                        <a href="">رویه‌های بازگرداندن کالا</a>
                    </li>
                    <li>
                        <a href="">شرایط استفاده</a>
                    </li>
                    <li>
                        <a href="">حریم خصوصی</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h6>از تخفیف‌ها و جدیدترین‌های <?php echo e(env('SHOP_NAME','')); ?> باخبر شوید</h6>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="آدرس ایمیل خود را وارد کنید">
                    <button class="btn btn-success">ارسال</button>
                </div>
            </div>
            <div class="col-md-3">
                <h6>مجوز های فروشگاه</h6>
                <div>
                    <img src="<?php echo e(url('files/images/enamad.png')); ?>">
                    <img src="<?php echo e(url('files/images/BPMLogo.png')); ?>">
                </div>
            </div>
        </div>

        <p>
            استفاده از مطالب فروشگاه اینترنتی <?php echo e(config('shop-info.shop_name')); ?> فقط برای مقاصد غیرتجاری و با ذکر منبع بلامانع است. کلیه حقوق این سایت متعلق به (فروشگاه <?php echo e(config('shop-info.shop_name')); ?>) می‌باشد.
        </p>
    </footer>
</div>


<script src="<?php echo e(asset('js/ShopVue.js')); ?>" type="text/javascript"></script>
<?php echo $__env->yieldContent('footer'); ?>

</body>
</html>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/layouts/shop.blade.php ENDPATH**/ ?>