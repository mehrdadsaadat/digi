@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت تنوع های قیمت','url'=>url('admin/product_warranties?product_id='.$product->id)]]])
    <div class="panel">

        <div class="header">
          مدیریت تنوع های قیمت {{ $product->title }}

            @include('include.item_table',['count'=>$trash_product_warranties_count,'route'=>'admin/product_warranties','title'=>'تنوع قیمت','queryString'=>['param'=>'product_id','value'=>$product->id]])
        </div>

        <div class="panel_content" >

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>


            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped" id="product_warranties">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام گارانتی</th>
                        <th>قیمت محصول</th>
                        <th>قیمت محصول برای فروش</th>
                        <th>تعداد موجودی محصول</th>
                        <th>رنگ (یا سایز)</th>
                        <th>فروشنده</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product_warranties as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="product_warranties_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->getWarranty->name }}</td>
                            <td style="min-width:160px"><div class="alert alert-success">{{ replace_number(number_format($value->price1)). 'تومان' }}</div></td>
                            <td style="min-width:160px"><span class="alert alert-warning">{{ replace_number(number_format($value->price2)). 'تومان' }}</span></td>
                            <td>{{ replace_number($value->product_number) }}</td>
                            <td style="width:140px">

                                @if($value->getColor!=null)
                                    @if($value->getColor->type==1)
                                        <span style="background:#{{ $value->getColor->code }}" class="color_td">
                                           <span style="@if($value->getColor->name!='سفید') color:white @endif">{{ $value->getColor->name }}</span>
                                        </span>
                                    @else
                                        <span class="color_td">{{ $value->getColor->name }}</span>
                                    @endif
                                @endif
                            </td>
                            <td>{{ $value->getSeller->brand_name }}
                              @if($value->status==2 || $value->getSeller->account_status!='active')
                                    <p style="color: grey;margin-top:5px">
                                        غیر فعال
                                    </p>
                              @endif
                            </td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/product_warranties/'.$value->id.'/edit?product_id='.$product->id) }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی تنوع قیمت' onclick="restore_row('{{ url('admin/product_warranties/'.$value->id.'?product_id='.$product->id) }}','{{ Session::token() }}','آیا از بازیابی این تنوع قیمت مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف تنوع قیمت' onclick="del_row('{{ url('admin/product_warranties/'.$value->id.'?product_id='.$product->id) }}','{{ Session::token() }}','آیا از حذف این تنوع قیمت مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف تنوع قیمت برای همیشه' onclick="del_row('{{ url('admin/product_warranties/'.$value->id.'?product_id='.$product->id) }}','{{ Session::token() }}','آیا از حذف این تنوع قیمت مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($product_warranties)==0)
                        <tr>
                            <td colspan="9">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $product_warranties->links() }}
        </div>
    </div>

@endsection

@section('footer')
    <script>
        $("#sidebarToggle").click();
    </script>
@endsection
