<form method="post" action="<?php echo e(url('Cart')); ?>" id="add_cart_form">
    <?php echo e(csrf_field()); ?>


    <?php

    $warranty_id=0;
    $product_price1=0;
    $product_price2=0;
    ?>

    <?php if(sizeof($product->getProductColor)>0): ?>
        <ul class="color_ul">
            <?php $__currentLoopData = $product->getProductColor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(check_has_color_in_warranty_list($product->getProductWarranty,$value->getColor->id)): ?>
                    <?php if($color_id==0): ?> <?php if(get_first_color_id($product->getProductWarranty,$value->getColor->id)) { $color_id=$value->getColor->id; } ?> <?php endif; ?>
                    <li class="color_li <?php if($color_id==$value->getColor->id): ?> active <?php endif; ?>" data="<?php echo e($value->getColor->id); ?>">
                        <label>
                            <span class="ui_variant_shape" style="background:#<?php echo e($value->getColor->code); ?>"></span>
                            <span class="color_name"><?php echo e($value->getColor->name); ?></span>
                        </label>
                    </li>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>

    <?php if(sizeof($priceItem)>0): ?>
       <div style="padding-bottom: 20px">
           <span>سایز:</span>
           <select class="selectpicker price_item" id="priceItem">
               <?php $__currentLoopData = $priceItem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <?php if(check_has_color_in_warranty_list($product->getProductWarranty,$value->id)): ?>
                       <?php if($color_id==0): ?> <?php if(get_first_color_id($product->getProductWarranty,$value->id)) { $color_id=$value->id; } ?> <?php endif; ?>
                       <option <?php if($color_id==$value->id): ?> selected="selected" <?php endif; ?> value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                   <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           </select>
       </div>
    <?php endif; ?>

    <input type="hidden" name="color_id" id="color_id" value="<?php echo e($color_id); ?>">
    <?php $brand_name='' ?>
    <p class="info_item_product">
        <?php
        $send_time=-1;
        ?>
        <span class="fa fa-check-square"></span>
        <?php $__currentLoopData = $product->getProductWarranty; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($color_id>0): ?>
                <?php if($value->color_id==$color_id && $warranty_id==0): ?>
                    <?php
                      $warranty_id=$value->id.'_'.$value->warranty_id;
                      $send_time=$value->send_time;
                      $product_price1=$value->price1;
                      $product_price2=$value->price2;
                      $brand_name=$value->getSeller->brand_name
                     ?>
                    <?php echo e($value->getWarranty->name); ?>

                     <span id="offers_time" data="<?php echo e($value->offers_last_time-time()>0 ? $value->offers_last_time-time()  : 0); ?>"></span>
                <?php endif; ?>

            <?php else: ?>
                <?php if($key==0): ?>
                    <?php
                      $send_time=$value->send_time;
                      $product_price1=$value->price1;
                      $product_price2=$value->price2;
                      $warranty_id=$value->id.'_'.$value->warranty_id;
                      $brand_name=$value->getSeller->brand_name
                    ?>
                    <?php echo e($value->getWarranty->name); ?>

                    <span id="offers_time" data="<?php echo e($value->offers_last_time-time()>0 ? $value->offers_last_time-time()  : 0); ?>"></span>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </p>
    <p class="info_item_product">
        <span class="fa fa-home"></span>
        فروشنده : <?php echo e($brand_name); ?>

    </p>
    <p class="info_item_product">
        <span class="fa fa-ambulance"></span>
        <?php if($send_time>-1): ?>

            <?php if($send_time==0): ?>
                <span>آماده ارسال</span>
                <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom"
                      title="این کالا در حال حاضر در انبار تنیس شاپ موجود ، آماده پردازش و ارسال است"></span>

            <?php else: ?>
                <span>ارسال از <?php echo e(replace_number($send_time)); ?> روز کاری آینده </span>
                <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom"
                title="این کالا در انبار فروشنده موجود است، برای ارسال باید برای مدت زمان ذکر شده منتظر بمانید"></span>
            <?php endif; ?>

        <?php endif; ?>
    </p>
    <input type="hidden" name="warranty_id" id="warranty_id" value="<?php echo e($warranty_id); ?>">
    <input type="hidden" name="product_id" id="product_id" value="<?php echo e($product->id); ?>">

    <div style="display:inline-flex;width:100%">
        <div>
            <?php if($product_price2!=$product_price1): ?>
                <del><?php echo e(replace_number(number_format($product_price1))); ?></del>
            <?php endif; ?>
            <span class="final_product_price">
                <?php echo e(replace_number(number_format($product_price2))); ?> تومان
            </span>
        </div>

        <?php if($product_price2!=$product_price1): ?>

            <div class="product_discount" data-title="تخفیف">
                <?php
                $a=($product_price2/$product_price1)*100;
                $a=100-$a;
                $a=round($a);
                ?>
               <?php echo e(replace_number($a)); ?> ٪
            </div>

        <?php endif; ?>
    </div>
</form>
<?php /**PATH C:\xampp\htdocs\digikala\resources\views/include/warranty.blade.php ENDPATH**/ ?>