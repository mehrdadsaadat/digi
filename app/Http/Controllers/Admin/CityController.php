<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Http\Requests\CityRequest;
use App\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends CustomController
{
    protected  $model='City';
    protected  $title='شهر';
    protected $route_params='city';
    public function index(Request $request)
    {
        $city=City::getData($request->all());
        $trash_city_count=City::onlyTrashed()->count();
        return view('city.index',['city'=>$city,'trash_city_count'=>$trash_city_count,'req'=>$request]);

    }
    public function create()
    {
        $province=Province::get()->pluck('name','id')->toArray();
        return view('city.create',['province'=>$province]);
    }
    public function store(CityRequest $request)
    {
        $city=new City($request->all());
        $city->saveOrFail();
        return redirect('admin/city')->with('message','ثبت  شهر با موفقیت انجام شد');
    }
    public function edit($id)
    {
        $province=Province::get()->pluck('name','id')->toArray();
        $city=City::findOrFail($id);
        return view('city.edit',['province'=>$province,'city'=>$city]);
    }
    public function update($id,CityRequest $request)
    {
        $city=City::findOrFail($id);
        $city->update($request->all());
        return redirect('admin/city')->with('message','ویرایش شهر با موفقیت انجام شد');
    }
}
