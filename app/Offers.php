<?php
namespace App;
use App\Jobs\IncredibleOffers;
use DB;
use Validator;
class Offers
{
    public function add($request,$ProductWarranty)
    {
        $Validator=Validator::make($request->all(),[
            'price1'=>'required|numeric',
            'price2'=>'required|numeric',
            'product_number'=>'required|numeric',
            'product_number_cart'=>'required|numeric',
            'date1'=>'required',
            'date2'=>'required'
        ],[],[
            'price1'=>'هزینه محصول',
            'price2'=>'هزینه محصول برای فروش',
            'product_number'=>'تعداد موجودی محصول',
            'product_number_cart'=>'تعداد قابل سفارش در سبد خرید',
            'date1'=>'تاریخ شروع',
            'date2'=>'تاریخ پایان',

        ]);
        if($Validator->fails())
        {
             return $Validator->errors();
        }
        else{
            $date1=$request->get('date1');
            $date2=$request->get('date2');
            $offers_first_time=getTimestamp($date1,'first');
            $offers_last_time=getTimestamp($date2,'last');


            $row=DB::table('old_price')->where('warranty_id',$ProductWarranty->id)->first();
            if(!$row)
            {
                $this->addNewPriceRow($ProductWarranty,$request);
            }
            else
            {
                $this->updatePriceRow($row,$ProductWarranty,$request);
            }

            $ProductWarranty->offers_first_date=$date1;
            $ProductWarranty->offers_last_date=$date2;
            $ProductWarranty->offers_first_time=$offers_first_time;
            $ProductWarranty->offers_last_time=$offers_last_time;
            $ProductWarranty->offers=1;
            if($ProductWarranty->update($request->all()))
            {
                $second=$offers_last_time-time()+1;
                IncredibleOffers::dispatch($ProductWarranty->id)->delay(now()->addSecond($second));
                add_min_product_price($ProductWarranty);
                $product=Product::where('id',$ProductWarranty->product_id)->select(['price','id','status'])->first();
                update_product_price($product);
                return 'ok';
            }
            else
            {
                return ['error'=>true];
            }
        }

    }
    public function addNewPriceRow($ProductWarranty,$request)
    {
        $n=$ProductWarranty->product_number-$request->get('product_number');
        if($n<0){
            $n=0;
        }
        $insert_id=DB::table('old_price')
            ->insertGetId([
                'warranty_id'=>$ProductWarranty->id,
                'price1'=>$ProductWarranty->price1,
                'price2'=>$ProductWarranty->price2,
                'product_number'=>$n,
                'product_number_cart'=>$ProductWarranty->product_number_cart,
                'Number_product_sales'=>$request->get('product_number'),
            ]);
    }
    public function updatePriceRow($row,$ProductWarranty,$request)
    {
        $n=$row->product_number;
        if($row->Number_product_sales>$request->get('product_number'))
        {
            $n1=$row->Number_product_sales-$request->get('product_number');
            $n=$n+$n1;
        }
        else
        {
            $n1=$request->get('product_number')-$row->Number_product_sales;
            $n=$n-$n1;
        }
        DB::table('old_price')->where(['warranty_id'=>$ProductWarranty->id])
            ->update([
                'Number_product_sales'=>$request->get('product_number'),
                'product_number'=>$n
            ]);
    }
    public function remove($ProductWarranty)
    {
        $old_price=DB::table('old_price')->where('warranty_id',$ProductWarranty->id)->first();
        if($old_price)
        {
            $ProductWarranty->price1=$old_price->price1;
            $ProductWarranty->price2=$old_price->price2;
            if($old_price->product_number>0)
            {
                $ProductWarranty->product_number= $ProductWarranty->product_number+$old_price->product_number;
            }
        }
        $ProductWarranty->offers=0;
        $ProductWarranty->update();
        DB::table('old_price')->where('warranty_id',$ProductWarranty->id)->delete();

        add_min_product_price($ProductWarranty);
        $product=Product::where('id',$ProductWarranty->product_id)->select(['price','id','status'])->first();
        update_product_price($product);

        return $ProductWarranty;
    }
}
