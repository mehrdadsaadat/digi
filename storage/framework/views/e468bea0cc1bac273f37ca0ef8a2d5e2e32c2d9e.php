<?php $__env->startSection('content'); ?>

    <div class="row">

        <div class="col-md-3">
            <?php echo $__env->make('include.user_panel_menu',['active'=>'orders'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
        <div class="col-md-9" style="padding-right: 0px">



                <?php use App\Lib\Jdf;use App\Order;$Jdf=new Jdf(); $OrderStatus=Order::OrderStatus() ?>

                <div class="profile_menu">
            <span class="profile_menu_title">
                جزییات سفارش :‌ <?php echo e(replace_number($order->order_id)); ?>

            </span>

                    <span class="profile_menu_title" style="padding: 0px 20px 0px;font-size:12px">
                تاریخ ثبت سفارش :‌ <?php echo e($Jdf->jdate(' j F Y',$order->created_at)); ?>

            </span>


                    <?php if(isset($error_payment)): ?>
                        <div class="alert alert-warning payemnt_warning">
                            <span>پرداخت انجام نشد</span>
                            در صورت کسر پول از حساب شما تا ۱۵ دقیقه دیگه پول به حساب شما برمیگردد
                        </div>
                    <?php endif; ?>

                    <table class="table table-bordered order_table_info">

                        <tr>
                            <td>
                                تحویل گیرنده:
                                <span><?php echo e($order->getAddress->name); ?></span>
                            </td>
                            <td>
                                شماره تماس تحویل گیرنده:
                                <span><?php echo e(replace_number($order->getAddress->mobile )); ?></span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                آدرس تحویل گیرنده:
                                <span><?php echo e($order->getAddress->getProvince->name.' '. $order->getAddress->getCity->name.' '. $order->getAddress->address); ?></span>
                            </td>
                            <td>
                                تعداد مرسوله:
                                <span><?php echo e(replace_number(sizeof($order->getOrderInfo))); ?></span>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                مبلغ قابل پرداخت:
                                <span><?php echo e(replace_number(number_format($order->price))); ?> تومان</span>
                            </td>
                            <td>
                                مبلغ کل :
                                <span><?php echo e(replace_number(number_format($order->total_price))); ?> تومان</span>
                            </td>
                        </tr>

                        <?php if(!empty($order->gift_value) && $order->gift_value>0): ?>
                            <tr>
                                <td>
                                    مبلغ کارت هدیه:
                                    <span><?php echo e(replace_number(number_format($order->gift_value))); ?> تومان</span>
                                </td>
                                <td>
                                    کد کارت هدیه:
                                    <span><?php echo e($order->getGiftCart->code); ?> </span>
                                </td>
                            </tr>
                        <?php endif; ?>

                        <?php if(!empty($order->discount_value) && $order->discount_value>0): ?>
                            <tr>
                                <td>
                                    مبلغ کد تخفیف:
                                    <span><?php echo e(replace_number(number_format($order->discount_value))); ?> تومان</span>
                                </td>
                                <td>
                                    کد تخفیف:
                                    <span><?php echo e($order->discount_code); ?> </span>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </table>

                    <?php $__currentLoopData = $order->getOrderInfo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="order_info_div">

                            <div class="header">
                                <?php echo e(\App\Order::getOrderStatus($value['send_status'],$OrderStatus)); ?>

                            </div>


                            <?php if($value['send_status']==0 &&  $order->pay_status=='ok'): ?> <?php $value['send_status']=1 ?> <?php endif; ?>
                            <div class="swiper-container order_steps">
                                <div class="swiper-wrapper">
                                    <?php $__currentLoopData = $OrderStatus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($key>-1): ?>
                                            <div class="swiper-slide">
                                                <div class="step_div <?php if($value['send_status']<$key): ?> step_inactive <?php endif; ?>">
                                                    <img src="<?php echo e(url('files/images/step'.$key.'.svg')); ?>" <?php if($key==6): ?>  style="width:65%" <?php endif; ?>/>
                                                    <span><?php echo e($status); ?></span>
                                                </div>

                                                <?php if($key<6): ?>
                                                    <hr class="<?php if($value['send_status']>$key): ?> hr_active <?php endif; ?>">
                                                <?php else: ?>
                                                    <div style="min-width:66px"></div>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>



                            <table class="table table-bordered order_table_info">

                                <tr>
                                    <td>
                                        کد مرسوله:
                                        <span><?php echo e(replace_number($value['id'])); ?></span>
                                    </td>
                                    <td>
                                        زمان تحویل:
                                        <span><?php echo e($value['delivery_order_interval']); ?></span>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        نحوه ارسال:
                                        <span>پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                                    </td>
                                    <td>
                                        هزینه ارسال:
                                        <span>
                                    <?php if($value['send_order_amount']==0): ?>
                                                رایگان
                                            <?php else: ?>
                                                <?php echo e(replace_number(number_format($value['send_order_amount']))); ?> تومان
                                            <?php endif; ?>
                                </span>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" style="text-align: center">
                                        مبلغ این مرسوله:
                                        <span>
                                    <?php echo e(replace_number(number_format($order_data['order_row_amount'][$value->id]))); ?> تومان
                                </span>
                                    </td>
                                </tr>

                            </table>

                            <table class="table product_list_data">
                                <tr>
                                    <th>نام محصول</th>
                                    <th>تعداد</th>
                                    <th>قیمت واحد</th>
                                    <th>قیمت کل</th>
                                    <th>تخفیف</th>
                                    <th>قیمت نهایی</th>
                                </tr>
                                <?php $__currentLoopData = $order_data['row_data'][$value->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr>
                                        <td class="product__info">
                                            <div>
                                                <img src="<?php echo e(url('files/thumbnails/'.$product['image_url'])); ?>" />
                                                <ul>
                                                    <li class="title">
                                                        <?php echo e($product['title']); ?>

                                                    </li>
                                                    <li>
                                                        <span>فروشنده :‌ ‌</span>
                                                        <span><?php echo e($product['seller']); ?></span>
                                                    </li>
                                                    <?php if($product['color_id']>0): ?>
                                                        <li>
                                                            <?php if($product['color_type']==1): ?>
                                                                <span> رنگ :‌</span>
                                                            <?php else: ?>
                                                                <span> سایز :‌</span>
                                                            <?php endif; ?>
                                                            <span><?php echo e($product['color_name']); ?></span>
                                                        </li>
                                                    <?php endif; ?>
                                                    <li>
                                                        <span>گارانتی :‌ ‌</span>
                                                        <span><?php echo e($product['warranty_name']); ?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <?php echo e(replace_number($product['product_count'])); ?>

                                        </td>
                                        <td>
                                            <?php echo e(replace_number(number_format($product['product_price1']))); ?> تومان
                                        </td>
                                        <td>
                                            <?php echo e(replace_number(number_format($product['product_price1']*$product['product_count']))); ?> تومان
                                        </td>
                                        <td>
                                            <?php
                                            $discount=(($product['product_price1']*$product['product_count'])-($product['product_price2']*$product['product_count']));
                                            ?>
                                            <?php echo e(replace_number(number_format($discount))); ?> تومان

                                        </td>
                                        <td>
                                            <?php echo e(replace_number(number_format($product['product_price2']*$product['product_count']))); ?> تومان

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </table>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/swiper.min.css')); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(asset('js/swiper.min.js')); ?>"></script>
    <script>
        const swiper=new Swiper('.swiper-container',{
            slidesPerView:5,
            spaceBetween:0,
            navigation:{
                nextEl:'.swiper-button-next',
                prevEl:'.swiper-button-prev'
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.shop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/userPanel/show_order.blade.php ENDPATH**/ ?>