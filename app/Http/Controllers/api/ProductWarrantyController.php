<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Warranty;
use App\ProductColor;
use App\ProductWarranty;
use DB;
use App\Product;
use App\SellerProduct;
class ProductWarrantyController extends Controller
{
    public function index(Request $request,$id)
    {
        $seller_id=$request->user()->id;
        $product=Product::select('title')->where('id',$id)->withTrashed()->first();
        if($product)
        {
            $product_warranties=ProductWarranty::getData($request->all(),$seller_id,$id);
            $trash_product_warranties_count=ProductWarranty::where(['product_id'=>$id])
            ->onlyTrashed()->count();

            return [
                'product_warranties'=>$product_warranties,
                'trash_product_warranties_count'=>$trash_product_warranties_count,
                'title'=>$product->title,
            ];
        }
        else{
            return 'error';
        }

    }
    public function create(Request $request,$id)
    {
        $seller_id=$request->user()->id;
        $ProductWarranty=true;
        $product=Product::select(['id','title','cat_id'])->where('id',$id)->whereIn('status',[0,1])->first();
        $warranty=Warranty::orderBy('id','DESC')->pluck('name','id')->toArray();
        if($request->get('warranty_id'))
        {
            $ProductWarranty=ProductWarranty::where([
                'seller_id'=>$seller_id,
                'product_id'=>$id,
                'id'=>$request->get('warranty_id')
            ])->first();
        }
        if($product && $ProductWarranty)
        {
            $colors=ProductWarranty::getPriceItem($product);
            $array=array();
            $array['warranty']=$warranty;
            $array['colors']=$colors;
            $array['title']=$product->title;
            if($request->get('warranty_id')){
                $array['product_warranty']=$ProductWarranty;
            }
            return $array;
        }
        else{
            return 'redirect';
        }
    }
    public function store(Request $request,$id)
    {
        $color_id=$request->get('color_id');
        $e=explode('color_',$color_id);
        $color_id=sizeof($e)==2 ? $e[1] : $e[0];

        $product=Product::select(['id','title','cat_id','brand_id'])->where('id',$id)->whereIn('status',[0,1])->first();
        if($product){
            $seller_id=$request->user()->id;
            $check=ProductWarranty::where([
                'seller_id'=>$seller_id,
                'warranty_id'=>$request->get('warranty_id'),
                'product_id'=>$id,
                'color_id'=>$color_id
            ])->first();
            if(!$check)
            {
                $warranty=new ProductWarranty($request->all());
                $warranty->product_id=$id;
                $warranty->seller_id=$seller_id;
                $warranty->saveOrFail();
                add_min_product_price($warranty);
                update_product_price($product);

                SellerProduct::firstOrCreate([
                    'product_id'=>$id,
                    'seller_id'=>$seller_id,
                    'cat_id'=>$product->cat_id,
                    'brand_id'=>$product->brand_id,
                ]);
                if(sizeof($e)==2){
                    add_product_color($request->get('product_id'),$color_id);
                }
                return 'ok';
            }
            else{
                return 'repetitive';
            }
        }
        else{
            return 'error';
        }
    }
    public function update(Request $request,$id)
    {
        $seller_id=$request->user()->id;
        $product_id=$request->get('product_id');

        $color_id=$request->get('color_id');
        $e=explode('color_',$color_id);
        $color_id=sizeof($e)==2 ? $e[1] : $e[0];
        $data=$request->all();
        $data['color_id']=$color_id;

        $product=Product::where(['id'=>$product_id])->whereIn('status',[0,1])->first();
        $product_warranties=ProductWarranty::where(['id'=>$id,'seller_id'=>$seller_id,'product_id'=>$product_id])->first();
        if($product_warranties && $product){
            $product_warranties->update($request->all());
            update_product_price($product);
            add_min_product_price($product_warranties);
            if(sizeof($e)==2){
                add_product_color($request->get('product_id'),$color_id);
            }
            return 'ok';
        }
        else{
            return 'error';
        }

    }
    public function destroy(Request $request,$id)
    {
        $seller_id=$request->user()->id;
        $row=ProductWarranty::where(['seller_id'=>$seller_id,'id'=>$id])->withTrashed()->first();
        if($row)
        {
            $product_id=$row->product_id;
            if($row->trashed())
            {
                $row->restore();
            }
            else{
                $row->delete();
            }

            $product_warranties=ProductWarranty::getData($request->all(),$seller_id,$id);
            $trash_product_warranties_count=ProductWarranty::where(['product_id'=>$product_id])
                ->onlyTrashed()->count();
            return [
                'product_warranties'=>$product_warranties,
                'trash_product_warranties_count'=>$trash_product_warranties_count,
            ];
        }
        else{
            return 'error';
        }
    }

}
