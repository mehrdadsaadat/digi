@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت تنوع های قیمت','url'=>url('admin/product_warranties?product_id='.$product->id)],
         ['title'=>'ویرایش تنوع قیمت','url'=>url('admin/product_warranties/'.$product_warranties->id.'/edit?product_id='.$product->id)]
    ]])

    <div class="panel">

        <div class="header">
            ویرایش تنوع قیمت - {{ $product->title }}
        </div>

        <div class="panel_content">


            @include('include.warring')
            {!! Form::model($product_warranties,['url' => 'admin/product_warranties/'.$product_warranties->id.'?product_id='.$product->id]) !!}

            {{ method_field('PUT') }}

            <div class="form-group">
                {{ Form::label('warranty_id','انتخاب گارانتی : ') }}
                {{ Form::select('warranty_id',$warranty,null,['class'=>'selectpicker auto_width','data-live-search'=>'true']) }}
            </div>

            @if(sizeof($colors)>0)
                <div class="form-group">
                    <label>
                        @if($colors[0]->type==1)
                            انتخاب رنگ :
                        @else
                            انتخاب سایز :
                        @endif
                    </label>
                    <select class="selectpicker auto_width" data-live-search="true"  name="color_id">
                        @foreach($colors as $key=>$value)
                            @if($value->type==1)
                                <option @if($product_warranties->color_id==$value->id) selected="selected" @endif value="color_{{ $value->id }}" data-content="<span  style='background:#{{ $value->code }}; @if($value->name=='سفید') color:#000 @endif' class='color_option'>{{ $value->name }}</span>"></option>
                            @else
                                <option @if($product_warranties->color_id==$value->id) selected="selected" @endif value="{{ $value->id }}">{{ $value->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            @endif


            <div class="form-group">

                {{ Form::label('price1','قیمت محصول : ') }}
                {{ Form::text('price1',null,['class'=>'form-control left price_input']) }}
                @if($errors->has('price1'))
                    <span class="has_error">{{ $errors->first('price1') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('price2','قیمت محصول برای فروش  : ') }}
                {{ Form::text('price2',null,['class'=>'form-control left discount_price_input']) }}
                @if($errors->has('price2'))
                    <span class="has_error">{{ $errors->first('price2') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('product_number','تعداد موجودی محصول  : ') }}
                {{ Form::text('product_number',null,['class'=>'form-control left product_number']) }}
                @if($errors->has('product_number'))
                    <span class="has_error">{{ $errors->first('product_number') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('product_number_cart','تعداد سفارش در سبد خرید  : ') }}
                {{ Form::text('product_number_cart',null,['class'=>'form-control left product_number_cart']) }}
                @if($errors->has('product_number_cart'))
                    <span class="has_error">{{ $errors->first('product_number_cart') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('send_time','زمان آماده سازی محصول  : ') }}
                {{ Form::text('send_time',null,['class'=>'form-control left']) }}
                @if($errors->has('send_time'))
                    <span class="has_error">{{ $errors->first('send_time') }}</span>
                @endif
            </div>

            <button class="btn btn-primary">ویرایش تنوع قیمت</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer')
<script type="text/javascript" src="{{ asset('js/cleave.min.js') }}"></script>
<script>

    var cleave1 = new Cleave('.price_input', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave2 = new Cleave('.discount_price_input', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave3 = new Cleave('.discount_price_input', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave4 = new Cleave('.product_number', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave5 = new Cleave('.product_number_cart', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
    var cleave6 = new Cleave('#send_time', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
</script>
@endsection
