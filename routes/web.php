<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Cart;
use App\OrderingTime;
use App\ProductColor;
use Illuminate\Http\Request;

Route::prefix('admin')->middleware(['auth','admin'])->group(function (){

    Route::get('/','Admin\AdminController@index')->name('admin');
    Route::get('/panel','Admin\AdminController@author_panel')->name('author_panel');
    Route::get('/403','Admin\AdminController@error403')->name('error403');
    Route::get('filemanager','Admin\AdminController@filemanager')->name('filemanager');
    Route::get('payments', 'Admin\AdminController@payments')->name('payments');
    create_crud_route('category','CategoryController');
    create_crud_route('brands','BrandController');
    create_crud_route('colors','ColorController');
    create_crud_route('products','ProductController',[]);
    create_crud_route('warranties','WarrantyController');
    create_crud_route('product_warranties','ProductWarrantyController');
    create_crud_route('sliders','SliderController');
    create_crud_route('province','ProvinceController');
    create_crud_route('city','CityController');
    create_crud_route('discount','DiscountController');
    create_crud_route('commissions','CommissionController');
    create_crud_route('pages','PageController');

    create_crud_route('category-common-question','CategoryCommonQuestionController');
    create_crud_route('common-question','CommonQuestionController');

    create_crud_route('sellers/{seller_id}/messages', 'UserMessageController',['edit','update'],['names'=>'seller.message']);
    Route::put('sellers/{seller_id}/messages/{message_id}', 'Admin\UserMessageController@addAnswer');


    create_crud_route('users/{user_id}/messages', 'UserMessageController',['edit','update'],['names'=>'user.message']);
    Route::put('users/{user_id}/messages/{message_id}', 'Admin\UserMessageController@addAnswer')->name('user.message.answer');


    create_crud_route('messages', 'MessageController',['edit','update','create','store']);
    Route::put('messages/addAnswer/{id}','Admin\MessageController@addAnswer');

    create_crud_route('comments','CommentController',['show','create','store','edit','update']);
    Route::post('comment/change_status','Admin\CommentController@change_status')->name('comment_change_status');

    create_crud_route('questions','QuestionController',['show','create','store','edit','update']);
    Route::post('question/change_status','Admin\QuestionController@change_status')->name('change_question_status');
    Route::post('question/addAnswer/{id}','Admin\QuestionController@addAnswer')->name('questions_addAnswer');

    Route::get('product/review/primary','Admin\ReviewController@primary');
    Route::post('product/review/primary','Admin\ReviewController@add_primary_content');
    create_crud_route('product/review','ReviewController');

    create_crud_route('userRole','UserRoleController');
    Route::get('userRole/access/{role_id}','Admin\UserRoleController@access');
    Route::post('userRole/access/{role_id}','Admin\UserRoleController@add_access');
    create_crud_route('users','UsersController',[]);

    create_crud_route('sellers', 'SellerController',['create','store']);
    Route::get('seller/pay/export', 'Admin\SellerController@export');
    Route::get('seller/pay/import', 'Admin\SellerController@import');
    Route::post('seller/pay/import', 'Admin\SellerController@add_payment');

    create_crud_route('orders','OrdersController',['create','store','edit','update']);
    Route::get('orders/submission','Admin\OrdersController@submission')->name('orders.submissions');
    Route::get('orders/submission/approved','Admin\OrdersController@submission_approved')->name('orders.submission_approved');
    Route::get('orders/submission/items/today','Admin\OrdersController@items_today')->name('orders.items_today');
    Route::get('orders/submission/ready','Admin\OrdersController@submission_ready')->name('orders.submissions_ready');
    Route::get('orders/posting/send','Admin\OrdersController@posting_send')->name('orders.postings_sent');
    Route::get('orders/posting/receive','Admin\OrdersController@posting_receive')->name('orders.postings_receive');
    Route::get('orders/delivered/shipping','Admin\OrdersController@delivered_shipping')->name('orders.delivered_shipping');
    Route::get('orders/submission/{submission_id}','Admin\OrdersController@submission_info')->name('submissions_info');
    Route::get('orders/return-product','Admin\OrdersController@return_product_list')->name('orders.return-product-list');
    Route::post('orders/return-product','Admin\OrdersController@remove_return_product')->name('orders.remove-return-product');
    Route::post('order/change_status','Admin\OrdersController@change_status')->name('order.change_status');
    Route::get('orders/submission/factor/{submission_id}','Admin\OrdersController@submission_foctor')->name('submissions_factor');
    Route::get('orders/return-product/{id}','Admin\OrdersController@return_product')->name('return-product');
    Route::post('orders/return-product/{id}','Admin\OrdersController@add_return_product')->name('return-product');

    Route::get('products/gallery/{id}','Admin\ProductController@gallery');
    Route::post('products/gallery_upload/{id}','Admin\ProductController@gallery_upload');
    Route::delete('products/gallery/{id}','Admin\ProductController@removeImageGallery');
    Route::post('products/change_images_status/{id}','Admin\ProductController@change_images_status');
    Route::get('products/{id}/items','Admin\ProductController@items');
    Route::post('products/{id}/items','Admin\ProductController@add_items');
    Route::get('products/{id}/filters','Admin\ProductController@filters');
    Route::post('products/{id}/filters','Admin\ProductController@add_filters');

    Route::get('category/{id}/items','Admin\ItemController@items');
    Route::post('category/{id}/item','Admin\ItemController@add_items');
    Route::delete('category/items/{id}','Admin\ItemController@destroy');

    Route::get('category/{id}/filters','Admin\FilterController@filters');
    Route::post('category/{id}/filters','Admin\FilterController@add_filters');
    Route::delete('category/filters/{id}','Admin\FilterController@destroy');

    Route::get('category/{id}/price_variation','Admin\PriceVariationController@priceVariation');
    Route::post('category/{id}/price_variation','Admin\PriceVariationController@add_price_variation');
    Route::delete('category/price_variation/{id}','Admin\PriceVariationController@destroy');

    Route::get('incredible-offers','Admin\AdminController@incredible_offers')->name('incredible_offers');
    Route::get('ajax/getWarranty','Admin\AdminController@getWarranty')->name('ajaxGetWarranty');
    Route::post('add_incredible_offers/{warranty_id}','Admin\AdminController@add_incredible_offers')->name('add_incredible_offers');
    Route::post('remove_incredible_offers/{warranty_id}','Admin\AdminController@remove_incredible_offers')->name('remove_incredible_offers');

    Route::match(['get','post'],'setting/send-order-price','Admin\SettingController@send_order_price');
    Route::match(['get','post'],'setting/shop','Admin\SettingController@shop');
    Route::match(['get','post'],'setting/payment-gateway','Admin\SettingController@payment_gateway');

    Route::get('stockroom/input','Admin\StockroomController@input')->name('stockroom.input');
    Route::get('stockroom/input/{id}','Admin\StockroomController@show_input')->name('stockroom.show_input');
    Route::post('stockroom/add_product','Admin\StockroomController@add_product')->name('stockroom.add_product');
    Route::get('stockroom/add/input','Admin\StockroomController@add_input')->name('stockroom.add_input');

    Route::get('stockroom/output','Admin\StockroomController@output')->name('stockroom.output');
    Route::get('stockroom/output/{id}','Admin\StockroomController@show_output')->name('stockroom.show_output');
    Route::post('stockroom/add_output','Admin\StockroomController@add_product')->name('stockroom.add_product');
    Route::get('stockroom/add/output','Admin\StockroomController@add_output')->name('stockroom.add_output');
    Route::get('factor/{id}/input','Admin\StockroomController@input_factor')->name('stockroom.input_factor');
    Route::get('factor/{id}/output','Admin\StockroomController@output_factor')->name('stockroom.output_factor');

    create_crud_route('packages','PackageController',['create','store','edit','update']);
    Route::get('package/getContent/{id}','Admin\PackageController@getContent');
    Route::post('package/stockroom/add_product','Admin\PackageController@add_product');

    create_crud_route('stockrooms','StockroomController',[]);
    Route::get('stockroom/getProductWarrnty','Admin\StockroomController@getProductWarrnty')->name('get_product_warrnty');
    Route::get('stockroom/getInventory','Admin\StockroomController@getInventory')->name('get_inventory');
    Route::get('report/sale','Admin\AdminController@sale_report')->name('sale_report');
    Route::get('shop/get_sale_report','Admin\AdminController@get_sale_report')->name('get_sale_report');
    Route::get('product/get_sale_report','Admin\ProductController@get_sale_report');
});



Route::get('/', 'SiteController@index');
Route::get('sitemap-category.xml','SiteController@sitemap_category');
Route::get('sitemap.xml','SiteController@sitemap');
Route::get('products/{page}/sitemap-products.xml','SiteController@sitemap_products');
Route::get('/faq', 'SiteController@faq');

Route::get('search','SiteController@search_product');
Route::get('getProduct/search','SiteController@get_search_product');

Route::get(config('shop-info.login_url'),'Admin\AdminController@admin_login_form')->middleware('guest');
Route::post('Cart','SiteController@add_cart');
Route::get('Cart','SiteController@show_cart');
Route::get('shipping','ShoppingController@shipping');
Route::post('get_compare_products','SiteController@get_compare_products');
Route::post('site/getCatBrand','SiteController@getCatBrand');
Route::get('shipping/getSendData/{city_id}','ShoppingController@getSendData');
Route::post('payment','ShoppingController@payment');
Route::match(['get','post'],'order/verify','ShoppingController@verify');
Route::get('order/verify/{order_id}','ShoppingController@order_verify')->middleware('auth');
//Route::get('order/verify2','ShoppingController@verify2');
Route::get('order/payment','ShoppingController@order_payment');
Route::post('site/check_gift_cart','ShoppingController@check_gift_cart');
Route::post('site/check_discount_code','ShoppingController@check_discount_code');
Route::get('product/comment/{product_id}','SiteController@comment_form')->middleware('auth');
Route::post('product/comment/{product_id}','SiteController@add_comment')->middleware('auth');
Route::get('site/getComment','ApiController@getComment');
Route::get('product/{product_id}/{product_url}','SiteController@show_product');
Route::get('product/{product_id}','SiteController@show_product');
Route::get('/confirm','SiteController@confirm')->middleware('guest');
Route::get('/confirmphone','SiteController@confirmphone')->middleware('auth');
Route::post('active_account','SiteController@active_account')->middleware('guest')->name('active_account');
Route::post('changeMobileNumber','SiteController@changeMobileNumber')->middleware('auth');
Route::get('main/{cat_url}','SiteController@show_child_cat_list');
Route::get('search/{cat_url}','SiteController@cat_product');
Route::get('getProduct/search/{cat_url}','SiteController@get_cat_product');
Route::get('brand/{brand_name}','SiteController@brand_product');
Route::get('getProduct/brand/{brand_name}','SiteController@get_brand_product');
Route::get('compare/{productId1}','SiteController@compare');
Route::get('compare/{productId1}/{productId2}','SiteController@compare');
Route::get('compare/{productId1}/{productId2}/{productId3}','SiteController@compare');
Route::get('compare/{productId1}/{productId2}/{productId3}/{productId4}','SiteController@compare');
Route::get('page/{page}','SiteController@page');
Route::get('seller/{seller_id}', 'SiteController@seller_product');
Route::get('getProduct/seller/{seller_id}', 'SiteController@get_seller_product');
Route::get('faq/category/{id}','SiteController@faq_category');
Route::get('faq/question/{id}','SiteController@faq_question');
//ajax
Route::post('site/change_color','SiteController@change_color');
Route::post('ajax/resend','SiteController@resend');
Route::post('site/cart/remove_product','SiteController@remove_product');
Route::post('site/cart/change_product_cart','SiteController@change_product_cart');
Route::get('site/CartProductData','SiteController@CartProductData');
Route::get('site/get_question/{product_id}','SiteController@get_question');
Route::post('site/share_product','SiteController@share_product');
Auth::routes();
Route::get('password/confirm','Auth\ForgotPasswordController@confirm')->middleware('guest');
Route::post('password/confirm','Auth\ForgotPasswordController@check_confirm_code')->middleware('guest');
Route::post('/vue_login','Auth\LoginController@vue_login')->middleware('guest');
Route::prefix('user')->middleware(['auth'])->group(function (){
    Route::get('getFavoriteList','UserController@getFavoriteList');
    Route::post('addQestion','UserController@addQestion');
    Route::get('getAddreass','UserController@getAddreass');
    Route::get('profile','UserPanelController@profile');
    Route::post('/addAddress','UserController@addAddress');
    Route::post('/add_favorite','UserController@add_favorite');
    Route::post('/like','ApiController@like');
    Route::post('/dislike','ApiController@dislike');
    Route::delete('/removeAddress/{address_id}','UserController@removeAddress');
    Route::get('/profile/gift-cart','UserPanelController@gift_cart');
    Route::get('profile/orders','UserPanelController@orders');
    Route::get('profile/orders/{order_id}','UserPanelController@show_order');
    Route::get('profile/additional-info','UserPanelController@additional_info');
    Route::post('profile/additional-info','UserPanelController@save_additional_info');
    Route::get('profile/personal-info','UserPanelController@personal_info');
    Route::get('profile/address','UserPanelController@address');
    Route::get('profile/favorite','UserPanelController@favorite');
    Route::get('profile/comments','UserPanelController@comments');
    Route::post('favorate/removeProductOfList','UserController@removeProductOfList');
    Route::get('profile/messages', 'UserPanelController@getUserMessage');
    Route::post('profile/messages', 'UserPanelController@addMessage');
    Route::get('profile/messages/create', 'UserPanelController@addMessageForm');
    Route::get('profile/messages/{id}','UserPanelController@showMessageContent');
    Route::put('profile/messages/addAnswer/{id}', 'UserPanelController@addAnswer');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::get('test',function (Request $request){

});

