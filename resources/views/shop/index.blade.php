@extends('layouts.shop')

@section('content')
    <div class="row slider">
        <div class="col-2">
            <div>
                <a href="{{ url('') }}" >
                    <img src="{{ url('files/images/1000007524.jpg') }}"   @if(sizeof($incredible_offers)==0) style="height:155px" @endif class="index-pic">
                </a>
                <a href="{{ url('') }}" >
                    <img src="{{ url('files/images/1000007568.jpg') }}" @if(sizeof($incredible_offers)==0) style="height:155px" @endif class="index-pic">
                </a>
              @if(sizeof($incredible_offers)>0)
                <a href="{{ url('') }}">
                    <img src="{{ url('files/images/1000005397.jpg') }}" class="index-pic">
                </a>
                <a href="{{ url('') }}">
                    <img src="{{ url('files/images/1000005397.jpg') }}" class="index-pic">
                </a>
              @endif
            </div>
        </div>
        <div class="col-10">
            @if(sizeof($sliders)>0)
                <div class="slider_box">
                    <div style="position:relative" >
                        @foreach($sliders as $key=>$value)
                            <div class="slide_div an" id="slider_img_{{ $key }}" @if($key==0) style="display:block" @endif>
                                <a href="{{ $value->url }}" style='background-image:url("<?= url('files/slider/'.$value->image_url) ?>")'></a>
                            </div>
                        @endforeach
                    </div>


                    <div id="right-slide" onclick="previous()"></div>
                    <div id="left-slide" onclick="next()"></div>
                </div>
                <div class="slider_box_footer">
                    <div class="slider_bullet_div">
                        @for($i=0;$i<sizeof($sliders);$i++)
                            <span id="slider_bullet_{{ $i }}" @if($i==0) class="active" @endif></span>
                        @endfor
                    </div>
                </div>

            @endif


           @include('include.incredible-offers')





        </div>
    </div>

    <div class="row">

        @if(sizeof($randomProduct)>1)
            <div class="col-md-9">
                @include('include.horizontal_product_list',['title'=>'جدید ترین محصولات فروشگاه','products'=>$new_products])
            </div>
            <div class="col-md-3 promo_single">
                <div class="promo_single_header">
                    <span>پیشنهاد های لحظه ای برای شما</span>
                </div>
                @foreach($randomProduct as $key=>$value)
                    <a data-swiper-slide-index="{{ $key }}" href="{{ url('product/dkp-'.$value->id.'/'.$value->product_url) }}" @if($key==0) class="active" @endif>
                        <img src="{{ url('files/thumbnails/'.$value->image_url) }}">
                        <p class="title">
                            @if(strlen($value->title)>50)
                                {{ mb_substr($value->title,0,33).'...' }}
                            @else
                                {{ $value->title }}
                            @endif
                        </p>
                        <?php
                          $price1=$value->price+$value->discount_price;
                        ?>
                        <p class="discount_price">
                            @if(!empty($value->discount_price))
                                <del>
                                    {{ replace_number(number_format($price1))  }}
                                </del>
                            @endif
                        </p>
                        <p class="price">
                            {{ replace_number(number_format($value->price)).' تومان'   }}
                        </p>
                    </a>
                @endforeach
            </div>
        @else
            @include('include.horizontal_product_list',['title'=>'جدید ترین محصولات فروشگاه','products'=>$new_products])
        @endif
    </div>

    <div class="row">
        @include('include.horizontal_product_list',['title'=>' پرفروش ترین محصولات فروشگاه','products'=>$best_selling_product])
    </div>



@endsection

@section('head')
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('slick/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('slick/slick-theme.css') }}" />
@endsection

@section('seo')
<meta name="description" content="{{ config('shop-info.description') }}"/>
    <meta name="keywords" content="{{ config('shop-info.keywords') }}"/>
    <meta property="og:site_name" content="{{ config('shop-info.shop_name') }}"/>
    <meta property="og:description" content="{{ config('shop-info.description') }}"/>
    <meta property="og:title" content="{{ config('shop-info.shop_name') }}"/>
    <meta property="og:locale" content="fa_IR"/>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('slick/slick.js') }}"></script>
    <script>
        load_slider('<?= sizeof($sliders) ?>');
        const swiper=new Swiper('.swiper-container',{
            slidesPerView:'auto',
            spaceBetween:10,
            navigation:{
                nextEl:'.slick-next',
                prevEl:'.slick-prev'
            }
        });
        <?php
            if(sizeof($incredible_offers)<6)
            {
                ?>
                  $(".discount_box_footer .slick-next").hide();
                  $(".discount_box_footer .slick-prev").hide();
                <?php
            }
        ?>
        $('.product_list').slick({
             speed: 900,
             slidesToShow: 4,
             slidesToScroll: 3,
             rtl:true,
             infinite: false,
        });
    </script>
@endsection
