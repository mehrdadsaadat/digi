<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Cart;
use App\CatBrand;
use App\Category;
use App\CategoryCommonQuestion;
use App\Color;
use App\Comment;
use App\CommonQuestion;
use App\Item;
use App\ItemValue;
use App\Lib\Mobile_Detect;
use App\Product;
use App\ProductWarranty;
use App\ReView;
use App\SearchProduct;
use App\Slider;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Auth;
use DB;
use App\Question;
use App\Favorite;
use App\Seller;
use App\SellerProduct;
use Mail;
class SiteController extends Controller
{
    protected $view='';
    public function __construct()
    {
        getCatList();
        $detect=new Mobile_Detect();
        if($detect->isMobile() || $detect->isTablet()){
            $this->view='mobile.';
        }
    }
    public function index()
    {
        $sliders=Slider::OrderBy('id','DESC')->get();
        $incredible_offers=ProductWarranty::with('getProduct.getCat')
            ->with(['itemValue'=>function($query){
                $query->whereHas('important_item')->with('important_item');
            }])
            ->where(['offers'=>1])
            ->limit(9)->get()->unique('product_id');

        $new_products=Product::where('status',1)->whereHas('getFirstProductPrice')->orderBy('id','DESC')->orderBy('view','DESC')->limit(10)->get();
        $best_selling_product=Product::where('status',1)->whereHas('getFirstProductPrice')->orderBy('order_number','DESC')->limit(10)->get();
        $randomProduct=Product::where('status',1)->whereHas('getFirstProductPrice')->inRandomOrder()->limit(10)
            ->select(['id','title','price','image_url','product_url','discount_price'])->get();
        return view(  $this->view.'shop.index',[
             'sliders'=>$sliders,
             'incredible_offers'=>$incredible_offers,
             'new_products'=>$new_products,
             'best_selling_product'=>$best_selling_product,
             'randomProduct'=>$randomProduct
        ]);
    }
    public function show_product($product_id,$product_url=null)
    {
        $priceItem=[];
        $id=str_replace('dkp-','',$product_id);
        $where=['id'=>$id];
        if($product_url!=null)
        {
            $where['product_url']=$product_url;
        }
        $product=Product::with(['getBrand','Gallery','getProductColor.getColor','getCat'])
            ->with(['getProductWarranty'=>function ($query){
                $query->with(['getWarranty','getSeller']);
            }])->where($where)->firstOrFail();
        if(sizeof($product->getProductColor)==0){
            $priceItem=Product::getPriceItem($product->cat_id);
        }
        $product_items=Item::getProductItem($product);
        $product_item_count=ItemValue::where('product_id',$product->id)->count();
        $relate_products=Product::where(['cat_id'=>$product->cat_id,'brand_id'=>$product->brand_id])
            ->where('id','!=',$product->id)->limit(15)->get();
        $review=ReView::where('product_id',$product->id)->get();
        $comment_count=0;
        $useful_comment=null;

        $question_count=0;
        $questions=null;

        if($this->view=='mobile.')
        {
            $comment_count=Comment::where(['product_id'=>$product->id,'status'=>1])->count();
            $useful_comment=Comment::with('getUserInfo')->where(['product_id'=>$product->id,'status'=>1])->orderBy('like','DESC')->limit(2)->get();

            $question_count=Question::where(['product_id'=>$product->id,'status'=>1,'question_id'=>0])->count();
            $questions=Question::with('getUser')->where(['product_id'=>$product->id,'status'=>1,'question_id'=>0])->orderBy('id','DESC')->limit(2)->get();
        }
        $favorite=null;
        if(Auth::check())
        {
            $user_id=Auth::user()->id;
            $favorite=Favorite::where(['product_id'=>$product->id,'user_id'=>$user_id])->first();
        }
        $category=Category::with(['getParent.getParent'])->where('id',$product->cat_id)->first();
        return view($this->view.'shop.show_product',[
            'product'=>$product,
            'product_items'=>$product_items,
            'product_item_count'=>$product_item_count,
            'relate_products'=>$relate_products,
            'review'=>$review,
            'comment_count'=>$comment_count,
            'useful_comment'=>$useful_comment,
            'favorite'=>$favorite,
            'category'=>$category,
            'question_count'=>$question_count,
            'questions'=>$questions,
            'priceItem'=>$priceItem
        ]);
    }
    public function change_color(Request $request)
    {
        $priceItem=[];
        $color_id=$request->get('color_id');
        $product_id=$request->get('product_id');
        $product=Product::with(['getProductColor.getColor'])
            ->with(['getProductWarranty'=>function ($query){
                $query->with(['getWarranty','getSeller']);
            }])->where(['id'=>$product_id])->first();
        if(sizeof($product->getProductColor)==0){
            $priceItem=Product::getPriceItem($product->cat_id);
        }
        $check_has_color=ProductWarranty::where(['color_id'=>$color_id,'product_id'=>$product_id])
            ->where('product_number','>',0)->first();
        if($product && $check_has_color){
            return view('include.warranty',['product'=>$product,'color_id'=>$color_id,'priceItem'=>$priceItem]);
        }
        else{
            return false;
        }

    }
    public function confirm()
    {

        if(Session::has('mobile_number'))
        {
            $layout=$this->view=='mobile.' ? 'mobile-auth' : 'auth';
            $margin=$this->view=='mobile.' ? '10' : '25';
            return view('auth.confirm',['layout'=>$layout,'margin'=>$margin]);
        }
        else{
            return redirect('/');
        }
    }
    public function confirmphone()
    {
        if(Session::has('mobile_number'))
        {
            $layout=$this->view=='mobile.' ? 'mobile-auth' : 'auth';
            $margin=$this->view=='mobile.' ? '10' : '25';
            return view('auth.confirmphone',['layout'=>$layout,'margin'=>$margin]);
        }
        else{
            return redirect('/');
        }
    }
    public function resend(Request $request)
    {
        return User::resend($request);
    }
    public function active_account(Request $request)
    {
       $mobile=$request->get('mobile');
       $active_code=$request->get('active_code');
       $user=User::where(['mobile'=>$mobile,'active_code'=>$active_code,'account_status'=>'InActive'])->first();
       if($user)
       {
           $user->account_status='active';
           $user->active_code=null;
           $user->update();
           Auth::guard()->login($user);
           return redirect('/');
       }
       else{
           return redirect()->back()
               ->with('mobile_number',$mobile)->with('validate_error','کد وارد شده اشتباه میباشد')->withInput();
       }
    }
    public function changeMobileNumber(Request $request)
    {
        return User::changeMobileNumber($request);
    }
    public function add_cart(Request $request)
    {
        Cart::add_cart($request->all());
        return redirect('/Cart');
    }
    public function show_cart()
    {
        $cart_data=Cart::getCartData();
        return view($this->view.'shop.cart',['cart_data'=>$cart_data]);
    }
    public function remove_product(Request $request)
    {
        return Cart::removeProduct($request);
    }
    public function change_product_cart(Request $request)
    {
        return Cart::ChangeProductCount($request);
    }
    public function show_child_cat_list($cat_url){
        $category=Category::with('getChild.getChild')->where('url',$cat_url)->firstOrFail();
        return view('shop.child_cat',['category'=>$category]);
    }
    public function cat_product($cat_url,Request $request){
        $category=Category::with('getParent.getParent')
            ->with(['getChild'=>function($query){
                $query->whereNull('search_url');
            }])
            ->where('url',$cat_url)->firstOrFail();
        $filter=Category::getCatFilter($category);
        $brands=CatBrand::with('getBrand')->where('cat_id',$category->id)->get();
        $colors=[];
        $checkHasColor=DB::table('product_color')->where('cat_id',$category->id)->first();
        if($checkHasColor){
            $colors=Color::where('type',1)->get();
        }
        return view($this->view.'shop.cat_product',['filter'=>$filter,'category'=>$category,'brands'=>$brands,'colors'=>$colors]);
    }
    public function get_cat_product($cat_url,Request $request){
        $category=Category::with('getChild.getChild')
            ->where('url',$cat_url)->whereNull('search_url')->firstOrFail();
        $searchProduct=new SearchProduct($request);
        $searchProduct->set_product_category($category);
        $searchProduct->brands=$request->get('brand',null);
        return $result=$searchProduct->getProduct();
    }
    public function brand_product($brand_name)
    {
        $brand=Brand::with('getCat.getCategory')->where('brand_ename',$brand_name)->firstOrFail();
        return view($this->view.'shop.brand_product',['brand'=>$brand]);
    }
    public function get_brand_product($brand_name,Request $request)
    {
        $brand=Brand::where('brand_ename',$brand_name)->firstOrFail();
        $searchProduct=new SearchProduct($request);
        $searchProduct->brands=$brand->id;
        $searchProduct->set_brand_category($request->get('category',array()));
        return $result=$searchProduct->getProduct();
    }
    public function compare($productId1,$productId2=null,$productId3=null,$productId4=null)
    {
        $items=[];
        $products_id=get_compare_product_id(array($productId1,$productId2,$productId3,$productId4));
        $products=Product::with(['getItemValue','Gallery'])->whereIn('id',$products_id)
            ->select(['id','title','cat_id','price','product_url'])->get();
        if(sizeof($products)>0)
        {
            $items=Item::getCategoryItem($products[0]->cat_id);
            $category=Category::where('id',$products[0]->cat_id)->firstOrFail();
            return view('shop.compare',[
                'items'=>$items,
                'products'=>$products,
                'category'=>$category
            ]);
        }
        else{
            return redirect('/');
        }
    }
    public function get_compare_products(Request $request)
    {
        $brand_id=$request->get('brand_id',0);
        $cat_id=$request->get('cat_id',0);
        $search_text=$request->get('search_text');
        $products=Product::where('cat_id',$cat_id)->select(['id','price','image_url','title']);
        if($brand_id>0){
            $products=$products->where('brand_id',$brand_id);
        }
        if($search_text){
            $products=$products->where('title','like','%'.$search_text.'%');
        }
        $products=$products->orderBy('order_number','DESC')->paginate(10);
        return $products;
    }
    public function getCatBrand(Request $request)
    {
        $cat_id=$request->get('cat_id',0);
        $brands=CatBrand::with('getBrand')->where('cat_id',$cat_id)->get();
        return $brands;
    }
    public function comment_form($product_id)
    {
        $product=Product::findOrFail($product_id);
        return view($this->view.'shop.comment_form',['product'=>$product]);
    }
    public function add_comment($product_id,Request $request)
    {
        $product=Product::findOrFail($product_id);
        $status=Comment::addComment($request,$product);
        return redirect('product/dkp-'.$product->id.'/'.$product->product_url)->with('comment_status',$status['status']);
    }
    public function CartProductData()
    {
        return Cart::getCartData();
    }
    public function get_question($product_id,Request $request){
        $ordering=$request->get('ordering','new');
        $Question=Question::with(['getUser','getAnswer.getUser'])->where(['question_id'=>0,'product_id'=>$product_id,'status'=>1]);
        if($ordering=='new')
        {
            $Question=$Question->orderBy('id','DESC');
        }
        else if($ordering=='answer_count')
        {
            $Question=$Question->orderBy('answer_count','DESC');
        }
        else if($ordering=='user' && Auth::check())
        {
            $user_id=$request->user()->id;
            $Question=$Question->orderByRaw(\DB::raw("FIELD(user_id,".$user_id.") DESC"));
        }
        else{
            $Question=$Question->orderBy('id','DESC');
        }
        $Question=$Question->paginate(10);
        return $Question;
    }
    public function share_product(Request $request)
    {
        if($request->ajax())
        {
            $email=$request->get('email');
            $product_id=$request->get('product_id');
            $user_name=(Auth::check() && !empty(Auth::user()->name)) ? Auth::user()->name :'کاربر ناشناس';
            $product=\App\Product::where('id',$product_id)->select(['id','title','price','image_url','product_url'])
            ->first();
            if($product)
            {
                try{
                    Mail::to( $email)->send(new \App\Mail\ShareEmail($user_name, $product));
                    return 'ok';
                 }
                 catch(\Exception $e){
                    return 'error';
                 }
            }
            else{
                return 'error';
            }

        }
    }
    public function page($url)
    {
        $page=\App\Page::where(['url'=>$url])->firstOrFail();
        return view($this->view.'shop.page',['page'=>$page]);
    }
    public function seller_product($seller_id)
    {
        $seller=Seller::where(['id'=>$seller_id,'account_status'=>'active'])->firstOrFail();
        $category=SellerProduct::getCat($seller_id);
        $brands = SellerProduct::brand($seller_id);
        return view($this->view . 'shop.seller',['seller'=> $seller, 'category'=> $category, 'brands'=> $brands]);
    }
    public function get_seller_product($seller_id,Request $request)
    {
        $seller = Seller::where(['id' => $seller_id, 'account_status' => 'active'])->firstOrFail();
        $products_id=SellerProduct::where(['seller_id'=>$seller_id])->pluck('product_id', 'product_id')->toArray();
        $searchProduct = new SearchProduct($request);
        $searchProduct->set_products_id($products_id);
        $searchProduct->category= $request->get('category', array());
        $searchProduct->brands = $request->get('brand', null);
        $searchProduct->seller_id = $seller_id;
        return $result = $searchProduct->getProduct();
    }
    public function faq(Request $request){
        $search_question=[];
        $cat=CategoryCommonQuestion::get();
        $question=CommonQuestion::where('pin',1)->get();
        $q=$request->get('q');
        if(!empty($q)){
            $search_question=CommonQuestion::where('title','like','%'.$q.'%')->get();
        }
        return view($this->view . 'shop.faq',['cat'=>$cat,'question'=> $question,'search_question'=>$search_question,'q'=>$q,'request'=>$request]);
    }
    public function faq_category($id){
        $cat=CategoryCommonQuestion::find($id);
        $question=CommonQuestion::where('cat_id',$id)->get();
        $pin_question=CommonQuestion::where('pin',1)->get();
        return view($this->view . 'shop.faq_category',['cat'=>$cat,'pin_question'=> $pin_question,'question'=>$question]);
    }
    public function faq_question($id){
        $question=CommonQuestion::find($id);
        $pin_question=CommonQuestion::where('pin',1)->get();
        return view($this->view . 'shop.faq_question',['pin_question'=> $pin_question,'question'=>$question]);
    }
    public function search_product(Request $request){
        $string=$request->get('string','');
        if(strlen(trim($string))>=2){
            return view($this->view.'shop.cat_product');
        }
        else{
            return  redirect('/');
        }
    }
    public function get_search_product(Request $request){
        $searchProduct=new SearchProduct($request);
        return $result=$searchProduct->getProduct();
    }
    public function sitemap(){
        $product_count=Product::where('status','>=',-1)->count();
        return response()->view('sitemap',['product_count'=>$product_count])
            ->header('Content-Type','text/xml');
    }
    public function sitemap_products($page){
        $offset=($page-1)*100;
        $products=Product::where('status','>=',-1)->offset($offset)->limit(100)
            ->get();
        return response()->view('sitemap-products',['products'=>$products])
            ->header('Content-Type','text/xml');
    }
    public function sitemap_category()
    {
        $category=Category::with('getChild.getChild.getChild')->where('parent_id',0)->get();
        return response()->view('sitemap-category',['category'=>$category])
            ->header('Content-Type','text/xml');
    }
}
