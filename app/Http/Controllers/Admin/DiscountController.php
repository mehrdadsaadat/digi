<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\DiscountCode;
use App\Http\Requests\DiscountRequest;
use Illuminate\Http\Request;

class DiscountController extends CustomController
{
    protected  $model='DiscountCode';
    protected  $title='تخفیف';
    protected $route_params='discount';
    public function index(Request $requestٍ){
        $discount=DiscountCode::getData($requestٍ->all());
        $trash_discount_count=DiscountCode::onlyTrashed()->count();
        return view('discount.index',['discount'=>$discount,'trash_discount_count'=>$trash_discount_count,'req'=>$requestٍ]);
    }
    public function create(){
        $cat=Category::get_parent();
        return view('discount.create',['cat'=>$cat]);
    }
    public function store(DiscountRequest $request){

        $incredible_offers=$request->has('incredible_offers') ? 1 : 0;
        $date=getTimestamp($request->get('expiry_time'),'last');
        $discount=new DiscountCode($request->all());
        $discount->expiry_time=$date;
        $discount->incredible_offers=$incredible_offers;
        $discount->saveOrFail();
        return redirect('admin/discount')->with('message','ثبت کد تخفیف با موفقیت انجام شد');
    }
    public function edit($id)
    {
        $discount=DiscountCode::findOrFail($id);
        $cat=Category::get_parent();
        return view('discount.edit',['cat'=>$cat,'discount'=>$discount]);
    }
    public function update($id,DiscountRequest $request)
    {
        $discount=DiscountCode::findOrFail($id);
        $incredible_offers=$request->has('incredible_offers') ? 1 : 0;
        $date=getTimestamp($request->get('expiry_time'),'last');
        $formData=$request->all();
        $formData['expiry_time']=$date;
        $formData['incredible_offers']=$incredible_offers;
        $discount->update($formData);
        return redirect('admin/discount')->with('message','ویرایش کد تخفیف با موفقیت انجام شد');
    }

}
