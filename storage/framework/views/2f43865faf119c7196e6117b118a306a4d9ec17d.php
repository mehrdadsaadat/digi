<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
        ['title'=>'مدیریت انبار ها','url'=>url('admin/stockrooms')],
        ['title'=>'لیست ورودی های انبار','url'=>url('admin/stockroom/input')],
        ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            لیست ورودی های انبار
        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; $Jdf=new \App\Lib\Jdf() ?>

            <form method="get" class="search_form">
              <div class="form-group">
                    <?php echo e(Form::select('stockroom_id',$stockroom,$req->get('stockroom_id',''),['class'=>'selectpicker auto_width'])); ?>

              </div>
              <button class="btn btn-primary">جست و جو</button>
            </form>
            <a href="<?php echo e(url('admin/stockroom/add/input')); ?>" class="btn btn-success add_btn">
              <span class="fa fa-pencil"></span>
                اضافه کردن محصول انبار
            </a>
            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>نام انبار</th>
                        <th>اضافه شده توسط</th>
                        <th>تعداد محصول اضافه شده</th>
                        <th>زمان ثبت</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $stockroomEvent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr>
                            <td><?php echo e(replace_number($i)); ?></td>
                            <td><?php echo e($value->getStockroom->name); ?></td>
                            <td><?php echo e($value->getUser->name); ?></td>
                            <td><?php echo e(replace_number($value->product_count)); ?></td>
                            <td><?php echo e($Jdf->jdate('H:i:s',$value->time)); ?> / <?php echo e($Jdf->jdate('Y-m-d',$value->time)); ?></td>
                            <td>
                                <a href="<?php echo e(url('admin/stockroom/input/'.$value->id)); ?>">
                                    <span data-toggle="tooltip" data-placement="bottom"  title='لیست محصولات موجود در انبار' class="fa fa-ambulance"></span>
                                </a>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if(sizeof($stockroomEvent)==0): ?>
                        <tr>
                            <td colspan="6">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </form>

            <?php echo e($stockroomEvent->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/stockroom/input.blade.php ENDPATH**/ ?>