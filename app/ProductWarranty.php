<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductWarranty extends Model
{
   use SoftDeletes;
   protected $table='product_warranties';
   protected $fillable=['product_id','warranty_id','color_id','price1','price2','send_time','seller_id','product_number','product_number_cart',
       'offers_first_date','offers_last_date','offers_first_time','offers_last_time','offers','show_index'];
   public static function getData($request,$seller_id=0,$product_id=0)
   {
       $string='?';
       $product_warranties=self::with(['getColor','getWarranty','getSeller'])->orderBy('id','DESC');
       if(inTrashed($request)){
           $product_warranties=$product_warranties->onlyTrashed();
           $string=create_paginate_url($string,'trashed=true');
       }
       if($seller_id>0)
       {
           $product_warranties=$product_warranties->where('seller_id',$seller_id);
       }
       $product_id= $product_id > 0 ? $product_id : $request['product_id'];
       $product_warranties=$product_warranties->where('product_id',$product_id);
       $product_warranties=$product_warranties->paginate(10);
       $product_warranties->withPath($string);
       return $product_warranties;
   }
   public function getColor()
   {
       return $this->belongsTo(Color::class,'color_id','id')
           ->withDefault(['name'=>'','id'=>0]);
   }
   public function getSeller()
   {
        return $this->belongsTo(Seller::class, 'seller_id', 'id')
            ->withDefault(['brand_name'=>env('SHOP_NAME',''),'id'=>0,'account_status'=>'active']);
   }
   public function getWarranty()
   {
      return $this->belongsTo(Warranty::class,'warranty_id','id');
   }
   protected static function boot()
    {
        parent::boot();
        static::restored(function($warranty){

            add_min_product_price($warranty);
            $product=Product::select(['id','price','status'])->where('id',$warranty->product_id)->withTrashed()->first();
            update_product_price($product);
        });
        static::deleted(function($warranty){

            check_has_product_warranty($warranty);
            $product=Product::select(['id','price','status'])->where('id',$warranty->product_id)->withTrashed()->first();
            update_product_price($product);
        });
    }
   public function getProduct(){
       return $this->hasOne(Product::class,'id','product_id')
           ->select(['id','title','image_url','cat_id','product_url']);
    }
   public function itemValue()
    {
        return $this->hasMany(ItemValue::class,'product_id','product_id');
    }
   public static  function  getPriceItem($product){
       $items=[];
       $cat=Category::where('id',$product->cat_id)->first();
       if($cat){
           $parent_cat=Category::where('id',$cat->parent_id)->first();
           $catId=$parent_cat ? [$parent_cat->id,$cat->id] : [$cat->id];
           $items=Color::where('type',2)->whereIn('cat_id',$catId)
               ->orderBy('position','ASC')->get();
       }

       if(sizeof($items)==0){
           $items=Color::where('type',1)->get();
       }
       return $items;
   }
}
