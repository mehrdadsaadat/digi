<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'مدیریت تنوع های قیمت','url'=>url('admin/product_warranties?product_id='.$product->id)]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
          مدیریت تنوع های قیمت <?php echo e($product->title); ?>


            <?php echo $__env->make('include.item_table',['count'=>$trash_product_warranties_count,'route'=>'admin/product_warranties','title'=>'تنوع قیمت','queryString'=>['param'=>'product_id','value'=>$product->id]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="panel_content" >

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>


            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <table class="table table-bordered table-striped" id="product_warranties">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام گارانتی</th>
                        <th>قیمت محصول</th>
                        <th>قیمت محصول برای فروش</th>
                        <th>تعداد موجودی محصول</th>
                        <th>رنگ (یا سایز)</th>
                        <th>فروشنده</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $product_warranties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr>
                            <td>
                                <input type="checkbox" name="product_warranties_id[]" class="check_box" value="<?php echo e($value->id); ?>"/>
                            </td>
                            <td><?php echo e(replace_number($i)); ?></td>
                            <td><?php echo e($value->getWarranty->name); ?></td>
                            <td style="min-width:160px"><div class="alert alert-success"><?php echo e(replace_number(number_format($value->price1)). 'تومان'); ?></div></td>
                            <td style="min-width:160px"><span class="alert alert-warning"><?php echo e(replace_number(number_format($value->price2)). 'تومان'); ?></span></td>
                            <td><?php echo e(replace_number($value->product_number)); ?></td>
                            <td style="width:140px">

                                <?php if($value->getColor!=null): ?>
                                    <?php if($value->getColor->type==1): ?>
                                        <span style="background:#<?php echo e($value->getColor->code); ?>" class="color_td">
                                           <span style="<?php if($value->getColor->name!='سفید'): ?> color:white <?php endif; ?>"><?php echo e($value->getColor->name); ?></span>
                                        </span>
                                    <?php else: ?>
                                        <span class="color_td"><?php echo e($value->getColor->name); ?></span>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($value->getSeller->brand_name); ?>

                              <?php if($value->status==2 || $value->getSeller->account_status!='active'): ?>
                                    <p style="color: grey;margin-top:5px">
                                        غیر فعال
                                    </p>
                              <?php endif; ?>
                            </td>
                            <td>
                                <?php if(!$value->trashed()): ?>
                                <a href="<?php echo e(url('admin/product_warranties/'.$value->id.'/edit?product_id='.$product->id)); ?>"><span class="fa fa-edit"></span></a>
                                <?php endif; ?>

                                <?php if($value->trashed()): ?>
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی تنوع قیمت' onclick="restore_row('<?php echo e(url('admin/product_warranties/'.$value->id.'?product_id='.$product->id)); ?>','<?php echo e(Session::token()); ?>','آیا از بازیابی این تنوع قیمت مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                <?php endif; ?>

                                <?php if(!$value->trashed()): ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف تنوع قیمت' onclick="del_row('<?php echo e(url('admin/product_warranties/'.$value->id.'?product_id='.$product->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این تنوع قیمت مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                <?php else: ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف تنوع قیمت برای همیشه' onclick="del_row('<?php echo e(url('admin/product_warranties/'.$value->id.'?product_id='.$product->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این تنوع قیمت مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if(sizeof($product_warranties)==0): ?>
                        <tr>
                            <td colspan="9">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </form>

            <?php echo e($product_warranties->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script>
        $("#sidebarToggle").click();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/product_warranties/index.blade.php ENDPATH**/ ?>