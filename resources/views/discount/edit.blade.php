@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت کد های تخفیف','url'=>url('admin/discount')],
         ['title'=>'ویرایش کد تخفیف','url'=>url('admin/discount/'.$discount->id.'edit')]
    ]])

    <div class="panel">

        <div class="header">
            ویرایش کد تخفیف
        </div>

        <div class="panel_content discount_form">

            @php
               use App\Lib\Jdf;$jdf=new Jdf();
              $discount->expiry_time=$jdf->tr_num($jdf->jdate('Y/n/j',$discount->expiry_time));
            @endphp

            @include('include.warring')
            {!! Form::model($discount,['url' => 'admin/discount/'.$discount->id]) !!}

            {{ method_field('PUT') }}
            <div class="form-group">

                {{ Form::label('code','کد تخفیف : ') }}
                {{ Form::text('code',null,['class'=>'form-control left']) }}
                @if($errors->has('code'))
                    <span class="has_error">{{ $errors->first('code') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('expiry_time','تاریخ انقضا : ') }}
                {{ Form::text('expiry_time',null,['class'=>'form-control','id'=>'expiry_time','style'=>'text-align:center']) }}
                @if($errors->has('expiry_time'))
                    <span class="has_error">{{ $errors->first('expiry_time') }}</span>
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('cat_id','انتخاب دسته : ') }}
                {{ Form::select('cat_id',$cat,null,['class'=>'selectpicker','data-live-search'=>'true']) }}
            </div>



            <div class="form-group">

                {{ Form::label('amount','حداقل خرید : ') }}
                {{ Form::text('amount',null,['class'=>'form-control left amount']) }}
                @if($errors->has('amount'))
                    <span class="has_error">{{ $errors->first('amount') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('number_usable','حداکثر دفعات استفاده از کد تخغیف : ') }}
                {{ Form::text('number_usable',null,['class'=>'form-control left number_usable']) }}
                @if($errors->has('number_usable'))
                    <span class="has_error">{{ $errors->first('number_usable') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('amount_discount','میزان  تخفیف (بر حسب تومان)  : ') }}
                {{ Form::text('amount_discount',null,['class'=>'form-control left amount_discount']) }}
                @if($errors->has('amount_discount'))
                    <span class="has_error">{{ $errors->first('amount_discount') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('amount_percent','میزان تخفیف (بر حسب درصد)  : ') }}
                {{ Form::text('amount_percent',null,['class'=>'form-control left amount_percent']) }}
                @if($errors->has('amount_percent'))
                    <span class="has_error">{{ $errors->first('amount_percent') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label>استفاده برای پیشنهادات شگفت انگیز</label>
                <input type="checkbox" @if($discount->incredible_offers==1) checked="checked" @endif name="incredible_offers" />
            </div>

            <button class="btn btn-primary">ویرایش </button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('head')
    <link href="{{ asset('css/js-persian-cal.css') }}" rel="stylesheet">
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/js-persian-cal.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/cleave.min.js') }}"></script>

    <script>
        const expiry_time= new AMIB.persianCalendar('expiry_time');
        var cleave1 = new Cleave('.amount', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        var cleave2 = new Cleave('.number_usable', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        var cleave3 = new Cleave('.amount_discount', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        var cleave4 = new Cleave('.amount_percent', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });

    </script>
@endsection
