<div class="catBox" id="catBox">

    <div id="mySideNav" class="sidenav">
        <img src="<?php echo e(url('files/images/shop_icon.jpg')); ?>">
        <p>فروشگاه <?php echo e(env('SHOP_NAME','')); ?></p>
        <ul>
            <?php $__currentLoopData = $calList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <a class="parent_cat">
                        <span class="fa fa-plus-circle"></span>
                        <?php echo e($value->name); ?>

                    </a>
                    <?php if(sizeof($value->getChild)>0): ?>
                        <div class="li_div">
                            <ul>
                                <?php $__currentLoopData = $value->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($value2->notShow==0): ?>
                                        <li>
                                            <a class="child_cat" <?php if(sizeof($value2->getChild)==0): ?> href="<?php echo e(get_cat_url($value2)); ?>" <?php endif; ?>>
                                                <span><?php echo e($value2->name); ?></span>
                                            </a>

                                            <ul>
                                                <?php $__currentLoopData = $value2->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key3=>$value3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($value3->notShow==0): ?>
                                                        <li>
                                                            <a style="color:#515151" href="<?php echo e(get_cat_url($value3)); ?>"><?php echo e($value3->name); ?></a>
                                                        </li>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </li>

                                    <?php else: ?>
                                        <?php $__currentLoopData = $value2->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key3=>$value3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($value3->notShow==0): ?>
                                                <li>
                                                    <a class="child_cat"  <?php if(sizeof($value3->getChild )==0): ?> href="<?php echo e(get_cat_url($value3)); ?>" <?php endif; ?>>
                                                        <?php if(sizeof($value3->getChild)>0): ?>
                                                            <span class="fa fa-plus-circle"></span>
                                                         <?php endif; ?>
                                                        <span><?php echo e($value3->name); ?></span>
                                                    </a>
                                                    <ul>
                                                        <?php $__currentLoopData = $value3->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key4=>$value4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($value4->notShow==0): ?>
                                                                <li>
                                                                    <a style="color:#515151" href="<?php echo e(get_cat_url($value4)); ?>"><?php echo e($value4->name); ?></a>
                                                                </li>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                </li>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
</div>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/CatList.blade.php ENDPATH**/ ?>