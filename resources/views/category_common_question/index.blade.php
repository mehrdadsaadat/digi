@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'پرسش های متداول','url'=>url('admin/common-question')],
         ['title'=>'مدیریت دسته ها','url'=>url('admin/category-common-question')]
         ]])
    <div class="panel">

        <div class="header">
            مدیریت دسته ها

            @include('include.item_table',['count'=>$trash_common_question_cat_count,'route'=>'admin/category-common-question','title'=>'دسته'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="title" class="form-control" value="{{ $req->get('title','') }}" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام دسته</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($CategoryCommonQuestion as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="category-common-question_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->title }}</td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/category-common-question/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی دسته' onclick="restore_row('{{ url('admin/category-common-question/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این دسته مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف دسته' onclick="del_row('{{ url('admin/category-common-question/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این دسته مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف دسته برای همیشه' onclick="del_row('{{ url('admin/category-common-question/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این دسته مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($CategoryCommonQuestion)==0)
                        <tr>
                            <td colspan="4">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $CategoryCommonQuestion->links() }}
        </div>
    </div>

@endsection
