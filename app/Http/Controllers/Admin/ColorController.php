<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Color;
use App\Http\Requests\BrandRequest;
use Illuminate\Http\Request;

class ColorController extends CustomController
{
    protected  $model='Color';
    protected  $title='رنگ';
    protected $route_params='colors';
    public function index(Request $request)
    {
        $color=Color::getData($request->all());
        $trash_color_count=Color::onlyTrashed()->count();
        return view('color.index',['color'=>$color,'trash_color_count'=>$trash_color_count,'req'=>$request]);
    }
    public function create()
    {
        return view('color.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,['name'=>'required','code'=>'required'],[],['name'=>'نام رنگ','code'=>'کد رنگ']);
        $color=new Color($request->all());
        $color->saveOrFail();
        return redirect('admin/colors')->with('message','ثبت رنگ با موفقیت انجام شد');
    }
    public function edit($id)
    {
        $color=Color::findOrFail($id);
        return view('color.edit',['color'=>$color]);
    }
    public function update($id,Request $request)
    {
        $this->validate($request,['name'=>'required','code'=>'required'],[],['name'=>'نام رنگ','code'=>'کد رنگ']);
        $color=Color::findOrFail($id);
        $color->update($request->all());
        return redirect('admin/colors')->with('message','ویرایش رنگ با موفقیت انجام شد');
    }
}
