<?php $__env->startSection('content'); ?>

    <div class="error_content">

        <?php if($errorLayout=='shop'): ?>
            <h3>صفحه‌ای که دنبال آن بودید پیدا نشد!</h3>

        <?php else: ?>
            <h4>صفحه‌ای که دنبال آن بودید پیدا نشد!</h4>
        <?php endif; ?>
        <a href="<?php echo e(url('/')); ?>" class="btn btn-success">صفحه اصلی</a>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.".$errorLayout, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/errors/404.blade.php ENDPATH**/ ?>