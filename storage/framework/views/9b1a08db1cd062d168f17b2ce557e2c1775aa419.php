<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت محصولات','url'=>url('admin/products')],
         ['title'=>'افزودن محصول جدید','url'=>url('admin/products/create')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php
    use App\Product;$status=Product::ProductStatus();
    ?>
    <div class="panel">

        <div class="header">افزودن محصول جدید</div>

        <div class="panel_content">
            <?php echo Form::open(['url' => 'admin/products','files'=>true]); ?>


            <div class="form-group">

                <?php echo e(Form::label('title','عنوان محصول : ')); ?>

                <?php echo e(Form::text('title',null,['class'=>'form-control total_width_input'])); ?>

                <?php if($errors->has('title')): ?>
                    <span class="has_error"><?php echo e($errors->first('title')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::textarea('tozihat',null,['class'=>'form-control ckeditor'])); ?>

            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        <?php echo e(Form::label('ename','نام لاتین محصول : ')); ?>

                        <?php echo e(Form::text('ename',null,['class'=>'form-control left'])); ?>

                    </div>

                    <div class="form-group">
                        <?php echo e(Form::label('cat_id','انتخاب دسته : ')); ?>

                        <?php echo e(Form::select('cat_id',$catList,null,['class'=>'selectpicker','data-live-search'=>'true'])); ?>

                        <?php if($errors->has('cat_id')): ?>
                            <span class="has_error"><?php echo e($errors->first('cat_id')); ?></span>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <?php echo e(Form::label('brand_id','انتخاب برند : ')); ?>

                        <?php echo e(Form::select('brand_id',$brand,null,['class'=>'selectpicker','data-live-search'=>'true'])); ?>

                        <?php if($errors->has('brand_id')): ?>
                            <span class="has_error"><?php echo e($errors->first('brand_id')); ?></span>
                        <?php endif; ?>
                    </div>



                    <div class="form-group">
                        <?php echo e(Form::label('status','وضعیت محصول : ')); ?>

                        <?php echo e(Form::select('status',$status,0,['class'=>'selectpicker','data-live-search'=>'true'])); ?>


                    </div>
                </div>
                <div class="col-md-6">

                    <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                    <div class="choice_pic_box" onclick="select_file()">

                        <span class="title">انتخاب تصویر محصول</span>
                        <img id="output" class="pic_tag">
                    </div>
                    <?php if($errors->has('pic')): ?>
                        <span class="has_error"><?php echo e($errors->first('pic')); ?></span>
                    <?php endif; ?>
                </div>
            </div>

            <p class="message_text">برچسب ها با استفاده از (،) از هم جدا شود</p>
            <div class="form-group">
                <input type="text" name="tag_list" id="tag_list" class="form-control" placeholder="برجسب های محصول">
                <div class="btn btn-success" onclick="add_tag()">افزودن</div>
                <input type="hidden" name="keywords" id="keywords">
            </div>

            <div id="tag_box">

            </div>
            <div style="clear:both"></div>

            <div class="form-group">

                <?php echo e(Form::label('description','توضیحات مختصر در مورد محصول (حداکثر 150 کاراکتر) : ',['style'=>'color:red;width:100%'])); ?>

                <?php echo e(Form::textarea('description',null,['class'=>'form-control','id'=>'description'])); ?>

                <?php if($errors->has('description')): ?>
                    <span class="has_error"><?php echo e($errors->first('description')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label>
                    <input type="checkbox" name="use_for_gift_cart" id="use_for_gift_cart">
                    استفاده به عنوان کارت هدیه
                </label>
            </div>

            <div class="form-group">
                <label>
                    <?php echo e(Form::checkbox('fake')); ?>

                    کالای غیر اصل
                </label>
            </div>

            <button class="btn btn-success">ثبت محصول</button>
            <?php echo Form::close(); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<script type="text/javascript" src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/product/create.blade.php ENDPATH**/ ?>