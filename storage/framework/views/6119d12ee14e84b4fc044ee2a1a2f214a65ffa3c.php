<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[

       ['title'=>'مدیریت دسته بندی ها','url'=>url('admin/category')],
       ['title'=>'مدیریت معیار تعیین قیمت','url'=>url('admin/category/'.$category->id.'/price_variation')]
      ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت معیار تعیین قیمت دسته <?php echo e($category->name); ?>

        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->make('include.warring', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <form method="post" action="<?php echo e(url('admin/category').'/'.$category->id.'/price_variation'); ?>">
                <?php echo csrf_field(); ?>

                <div class="category_items">
                    <?php if(sizeof($priceVariation)>0): ?>
                        <?php $__currentLoopData = $priceVariation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="form-group item_groups" id="price_variation_<?php echo e($value->id); ?>">
                                <input type="text" value="<?php echo e($value->name); ?>" class="form-control item_input" name="price_variation[<?php echo e($value->id); ?>]" placeholder="مقدار">

                                <span class="item_remove_message" onclick="del_row('<?php echo e(url('admin/category/price_variation/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این مقدار مطمئن هستین ؟ ')">حذف</span>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <div class="form-group item_groups" id="price_variation_-1">
                            <input type="text"  class="form-control item_input" name="price_variation[-1]" placeholder="مقدار">
                            <div class="child_item_box">

                            </div>
                        </div>
                    <?php endif; ?>

                </div>

                <div id="price_variation_box"></div>
                <span class="fa fa-plus-square" onclick="add_price_variation_input()"></span>
                <div class="form-group">
                    <button class="btn btn-primary">ثبت اطلاعات</button>
                </div>
            </form>
        </div>

    </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('footer'); ?>
    <script>
        $(document).ready(function () {
            $('.category_items').sortable();
            $('.child_item_box').sortable();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/priceVariation/index.blade.php ENDPATH**/ ?>