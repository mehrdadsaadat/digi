<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model
{
    use SoftDeletes;
    protected $table='colors';
    protected $fillable=['name','code'];
    public static function getData($request)
    {
        $string='?';
        $color=self::orderBy('id','DESC');
        if(inTrashed($request))
        {
            $color=$color->onlyTrashed();
            $string=create_paginate_url($string,'trashed=true');
        }
        if(array_key_exists('string',$request) && !empty($request['string']))
        {
            $color=$color->where('name','like','%'.$request['string'].'%');
            $string=create_paginate_url($string,'string='.$request['string']);
        }
        $color=$color->paginate(10);
        $color->withPath($string);
        return $color;
    }
    public static function addPriceVariation($price_variation,$cat_id)
    {
        $parent_position=0;
        self::where(['cat_id'=>$cat_id,'type'=>2])->update(['position'=>0]);
        foreach ($price_variation as $key=>$value)
        {
            if(!empty($value)){
                $parent_position++;
                if ($key < 0) {
                    self::insert(['name' => $value, 'cat_id' => $cat_id, 'type' =>2, 'position' => $parent_position]);

                }
                else{
                    self::where('id',$key)->update(['name'=>$value,'position'=>$parent_position]);
                }
            }

        }
    }
}
