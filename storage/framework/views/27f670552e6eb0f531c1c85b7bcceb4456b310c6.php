<?php $__env->startSection('content'); ?>

    <div>

        <div style="margin-bottom:40px">
            <div class="panel">
                <div class="header">
                    نمودار میزان فروش این ماه فروشگاه
                </div>
                <div class="panel_content">
                    <div id="container" style="width:100%;height:400px;margin:0px auto;direction:ltr"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel">
                    <div class="header">
                        مرسوله ها
                    </div>
                    <div class="panel_content submission_box">
                        <table class="table">
                            <tr>
                                <td>
                                    <img src="<?php echo e(url('files/images/step1.svg')); ?>" style="width: 60px">
                                    کل مرسوله ها
                                </td>
                                <td>
                                    <?php echo e(replace_number($submissions)); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?php echo e(url('files/images/step1.svg')); ?>" style="width: 60px">
                                    مرسوله های تایید شده
                                </td>
                                <td>
                                    <?php echo e(replace_number($submissions_approved)); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?php echo e(url('files/images/step2.svg')); ?>" style="width: 60px">
                                    مرسوله های ارسالی امروز
                                </td>
                                <td>
                                    <?php echo e(replace_number($items_today)); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?php echo e(url('files/images/step3.svg')); ?>" style="width: 60px">
                                    مرسوله های آماده ارسال
                                </td>
                                <td>
                                    <?php echo e(replace_number($submissions_ready)); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?php echo e(url('files/images/step4.svg')); ?>" style="width: 60px">
                                    مرسوله های ارسال شده به پست
                                </td>
                                <td>
                                    <?php echo e(replace_number($posting_send)); ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<?php echo e(url('files/images/step5.svg')); ?>" style="width: 60px">
                                    مرسوله های آماده دریافت از پست
                                </td>
                                <td>
                                    <?php echo e(replace_number($posting_receive)); ?>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <img src="<?php echo e(url('files/images/step6.svg')); ?>" style="width: 60px">
                                    مرسوله های تحویل داده شده
                                </td>
                                <td>
                                    <?php echo e(replace_number($delivered)); ?>

                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel">
                    <div class="header">
                       اطلاعات کلی فروشگاه
                    </div>
                    <div class="panel_content submission_box shop_info">
                        <table class="table">
                            <tr>
                                <td>
                                    <a href="<?php echo e(url('admin/users')); ?>" target="_blank">
                                    <span class="fa fa-user-o"></span>
                                    کاربران سایت
                                    </a>
                                </td>
                                <td>
                                    <span class="count_span user"><?php echo e(replace_number($user_count)); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?php echo e(url('admin/orders')); ?>" target="_blank">
                                    <span class="fa fa-list"></span>
                                    سفارشات ثبت شده
                                    </a>
                                </td>
                                <td>
                                    <span class="count_span order"><?php echo e(replace_number($order_count)); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <a href="<?php echo e(url('admin/comments')); ?>" target="_blank">
                                       <span class="fa fa-comment-o"></span>
                                       نظرات ثبت شده
                                   </a>
                                </td>
                                <td>
                                    <span class="count_span comment"><?php echo e(replace_number($comment_count)); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?php echo e(url('admin/questions')); ?>" target="_blank">
                                        <span class="fa fa-question"></span>
                                        پرسش های ثبت شده
                                    </a>
                                </td>
                                <td>
                                    <span class="count_span question"><?php echo e(replace_number($total_question_count)); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?php echo e(url('admin/products')); ?>" target="_blank">
                                        <span class="fa fa-shopping-cart"></span>
                                        محصولات ثبت شده
                                    </a>
                                </td>
                                <td>
                                    <span class="count_span product"><?php echo e(replace_number($product_count)); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?php echo e(url('admin/sellers')); ?>" target="_blank">
                                        <span class="fa fa-home"></span>
                                        فروشندگان
                                    </a>
                                </td>
                                <td>
                                    <span class="count_span user"><?php echo e(replace_number($seller_count)); ?></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div style="margin-top:30px">
            <div class="panel">
                <div class="header">
                    آخرین سفارشات ثبت شده
                </div>
                <div class="panel_content">
                    <?php echo $__env->make('include.orderList',['orders'=>$last_orders,'remove_delete_link'=>true], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
 <?php
    $price='';
    $date='';
    $count='';
    foreach($indexChartData['date_list'] as $key=>$value){
        if(array_key_exists($value,$indexChartData['price_array']))
        {
            $p=$indexChartData['price_array'][$value];
            $c=$indexChartData['count_array'][$value];
            $price.="$p,";
            $count.="$c,";
        }
        else {
            $price.="0,";
            $count.="0,";
        }
        $value=replace_number($value);
        $date.="'$value',";

    }
 ?>
 <script type="text/javascript" src=<?php echo e(url('js/highcharts.js')); ?>></script>
 <script>
   Highcharts.chart('container', {
   title: {
      text: ''
   },
   chart:{
       type:'line',
       style:{
           fontFamily:'IRANSansWeb'
       }
    },
    subtitle: {
        text: ''
      },
    yAxis: {
        title: {
           text: ''
       },
       labels:{
          useHTML:true,
          formatter:function()
          {
              let value=this.value;
              if(value>1000){
                value=number_format(value);
              }
              value=replaceNumber(value);
              return '<div style="direction:ltr">'
                +'<span>'+value+'</span>'
                +'</div>';
          },
          style:{
            fontSize:'15px'
          }
       }
    },
    xAxis: {
        categories:[<?= $date ?>]
    },

    legend: {
        verticalAlign: 'top',
        y:0
    },
    series: [{
        name: 'میزان فروش',
        data: [<?= $price ?>],
        color:'red'
   },{
        name: 'تعداد تراکنش',
        data: [<?= $count ?>],
        marker:{
            symbol:'circle'
        }
   }],
   tooltip:{
       useHTML:true,
       formatter:function()
       {
           if(this.series.name=='میزان فروش'){
               return this.x+'<br>'+'<div style="padding:5px">'+this.series.name+' : '+replaceNumber(number_format(this.y))+' تومان'+'</div>';
           }
           else{
            return this.x+'<br>'+'<div style="padding:5px">'+this.series.name+' : '+replaceNumber(number_format(this.y))+' بار'+'</div>';

           }
       },
       style:{
           fontSize:'15px'
       }
   },
    responsive: {
    rules: [{
        condition: {
            maxWidth: 500
        },
        chartOptions: {
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            }
        }
    }]
}

});
 </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/admin/index.blade.php ENDPATH**/ ?>