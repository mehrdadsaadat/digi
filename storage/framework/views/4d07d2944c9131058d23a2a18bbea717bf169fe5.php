<div class="dropdown">
    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        گزینه ها
    </button>
    <?php
    $create_param='';
    $trash_param='';
    if(isset($queryString) && is_array($queryString))
    {
        $create_param='?'.$queryString['param'].'='.$queryString['value'];
        $trash_param='&'.$queryString['param'].'='.$queryString['value'];
    }
    ?>
    <div class="dropdown-menu">
        <?php if(!isset($remove_new_record)): ?>
        <a class="dropdown-item" href="<?php echo e(url($route.'/create').$create_param); ?>">
            <span class="fa fa-pencil"></span>
            <span>افزودن <?php echo e($title); ?> جدید</span>
        </a>
        <?php endif; ?>

        <a class="dropdown-item" href="<?php echo e(url($route.'?trashed=true').$trash_param); ?>">
            <span class="fa fa-trash"></span>
            <span>سطل زباله (<?php echo e(replace_number($count)); ?>)</span>
        </a>


        <a class="dropdown-item off item_form" id="remove_items" msg="آیا از حذف <?php echo e($title); ?> های انتخابی مطمئن هستید ؟‌">
            <span class="fa fa-remove"></span>
            <span>حذف <?php echo e($title); ?> ها</span>
        </a>

        

        <?php if(isset($_GET['trashed']) && $_GET['trashed']=='true'): ?>
            <a class="dropdown-item off item_form" id="restore_items" msg="آیا از بازیابی <?php echo e($title); ?> های انتخابی مطمئن هستید ؟‌">
                <span class="fa fa-refresh"></span>
                <span>بازیابی <?php echo e($title); ?> ها</span>
            </a>
        <?php endif; ?>

        <?php if(isset($other)): ?>
          <?php $__currentLoopData = $other; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <a class="dropdown-item" href="<?php echo e($item['url']); ?>">
              <span class="fa <?php echo e($item['icon']); ?>"></span>
              <span><?php echo e($item['label']); ?></span>
            </a>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

    </div>
</div>
<?php /**PATH C:\xampp\htdocs\digikala\resources\views/include/item_table.blade.php ENDPATH**/ ?>