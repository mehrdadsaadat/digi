@extends('layouts.mobile')

@section('content')

    @php $jdf=new \App\Lib\Jdf();@endphp

    <div>
        <div class="profile_item_header order_content_header">
            <div>
                <span>آدرس ها</span>
            </div>
        </div>


        <input type="hidden" id="lat" value="0">
        <input type="hidden" id="lng" value="0">
        <div style="margin:10px">
            <profile-address :layout="'mobile'"></profile-address>
        </div>
       
    </div>
@endsection

@section('head')
    <link rel="stylesheet" href="{{ url('css/cedarmaps.css') }}" />
@endsection
@section('footer')
    <script type="text/javascript" src="{{ url('js/cedarmaps.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/leaflet.rotatedMarker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/Map.js') }}"></script>
@endsection