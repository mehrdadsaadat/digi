<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'مدیریت محصولات','url'=>url('admin/products')]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت محصولات

            <?php echo $__env->make('include.item_table',['count'=>$trash_product_count,'route'=>'admin/products','title'=>'محصول'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php use App\Product;$i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                <?php if(isset($_GET['trashed']) && $_GET['trashed']==true): ?>
                    <input type="hidden" name="trashed" value="true">
                <?php endif; ?>
                <input type="text" name="string" class="form-control" value="<?php echo e($req->get('string','')); ?>" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <?php  $status=Product::ProductStatus(); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>تصویر محصول</th>
                        <th>عنوان</th>
                        <th>فروشنده</th>
                        <th>وضعیت محصول</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr id="<?php echo e($value->id); ?>">
                            <td>
                                <input type="checkbox" name="products_id[]" class="check_box" value="<?php echo e($value->id); ?>"/>
                            </td>
                            <td><?php echo e(replace_number($i)); ?></td>
                            <td><img src="<?php echo e(url('files/thumbnails/'.$value->image_url)); ?>" class="product_pic"></td>
                            <td><?php echo e($value->title); ?></td>
                            <td>
                                <?php echo e($value->getSeller->brand_name); ?>

                            </td>
                            <td style="width:130px">
                                <?php if(array_key_exists($value->status,$status)): ?>
                                    <span class="alert <?php if($value->status==1): ?> alert-success <?php else: ?> alert-warning <?php endif; ?>" style="font-size:13px;padding:5px 7px">
                                        <?php echo e($status[$value->status]); ?>

                                    </span>
                                <?php endif; ?>
                            </td>
                            <td style="width:120px">
                                <?php if(!$value->trashed()): ?>
                                <a href="<?php echo e(url('admin/products/'.$value->id.'/edit')); ?>"><span class="fa fa-edit"></span></a>
                                <?php endif; ?>

                                <?php if($value->trashed()): ?>
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی محصول' onclick="restore_row('<?php echo e(url('admin/products/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از بازیابی این محصول مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                <?php endif; ?>
                                <a style="color:#000" href="<?php echo e(url('admin/products/'.$value->id)); ?>"><span data-toggle="tooltip" data-placement="bottom"  title='آمار فروش' class="fa fa-line-chart"></span></a>

                                <?php if(!$value->trashed()): ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف محصول' onclick="del_row('<?php echo e(url('admin/products/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این محصول مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                <?php else: ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف محصول برای همیشه' onclick="del_row('<?php echo e(url('admin/products/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این محصول مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                               <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if(sizeof($product)==0): ?>
                        <tr>
                            <td colspan="7">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </form>

            <?php echo e($product->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(asset('css/contextmenu.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(url('js/contextmenu.js')); ?>"></script>
    <script>
        let id=0;
        var menu= new Contextmenu({
            name:"menu",
            wrapper:".table",
            trigger: "tbody tr",
            item:[
                {
                    "name":"ثبت تنوع قیمت",
                    "func":"setWarranty()",
                    "disable":false
                },
                {
                    "name":"ثبت مشخصات فنی",
                    "func":"setItem()",
                    "disable":false
                },
                {
                    "name":"ثبت نقد و بررسی تخصصی",
                    "func":"setReview()",
                    "disable":false
                },
                {
                    "name":"گالری تصاویر",
                    "func":"Gallery()",
                    "disable":false
                },
                {
                    "name":"ثبت فیلتر های محصول",
                    "func":"setFilter()",
                    "disable":false
                }

            ],
            target:"_blank",
            beforeFunc: function (ele) {
                id = $(ele).attr('id');
            }
        });
        function setWarranty() {
            const url='product_warranties?product_id='+id;
            window.open(url, '_blank');
        }
        function setItem() {
            const url='products/'+id+"/items";
            window.open(url, '_blank');
        }
        function setFilter() {
            const url='products/'+id+"/filters";
            window.open(url, '_blank');
        }
        function Gallery() {
            const url='products/gallery/'+id;
            window.location=url;
        }
        function setReview() {
            const url='product/review?product_id='+id;
            window.open(url, '_blank');
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/product/index.blade.php ENDPATH**/ ?>