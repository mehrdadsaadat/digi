@extends('layouts.mobile')

@section('content')
    <div class="profile_item">
        <div class="profile_item_header message_box_title">
            <div>
                <span>ارسال پیام جدید</span>
            </div>
        </div>
        <div class="message_form">

            {!! Form::open(['url' => 'user/profile/messages','files'=>true]) !!}

            <div class="form-group">
                {{ Form::label('title','عنوان پیام : ') }}
                {{ Form::text('title',null,['class'=>'form-control']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group margin-10">
                {{ Form::label('content','محتوای پیام : ') }}
                {{ Form::textArea('content',null,['class'=>'form-control','style'=>'width:100%']) }}
            </div>

            <div class="form-group">
                @if($errors->has('content'))
                    <p class="has_error" >{{ $errors->first('content') }}</p>
                @endif
            </div>

            <div class="form-group margin-10 file-input-div">
                {{ Form::label('pic','انتخاب فایل : ') }}
                <input type="file" name="pic" id="pic" style="direction:ltr">
                @if($errors->has('pic'))
                    <span class="has_error">{{ $errors->first('pic') }}</span>
                @endif
            </div>

            <button class="btn btn-success margin-10">ارسال پیام</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
