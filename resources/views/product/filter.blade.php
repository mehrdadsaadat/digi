@extends('layouts.admin')

@section('content')
    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت محصولات','url'=>url('admin/products')],
         ['title'=>'ثبت فیلتر های محصول','url'=>url('admin/products/'.$product->id.'/filters')]
       ]])

    <div class="panel">

        <div class="header">
            افزودن فیلتر ها - {{ $product->title }}
        </div>

        <div class="panel_content" id="product_filter_box">

            @include('include.Alert')

            @if(sizeof($product_filters)>0)
                <form method="post" id="product_filters_form" action="{{ url('admin/products/'.$product->id.'/filters') }}">
                @csrf

                    @foreach($product_filters as $key=>$value)
                        <div class="item_groups" style="margin-bottom:20px">
                            <p class="title">{{ $value->title }}</p>

                            @foreach($value->getChild as $key2=>$value2)

                                <div class="form-group">
                                    <input @if(is_selected_filter($value->getValue,$value2->id)) checked="checked" @endif type="checkbox" name="filter[{{ $value->id }}][]" value="{{ $value2->id }}">
                                    <label>{{ $value2->title }}</label>
                                </div>

                           @endforeach
                        </div>
                    @endforeach
                    <button class="btn btn-success">ثبت اطلاعات</button>
                </form>
            @endif
        </div>
    </div>
@endsection
