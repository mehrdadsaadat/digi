<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'مدیریت سفارشات','url'=>url('admin/orders')]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت سفارشات

            <?php echo $__env->make('include.item_table',['count'=>$trash_orders_count,'route'=>'admin/orders','title'=>'سفارش'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <form method="get" class="search_form order_search">
               <?php if(isset($_GET['trashed']) && $_GET['trashed']==true): ?>
                   <input type="hidden" name="trashed" value="true">
               <?php endif; ?>
               <input type="text" autocomplete="off" name="order_id" class="form-control" value="<?php echo e($req->get('order_id','')); ?>" placeholder="شماره سفارش">
               <input type="text" autocomplete="off" name="first_date" style="margin-right: 10px" class="pdate form-control" id="pcal1" value="<?php echo e($req->get('first_date','')); ?>" placeholder="شروع از تاریخ">
               <input type="text" autocomplete="off" name="end_date" style="margin: 0px 10px" class="pdate form-control" id="pcal2" value="<?php echo e($req->get('end_date','')); ?>" placeholder="تا تاریخ">
               <button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <?php echo $__env->make('include.orderList', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </form>

            <?php echo e($orders->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(asset('css/js-persian-cal.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(asset('js/js-persian-cal.min.js')); ?>"></script>
    <script>
        const pcal1= new AMIB.persianCalendar('pcal1');
        const pcal2= new AMIB.persianCalendar('pcal2');
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/orders/index.blade.php ENDPATH**/ ?>