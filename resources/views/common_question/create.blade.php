@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت پرسش ها','url'=>url('admin/common-question/create')],
         ['title'=>'افزودن پرسش جدید','url'=>url('admin/common-question/create')]
    ]])

    <div class="panel">

        <div class="header">افزودن پرسش جدید</div>

        <div class="panel_content">


            {!! Form::open(['url' => 'admin/common-question','files'=>true]) !!}

            <div class="form-group">

                {{ Form::label('title','عنوان پرسش : ') }}
                {{ Form::text('title',null,['class'=>'form-control total_width_input']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group">
                {{ Form::label('small_answer','پاسخ کوتاه : ',['style'=>'margin:0px']) }}
            </div>
            <div class="form-group">
                {{ Form::textarea('small_answer',null,['class'=>'form-control ckeditor']) }}
            </div>
            @if($errors->has('small_answer'))
                <span class="has_error">{{ $errors->first('small_answer') }}</span>
            @endif

            <div class="form-group">
                {{ Form::label('answer','پاسخ  : ',['style'=>'margin:0px']) }}
            </div>
            <div class="form-group">
                {{ Form::textarea('answer',null,['class'=>'form-control ckeditor']) }}
            </div>

            @if($errors->has('answer'))
                <span class="has_error">{{ $errors->first('answer') }}</span>
            @endif

            <div class="form-group">
                {{ Form::label('cat_id','انتخاب دسته : ') }}
                {{ Form::select('cat_id',$cat,null,['class'=>'selectpicker','data-live-search'=>'true']) }}
            </div>


            <div class="form-group">
                {{ Form::label('pin','ثبت به عنوان پرسش پر تکرار : ') }}
                {{ Form::checkbox('pin',false) }}
            </div>

            <button class="btn btn-success">ثبت پرسش</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
@endsection

