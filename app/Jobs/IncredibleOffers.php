<?php

namespace App\Jobs;

use App\Offers;
use App\ProductWarranty;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class IncredibleOffers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $row_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->row_id=$id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ProductWarranty=ProductWarranty::find($this->row_id);
        if($ProductWarranty && $ProductWarranty->offers==1){
            $time=time();
            if($ProductWarranty->offers_first_time<=$time)
            {
                $offers=new Offers();
                $offers->remove($ProductWarranty);
            }

        }
    }
}
