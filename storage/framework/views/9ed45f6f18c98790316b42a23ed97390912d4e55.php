<?php if(sizeof($products)>0): ?>
    <div class="product_box">
        <div class="box_title">
            <h6><?php echo e($title); ?></h6>
        </div>
        <div class="product_list" dir="rtl">
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                $price1=$product->price+$product->discount_price;
                ?>
                <a href="<?php echo e(url('product/dkp-'.$product->id.'/'.$product->product_url)); ?>">
                    <div class="product">
                        <div class="product_img_div">
                            <img src="<?php echo e(url('files/thumbnails/'.$product->image_url)); ?>">
                            <?php if(!empty($product->discount_price)): ?>
                                <span class="discount-badge">
                                        <?php
                                    $d=($product->price/$price1)*100;
                                    $d=100-$d;
                                    $d=round($d);
                                    ?>
                                        ٪<?php echo e(replace_number($d)); ?>

                                    </span>
                            <?php endif; ?>
                        </div>

                        <p class="title">
                            <?php if(strlen($product->title)>33): ?>
                                <?php echo e(mb_substr($product->title,0,33).' ... '); ?>

                            <?php else: ?>
                                <?php echo e($product->title); ?>

                            <?php endif; ?>
                        </p>
                        <p class="discount_price">
                            <?php if(!empty($product->discount_price)): ?>
                                <del>
                                    <?php echo e(replace_number(number_format($price1))); ?>

                                </del>
                            <?php endif; ?>
                        </p>
                        <p class="price">
                            <?php echo e(replace_number(number_format($product->price)).' تومان'); ?>

                        </p>
                    </div>
                </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php endif; ?>

<?php /**PATH C:\xampp\htdocs\digikala\resources\views/include/horizontal_product_list.blade.php ENDPATH**/ ?>