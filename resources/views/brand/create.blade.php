@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت برند ها','url'=>url('admin/brands')],
         ['title'=>'افزودن برند جدید','url'=>url('admin/brands/create')]
    ]])
    <div class="panel">
        <div class="header">افزودن برند جدید</div>

        <div class="panel_content">

            {!! Form::open(['url' => 'admin/brands','files'=>true]) !!}

            <div class="form-group">

                {{ Form::label('brand_name','نام برند : ') }}
                {{ Form::text('brand_name',null,['class'=>'form-control']) }}
                @if($errors->has('brand_name'))
                    <span class="has_error">{{ $errors->first('brand_name') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('brand_ename','نام انگلیسی برند : ') }}
                {{ Form::text('brand_ename',null,['class'=>'form-control']) }}
                @if($errors->has('brand_ename'))
                    <span class="has_error">{{ $errors->first('brand_ename') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('tozihat','توضیحات : ') }}
                {{ Form::textArea('tozihat',null,['class'=>'form-control brand_tozihat']) }}

            </div>



            <div class="form-group">
                <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                <div onclick="select_file()" class="btn btn-primary">انتخاب ایکون برند</div>
                @if($errors->has('pic'))
                    <span class="has_error">{{ $errors->first('pic') }}</span>
                @endif
            </div>

            <div class="form-group">
                <img  onclick="select_file()" style="margin-top:0px" id="output">

            </div>
            <button class="btn btn-success">ثبت برند</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
