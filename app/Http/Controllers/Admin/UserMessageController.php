<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use App\Message;
use App\Seller;
use App\User;
use Illuminate\Http\Request;

class UserMessageController extends Controller
{
    protected $url_param='';
    protected $type='';
    protected $user;
    public function index($id,Request $request)
    {
        $this->setUserType($request,$id);
        $messages=Message::getMessage($id, $this->type,$request->all());
        return view('messages.user_message_list', [
            'user' => $this->user,
            'messages'=> $messages,
            'req'=>$request,'
            type'=> $this->type,
            'url_param'=>$this->url_param
        ]);
    }
    public function create($id,Request $request)
    {
        $this->setUserType($request,$id);
        return view('messages.add_message',['user'=> $this->user,'type'=> $this->type,'url_param'=>$this->url_param]);
    }
    public function store($id,MessageRequest $request)
    {
        $this->setUserType($request,$id);
        $user_id=$request->user()->id;
        addMessage($request,$this->user, $user_id,0,$this->type);
        return redirect('admin/'.$this->url_param.'/'.$id.'/messages')->with('message', 'ثبت با موفقیت انجام شد');
    }
    public function show($user_id,$id,Request $request)
    {
        $this->setUserType($request,$user_id);
        $message = Message::with(['getAnswer'=>function($query){
            $query->with(['to','from']);
        }])->where(['id' => $id, 'user_id' => $user_id, 'user_type' =>$this->type,'parent_id'=>0])->firstOrFail();
        if( $message->status==1){
            $message->status=0;
        }
        $message->update();
        return view('messages.message_content', ['user' => $this->user, 'message'=> $message,'type'=> $this->type,'url_param'=>$this->url_param]);
    }
    public function addAnswer($user_id,$id,MessageRequest $request)
    {
        $admin_id = $request->user()->id;
        $this->setUserType($request,$user_id);
        $message = Message::where(['id' => $id, 'user_id' => $user_id, 'user_type' => $this->type, 'parent_id' => 0])->firstOrFail();
        addMessage($request, $this->user, $admin_id,$id,$this->type,$message);
        return redirect()->back()->with('message', 'ثبت با موفقیت انجام شد');
    }
    protected function  setUserType($request,$id)
    {
        $uri=$request->route()->uri;
        $e=explode('/',$uri);
        if($e[1]=='users'){
            $this->url_param='users';
            $this->type='App\User';
            $this->user = User::findOrFail($id);
        }
        else {
            $this->url_param = 'sellers';
            $this->type = 'App\Seller';
            $this->user = Seller::findOrFail($id);
        }
    }
}
