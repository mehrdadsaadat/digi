@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت دسته ها','url'=>url('admin/category')]]])
    <div class="panel">

        <div class="header">
            مدیریت دسته ها

            @include('include.item_table',['count'=>$trash_cat_count,'route'=>'admin/category','title'=>'دسته'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="string" class="form-control" value="{{ $req->get('string','') }}" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام دسته</th>
                        <th>دسته والد</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($category as $key=>$value)
                        @php $i++; @endphp
                        <tr id="{{ $value->id }}">
                            <td>
                                <input type="checkbox" name="category_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->getParent->name }}</td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/category/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی دسته' onclick="restore_row('{{ url('admin/category/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این دسته مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف دسته' onclick="del_row('{{ url('admin/category/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این دسته مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف دسته برای همیشه' onclick="del_row('{{ url('admin/category/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این دسته مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($category)==0)
                        <tr>
                            <td colspan="5">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $category->links() }}
        </div>
    </div>

@endsection
@section('head')
    <link href="{{ asset('css/contextmenu.css') }}" rel="stylesheet" />
@endsection
@section('footer')
    <script type="text/javascript" src="{{ url('js/contextmenu.js') }}"></script>
    <script>
        let id=0;
        var menu= new Contextmenu({
            name:"menu",
            wrapper:".table",
            trigger: "tbody tr",
            item:[
                {
                    "name":"ثبت لیست مشخصات فنی",
                    "func":"setItems()",
                    "disable":false,
                },
                {
                    "name":"ثبت لیست فیلترها",
                    "func":"setFilters()",
                    "disable":false
                },
                {
                    "name":"ثبت معیار تعیین هزینه",
                    "func":"setPriceVariation()",
                    "disable":false
                }

            ],
            target:"_blank",
            beforeFunc: function (ele) {
                id = $(ele).attr('id');
            }
        });

        function setItems() {
            const url='category/'+id+"/items";
            window.open(url, '_blank');
        }
        function setFilters() {
            const url='category/'+id+"/filters";
            window.open(url, '_blank');
        }
        function  setPriceVariation() {
            const url='category/'+id+"/price_variation";
            window.open(url, '_blank');
        }


    </script>
@endsection
