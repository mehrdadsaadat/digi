<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'مدیریت دسته ها','url'=>url('admin/category')]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت دسته ها

            <?php echo $__env->make('include.item_table',['count'=>$trash_cat_count,'route'=>'admin/category','title'=>'دسته'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                <?php if(isset($_GET['trashed']) && $_GET['trashed']==true): ?>
                    <input type="hidden" name="trashed" value="true">
                <?php endif; ?>
                <input type="text" name="string" class="form-control" value="<?php echo e($req->get('string','')); ?>" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام دسته</th>
                        <th>دسته والد</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr id="<?php echo e($value->id); ?>">
                            <td>
                                <input type="checkbox" name="category_id[]" class="check_box" value="<?php echo e($value->id); ?>"/>
                            </td>
                            <td><?php echo e(replace_number($i)); ?></td>
                            <td><?php echo e($value->name); ?></td>
                            <td><?php echo e($value->getParent->name); ?></td>
                            <td>
                                <?php if(!$value->trashed()): ?>
                                <a href="<?php echo e(url('admin/category/'.$value->id.'/edit')); ?>"><span class="fa fa-edit"></span></a>
                                <?php endif; ?>

                                <?php if($value->trashed()): ?>
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی دسته' onclick="restore_row('<?php echo e(url('admin/category/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از بازیابی این دسته مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                <?php endif; ?>

                                <?php if(!$value->trashed()): ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف دسته' onclick="del_row('<?php echo e(url('admin/category/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این دسته مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                <?php else: ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف دسته برای همیشه' onclick="del_row('<?php echo e(url('admin/category/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این دسته مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if(sizeof($category)==0): ?>
                        <tr>
                            <td colspan="5">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </form>

            <?php echo e($category->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(asset('css/contextmenu.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(url('js/contextmenu.js')); ?>"></script>
    <script>
        let id=0;
        var menu= new Contextmenu({
            name:"menu",
            wrapper:".table",
            trigger: "tbody tr",
            item:[
                {
                    "name":"ثبت لیست مشخصات فنی",
                    "func":"setItems()",
                    "disable":false,
                },
                {
                    "name":"ثبت لیست فیلترها",
                    "func":"setFilters()",
                    "disable":false
                },
                {
                    "name":"ثبت معیار تعیین هزینه",
                    "func":"setPriceVariation()",
                    "disable":false
                }

            ],
            target:"_blank",
            beforeFunc: function (ele) {
                id = $(ele).attr('id');
            }
        });

        function setItems() {
            const url='category/'+id+"/items";
            window.open(url, '_blank');
        }
        function setFilters() {
            const url='category/'+id+"/filters";
            window.open(url, '_blank');
        }
        function  setPriceVariation() {
            const url='category/'+id+"/price_variation";
            window.open(url, '_blank');
        }


    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/category/index.blade.php ENDPATH**/ ?>