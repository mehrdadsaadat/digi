@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت صفحات اضافی','url'=>url('admin/pages')],
         ['title'=>'ویرایش صفحه','url'=>url('admin/pages/'.$page->id.'/edit')]
    ]])


    <div class="panel">

        <div class="header">
            ویرایش صفحه - {{ $page->title }}
        </div>

        <div class="panel_content">
            {!! Form::model($page,['url' => 'admin/pages/'.$page->id]) !!}

            {{ method_field('PUT') }}
            <div class="form-group">

                {{ Form::label('title','عنوان محصول : ') }}
                {{ Form::text('title',null,['class'=>'form-control total_width_input']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::textarea('content',null,['class'=>'form-control ckeditor']) }}
                @if($errors->has('content'))
                  <span class="has_error">{{ $errors->first('content') }}</span>
                @endif
            </div>




            <p class="message_text">برچسب ها با استفاده از (،) از هم جدا شود</p>
            <div class="form-group">
                <input type="text" name="tag_list"  id="tag_list" class="form-control" placeholder="برجسب های محصول">
                <div class="btn btn-success" onclick="add_tag()">افزودن</div>
                <input type="hidden" value="{{ $page->keywords }}" name="keywords" id="keywords">
            </div>

            <div id="tag_box">
                @php
                $keywords=$page->keywords;
                $e=explode(',',$keywords);
                $i=1;
                @endphp
                @if(is_array($e))
                    @foreach($e as $key=>$value)
                      @if(!empty($value))

                            <div class="tag_div" id="tag_div_{{ $i }}">
                                <span  class="fa fa-remove" onclick="remove_tag({{ $i }},'{{ $value }}')"></span>
                                {{ $value }}
                           </div>
                            @php $i++;  @endphp
                      @endif

                    @endforeach
                @endif
            </div>
            <div style="clear:both"></div>

            <div class="form-group">

                {{ Form::label('description','توضیحات مختصر در مورد محصول (حداکثر 150 کاراکتر) : ',['style'=>'color:red;width:100%']) }}
                {{ Form::textarea('description',null,['class'=>'form-control','id'=>'description']) }}
                @if($errors->has('description'))
                    <span class="has_error">{{ $errors->first('description') }}</span>
                @endif
            </div>

            <div class="form-group">

            <button class="btn btn-primary">ویرایش صفحه</button>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('footer')
<script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
@endsection
