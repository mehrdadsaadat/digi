<?php $__env->startSection('content'); ?>

    <div id="auth_box">
        <div class="auth_box_title">
            <span>تایید شماره تلفن همراه</span>
        </div>

        <div class="alert alert-success">

           <span>برای شماره موبایل <?php echo e(Session::get('mobile_number')); ?> کد تایید ارسال شد</span>
            <a href="<?php echo e(url('register')); ?>" class="data_link">ویرایش شماره</a>
        </div>

        <div style="margin:<?php echo e($margin); ?>px">

            <form method="POST" action=" <?php echo e(route('active_account')); ?>" id="active_account_form">
            <?php echo csrf_field(); ?>
                <div class="form-group">
                    <div class="field_name">کد تایید را وارد نمایید</div>
                    <input type="hidden" value="<?php echo e(Session::get('mobile_number')); ?>" id="user_mobile" name="mobile">
                    <div class="number_input_div">
                        <input type="text" name="active_code" max="6" value="<?php echo e(old('active_code')); ?>" class="number_input number" maxlength="6">
                    </div>
                    <div class="line_box">
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                    </div>

                    <p id="resend_active_code">
                        <span>ارسال مجدد کد</span>
                        <span id="timer"></span>
                    </p>

                    <?php if(Session::has('validate_error')): ?>
                        <p class="alert alert-danger"><?php echo e(Session::get('validate_error')); ?></p>
                    <?php endif; ?>

                    <div class="send_btn register_btn" id="active_account_btn">
                        <span class="line"></span>
                        <span class="title">نهایی کردن ثبت نام</span>
                    </div>
                </div>
            </form>
        </div>



        <div class="alert alert-warning">
            <span>قبلا در سایت ثبت نام کرده اید ؟</span>
            <span>
                 <a class="data_link" href="<?php echo e(route('login')); ?>">وارد شوید</a>
            </span>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script>
        startTime();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.$layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/auth/confirm.blade.php ENDPATH**/ ?>