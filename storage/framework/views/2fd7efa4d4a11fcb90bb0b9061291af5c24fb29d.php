<div class="review_box">
    <h2 class="headline">
        نقد و بررسی اجمالی
    <?php if(!empty($product->ename) && $product->ename!='null'): ?>
            <span><?php echo e($product->ename); ?></span>
        <?php endif; ?>
    </h2>
    <?php if(!empty($product->tozihat)): ?>
        <div class="tozihat">
           <div class="content">
               <div id="product_tozihat">
                   <?php echo $product->tozihat; ?>

               </div>
               <a class="more_content">
                   <span>ادامه مطلب</span>
               </a>
           </div>
        </div>

    <?php endif; ?>
    <?php $__currentLoopData = $review; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(empty($value->title)): ?>
            <div class="review_tozihat">
                <h4>نقد و بررسی تخصصی</h4>
                <?php echo $value->tozihat; ?>

            </div>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php $__currentLoopData = $review; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(!empty($value->title)): ?>
            <div class="item_row">
                <button class="expert_button"></button>
                <h3><?php echo e($value->title); ?></h3>
                <div class="content">
                    <?php
                    $find='style="width:100%"';
                    $replace='class="review_image"';
                    $tozihat=str_replace($find,$replace,$value->tozihat);
                    ?>
                    <?php echo $tozihat; ?>

                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>

<?php /**PATH C:\xampp\htdocs\digikala\resources\views/include/product_review.blade.php ENDPATH**/ ?>