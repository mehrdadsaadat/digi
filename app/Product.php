<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Mail;
use App\Mail\ProductStatus;
class Product extends Model
{
    use SoftDeletes;
    protected $table='products';
    protected $fillable=['title','ename','product_url','show','view','keywords',
        'description','special','cat_id','image_url','tozihat','brand_id','status','price','discount_price','order_number','use_for_gift_cart','ready_to_shipment','fake'];
    public static function ProductStatus()
    {
        $array=array();
        $array[-3]='رد شده';
        $array[-2]='در انتظار بررسی';
        $array[-1]='توقف تولید';
        $array[0]='ناموجود';
        $array[1]='منتشر شده';

        return $array;
    }
    public static function getData($request)
    {
        $string='?';
        $products=self::orderBy('id','DESC')->with('getSeller');
        if(inTrashed($request)){
            $products=$products->onlyTrashed();
            $string=create_paginate_url($string,'trashed=true');
        }
        if(array_key_exists('string',$request) && !empty($request['string']))
        {
            $products=$products->where('title','like','%'.$request['string'].'%');
            $products=$products->orWhere('ename','like','%'.$request['string'].'%');
            $string=create_paginate_url($string,'string='.$request['string']);
        }
        $products=$products->paginate(10);
        $products->withPath($string);
        return $products;
    }
    public function  getSeller()
    {
        return $this->hasOne(Seller::class,'id','seller_id')
            ->withDefault(['brand_name'=>config('shop-info.shop_name')]);
    }
    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($product) {
            if ($product->isForceDeleting()) {
                remove_file($product->image_url,'products');
                remove_file($product->image_url,'thumbnails');
                DB::table('product_color')->where('product_id',$product->id)->delete();
                DB::table('item_value')->where('product_id',$product->id)->delete();
            }
        });
    }
    public function getCat()
    {
        return $this->hasOne(Category::class,'id','cat_id')->withDefault(['name'=>'']);
    }
    public function getBrand()
    {
        return $this->hasOne(Brand::class,'id','brand_id')
            ->withDefault(['brand_name'=>'','brand_ename'=>'']);
    }
    public function getProductColor()
    {
        return $this->hasMany(ProductColor::class,'product_id','id');
    }
    public function getProductWarranty()
    {
        return $this->hasMany(ProductWarranty::class,'product_id','id')
            ->where('product_number','>',0)
            ->orderBy('price2','ASC');
    }
    public function getFirstProductPrice(){
        return $this->hasOne(ProductWarranty::class,'product_id','id')
            ->orderBy('price2','ASC')
            ->select(['id','product_id','price1','price2','offers_last_time','offers','seller_id']);
    }
    public function getItemValue()
    {
       return $this->hasMany(ItemValue::class,'product_id','id');
    }
    public function Gallery()
    {
        return $this->hasMany(ProductGallery::class,'product_id','id')->orderBy('position','ASC');
    }
    public static function setProductStatus($product,$request,$old_status)
    {
        $seller_info=null;
        if($product->seller_id>0)
        {
            $seller_info=Seller::where('id',$product->seller_id)->withTrashed()->first();
        }
        $reject_message=$request->get('reject_message');
        if(!empty($reject_message))
        {
            $user_id=$request->user()->id;
            $time=time();
            DB::table('reject_message')->insert([
                'product_id'=>$product->id,
                'time'=>$time,
                'user_id'=>$user_id,
                'tozihat'=>$reject_message
            ]);
            if($seller_info && !empty($seller_info->email))
            {
                Mail::to($seller_info->email)->queue(new ProductStatus($product->title,$product->status,$reject_message));
            }
        }
        else if($old_status!=$product->status)
        {
            if($seller_info && !empty($seller_info->email))
            {
                Mail::to($seller_info->email)->queue(new ProductStatus($product->title,$product->status,''));

            }
        }
    }
    public function sellerList()
    {
        return $this->hasMany(SellerProduct::class,'product_id','id');
    }
    public function DigiWarranty()
    {
        return $this->hasOne(ProductWarranty::class,'product_id','id')->select(['id','product_id'])->where('product_number','>',0);
    }
    public function getWarranty(Type $var = null)
    {
        return $this->hasMany(ProductWarranty::class,'product_id','id')->where('product_number','>',0);
    }
    public static function getPriceItem($cat_id)
    {
        $items=[];
        $cat=Category::where('id',$cat_id)->first();
        if($cat){
            $parent_cat=Category::where('id',$cat->parent_id)->first();
            $catId=$parent_cat ? [$parent_cat->id,$cat->id] : [$cat->id];
            $items=Color::where('type',2)->whereIn('cat_id',$catId)
                ->orderBy('position','ASC')->get();
        }
        return $items;
    }
}
