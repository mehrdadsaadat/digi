<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت کاربران','url'=>url('admin/users')],
         ['title'=>'ویرایش اطلاعات کاربر','url'=>url('admin/users/'.$user->id.'/ediy')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="panel">

        <div class="header">
            ویرایش اطلاعات کاربر - <?php echo e($user->mobile); ?>

        </div>

        <div class="panel_content">


            <?php echo Form::model($user,['url' => 'admin/users/'.$user->id]); ?>


            <?php echo method_field('PUT'); ?>

            <div class="form-group">

                <?php echo e(Form::label('name','نام و نام خانوادگی : ')); ?>

                <?php echo e(Form::text('name',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('name')): ?>
                    <span class="has_error"><?php echo e($errors->first('name')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('username','نام کاربری (برای نقش مدیر) : ')); ?>

                <?php echo e(Form::text('username',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('username')): ?>
                    <span class="has_error"><?php echo e($errors->first('username')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('mobile','شماره موبایل : ')); ?>

                <?php echo e(Form::text('mobile',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('mobile')): ?>
                    <span class="has_error"><?php echo e($errors->first('mobile')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('password','کلمه عبور : ')); ?>

                <?php echo e(Form::password('password',['class'=>'form-control'])); ?>

                <?php if($errors->has('password')): ?>
                    <span class="has_error"><?php echo e($errors->first('password')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <?php echo e(Form::label('account_status','وضعیت اکانت : ')); ?>

                <?php echo e(Form::select('account_status',['active'=>'فعال','Inactive'=>'غیر فعال'],null,['class'=>'selectpicker'])); ?>

            </div>

            <div class="form-group">
                <?php echo e(Form::label('role','نقش کاربری : ')); ?>

                <?php echo e(Form::select('role',$roles,null,['class'=>'selectpicker'])); ?>

            </div>
            <button class="btn btn-primary">ویراش اطلاعات کاربر</button>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/users/edit.blade.php ENDPATH**/ ?>