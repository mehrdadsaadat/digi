<?php
namespace App\Http\Controllers\api;
use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\SearchProduct;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getCategory(){
        return Category::select(['id','name'])->with('getChild')->where('parent_id',0)->get();
    }
    public function getChildCategory($id){
        return Category::select(['id','name','img'])->where('parent_id',$id)->get();
    }
    public function category_new_product($cat_id){
        $category=Category::where('parent_id',$cat_id)->pluck('id','id')->toArray();
        return Product::where(['status'=>1])->whereIn('cat_id',$category)
            ->whereHas('getFirstProductPrice')
            ->with('getFirstProductPrice')
            ->orderBy('id','DESC')
            ->select(['id','title','price','discount_price','image_url'])
            ->limit(10)->get();
    }
    public function category_best_selling_product($cat_id){
        $category=Category::where('parent_id',$cat_id)->pluck('id','id')->toArray();
        return Product::where(['status'=>1])->whereIn('cat_id',$category)
            ->whereHas('getFirstProductPrice')
            ->with('getFirstProductPrice')
            ->orderBy('order_number','DESC')
            ->select(['id','title','price','discount_price','image_url','order_number'])
            ->limit(10)->get();
    }
    public function product_getList(Request $request){
       // sleep(5);
        $searchProduct=new SearchProduct($request);
        $cat_id=$request->get('cat_id',null);
        if($cat_id){
          $category=Category::with('getChild.getChild')
            ->where('id',$cat_id)->whereNull('search_url')->firstOrFail();
          $searchProduct->set_product_category($category);
        }
        $searchProduct->brands=$request->get('brand',null);
        return $result=$searchProduct->getProduct();
    }
}
