<div class="index-cat-list container-fluid">

    <ul>
        <li class="cat_hover">
            <div></div>
        </li>
        <?php $__currentLoopData = $catList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="cat_item">
                <a href="<?php echo e(url('main/'.$value->url)); ?>"><?php echo e($value->name); ?></a>
                <?php if(sizeof($value->getChild)>0): ?>
                    <div class="li_div" >
                        <?php
                        $c=0;
                        ?>
                        <?php if(sizeof($value->getChild)>0): ?> <?php if($c==0): ?> <ul class="list-inline subList"> <?php endif; ?> <?php endif; ?>
                            <?php $__currentLoopData = $value->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($value2->notShow==0): ?>
                                    <?php if(get_show_category_count($value2->getChild)>=(14-$c)): ?> <?php $c=0 ?>  </ul> <ul class="list-inline subList"> <?php endif; ?>
                            <li>
                                <a class="child_cat" href="<?php echo e(get_cat_url($value2)); ?>">
                                    <span class="fa fa-angle-left"></span>
                                    <span><?php echo e($value2->name); ?></span>
                                </a>
                                <ul>
                                    <?php $__currentLoopData = $value2->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key3=>$value3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($value3->notShow==0): ?>
                                            <li>
                                                <a href="<?php echo e(get_cat_url($value3)); ?>"><?php echo e($value3->name); ?></a>
                                            </li>
                                            <?php $c++; ?>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                            <?php $c++ ?>
                            <?php if($c==13): ?>  </ul> <?php $c=0; ?> <ul class="list-inline subList"> <?php endif; ?>
                            <?php else: ?>
                                <?php $__currentLoopData = $value2->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key3=>$value3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if(get_show_category_count($value3->getChild)>=(14-$c)): ?> <?php $c=0 ?>  </ul> <ul class="list-inline subList"> <?php endif; ?>
                            <?php if($value3->notShow==0): ?>
                                <li>
                                    <a class="child_cat" href="<?php echo e(get_cat_url($value3)); ?>">
                                        <span class="fa fa-angle-left"></span>
                                        <span><?php echo e($value3->name); ?></span>
                                    </a>
                                    <ul>
                                        <?php $__currentLoopData = $value3->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key4=>$value4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($value4->notShow==0): ?>
                                                <li>
                                                    <a href="<?php echo e(get_cat_url($value4)); ?>"><?php echo e($value4->name); ?></a>
                                                </li>
                                                <?php $c++; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </li>
                                <?php $c++; ?>

                            <?php endif; ?>

                            <?php if($c==13): ?>  </ul> <?php $c=0; ?> <ul class="list-inline subList"> <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php endif; ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php if($c!=0): ?> </ul> <?php endif; ?>

                        <div class="show-total-sub-cat">
                            <a href="">
                                <span>مشاهده تمام دسته های </span>
                                <span><?php echo e($value->name); ?></span>
                            </a>
                        </div>

                        <?php if(!empty($value->img)): ?>
                            <a href="">
                                <div class="sub-menu-pic">
                                    <img src="<?php echo e(url('files/upload/'.$value->img)); ?>" />
                                </div>
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

            </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <li class="cat_item left_item">
            <a href="">فروش ویژه</a>
        </li>
    </ul>

</div>
<?php /**PATH C:\xampp\htdocs\digikala\resources\views/include/CategoryList.blade.php ENDPATH**/ ?>