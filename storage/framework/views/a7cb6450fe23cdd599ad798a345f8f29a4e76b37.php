<div class="item_content">

     <?php if(sizeof($product_items)>0 && $product_item_count>0): ?>
         <h3>مشخصات فنی</h3>

         <table class="item_table">
             <?php $__currentLoopData = $product_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                 <tr>
                     <td colspan="2" style="padding: 15px 0px">
                         <span class="item_name"><?php echo e($value->title); ?></span>
                     </td>
                 </tr>

                 <?php $__currentLoopData = $value->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <?php if(sizeof($value2->getValue)>0): ?>
                         <tr>
                             <td class="product_item_name">
                                 <p><?php echo e($value2->title); ?></p>
                             </td>
                             <td class="product_item_value">
                                 <p><?php echo e($value2->getValue[0]->item_value); ?></p>
                             </td>
                         </tr>

                         <?php $__currentLoopData = $value2->getValue; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key3=>$value3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                             <?php if($key3>0): ?>
                                 <tr>
                                     <td class="product_item_name">
                                     </td>
                                     <td class="product_item_value">
                                         <p><?php echo e($value3->item_value); ?></p>
                                     </td>
                                 </tr>
                             <?php endif; ?>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php endif; ?>


                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </table>
     <?php else: ?>

         <p class="empty_message">
             مشخصات فنی برای این محصول ثبت نشده
         </p>
     <?php endif; ?>
</div>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/include/product_items.blade.php ENDPATH**/ ?>