@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت پرسش های متداول','url'=>url('admin/common-question')]]])
    <div class="panel">

        <div class="header">
            مدیریت پرسش های متداول

            @include('include.item_table',['count'=>$trash_common_question_count,'route'=>'admin/common-question','title'=>'پرسش متداول'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="title" class="form-control" value="{{ $req->get('title','') }}" placeholder="عنوان پرسش ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>عنوان پرسش</th>
                        <th>دسته</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($CommonQuestion as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="common-question_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->title }}</td>
                            <td>{{ $value->getCat->title }}</td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/common-question/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی پرسش' onclick="restore_row('{{ url('admin/common-question/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این پرسش مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف پرسش' onclick="del_row('{{ url('admin/common-question/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این پرسش مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف پرسش برای همیشه' onclick="del_row('{{ url('admin/common-question/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این پرسش مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($CommonQuestion)==0)
                        <tr>
                            <td colspan="5">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $CommonQuestion->links() }}
        </div>
    </div>

@endsection
