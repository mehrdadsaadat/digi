@extends('layouts.order')

@section('content')

    <div class="order_header">
        <img src="{{ asset('files/images/shop_icon.jpg') }}" class="shop_icon">
        <ul class="checkout_steps">
            <li>
                <a class="checkout_step">
                    <div class="step_item active_item" step-title="اطلاعات ارسال"></div>
                </a>
            </li>

            <li class="active">
                <a class="checkout_step">
                    <div class="step_item active_item" step-title="پرداخت"></div>
                </a>
            </li>

            <li class="inactive">
                <a class="checkout_step">
                    <div class="step_item" step-title="اتمام خرید و ارسال"></div>
                </a>
            </li>
        </ul>
    </div>

    <div class="container-fluid">
        <div class="row headline-checkout">
            <h6>انتخاب شیوه پرداخت</h6>
        </div>
        <div class="page_row">
            <div class="page_content">

                <div class="shipping_data_box payment_box" style="margin-top:0px">
                    <span class="radio_check active_radio_check"></span>
                    <span class="label">پرداخت اینترنتی (آنلاین با تمامی کارت های بانکی)</span>
                </div>

                <h6>خلاصه سفارش</h6>

                <div class="shipping_data_box" style="padding-right:15px;padding-left: 15px">

                    <?php  $i=1;?>
                    @if($send_type==1)
                            <div class="shipping_data_box" style="padding: 0px">
                                <div class="header_box">
                                    <div>
                                        مرسوله ۱ از ۱
                                        <span>({{ replace_number(\App\Cart::get_product_count()) }} کالا)</span>
                                    </div>
                                    <div>
                                        نحوه ارسال
                                        <span>پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                                    </div>
                                    <div>
                                        ارسال از
                                        <span>
                                            @if($send_order_data['normal_send_day']==0)
                                                آماده ارسال
                                            @else
                                                {{ replace_number($send_order_data['normal_send_day']) }} روز کاری
                                            @endif
                                        </span>
                                    </div>

                                    <div>
                                        هزینه ارسال
                                        <span>{{ $send_order_data['normal_send_order_amount'] }} </span>
                                    </div>
                                </div>
                                <div class="ordering_product_list swiper-container">
                                    <div class="swiper-wrapper swiper_product_box">
                                        @foreach($send_order_data['cart_product_data'] as $product)
                                            <div class="product_info_box swiper-slide">

                                                <img src="{{ url('files/thumbnails/'.$product['product_image_url']) }}">
                                                <p class="product_title">{{ $product['product_title'] }}</p>
                                                @if($product['color_id']>0)
                                                    <p class="product_color">
                                                        @if($product['color_type']==1)
                                                            رنگ :
                                                        @else
                                                            سایز:
                                                        @endif
                                                        {{ $product['color_name'] }}</p>
                                                @endif

                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-button-next"></div>

                                </div>
                            </div>
                    @else

                        @foreach($send_order_data['delivery_order_interval'] as $key=>$value)

                            <div class="shipping_data_box" style="padding: 0px">
                                <div class="header_box">
                                    <div>
                                        مرسوله {{ replace_number($i) }} از {{ replace_number(sizeof($send_order_data['delivery_order_interval'])) }}
                                         <span>({{ replace_number(sizeof($send_order_data['array_product_id'][$key])) }} کالا)</span>
                                    </div>
                                    <div>
                                        نحوه ارسال
                                        <span>پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                                    </div>
                                    <div>
                                        ارسال از
                                        <span>
                                            @if($value['send_order_day_number']==0)
                                                آماده ارسال
                                            @else
                                             {{ replace_number($value['send_order_day_number']) }} روز کاری
                                            @endif
                                        </span>
                                    </div>

                                    <div>
                                        هزینه ارسال
                                        <span>{{ $value['send_fast_price'] }} </span>
                                    </div>
                                </div>
                                <div class="ordering_product_list swiper-container">
                                    <div class="swiper-wrapper swiper_product_box">
                                        @foreach($send_order_data['array_product_id'][$key] as $key2=>$value2)
                                            <div class="product_info_box swiper-slide">
                                                <?php
                                                $product=$send_order_data['cart_product_data'][$value2.'_'.$key2];
                                                ?>
                                                <img src="{{ url('files/thumbnails/'.$product['product_image_url']) }}">
                                                <p class="product_title">{{ $product['product_title'] }}</p>
                                                @if($product['color_id']>0)
                                                  <p class="product_color">
                                                      @if($product['color_type']==1)
                                                          رنگ :
                                                      @else
                                                          سایز:
                                                      @endif

                                                      {{ $product['color_name'] }}</p>
                                                @endif

                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-button-next"></div>
                                </div>
                            </div>

                            <?php $i++; ?>
                        @endforeach
                    @endif
                </div>

                <discount-box></discount-box>
                <gift-cart></gift-cart>

            </div>
            <div class="page_aside">
                <div class="order_info" style="margin-top: 0px">
                    <?php
                    $cart_final_price=$send_type==1 ? $send_order_data['integer_normal_cart_price'] : $send_order_data['integer_fasted_cart_amount'];
                    $final_price=Session::get('final_price',0);
                    ?>
                    <ul>
                        <li>
                            <span>مبلغ کل </span>
                            <span>({{ replace_number(\App\Cart::get_product_count()) }}) کالا</span>
                            <span class="left">{{ replace_number(number_format($final_price)) }} تومان</span>
                        </li>


                        <li>
                            <span>هزینه ارسال</span>
                            <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="هزینه ارسال مرسولات می‌تواند وابسته به شهر و آدرس گیرنده متفاوت باشد. در صورتی که هر یک از مرسولات حداقل ارزشی برابر با ۱۵۰هزار تومان داشته باشد، آن مرسوله بصورت رایگان ارسال می‌شود."></span>
                            <span class="left" id="total_send_order_price">
                                <?= $send_type==1 ? $send_order_data['normal_send_order_amount'] :  $send_order_data['total_fast_send_amount']  ?>
                            </span>
                        </li>

                        <li class="discount_li" @if(Session::get('discount_value',0)>0) style="display: block" @endif>
                            <span>تخفیف</span>
                            <span class="left" id="discount_value">
                                {{ replace_number(number_format(Session::get('discount_value',0))). ' تومان' }}
                            </span>
                        </li>

                        <li class="gift_li" @if(Session::get('gift_value',0)>0) style="display: block" @endif>
                            <span>کارت هدیه</span>
                            <span class="left" id="gift_cart_amout">
                                {{ replace_number(number_format(Session::get('gift_value',0))). ' تومان' }}
                            </span>
                        </li>

                    </ul>
                    <div class="checkout_devider"></div>

                        <div class="checkout_content">
                            <p style="color:red">مبلغ قابل پرداخت : </p>
                            <p id="final_price">{{ replace_number(number_format($cart_final_price)) }} تومان</p>
                        </div>
                        <a href="{{ url('order/payment') }}">
                            <div class="send_btn checkout">
                                <span class="line"></span>
                                <span class="title">پرداخت و ثبت نهایی سفارش</span>
                            </div>
                        </a>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('head')
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}" />
@endsection
@section('footer')

    <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        const swiper=new Swiper('.swiper-container',{
            slidesPerView:5,
            spaceBetween:0,
            navigation:{
                nextEl:'.swiper-button-next',
                prevEl:'.swiper-button-prev'
            }
        });
    </script>
@endsection
