@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت مرسوله ها','url'=>url('admin/orders/submission')],
         ['title'=>'جزییات مرسوله','url'=>url('admin/orders/submission/'.$submission_info->id)]
      ]])
    <div class="panel">

        <div class="header">
          جزییات مرسوله {{ $submission_info->id }}
        </div>
        @php use App\Lib\Jdf;use App\Order;$Jdf=new Jdf(); $OrderStatus=Order::OrderStatus() @endphp
        <div class="panel_content">




            <?php

               $order=$submission_info->getOrder;
            ?>

            <table class="table table-bordered order_table_info" style="width: 100% !important;">

                <tr>
                    <td>
                        تحویل گیرنده:
                        <span>{{ $order->getAddress->name }}</span>
                    </td>
                    <td>
                        شماره تماس تحویل گیرنده:
                        <span>{{ replace_number($order->getAddress->mobile ) }}</span>
                    </td>
                </tr>

                <tr>
                    <td>
                        آدرس تحویل گیرنده:
                        <span>{{ $order->getAddress->getProvince->name.' '. $order->getAddress->getCity->name.' '. $order->getAddress->address }}</span>
                    </td>
                    <td>
                        شماره سفارش
                        <span>{{ replace_number($order->order_id) }}</span>
                    </td>
                </tr>

                <tr>
                    <td>
                        مبلغ قابل پرداخت:
                        <span>{{ replace_number(number_format($order->price)) }} تومان</span>
                    </td>
                    <td>
                        مبلغ کل :
                        <span>{{ replace_number(number_format($order->total_price)) }} تومان</span>
                    </td>
                </tr>
            </table>

            <a href="{{ url('admin/orders/submission/factor/'.$submission_info->id) }}" class="btn btn-primary" target="_blank"  style="margin-bottom:20px">نمایش فاکتور</a>

            <div class="order_info_div" style="width:100%">

                <div class="header">
                    {{ \App\Order::getOrderStatus($submission_info->send_status,$OrderStatus) }}
                </div>

                <order-step :steps="{{ json_encode($OrderStatus) }}" :send_status="{{ $submission_info->send_status }}" :order_id="{{ $submission_info->id }}"></order-step>



                <table class="table table-bordered order_table_info">

                    <tr>
                        <td>
                            کد مرسوله:
                            <span>{{ replace_number($submission_info['id'])  }}</span>
                        </td>
                        <td>
                            زمان تحویل:
                            <span>{{ $submission_info['delivery_order_interval'] }}</span>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            نحوه ارسال:
                            <span>پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                        </td>
                        <td>
                            هزینه ارسال:
                            <span>
                                    @if($submission_info['send_order_amount']==0)
                                    رایگان
                                @else
                                    {{ replace_number(number_format($submission_info['send_order_amount'])) }} تومان
                                @endif
                                </span>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="text-align: center">
                            مبلغ این مرسوله:
                            <span>
                                    {{ replace_number(number_format($order_data['order_row_amount'][$submission_info->id])) }} تومان
                                </span>
                        </td>
                    </tr>

                </table>

                <table class="table product_list_data">
                    <tr>
                        <th>نام محصول</th>
                        <th>تعداد</th>
                        <th>قیمت واحد</th>
                        <th>قیمت کل</th>
                        <th>تخفیف</th>
                        <th>قیمت نهایی</th>
                    </tr>
                    @foreach($order_data['row_data'][$submission_info->id] as $product)

                        <tr>
                            <td class="product__info">
                                <div>
                                    <img src="{{ url('files/thumbnails/'.$product['image_url']) }}" />
                                    <ul>
                                        <li class="title">
                                            {{ $product['title'] }}
                                        </li>
                                        @if($product['color_id']>0)
                                            <li>
                                                @if($product['color_type']==1)
                                                    <span> رنگ :‌</span>
                                                @else
                                                    <span> سایز :‌</span>
                                                @endif
                                                <span>{{ $product['color_name'] }}</span>
                                            </li>
                                        @endif
                                        <li>
                                            <span>گارانتی :‌ ‌</span>
                                            <span>{{ $product['warranty_name'] }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                {{ replace_number($product['product_count']) }}
                            </td>
                            <td>
                                {{ replace_number(number_format($product['product_price1'])) }} تومان
                            </td>
                            <td>
                                {{ replace_number(number_format($product['product_price1']*$product['product_count'])) }} تومان
                            </td>
                            <td>
                                <?php
                                $discount=(($product['product_price1']*$product['product_count'])-($product['product_price2']*$product['product_count']));
                                ?>
                                {{ replace_number(number_format($discount)) }} تومان

                            </td>
                            <td>
                                {{ replace_number(number_format($product['product_price2']*$product['product_count'])) }} تومان

                            </td>
                        </tr>
                    @endforeach

                </table>



                @if(sizeof($submission_info->getEvent)>0)
                    <div  style="width: 95%;margin:20px auto">
                        <p>رویداد های مرسوله</p>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ردیف</th>
                                <th>تغییر از وضعیت</th>
                                <th>به وضعیت</th>
                                <th>زمان تغییر وضعیت</th>
                                <th>توسط</th>
                            </tr>
                            </thead>

                            @foreach($submission_info->getEvent as $k=>$event)
                                <tr>
                                    <td>{{ replace_number(++$k) }}</td>
                                    <td>
                                        @if(array_key_exists($event->from,$OrderStatus))
                                            {{  $OrderStatus[$event->from] }}
                                        @endif
                                    </td>
                                    <td>
                                        @if(array_key_exists($event->to,$OrderStatus))
                                            {{  $OrderStatus[$event->to] }}
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                            $e=explode(' ',$event->created_at);
                                            $e2=explode('-',$e[0]);
                                        @endphp
                                        {{ replace_number($Jdf->gregorian_to_jalali($e2[0],$e2[1],$e2[2],'-')) }}
                                    </td>
                                    <td>
                                        @if($event->getUser)
                                            <a href="{{ url('admin/users/'.$event->getUser->id) }}" style="color: black">
                                                {{ $event->getUser->name }}
                                            </a>
                                        @endif
                                    </td>
                                </tr>

                                @if(!empty($event->tozihat))
                                    <tr>
                                        <td colspan="5" style="text-align: right">
                                            <span>توضیحات : </span>
                                            {{ $event->tozihat }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                @endif
            </div>

        </div>
    </div>
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}" />
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
    <script>
        $("#sidebarToggle").click();
        const swiper=new Swiper('.swiper-container',{
            slidesPerView:5,
            spaceBetween:0,
            navigation:{
                nextEl:'.swiper-button-next',
                prevEl:'.swiper-button-prev'
            }
        });
    </script>
@endsection
