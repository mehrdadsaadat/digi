<?php $__env->startSection('content'); ?>
    <div class="o-collapse">
        <?php
          $list=array();
          $list[0]=['title'=>'سفارشات','link'=>'user/profile/orders'];
          $list[1]=['title'=>'لیست علاقه مندی ها','link'=>'user/profile/favorite'];
          $list[2]=['title'=>'نقد و نظرات','link'=>'user/profile/comments'];
          $list[3]=['title'=>'کارت های هدیه','link'=>'user/profile/gift-cart'];
          $list[4]=['title'=>'آدرس ها','link'=>'user/profile/address'];
          $list[5]=['title'=>'پیغام های من','link'=>'user/profile/messages'];
          $list[6]=['title'=>'اطلاعات شخصی','link'=>'user/profile/personal-info'];
        ?>
        <div  class="profile_item profile_item_list">
            <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a href="<?php echo e(url($value['link'])); ?>">
                    <div class="profile_item_header">
                        <span><?php echo e($value['title']); ?></span>
                        <span class="fa fa-angle-left"></span>
                    </div>
                </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <form method="post" action="<?php echo e(url('logout')); ?>" id="logout_form"><?php echo csrf_field(); ?></form>

            <div class="profile_item_header" style="border: 0px" onclick="$('#logout_form').submit()">
                 <span>خروج</span>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.mobile', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/userPanel/profile.blade.php ENDPATH**/ ?>