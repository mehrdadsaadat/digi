<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Validator;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Rules\ValidateMobileNumber;
use App\Rules\CheckIBANCode;
use App\Rules\CheckMobileNumberFormat;
use Crypt;
class Seller extends Authenticatable
{
   use SoftDeletes;
   use HasApiTokens;
   use Notifiable;
   protected $fillable=['email','password','mobile','step','active_code','brand_name','fname','lname','province_id','city_id',
  'account_type','account_status','shaba'];
   protected $hidden = [
        'password',
   ];
   public static function first_step_register($request)
   {
       $mobile=$request->get('mobile');
       $password=$request->get('password');
       $email=$request->get('email');

       if(self::check('mobile',$mobile))
       {
           return ['status'=>'error','message'=>'شماره موبایل وارد شده قبلا توسط کاربر دیگری استفاده شده'];
       }

       if(self::check('email',$email))
       {
           return ['status'=>'error','message'=>'ایمیل وارد شده قبلا توسط کاربر دیگری استفاده شده'];
       }
       DB::table('sellers')->where(['mobile'=>$mobile])->orWhere(['email'=>$email])->delete();
       $active_code=rand(99999,1000000);

       $seller=new Seller($request->all());
       $seller->active_code=$active_code;
       $seller->password=Hash::make($password);
       $seller->step=1;
       $seller->save();
       return ['status'=>'ok','step'=>1];
   }
   public static function second_step_register($request)
   {
       $mobile=$request->get('mobile');
       $seller=Seller::where(['mobile'=>$mobile,'step'=>1])->first();
       if($seller)
       {
           $seller->step=2;
           $seller->update($request->all());
           $message=config('shop-info.shop_name')."\n";
           $message.='کد تایید';
           $message.=" : ".$seller->active_code;

           $seller->notify(new \App\Notifications\SendSms($seller->mobile,$message));
           return ['status'=>'ok','step'=>2];
       }
       else{
          return ['status'=>'error','message'=>'خطا در ثبت اطلاعات - مجددا تلاش نمایید'];
       }
   }
   public static function check($field_name,$value)
   {
       $result=DB::table('sellers')->where([$field_name=>$value,'step'=>4])->first();
       if($result)
       {
           return true;
       }
       else{
           return false;
       }
   }
   public static function check_active_code($request)
   {
       $mobile=$request->get('mobile');
       $code=$request->get('code');
       $seller=Seller::where(['mobile'=>$mobile,'step'=>2,'active_code'=>$code])->first();
       if($seller)
       {
          $seller->step=3;
          $seller->active_code ='';
          $seller->update();
          return ['status'=>'ok'];
       }
       else
       {
        return ['status'=>'error','message'=>'کد تایید وارد شده اشتباه می باشد'];
       }
   }
   public static function upload_file($request)
   {
        $image_url3=true;
        $mobile=$request->get('mobile');
        $account_type=$request->get('account_type');
        $seller=Seller::where(['mobile'=>$mobile,'step'=>3,'account_type'=>$account_type])->first();
        if($seller)
        {
            $rules=[
                'shenasname'=>'required|image',
                'cart'=>'required|image'
            ];
            if($account_type==2)
            {
                $image_url3=false;
                $rules['rooznamepic']='required|image';
            }
            $validator=Validator::make($request->all(),$rules);
            if($validator->fails()){
                return ['status'=>'error_file_type'];
            }
            else{
                $image_url1=upload_file($request,'shenasname','seller','shenasname_');
                $image_url2=upload_file($request,'cart','seller','cart_');
                if(!$image_url3)
                {
                    $image_url3=upload_file($request,'rooznamepic','seller','rooznamepic_');
                }

                if($image_url1 && $image_url2 && $image_url3)
                {
                    $insert=[
                        'seller_id'=>$seller->id,
                        'shenasname'=>$image_url1,
                        'cart'=>$image_url2,
                    ];
                    if($account_type==2)
                    {
                        $insert['rooznamepic']=$image_url3;
                    }
                    DB::table('seller_document')->insert($insert);
                    $seller->step=4;
                    $seller->update();
                    return ['status'=>'ok'];
                }
            }
        }
        else{
            return ['status'=>'error'];
        }
   }
   public function findForPassport($mobile)
   {
       return $this->where('mobile',$mobile)->first();
   }
   public static function editProfile($request)
   {
      $seller_id=$request->user()->id;
      $validate=self::ValidateProfile($request);
      if($validate['status']=='ok')
      {
          $data=$request->all();
          $seller=Seller::find($seller_id);
          if($seller->account_status=='reject')
          {
              $seller->account_status="awaiting_approval";
          }
          if(!empty($request->get('password')))
          {
              unset($data['password']);
              $seller->password=Hash::make($request->get('password'));
          }
          else{
              unset($data['password']);
          }

          $result1=add_seller_document($request,$seller_id,'shenasname');
          $result2=add_seller_document($request,$seller_id,'cart');
          $result3=add_seller_document($request,$seller_id,'rooznamepic');
          if($result1 && $result2 && $result3){
                $seller->update($data);
                if($request->get('mobile_number')==$seller->mobile){
                    return ['status' => 'ok', 'seller'=> $seller];
                }
                else{
                    $active_code = rand(99999, 1000000);
                    $seller->active_code= $active_code;
                    $seller->update();
                    // $message = config('shop-info.shop_name') . "\n";
                    // $message .= 'کد تایید';
                    // $message .= " : " . $seller->active_code;

                    // $seller->notify(new \App\Notifications\SendSms($seller->mobile, $message));
                    $encrypted = Crypt::encryptString($request->get('mobile_number'));
                    return ['status' => 'active_mobile', 'seller' => $seller, 'encrypted'=> $encrypted];
                }

          }
          else{
                return ['status' => 'server_error'];
          }

      }
      else{
           return $validate;
      }

   }
   public static function ValidateProfile($request)
   {
      $seller_id=$request->user()->id;
      $pic1=true;
      $pic2=true;
      $pic3=true;
      $rule=[
          'fname'=>'required',
          'lname'=>'required',
          'province_id'=>'required',
          'city_id'=>'required',
          'brand_name'=>'required',
          'email'=>'required|email|unique:sellers,email,'.$seller_id.'',
          'mobile_number' => ['required', 'unique:sellers,mobile,' . $seller_id . '', new CheckMobileNumberFormat],
      ];
      if(!empty($request->get('password')))
      {
         $rule['password']='required|min:6';
      }
      if(!empty($request->get('shaba')))
      {
         $rule['shaba']=[new CheckIBANCode()];
      }
      if($request->hasFile('shenasname'))
      {
          $pic1=false;
          $rule['shenasname']=['image'];
      }
      if($request->hasFile('cart'))
      {
          $pic2=false;
          $rule['cart']=['image'];
      }
      if($request->hasFile('rooznamepic'))
      {
          $pic3=false;
          $rule['rooznamepic']=['rooznamepic'];
      }
      $validator=Validator::make($request->all(),$rule);
      if($validator->fails())
      {
          return ['errors'=>$validator->errors(),'status'=>'error'];
      }
      else{
          return ['status'=>'ok'];;
      }
   }
    public static function getData($request)
    {
        $string = '?';
        $selles = self::withCount('product');
        if (inTrashed($request)) {
            $selles = $selles->onlyTrashed();
            $string = create_paginate_url($string, 'trashed=true');
        }
        if (array_key_exists('brand_name', $request) && !empty($request['brand_name'])) {
            $selles = $selles->where('brand_name', 'like', '%' . $request['brand_name'] . '%');
            $string = create_paginate_url($string, 'brand_name=' . $request['brand_name']);
        }
        $selles = $selles->orderBy('id', 'DESC')->paginate(10);
        $selles->withPath($string);
        return $selles;
    }
    public function product()
    {
        return $this->hasMany(SellerProduct::class,'seller_id','id');
    }
    public function getProvince()
    {
        return $this->hasOne(Province::class, 'id', 'province_id')
            ->withDefault(['name' => '']);
    }
    public function getCity()
    {
        return $this->hasOne(City::class, 'id', 'city_id')
            ->withDefault(['name' => ''])->select(['id', 'name']);
    }
    public static function addMessage($request)
    {
        $seller_id = $request->user()->id;
        $rule = [
            'title' => 'required',
            'content' => 'required',
            'pic' => 'file|mimes:png,jpg,jpeg,pdf,txt,rtf|max:1024',
        ];
        $validator = Validator::make($request->all(), $rule,['pic.mimes'=>'فرمت فایل انتخاب شده مجاز نمی باشد']);
        if ($validator->fails()) {
            return ['errors' => $validator->errors(), 'status' => 'error'];
        }
        else{
            $time=time();
            $message = new Message($request->all());
            $message->parent_id = 0;
            $message->user_id = $seller_id;
            $message->user_type = 'App\Seller';
            $message->from_id = $seller_id;
            $message->from_type = 'App\Seller';
            $message->to_type = 'App\User';
            $message->to_id =0;
            $message->time = $time;
            $img_url = upload_file($request, 'pic', 'upload');
            if ($img_url) {
                $message->file = $img_url;
            }
            $message->save();
            return ['status'=>'ok'];
        }
    }

    public static function addAnswer($message_id,$request)
    {
        $seller_id = $request->user()->id;
        $message = Message::where(['id' => $message_id, 'user_id' => $seller_id, 'user_type' => 'App\Seller','parent_id'=>0])->first();
        if($message){
            $rule = [
                'content' => 'required',
                'pic' => 'file|mimes:png,jpg,jpeg,pdf,txt,rtf|max:1024',
            ];
            $validator = Validator::make($request->all(), $rule, ['pic.mimes' => 'فرمت فایل انتخاب شده مجاز نمی باشد']);
            if ($validator->fails()) {
                return ['errors' => $validator->errors(), 'status' => 'error'];
            } else {
                $time = time();
                $answer = new Message($request->all());
                $answer->parent_id =$message_id;
                $answer->user_id = $seller_id;
                $answer->user_type = 'App\Seller';
                $answer->from_id = $seller_id;
                $answer->from_type = 'App\Seller';
                $answer->to_type = 'App\User';
                $answer->to_id = 0;
                $answer->time = $time;
                $img_url = upload_file($request, 'pic', 'upload');
                if ($img_url) {
                    $answer->file = $img_url;
                }
                $message->status=1;
                $message->update();
                $answer->save();
                return ['status' => 'ok', 'answer'=> $answer];
            }
        }
        else{
            return 'error';
        }
    }
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($seller){

            if(!$seller->isForceDeleting() && $seller->account_status=='active')
            {
                ProductWarranty::where(['seller_id'=>$seller->id,'status'=>1])->update(['status'=>2]);
                ProductWarranty::where(['seller_id'=>$seller->id])->delete();
            }
        });
        static::restoring(function ($seller){
            if($seller->account_status=='active')
            {
                ProductWarranty::where(['seller_id'=>$seller->id,'status'=>2])->withTrashed()
                    ->update(['status'=>1,'deleted_at'=>null]);
            }

        });
    }
}
