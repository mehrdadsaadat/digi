<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
        ['title'=>'مدیریت محصولات','url'=>url('admin/products')],
        ['title'=>'آمار فروش','url'=>url('admin/products/'.$product->id)]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

     <div class="panel">
        
        <div class="header">
            آمار فروش محصول - <?php echo e($product->title); ?>

        </div>

        <div class="panel_content">
            <sale-report :product_id='<?php echo e($product->id); ?>'></sale-report>

            <table class="table table-bordered table-striped"  style="margin-top:40px">
                <tr>
                    <td style="width:50%">میزان فروش محصول</td>
                    <td><?php echo e(replace_number(number_format($tatal_sale))); ?> تومان</td>
                </tr>
                <tr>
                    <td>کمیسون کسر شده</td>
                    <td><?php echo e(replace_number(number_format($commission))); ?> تومان</td>
                </tr>
            </table>
        </div>

     </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/product/show.blade.php ENDPATH**/ ?>