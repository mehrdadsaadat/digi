<?php $__env->startSection('content'); ?>

    <div id="auth_box">
        <div class="auth_box_title">
            <span>ثبت نام در سایت</span>
        </div>
        <div style="margin:<?php echo e($margin); ?>px">
            <form method="POST" action="<?php echo e(route('register')); ?>" id="register_form">
              <?php echo csrf_field(); ?>

                <div class="form-group">
                    <div class="field_name">شماره موبایل</div>
                    <label class="input_label user_name">
                       <input type="text" class="form-control <?php if($errors->has('mobile')): ?> validate_error_border <?php endif; ?>" name="mobile" id="register_mobile" value="<?php echo e(old('mobile')); ?>" placeholder="شماره موبایل خود را وارد نمایید">

                        <label id="mobile_error_message" class="feedback-hint"  <?php if($errors->has('mobile')): ?> style="display:block" <?php endif; ?>>
                            <?php if($errors->has('mobile')): ?>
                                <span><?php echo e($errors->first('mobile')); ?></span>
                            <?php endif; ?>
                        </label>
                    </label>
                </div>

                <div class="form-group">
                    <div class="field_name">کلمه عبور</div>
                    <label class="input_label user_pass">
                        <input type="password" class="form-control <?php if($errors->has('password')): ?> validate_error_border <?php endif; ?>" name="password" id="register_password" placeholder="کلمه عبور خود را وارد نمایید">

                        <label id="password_error_message" class="feedback-hint"  <?php if($errors->has('password')): ?> style="display:block" <?php endif; ?>>
                            <?php if($errors->has('password')): ?>
                                <span><?php echo e($errors->first('password')); ?></span>
                            <?php endif; ?>
                        </label>
                    </label>
                </div>

                <div class="register_accept">
                    <label>
                        <span class="check_box active"></span>
                        <a class="data_link" href="">حریم خصوصی</a>

                        <span> و </span>
                        <a class="data_link"  href="">شرایط و قوانین</a>

                        <span>استفاده از سرویس های سایت  را مطالعه نموده و با کلیه موارد آن موافقم. </span>
                    </label>
                </div>
                <div class="send_btn register_btn" id="register_btn">
                    <span class="line"></span>
                    <span class="title">ثبت نام در فروشگاه</span>
                </div>
            </form>

        </div>

        <div class="alert alert-warning">
            <span>قبلا در سایت ثبت نام کرده اید ؟</span>
            <span>
                 <a class="data_link" href="<?php echo e(route('login')); ?>">وارد شوید</a>
            </span>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.$layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/auth/register.blade.php ENDPATH**/ ?>