@extends('layouts.admin')

@section('content')

    <?php
       $array['users']= 'مدیریت کاربران';
       $array['sellers']= 'مدیریت فروشندگان';
    ?>
    @include('include.breadcrumb',['data'=>[
         ['title'=>$array[$url_param],'url'=>url('admin/'.$url_param)],
         ['title'=>'مدیریت پیام ها','url'=>url('admin/'.$url_param.'/'.$user->id.'/messages')],
         ['title'=>'ارسال پیام جدید','url'=>url('admin/'.$url_param.'/'.$user->id.'/messages/create')],
    ]])
    <div class="panel">
        <div class="header">
            ارسال پیام به
            @if($url_param=='users')
                {{ $user->name }}
            @else
                {{ $user->brand_name }}
            @endif
        </div>

        <div class="panel_content">

            {!! Form::open(['url' => 'admin/'.$url_param.'/'.$user->id.'/messages','files'=>true]) !!}

            <div class="form-group">
                {{ Form::label('title','عنوان پیام : ') }}
                {{ Form::text('title',null,['class'=>'form-control']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group center_item">

                {{ Form::label('content','محتوای پیام : ') }}
                {{ Form::textArea('content',null,['class'=>'form-control','style'=>'width:calc(100% - 180px);']) }}
            </div>
            <div class="form-group">
                @if($errors->has('content'))
                  <p class="has_error" style="margin-right:175px">{{ $errors->first('content') }}</p>
                @endif
            </div>



            <div class="form-group">
                {{ Form::label('pic','انتخاب فایل : ') }}
                <input type="file" name="pic" id="pic" style="direction:ltr">
                @if($errors->has('pic'))
                    <span class="has_error">{{ $errors->first('pic') }}</span>
                @endif
            </div>

            <button class="btn btn-success">ارسال پیام</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
