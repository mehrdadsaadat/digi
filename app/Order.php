<?php

namespace App;

use App\Jobs\OrderStatistics;
use App\Lib\Jdf;
use App\Lib\Mellat_Bank;
use App\Lib\ZarinPal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use Auth;
use DB;
class Order extends Model
{
    protected $colors_id='';
    protected $fillable=[
        'user_id','send_type','order_id','address_id','order_read','pay_status','date','price','total_price',
        'gift_value','gift_id','discount_value','discount_code','pay_code1','pay_code2'
    ];
    use SoftDeletes;
    protected $table='orders';
    protected $dateFormat = 'U';
    public function add_order($order_data)
    {

        $user_id=Auth::user()->id;
        $order_send_type=Session::get('order_send_type');
        $order_address_id=Session::get('order_address_id');
        $time=time();
        $order_code=substr($time,1,5).$user_id.substr($time,5,10);
        $jdf=new Jdf();

        $this->user_id=$user_id;
        $this->address_id=$order_address_id;
        $this->order_read='no';
        $this->pay_status='awaiting_payemnt';
        $this->order_id=$order_code;
        $this->date=$jdf->tr_num($jdf->jdate('Y-n-j'));
        $this->send_type=$order_send_type;
        $final_price=Session::get('final_price',0);
        if($this->send_type==1)
        {
            $final_price=$final_price+$order_data['integer_normal_send_order_amount'];
            $this->price=$order_data['integer_normal_cart_price'];
            $this->total_price=$final_price;
        }
        else{
            $final_price=$final_price+$order_data['integer_total_fast_send_amount'];
            $this->price=$order_data['integer_fasted_cart_amount'];
            $this->total_price=$final_price;
        }

        if(Session::has('gift_value') && Session::get('gift_value')>0){
            $this->gift_value=Session::get('gift_value');
            $this->gift_id=Session::get('gift_cart');
        }

        if(Session::has('discount_value') && Session::get('discount_value')>0){
            $this->discount_value=Session::get('discount_value');
            $this->discount_code=Session::get('discount_code');
        }

        DB::beginTransaction();
        try{
            $this->save();
            $this->add_order_row($order_data);
            $this->add_order_discount();
            DB::commit();
            return [
                'status'=>'ok',
                'order_id'=>$this->id,
                'price'=>$this->price
            ];
        }
        catch (\Exception $exception)
        {
            DB::rollBack();
            return [
                'status'=>'error',
            ];
        }

    }
    public function add_order_row($order_data)
    {
        $time=time();
        if(array_key_exists('cart_product_data',$order_data))
        {
            foreach ($order_data['cart_product_data'] as $key=>$value)
            {
                $p1=($value['price1']/$value['product_count']);
                $p2=($value['price2']/$value['product_count']);
                DB::table('order_products')->insert([
                    'order_id'=> $this->id,
                    'product_id'=>$value['product_id'],
                    'color_id'=>$value['color_id'],
                    'warranty_id'=>$value['warranty_id'],
                    'product_price1'=>$p1,
                    'product_price2'=>$p2,
                    'product_count'=>$value['product_count'],
                    'seller_id'=>$value['seller_id'],
                    'time'=>$time,
                    'preparation_time'=>$value['send_day'],
                    'product_warranty_id'=>$value['product_warranty_id']
                ]);
            }

            $this->add_order_info($order_data);


        }
    }
    public function add_order_discount()
    {
       $discount_info=Session::get('discount_info',array());
       $discount_value_array=Session::get('discount_value_array',array());
       foreach( $discount_value_array as $key=>$value)
       {
           if(array_key_exists($key,$discount_info))
           {
              $amount_percent=$discount_info[$key]['amount_percent'];
              settype($value,"integer");
              settype($amount_percent,"integer");
              DB::table('order_discount')->insert([
                 'cat_id'=>$key,
                 'discount_price'=>$value,
                 'total_price'=>$discount_info[$key]['price'],
                 'min_price'=>$discount_info[$key]['discount_amount'],
                 'order_id'=>$this->id,
                 'amount_percent'=>$amount_percent
              ]);
           }
       }
    }
    public function add_order_info($order_data)
    {
        $this->colors_id='';
        $jdf=new Jdf();
        $h=$jdf->tr_num($jdf->jdate('H'));
        $h=(24-$h);
        if($this->send_type==1)
        {
            $send_order_day_number=$order_data['normal_send_day'];
            settype($send_order_day_number,'integer');
            $time=$send_order_day_number*24*60*60;
            $order_send_time=time()+$time+($h*60*60);

            $order_info=new OrderInfo();
            $order_info->order_id=$this->id;
            $order_info->delivery_order_interval=$order_data['min_ordering_day'].' تا '.$order_data['max_ordering_day'];
            $order_info->send_order_amount=$order_data['integer_normal_send_order_amount'];
            $order_info->send_status=0;
            $order_info->order_send_time=$order_send_time;
            $order_info->products_id=$this->get_products_id($order_data);
            $order_info->warranty_id=$this->get_warranties_id($order_data);
            $order_info->colors_id=$this->colors_id;
            $order_info->save();
        }
        else{
            foreach ($order_data['delivery_order_interval'] as $key=>$value)
            {
                $send_order_day_number=$value['send_order_day_number'];
                settype($send_order_day_number,'integer');
                $time=$send_order_day_number*24*60*60;
                $order_send_time=time()+$time+($h*60*60);
                $order_info=new OrderInfo();
                $order_info->order_id=$this->id;
                $order_info->delivery_order_interval=$value['day_label1'].' تا '.$value['day_label2'];
                $order_info->send_order_amount=$value['integer_send_fast_price'];
                $order_info->products_id=$this->get_fasted_send_product_id($order_data,$key);
                $order_info->warranty_id=$this->get_fasted_send_warranty_id($order_data,$key);
                $order_info->colors_id=$this->get_fasted_send_colors_id($order_data,$key);
                $order_info->send_status=0;
                $order_info->order_send_time=$order_send_time;
                $order_info->save();
            }
        }
    }
    public function get_fasted_send_product_id($order_data,$key)
    {
        $collection=collect($order_data['array_product_id'][$key]);
        $products_id=$collection->implode('-');
        return $products_id;
    }
    public function get_fasted_send_warranty_id($order_data,$key)
    {
        $collection=collect($order_data['array_warranty_id'][$key]);
        $warranty_id=$collection->implode('-');
        return $warranty_id;
    }
    public function get_fasted_send_colors_id($order_data,$key)
    {
        $collection=collect($order_data['array_colors_id'][$key]);
        $colors_id=$collection->implode('-');
        return $colors_id;
    }
    public function get_products_id($order_data)
    {
        $products_id='';
        $j=0;
        foreach ($order_data['cart_product_data'] as $key=>$value)
        {
            $products_id=$products_id.$value['product_id'];
            if($j!=(sizeof($order_data['cart_product_data'])-1))
            {
                $products_id.='-';
            }
            $j++;
        }
        return $products_id;
    }
    public function get_warranties_id($order_data)
    {
        $warranties_id='';
        $j=0;
        foreach ($order_data['cart_product_data'] as $key=>$value)
        {
            $warranties_id=$warranties_id.($value['product_warranty_id'].'_'.$value['warranty_id']);
            $this->colors_id=$this->colors_id.$value['color_id'];
            if($j!=(sizeof($order_data['cart_product_data'])-1))
            {
                $warranties_id.='-';
                $this->colors_id.='-';
            }
            $j++;
        }
        return $warranties_id;
    }
    public function getProductRow()
    {
        return $this->hasMany(OrderProduct::class,'order_id','id')
            ->with(['getProduct','getColor','getWarranty']);
    }
    public function getOrderInfo()
    {
        return $this->hasMany(OrderInfo::class,'order_id','id');
    }
    public function getAddress()
    {
        return $this->hasOne(Address::class,'id','address_id')
            ->with(['getProvince','getCity'])->withDefault(['name'=>''])->withTrashed();
    }
    public static function OrderStatus()
    {
        $array=array();
        $array[-2]='خطا در اتصال به درگاه';
        $array[-1]='لغو شده';
        $array[0]='در انتظار پرداخت';
        $array[1]='تایید سفارش';
        $array[2]='آماده سازی سفارش';
        $array[3]='خروج از مرکز پردازش';
        $array[4]='تحویل به پست';
        $array[5]='دریافت از مرکز مبادلات پست';
        $array[6]='تحویل مرسوله به مشتری';
        return $array;
    }
    public static function getOrderStatus($status,$OrderStatus)
    {
        return $OrderStatus[$status];
    }
    public static function getData($request)
    {
        $string='?';
        $orders=self::orderBy('id','DESC');
        if(inTrashed($request))
        {
            $orders=$orders->onlyTrashed();
            $string=create_paginate_url($string,'trashed=true');
        }
        if(array_key_exists('user_id',$request) && !empty($request['user_id']))
        {
            $orders=$orders->where('user_id',$request['user_id']);
            $string=create_paginate_url($string,'user_id='.$request['user_id']);
        }
        if(array_key_exists('order_id',$request) && !empty($request['order_id']))
        {
            $order_id=replace_number2($request['order_id']);
            $orders=$orders->where('order_id',$order_id);
            $string=create_paginate_url($string,'order_id='.$request['order_id']);
        }
        if(array_key_exists('first_date',$request) && !empty($request['first_date']))
        {
            $first_date=getTimestamp($request['first_date'],'first');
            $orders=$orders->where('created_at','>=',$first_date);
            $string=create_paginate_url($string,'first_date='.$request['first_date']);
        }
        if(array_key_exists('end_date',$request) && !empty($request['end_date']))
        {
            $end_date=getTimestamp($request['end_date'],'end');
            $orders=$orders->where('created_at','<=',$end_date);
            $string=create_paginate_url($string,'end_date='.$request['end_date']);
        }
        $orders=$orders->paginate(10);
        $orders->withPath($string);
        return $orders;
    }
    public function getCreatedAtAttribute($value)
    {
        return $value;
    }
    public function getGiftCart()
    {
        return $this->hasOne(GiftCart::class,'id','gift_id')->withDefault(['code'=>'']);
    }
    public function getOrderProduct()
    {
        return $this->hasMany(OrderProduct::class,'order_id','id');
    }
    public static function ChartData(){
        $jdf=new Jdf();
        $date=$jdf->tr_num($jdf->jdate('Y/n')).'/1';
        $time=getTimestamp($date,'first');

        $y=$jdf->tr_num($jdf->jdate('Y'));
        $m=$jdf->tr_num($jdf->jdate('n'));
        $t=$jdf->tr_num($jdf->jdate('t'));

        $date_list=array();
        $price_array=array();
        $count_array=array();

        for($i=1;$i<=$t;$i++){
            $d=$y.'-'.$m.'-'.$i;
            $date_list[$i]=$d;
        }
        $orders=self::where(['pay_status'=>'ok'])->where('created_at','>=',$time)
        ->get();
        foreach($orders as $order){
            if(array_key_exists($order->date,$price_array)){
                $price_array[$order->date]= $price_array[$order->date]+$order->price;
                $count_array[$order->date]=$count_array[$order->date]+1;
            }
            else{
                $price_array[$order->date]=$order->price;
                $count_array[$order->date]=1;
            }
        }

        return [
            'price_array'=>$price_array,
            'count_array'=>$count_array,
            'date_list'=>$date_list
        ];
    }
    public static function SellerChartData($seller_id){
        $jdf=new Jdf();
        $date=$jdf->tr_num($jdf->jdate('Y/n')).'/1';
        $time=getTimestamp($date,'first');

        $y=$jdf->tr_num($jdf->jdate('Y'));
        $m=$jdf->tr_num($jdf->jdate('n'));
        $t=$jdf->tr_num($jdf->jdate('t'));

        $date_list=array();
        $price_array=array();
        $count_array=array();

        for($i=0;$i<$t;$i++){
            $j=$i+1;
            $d=$y.'-'.$m.'-'.$j;
            $date_list[$i]=$d;
        }
        $orders=DB::table('order_products')->where('seller_id',$seller_id)
        ->where('send_status','>=',1)
        ->where('time','>=',$time)
        ->get();
        foreach($orders as $order){
            $order_date=$jdf->tr_num($jdf->jdate('Y-n-j',$order->time));
            if(array_key_exists($order_date,$price_array)){
                $price_array[$order_date]= $price_array[$order_date]+($order->product_price2*$order->product_count);
                $count_array[$order_date]=$count_array[$order_date]+1;
            }
            else{
                $price_array[$order_date]=($order->product_price2*$order->product_count);
                $count_array[$order_date]=1;
            }
        }
        foreach($date_list as $key=>$value)
        {
            if(!array_key_exists($value,$price_array))
            {
                $price_array[$key]=0;
                $count_array[$key]=0;
            }
            else{
                $price_array[$key]=$price_array[$value];
                $count_array[$key]=$count_array[$value];
                unset($price_array[$value]);
                unset($count_array[$value]);
            }
        }
        return [
            'price_array'=>$price_array,
            'count_array'=>$count_array,
            'date_list'=>$date_list
        ];
    }
    public function getUserInfo()
    {
       return $this->hasOne(AdditionalInfo::class,'user_id','user_id')
       ->withDefault(['email'=>'']);
    }
    public static function Gateway($data)
    {
        $gateway=Setting::get_value('gateway');
        if($gateway==2){
            $zarinpal=new ZarinPal();
            $code=$zarinpal->pay($data['price']);
            if($code){
                DB::table('orders')->where('id',$data['order_id'])->update(['pay_code1'=>$code]);
            }
            else{
                DB::table('orders')->where('id',$data['order_id'])->update(['pay_status'=>'error_connect']);
                return view('shipping.location');
            }
        }
        else if($gateway==1){
            require  '../app/Lib/nusoap.php';
            $mellat_bank=new Mellat_Bank();
            $refId=$mellat_bank->pay($data['price'],$data['order_id']);
            if($refId){
                DB::table('orders')->where('id',$data['order_id'])->update(['pay_code1'=>$refId]);
                return view('shipping.location',['res'=>$refId]);
            }
            else{
                DB::table('orders')->where('id',$data['order_id'])->update(['pay_status'=>'error_connect']);
                return view('shipping.location');
            }
        }
        else{
            return view('shipping.location');
        }
    }
    public static function changeOrderStatus($order,$SaleReferenceId){
        DB::beginTransaction();
        try{
            $order->pay_status='ok';
            $order->pay_code2=$SaleReferenceId;
            $order->update();

            $order_data=new OrderData($order->getOrderInfo,$order->getProductRow,$order->user_id,'ok');
            $order_data=$order_data->getData();

            if(Session::has('gift_value') && Session::get('gift_value')>0){
                $gift_value=Session::get('gift_value');
                $gift_id=Session::get('gift_cart');
                $giftCart=GiftCart::where('id',$gift_id)->first();
                if($giftCart){
                    $giftCart->credit_used=$giftCart->credit_used+$gift_value;
                    $giftCart->update();
                }

                Session::forget('gift_value');
                Session::forget('gift_cart');
            }

            DB::table('order_infos')->where('order_id',$order->id)->update(['send_status'=>1]);
            DB::table('order_products')->where('order_id',$order->id)->update(['send_status'=>1]);

            DB::commit();
            OrderStatistics::dispatch($order);

            set_sale($order);
            // if(!empty($order->getUserInfo->email))
            // {
            //     Mail::to($order->getUserInfo->email)->queue(new \App\Mail\Order($order,$order_data));
            // }

            return view('shipping.verify',['order'=>$order,'order_data'=>$order_data]);

        }
        catch (\Exception $exception){
            return view('shipping.verify',['order'=>$order,'order_data'=>$order_data,'error_payment'=>'خطا در ثبت اطلاهات،لطفا برای بررسی خطا پیش آمده با پشتیبانی فروشگاه در ارتباط باشید']);
        }
    }
}
