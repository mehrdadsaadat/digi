@extends('layouts.admin')

@section('content')
    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت محصولات','url'=>url('admin/products')],
         ['title'=>'ثبت مشخصات فنی محصول','url'=>url('admin/products/'.$product->id.'/items')]
       ]])

    <div class="panel">

        <div class="header">
            افزودن مشخصات فنی - {{ $product->title }}
        </div>

        <div class="panel_content">

            <?php
                $filter_array=getFilterArray($filters);
            ?>
            @include('include.Alert')
            @if(sizeof($product_items)>0)
                <form method="post" id="product_items_form" action="{{ url('admin/products/'.$product->id.'/items') }}">
                   @csrf
                    @foreach($product_items as $key=>$value)
                        <div class="item_groups" style="margin-bottom:20px">
                            <p class="title">{{ $value->title }}</p>
                            @foreach($value->getChild as $key2=>$value2)
                                <div class="form-group">
                                    <label>{{ $value2->title }}</label>
                                    @if(sizeof($value2->getValue)>0)
                                        <input type="text" class="form-control item_value" value="{{ $value2->getValue[0]->item_value }}" name="item_value[{{ $value2->id }}][]">
                                    @else
                                        <input type="text" class="form-control item_value" name="item_value[{{ $value2->id }}][]">
                                    @endif

                                    @if(array_key_exists($value2->id,$filter_array))
                                        <div class="btn btn-success show_filter_box">انتخاب</div>
                                        <input type="hidden" value="{{ getFilterItemValue($filters[$filter_array[$value2->id]]->id,$product_filters) }}" class="filter_value" name="filter_value[{{ $value2->id }}][{{ $filters[$filter_array[$value2->id]]->id }}]">
                                        <div class="item_filter_box">
                                            <ul>
                                                @foreach($filters[$filter_array[$value2->id]]['getChild'] as $k=>$v)
                                                    <li>
                                                        <input @if(array_key_exists($v->id,$product_filters)) checked="checked" @endif type="checkbox" value="{{ $v->id }}">
                                                        {{ $v->title }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @else
                                        <span class="fa fa-plus-circle" onclick="add_item_value_input({{ $value2->id }})"></span>
                                    @endif
                                        <div class="input_item_box" id="input_item_box_{{ $value2->id }}">
                                        @if(sizeof($value2->getValue)>1)
                                            @foreach($value2->getValue as $item_key=>$item_value )
                                                @if($item_key>0)
                                                    <div class="form-group">
                                                        <label></label>
                                                        <input name="item_value[{{ $value2->id }}][]" value="{{ $item_value->item_value }}" type="text" class="form-control">
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                    <button class="btn btn-success">ثبت اطلاعات</button>
                </form>
            @else

            @endif
        </div>
    </div>

@endsection
