@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت محصولات','url'=>url('admin/products')]]])
    <div class="panel">

        <div class="header">
            مدیریت محصولات

            @include('include.item_table',['count'=>$trash_product_count,'route'=>'admin/products','title'=>'محصول'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php use App\Product;$i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="string" class="form-control" value="{{ $req->get('string','') }}" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <?php  $status=Product::ProductStatus(); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>تصویر محصول</th>
                        <th>عنوان</th>
                        <th>فروشنده</th>
                        <th>وضعیت محصول</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product as $key=>$value)
                        @php $i++; @endphp
                        <tr id="{{ $value->id }}">
                            <td>
                                <input type="checkbox" name="products_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td><img src="{{ url('files/thumbnails/'.$value->image_url) }}" class="product_pic"></td>
                            <td>{{ $value->title }}</td>
                            <td>
                                {{ $value->getSeller->brand_name }}
                            </td>
                            <td style="width:130px">
                                @if(array_key_exists($value->status,$status))
                                    <span class="alert @if($value->status==1) alert-success @else alert-warning @endif" style="font-size:13px;padding:5px 7px">
                                        {{ $status[$value->status] }}
                                    </span>
                                @endif
                            </td>
                            <td style="width:120px">
                                @if(!$value->trashed())
                                <a href="{{ url('admin/products/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی محصول' onclick="restore_row('{{ url('admin/products/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این محصول مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif
                                <a style="color:#000" href="{{ url('admin/products/'.$value->id) }}"><span data-toggle="tooltip" data-placement="bottom"  title='آمار فروش' class="fa fa-line-chart"></span></a>

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف محصول' onclick="del_row('{{ url('admin/products/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این محصول مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف محصول برای همیشه' onclick="del_row('{{ url('admin/products/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این محصول مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                               @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($product)==0)
                        <tr>
                            <td colspan="7">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $product->links() }}
        </div>
    </div>

@endsection

@section('head')
    <link href="{{ asset('css/contextmenu.css') }}" rel="stylesheet" />
@endsection
@section('footer')
    <script type="text/javascript" src="{{ url('js/contextmenu.js') }}"></script>
    <script>
        let id=0;
        var menu= new Contextmenu({
            name:"menu",
            wrapper:".table",
            trigger: "tbody tr",
            item:[
                {
                    "name":"ثبت تنوع قیمت",
                    "func":"setWarranty()",
                    "disable":false
                },
                {
                    "name":"ثبت مشخصات فنی",
                    "func":"setItem()",
                    "disable":false
                },
                {
                    "name":"ثبت نقد و بررسی تخصصی",
                    "func":"setReview()",
                    "disable":false
                },
                {
                    "name":"گالری تصاویر",
                    "func":"Gallery()",
                    "disable":false
                },
                {
                    "name":"ثبت فیلتر های محصول",
                    "func":"setFilter()",
                    "disable":false
                }

            ],
            target:"_blank",
            beforeFunc: function (ele) {
                id = $(ele).attr('id');
            }
        });
        function setWarranty() {
            const url='product_warranties?product_id='+id;
            window.open(url, '_blank');
        }
        function setItem() {
            const url='products/'+id+"/items";
            window.open(url, '_blank');
        }
        function setFilter() {
            const url='products/'+id+"/filters";
            window.open(url, '_blank');
        }
        function Gallery() {
            const url='products/gallery/'+id;
            window.location=url;
        }
        function setReview() {
            const url='product/review?product_id='+id;
            window.open(url, '_blank');
        }

    </script>
@endsection
