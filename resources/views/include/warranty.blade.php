<form method="post" action="{{ url('Cart') }}" id="add_cart_form">
    {{ csrf_field() }}

    <?php

    $warranty_id=0;
    $product_price1=0;
    $product_price2=0;
    ?>

    @if(sizeof($product->getProductColor)>0)
        <ul class="color_ul">
            @foreach($product->getProductColor as $key=>$value)
                @if(check_has_color_in_warranty_list($product->getProductWarranty,$value->getColor->id))
                    @if($color_id==0) <?php if(get_first_color_id($product->getProductWarranty,$value->getColor->id)) { $color_id=$value->getColor->id; } ?> @endif
                    <li class="color_li @if($color_id==$value->getColor->id) active @endif" data="{{ $value->getColor->id }}">
                        <label>
                            <span class="ui_variant_shape" style="background:#{{ $value->getColor->code }}"></span>
                            <span class="color_name">{{ $value->getColor->name }}</span>
                        </label>
                    </li>
                @endif
            @endforeach
        </ul>
    @endif

    @if(sizeof($priceItem)>0)
       <div style="padding-bottom: 20px">
           <span>سایز:</span>
           <select class="selectpicker price_item" id="priceItem">
               @foreach($priceItem as $key=>$value)
                   @if(check_has_color_in_warranty_list($product->getProductWarranty,$value->id))
                       @if($color_id==0) <?php if(get_first_color_id($product->getProductWarranty,$value->id)) { $color_id=$value->id; } ?> @endif
                       <option @if($color_id==$value->id) selected="selected" @endif value="{{ $value->id }}">{{ $value->name }}</option>
                   @endif
               @endforeach
           </select>
       </div>
    @endif

    <input type="hidden" name="color_id" id="color_id" value="{{ $color_id }}">
    @php $brand_name='' @endphp
    <p class="info_item_product">
        <?php
        $send_time=-1;
        ?>
        <span class="fa fa-check-square"></span>
        @foreach($product->getProductWarranty as $key=>$value)
            @if($color_id>0)
                @if($value->color_id==$color_id && $warranty_id==0)
                    <?php
                      $warranty_id=$value->id.'_'.$value->warranty_id;
                      $send_time=$value->send_time;
                      $product_price1=$value->price1;
                      $product_price2=$value->price2;
                      $brand_name=$value->getSeller->brand_name
                     ?>
                    {{ $value->getWarranty->name }}
                     <span id="offers_time" data="{{ $value->offers_last_time-time()>0 ? $value->offers_last_time-time()  : 0  }}"></span>
                @endif

            @else
                @if($key==0)
                    <?php
                      $send_time=$value->send_time;
                      $product_price1=$value->price1;
                      $product_price2=$value->price2;
                      $warranty_id=$value->id.'_'.$value->warranty_id;
                      $brand_name=$value->getSeller->brand_name
                    ?>
                    {{ $value->getWarranty->name }}
                    <span id="offers_time" data="{{ $value->offers_last_time-time()>0 ? $value->offers_last_time-time()  : 0  }}"></span>
                @endif
            @endif
        @endforeach
    </p>
    <p class="info_item_product">
        <span class="fa fa-home"></span>
        فروشنده : {{ $brand_name }}
    </p>
    <p class="info_item_product">
        <span class="fa fa-ambulance"></span>
        @if($send_time>-1)

            @if($send_time==0)
                <span>آماده ارسال</span>
                <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom"
                      title="این کالا در حال حاضر در انبار تنیس شاپ موجود ، آماده پردازش و ارسال است"></span>

            @else
                <span>ارسال از {{ replace_number($send_time) }} روز کاری آینده </span>
                <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom"
                title="این کالا در انبار فروشنده موجود است، برای ارسال باید برای مدت زمان ذکر شده منتظر بمانید"></span>
            @endif

        @endif
    </p>
    <input type="hidden" name="warranty_id" id="warranty_id" value="{{ $warranty_id }}">
    <input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}">

    <div style="display:inline-flex;width:100%">
        <div>
            @if($product_price2!=$product_price1)
                <del>{{ replace_number(number_format($product_price1)) }}</del>
            @endif
            <span class="final_product_price">
                {{ replace_number(number_format($product_price2)) }} تومان
            </span>
        </div>

        @if($product_price2!=$product_price1)

            <div class="product_discount" data-title="تخفیف">
                <?php
                $a=($product_price2/$product_price1)*100;
                $a=100-$a;
                $a=round($a);
                ?>
               {{ replace_number($a) }} ٪
            </div>

        @endif
    </div>
</form>
