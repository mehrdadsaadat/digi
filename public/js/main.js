$(document).ready(function () {
    $('.form_cover span').click(function () {

        $("#account_type").click();
        $('.form_cover').hide();
        document.getElementById('Legal').value=true;
    });
    $("#profile_province_id").change(function () {

        const province_id=$(this).val();
        if(parseInt(province_id)>0){
            const url=site_url+"api/get_city/"+province_id;
            $.ajax({
                type:"GET",
                url:url,
                success:function (response) {
                    let html='';
                    for (let i=0;i<response.length;i++)
                    {
                        html+='<option value='+response[i].id+'>'+response[i].name+'</option>';
                    }
                    if(html.trim()!='')
                    {
                        $("#profile_city").html(html).selectpicker('refresh');
                    }
                    else {
                        html='<option value="">انتخاب شهر</option>';
                        $("#profile_city").html(html).selectpicker('refresh');
                    }
                }
            })
        }
        else {
            html='<option value="">انتخاب شهر</option>';
            $("#profile_city").html(html).selectpicker('refresh');
        }
    });
    $("#newsletter").click(function () {
        if($(this).hasClass('active'))
        {
            $("#newsletter_input").removeAttr('checked');
            $(this).removeClass('active');
        }
        else {
            $(this).addClass('active');
            $("#newsletter_input").attr('checked',true);
        }
    });
    $("#login_btn").click(function () {
        const mobile=$("#mobile").val();
        const password=$("#password").val();
        const result1=validate_login_mobile(mobile);
        const result2=validate_login_password(password);
        if(result1 && result2)
        {
            $("#login_form").submit();
        }
    });
    $("#admin_login_btn").click(function () {
        const username=$("#username").val();
        const password=$("#password").val();
        const result1=validate_login_username(username);
        const result2=validate_login_password(password);
        if(result1 && result2)
        {
            $("#admin_login_form").submit();
        }
    });
    $("#register_btn").click(function () {
        const mobile=$("#register_mobile").val();
        const password=$("#register_password").val();
        const result1=validate_mobile(mobile);
        const result2=validate_password(password);
        if(result1 && result2)
        {
            $("#register_form").submit();
        }
    });
    $("#active_account_btn").click(function () {
        $("#active_account_form").submit();
    });
    $("#resend_active_code").click(function () {
        if(t==0)
        {
            const mobile=$("#user_mobile").val();
            $.ajaxSetup(
                {
                    'headers':{
                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                    }
                });
            const url=site_url+"ajax/resend";
            $.ajax({
                url: url,
                type:"POST",
                data:"mobile="+mobile,
                success:function (response) {
                    t=180;
                    startTime();
                },
                error:function (jqXhr,textStatus,error) {
                    t=180;
                    startTime();
                }
            });
        }
     });
     $("#forget_password_code").click(function () {
        if(t==0)
        {
            const mobile=$("#mobile").val();
            $.ajaxSetup(
                {
                    'headers':{
                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                    }
                });
            const url=site_url+"ajax/resend";
            $.ajax({
                url: url,
                type:"POST",
                data:"mobile="+mobile+'&forget_password=ok',
                success:function (response) {
                    t=180;
                    startTime();
                },
                error:function (jqXhr,textStatus,error) {
                    t=180;
                    startTime();
                }
            });
        }
     });
     $('.favorite').click(function(){
        const product_id=$(this).attr('product-id');
        $("#loading_box").show();
        $.ajaxSetup(
            {
                'headers':{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
        const url=site_url+"user/add_favorite";
        $.ajax({
            url: url,
            type:"POST",
            data:"product_id="+product_id,
            success:function (response) {
                $("#loading_box").hide();
                if(response=='ok')
                {
                    if($('.favorite span').hasClass('chosen'))
                    {
                        $('.favorite span').removeClass('chosen');
                    }
                    else{
                        $('.favorite span').addClass('chosen');
                    }
                }
            },
            error:function(xhr,textStatus,error)
            {
                $("#loading_box").hide();
                if(error=="Unauthorized")
                {
                    $("#login_box").modal('show');
                }
            }
        });
    });
    $("#share_box .fa-envelope").click(function(){
        $('.share_link_form').show();
    });
    $("#send_email").click(function(){
        const email=$("#email").val();
        const share_prroduct_id=$("#share_prroduct_id").val();
        if(email.trim()!="" && validateEmailAddress(email))
        {
            $("#share_box").modal('hide');
            $("#loading_box").show();
            $.ajaxSetup(
            {
                'headers':{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            const url=site_url+"site/share_product";
            $.ajax({
               url: url,
               type:"POST",
               data:"email="+email+'&product_id='+share_prroduct_id,
               success:function (response) {
                  $("#loading_box").hide();
                  if(response=='ok')
                  {
                     $("#email").val('');
                  }
                  else{
                    $("#share_box").modal('hide');
                    $("#share_link_error").text('خطا در ارسال ایمیل مجددا تلاش نمایید');
                 }
               },
               error:function(){
                  $("#loading_box").hide();
               }
           });
        }
    });
    $("#copy_btn").click(function()
    {
        const copy_data=$(this).attr('copy-data');
        const input=document.createElement('input');
        input.setAttribute('value',copy_data);
        input.setAttribute('id',"url_input");
        input.setAttribute('contenteditable',true);
        $('#share_box').append(input);
        input.select();
        document.execCommand('copy');
        $("#url_input").remove();
        $(this).text('کپی شد');
        setTimeout(function(){
            $("#copy_btn").text('کپی لینک');
        },3000);
    });
    $("#forget_password").click(function(){
        const mobile=$("#mobile").val();
        const result=validate_mobile(mobile);
        if(result)
        {
            $("#forget_password_form").submit();
        }
    });
    $("#update_password_btn").click(function(){
        const mobile=$("#mobile").val();
        const password=$("#password").val();
        const password_confirmation=$("#password_confirmation").val();
        const result1=validate_mobile(mobile,"#mobile");
        const result2=validate_password(password,"#password");
        const result3=validate_password_confirmation(password,password_confirmation);
        if(result1 && result2 && result3)
        {
            $("#updatePasswordForm").submit();
        }

    });

});

function validate_login_mobile(mobile_number) {

    if(mobile_number.toString().trim()=="")
    {
        $("#mobile").addClass('validate_error_border');
        $("#mobile_error_message").show().text('لطفا شماره موبایل خود را وارد نمایید');
        return false;
    }
    else if(check_mobile_number(mobile_number))
    {
        $("#mobile").addClass('validate_error_border');
        $("#mobile_error_message").show().text('شماره موبایل وارد شده معتبر نمی باشد');
        return false;
    }
    else {
        $("#mobile_error_message").hide();
        $("#mobile").removeClass('validate_error_border');
        return  true;
    }

}
function validate_login_password(password) {

    if(password.toString().trim()=="")
    {
        $("#password").addClass('validate_error_border');
        $("#password_error_message").show().text('لطفا کلمه عبور خود را وارد نمایید');
        return false;
    }
    else {
        $("#password").removeClass('validate_error_border');
        $("#password_error_message").hide();
        return true;
    }
}
function check_mobile_number(mobile_number) {
    if(isNaN(mobile_number))
    {
        return true;
    }
    else {
        if(mobile_number.toString().trim().length==11)
        {
            if(mobile_number.toString().charAt(0)=='0' && mobile_number.toString().charAt(1)=='9' )
            {
                return  false;
            }
            else
            {
                return true;
            }
        }
        else if(mobile_number.toString().trim().length==10)
        {
            if(mobile_number.toString().charAt(0)=='9')
            {
                return  false;
            }
            else
            {
                return true;
            }
        }
        else{
            return  true;
        }
    }
}
function validate_mobile(mobile_number,element) {
    const el = (element==undefined) ? "#register_mobile" : element;
    if(mobile_number.toString().trim()=="")
    {
        $(el).addClass('validate_error_border');
        $("#mobile_error_message").show().text('لطفا شماره موبایل خود را وارد نمایید');
        return false;
    }
    else if(check_mobile_number(mobile_number))
    {
        $(el).addClass('validate_error_border');
        $("#mobile_error_message").show().text('شماره موبایل وارد شده معتبر نمی باشد');
        return false;
    }
    else {
        $("#mobile_error_message").hide();
        $(el).removeClass('validate_error_border');
        return  true;
    }

}
function validate_password(password,element) {

   const el= (element==undefined) ? "#register_mobile" : element;
   if(password.toString().trim().length<6)
   {
       $(el).addClass('validate_error_border');
       $("#password_error_message").show().text('کلمه عبور باید حداقل شامل ۶ کاراکتر باشد');
       return false;
   }
   else {
       $(el).removeClass('validate_error_border');
       $("#password_error_message").hide();
       return true;
   }
}
function validate_password_confirmation(password,password_confirmation)
{
    if(password.toString().trim().length>=6)
    {
        if(password_confirmation!=password)
        {
            $("#password_confirmation").addClass('validate_error_border');
            $("#password_confirmation_error_message").show().text('تکرار کلمه عبور مطابقت ندارد');
            return false;
        }
        else{
            $("#password_confirmation").removeClass('validate_error_border');
            $("#password_confirmation_error_message").hide();
            return true;
        }
    }
    else{
        return false;
    }
}
replaceNumber=function (n) {
    n=n.toString();
    const find=["0","1","2","3","4","5","6","7","8","9"];
    const replace=["۰","۱","۲","۳","۴","۵","۶","۷","۸","۹"];
    for (let i=0;i<find.length;i++)
    {
        n=n.replace(new RegExp(find[i],'g'),replace[i]);
    }
    return n;
};
let times=null;
let t=180;
function startTime() {

    times=setInterval(function () {
        t=t-1;
        let m=Math.floor(t/60);
        let s=t-m*60;
        if(s.toString().length==1)
        {
            s="0"+s;
        }
        let text=replaceNumber("0"+m.toString())+":"+replaceNumber(s.toString());
        if(t==0)
        {
            clearInterval(times);
            times=null;
            $("#timer").text('');
        }
        else{
            $("#timer").text(text);
        }

    },1000);
}
validate_login_username=function(username){
    if(username.toString().trim()=="")
    {
        $("#username").addClass('validate_error_border');
        $("#username_error_message").show().text('لطفا نام کاربری خود را وارد نمایید');
        return false;
    }
    else {
        $("#username").removeClass('validate_error_border');
        $("#username_error_message").hide();
        return true;
    }
}
validateEmailAddress=function(email){
    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
        $("#share_link_error").text('');
        return true;
    }
    else{
        $("#share_link_error").text('آدرس ایمیل وارد شده معتبر نمی باشد');
        return false;
    }
}
number_format=function (num)
{
    num=num.toString();
    let format='';
    let counter=0;
    for (let i=num.length-1;i>=0;i--)
    {
        format+=num[i];
        counter++;
        if(counter==3){
            format+=",";
            counter=0;
        }
    }
    return format.split('').reverse().join('');
};
