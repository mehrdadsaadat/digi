<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>فاکتور</title>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <?php echo $__env->yieldContent('head'); ?>
    <link href="<?php echo e(asset('css/admin.css')); ?>" rel="stylesheet">
    <style>
        body{
            background-color: white !important;
        }
        .product_list_data tr th{
            background-color: white !important;
            color: black !important;
        }
    </style>
</head>
<body>
    <?php
    $order=$submission_info->getOrder;
    ?>
    <div class="container factor">
        <?php
            use App\Lib\Jdf;$jdf=new Jdf();
            $total_price=0;
            $order_price=0;
            $total_return_product_price=0;
        ?>
        <div class="line"></div>
        <div class="header_factor">
            <div>
                <p>
                    <span>تاریخ : <?php echo e($jdf->jdate('j-n-Y',$order->created_at)); ?>

                    </span>
                    <span>
                        شماره مرسوله : <?php echo e(replace_number($submission_info->id)); ?>

                    </span>
                    <span>
                        تعداد محصول : <?php echo e(replace_number(sizeof($order_data['row_data'][$submission_info->id]))); ?>

                    </span>
                </p>
            </div>
            <div class="title">
                فاکتور سفارش
            </div>
            <div>
                <img src="<?php echo e(asset(env("SHOP_LOGO",'files/images/shop_icon.jpg'))); ?>" class="shop_logo">
            </div>
        </div>

        <table class="table table-bordered order_table_info" style="width:100% !important">

            <tr>
                <td>
                    تحویل گیرنده:
                    <span><?php echo e($order->getAddress->name); ?></span>
                </td>
                <td>
                    شماره تماس تحویل گیرنده:
                    <span><?php echo e(replace_number($order->getAddress->mobile )); ?></span>
                </td>
            </tr>

            <tr>
                <td>
                    آدرس تحویل گیرنده:
                    <span><?php echo e($order->getAddress->getProvince->name.' '. $order->getAddress->getCity->name.' '. $order->getAddress->address); ?></span>
                </td>
                <td>
                    تعداد مرسوله:
                    <span><?php echo e(replace_number(sizeof($order->getOrderInfo))); ?></span>
                </td>
            </tr>

            <tr>
                <td>
                    مبلغ  پرداخت شده:
                    <span><?php echo e(replace_number(number_format($order->price))); ?> تومان</span>
                </td>
                <td>
                    مبلغ کل :
                    <span><?php echo e(replace_number(number_format($order->total_price))); ?> تومان</span>
                </td>
            </tr>

            <?php if(!empty($order->gift_value) && $order->gift_value>0): ?>
                <tr>
                    <td>
                       مبلغ کارت هدیه:
                        <span><?php echo e(replace_number(number_format($order->gift_value))); ?> تومان</span>
                    </td>
                    <td>
                       کد کارت هدیه:
                        <span><?php echo e($order->getGiftCart->code); ?> </span>
                    </td>
                </tr>
            <?php endif; ?>

            <?php if(!empty($order->discount_value) && $order->discount_value>0): ?>
                <tr>
                    <td>
                        مبلغ کد تخفیف:
                        <span><?php echo e(replace_number(number_format($order->discount_value))); ?> تومان</span>
                    </td>
                    <td>
                        کد تخفیف:
                        <span><?php echo e($order->discount_code); ?> </span>
                    </td>
                </tr>
            <?php endif; ?>
        </table>
        <p>جزییات مرسوله</p>
        <table class="table product_list_data">
            <thead>
                <tr>
                    <th>نام محصول</th>
                    <th>تعداد</th>
                    <th>قیمت واحد</th>
                    <th>قیمت کل</th>
                    <th>تخفیف</th>
                    <th>قیمت نهایی</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $order_data['row_data'][$submission_info->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <tr>
                    <td class="product__info">
                        <div>
                            <img src="<?php echo e(url('files/thumbnails/'.$product['image_url'])); ?>" />
                            <ul>
                                <li class="title">
                                    <?php echo e($product['title']); ?>

                                </li>
                                <li>
                                    <span>فروشنده :‌ ‌</span>
                                    <span><?php echo e($product['seller']); ?></span>
                                </li>
                                <?php if($product['color_id']>0): ?>
                                    <li>
                                        <?php if($product['color_type']==1): ?>
                                            <span> رنگ :‌</span>
                                        <?php else: ?>
                                            <span> سایز :‌</span>
                                        <?php endif; ?>
                                        <span><?php echo e($product['color_name']); ?></span>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <span>گارانتی :‌ ‌</span>
                                    <span><?php echo e($product['warranty_name']); ?></span>
                                </li>
                            </ul>
                        </div>
                    </td>
                    <td>
                        <?php echo e(replace_number($product['product_count'])); ?>

                    </td>
                    <td>
                        <?php echo e(replace_number(number_format($product['product_price1']))); ?> تومان
                    </td>
                    <td>
                        <?php
                            $total_price+=($product['product_price1']*$product['product_count']);
                        ?>
                        <?php echo e(replace_number(number_format($product['product_price1']*$product['product_count']))); ?> تومان
                    </td>
                    <td>
                        <?php
                        $discount=(($product['product_price1']*$product['product_count'])-($product['product_price2']*$product['product_count']));
                        ?>
                        <?php echo e(replace_number(number_format($discount))); ?> تومان

                    </td>
                    <td>
                        <?php
                          $order_price+=($product['product_price2']*$product['product_count']);
                        ?>
                        <?php echo e(replace_number(number_format($product['product_price2']*$product['product_count']))); ?> تومان

                    </td>
                </tr>
                <?php if($product['send_status']==-1): ?>
                           <tr>
                               <td colspan="3">
                                   <span>این کالا توسط مشتری برگشت داده شده</span>
                                   <?php if(!empty($product['tozihat'])): ?>
                                       <p style=" margin-top:10px;color:red">
                                           <?php echo e($product['tozihat']); ?>

                                       </p>
                                   <?php endif; ?>
                               </td>
                               <td colspan="3">
                                   <?php
                                       $p=($product['product_price2']*$product['product_count']);
                                   ?>
                                   <span>هزینه قابل پرداخت به کاربر :</span>
                                   <?php
                                       $return_product_price=get_return_product_price($product['cat_id'],$order_discount,$p);
                                       $total_return_product_price+=$return_product_price;
                                  ?>
                                   <?php echo e(replace_number(number_format($return_product_price)).' تومان'); ?>

                               </td>
                           </tr>
                        <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <tr>
                    <td colspan="3"  style="padding-top:20px;padding-bottom:20px">کل</td>
                    <td>
                        <?php echo e(replace_number(number_format($total_price))); ?> تومان
                    </td>
                    <td>
                        <?php echo e(replace_number(number_format(($total_price-$order_price)))); ?> تومان
                    </td>
                    <td>
                        <?php echo e(replace_number(number_format($order_price))); ?> تومان
                    </td>
                </tr>
            </tbody>

        </table>

        <table class="table table-bordered factor_price_table">
            <tr>
                <td>مبلغ کل</td>
                <td>
                    <?php echo e(replace_number(number_format($order_price))); ?> تومان
                </td>
            </tr>
            <tr>
                <td>
                    هزینه ارسال
                </td>
                <td>
                    <?php if($submission_info['send_order_amount']==0): ?>
                    رایگان
                   <?php else: ?>
                      <?php echo e(replace_number(number_format($submission_info['send_order_amount']))); ?> تومان
                  <?php endif; ?>
                </td>
            </tr>
            <?php if($total_return_product_price>0): ?>
            <tr>
                 <td style="color:red">
                   مبلغ قابل برگشت به کاربر
                 </td>
                <td>
                     <?php echo e(replace_number(number_format($total_return_product_price))); ?> تومان
                 </td>
            </tr>
            <?php endif; ?>

            <tr>
                <td>مبلغ نهایی</td>
                <td>
                    <?php
                        $final_price=$order_data['order_row_amount'][$submission_info->id]-$total_return_product_price;
                    ?>
                    <?php echo e(replace_number(number_format($final_price))); ?> تومان
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/orders/factor.blade.php ENDPATH**/ ?>