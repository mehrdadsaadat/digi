<?php

namespace App\Http\Controllers\Admin;

use App\CategoryCommonQuestion;
use App\CommonQuestion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonQuestionController extends CustomController
{
    protected  $model='CommonQuestion';
    protected  $title='پرسش  متداول';
    protected  $route_params='common-question';
    public function index(Request $request)
    {
        $CommonQuestion=CommonQuestion::getData($request->all());
        $trash_common_question_count=CommonQuestion::onlyTrashed()->count();
        return view('common_question.index',['CommonQuestion'=>$CommonQuestion,'trash_common_question_count'=>$trash_common_question_count,'req'=>$request]);
    }
    public function create()
    {
        $cat=CategoryCommonQuestion::pluck('title','id')->toArray();
        return view('common_question.create',['cat'=>$cat]);
    }
    public function store(Request $request)
    {
        $this->validate($request,
            ['title'=>'required','cat_id'=>'required','small_answer'=>'required'],[],
            ['title'=>'عنوان پرسش','cat_id'=>'دسته','answer'=>'پاسخ','small_answer'=>'پاسخ کوتاه'
            ]);
        $pin=$request->has('pin') ? 1 : 0;
        $CommonQuestion=new CommonQuestion($request->all());
        $CommonQuestion->pin=$pin;
        $CommonQuestion->save();
        return redirect('admin/common-question')->with('message','ثبت پرسش با موفقیت انجام شد');
    }
    public function edit($id)
    {
        $CommonQuestion=CommonQuestion::findOrFail($id);
        $cat=CategoryCommonQuestion::pluck('title','id')->toArray();
        return view('common_question.edit',['CommonQuestion'=>$CommonQuestion,'cat'=>$cat]);
    }
    public function update($id,Request $request)
    {
        $this->validate($request,
            ['title'=>'required','cat_id'=>'required','small_answer'=>'required'],[],
            ['title'=>'عنوان پرسش','cat_id'=>'دسته','answer'=>'پاسخ','small_answer'=>'پاسخ کوتاه'
            ]);

        $data=$request->all();
        $CommonQuestion=CommonQuestion::findOrFail($id);
        $pin=$request->has('pin') ? 1 : 0;
        $data['pin']=$pin;
        $CommonQuestion->update($data);
        return redirect('admin/common-question')->with('message','ویرایش پرسش با موفقیت انجام شد');

    }
}
