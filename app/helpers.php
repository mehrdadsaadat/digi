<?php

use App\Category;
use App\GiftCart;
use App\Lib\Jdf;
use App\Message;
use App\Product;
use App\ProductPrice;
use App\ProductWarranty;
use App\Warranty;

function get_url($string)
{
    $url=str_replace('-',' ',$string);
    $url=str_replace('/',' ',$url);
    $url=preg_replace('/\s+/','-',$url);
    return $url;
}
function upload_file($request,$name,$directory,$pix='')
{
    if($request->hasFile($name))
    {
        $file_name=$pix.time().'.'.$request->file($name)->getClientOriginalExtension();
        if($request->file($name)->move('files/'.$directory,$file_name))
        {
            return $file_name;
        }
        else{
            return null;
        }
    }
    else{
        return null;
    }
}
function replace_number($number)
{
    $number=str_replace("0",'۰',$number);
    $number=str_replace("1",'۱',$number);
    $number=str_replace("2",'۲',$number);
    $number=str_replace("3",'۳',$number);
    $number=str_replace("4",'۴',$number);
    $number=str_replace("5",'۵',$number);
    $number=str_replace("6",'۶',$number);
    $number=str_replace("7",'۷',$number);
    $number=str_replace("8",'۸',$number);
    $number=str_replace("9",'۹',$number);

    return $number;
}
function replace_number2($number)
{
    $number=str_replace("۰",'0',$number);
    $number=str_replace("۱",'1',$number);
    $number=str_replace("۲",'2',$number);
    $number=str_replace("۳",'3',$number);
    $number=str_replace("۴",'4',$number);
    $number=str_replace("۵",'5',$number);
    $number=str_replace("۶",'6',$number);
    $number=str_replace("۷",'7',$number);
    $number=str_replace("۸",'8',$number);
    $number=str_replace("۹",'9',$number);

    return $number;
}
function inTrashed($req)
{
    if(array_key_exists('trashed',$req) && $req['trashed']=='true')
    {
        return true;
    }
    else{
        return false;
    }
}
function create_paginate_url($string,$text)
{
    if($string=='?')
    {
        $string=$string.$text;
    }
    else{
        $string=$string.'&'.$text;
    }
    return $string;
}
function create_crud_route($route_param,$controller,$except=['show'],$config=[])
{
    Route::resource($route_param,'Admin\\'.$controller,$config)->except($except);
    Route::post($route_param.'/remove_items','Admin\\'.$controller.'@remove_items')->name($route_param.'.destroy');
    Route::post($route_param.'/restore_items','Admin\\'.$controller.'@restore_items')->name($route_param.'.restore');
    Route::post($route_param.'/{id}','Admin\\'.$controller.'@restore')->name($route_param.'.restore');
}
function create_fit_pic($pic_url,$pic_name)
{
    if($pic_name!=null)
    {
        $thum=Image::make($pic_url);
        $thum->resize(350,350);
        $thum->save('files/thumbnails/'.$pic_name);
    }
}
function remove_file($file_name,$directory)
{
    if(!empty($file_name) && file_exists('files/'.$directory.'/'.$file_name))
    {
        unlink('files/'.$directory.'/'.$file_name);
    }
}
function add_min_product_price($warranty,$type='insert')
{
    $jdf=new Jdf();
    $year=$jdf->tr_num($jdf->jdate('Y'));
    $mount=$jdf->tr_num($jdf->jdate('n'));
    $day=$jdf->tr_num($jdf->jdate('j'));
    $has_row=DB::table('product_price')
        ->where(['Year'=>$year,'mount'=>$mount,'day'=>$day,'color_id'=>$warranty->color_id,'product_id'=>$warranty->product_id])->first();
    if($has_row)
    {
        if($warranty->price2<$has_row->price || $has_row->price==0)
        {
            DB::table('product_price')
                ->where(['Year'=>$year,'mount'=>$mount,'day'=>$day,'color_id'=>$warranty->color_id,'product_id'=>$warranty->product_id])
                ->update([
                    'price'=> $warranty->price2,
                    'warranty_id'=>$warranty->id
                ]);
        }
    }
    else{
        DB::table('product_price')
            ->insert(
                [
                    'Year'=>$year,
                    'mount'=>$mount,
                    'day'=>$day,
                    'color_id'=>$warranty->color_id,
                    'product_id'=>$warranty->product_id,
                    'price'=>$warranty->price2,
                    'time'=>time(),
                    'warranty_id'=>$warranty->id
                ]
            );
    }
}
function update_product_price($product)
{
    $warranty=ProductWarranty::where('product_id',$product->id)->where('product_number','>',0)->orderBy('price2','ASC')->first();
    $warranty2=ProductWarranty::where('product_id',$product->id)->orderBy('send_time','ASC')->first();
    if($warranty && $product)
    {
        $product->price=$warranty->price2;
        if($warranty->price1>$warranty->price2){
            $discount_price=$warranty->price1-$warranty->price2;
            $product->discount_price=$discount_price;
        }
        else{
            $product->discount_price=0;
        }
        $product->status=1;
        $product->ready_to_shipment=$warranty2->send_time;
        $product->update();
    }
    else{
        $product->discount_price=0;
        $product->status=0;
        $product->update();
    }
}
function check_has_product_warranty($warranty)
{
    $jdf=new Jdf();
    $year=$jdf->tr_num($jdf->jdate('Y'));
    $mount=$jdf->tr_num($jdf->jdate('n'));
    $day=$jdf->tr_num($jdf->jdate('j'));

    $row=ProductWarranty::where(['product_id'=>$warranty->product_id,'color_id'=>$warranty->color_id])
        ->where('product_number','>',0)->orderBy('price2','ASC')->first();
    $price=$row ? $row->price2 : 0;
    $warranty_id=$row ? $row->id : 0;
    $has_row=ProductPrice::where(['Year'=>$year,'mount'=>$mount,'day'=>$day,'color_id'=>$warranty->color_id,'product_id'=>$warranty->product_id])->first();
    if($has_row)
    {
        $has_row->price=$price;
        $has_row->warranty_id=$warranty_id;
        $has_row->update();
    }
    else{
        DB::table('product_price')
            ->insert(
                [
                    'Year'=>$year,
                    'mount'=>$mount,
                    'day'=>$day,
                    'color_id'=>$warranty->color_id,
                    'product_id'=>$warranty->product_id,
                    'price'=>$price,
                    'time'=>time(),
                    'warranty_id'=>$warranty_id
                ]
            );
    }
}
function is_selected_filter($list,$filter_id)
{
    $result=false;

    foreach ($list as $key=>$value)
    {
        if($value->filter_value==$filter_id)
        {
            $result=true;
        }
    }

    return $result;
}
function getFilterArray($list)
{
   $array=array();
   foreach ($list as $key=>$value)
   {
       $array[$value->item_id]=$key;
   }
   return $array;
}
function getFilterItemValue($filter_id,$product_filters)
{
    $string='';

    foreach ($product_filters as $key=>$value)
    {
        if($value==$filter_id)
        {
            $string.='@'.$key;
        }
    }

    return $string;
}
function get_show_category_count($catList)
{
    $n=0;
    foreach ($catList as $key=>$value)
    {
        if($value->notShow==0)
        {
            $n++;
        }
    }
    return $n;
}
function getCatList()
{
    $data=cache('catList');
    if($data)
    {
        View::share('calList',$data);
    }
    else{
        $category=Category::with('getChild.getChild.getChild')->where('parent_id',0)->get();
        $minutes=30*24*60*60;
        cache()->put('catList',$category,$minutes);
        View::share('calList',$category);
    }
}
function get_cat_url($cat)
{
    if(!empty($cat->search_url))
    {
        return url($cat->search_url);
    }
    else{
        return url('search/'.$cat->url);
    }
}
function getTimestamp($date,$type)
{
    $Jdf=new Jdf();
    $time=0;
    $e=explode('/',$date);
    if(sizeof($e)==3)
    {
        $y=$e[0];
        $m=$e[1];
        $d=$e[2];
        if($type=='first')
        {
            $time=$Jdf->jmktime(0,0,0,$m,$d,$y);
        }
        else{
            $time=$Jdf->jmktime(23,59,59,$m,$d,$y);
        }
    }
    return $time;
}
function check_has_color_in_warranty_list($warranty_list,$color_id)
{
    $r=false;
    foreach ($warranty_list as $key=>$value)
    {
        if($value->color_id==$color_id)
        {
            $r=true;
        }
    }
    return $r;
}
function get_first_color_id($warranty_list,$color_id)
{
    if(sizeof($warranty_list)>0)
    {
        if($warranty_list[0]->color_id==$color_id)
        {
            return true;
        }
        else{
            return false;
        }
    }
}
function getCartProductData($products,$product_id)
{
    foreach ($products as $key=>$value)
    {
        if($value->id==$product_id)
        {
            return $value;
        }
    }
}
function getCartColorData($colors,$color_id)
{
    foreach ($colors as $key=>$value)
    {
        if($value->id==$color_id)
        {
            return $value;
        }
    }
}
function getCartSellerData($sellers,$seller_id)
{
    foreach ($sellers as $key=>$value)
    {
        if($value->id==$seller_id)
        {
            return $value;
        }
    }
}
function getCartWarrantyData($warranties,$warranty_id)
{
    foreach ($warranties as $key=>$value)
    {
        if($value->id==$warranty_id)
        {
            return $value;
        }
    }
}

function set_order_product_status($orderInfo,$status){
    $products_id=$orderInfo->products_id;
    $colors_id=$orderInfo->colors_id;
    $warranty_id=$orderInfo->warranty_id;
    $products_id=explode('-',$products_id);
    $colors_id=explode('-',$colors_id);
    $warranty_id=explode('-',$warranty_id);
    foreach ($products_id as $key=>$value)
    {
        if(!empty($value)){
            DB::table('order_products')->where(['order_id'=>$orderInfo->order_id,'product_id'=>$value,'color_id'=>$colors_id[$key],'warranty_id'=>$warranty_id[$key]])
                ->update(['send_status'=>$status]);
        }
    }
}
function getOrderProductCount($products_id){
    $e=explode('-',$products_id);
    return sizeof($e);
}
function CheckGiftCart($product,$user_id,$credit_cart,$order_id){

    if($product->use_for_gift_cart=='yes')
    {
        $code='digiGift-'.rand(99,999).$user_id.rand(9,99);
        $gift_cart=new GiftCart();
        $gift_cart->user_id=$user_id;
        $gift_cart->order_id=$order_id;
        $gift_cart->credit_cart=$credit_cart;
        $gift_cart->credit_used=0;
        $gift_cart->code=$code;
        $gift_cart->save();
    }
}
function set_cat_brand($product,$oldData)
{
    if($oldData){

        if($oldData['cat_id']!=$product->cat_id)
        {
            check_has_cat_brand($product->cat_id,$product->brand_id);
            remove_cat_brand($oldData['cat_id'],$oldData['brand_id']);
        }
        elseif($oldData['brand_id']!=$product->brand_id){
            check_has_cat_brand($product->cat_id,$product->brand_id);
            remove_cat_brand($oldData['cat_id'],$oldData['brand_id']);
        }
    }
    else
    {
        check_has_cat_brand($product->cat_id,$product->brand_id);
    }
}
function add_cat_brand($cat_id,$brand_id){
    $CatBrand=new \App\CatBrand();
    $CatBrand->cat_id=$cat_id;
    $CatBrand->brand_id=$brand_id;
    $CatBrand->product_count=1;
    $CatBrand->save();
}

function check_has_cat_brand($cat_id,$brand_id){
    $row=\App\CatBrand::where(['cat_id'=>$cat_id,'brand_id'=>$brand_id])->first();
    if($row){
        $product_count=$row->product_count+1;
        $row->product_count=$product_count;
        $row->update();
    }
    else{
        add_cat_brand($cat_id,$brand_id);
    }
}
function remove_cat_brand($cat_id,$brand_id){
    $row=\App\CatBrand::where(['cat_id'=>$cat_id,'brand_id'=>$brand_id])->first();
    if($row && $row->product_count>1){
        $product_count=$row->product_count-1;
        $row->product_count=$product_count;
        $row->update();
    }
    else{
       if($row){
           $row->delete();
       }
    }
}

function get_compare_product_id($data)
{
    $array=array();
    $i=0;
    if(is_array($data))
    {
        foreach ($data as $key=>$value)
        {
            if(!empty($value))
            {
                $array[$i]=str_replace('dkp-','',$value);
                $i++;
            }
        }
    }
    return $array;
}
function get_item_value($key,$products,$item_id){
    $string='';
    if(sizeof($products)>$key)
    {
        foreach ($products[$key]->getItemValue as $item)
        {
            if($item_id==$item->item_id)
            {
                $string.=$item->item_value.'<br>';
            }
        }
    }
    return $string;
}
function getCommentItem($array)
{
    $string='';
    foreach ($array as $key=>$value)
    {
        $string.=$value.'|[@#]|';
    }
    return $string;
}
function getCommentOrderId($product_id,$user_id)
{
    define('product_id',$product_id);
    $order_id=0;
    $order=\App\Order::whereHas('getOrderProduct',function ( \Illuminate\Database\Eloquent\Builder $query){
        $query->where('product_id',product_id);
    })->where(['user_id'=>$user_id,'pay_status'=>'ok'])->select(['id'])->first();
    return $order_id;
}
function addScore($score_array,$comment_id,$product_id)
{
    if(sizeof($score_array)==6)
    {
        $commentScore=new \App\CommentScore();
        $commentScore->product_id=$product_id;
        $commentScore->comment_id=$comment_id;
        $commentScore->score1=$score_array[0];
        $commentScore->score2=$score_array[1];
        $commentScore->score3=$score_array[2];
        $commentScore->score4=$score_array[3];
        $commentScore->score5=$score_array[4];
        $commentScore->score6=$score_array[5];
        $commentScore->save();
    }
}
function getScoreItem($score,$scoreType)
{

    $score=[
        ['label'=>'کیفیت ساخت : ','value'=>$score->score1,'type'=>getScoreType($score->score1,$scoreType)],
        ['label'=>'نوآوری : ','value'=>$score->score2,'type'=>getScoreType($score->score2,$scoreType)],
        ['label'=>'سهولت استفاده : ','value'=>$score->score3,'type'=>getScoreType($score->score3,$scoreType)],
        ['label'=>'ارزش خرید به نسبت قیمت : ','value'=>$score->score4,'type'=>getScoreType($score->score4,$scoreType)],
        ['label'=>'امکانات و قابلیت ها : ','value'=>$score->score5,'type'=>getScoreType($score->score5,$scoreType)],
        ['label'=>'سهولت طراحی و ظاهر : ','value'=>$score->score6,'type'=>getScoreType($score->score6,$scoreType)]
    ];
    return $score;
}
function getScoreType($score,$scoreType){
    if(array_key_exists($score,$scoreType))
    {
        return $scoreType[$score];
    }
    else
    {
        return '';
    }
}
function get_product_price_changed($product_id)
{
    $array=array();
    $jdf=new Jdf();
    $points=[];

    $productColor=\App\ProductColor::with('getColor')->where('product_id',$product_id)->get();
    $timeStamp=strtotime('-30 day');

    $product_price=ProductPrice::with(['getColor'])
        ->with(['getProductWarranty'=>function($query){
            $query->withTrashed();
        }])
        ->where('product_id',$product_id)
        ->where('time','>=',$timeStamp)->get();
    $warranty_price=[];
    $price=[];
    $seller=[];
    $color=[];
    $zone=[];

    foreach($product_price as $key=>$value)
    {
        $date=$jdf->tr_num($jdf->jdate('Y-n-j',$value->time));
        $warranty_price[$date][$value->color_id]=$value->price;
        $seller[$date][$value->color_id]='تنیس شاپ';
    }
    for ($i=30;$i>=0;$i--)
    {
        $timeStamp=strtotime('-'.$i.' day');
        $date=$jdf->tr_num($jdf->jdate('Y-n-j',$timeStamp));

        if(array_key_exists($date,$warranty_price))
        {

            foreach ($productColor as $key=>$value)
            {

                $size=array_key_exists($value->color_id,$price) ? sizeof($price[$value->color_id]) :0;
                $points[$date]=replace_number($date);
                if(array_key_exists($value->color_id,$warranty_price[$date]))
                {
                    $color[$value->color_id]=['name'=>$value->getColor->name,'code'=>$value->getColor->code,'id'=>$value->getColor->id];
                    $price[$value->color_id][$size]['y']=$warranty_price[$date][$value->color_id];
                    if($warranty_price[$date][$value->color_id]==0)
                    {

                        $price[$value->color_id][$size]['y']=$price[$value->color_id][($size-1)]['y'];
                        $price[$value->color_id][$size]['price']=0;
                        $price[$value->color_id][$size]['has_product']='no';
                        $price[$value->color_id][$size]['color']='grey';

                        $zone_size=array_key_exists($value->color_id,$zone) ? sizeof($zone[$value->color_id]) : 0;
                        $zone[$value->color_id][$zone_size]=['value'=>$size];

                        if(sizeof($zone[$value->color_id])==1 && $i==0)
                        {
                           $zone[$value->color_id][($zone_size+1)]['value']=$zone[$value->color_id][$zone_size]['value'];
                           $zone[$value->color_id][($zone_size+1)]['color']='gray';
                        }
                    }
                    else{
                        $price[$value->color_id][$size]['price']=$warranty_price[$date][$value->color_id];
                        $price[$value->color_id][$size]['has_product']='ok';
                        $price[$value->color_id][$size]['color']='#00bfd6';
                        $price[$value->color_id][$size]['seller']='تنیس شاپ';

                        if(array_key_exists($value->color_id,$zone))
                        {
                            $first=sizeof($zone[$value->color_id])-1;
                            $end=$zone[$value->color_id][$first];

                            if($price[$value->color_id][($size-1)]['price']==0)
                            {
                                $zone[$value->color_id][sizeof($zone[$value->color_id])]=['value'=>$size,'color'=>'gray'];
                            }
                        }
                    }
                }
                else{
                    if(array_key_exists($value->color_id,$price) && array_key_exists(($size-1),$price[$value->color_id]))
                    {

                       $color[$value->color_id]=['name'=>$value->getColor->name,'code'=>$value->getColor->code,'id'=>$value->getColor->id];
                       if($price[$value->color_id][($size-1)]['price']==0)
                       {
                           $price[$value->color_id][$size]['y']=$price[$value->color_id][($size-1)]['y'];
                           $price[$value->color_id][$size]['price']=0;
                           $price[$value->color_id][$size]['has_product']='no';
                           $price[$value->color_id][$size]['color']='grey';

                           $zone_size=array_key_exists($value->color_id,$zone) ? sizeof($zone[$value->color_id]) : 0;
                           $zone[$value->color_id][$zone_size]=['value'=>$size,'color'=>'gray'];
                       }
                       else{
                           $price[$value->color_id][$size]['y']=$price[$value->color_id][($size-1)]['y'];
                           $price[$value->color_id][$size]['price']=$price[$value->color_id][($size-1)]['price'];
                           $price[$value->color_id][$size]['has_product']='ok';
                           $price[$value->color_id][$size]['color']='#00bfd6';
                           $price[$value->color_id][$size]['seller']='تنیس شاپ';
                           if(array_key_exists($value->color_id,$zone))
                           {
                               $first=sizeof($zone[$value->color_id])-1;
                               $end=$zone[$value->color_id][$first];

                               if($price[$value->color_id][($size-1)]['price']==0)
                               {
                                   $zone[$value->color_id][sizeof($zone[$value->color_id])]=['value'=>$size,'color'=>'gray'];
                               }
                           }
                       }

                    }
                }
            }
        }
        else if(sizeof($price)>0){
            $points[$date]=replace_number($date);

            foreach ($productColor as $key=>$value){
                $size=array_key_exists($value->color_id,$price) ? sizeof($price[$value->color_id]) :0;

                if(array_key_exists($value->color_id,$price) && array_key_exists(($size-1),$price[$value->color_id]))
                {
                    $color[$value->color_id]=['name'=>$value->getColor->name,'code'=>$value->getColor->code,'id'=>$value->getColor->id];
                    if($price[$value->color_id][($size-1)]['price']==0)
                    {
                        $price[$value->color_id][$size]['y']=$price[$value->color_id][($size-1)]['y'];
                        $price[$value->color_id][$size]['price']=0;
                        $price[$value->color_id][$size]['has_product']='no';
                        $price[$value->color_id][$size]['color']='grey';

                        if (array_key_exists($value->color_id,$zone))
                        {
                            $first=sizeof($zone[$value->color_id])-1;
                            $end=$zone[$value->color_id][$first];

                            if($price[$value->color_id][($size-1)]['price']==0)
                            {
                                $zone[$value->color_id][sizeof($zone[$value->color_id])]=['value'=>$size,'color'=>'gray'];
                            }
                        }

                    }
                    else{
                        $price[$value->color_id][$size]['y']=$price[$value->color_id][($size-1)]['y'];
                        $price[$value->color_id][$size]['price']=$price[$value->color_id][($size-1)]['price'];
                        $price[$value->color_id][$size]['has_product']='ok';
                        $price[$value->color_id][$size]['color']='#00bfd6';
                        $price[$value->color_id][$size]['seller']='تنیس شاپ';

                    }
                }
            }
        }
    }

    $i=0;
    foreach ($points as $key=>$value)
    {
        $points[$i]=$value;
        unset($points[$key]);
        $i++;
    }

    $j=0;
    foreach ($price as $key=>$value)
    {
        $price[$j]=$value;
        unset($price[$key]);
        $j++;
    }

    $x=0;
    foreach ($color as $key=>$value)
    {
        $color[$x]=$value;
        unset($color[$key]);
        $x++;
    }

    $array['price']=$price;
    $array['points']=$points;
    $array['color']=$color;
    $array['zone']=$zone;
    return $array;
}

function getUserPersonalData($additionalInfo,$att1,$attr2=null)
{
    $result='-';
    if($additionalInfo && !empty($additionalInfo->$att1))
    {
        $result=$additionalInfo->$att1;
        if($attr2){
            $result.=' '. $additionalInfo->$attr2;
        }
    }
    return $result;
}
function getUserData($key,$additionalInfo)
{
    if(!empty(old($key))){
        return old($key);
    }
    else{
        if($key=='mobile_phone'){
            return Auth::user()->mobile;
        }
        elseif($additionalInfo && !empty($additionalInfo->$key)){
            return $additionalInfo->$key;
        }
        else{
           return '';
        }
    }
}
function checkEven($n)
{
    if($n%2==0){
        return true;
    }
    else{
        return false;
    }
}
function addLike($request,$score_type)
{
    $table_name=$request->get('table_name','');
    $row_id=$request->get('row_id','');
    if($table_name=='comments' || $table_name=='questions'){
        $row=DB::table($table_name)->where('id',$row_id)->first();
        if($row){
            $user_id=$request->user()->id;

            $user_scored_status=DB::table('user_scored_status')
            ->where(['user_id'=>$user_id,'row_id'=>$row_id,'score_type'=>$score_type,'type'=>$table_name])
            ->first();
            if($user_scored_status)
            {
                DB::table('user_scored_status')
                    ->where(['user_id'=>$user_id,'row_id'=>$row_id,'score_type'=>$score_type,'type'=>$table_name])->delete();
                DB::table($table_name)->where('id',$row_id)->decrement($score_type,1);
                return 'remove';
            }
            else
            {
                DB::table('user_scored_status')->insert([
                    'user_id'=>$user_id,
                    'row_id'=>$row_id,
                    'score_type'=>$score_type,
                    'type'=>$table_name
                ]);
                DB::table($table_name)->where('id',$row_id)->increment($score_type,1);
                return 'add';
            }
        }
        else{
            return 'error';
        }

    }
    else{
        return 'error';
    }

}
function CheckAccess($AccessList,$key1,$key2)
{
    $result=false;
    if($AccessList){
        $access=json_decode($AccessList->access);
        if(is_object($access)){
            if(property_exists($access,$key1)){
                if(is_array($access->$key1))
                {
                    foreach($access->$key1 as $k=>$v){
                        if($v==$key2){
                            $result=true;
                        }
                    }
                }
            }
        }
    }
    return $result;
}
function checkUserAccess($access,$route,$AccessList){
    $result=false;
    $access=json_decode($access);
    if(is_object($access)){
        foreach($access as $key=>$value)
        {
            if(array_key_exists($key,$AccessList)){
                $userAccess=$AccessList[$key]['access'];
                foreach($value as $key2=>$value2){
                    if(array_key_exists($value2,$userAccess)){
                        if(array_key_exists('routes',$userAccess[$value2]))
                        {
                            foreach($userAccess[$value2]['routes'] as $accessRoute){
                                if($accessRoute==$route){
                                    $result=true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $result;
}
function checkParentMenuAccess($accessList,$access)
{
    $result=false;
    if(Auth::user()->role=='admin'){
        $result=true;
    }
    else{
        $e=explode('|',$access);
        if(sizeof($e)>0 && is_object($accessList)){
            foreach($e as $key=>$value)
            {
                if(!empty($value)){
                    if(property_exists($accessList,$value)){
                        $result=true;
                    }
                }
            }
        }
    }
    return $result;
}
function checkAddChildMuenAccess($accessList,$child){
    $result=false;
    if(Auth::user()->role=='admin'){
        $result=true;
    }
    else{
        $property=$child['access'];
        if(is_object($accessList))
        {
            if(property_exists($accessList,$property))
            {
                if(is_array($accessList->$property))
                {
                    if(!array_key_exists('accessValue',$child))
                    {
                        $result=true;
                    }
                    else{
                        foreach($accessList->$property as $key=>$value)
                        {
                            if($value==$child['accessValue'])
                            {
                                $result=true;
                            }
                        }
                    }
                }
            }
        }
    }
    return $result;
}
function get_stockroom_product_count($list)
{
    $n=0;
    foreach($list as $key=>$value)
    {
        if(!empty($value)){
            $e=explode('_',$value);
            if(sizeof($e)==2){
                $a=$e[1];
                setType($a,'integer');
                $n=$n+$a;
            }
        }
    }
    return $n;
}
function set_sale($order)
{
    if($order)
    {
        $jdf=new \App\Lib\Jdf();
    $y=$jdf->tr_num($jdf->jdate('Y'));
    $m=$jdf->tr_num($jdf->jdate('n'));
    $d=$jdf->tr_num($jdf->jdate('j'));
    $total_price=0;
    $commision_price=0;

    foreach($order->getProductRow as $key=>$value)
    {
        $cat_id=$value->getProduct->cat_id;
        $brand_id=$value->getProduct->brand_id;
        $c=0;
        $product_price=$value->product_price2*$value->product_count;
        if($value->seller_id>0)
        {
            $commision=\App\Commission::where(['cat_id'=>$cat_id,'brand_id'=>$brand_id])->first();
            if($commision){
                $c=($value->product_price2*$commision->percentage)/100;
                $c=($c*$value->product_count);
                $commision_price+=$c;
            }
            DB::table('sellers')->where('id',$value->seller_id)->increment('new_order_count');
            DB::table('sellers')->where('id',$value->seller_id)->increment('total_commission',$c);
            DB::table('sellers')->where('id',$value->seller_id)->increment('total_price',$product_price);
            $value->commission=$c;
            $value->update();

            set_seller_sale_statistics($product_price,$c,$y,$m,$d,$value->seller_id);
        }
        $total_price+=$product_price;
        product_sale_statistics($y,$m,$d,$c,$product_price,$value->product_id,$value->seller_id);
    }
    set_overall_statistics($y,$m,$d, $total_price,$commision_price);
    }
}
function product_sale_statistics($y,$m,$d,$commision,$product_price,$product_id,$seller_id,$type='plus')
{
    $product_sale=DB::table('product_sale_statistics')
    ->where(['year'=>$y,'month'=>$m,'day'=>$d,'product_id'=>$product_id])->first();
    if($product_sale)
    {
        if($type=='plus')
        {
            $product_price=$product_price+$product_sale->price;
            $commision=$commision+$product_sale->commision;
        }
        else{
            $product_price=$product_sale->price-$product_price;
            $commision=$product_sale->commision-$commision;
        }
        DB::table('product_sale_statistics')
        ->where(['year'=>$y,'month'=>$m,'day'=>$d,'product_id'=>$product_id])
        ->update([
            'price'=>$product_price,
            'commision'=>$commision
        ]);
    }
    else{
        if($type=='minus')
        {
            $commision=-$commision;
            $product_price=-$product_price;
        }
        DB::table('product_sale_statistics')
        ->insert([
            'year'=>$y,
            'month'=>$m,
            'day'=>$d,
            'product_id'=>$product_id,
            'commision'=>$commision,
            'price'=>$product_price
        ]);
    }
}
function set_seller_sale_statistics($product_price,$commision,$y,$m,$d,$seller_id,$type='plus')
{
    $seller_sale=DB::table('seller_sale_statistics')
    ->where(['year'=>$y,'month'=>$m,'day'=>$d,'seller_id'=>$seller_id])->first();
    if($seller_sale)
    {
        if($type=='plus')
        {
            $product_price=$product_price+$seller_sale->price;
            $commision=$commision+$seller_sale->commision;
        }
        else{
            $product_price=$seller_sale->price-$product_price;
            $commision=$seller_sale->commision-$commision;
        }
        DB::table('seller_sale_statistics')
        ->where(['year'=>$y,'month'=>$m,'day'=>$d,'seller_id'=>$seller_id])
        ->update([
            'price'=>$product_price,
            'commision'=>$commision
        ]);
    }
    else{
        if($type=='minus')
        {
            $commision=-$commision;
            $product_price=-$product_price;
        }
        DB::table('seller_sale_statistics')
        ->insert([
            'year'=>$y,
            'month'=>$m,
            'day'=>$d,
            'seller_id'=>$seller_id,
            'commision'=>$commision,
            'price'=>$product_price
        ]);
    }
}
function set_overall_statistics($y,$m,$d,$total_price,$commision,$type='plus')
{

    $sale_statistics=DB::table('sale_statistics')
    ->where(['year'=>$y,'month'=>$m,'day'=>$d])->first();
    if($sale_statistics)
    {
        if($type=='plus')
        {
            $total_price=$total_price+$sale_statistics->price;
            $commision=$commision+$sale_statistics->commision;
        }
        else{
            $total_price=$sale_statistics->price-$total_price;
            $commision=$sale_statistics->commision-$commision;
        }
        DB::table('sale_statistics')
        ->where(['year'=>$y,'month'=>$m,'day'=>$d])
        ->update([
            'price'=>$total_price,
            'commision'=>$commision
        ]);
    }
    else{
        if($type=='minus')
        {
            $commision=-$commision;
            $total_price=-$total_price;
        }
        DB::table('sale_statistics')
        ->insert([
            'year'=>$y,
            'month'=>$m,
            'day'=>$d,
            'commision'=>$commision,
            'price'=>$total_price
        ]);
    }
}
function get_sale_report($request,$year,$table_name,$where,$attr,$now)
{
    $sale=[0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0,8=>0,9=>0,10=>0,11=>0,12=>0];
    $commision=[0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0,8=>0,9=>0,10=>0,11=>0,12=>0];
    $data=DB::table($table_name)->where($where)->get();
    foreach($data as $key=>$value)
    {
        if(array_key_exists($value->month,$sale))
        {
            $sale[$value->month]=$sale[$value->month]+$value->$attr;
        }
        if(array_key_exists($value->month,$commision))
        {
            $commision[$value->month]=$commision[$value->month]+$value->commision;
        }
    }

    $first=DB::table($table_name)->first();
    $year_list=array();
    if($first && $first->year!=$now)
    {
        $j=0;
        $a=$first->year;
        settype($a,'integer');
        for($i=$a;$i<=$now;$i++)
        {
            $year_list[$j]=$i;
            $j++;
        }
    }
    else{
        $year_list[0]=$now;
    }
    $response=array();
    $response['sale']=$sale;
    $response['commision']=$commision;
    $response['default_year']=$year;
    $response['year_list']=$year_list;
    return $response;
}
function get_return_product_price($cat_id,$order_discount,$product_price)
{
    foreach($order_discount as $key=>$value)
    {
       if($value->cat_id==$cat_id)
       {
           $p=$value->total_price-$product_price;
           if($p>$value->min_price)
           {
               if(!empty($value->amount_percent))
               {
                  $product_price=$product_price-(($product_price*$value->amount_percent)/100);
               }
               return $product_price;
           }
           else{
               return ($product_price-$value->discount_price);
           }
       }
       else if($value->cat_id==0){
           $p=$value->total_price-$product_price;
           if($p>$value->min_price)
           {
              if(!empty($value->amount_percent))
              {
                $product_price=$product_price-(($product_price*$value->amount_percent)/100);
              }
              return $product_price;
           }
           else{
               return ($product_price-$value->discount_price);
           }
       }
    }

    if(sizeof($order_discount)==0)
    {
        return $product_price;
    }
}
function  set_product_color($product,$product_color)
{
    if(is_array($product_color))
    {
        foreach ($product_color as $key=>$value)
        {
            if(!empty($value))
            {
                DB::table('product_color')->insert([
                    'product_id'=>$product->id,
                    'color_id'=>$value,
                    'cat_id'=>$product->cat_id
                ]);
            }
        }
    }
}
function add_seller_document($request,$seller_id,$file_name)
{
    $image_url=upload_file($request,$file_name,'seller',$file_name.'_');
    if($image_url)
    {
        $check=DB::table('seller_document')->where('seller_id',$seller_id)->first();
        if($check && !empty($check->$file_name))
        {
            if(file_exists('files/seller/'.$check->$file_name)){
                unlink('files/seller/'.$check->$file_name);
            }
            DB::table('seller_document')->where(['seller_id'=>$seller_id])->update([
                $file_name=>$image_url
            ]);
        }
        else{
            DB::table('seller_document')
            ->insert(['seller_id'=>$seller_id,$file_name=>$image_url]);
        }
        return true;
    }
    else{
        if($request->hasFile($file_name))
        {
            return false;
        }
        else{
            return true;
        }
    }
}
function addMessage($request, $user, $user_id,$parent_id=0,$user_type,$parent=null){
    $time = time();
    $message = new Message($request->all());
    $message->parent_id = $parent_id;
    $message->user_id = $user->id;
    $message->user_type =$user_type;
    $message->from_id = $user_id;
    $message->from_type = 'App\User';
    $message->to_type =$user_type;
    $message->to_id = $user->id;
    $message->time = $time;
    $img_url = upload_file($request, 'pic', 'upload');
    if ($img_url) {
        $message->file = $img_url;
    }
    $message->save();
    $message=$parent==null ? $message : $parent;
    send_notification($parent_id,$message->user_id,$user_type,$message);
}
function send_notification($parent_id,$user_id,$user_type,$message){

    if($user_type=='App\User'){
        $userData=\App\AdditionalInfo::where('user_id',$user_id)->whereNotNull('email')->first();
        $user=\App\User::where('id',$user_id)->select(['mobile'])->first();
        if($userData){

            $name=$userData->first_name.' '.$userData->last_name;
            $url=url('user/profile/messages/'.$message->id);
            Notification::route('mail',$userData->email)
                ->notify(new \App\Notifications\SendMessage($message,$name,$url,$parent_id,'کاربر',$user->mobile));
        }
        else if($user){
            $name=false;
            $url='';
            Notification::notify(new \App\Notifications\SendMessage($message,$name,$url,$parent_id,'کاربر',$user->mobile));
        }
    }
    else{
        $seller=\App\Seller::where('id',$user_id)->withTrashed()->first();
        if($seller){
            $name=$seller->brand_name;
            $url='seller.shop.com/message/'.$message->id.'/show';
            Notification::route('mail',$seller->email)
                ->notify(new \App\Notifications\SendMessage($message,$name,$url,$parent_id,'فروشنده',$seller->mobile));
        }
    }

}
function set_new_message_count($user_id){
    $count=Message::where(['user_id'=>$user_id,'user_type'=>'App\User','status'=>-1,'parent_id'=>0])->count();
    View::share('new_message_count',$count);
}



function set_admin_panel_variables(){
    $new_order_count=\App\Order::where('order_read','no')->count();
    $product_awaiting_review=Product::where('status',-2)->count();
    $message_count=Message::where('status',1)->count();
    $question_count=\App\Question::where('status',0)->count();
    $comments_count=\App\Comment::where('status',0)->count();
    View::share('new_order_count',$new_order_count);
    View::share('product_awaiting_review',$product_awaiting_review);
    View::share('message_count',$message_count);
    View::share('question_count',$question_count);
    View::share('comments_count',$comments_count);
}
function set_author_admin_variables($access){
    if(has_access_author_admin($access,'products','product_edit')){
        $product_awaiting_review=Product::where('status',-2)->count();
        View::share('product_awaiting_review',$product_awaiting_review);
    }

    if(has_access_author_admin($access,'products','product_edit')){
        $product_awaiting_review=Product::where('status',-2)->count();
        View::share('product_awaiting_review',$product_awaiting_review);
    }
    if(has_access_author_admin($access,'orders')){
        $new_order_count=\App\Order::where('order_read','no')->count();
        View::share('new_order_count',$new_order_count);
    }
    if(has_access_author_admin($access,'message')){
        $message_count=Message::where('status',1)->count();
        View::share('message_count',$message_count);
    }
    if(has_access_author_admin($access,'questions')){
        $question_count=\App\Question::where('status',0)->count();
        View::share('question_count',$question_count);
    }
    if(has_access_author_admin($access,'comments')){
        $comments_count=\App\Comment::where('status',0)->count();
        View::share('comments_count',$comments_count);
    }
}

function has_access_author_admin($accessList,$property,$accessValue=null){
    $result=false;
    try{
        $accessList=json_decode($accessList);
        if(is_object($accessList))
        {
            if(property_exists($accessList,$property))
            {
                if(is_array($accessList->$property))
                {
                    if($accessValue==null){
                        $result=true;
                    }else{
                        foreach($accessList->$property as $key=>$value)
                        {
                            if($value==$accessValue)
                            {
                                $result=true;
                            }
                        }
                    }
                }
            }
        }
    }
    catch (\Exception $e){

    }
    return $result;
}
function set_submission_event($old_status,$status,$order_id,$tozihat,$user_id){
    $event=new \App\SubmissionEvent();
    $event->from=$old_status;
    $event->to=$status;
    $event->tozihat=$tozihat;
    $event->user_id=$user_id;
    $event->submission_id=$order_id;
    $event->save();
}
function check_save_product_to_cart_table($product_data,$cart_table_data,$user_id){
    $save=true;
    $row_id=0;
    $product_price=0;
    $product_count=0;
    $initial_amount=0;
    $product_status='available';
    $price=0;
    if(is_integer($product_data['price2']))
    {
        $price=$product_data['price2'];
    }
    else{
        $price=$product_data['int_price'];
    }
    foreach ($cart_table_data as $key=>$value){
        if($value->product_id==$product_data['product_id'] && $value->product_warranty_id==$product_data['product_warranty_id']
        && $value->color_id==$product_data['color_id']   && $value->warranty_id==$product_data['warranty_id'])
        {
            $save=false;
            $product_price=$value->initial_amount;
            $product_count=$value->count;
            $product_status=$value->product_status;
            $row_id=$value->id;
        }
    }
    if($save)
    {
        DB::table('cart')->insert([
            'user_id'=>$user_id,
            'product_id'=>$product_data['product_id'],
            'product_warranty_id'=>$product_data['product_warranty_id'],
            'warranty_id'=>$product_data['warranty_id'],
            'color_id'=>$product_data['color_id'],
            'count'=>$product_data['product_count'],
            'initial_amount'=>$price,
        ]);
        $initial_amount=$price;
    }
    else{
        if($product_data['product_count']!=$product_count)
        {
            $status=$product_data['product_count']==0 ? 'unavailable' : $product_data['product_count'];

            DB::table('cart')->where(['user_id'=>$user_id,'id'=>$row_id])
                ->update(['product_status'=>$status]);
        }
        else{
            if($product_price!=$price){
                $price=$price/$product_data['product_count'];
                DB::table('cart')->where(['user_id'=>$user_id,'id'=>$row_id])
                    ->update(['final_amount'=>$price]);
            }

        }
        $initial_amount=$product_price;
    }
    return [
        'initial_amount'=>$initial_amount,
        'initial_product_count'=>$product_count
    ];
}
function get_cart_data($cart_table_data)
{
    $cart=Session::get('cart',array());
    if(sizeof($cart)>0){
        return $cart;
    }
    elseif (sizeof($cart_table_data)>0)
    {
        $cart=array();
        foreach ($cart_table_data as $key=>$value)
        {
            $k=$value->product_warranty_id.'_'.$value->warranty_id.'_'.$value->color_id;
            $cart[$value->product_id]['product_data'][$k]=$value->count;
        }
        return  $cart;
    }
    else{
        return array();
    }
}
function remove_product_of_cart_table($product_id,$warranty_id,$color_id)
{

    $result=false;
    if(Auth::check())
    {
        $user_id=Auth::user()->id;
        $e=explode('_',$warranty_id);
        if(sizeof($e)==2){
            $row=DB::table('cart')->where([
                'product_id'=>$product_id,
                'product_warranty_id'=>$e[0],
                'warranty_id'=>$e[1],
                'color_id'=>$color_id,
                'user_id'=>$user_id
            ])->first();
            if($row){
                DB::table('cart')->where([
                    'product_id'=>$product_id,
                    'product_warranty_id'=>$e[0],
                    'warranty_id'=>$e[1],
                    'color_id'=>$color_id,
                ])->delete();
                Session::forget('cart_final_price');
                Session::forget('discount_value');
                $result=true;
            }
        }


        if($result){
            return \App\Cart::getCartData();
        }
        else{
            return  'error';
        }
    }
    else{
        return 'error';
    }
}
function change_product_count($product_id,$warranty_id,$color_id,$product_count)
{
    if(Auth::check())
    {
        $result=false;
        $user_id=Auth::user()->id;
        $e=explode('_',$warranty_id);
        if(sizeof($e)==2){
            $row=DB::table('cart')->where([
                'product_id'=>$product_id,
                'product_warranty_id'=>$e[0],
                'warranty_id'=>$e[1],
                'color_id'=>$color_id,
                'user_id'=>$user_id
            ])->first();
            if($row){
                if(\App\Cart::check($product_id,$color_id,$warranty_id,$product_count))
                {
                    Session::forget('cart_final_price');
                    Session::forget('discount_value');
                    DB::table('cart')->where([
                        'product_id'=>$product_id,
                        'product_warranty_id'=>$e[0],
                        'warranty_id'=>$e[1],
                        'color_id'=>$color_id,
                    ])->update(['count'=>$product_count]);
                    $result=true;
                }
            }
        }

        if($result){
            return \App\Cart::getCartData();
        }
        else{
            return  'error';
        }
    }
}
function add_product_color($product_id,$color_id){
    $product=Product::where('id',$product_id)->select(['id','cat_id'])->first();
    if($product){
        $data=[
            'product_id'=>$product_id,
            'color_id'=>$color_id,
            'cat_id'=>$product->cat_id
        ];
        \App\ProductColor::firstOrCreate($data);
    }
}
