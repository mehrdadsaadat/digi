<?php $__env->startSection('content'); ?>

    <div class="order_header">
        <img src="<?php echo e(asset('files/images/shop_icon.jpg')); ?>" class="shop_icon">
        <ul class="checkout_steps">
            <li>
                <a class="checkout_step">
                    <div class="step_item active_item" step-title="اطلاعات ارسال"></div>
                </a>
            </li>

            <li class="active">
                <a class="checkout_step">
                    <div class="step_item active_item" step-title="پرداخت"></div>
                </a>
            </li>

            <li class="inactive">
                <a class="checkout_step">
                    <div class="step_item" step-title="اتمام خرید و ارسال"></div>
                </a>
            </li>
        </ul>
    </div>

    <div class="container-fluid">
        <div class="row headline-checkout">
            <h6>انتخاب شیوه پرداخت</h6>
        </div>
        <div class="page_row">
            <div class="page_content">

                <div class="shipping_data_box payment_box" style="margin-top:0px">
                    <span class="radio_check active_radio_check"></span>
                    <span class="label">پرداخت اینترنتی (آنلاین با تمامی کارت های بانکی)</span>
                </div>

                <h6>خلاصه سفارش</h6>

                <div class="shipping_data_box" style="padding-right:15px;padding-left: 15px">

                    <?php  $i=1;?>
                    <?php if($send_type==1): ?>
                            <div class="shipping_data_box" style="padding: 0px">
                                <div class="header_box">
                                    <div>
                                        مرسوله ۱ از ۱
                                        <span>(<?php echo e(replace_number(\App\Cart::get_product_count())); ?> کالا)</span>
                                    </div>
                                    <div>
                                        نحوه ارسال
                                        <span>پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                                    </div>
                                    <div>
                                        ارسال از
                                        <span>
                                            <?php if($send_order_data['normal_send_day']==0): ?>
                                                آماده ارسال
                                            <?php else: ?>
                                                <?php echo e(replace_number($send_order_data['normal_send_day'])); ?> روز کاری
                                            <?php endif; ?>
                                        </span>
                                    </div>

                                    <div>
                                        هزینه ارسال
                                        <span><?php echo e($send_order_data['normal_send_order_amount']); ?> </span>
                                    </div>
                                </div>
                                <div class="ordering_product_list swiper-container">
                                    <div class="swiper-wrapper swiper_product_box">
                                        <?php $__currentLoopData = $send_order_data['cart_product_data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="product_info_box swiper-slide">

                                                <img src="<?php echo e(url('files/thumbnails/'.$product['product_image_url'])); ?>">
                                                <p class="product_title"><?php echo e($product['product_title']); ?></p>
                                                <?php if($product['color_id']>0): ?>
                                                    <p class="product_color">
                                                        <?php if($product['color_type']==1): ?>
                                                            رنگ :
                                                        <?php else: ?>
                                                            سایز
                                                        <?php endif; ?>
                                                        <?php echo e($product['color_name']); ?></p>
                                                <?php endif; ?>

                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-button-next"></div>

                                </div>
                            </div>
                    <?php else: ?>

                        <?php $__currentLoopData = $send_order_data['delivery_order_interval']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="shipping_data_box" style="padding: 0px">
                                <div class="header_box">
                                    <div>
                                        مرسوله <?php echo e(replace_number($i)); ?> از <?php echo e(replace_number(sizeof($send_order_data['delivery_order_interval']))); ?>

                                         <span>(<?php echo e(replace_number(sizeof($send_order_data['array_product_id'][$key]))); ?> کالا)</span>
                                    </div>
                                    <div>
                                        نحوه ارسال
                                        <span>پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                                    </div>
                                    <div>
                                        ارسال از
                                        <span>
                                            <?php if($value['send_order_day_number']==0): ?>
                                                آماده ارسال
                                            <?php else: ?>
                                             <?php echo e(replace_number($value['send_order_day_number'])); ?> روز کاری
                                            <?php endif; ?>
                                        </span>
                                    </div>

                                    <div>
                                        هزینه ارسال
                                        <span><?php echo e($value['send_fast_price']); ?> </span>
                                    </div>
                                </div>
                                <div class="ordering_product_list swiper-container">
                                    <div class="swiper-wrapper swiper_product_box">
                                        <?php $__currentLoopData = $send_order_data['array_product_id'][$key]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="product_info_box swiper-slide">
                                                <?php
                                                $product=$send_order_data['cart_product_data'][$value2.'_'.$key2];
                                                ?>
                                                <img src="<?php echo e(url('files/thumbnails/'.$product['product_image_url'])); ?>">
                                                <p class="product_title"><?php echo e($product['product_title']); ?></p>
                                                <?php if($product['color_id']>0): ?>
                                                  <p class="product_color">
                                                      <?php if($product['color_type']==1): ?>
                                                          رنگ :
                                                      <?php else: ?>
                                                          سایز
                                                      <?php endif; ?>

                                                      <?php echo e($product['color_name']); ?></p>
                                                <?php endif; ?>

                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-button-next"></div>
                                </div>
                            </div>

                            <?php $i++; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>

                <discount-box></discount-box>
                <gift-cart></gift-cart>

            </div>
            <div class="page_aside">
                <div class="order_info" style="margin-top: 0px">
                    <?php
                    $cart_final_price=$send_type==1 ? $send_order_data['integer_normal_cart_price'] : $send_order_data['integer_fasted_cart_amount'];
                    $final_price=Session::get('final_price',0);
                    ?>
                    <ul>
                        <li>
                            <span>مبلغ کل </span>
                            <span>(<?php echo e(replace_number(\App\Cart::get_product_count())); ?>) کالا</span>
                            <span class="left"><?php echo e(replace_number(number_format($final_price))); ?> تومان</span>
                        </li>


                        <li>
                            <span>هزینه ارسال</span>
                            <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="هزینه ارسال مرسولات می‌تواند وابسته به شهر و آدرس گیرنده متفاوت باشد. در صورتی که هر یک از مرسولات حداقل ارزشی برابر با ۱۵۰هزار تومان داشته باشد، آن مرسوله بصورت رایگان ارسال می‌شود."></span>
                            <span class="left" id="total_send_order_price">
                                <?= $send_type==1 ? $send_order_data['normal_send_order_amount'] :  $send_order_data['total_fast_send_amount']  ?>
                            </span>
                        </li>

                        <li class="discount_li" <?php if(Session::get('discount_value',0)>0): ?> style="display: block" <?php endif; ?>>
                            <span>تخفیف</span>
                            <span class="left" id="discount_value">
                                <?php echo e(replace_number(number_format(Session::get('discount_value',0))). ' تومان'); ?>

                            </span>
                        </li>

                        <li class="gift_li" <?php if(Session::get('gift_value',0)>0): ?> style="display: block" <?php endif; ?>>
                            <span>کارت هدیه</span>
                            <span class="left" id="gift_cart_amout">
                                <?php echo e(replace_number(number_format(Session::get('gift_value',0))). ' تومان'); ?>

                            </span>
                        </li>

                    </ul>
                    <div class="checkout_devider"></div>

                        <div class="checkout_content">
                            <p style="color:red">مبلغ قابل پرداخت : </p>
                            <p id="final_price"><?php echo e(replace_number(number_format($cart_final_price))); ?> تومان</p>
                        </div>
                        <a href="<?php echo e(url('order/payment')); ?>">
                            <div class="send_btn checkout">
                                <span class="line"></span>
                                <span class="title">پرداخت و ثبت نهایی سفارش</span>
                            </div>
                        </a>
                </div>
            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/swiper.min.css')); ?>" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>

    <script type="text/javascript" src="<?php echo e(asset('js/swiper.min.js')); ?>"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        const swiper=new Swiper('.swiper-container',{
            slidesPerView:5,
            spaceBetween:0,
            navigation:{
                nextEl:'.swiper-button-next',
                prevEl:'.swiper-button-prev'
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.order', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/shipping/payment.blade.php ENDPATH**/ ?>