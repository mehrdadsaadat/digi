<?php $__env->startSection('content'); ?>

    <div class="row" id="product_box">

        <div class="col-3">

            <div class="item_box" id="filter_div" <?php if(sizeof($_GET)==0 || (sizeof($_GET)==1) && array_key_exists('page',$_GET)): ?> style="display: none" <?php endif; ?>>
                <div class="title_box">
                    <label>فیلتر های اعمال شده</label>
                    <span id="remove_all_filter">حذف</span>
                </div>
                <div id="selected_filter_box">

                </div>
            </div>
            <?php if(isset($category) && sizeof($category->getChild)>0): ?>
                <div class="item_box">
                    <div class="title_box">
                        <label>دسته بندی</label>
                    </div>
                    <ul class="search_category_ul">
                       <li class="parent">
                           <a href="<?php echo e(url('search/'.$category->url)); ?>"><?php echo e($category->name); ?></a>
                           <ul>
                               <?php $__currentLoopData = $category->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                   <li>
                                       <a href="<?php echo e(url('search/'.$cat->url)); ?>"><?php echo e($cat->name); ?></a>
                                   </li>
                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </ul>
                       </li>
                    </ul>
                </div>
            <?php endif; ?>
            <?php if(isset($brands) && sizeof($brands)>0): ?>
                <div class="item_box">
                    <div class="title_box">
                        <label>برند</label>
                        <span class="fa fa-angle-down"></span>
                    </div>
                    <div>
                        <div class="filter_box filter_brand_div" style="display: block">
                            <input type="text" id="brand_search" placeholder="جست و جوی نام برند ...">

                            <ul class="list-inline product_cat_ul">
                                <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <li data="brand_param_<?php echo e($value->brand_id); ?>">
                                        <span class="check_box"></span>
                                        <span class="title"><?php echo e($value->getBrand->brand_name); ?></span>
                                        <span class="ename"><?php echo e($value->getBrand->brand_ename); ?></span>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="item_box">
                <div class="title_box">
                    <label>جست و جو در نتایج:</label>
                </div>
                <div>
                    <input type="text" <?php if(array_key_exists('string',$_GET)): ?> value="<?php echo e($_GET['string']); ?>" <?php endif; ?> id="search_input" placeholder="نام محصول با نام برند مورد نظر خود را بنوسید ...">
                </div>
            </div>

            <div class="item_box toggle_box">
                <div class="toggle-light" id="product_status"></div>
                <span>فقط کالاهای موجود</span>
            </div>

            <div class="item_box toggle_box">
                <div class="toggle-light" id="send_status"></div>
                <span>فقط کالاهای آماده ارسال</span>
            </div>

            <?php if(isset($filter)): ?>
                <?php $__currentLoopData = $filter; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item_box">
                        <div class="title_box">
                            <label><?php echo e($value->title); ?></label>
                            <span class="fa fa-angle-down"></span>
                        </div>
                        <div>
                            <div class="filter_box">
                                <ul class="list-inline product_cat_ul">
                                    <?php $__currentLoopData = $value->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                        $filter_key='attribute['.$value->id.']';
                                        ?>
                                        <li data="<?php echo e($filter_key); ?>_param_<?php echo e($value2->id); ?>">
                                            <span class="check_box"></span>
                                            <span class="title"><?php echo e($value2->title); ?></span>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <div class="item_box">
               <div class="title_box">
                   <label>محدوده قیمت مورد نظر</label>
                   <span class="fa fa-angle-down"></span>
               </div>
               <div>
                   <div class="filter_box" style="display:block">
                       <div class="range_slider_div">
                           <div id="slider" class="price_rang_slider"></div>
                       </div>
                       <ul class="filter_price_ul">
                           <li>
                               <div>از</div>
                               <div class="price" id="min_price"></div>
                               <div>تومان</div>
                           </li>
                           <li>
                               <div>تا</div>
                               <div class="price" id="max_price"></div>
                               <div>تومان</div>
                           </li>
                       </ul>
                       <button class="btn btn-primary" id="price_filter_btn">
                           <span class="fa fa-filter"></span>
                           <span>اعمال محدوده قیمت</span>
                       </button>
                   </div>
               </div>
            </div>
            <?php if(isset($colors) && sizeof($colors)>1): ?>
               <div class="item_box">
                   <div class="title_box">
                       <label>رنگ ها</label>
                       <span class="fa fa-angle-down"></span>
                   </div>
                   <div>
                       <div class="filter_box" style="display: block">
                           <ul class="list-inline product_cat_ul color_filter_ul">
                             <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li data="color_param_<?php echo e($value->id); ?>">
                                    <div>
                                        <span class="check_box"></span>
                                        <span class="title"><?php echo e($value->name); ?></span>
                                    </div>
                                    <div style="background:#<?= $value->code ?>;<?php if($value->name=='سفید'): ?> border:1px solid black <?php endif; ?>" class="color_div"></div>
                                </li>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </ul>
                       </div>
                   </div>
              </div>
           <?php endif; ?>
        </div>

        <div class="col-9" style="position:relative">
            <div style="display: flex;justify-content: space-between;align-items: center">
                <ul class="list-inline map_ul">
                    <li>
                        <a href="<?php echo e(url('/')); ?>">فروشگاه</a>
                        <?php if(isset($category)): ?> / <?php endif; ?>
                    </li>
                    <?php if(isset($category)): ?>
                        <?php if($category->getParent->getParent->name!="-"): ?>
                            <li><a href="<?php echo e(url('main/'.$category->getParent->getParent->url)); ?>"><?php echo e($category->getParent->getParent->name); ?></a> </li>
                        <?php endif; ?>
                        <?php if($category->getParent->name!="-"): ?>
                            <li>  <?php if($category->getParent->getParent->name!="-"): ?>
                                      /
                                  <?php endif; ?>
                                <a href="<?php echo e(url('search/'.$category->getParent->url)); ?>"><?php echo e($category->getParent->name); ?></a></li>
                        <?php endif; ?>
                        <li>
                            / <a href="<?php echo e(url()->current()); ?>">
                                <?php echo e($category->name); ?>

                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
                <div id="product_count">
                </div>
            </div>
            <product-box  :compare="'yes'"></product-box>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(url('css/nouislider.min.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(url('css/toggles-full.css')); ?>"/>
    <script type="text/javascript" src="<?php echo e(url('js/nouislider.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
  <script type="text/javascript" src="<?php echo e(url('js/toggles.min.js')); ?>"></script>
  <script>
      $("#product_status").toggles({
          type:'Light',
          text:{'on':'','off':''},
          width:50,
          direction:'rtl',
          on:true
      });
      $("#send_status").toggles({
          type:'Light',
          text:{'on':'','off':''},
          width:50,
          direction:'rtl',
          on:true
      });
  </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.shop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/shop/cat_product.blade.php ENDPATH**/ ?>