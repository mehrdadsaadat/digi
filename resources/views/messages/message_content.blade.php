@extends('layouts.admin')

@section('content')

    <?php
       $array['users']= 'مدیریت کاربران';
       $array['sellers']= 'مدیریت فروشندگان';
    ?>
    @include('include.breadcrumb',['data'=>[
         ['title'=>$array[$url_param],'url'=>url('admin/'.$url_param)],
         ['title'=>'مدیریت پیام ها','url'=>url('admin/'.$url_param.'/'.$user->id.'/messages')],
         ['title'=>'جزییات پیام','url'=>url('admin/'.$url_param.'/'.$user->id.'/messages/'.$message->id)],
    ]])
    <div class="panel">

        <div class="header">
           <div>
               جزییات پیام - {{ $message->title }}
               , ارسال شده به
            @if($message->to)
                @if($message->to_type=='App\\User')
                    <a href="{{ url('admin/users/'.$message->to->id) }}" target="_blank">
                        <span>
                            {{ $message->to->name }}
                        </span>
                    </a>
                @else
                    <a href="{{ url('admin/sellers/'.$message->to->id) }}" target="_blank">
                        <span>
                            {{ $message->to->brand_name }}
                        </span>
                    </a>
                @endif
            @elseif($message->to==0)
                    <span>
                        {{ config('shop-info.shop_name') }}
                    </span>
            @endif

           </div>
        </div>

        <div class="panel_content">
            <?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
            @include('include.Alert')
            <div class="question_div">
                <div class="question_div_header">
                    <div>
                        @if($message->from_type=='App\\User')
                            @if($message->from)
                                <a href="{{ url('admin/users/'.$message->from->id) }}" target="_blank">
                                    <span class="to_link">
                                        ارسال کننده : {{ $message->from->name }}
                                    </span>
                                </a>
                             @endif
                        @else
                            <a href="{{ url('admin/sellers/'.$message->from->id) }}" target="_blank">
                                <span class="form_link">
                                    ارسال کننده : {{ $message->from->brand_name }}
                                </span>
                            </a>
                        @endif
                    </div>
                    <div>
                        {{ $jdf->jdate('H:i:s',$message->time)  }} / {{  $jdf->jdate('Y-n-j',$message->time) }}
                    </div>
                </div>

                <div class="question_content">
                    {!! strip_tags($message->content,'<br>') !!}
                    @if(!empty($message->file))
                        <div  class="attached-file">
                           <span>فایل ضمیمه شده : </span>
                           <a href="{{ url('/files/upload/'.$message->file) }}" target="_blank">
                              {{ $message->file }}
                           </a>
                       </div>
                    @endif
                </div>
            </div>

            @foreach($message->getAnswer as $answer)
            <div class="question_div">
                <div class="question_div_header">
                    <div>
                        @if($answer->from_type=='App\\User')
                            @if($answer->from)
                                <a href="{{ url('admin/users/'.$answer->from->id) }}" target="_blank">
                                    <span class="to_link">
                                        ارسال کننده : {{ $answer->from->name }}
                                    </span>
                                </a>
                             @endif
                        @else
                            <a href="{{ url('admin/sellers/'.$answer->from->id) }}" target="_blank">
                                <span class="form_link">
                                    ارسال کننده : {{ $answer->from->brand_name }}
                                </span>
                            </a>
                        @endif
                    </div>
                    <div>
                        {{ $jdf->jdate('H:i:s',$answer->time)  }} / {{  $jdf->jdate('Y-n-j',$answer->time) }}
                    </div>
                </div>

                <div class="question_content">
                    {!! strip_tags($answer->content,'<br>') !!}
                    @if(!empty($answer->file))
                        <div  class="attached-file">
                           <span>فایل ضمیمه شده : </span>
                           <a href="{{ url('/files/upload/'.$answer->file) }}" target="_blank">
                              {{ $answer->file }}
                           </a>
                       </div>
                    @endif
                </div>
            </div>
            @endforeach


            <div>
              <span style="color:gray;padding-top:20px;display:block">ارسال پاسخ</span>
              {!! Form::open(['url' => 'admin/'.$url_param.'/'.$user->id.'/messages/'.$message->id,'files'=>true]) !!}

              {{ method_field('PUT') }}

              <div class="form-group center_item">
                {{ Form::textArea('content',null,['class'=>'form-control','style'=>'width:100%;margin-top:20px','placeholder'=>'پاسخ شما']) }}
              </div>
              <div class="form-group">
                @if($errors->has('content'))
                  <p class="has_error">{{ $errors->first('content') }}</p>
                @endif
              </div>



              <div class="form-group">
                {{ Form::label('pic','انتخاب فایل : ') }}
                <input type="file" name="pic" id="pic" style="direction:ltr">
                @if($errors->has('pic'))
                    <span class="has_error">{{ $errors->first('pic') }}</span>
                @endif
              </div>

              <button class="btn btn-success">ارسال پاسخ</button>
              {!! Form::close() !!}
         </div>
        </div>
    </div>

@endsection
