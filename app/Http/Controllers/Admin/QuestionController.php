<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Question;
class QuestionController extends CustomController
{
    protected  $model='Question';
    protected  $title='پرسش';
    protected $route_params='questions';
    public function  index(Request $request)
    {
        $questions=Question::getData($request->all());
        $trash_question_count=Question::onlyTrashed()->count();
        return view('question.index',['questions'=>$questions,'trash_question_count'=>$trash_question_count,'req'=>$request]);
    }
    public function change_status(Request $request)
    {
       return Question::change_status($request);
    }
    public function addAnswer($id,Request $request)
    {
        $answer=$request->get('answer');
        if(!empty($answer))
        {
           return Question::addAnswer($request,$id,$answer);
        }
        else{
           return redirect()->back();
        }
    }
}
