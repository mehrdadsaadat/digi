<?php

namespace App\Http\Requests;

use App\Rules\CheckMobileNumberFormat;
use Illuminate\Foundation\Http\FormRequest;

class SellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => 'required',
            'lname' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'brand_name' => 'required',
            'email' => 'required|email|unique:sellers,email,' . $this->seller . '',
            'mobile' =>['required', 'unique:sellers,mobile,' . $this->seller . '',new CheckMobileNumberFormat],
        ];
    }
}
