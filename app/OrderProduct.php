<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Stockroom;
use Illuminate\Database\Eloquent\Builder;
class OrderProduct extends Model
{
    protected $table='order_products';
    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','product_id')
            ->select(['id','title','image_url','cat_id','brand_id']);
    }
    public function getSeller()
    {
        return $this->hasOne(Seller::class,'id','seller_id')
            ->select(['id','brand_name'])
            ->withDefault(['brand_name'=>env("SHOP_NAME",'')]);
    }
    public function getColor()
    {
        return $this->hasOne(Color::class,'id','color_id')
            ->withDefault(['name'=>'','code'=>'fff']);
    }
    public function getStockroom()
    {
        return $this->hasOne(Stockroom::class,'id','stockroom_id');
    }
    public function getWarranty()
    {
        return $this->hasOne(Warranty::class,'id','warranty_id')
            ->withDefault(['name'=>'']);
    }
    public function getOrder()
    {
        return $this->hasOne(Order::class,'id','order_id');
    }
    public static function setReturnProduct($count,$request,$orderProduct)
    {
        $user_id=$request->user()->id;
        $time=time();
        if($count==$orderProduct->product_count)
        {
            DB::beginTransaction();
            try{
                $orderProduct->send_status=-1;
                $orderProduct->tozihat=$request->get('tozihat');
                $orderProduct->stockroom_id=$request->get('stockroom_id',0);
                $orderProduct->update();
                self::set_sale($orderProduct);
                if($orderProduct->stockroom_id>0)
                {
                    self::addStockroom($orderProduct,$count,$request,$orderProduct->stockroom_id);
                }
                DB::commit();
                return 'ok';
            }
            catch (\Exception $exception)
            {
                DB::rollBack();
                return 'error';
            }
        }
        else{
            DB::beginTransaction();
            try{
                $product_count=$orderProduct->product_count-$count;
                $commission=$orderProduct->commission;
                if($commission>0)
                {
                    $commission=($commission/$orderProduct->product_count)*$product_count;
                }
                $new_record=$orderProduct->replicate();
                $new_record->product_count=$product_count;
                $new_record->commission=$commission;
                $new_record->save();

                $orderProduct->send_status=-1;
                $orderProduct->tozihat=$request->get('tozihat');
                $orderProduct->product_count=$count;
                $orderProduct->stockroom_id=$request->get('stockroom_id',0);
                $orderProduct->update();

                self::set_sale($orderProduct);
                if($orderProduct->stockroom_id>0)
                {
                    self::addStockroom($orderProduct,$count,$request,$orderProduct->stockroom_id);
                }
                DB::commit();
            }
            catch (\Exception $exception)
            {
                DB::rollBack();
            }
        }
    }
    public static function RemoveReturnProduct($request,$orderProduct)
    {
        $user_id=$request->user()->id;
        $time=time();
        DB::beginTransaction();
        $stockroom_id=$orderProduct->stockroom_id;
        try{
            $orderProduct->send_status=6;
            $orderProduct->tozihat=$request->get('tozihat');
            $orderProduct->stockroom_id=0;
            $orderProduct->update();
            self::set_sale($orderProduct,'plus');
            if($stockroom_id>0)
            {
                $count=$orderProduct->product_count;
                self::addStockroom($orderProduct,$count,$request,$stockroom_id);
            }
            DB::commit();
            return 'ok';
        }
        catch (\Exception $exception)
        {
            DB::rollBack();
            return 'error';
        }
    }
    public static function set_sale($orderProduct,$type='minus')
    {
        $time=$orderProduct->time;
        $jdf=new \App\Lib\Jdf();
        $y=$jdf->tr_num($jdf->jdate('Y',$time));
        $m=$jdf->tr_num($jdf->jdate('n',$time));
        $d=$jdf->tr_num($jdf->jdate('j',$time));

        $product_price=($orderProduct->product_price2*$orderProduct->product_count);
        if($orderProduct->seller_id>0)
        {
            if($type=='minus')
            {
                DB::table('sellers')->where('id',$orderProduct->seller_id)
                ->decrement('total_commission',$orderProduct->commission);
                DB::table('sellers')->where('id',$orderProduct->seller_id)
                ->decrement('total_price',$product_price);
            }
            else{
                DB::table('sellers')->where('id',$orderProduct->seller_id)
                ->increment('total_commission',$orderProduct->commission);
                DB::table('sellers')->where('id',$orderProduct->seller_id)
                ->increment('total_price',$product_price);
            }
            set_seller_sale_statistics($product_price,$orderProduct->commission,$y,$m,$d,$orderProduct->seller_id,$type);
        }

        product_sale_statistics($y,$m,$d,$orderProduct->commission,$product_price,$orderProduct->product_id,$orderProduct->seller_id,$type);
        set_overall_statistics($y,$m,$d, $product_price,$orderProduct->commission,$type);
    }
    public static function addStockroom($orderProduct,$count,$request,$stockroom_id)
    {
        $product_warranty=ProductWarranty::where([
            'product_id'=>$orderProduct->product_id,
            'warranty_id'=>$orderProduct->warranty_id,
            'color_id'=>$orderProduct->color_id,
            'seller_id'=>$orderProduct->seller_id,
        ])->withTrashed()->first();
        if($product_warranty)
        {
            $list=$product_warranty->id.'_'.$count;
            $result=Stockroom::add_product($request,$list,$stockroom_id);
        }
        else{

        }
    }
    public static function getList($request)
    {
        $product_title=array_key_exists('string',$request) ? $request['string'] : '';
        $string='?';
        $return_product_list=self::with(['getProduct','getColor','getWarranty','getSeller','getStockroom']);
        if(!empty($product_title))
        {
            define('product_title',$product_title);
            $return_product_list=$return_product_list->whereHas('getProduct',function($query){
                $query->where('title','like','%'.product_title.'%');
            });
        }
        else{
            $return_product_list=$return_product_list->whereHas('getProduct');
        }
        $return_product_list=$return_product_list->where('send_status',-1)->orderBy('id','DESC')->paginate(10);
        $return_product_list->withPath($string);
        return $return_product_list;
    }
    public function getprepartionproduct()
    {
        return $this->hasMany(OrderProduct::class,'product_id','product_id')
        ->whereIn('send_status',[1,2]);
    }
    public function hasproduct()
    {
        return $this->hasOne(InventoryList::class,'product_warranty_id','product_warranty_id')
        ->where('product_count','>','0');
    }
    public static function getSellerOrders($request)
    {
        $seller_id=$request->user()->id;
        $search_text=$request->get('title','');
        $date=$request->get('date');
        define('seller_id',$seller_id);
        $orders=self::with(['getWarranty','getColor'])->where('seller_id',$seller_id);
        if(!empty($date)){
            $first=getTimestamp($date,'first');
            $last=getTimestamp($date,'last');
            $orders=$orders->whereBetween('time',[$first,$last]);
        }
        if(!empty($search_text))
        {
            define('search_text',$search_text);
            $orders=$orders->whereHas('getProduct',function(Builder $query){
                $query->where('title','like','%'.search_text.'%');
            })->with('getProduct');
        }
        else{
            $orders=$orders->whereHas('getProduct')->with('getProduct');
        }
        $orders=$orders->withCount(['getprepartionproduct'=>function($query){
            $query->where('seller_id',seller_id)->select(DB::raw("SUM(product_count)"));
        }]);
        $orders=$orders->withCount(['hasproduct'=>function($query){
            $query->select(DB::raw("SUM(product_count)"));
        }]);
        $orders=$orders->orderBy('id','DESC')->paginate(10);

        return $orders;
    }
}
