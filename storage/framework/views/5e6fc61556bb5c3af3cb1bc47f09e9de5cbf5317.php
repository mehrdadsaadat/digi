<?php if(sizeof($product->Gallery)>0): ?>
<div class="modal fade" id="product_gallery_box" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content gallery_box_content">
        <div class="modal-body">
             <div class="right_box">
                 <div class="img_swiper" id="img_swiper">
                    <?php $__currentLoopData = $product->Gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div class="swiper-slide <?php if($key==0): ?> img_select_border <?php endif; ?>">
                        <img src="<?php echo e(url('files/gallery/'.$value->image_url)); ?>">
                      </div>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 </div>
             </div>
             <div class="left_box">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="gallery_item" id="gallery_item">
                    <?php $__currentLoopData = $product->Gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($key==0): ?>
                        <img ondragstart="return false" onload="set_image_width()" src="<?php echo e(url('files/gallery/'.$value->image_url)); ?>" width="65%" id="selected_img">
                     <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

                <div class="rang_slider">
                    <span class="fa fa-minus rang_slider_minus"></span>
                    <input type="range" value="0" min="0" max="100" id="image_zoom_rang">
                    <span class="fa fa-plus rang_slider_plus"></span>
                </div>
             </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/include/gallery-modal-box.blade.php ENDPATH**/ ?>