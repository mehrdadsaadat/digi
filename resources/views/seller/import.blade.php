@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت فروشندگان','url'=>url('admin/sellers')],
         ['title'=>'ثبت پرداخت ها','url'=>url('admin/seller/pay/import')]
    ]])
    <div class="panel">
        <div class="header">ثبت پرداخت ها</div>

        <div class="panel_content">
            @include('include.warring')
            @include('include.alert')
            {!! Form::open(['url' => 'admin/seller/pay/import','files'=>true]) !!}

            <div class="form-group">
                <label for="payment_file">انتخاب فایل اکسل : </label>
                <input type="file" name="payment_file" id="payment_file"  style="direction:ltr">
                @if($errors->has('payment_file'))
                    <span class="has_error">{{ $errors->first('payment_file') }}</span>
                @endif
            </div>

            <button class="btn btn-success">ثبت </button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
