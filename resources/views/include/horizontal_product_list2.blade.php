@if(sizeof($products)>0)
    <div class="product_box">
        <div class="box_title">
            <h6>{{ $title }}</h6>
        </div>

        <div class="swiper-container products">
            <div class="swiper-wrapper">
                @foreach($products as $product)
                    <?php
                    $price1=$product->price+$product->discount_price;
                    ?>
                    <div class="swiper-slide product">
                        <a href="{{ url('product/dkp-'.$product->id.'/'.$product->product_url) }}">
                            <div style="position: relative">
                                @if(!empty($product->discount_price))
                                    <span class="discount-badge">
                                        <?php
                                        $d=($product->price/$price1)*100;
                                        $d=100-$d;
                                        $d=round($d);
                                        ?>
                                        ٪{{ replace_number($d)  }}
                                    </span>
                                @endif
                               <img src="{{ url('files/thumbnails/'.$product->image_url) }}">
                            </div>
                            <p class="title">
                                @if(strlen($product->title)>33)
                                    {{ mb_substr($product->title,0,33).' ... ' }}
                                @else
                                    {{ $product->title  }}
                                @endif
                            </p>

                            <p class="price">
                                {{ replace_number(number_format($product->price)).' تومان'   }}
                            </p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
