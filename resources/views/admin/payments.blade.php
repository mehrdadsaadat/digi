@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت محموله ها','url'=>url('admin/packages')]]])
    <div class="panel">

        <div class="header">
           مدیریت پرداخت ها
        </div>
<?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
        <div class="panel_content">

            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form package_search">
                <div class="form-group">
                    {{ Form::select('seller_id',$sellers,$req->get('seller_id',''),['class'=>'selectpicker auto_width']) }}
               </div>
               <div class="form-group">
                   <input type="text" autocomplete="off" name="date" class="pdate form-control" id="pcal1" value="{{ $req->get('date','') }}" placeholder="تاریخ پرداخت">
               </div>
              <button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>شناسه پرداخت</th>
                        <th>مبلغ پرداخت شده</th>
                        <th>زمان ثبت</th>
                        <th>فروشنده</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $key=>$value)
                        @php $i++; @endphp
                        <tr>

                            <td>{{ replace_number($i) }}</td>
                            <td>{{ replace_number($value->shebase) }}</td>
                            <td>{{ replace_number(number_format($value->price)) }} ریال</td>
                            <td>{{  $jdf->jdate('Y-n-j',$value->time) }}</td>
                            <td>{{ $value->getSeller->brand_name }}</td>
                        </tr>

                    @endforeach

                    @if(sizeof($payments)==0)
                        <tr>
                            <td colspan="5">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $payments->links() }}
        </div>
    </div>

@endsection
@section('head')
    <link href="{{ asset('css/js-persian-cal.css') }}" rel="stylesheet">
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/js-persian-cal.min.js') }}"></script>
    <script>
        const pcal1= new AMIB.persianCalendar('pcal1');
    </script>
@endsection