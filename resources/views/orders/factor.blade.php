<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>فاکتور</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('head')
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <style>
        body{
            background-color: white !important;
        }
        .product_list_data tr th{
            background-color: white !important;
            color: black !important;
        }
    </style>
</head>
<body>
    <?php
    $order=$submission_info->getOrder;
    ?>
    <div class="container factor">
        @php
            use App\Lib\Jdf;$jdf=new Jdf();
            $total_price=0;
            $order_price=0;
            $total_return_product_price=0;
        @endphp
        <div class="line"></div>
        <div class="header_factor">
            <div>
                <p>
                    <span>تاریخ : {{  $jdf->jdate('j-n-Y',$order->created_at) }}
                    </span>
                    <span>
                        شماره مرسوله : {{ replace_number($submission_info->id) }}
                    </span>
                    <span>
                        تعداد محصول : {{ replace_number(sizeof($order_data['row_data'][$submission_info->id])) }}
                    </span>
                </p>
            </div>
            <div class="title">
                فاکتور سفارش
            </div>
            <div>
                <img src="{{ asset(env("SHOP_LOGO",'files/images/shop_icon.jpg')) }}" class="shop_logo">
            </div>
        </div>

        <table class="table table-bordered order_table_info" style="width:100% !important">

            <tr>
                <td>
                    تحویل گیرنده:
                    <span>{{ $order->getAddress->name }}</span>
                </td>
                <td>
                    شماره تماس تحویل گیرنده:
                    <span>{{ replace_number($order->getAddress->mobile ) }}</span>
                </td>
            </tr>

            <tr>
                <td>
                    آدرس تحویل گیرنده:
                    <span>{{ $order->getAddress->getProvince->name.' '. $order->getAddress->getCity->name.' '. $order->getAddress->address }}</span>
                </td>
                <td>
                    تعداد مرسوله:
                    <span>{{ replace_number(sizeof($order->getOrderInfo)) }}</span>
                </td>
            </tr>

            <tr>
                <td>
                    مبلغ  پرداخت شده:
                    <span>{{ replace_number(number_format($order->price)) }} تومان</span>
                </td>
                <td>
                    مبلغ کل :
                    <span>{{ replace_number(number_format($order->total_price)) }} تومان</span>
                </td>
            </tr>

            @if(!empty($order->gift_value) && $order->gift_value>0)
                <tr>
                    <td>
                       مبلغ کارت هدیه:
                        <span>{{ replace_number(number_format($order->gift_value)) }} تومان</span>
                    </td>
                    <td>
                       کد کارت هدیه:
                        <span>{{ $order->getGiftCart->code }} </span>
                    </td>
                </tr>
            @endif

            @if(!empty($order->discount_value) && $order->discount_value>0)
                <tr>
                    <td>
                        مبلغ کد تخفیف:
                        <span>{{ replace_number(number_format($order->discount_value)) }} تومان</span>
                    </td>
                    <td>
                        کد تخفیف:
                        <span>{{ $order->discount_code }} </span>
                    </td>
                </tr>
            @endif
        </table>
        <p>جزییات مرسوله</p>
        <table class="table product_list_data">
            <thead>
                <tr>
                    <th>نام محصول</th>
                    <th>تعداد</th>
                    <th>قیمت واحد</th>
                    <th>قیمت کل</th>
                    <th>تخفیف</th>
                    <th>قیمت نهایی</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order_data['row_data'][$submission_info->id] as $product)

                <tr>
                    <td class="product__info">
                        <div>
                            <img src="{{ url('files/thumbnails/'.$product['image_url']) }}" />
                            <ul>
                                <li class="title">
                                    {{ $product['title'] }}
                                </li>
                                <li>
                                    <span>فروشنده :‌ ‌</span>
                                    <span>{{ $product['seller'] }}</span>
                                </li>
                                @if($product['color_id']>0)
                                    <li>
                                        @if($product['color_type']==1)
                                            <span> رنگ :‌</span>
                                        @else
                                            <span> سایز :‌</span>
                                        @endif
                                        <span>{{ $product['color_name'] }}</span>
                                    </li>
                                @endif
                                <li>
                                    <span>گارانتی :‌ ‌</span>
                                    <span>{{ $product['warranty_name'] }}</span>
                                </li>
                            </ul>
                        </div>
                    </td>
                    <td>
                        {{ replace_number($product['product_count']) }}
                    </td>
                    <td>
                        {{ replace_number(number_format($product['product_price1'])) }} تومان
                    </td>
                    <td>
                        @php
                            $total_price+=($product['product_price1']*$product['product_count']);
                        @endphp
                        {{ replace_number(number_format($product['product_price1']*$product['product_count'])) }} تومان
                    </td>
                    <td>
                        <?php
                        $discount=(($product['product_price1']*$product['product_count'])-($product['product_price2']*$product['product_count']));
                        ?>
                        {{ replace_number(number_format($discount)) }} تومان

                    </td>
                    <td>
                        @php
                          $order_price+=($product['product_price2']*$product['product_count']);
                        @endphp
                        {{ replace_number(number_format($product['product_price2']*$product['product_count'])) }} تومان

                    </td>
                </tr>
                @if($product['send_status']==-1)
                           <tr>
                               <td colspan="3">
                                   <span>این کالا توسط مشتری برگشت داده شده</span>
                                   @if (!empty($product['tozihat']))
                                       <p style=" margin-top:10px;color:red">
                                           {{ $product['tozihat'] }}
                                       </p>
                                   @endif
                               </td>
                               <td colspan="3">
                                   @php
                                       $p=($product['product_price2']*$product['product_count']);
                                   @endphp
                                   <span>هزینه قابل پرداخت به کاربر :</span>
                                   @php
                                       $return_product_price=get_return_product_price($product['cat_id'],$order_discount,$p);
                                       $total_return_product_price+=$return_product_price;
                                  @endphp
                                   {{ replace_number(number_format($return_product_price)).' تومان' }}
                               </td>
                           </tr>
                        @endif
            @endforeach

                <tr>
                    <td colspan="3"  style="padding-top:20px;padding-bottom:20px">کل</td>
                    <td>
                        {{ replace_number(number_format($total_price)) }} تومان
                    </td>
                    <td>
                        {{ replace_number(number_format(($total_price-$order_price))) }} تومان
                    </td>
                    <td>
                        {{ replace_number(number_format($order_price)) }} تومان
                    </td>
                </tr>
            </tbody>

        </table>

        <table class="table table-bordered factor_price_table">
            <tr>
                <td>مبلغ کل</td>
                <td>
                    {{ replace_number(number_format($order_price)) }} تومان
                </td>
            </tr>
            <tr>
                <td>
                    هزینه ارسال
                </td>
                <td>
                    @if($submission_info['send_order_amount']==0)
                    رایگان
                   @else
                      {{ replace_number(number_format($submission_info['send_order_amount'])) }} تومان
                  @endif
                </td>
            </tr>
            @if($total_return_product_price>0)
            <tr>
                 <td style="color:red">
                   مبلغ قابل برگشت به کاربر
                 </td>
                <td>
                     {{ replace_number(number_format($total_return_product_price)) }} تومان
                 </td>
            </tr>
            @endif

            <tr>
                <td>مبلغ نهایی</td>
                <td>
                    @php
                        $final_price=$order_data['order_row_amount'][$submission_info->id]-$total_return_product_price;
                    @endphp
                    {{ replace_number(number_format($final_price)) }} تومان
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
