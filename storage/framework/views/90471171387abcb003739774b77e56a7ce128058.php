<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت برند ها','url'=>url('admin/brands')],
         ['title'=>'افزودن برند جدید','url'=>url('admin/brands/create')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">
        <div class="header">افزودن برند جدید</div>

        <div class="panel_content">

            <?php echo Form::open(['url' => 'admin/brands','files'=>true]); ?>


            <div class="form-group">

                <?php echo e(Form::label('brand_name','نام برند : ')); ?>

                <?php echo e(Form::text('brand_name',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('brand_name')): ?>
                    <span class="has_error"><?php echo e($errors->first('brand_name')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('brand_ename','نام انگلیسی برند : ')); ?>

                <?php echo e(Form::text('brand_ename',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('brand_ename')): ?>
                    <span class="has_error"><?php echo e($errors->first('brand_ename')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('tozihat','توضیحات : ')); ?>

                <?php echo e(Form::textArea('tozihat',null,['class'=>'form-control brand_tozihat'])); ?>


            </div>



            <div class="form-group">
                <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                <div onclick="select_file()" class="btn btn-primary">انتخاب ایکون برند</div>
                <?php if($errors->has('pic')): ?>
                    <span class="has_error"><?php echo e($errors->first('pic')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <img  onclick="select_file()" style="margin-top:0px" id="output">

            </div>
            <button class="btn btn-success">ثبت برند</button>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/brand/create.blade.php ENDPATH**/ ?>