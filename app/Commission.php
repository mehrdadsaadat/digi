<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Commission extends Model
{
    use SoftDeletes;
    protected $fillable=['cat_id','brand_id','percentage'];
    public static function getData($request)
    {
        $string='?';
        $commissions=self::orderBy('id','DESC')->with(['getBrand','getCategory']);
        if(inTrashed($request))
        {
            $commissions=$commissions->onlyTrashed();
            $string=create_paginate_url($string,'trashed=true');
        }
        if(array_key_exists('brand_id',$request) && !empty($request['brand_id']))
        {
            $commissions=$commissions->where('brand_id',$request['brand_id']);
            $string=create_paginate_url($string,'string='.$request['brand_id']);
        }
        if(array_key_exists('cat_id',$request) && !empty($request['cat_id']))
        {
            $commissions=$commissions->where('cat_id',$request['cat_id']);
            $string=create_paginate_url($string,'string='.$request['cat_id']);
        }
        $commissions=$commissions->paginate(10);
        $commissions->withPath($string);
        return $commissions;
    }
    public function getBrand()
    {
        return $this->hasone(Brand::class,'id','brand_id')->withDefault(['brand_id'=>'حذف شده']);
    }
    public function getCategory()
    {
        return $this->hasone(Category::class,'id','cat_id')->withDefault(['name'=>'حذف شده']);
    }
}
