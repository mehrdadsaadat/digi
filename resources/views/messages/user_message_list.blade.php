@extends('layouts.admin')

@section('content')

    <?php
       $array['users']= 'مدیریت کاربران';
       $array['sellers']= 'مدیریت فروشندگان';
    ?>
    @include('include.breadcrumb',['data'=>[
         ['title'=>$array[$url_param],'url'=>url('admin/'.$url_param)],
         ['title'=>'مدیریت پیام ها','url'=>url('admin/'.$url_param.'/'.$user->id.'/messages')],
    ]])
    <div class="panel">

        <div class="header">
              پیام های ارسالی و دریافتی از
             @if($url_param=='users')
                {{ $user->name }}
             @else
                {{ $user->brand_name }}
             @endif
        </div>

        <div class="panel_content">
            <?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>
            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="title" class="form-control" value="{{ $req->get('title','') }}" placeholder="عنوان پیام"><button class="btn btn-primary">جست و جو</button>
            </form>
            <a href="{{ url('admin/'.$url_param.'/'.$user->id.'/messages/create') }}" class="btn btn-success" style="margin-bottom:10px;font-size:14px">
                <span class="fa fa-envelope"></span>
                <span>ارسال پیام به
                   @if($url_param=='users')
                      کاربر
                   @else
                       فروشنده
                   @endif
                </span>
            </a>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>عنوان</th>
                        <th>ارسال کننده</th>
                        <th>دریافت کننده</th>
                        <th>زمان ارسال</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $key=>$value)
                        @php $i++; @endphp
                        <tr>

                            <td>{{ replace_number($i) }}</td>
                            <td>
                                {{ $value->title }}
                            </td>
                            <td>
                                @if($value->from_type=='App\\User')
                                   @if($value->from)
                                   <a href="{{ url('admin/users/'.$value->from->id) }}" target="_blank">
                                       <span class="form_link">
                                         {{ $value->from->name }}
                                       </span>
                                   </a>
                                   @endif
                                @else
                                    <a href="{{ url('admin/sellers/'.$value->from->id) }}" target="_blank">
                                       <span class="form_link">
                                         {{ $value->from->brand_name }}
                                       </span>
                                   </a>
                                @endif
                            </td>
                            <td>
                             @if($value->to)
                               @if($value->to_type=='App\\User')
                                   <a href="{{ url('admin/users/'.$value->to->id) }}" target="_blank">
                                       <span class="to_link">
                                         {{ $value->to->name }}
                                       </span>
                                   </a>
                                @else
                                    <a href="{{ url('admin/sellers/'.$value->to->id) }}" target="_blank">
                                       <span class="to_link">
                                         {{ $value->to->brand_name }}
                                       </span>
                                     </a>
                                @endif
                             @elseif($value->to==0)
                                 <span class="to_link">
                                     {{ config('shop-info.shop_name') }}
                                 </span>
                             @endif

                            </td>
                            <td>{{ $jdf->jdate('H:i:s',$value->time)  }} / {{  $jdf->jdate('Y-n-j',$value->time) }}</td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/'.$url_param.'/'.$user->id."/messages/".$value->id) }}"><span class="fa fa-eye" @if($value->status==1) style="color:red" @endif></span></a>
                                @endif

                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($messages)==0)
                        <tr>
                            <td colspan="6">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $messages->links() }}
        </div>
    </div>

@endsection
