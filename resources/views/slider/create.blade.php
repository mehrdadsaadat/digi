@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت اسلایدر ها','url'=>url('admin/sliders')],
         ['title'=>'افزودن اسلایدر جدید','url'=>url('admin/brands/sliders')]
    ]])

    <div class="panel">

        <div class="header">افزودن اسلایدر جدید</div>

        <div class="panel_content">


            {!! Form::open(['url' => 'admin/sliders','files'=>true]) !!}

            <div class="form-group">

                {{ Form::label('title','عنوان : ') }}
                {{ Form::text('title',null,['class'=>'form-control']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('url','آدرس (url) : ') }}
                {{ Form::text('url',null,['class'=>'form-control left total_width_input']) }}
                @if($errors->has('url'))
                    <span class="has_error">{{ $errors->first('url') }}</span>
                @endif
            </div>





            <div class="form-group">
                <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                <div onclick="select_file()" class="btn btn-primary">انتخاب تصویر اسلایدر</div>
                @if($errors->has('pic'))
                    <span class="has_error">{{ $errors->first('pic') }}</span>
                @endif
            </div>

            <div class="form-group">
                <img  onclick="select_file()" style="margin-top:0px" id="output" class="slider_img">
            </div>


            <div class="form-group">
                <input type="file" name="mobile_pic" id="mobile_pic" onchange="loadFile2(event)" style="display:none">
                <div onclick="select_file2()" class="btn btn-primary">انتخاب تصویر اسلایدر برای نمایش در گوشی موبایل</div>
                @if($errors->has('mobile_pic'))
                    <span class="has_error">{{ $errors->first('mobile_pic') }}</span>
                @endif
            </div>

            <div class="form-group">
                <img  onclick="select_file2()" style="margin-top:0px" id="output2" class="slider_img">
            </div>

            <button class="btn btn-success">ثبت اسلایدر</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
