<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>فاکتور</title>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <?php echo $__env->yieldContent('head'); ?>
    <link href="<?php echo e(asset('css/admin.css')); ?>" rel="stylesheet">
    <style>
        body{
            background-color: white !important;
        }
    </style>
</head>
<body>
    <?php
       $Jdf=new \App\Lib\Jdf();
       $count=8;
       $size=sizeof($input['stockroom_products']);
       $n=ceil(( $size/$count));

    ?>
    <div
        class="container"
        style="display:flex;justify-content: center;margin-top:20px"
        id="prind_div"
      >
        <button onclick="printFactor()" class="btn btn-danger factor_btn">پرینت فاکتور</button>
      </div>
    <?php for($i = 0; $i < $n; $i++): ?>
      <div class="container factor print_page">
        <div class="line"></div>
        <div class="header_factor">
            <?php
                $p=$i+1;
            ?>
            <div>
                <p>
                    <span>تاریخ : <?php echo e($Jdf->jdate('H:i:s',$input['stockroomEvent']->time)); ?> / <?php echo e($Jdf->jdate('d-m-Y',$input['stockroomEvent']->time)); ?>

                    </span>
                    <span>
                        شماره فاکتور : <?php echo e(replace_number($input['stockroomEvent']->id)); ?>

                    </span>
                    <span>
                        تعداد محصول : <?php echo e(replace_number(sizeof($input['stockroom_products']))); ?>

                    </span>
                    <?php if($n>1): ?>
                    <span>
                        برگه <?php echo e(replace_number($p)); ?> از <?php echo e(replace_number($n)); ?>

                    </span>
                    <?php endif; ?>
                </p>
            </div>
            <div class="title">
                <?php if($type=="input"): ?>
                   ورود کالا به انبار
                <?php else: ?>
                    خروج کالا از انبار
                <?php endif; ?>
            </div>
            <div>
                <img src="<?php echo e(asset(env("SHOP_LOGO",'files/images/shop_icon.jpg'))); ?>" class="shop_logo">
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ردیف</th>
                    <th>تصویر </th>
                    <th>عنوان محصول</th>
                    <th>فروشنده</th>
                    <th>گارانتی</th>
                    <th>رنگ</th>
                    <th>تعداد</th>
                </tr>
            </thead>
            <tbody>
               <?php $j=(($i-0)*$count) ; ?>
               <?php $__currentLoopData = $input['stockroom_products']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <?php if($key>=$j && $key<$j+$count): ?>
                   <tr>
                    <td><?php echo e(replace_number(++$key)); ?></td>
                    <td>
                      <img src="<?php echo e(url('files/thumbnails/'.$value->getProductWarranty->getProduct->image_url)); ?>" class="product_pic stockroom_product">
                    </td>
                    <td><?php echo e($value->getProductWarranty->getProduct->title); ?></td>
                    <td><?php echo e($value->getProductWarranty->getSeller->brand_name); ?></td>
                    <td><?php echo e($value->getProductWarranty->getWarranty->name); ?></td>
                    <td style="width:150px">
                         <?php if($value->getProductWarranty->getColor->id>0): ?>
                            <?php if($value->getProductWarranty->getColor->type==1): ?>
                                <span style="background:#<?php echo e($value->getProductWarranty->getColor->code); ?>" class="color_td">
                                       <span style="color:white"><?php echo e($value->getProductWarranty->getColor->name); ?></span>
                                   </span>
                            <?php else: ?>
                                <span>سایز : <?php echo e($value->getProductWarranty->getColor->name); ?></span>
                            <?php endif; ?>
                         <?php endif; ?>
                    </td>
                    <td><?php echo e(replace_number($value->product_count)); ?></td>
                </tr>
                   <?php endif; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>

         <?php if($i==($n-1)): ?>
         <div class="factor_tozihat">
            <span>کالا های فوق توسط </span>
            <?php echo e($input['stockroomEvent']->getUser->name); ?>

            <span>
                <?php if($type=="input"): ?>
                  به  <?php echo e($input['stockroomEvent']->getStockroom->name); ?> اضافه شده
                <?php else: ?>
                    از  <?php echo e($input['stockroomEvent']->getStockroom->name); ?> خارج شده
                <?php endif; ?>
            </span>
        </div>

        <div class="factor_footer">
            <span>
                مهر و امضای تحویل گیرنده
            </span>

            <span>
                مهر و امضای تحویل دهنده
            </span>

        </div>
         <?php endif; ?>
    </div>
    <?php endfor; ?>

 <script>
    printFactor=function(){
        document.getElementById('prind_div').style.display='none';
        window.print();
    };
    window.onafterprint = function() {
       document.getElementById('prind_div').style.display='flex';
    };
</script>
</body>
</html>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/stockroom/factor.blade.php ENDPATH**/ ?>