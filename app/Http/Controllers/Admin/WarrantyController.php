<?php

namespace App\Http\Controllers\Admin;

use App\Warranty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WarrantyController extends CustomController
{
    protected  $model='Warranty';
    protected  $title='گارانتی';
    protected $route_params='warranties';
    public function index(Request $request)
    {
        $warranty=Warranty::getData($request->all());
        $trash_warranty_count=Warranty::onlyTrashed()->count();
        return view('warranty.index',['warranty'=>$warranty,'trash_warranty_count'=>$trash_warranty_count,'req'=>$request]);
    }
    public function create()
    {
        return view('warranty.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,['name'=>'required'],[],['name'=>'نام گارانتی']);
        $warranty=new Warranty($request->all());
        $warranty->saveOrFail();
        return redirect('admin/warranties')->with('message','ثبت گارانتی با موفقیت انجام شد');
    }
    public function edit($id)
    {
        $warranty=Warranty::findOrFail($id);
        return view('warranty.edit',['warranty'=>$warranty]);
    }
    public function update($id,Request $request)
    {
        $this->validate($request,['name'=>'required'],[],['name'=>'نام گارانتی']);
        $warranty=Warranty::findOrFail($id);
        $warranty->update($request->all());
        return redirect('admin/warranties')->with('message','ویرایش گارانتی با موفقیت انجام شد');
    }
}
