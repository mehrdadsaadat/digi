@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
        ['title'=>'مدیریت فروشندگان','url'=>url('admin/sellers')],
        ['title'=>'مشخصات فروشنده','url'=>url('admin/sellers/'.$seller->id)]
    ]])
    <div class="panel">

        <div class="header">
            مشخصات فروشنده - {{ $seller->brand_name }}
        </div>

        <div class="panel_content">
            <div class="row order_info" style="background-color: #f6f8fa !important;">
                <div class="col-md-6">
                    <ul class="seller_info">
                        <li>
                            <span>نام و نام خانوادگی : </span>
                            <span>{{ $seller->fname.' '.$seller->lname}}</span>
                        </li>
                         <li>
                            <span>شماره موبایل : </span>
                            <span>{{ $seller->mobile }}</span>
                        </li>
                         <li>
                            <span>ایمیل : </span>
                            <span>{{ $seller->email }}</span>
                        </li>
                         <li>
                            <span>تعداد محصول : </span>
                            <span>{{ replace_number($seller->product_count) }}</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                      <ul class="seller_info">
                        <li>
                            <span>استان : </span>
                            <span>{{ $seller->getProvince->name }}</span>
                        </li>
                         <li>
                            <span>شهر : </span>
                            <span>{{ $seller->getCity->name }}</span>
                        </li>
                         <li>
                            @php
                                $Jdf=new \App\Lib\Jdf();
                                $e=explode(' ',$seller->created_at);
                                $e2=explode('-',$e[0]);
                            @endphp
                            <span>تاریخ عضویت : </span>
                            <span>{{ replace_number($Jdf->gregorian_to_jalali($e2[0],$e2[1],$e2[2],'/')) }}</span>
                        </li>
                        <li>
                            <span>درآمد کل فروشنده : </span>
                            <span>
                                @php
                                     $price=$seller->total_price - $seller->total_commission;
                                @endphp
                                {{ replace_number(number_format($price)) }} تومان
                            </span>
                        </li>
                    </ul>
                </div>
            </div>

            <p style="padding-top:20px;padding-bottom:0px;color:red">مدارک آپلود شده فروشنده</p>

            @if($document)
                <table class="table table-bordered table-striped" style="margin-top:20px">
                   <thead>
                      <tr>
                        <th>ردیف</th>
                        <th>عنوان</th>
                        <th>تصویر</th>
                        <th></th>
                      </tr>
                   </thead>
                   <tbody>
                       <?php $i=0;?>
                       @if(!empty($document->shenasname))
                       <?php $i++; ?>
                       <tr>
                          <td>{{ replace_number($i) }}</td>
                           <td style="width:250px">اسکن صفحه اصلی شناسنامه</td>
                           <td>
                               <img src="{{ url('files/seller/'.$document->shenasname) }}" style="max-width:30%;">
                           </td>
                           <td>
                               <a href="{{ url('files/seller/'.$document->shenasname) }}" target="_blank">
                                   <span class="fa fa-eye"></span>
                               </a>
                           </td>
                       </tr>
                       @endif

                      @if(!empty($document->cart))
                       <?php $i++; ?>
                       <tr>
                          <td>{{ replace_number($i) }}</td>
                           <td>اسکن کارت ملی</td>
                           <td>
                               <img src="{{ url('files/seller/'.$document->cart) }}" style="max-width:30%;">
                           </td>
                           <td>
                               <a href="{{ url('files/seller/'.$document->cart) }}" target="_blank">
                                   <span class="fa fa-eye"></span>
                               </a>
                           </td>
                       </tr>
                       @endif

                        @if(!empty($document->rooznamepic))
                       <?php $i++; ?>
                       <tr>
                          <td>{{ replace_number($i) }}</td>
                           <td>اسکن روزنامه ثبت شرکت</td>
                           <td>
                               <img src="{{ url('files/seller/'.$document->rooznamepic) }}" style="max-width:30%;">
                           </td>
                           <td>
                               <a href="{{ url('files/seller/'.$document->rooznamepic) }}" target="_blank">
                                   <span class="fa fa-eye"></span>
                               </a>
                           </td>
                       </tr>
                       @endif
                   </tbody>
                </table> 
            @endif
            <p style="padding-top:10px;padding-bottom:10px;color:red">محصولات ارائه شده توسط فروشنده</p>
            <?php use App\Product;$i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="post" id="data_form">
                @csrf
                <?php  $status=Product::ProductStatus(); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>تصویر محصول</th>
                        <th>عنوان</th>
                        <th>وضعیت محصول</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $key=>$value)
                        @php $i++; $value=$value->getProduct @endphp
                        <tr>
                            <td>{{ replace_number($i) }}</td>
                            <td><img src="{{ url('files/thumbnails/'.$value->image_url) }}" class="product_pic"></td>
                            <td>{{ $value->title }}</td>
                            <td style="width:140px">
                                @if(array_key_exists($value->status,$status))
                                    <span class="alert @if($value->status==1) alert-success @else alert-warning @endif" style="font-size:13px;padding:5px 7px">
                                        {{ $status[$value->status] }}
                                    </span>
                                @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($products)==0)
                        <tr>
                            <td colspan="4">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $products->links() }}
        </div>
    </div>

@endsection
