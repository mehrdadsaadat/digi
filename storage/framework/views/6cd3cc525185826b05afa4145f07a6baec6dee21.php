<?php $__env->startSection('content'); ?>

    <?php use App\Lib\Jdf;use App\Order;$Jdf=new Jdf(); $OrderStatus=Order::OrderStatus() ?>

    <div>
        <div class="profile_item_header order_content_header">
            <div>
                <span>سفارش</span>
                <span><?php echo e(replace_number($order->order_id)); ?></span>
            </div>
            <a href="<?php echo e(url('user/profile/orders')); ?>">
                <span>بازگشت</span>
                <span class="fa fa-angle-left"></span>
            </a>
        </div>

        <div class="profile_item order_content">
            <div class="profile_info_row remove_border">
                <span>تحویل گیرنده :‌</span>
                <span><?php echo e($order->getAddress->name); ?></span>
            </div>

            <div class="profile_info_row">
                <span> شماره تماس تحویل گیرنده:</span>
                <span><?php echo e(replace_number($order->getAddress->mobile )); ?></span>
            </div>
            <div class="profile_info_row">
                <span>تعداد مرسوله:</span>
                <span><?php echo e(replace_number(sizeof($order->getOrderInfo))); ?></span>
            </div>
            <div class="profile_info_row">
                <span> مبلغ قابل پرداخت:</span>
                <span><?php echo e(replace_number(number_format($order->price))); ?> تومان</span>
            </div>
            <div class="profile_info_row">
                <span>مبلغ کل :</span>
                <span><?php echo e(replace_number(number_format($order->total_price))); ?> تومان</span>
            </div>
            <div class="profile_info_row">
                <span>تاریخ ثبت سفارش :‌</span>
                <span><?php echo e($Jdf->jdate(' j F Y',$order->created_at)); ?></span>
            </div>
            <?php if(!empty($order->gift_value) && $order->gift_value>0): ?>
                <div class="profile_info_row">
                    <span> مبلغ کارت هدیه:</span>
                    <span><?php echo e(replace_number(number_format($order->gift_value))); ?> تومان</span>
                </div>
                <div class="profile_info_row">
                    <span>کد کارت هدیه:</span>
                    <span><?php echo e($order->getGiftCart->code); ?> </span>
                </div>
            <?php endif; ?>
            <?php if(!empty($order->discount_value) && $order->discount_value>0): ?>
                <div class="profile_info_row">
                    <span>مبلغ کد تخفیف:</span>
                    <span><?php echo e(replace_number(number_format($order->discount_value))); ?> تومان</span>
                </div>
                <div class="profile_info_row">
                    <span>کد تخفیف:</span>
                    <span><?php echo e($order->discount_code); ?> </span>
                </div>
            <?php endif; ?>

            <div class="profile_info_row">
                <span> آدرس تحویل گیرنده:</span>
                <span style="max-width: 150px"><?php echo e($order->getAddress->getProvince->name.' '. $order->getAddress->getCity->name.' '. $order->getAddress->address); ?></span>

            </div>

            <div class="profile_info_row">
                <span></span>
                <span></span>
            </div>
        </div>

        <?php $__currentLoopData = $order->getOrderInfo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="profile_item">
                <div class="header">
                    <?php echo e(\App\Order::getOrderStatus($value['send_status'],$OrderStatus)); ?>

                </div>
                <?php if($value['send_status']==0 &&  $order->pay_status=='ok'): ?> <?php $value['send_status']=1 ?> <?php endif; ?>
                <div class="swiper-container order_steps">
                    <div class="swiper-wrapper">
                        <?php $__currentLoopData = $OrderStatus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($key>-1): ?>
                                <div class="swiper-slide">
                                    <div class="step_div <?php if($value['send_status']<$key): ?> step_inactive <?php endif; ?>">
                                        <img src="<?php echo e(url('files/images/step'.$key.'.svg')); ?>" <?php if($key==6): ?>  style="width:55%" <?php endif; ?>/>
                                        <span><?php echo e($status); ?></span>
                                    </div>

                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>

                <div class="profile_info_row">
                    <span>کد مرسوله:</span>
                    <span><?php echo e(replace_number( $value['id'] )); ?></span>
                </div>
                <div class="profile_info_row">
                    <span> زمان تحویل:</span>
                    <span><?php echo e($value['delivery_order_interval']); ?></span>
                </div>
                <div class="profile_info_row">
                    <span>هزینه ارسال:</span>
                    <span>
                         <?php if($value['send_order_amount']==0): ?>
                            رایگان
                        <?php else: ?>
                            <?php echo e(replace_number(number_format($value['send_order_amount']))); ?> تومان
                        <?php endif; ?>
                    </span>
                </div>
                <div class="profile_info_row">
                    <span> مبلغ این مرسوله:</span>
                    <span>
                         <?php echo e(replace_number(number_format($order_data['order_row_amount'][$value->id]))); ?> تومان
                    </span>
                </div>
                <div class="profile_info_row">
                    <span>  نحوه ارسال:</span>
                    <span style="max-width: 150px">پست پیشتاز با ظرفیت اختصاصی برای تنیس شاپ</span>
                </div>
            </div>
            <div class="product_box">
                <div class="swiper-container products" id="order_product_box">
                    <div class="swiper-wrapper">
                        <?php $__currentLoopData = $order_data['row_data'][$value->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="swiper-slide product">
                                <div style="position: relative">
                                    <span class="order_product_count"><?php echo e(replace_number($product['product_count'])); ?></span>
                                    <img src="<?php echo e(url('files/thumbnails/'.$product['image_url'])); ?>" />
                                </div>
                                <ul>
                                    <li class="title">
                                        <?php echo e($product['title']); ?>

                                    </li>
                                    <?php if($product['color_id']>0): ?>
                                        <li>
                                            <?php if($product['color_type']==1): ?>
                                                <span> رنگ :‌</span>
                                            <?php else: ?>
                                                <span> سایز :‌</span>
                                            <?php endif; ?>
                                            <span><?php echo e($product['color_name']); ?></span>
                                        </li>
                                    <?php endif; ?>
                                    <li>
                                        <span>گارانتی :‌ ‌</span>
                                        <span><?php echo e($product['warranty_name']); ?></span>
                                    </li>

                                    <li class="order_product_price">
                                        <?php echo e(replace_number(number_format($product['product_price2']*$product['product_count']))); ?> تومان
                                    </li>
                                </ul>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/swiper.min.css')); ?>" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(asset('js/swiper.min.js')); ?>"></script>
    <script>
        const swiper=new Swiper('.order_steps',{
            slidesPerView:2,
            spaceBetween:0,
            navigation:{
                nextEl:'.swiper-button-next',
                prevEl:'.swiper-button-prev'
            }
        });
        const swiper2=new Swiper('.products',{
            slidesPerView:2,
            spaceBetween:10,
            navigation:{
                nextEl:'.swiper-button-next',
                prevEl:'.swiper-button-prev'
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.mobile', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/userPanel/show_order.blade.php ENDPATH**/ ?>