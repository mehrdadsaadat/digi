@extends('layouts.mobile')

@section('content')
    <div class="o-collapse">
        <?php
          $list=array();
          $list[0]=['title'=>'سفارشات','link'=>'user/profile/orders'];
          $list[1]=['title'=>'لیست علاقه مندی ها','link'=>'user/profile/favorite'];
          $list[2]=['title'=>'نقد و نظرات','link'=>'user/profile/comments'];
          $list[3]=['title'=>'کارت های هدیه','link'=>'user/profile/gift-cart'];
          $list[4]=['title'=>'آدرس ها','link'=>'user/profile/address'];
          $list[5]=['title'=>'پیغام های من','link'=>'user/profile/messages'];
          $list[6]=['title'=>'اطلاعات شخصی','link'=>'user/profile/personal-info'];
        ?>
        <div  class="profile_item profile_item_list">
            @foreach($list as $key=>$value)
                <a href="{{ url($value['link']) }}">
                    <div class="profile_item_header">
                        <span>{{ $value['title'] }}</span>
                        <span class="fa fa-angle-left"></span>
                    </div>
                </a>
            @endforeach

            <form method="post" action="{{ url('logout') }}" id="logout_form">@csrf</form>

            <div class="profile_item_header" style="border: 0px" onclick="$('#logout_form').submit()">
                 <span>خروج</span>
            </div>

        </div>
    </div>
@endsection
