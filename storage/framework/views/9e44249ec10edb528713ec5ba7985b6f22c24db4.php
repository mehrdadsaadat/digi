<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
        ['title'=>'مدیریت انبار ها','url'=>url('admin/stockrooms')],
        ['title'=>'لیست ورودی های انبار','url'=>url('admin/stockroom/input')],
        ['title'=>'ورودی انبار','url'=>url('admin/stockroom/input/'.$input['stockroomEvent']->id)]
        ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

         <?php
             $Jdf=new \App\Lib\Jdf()
         ?>
        <div class="header">
            محصولات اضافه شده به <?php echo e($input['stockroomEvent']->getStockroom->name); ?> توسط <?php echo e($input['stockroomEvent']->getUser->name); ?>


            <div style="margin-left:15px">
                <?php echo e($Jdf->jdate('H:i:s',$input['stockroomEvent']->time)); ?> / <?php echo e($Jdf->jdate('Y-m-d',$input['stockroomEvent']->time)); ?>

              <span class="fa fa-calendar"></span>
            </div>
        </div>

        <div class="panel_content">

            <form method="get" class="search_form">
                <input type="text" name="string" class="form-control" value="<?php echo e($req->get('string','')); ?>" placeholder="نام محصول ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <a href="<?php echo e(url('admin/factor/'.$input['stockroomEvent']->id.'/input')); ?>" class="btn btn-primary" target="_blank"  style="margin-bottom:20px">نمایش فاکتور</a>
            <?php if(!empty($input['stockroomEvent']->tozihat)): ?>
            <div class="tozihat">
                <span>توضیحات</span>
                <?php echo e($input['stockroomEvent']->tozihat); ?>

            </div>
            <?php endif; ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>تصویر محصول</th>
                        <th>عنوان محصول</th>
                        <th>فروشنده</th>
                        <th>گارانتی</th>
                        <th>رنگ</th>
                        <th>تعداد</th>
                    </tr>
                </thead>
                <tbody>
                   <?php $__currentLoopData = $input['stockroom_products']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <tr>
                           <td><?php echo e(replace_number(++$key)); ?></td>
                           <td>
                             <img src="<?php echo e(url('files/thumbnails/'.$value->getProductWarranty->getProduct->image_url)); ?>" class="product_pic stockroom_product">
                           </td>
                           <td><?php echo e($value->getProductWarranty->getProduct->title); ?></td>
                           <td><?php echo e($value->getProductWarranty->getSeller->brand_name); ?></td>
                           <td><?php echo e($value->getProductWarranty->getWarranty->name); ?></td>
                           <td style="width:150px">
                                <?php if($value->getProductWarranty->getColor->id>0): ?>
                                   <?php if($value->getProductWarranty->getColor->type==1): ?>
                                       <span style="background:#<?php echo e($value->getProductWarranty->getColor->code); ?>" class="color_td">
                                       <span style="color:white"><?php echo e($value->getProductWarranty->getColor->name); ?></span>
                                   </span>
                                   <?php else: ?>
                                       <span>سایز : <?php echo e($value->getProductWarranty->getColor->name); ?></span>
                                   <?php endif; ?>
                                <?php endif; ?>
                           </td>
                           <td><?php echo e(replace_number($value->product_count)); ?></td>
                       </tr>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/stockroom/show_input.blade.php ENDPATH**/ ?>