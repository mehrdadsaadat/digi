@extends('layouts.shop')

@section('content')

    <div class="content">

        <div class="page_cover">

            <div class="page_cover_title">
                <h3>پاسخ پرسش‌های پرتکرار</h3>
            </div>

            <div class="search_box">
                <form action="{{ url('faq') }}">
                    <input type="text" class="form-control" name="q" placeholder="پرسش خود را جستجو کنید">
                    <button class="btn btn-primary"> جست و جو</button>
                </form>
            </div>

        </div>

        <div class="page faq_list" >

            <div  class="cat_info">
                @if(!empty($cat->icon))
                    <img src="{{ url('files/upload/'.$cat->icon) }}" >
                @endif
                <h5>{{ $cat->title }}</h5>
            </div>
            <div style="margin-top:30px">
                @foreach($question as $key=>$value)
                    <div class="common_question">
                        <div class="common_question_header">
                            <h5>{{ $value->title }}</h5>
                            <span class="fa fa-angle-down"></span>
                        </div>
                        <div class="small_answer">
                            {!! strip_tags($value->small_answer,'<ul><li><a><p><br>') !!}
                            @if(!empty($value->answer))
                                <div class="more_data">
                                    <a href="{{ url('faq/question/'.$value->id) }}" class="data_link">مشاهده توضیحات تکمیلی</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach

                @if(sizeof($question)==0)
                        <p>موردی یافت نشد </p>
                @endif
            </div>
        </div>
        <div class="page faq_list common_question_list" >

            <h5>پرتکرارترین پرسش‌ها</h5>
            <div style="margin-top:30px">
                @foreach($pin_question as $key=>$value)
                    <div class="common_question">
                        <div class="common_question_header">
                            <h5>{{ $value->title }}</h5>
                            <span class="fa fa-angle-down"></span>
                        </div>
                        <div class="small_answer">
                            {!! strip_tags($value->small_answer,'<ul><li><a><p><br>') !!}

                            @if(!empty($value->answer))
                                <div class="more_data">
                                    <a href="{{ url('faq/question/'.$value->id) }}" class="data_link">مشاهده توضیحات تکمیلی</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
