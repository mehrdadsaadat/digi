<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'مدیریت فروشندگان','url'=>url('admin/sellers')]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت فروشندگان

            <?php echo $__env->make('include.item_table',['count'=>$trash_seller_count,'route'=>'admin/sellers',
            'title'=>'فروشنده','remove_new_record'=>false,'other'=>[
                ['label'=>'دریافت خروجی پرداخت','url'=>url('admin/seller/pay/export'),'icon'=>'fa-file-excel-o'],
                ['label'=>'ثبت پرداخت ها','url'=>url('admin/seller/pay/import'),'icon'=>'fa-paypal'],
            ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                <?php if(isset($_GET['trashed']) && $_GET['trashed']==true): ?>
                    <input type="hidden" name="trashed" value="true">
                <?php endif; ?>
                <input type="text" name="brand_name" class="form-control" value="<?php echo e($req->get('brand_name','')); ?>" placeholder="نام فروشگاه"><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام فروشنده</th>
                        <th>نام فروشگاه</th>
                        <th>شماره موبایل</th>
                        <th>تعداد محصول</th>
                        <th>وضعیت</th>
                        <th>کمیسون دریافت شده</th>
                        <th>مبلغ قابل واریز</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $sellers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr>
                            <td>
                                <input type="checkbox" name="sellers_id[]" class="check_box" value="<?php echo e($value->id); ?>"/>
                            </td>
                            <td><?php echo e(replace_number($i)); ?></td>
                            <td><?php echo e($value->fname.' '.$value->lname); ?></td>
                            <td><?php echo e($value->brand_name); ?></td>
                            <td><?php echo e(replace_number($value->mobile)); ?></td>
                            <td><?php echo e(replace_number($value->product_count)); ?></td>
                            <td>
                                <?php if($value->account_status=='active'): ?>
                                   <div class="alert alert-seccuss">فعال</div>
                                <?php elseif($value->account_status=='Inactive'): ?> 
                                   <div class="alert alert-secondary">غیر فعال</div>
                                <?php elseif($value->account_status=='reject'): ?>   
                                    <div class="alert alert-danger">رد شده</div>
                                <?php else: ?>
                                    <div class="alert alert-warning">در انتظار تایید</div>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e(replace_number(number_format($value->total_commission))); ?> تومان
                            </td>
                            <td>
                                 <?php
                                     $price=$value->total_price - $value->total_commission - $value->paid_commission;
                                 ?>
                                 <?php echo e(replace_number(number_format($price))); ?> تومان
                            </td>
                            <td>
                                <a href="<?php echo e(url('admin/sellers/'.$value->id.'/messages')); ?>" style="color:black">
                                    <span data-toggle="tooltip" data-placement="bottom"  title='پیام ها' class="fa fa-comment-o"></span>
                                </a>
                                <?php if(!$value->trashed()): ?>
                                <a href="<?php echo e(url('admin/sellers/'.$value->id.'/edit')); ?>"><span class="fa fa-edit"></span></a>
                                <?php endif; ?>

                                <?php if($value->trashed()): ?>
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی فروشنده' onclick="restore_row('<?php echo e(url('admin/sellers/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از بازیابی این فروشنده مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                <?php endif; ?>

                                <a href="<?php echo e(url('admin/sellers/'.$value->id)); ?>">
                                    <span  data-toggle="tooltip" data-placement="bottom"  title='محصولات فروشنده'  class="fa fa-eye"></span>
                                </a>
                                <?php if(!$value->trashed()): ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف فروشنده' onclick="del_row('<?php echo e(url('admin/sellers/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این فروشنده مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                <?php else: ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف فروشنده برای همیشه' onclick="del_row('<?php echo e(url('admin/sellers/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این فروشنده مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if(sizeof($sellers)==0): ?>
                        <tr>
                            <td colspan="10">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </form>

            <?php echo e($sellers->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
   <script>
        $("#sidebarToggle").click();
   </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/seller/index.blade.php ENDPATH**/ ?>