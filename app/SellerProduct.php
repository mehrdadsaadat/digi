<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerProduct extends Model
{
    protected $fillable=['seller_id','product_id','cat_id','brand_id'];
    public function getProduct()
    {
        return $this->hasOne(Product::class,'id', 'product_id');
    }
    public static function getCat($seller_id)
    {
        $cat=self::where('seller_id', $seller_id)->with('cat')
        ->distinct()->get(['cat_id']);
        return $cat;
    }
    public function cat()
    {
        return $this->hasOne(Category::class,'id', 'cat_id');
    }

    public static function brand($seller_id)
    {
        $cat = self::where('seller_id', $seller_id)->with('getBrand')
            ->distinct()->get(['brand_id']);
        return $cat;
    }
    public function getBrand()
    {
        return $this->hasOne(Brand::class, 'id', 'brand_id');
    }
}
