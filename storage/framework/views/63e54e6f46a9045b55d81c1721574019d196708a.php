<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'پیشنهاد شگفت انگیز','url'=>url('admin/incredible-offers')]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت محصولات پیشنهاد شگفت انگیز
        </div>

        <div class="panel_content">
            <incredible-offers></incredible-offers>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(asset('css/js-persian-cal.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <script type="text/javascript" src="<?php echo e(asset('js/js-persian-cal.min.js')); ?>"></script>
    <script>
       const pcal1= new AMIB.persianCalendar('pcal1');
       const pcal2= new AMIB.persianCalendar('pcal2');
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/admin/incredible_offers.blade.php ENDPATH**/ ?>