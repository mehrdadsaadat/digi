<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
        ['title'=>'مدیریت انبار ها','url'=>url('admin/stockrooms')],
        ['title'=>'افزودن محصول به انبار','url'=>url('admin/stockroom/add/input')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
           افزودن محصول به انبار
        </div>

        <div class="panel_content">

            <stockroom-product-list :stockroom='<?php echo e($stockroom); ?>'></stockroom-product-list>
            
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/stockroom/add_input.blade.php ENDPATH**/ ?>