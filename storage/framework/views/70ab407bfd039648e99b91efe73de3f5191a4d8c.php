<div class="product_item_box">
    <div class="item_box remove_item_box_shadow">
        <span>پرسش و پاسخ</span>
        <a  class="add_link" id="add_question">
            <span>ارسال پاسخ</span>
            <span class="fa fa-plus"></span>
        </a>
    </div>
    <?php $jdf=new \App\Lib\Jdf(); ?>
    <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div class="question_div">
             <div class="question_info">
                 <span class="user_name">
                    <?php if(!empty($question->getUser->name)): ?>
                       <?php echo e($question->getUser->name); ?>

                     <?php else: ?>
                       <span>ناشناس</span>
                    <?php endif; ?>
                 </span>
                 <span class="date"><?php echo e($jdf->jdate('d F Y',$question->time)); ?></span>
             </div>

             <div class="comment_content"><?php echo strip_tags($question->question); ?></div>
         </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


    <?php if(sizeof($questions)==0): ?>
       <p class="center_message">تاکنون پرسشی برای این محصول ثبت نشده</p>
    <?php endif; ?>

   <?php if($question_count>2): ?>
    <div class="show_more_div">
        <a class="more_link" id="show_more_question">
            <span>مشاهده همه <?php echo e(replace_number($question_count)); ?> پرسش  کاربران</span>
            <span class="fa fa-angle-left"></span>
        </a>
    </div>
   <?php endif; ?>

   <?php
       $auth=Auth::check() ? 'ok' : 'no';
   ?>
   <mobile-theme-question-list :product_id="'<?php echo e($product->id); ?>'" :auth="'<?php echo e($auth); ?>'" ></mobile-theme-question-list>

</div><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/show_product_questions.blade.php ENDPATH**/ ?>