@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت صفحات اضافی','url'=>url('admin/pages')]]])
    <div class="panel">

        <div class="header">
            مدیریت صفحات اضافی

            @include('include.item_table',['count'=>$trash_page_count,'route'=>'admin/pages','title'=>'صفحه'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="string" class="form-control" value="{{ $req->get('string','') }}" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>عنوان صفحه</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="pages_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->title }}</td>
                            <td>
                                <a href="{{ url('page/'.$value->url) }}" target="_blank" style="color:#000">
                                    <span class="fa fa-file-o"  data-toggle="tooltip" data-placement="bottom"  title='نمایش صفحه'></span>
                                </a>


                                @if(!$value->trashed())
                                <a href="{{ url('admin/pages/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی صفحه' onclick="restore_row('{{ url('admin/pages/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این صفحه مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف صفحه' onclick="del_row('{{ url('admin/pages/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این صفحه مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف صفحه برای همیشه' onclick="del_row('{{ url('admin/pages/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این صفحه مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                               @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($pages)==0)
                        <tr>
                            <td colspan="4">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $pages->links() }}
        </div>
    </div>

@endsection
