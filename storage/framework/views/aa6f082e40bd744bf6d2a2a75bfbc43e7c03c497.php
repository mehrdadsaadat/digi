<?php $__env->startSection('content'); ?>
    <mobile-shopping-cart :cart_data="<?php echo e(json_encode($cart_data)); ?>"></mobile-shopping-cart>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.mobile', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/shop/cart.blade.php ENDPATH**/ ?>