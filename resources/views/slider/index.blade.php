@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت اسلایدر ها','url'=>url('admin/sliders')]]])
    <div class="panel">

        <div class="header">
            مدیریت اسلایدر ها

            @include('include.item_table',['count'=>$trash_slider_count,'route'=>'admin/sliders','title'=>'اسلایدر'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>عنوان</th>
                        <th>تصویر</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sliders as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="sliders_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>

                            <td>{{ $value->title }}</td>
                            <td>
                                <img src="{{ url('files/slider/'.$value->image_url) }}" class="slide_image">

                            </td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/sliders/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی اسلایدر' onclick="restore_row('{{ url('admin/sliders/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این اسلایدر مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف اسلایدر' onclick="del_row('{{ url('admin/sliders/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این اسلایدر مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف اسلایدر برای همیشه' onclick="del_row('{{ url('admin/sliders/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این اسلایدر مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($sliders)==0)
                        <tr>
                            <td colspan="5">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $sliders->links() }}
        </div>
    </div>

@endsection
