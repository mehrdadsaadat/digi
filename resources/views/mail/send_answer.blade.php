<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <style>
        .box{
            width: 95%;
            margin: auto;
            direction: rtl;
            text-align: right;
            font-size: 16px;
            font-family: Tahoma;
        }
        .answer_box{
            width:99%;
            border:1px dashed gray;
            padding: 15px;
            background-color:#f6f6f6;
        }
    </style>
</head>
<body>
    <div class="box">

        <h4>
            <span>{{ $question->getUserInfo->first_name.' '.$question->getUserInfo->last_name }} </span>
            <span>
                برای پرسش شما پاسخی ارسال شد
            </span>
        </h4>
        
        <p>پاسخ ارسالی</p>
        <div class="answer_box">
            {!! $answer->question !!}
        </div>

        <p>پرسش شما </p>
        <div>
            {!! $question->question !!}
        </div>
        <br>
       <a href="{{ url('product/dkp-'.$question->getProduct->id.'/'.$question->getProduct->product_url) }}">
            {{ url('product/dkp-'.$question->getProduct->id.'/'.$question->getProduct->product_url)}}
        </a>
    </div>
</body>
</html>