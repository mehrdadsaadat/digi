<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>فروشگاه تنیس شاپ</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('head')
    <link href="{{ asset('css/mobile.css?id=rutqhpr98qher') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/mobile.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
</head>
<body>

<div id="app">

    <div>
        <div class="header auth">
            <a href="{{ url('/') }}">
                <span>فروشگاه اینترنتی {{ env('SHOP_NAME','') }}</span>
            </a>
        </div>
    </div>

  
   <div class="container-fluid">
       @yield('content')
   </div>



    @include('mobile.footer')
</div>


@yield('footer')
</body>
</html>
