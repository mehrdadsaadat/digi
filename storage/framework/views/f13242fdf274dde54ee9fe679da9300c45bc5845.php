<div id="auth_box">
    <div class="auth_box_title">
        <span>ورود به سایت</span>
    </div>
    <div style="margin:<?php echo e($margin); ?>px">
        <form method="POST" action="<?php echo e(route('login')); ?>" id="login_form">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <div class="field_name">شماره موبایل</div>
                <label class="input_label user_name">
                    <input type="text" class="form-control"  name="mobile" id="mobile" value="<?php echo e(old('mobile')); ?>" placeholder="شماره موبایل خود را وارد نمایید">

                    <label id="mobile_error_message" class="feedback-hint"></label>
                </label>
            </div>

            <div class="form-group">
                <div class="field_name">کلمه عبور</div>
                <label class="input_label user_pass">
                    <input type="password" class="form-control <?php if($errors->has('password')): ?> validate_error_border <?php endif; ?>" name="password" id="password" placeholder="کلمه عبور خود را وارد نمایید">

                    <label id="password_error_message" class="feedback-hint"  <?php if($errors->has('password')): ?> style="display:block" <?php endif; ?>>
                        <?php if($errors->has('password')): ?>
                            <span><?php echo e($errors->first('password')); ?></span>
                        <?php endif; ?>
                    </label>
                </label>
            </div>

            <?php if($errors->has('mobile')): ?>
                <div class="alert alert-danger"><?php echo e($errors->first('mobile')); ?></div>
            <?php endif; ?>

            <a class="reset_password_link" href="<?php echo e(url('password/reset')); ?>">بازیابی کلمه عبور</a>

            <div class="send_btn login_btn" id="login_btn">
                <span class="line"></span>
                <span class="title">ورود به سایت</span>
            </div>



            <div class="form-group">
                <input class="form-check-input" checked="checked"  type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                <span class="check_box active" id="login_remember"></span>
                <span class="form-check-label">مرا به خاطر بسپار</span>
            </div>

        </form>
    </div>

    <div class="alert alert-warning">
        <span>کاربر جدید هستید ؟</span>
        <span>
                 <a class="data_link" href="<?php echo e(route('register')); ?>">ثبت نام در سایت</a>
        </span>
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\digikala\resources\views/auth/login_form.blade.php ENDPATH**/ ?>