<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت انبار ها','url'=>url('admin/stockrooms')],
         ['title'=>'افزودن انبار جدید','url'=>url('admin/stockrooms/create')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="panel">

        <div class="header">افزودن انبار جدید</div>

        <div class="panel_content">


            <?php echo Form::open(['url' => 'admin/stockrooms']); ?>


            <div class="form-group">

                <?php echo e(Form::label('name','نام انبار : ')); ?>

                <?php echo e(Form::text('name',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('name')): ?>
                    <span class="has_error"><?php echo e($errors->first('name')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group textarea_feild">

                <?php echo e(Form::label('address','آدرس انبار : ')); ?>

                <?php echo e(Form::textarea('address',null,['class'=>'form-control'])); ?>

            </div>

            <button class="btn btn-success">ثبت </button>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/stockroom/create.blade.php ENDPATH**/ ?>