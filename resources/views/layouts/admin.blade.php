<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>پنل مدیریت</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('head')
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body>


<div class="container-fluid">

    <div class="page_sidebar">

        <?php
        $question_count=isset($question_count) ? $question_count : 0;
        $comments_count=isset($comments_count) ? $comments_count : 0;
        $sideBarMenu=array();
        $sideBarMenu[-1]=
            [
                'label'=>'مشاهده فروشگاه',
                'icon'=>'fa  fa-globe',
                'access'=>'files',
                'url'=>url('/')
        ];
        $sideBarMenu[0]=[
           'label'=>'محصولات',
           'icon'=>'fa fa-shopping-cart',
           'access'=>'products|category',
            'child'=>[
                ['url'=>url('admin/products'),'label'=>'مدیریت محصولات','access'=>'products'],
                ['url'=>url('admin/products/create'),'label'=>'افزودن محصول','access'=>'products','accessValue'=>'product_edit'],
                ['url'=>url('admin/category'),'label'=>'مدیریت دسته ها','access'=>'category']
           ]
        ];

        $sideBarMenu[1]=[
            'label'=>'اسلایدر ها',
            'icon'=>'fa fa-sliders',
            'access'=>'sliders',
            'child'=>[
                ['url'=>url('admin/sliders'),'label'=>'مدیریت اسلایدر ها','access'=>'sliders'],
                ['url'=>url('admin/sliders/create'),'label'=>'افزودن اسلایدر','access'=>'sliders'],
            ]
        ];

        $sideBarMenu[2]=[
            'label'=>'مناطق',
            'icon'=>'fa fa-location-arrow',
            'access'=>'location',
            'child'=>[
                ['url'=>url('admin/province'),'label'=>'مدیریت استان ها','access'=>'location'],
                ['url'=>url('admin/city'),'label'=>'مدیریت شهر ها','access'=>'location'],
            ]
        ];

        $sideBarMenu[3]=[
            'label'=>'سفارشات',
            'icon'=>'fa fa-list',
            'access'=>'orders',
            'child'=>[
                ['url'=>url('admin/orders'),'label'=>'مدیریت سفارشات','access'=>'orders','accessValue'=>'order_list'],
                ['url'=>url('admin/orders/submission'),'label'=>'مدیریت مرسوله ها','access'=>'orders','accessValue'=>'submissions'],
                ['url'=>url('admin/orders/submission/approved'),'label'=>'مرسوله های تایید شده','access'=>'orders','accessValue'=>'submission_approved'],
                ['url'=>url('admin/orders/submission/items/today'),'label'=>'مرسوله های ارسالی امروز','access'=>'orders','accessValue'=>'items_today'],
                ['url'=>url('admin/orders/submission/ready'),'label'=>'مرسوله های آماده ارسال','access'=>'orders','accessValue'=>'submissions_ready'],
                ['url'=>url('admin/orders/posting/send'),'label'=>'مرسوله های ارسال شده به پست','access'=>'orders','accessValue'=>'postings_sent'],
                ['url'=>url('admin/orders/posting/receive'),'label'=>'مرسوله آماده دریافت از پست','access'=>'orders','accessValue'=>'postings_receive'],
                ['url'=>url('admin/orders/delivered/shipping'),'label'=>'مرسوله های تحویل داده شده','access'=>'orders','accessValue'=>'delivered_shipping'],

            ]
        ];
        $sideBarMenu[4]=[
            'label'=>'تخفیف ها',
            'icon'=>'fa fa-percent',
            'access'=>'discount',
            'child'=>[
                ['url'=>url('admin/discount'),'label'=>'مدیریت کد های تخفیف','access'=>'discount'],
                ['url'=>url('admin/discount/create'),'label'=>'افزودن کد تخفیف','access'=>'discount'],
            ]
        ];
        $sideBarMenu[5]=[
            'label'=>'گزارشات',
            'icon'=>'fa fa-line-chart',
            'access'=>'report',
            'child'=>[
                ['url'=>url('admin/report/sale'),'label'=>'آمار فروش','access'=>'report','accessValue'=>'report_sale'],
                ['url'=>url('admin/report/view'),'label'=>'آمار بازید','access'=>'report','accessValue'=>'report_view'],
                ['url'=>url('admin/commissions'),'label'=>'کمیسیون ها','access'=>'report','accessValue'=>'commission'],
                ['url'=>url('admin/payments'),'label'=>'پرداخت ها','access'=>'report','accessValue'=>'payment'],
            ]
        ];
        $sideBarMenu[6]=[
            'label'=>'پیام ها',
            'icon'=>'fa fa-envelope',
            'access'=>'seller',
            'url'=>url('admin/messages')
        ];
        $sideBarMenu[7]=
        [
            'label'=>'مدیریت فایل ها',
            'icon'=>'fa  fa-file',
            'access'=>'files',
            'url'=>url('admin/filemanager')
        ];
        $sideBarMenu[8]=
        [
           'label'=>'کاربران',
           'icon'=>'fa fa-users',
           'access'=>'users',
           'child'=>[
               ['url'=>url('admin/users'),'label'=>'مدیریت کاربران','access'=>'users'],
               ['url'=>url('admin/userRole'),'label'=>'نقش های کاربری','access'=>'users','accessValue'=>'user_access'],
           ]
        ];
        $sideBarMenu[9]=
        [
            'label'=>'نظرات کاربران',
            'icon'=>'fa fa-comment-o',
            'access'=>'comments',
            'url'=>url('admin/comments'),
            'count'=>$comments_count
        ];
        $sideBarMenu[10]=
        [
            'label'=>'پرسش های کاربران',
            'icon'=>'fa fa-question',
            'access'=>'questions',
            'url'=>url('admin/questions'),
            'count'=>$question_count
        ];
        $sideBarMenu[11]=[
            'label'=>'فروشندگان',
            'icon'=>'fa fa-shopping-basket',
            'access'=>'seller',
            'url'=>url('admin/sellers')
        ];
        $sideBarMenu[12]=[
            'label'=>'انبار',
            'icon'=>'fa fa-ambulance',
            'access'=>'stockrooms',
            'child'=>[
                ['url'=>url('admin/stockrooms'),'label'=>'مدیریت انبار ها','access'=>'stockrooms','accessValue'=>'stockroom_edit'],
                ['url'=>url('admin/stockroom/input'),'label'=>'ورودی های انبار','access'=>'stockrooms','accessValue'=>'add_input'],
                ['url'=>url('admin/stockroom/output'),'label'=>'خروجی های انبار','access'=>'stockrooms','accessValue'=>'add_output'],
                ['url'=>url('admin/packages'),'label'=>'مدیریت محموله ها','access'=>'stockrooms','accessValue'=>'package'],
            ]
        ];
        $sideBarMenu[13]=
        [
            'label'=>'پیشنهاد شگفت انگیز',
            'icon'=>'fa fa-gift',
            'access'=>'incredible-offers',
            'url'=>url('admin/incredible-offers')
        ];
        $sideBarMenu[19]=[
            'label'=>'پرسش های متداول',
            'icon'=>'fa fa-question',
            'access'=>'pages',
            'child'=>[
                ['url'=>url('admin/category-common-question'),'label'=>'مدیریت دسته ها','access'=>'pages'],
                ['url'=>url('admin/common-question'),'label'=>'مدیریت پرسش ها','access'=>'pages'],
            ]
        ];

        $sideBarMenu[20]=[
            'label'=>'صفحات اضافی',
            'icon'=>'fa fa-file-o',
            'access'=>'pages',
            'child'=>[
                ['url'=>url('admin/pages'),'label'=>'مدیریت صفحات','access'=>'pages'],
                ['url'=>url('admin/pages/create'),'label'=>'افزودن صفحه جدید','access'=>'pages'],
            ]
        ];
        $sideBarMenu[30]=[
            'label'=>'تنظیمات',
            'icon'=>'fa fa-cogs',
            'access'=>'setting',
            'child'=>[
                ['url'=>url('admin/setting/shop'),'label'=>'فروشگاه','access'=>'setting'],
                ['url'=>url('admin/setting/send-order-price'),'label'=>'هزینه ارسال سفارشات','access'=>'setting'],
                ['url'=>url('admin/setting/payment-gateway'),'label'=>'درگاه پرداخت','access'=>'setting'],
            ]
        ];
        ?>
        <span class="fa fa-bars" id="sidebarToggle"></span>
        @php
            $access2=isset($access) ? $access : null;
            $access=isset($access) ? json_decode($access) : null;
        @endphp
        <ul id="sidebar_menu">

            @foreach($sideBarMenu as $key=>$value)

                <?php $child=array_key_exists('child',$value) ?>
                @if(checkParentMenuAccess($access,$value['access']))
                <li>
                    <a @if(array_key_exists('url',$value)) href="{{ $value['url'] }}" @endif>
                        <span class="{{ $value['icon'] }}"></span>
                        <span class="sidebar_menu_text">{{ $value['label'] }}</span>
                        @if(array_key_exists('count',$value))
                            @if($value['count']!=0)
                                <span class="count">{{ replace_number($value['count']) }}</span>
                            @endif
                        @endif
                        @if($child)  <span class="fa fa-angle-left"></span> @endif
                    </a>
                    @if($child)
                        <div class="child_menu">
                            @foreach($value['child'] as $key2=>$value2)
                                @if (checkAddChildMuenAccess($access,$value2))
                                  <a href="{{ $value2['url'] }}">
                                     {{ $value2['label'] }}
                                  </a>
                                @endif
                             @endforeach
                        </div>
                    @endif
                </li>
                @endif
            @endforeach

        </ul>

    </div>
    <div class="page_content">

        <div class="page_header">
            <div>پنل مدیریت</div>
            <div>
                <form method="post" action="{{ url('/logout') }}" id="logout_form">@csrf</form>
                <ul style="direction: ltr">
                    <li>
                        <a onclick="logout()">
                            <span class="fa fa-sign-out"></span>
                        </a>
                    </li>
                    @if(has_access_author_admin($access2,'orders','order_list') || Auth::user()->role=='admin')
                        <li>
                            <a href="{{ url('admin/orders') }}" target="_blank">
                                @if(isset($new_order_count) && $new_order_count!=0)
                                    <span class="count_header_span" data-count="{{ replace_number($new_order_count) }}"></span>
                                @endif
                                <span class="fa fa-shopping-cart"></span>
                            </a>
                        </li>
                    @endif
                    @if(has_access_author_admin($access2,'products','product_edit') || Auth::user()->role=='admin')
                    <li>
                        <a href="{{ url('admin/products') }}" target="_blank">
                            @if(isset($product_awaiting_review) && $product_awaiting_review!=0)
                                <span class="count_header_span" data-count="{{ replace_number($product_awaiting_review) }}"></span>
                            @endif
                            <span class="fa fa-bell-o"></span>
                        </a>
                    </li>
                    @endif
                    @if(has_access_author_admin($access2,'messages') || Auth::user()->role=='admin')
                    <li>
                        <a href="{{ url('admin/messages') }}" target="_blank">
                            @if(isset($message_count) && $message_count!=0)
                                <span class="count_header_span" data-count="{{ replace_number($message_count) }}"></span>
                            @endif
                            <span class="fa fa-envelope-o"></span>
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
        <div class="content_box" id="app">
            @yield('content')
        </div>
    </div>


</div>

<div class="message_div">
    <div class="message_box">
        <p id="msg"></p>
        <a class="alert alert-success" onclick="delete_row()">بله</a>
        <a class="alert alert-danger" onclick="hide_box()">خیر</a>
    </div>

</div>

<div id="loading_box">
    <div class="loading_div">
        <div class="loading"></div>
        <span>در حال ارسال اطلاعات</span>
    </div>
</div>




<div class="server_error_box" id="server_error_box">
    <div>
        <span class="fa fa-warning"></span>
        <span id="message">خطا در ارسال درخواست - مجددا تلاش نمایید</span>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/AdminVue.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
@yield('footer')
@if(isset($route_params))
    <script>
        setActiveAdminMenu('<?= $route_params ?>');
    </script>
@endif
</body>
</html>
