<?php if($product_item_count>0): ?>
    <?php if(!isset($remove_title)): ?>
    <h6 style="margin-right:-15px">ویژگی های محصول</h6>
    <?php endif; ?>
    <ul class="important_item_ul">

        <?php $i=0; ?>
        <?php $__currentLoopData = $product_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $value->getChild; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2=>$value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($value2->show_item==1): ?>
                    <li <?php if($i>3): ?> class="more_important_item" <?php endif; ?>>
                        <span><?php echo e($value2->title); ?> : </span>
                        <span>
                        <?php $__currentLoopData = $value2->getValue; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(strlen($itemValue->item_value)>30): ?>

                                    <br>
                                <?php endif; ?>
                                <?php echo e($itemValue->item_value); ?><br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </span>
                    </li>
                    <?php $i++; ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
    <?php if(!isset($remove_title)): ?>
    <?php if($i>3): ?>
        <p class="show_more_important_item">موارد بیشتر</p>
    <?php endif; ?>
    <?php endif; ?>

<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\digikala\resources\views/include/show_important_item.blade.php ENDPATH**/ ?>