@extends('layouts.mobile')

@section('content')


    <div>
        <div class="profile_item_header order_content_header">
            <div>
                <span>علاقه مندی ها</span>
            </div>
            <a href="{{ url('user/profile') }}">
                <span>بازگشت</span>
                <span class="fa fa-angle-left"></span>
            </a>
        </div>

        <div class="order_content favorite_list">
            <mobile-favorite-list></mobile-favorite-list>
        </div>
    </div>
@endsection
