<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;
use App\Question;
use Mail;
use App\Mail\SendAnswer;
use App\Favorite;
class UserController extends Controller
{
   public function addAddress(Request $request)
   {
       return Address::addUserAddress($request);
   }
   public function removeAddress($id,Request $request)
   {
       $user_id=$request->user()->id;
       $delete=Address::where(['user_id'=>$user_id,'id'=>$id])->delete();
       if($delete)
       {
           $AddressList=Address::with(['getCity','getProvince'])->where(['user_id'=>$user_id])->orderBy('id','DESC');
           if($request->get('paginate')=='ok'){
               $AddressList=$AddressList->paginate(10);
           }
           else{
               $AddressList=$AddressList->get();
           }
           return $AddressList;
       }
       else{
           return 'error';
       }
   }
   public function getAddreass(Request $request){
       $user_id=$request->user()->id;
       $userAddress=Address::with(['getProvince','getcity'])->where('user_id',$user_id)->orderBy('id','DESC')->paginate(10);
       return $userAddress;
   }
   public function addQestion(Request $request){
       $send_email=$request->get('send_email')=="true" ? 'ok' : 'no';
       $user_id=$request->user()->id;
       $Question=new Question($request->all());
       $Question->time=time();
       $Question->user_id=$user_id;
       $Question->send_email=$send_email;
       $Question->save();
       return 'ok';
   }
   public function add_favorite(Request $request)
   {
       if($request->ajax())
       {
           $product_id=$request->get('product_id');
           $user_id=$request->user()->id;
           $favorite=Favorite::where(['product_id'=>$product_id,'user_id'=>$user_id])->first();
           if($favorite){
               $favorite->delete();
               return 'ok';
           }
           else{
               $favorite=new Favorite($request->all());
               $favorite->user_id=$user_id;
               $favorite->saveOrFail();
               return 'ok';
           }
       }
       else{
           return redirect('/');
       }
   }
   public function getFavoriteList(Request $request)
   {
      if($request->header('X-Xsrf-Token',NULL)){
        $user_id=$request->user()->id;
        return Favorite::with('getProduct')->where('user_id',$user_id)->orderBy('id','DeSC')->paginate(5); 
       }
   }
   public function removeProductOfList(Request $request)
   {
       if($request->header('X-Xsrf-Token',NULL)){
           $user_id=$request->user()->id;
           $product_id=$request->get('product_id');
           Favorite::where(['product_id'=>$product_id,'user_id'=>$user_id])->delete();
           if($request->get('showMobile')=='ok'){
               return 'ok';
           }
           else{
              return Favorite::with('getProduct')->where('user_id',$user_id)->orderBy('id','DeSC')->paginate(5); 
           }
       }
   }
}
