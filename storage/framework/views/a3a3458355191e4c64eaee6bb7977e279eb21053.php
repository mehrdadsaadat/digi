<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>فروشگاه تنیس شاپ</title>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <?php echo $__env->yieldContent('head'); ?>
    <link href="<?php echo e(asset('css/shop.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/main.css')); ?>" rel="stylesheet">
    <script src="<?php echo e(asset('js/app.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/js.js')); ?>" type="text/javascript"></script>
</head>
<body>

<div id="app">

    <?php echo $__env->yieldContent('content'); ?>

</div>


<div id="loading_box">
    <div class="loading_div">
        <img src="<?php echo e(asset('files/images/shop_icon.jpg')); ?>">
        <div class="spinner">
            <div class="b1"></div>
            <div class="b2"></div>
            <div class="b3"></div>
        </div>
    </div>
</div>

<script src="<?php echo e(asset('js/ShopVue.js')); ?>" type="text/javascript"></script>
<?php echo $__env->yieldContent('footer'); ?>
</body>
</html>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/layouts/order.blade.php ENDPATH**/ ?>