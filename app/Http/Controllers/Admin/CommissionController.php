<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\brand;
use App\Commission;
class CommissionController extends CustomController
{
    protected  $model='Commission';
    protected  $title='کمیسیون';
    protected $route_params='commissions';
    public function index(Request $request){
        $commissions=Commission::getData($request->all());
        $trash_commission_count=Commission::onlyTrashed()->count();
        $category=Category::get_parent2();
        $brand=[''=>'انتخاب برند']+Brand::pluck('brand_name','id')->toArray();
        return view('commission.index',[
            'commissions'=>$commissions,
            'trash_commission_count'=>$trash_commission_count,
            'req'=>$request,
            'category'=>$category,
            'brand'=>$brand,
            ]);
    }
    public function create()
    {
        $category=Category::get_parent2();
        $brand=[''=>'انتخاب برند']+Brand::pluck('brand_name','id')->toArray();
        return view('commission.create',['category'=>$category,'brand'=>$brand]);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'cat_id'=>'required',
            'brand_id'=>'required',
            'percentage'=>'required|numeric'
        ],[],['cat_id'=>'دسته','brand_id'=>'برند','percentage'=>'درصد کمیسیون']);
        $cat_id=$request->get('cat_id');
        $brand_id=$request->get('brand_id');
        $row=Commission::where(['cat_id'=>$cat_id,'brand_id'=>$brand_id])->first();
        if($row)
        {
            return redirect()->back()->withInput()->with('warring','برای دسته و برند انتخاب شده قبلا درصد کمیسیون ثبت شده');
        }
        else{
           $commission=new Commission($request->all());   
           $commission->saveOrFail();
           return redirect('admin/commissions')->with('message','ثبت کمیسیون با موفقیت انجام شد');
        }
    }
    public function edit($id)
    {
        $commission=Commission::findOrFail($id);
        $category=Category::get_parent2();
        $brand=[''=>'انتخاب برند']+Brand::pluck('brand_name','id')->toArray();
        return view('commission.edit',['category'=>$category,'brand'=>$brand,'commission'=>$commission]);
    }
    public function update($id,Request $request)
    {
        $commission=Commission::findOrFail($id);
        $this->validate($request,[
            'cat_id'=>'required',
            'brand_id'=>'required',
            'percentage'=>'required|numeric'
        ],[],['cat_id'=>'دسته','brand_id'=>'برند','percentage'=>'درصد کمیسیون']);
        $cat_id=$request->get('cat_id');
        $brand_id=$request->get('brand_id');
        $row=Commission::where(['cat_id'=>$cat_id,'brand_id'=>$brand_id])->first();
        if($row && $row->id!=$id)
        {
            return redirect()->back()->withInput()->with('warring','برای دسته و برند انتخاب شده قبلا درصد کمیسیون ثبت شده');
        }
        else{
            $commission->update($request->all());
            return redirect('admin/commissions')->with('message','ویرایش کمیسیون با موفقیت انجام شد');
        }
    }
}
