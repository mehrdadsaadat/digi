@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت محصولات','url'=>url('admin/products')],
         ['title'=>'ویرایش محصول','url'=>url('admin/products/'.$product->id.'/edit')]
    ]])

    <?php
    use App\Product;$status=Product::ProductStatus();
    $jdf=new App\Lib\Jdf();
    ?>
    <div class="panel">

        <div class="header">
            ویرایش محصول - {{ $product->title }}
        </div>

        <div class="panel_content">
            {!! Form::model($product,['url' => 'admin/products/'.$product->id,'files'=>true]) !!}

            {{ method_field('PUT') }}
            <div class="form-group">

                {{ Form::label('title','عنوان محصول : ') }}
                {{ Form::text('title',null,['class'=>'form-control total_width_input']) }}
                @if($errors->has('title'))
                    <span class="has_error">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::textarea('tozihat',null,['class'=>'form-control ckeditor']) }}
            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        {{ Form::label('ename','نام لاتین محصول : ') }}
                        {{ Form::text('ename',null,['class'=>'form-control left']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('cat_id','انتخاب دسته : ') }}
                        {{ Form::select('cat_id',$catList,null,['class'=>'selectpicker','data-live-search'=>'true']) }}
                        @if($errors->has('cat_id'))
                            <span class="has_error">{{ $errors->first('cat_id') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('brand_id','انتخاب برند : ') }}
                        {{ Form::select('brand_id',$brand,null,['class'=>'selectpicker','data-live-search'=>'true']) }}
                        @if($errors->has('brand_id'))
                            <span class="has_error">{{ $errors->first('brand_id') }}</span>
                        @endif
                    </div>



                    <div class="form-group">
                        {{ Form::label('status','وضعیت محصول : ') }}
                        {{ Form::select('status',$status,null,['class'=>'selectpicker','data-live-search'=>'true']) }}

                    </div>
                </div>
                <div class="col-md-6">

                    <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                    <div class="choice_pic_box" onclick="select_file()">

                        <span class="title">انتخاب تصویر محصول</span>
                        <img src="{{ url('files/products/'.$product->image_url) }}" id="output" class="pic_tag">
                    </div>
                    @if($errors->has('pic'))
                        <span class="has_error">{{ $errors->first('pic') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <textarea name="reject_message" class="form-control" @if($product->status==-3) style="display:block" @endif id="reject_message" placeholder="دلیل شما برای رد کردن محصول"> </textarea>
            </div>

            <p class="message_text">برچسب ها با استفاده از (،) از هم جدا شود</p>
            <div class="form-group">
                <input type="text" name="tag_list"  id="tag_list" class="form-control" placeholder="برجسب های محصول">
                <div class="btn btn-success" onclick="add_tag()">افزودن</div>
                <input type="hidden" value="{{ $product->keywords }}" name="keywords" id="keywords">
            </div>


            <div id="tag_box">
                @php
                $keywords=$product->keywords;
                $e=explode(',',$keywords);
                $i=1;
                @endphp
                @if(is_array($e))
                    @foreach($e as $key=>$value)
                      @if(!empty($value))

                            <div class="tag_div" id="tag_div_{{ $i }}">
                                <span  class="fa fa-remove" onclick="remove_tag({{ $i }},'{{ $value }}')"></span>
                                {{ $value }}
                           </div>
                            @php $i++;  @endphp
                      @endif

                    @endforeach
                @endif
            </div>
            <div style="clear:both"></div>

            <div class="form-group">

                {{ Form::label('description','توضیحات مختصر در مورد محصول (حداکثر 150 کاراکتر) : ',['style'=>'color:red;width:100%']) }}
                {{ Form::textarea('description',null,['class'=>'form-control','id'=>'description']) }}
                @if($errors->has('description'))
                    <span class="has_error">{{ $errors->first('description') }}</span>
                @endif
            </div>

            <div class="form-group">

            <div class="form-group">
                <label>
                    <input type="checkbox" @if($product->use_for_gift_cart=='yes') checked="checked" @endif name="use_for_gift_cart" id="use_for_gift_cart">
                    استفاده به عنوان کارت هدیه
                </label>
            </div>

            <div class="form-group">
                <label>
                   {{ Form::checkbox('fake') }}
                   کالای غیر اصل
                </label>
            </div>

            <button class="btn btn-primary">ویرایش محصول</button>

             @foreach ($reject_message as $message)
             <div class="alert alert-danger reject_message">
                <div>
                    <span>ثبت شده توسط : </span>
                    @if($message->getUser)
                      <a target="_blank" href="{{ url('admin/users/'.$message->getUser->id) }}">{{ $message->getUser->name }}</a>
                    @endif
                    <span> در تاریخ </span>
                    <span>{{ $jdf->jdate('d F Y',$message->time) }}</span>
                </div>
                <div style="font-size:14px;padding-top:20px;padding-bottom:10px">
                    {{ $message->tozihat}}
                </div>
            </div>
             @endforeach

            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('footer')
<script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    $("#status").change(function(){
        const value=$(this).val();
        if(value==-3)
        {
            $("#reject_message").show();
        }
        else
        {
            $("#reject_message").hide();
        }
    });
</script>
@endsection
