@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت رنگ ها','url'=>url('admin/colors')]]])
    <div class="panel">

        <div class="header">
            مدیریت رنگ ها

            @include('include.item_table',['count'=>$trash_color_count,'route'=>'admin/colors','title'=>'رنگ'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>

            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="string" class="form-control" value="{{ $req->get('string','') }}" placeholder="کلمه مورد نظر ..."><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام رنگ</th>
                        <th>کد رنگ</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($color as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="colors_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>{{ $value->name }}</td>
                            <td><span class="color" style="background:#{{ $value->code }};@if($value->name=='سفید') color:#000 @endif" >{{ $value->code }}</span></td>
                            <td>
                                @if(!$value->trashed())
                                <a href="{{ url('admin/colors/'.$value->id.'/edit') }}"><span class="fa fa-edit"></span></a>
                                @endif

                                @if($value->trashed())
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی رنگ' onclick="restore_row('{{ url('admin/colors/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این رنگ مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف رنگ' onclick="del_row('{{ url('admin/colors/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این رنگ مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف رنگ برای همیشه' onclick="del_row('{{ url('admin/colors/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این رنگ مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 @endif
                            </td>
                        </tr>

                    @endforeach

                    @if(sizeof($color)==0)
                        <tr>
                            <td colspan="5">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </form>

            {{ $color->links() }}
        </div>
    </div>

@endsection
