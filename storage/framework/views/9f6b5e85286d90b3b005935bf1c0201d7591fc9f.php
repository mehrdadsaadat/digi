<div class="product_item_box">
    <div class="item_box remove_item_box_shadow">
        <span>مفید ترین نظرات</span>
        <a  class="add_link" href="<?php echo e(url('product/comment/'.$product->id)); ?>">
            <span>افزودن نظر جدید</span>
            <span class="fa fa-plus"></span>
        </a>
    </div>

    <?php $jdf=new \App\Lib\Jdf(); ?>
    <?php $__currentLoopData = $useful_comment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="comment_div">
            <span class="user_name">
                <?php if($comment->getUserInfo): ?>
                    <?php echo e($comment->getUserInfo->first_name.' '.$comment->getUserInfo->last_name); ?>

                <?php else: ?>
                    <span>ناشناس</span>
                <?php endif; ?>
            </span>
            <span class="date"><?php echo e($jdf->jdate('d F Y',$comment->time)); ?></span>
            <div class="comment_content"><?php echo e($comment->content); ?></div>
            <?php $advantages=$comment->advantage ?>
            <?php if(sizeof($advantages)>1): ?>
                <span class="evaluation_label">نقاط قوت</span>
                <ul class="evaluation_ul advantage">
                    <?php $__currentLoopData = $advantages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $advantage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(!empty($advantage)): ?>
                            <li><span><?php echo e($advantage); ?></span></li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            <?php endif; ?>
            <?php $disadvantages=$comment->disadvantage ?>
            <?php if(sizeof($disadvantages)>1): ?>
                <span class="evaluation_label">نقاط ضعف</span>
                <ul class="evaluation_ul disadvantage">
                    <?php $__currentLoopData = $disadvantages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $disadvantage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(!empty($disadvantage)): ?>
                            <li><span><?php echo e($disadvantage); ?></span></li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            <?php endif; ?>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


    <?php if(sizeof($useful_comment)==0): ?>
        <p class="center_message">تاکنون نظری برای این محصول ثبت نشده</p>
    <?php endif; ?>

    <?php if($comment_count>2): ?>
        <div class="show_more_div">
            <a class="more_link" id="show_more_comment">
                <span>مشاهده همه <?php echo e(replace_number($comment_count)); ?> نظر  کاربران</span>
                <span class="fa fa-angle-left"></span>
            </a>
        </div>
        <mobile-theme-comment-list :product_id="'<?php echo e($product->id); ?>'" :product_title="'<?php echo e($product->title); ?>'"></mobile-theme-comment-list>
    <?php endif; ?>
</div>

<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/show_product_comments.blade.php ENDPATH**/ ?>