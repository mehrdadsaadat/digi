<?php $__env->startSection('content'); ?>

    <?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
    <div class="order_list">
        <span class="profile_menu_title">سفارشات من</span>
        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="profile_item">
                <div class="profile_item_header">
                    <div>
                        <?php echo e(replace_number($value->order_id )); ?> |
                        <?php if($value['pay_status']=='awaiting_payemnt'): ?>
                            در انتظار پرداخت
                        <?php elseif($value['pay_status']=='ok'): ?>
                            پرداخت شده
                        <?php elseif($value['pay_status']=='canceled'): ?>
                            لغو شده
                        <?php else: ?>
                            خطا در اتصال به درگاه
                        <?php endif; ?>
                    </div>
                     <a href="<?php echo e(url('user/profile/orders/'.$value->id)); ?>">
                            <span class="fa fa-angle-left"></span>
                     </a>
                </div>
                <div class="profile_info_row">
                    <span>تاریخ ثبت سفارش</span>
                    <span><?php echo e($jdf->jdate('j F Y',$value->created_at)); ?></span>
                </div>
                <div class="profile_info_row">
                    <span>مبلغ قابل پرداخت</span>
                    <span><?php echo e(replace_number(number_format($value->price))); ?> تومان</span>
                </div>
                <div class="profile_info_row">
                    <span>مبلغ کلی</span>
                    <span><?php echo e(replace_number(number_format($value->total_price))); ?> تومان </span>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php echo e($orders->links()); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.mobile', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/mobile/userPanel/orders.blade.php ENDPATH**/ ?>