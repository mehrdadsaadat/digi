<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>فروشگاه تنیس شاپ</title>
    <?php echo $__env->yieldContent('seo'); ?>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <?php echo $__env->yieldContent('head'); ?>
    <link href="<?php echo e(asset('css/mobile.css?id=aghuguahgrtet')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/main.css?id=aghuguahgrtet')); ?>" rel="stylesheet">
    <script src="<?php echo e(asset('js/app.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/mobile.js?id=ewguaegru')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/main.js')); ?>" type="text/javascript"></script>
</head>
<body>

<div id="app">

    <?php echo $__env->make('mobile.CatList', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div>
        <div class="header">
            <span class="fa fa-align-justify"></span>
            <a href="<?php echo e(url('/')); ?>">
                <span>فروشگاه اینترنتی <?php echo e(env('SHOP_NAME','')); ?></span>
            </a>
            <span></span>
        </div>
    </div>

   <div class="navbar">

       <mobile-header-search></mobile-header-search>

       <div>
           <a href="<?php echo e(url('Cart')); ?>" style="position: relative">
               <?php if(\App\Cart::get_product_count()>0): ?>
                   <span class="cart_product_count"><?php echo e(replace_number(\App\Cart::get_product_count())); ?></span>
                <?php endif; ?>
               <span class="fa fa-shopping-basket"></span>
           </a>

           <?php if(Auth::check()): ?>
               <a href="<?php echo e(url('user/profile')); ?>"><span class="fa fa-user-o"></span></a>
           <?php else: ?>
               <a href="<?php echo e(url('login')); ?>"><span class="fa fa-user-o"></span></a>
           <?php endif; ?>
       </div>
   </div>
   <div class="container-fluid">
       <?php echo $__env->yieldContent('content'); ?>
   </div>

    <div id="loading_box">
        <div class="loading_div">
            <img src="<?php echo e(asset('files/images/shop_icon.jpg')); ?>">
            <div class="spinner">
                <div class="b1"></div>
                <div class="b2"></div>
                <div class="b3"></div>
            </div>
        </div>
    </div>


    <?php echo $__env->make('mobile.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>


<script src="<?php echo e(asset('js/ShopVue.js')); ?>" type="text/javascript"></script>
<?php echo $__env->yieldContent('footer'); ?>
</body>
</html>
<script>
    import MobileHeaderSearch from "../../js/components/MobileHeaderSearch";
    export default {
        components: {MobileHeaderSearch}
    }
</script>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/layouts/mobile.blade.php ENDPATH**/ ?>