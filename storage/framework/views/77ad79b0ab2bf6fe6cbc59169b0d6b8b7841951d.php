<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت دسته ها','url'=>url('admin/category')],
         ['title'=>'افزودن دسته جدید','url'=>url('admin/category/create')]
    ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="panel">

        <div class="header">افزودن دسته جدید</div>

        <div class="panel_content">


            <?php echo Form::open(['url' => 'admin/category','files'=>true]); ?>


            <div class="form-group">

                <?php echo e(Form::label('name','نام دسته : ')); ?>

                <?php echo e(Form::text('name',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('name')): ?>
                    <span class="has_error"><?php echo e($errors->first('name')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('ename','نام انگلیسی دسته : ')); ?>

                <?php echo e(Form::text('ename',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('ename')): ?>
                    <span class="has_error"><?php echo e($errors->first('ename')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">

                <?php echo e(Form::label('search_url','url دسته : ')); ?>

                <?php echo e(Form::text('search_url',null,['class'=>'form-control'])); ?>

                <?php if($errors->has('search_url')): ?>
                    <span class="has_error"><?php echo e($errors->first('search_url')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <?php echo e(Form::label('parent_id','انتخاب سردسته : ')); ?>

                <?php echo e(Form::select('parent_id',$parent_cat,null,['class'=>'selectpicker auto_width','data-live-search'=>'true'])); ?>

            </div>

            <div class="form-group">
                <input type="file" name="pic" id="pic" onchange="loadFile(event)" style="display:none">
                <?php echo e(Form::label('pic','انتخاب تصویر  : ')); ?>

                <img src="<?php echo e(url('files/images/pic_1.jpg')); ?>" onclick="select_file()" width="150" id="output">
                <?php if($errors->has('pic')): ?>
                    <span class="has_erro¡r"><?php echo e($errors->first('pic')); ?></span>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <?php echo e(Form::label('notShow','عدم نمایش در لیست اصلی : ')); ?>

                <?php echo e(Form::checkbox('notShow',false)); ?>

            </div>

            <button class="btn btn-success">ثبت دسته</button>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/category/create.blade.php ENDPATH**/ ?>