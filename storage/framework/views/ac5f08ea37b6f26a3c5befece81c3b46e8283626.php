<?php $__env->startSection('content'); ?>
<div id="auth_box">
    <div class="auth_box_title">
        <span>ورود به بخش مدیریت</span>
    </div>
    <div style="margin:30px">
        <form method="POST" action="<?php echo e(route('login')); ?>" id="admin_login_form">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <div class="field_name">نام کاربری</div>
                <label class="input_label username">
                    <input type="text" class="form-control"  name="username" id="username" value="<?php echo e(old('username')); ?>" placeholder="نام کاربری خود را وارد نمایید">

                    <label id="username_error_message" class="feedback-hint"></label>
                </label>
            </div>

            <div class="form-group">
                <div class="field_name">کلمه عبور</div>
                <label class="input_label user_pass">
                    <input type="password" class="form-control <?php if($errors->has('password')): ?> validate_error_border <?php endif; ?>" name="password" id="password" placeholder="کلمه عبور خود را وارد نمایید">

                    <label id="password_error_message" class="feedback-hint"  <?php if($errors->has('password')): ?> style="display:block" <?php endif; ?>>
                        <?php if($errors->has('password')): ?>
                            <span><?php echo e($errors->first('password')); ?></span>
                        <?php endif; ?>
                    </label>
                </label>
            </div>

            <?php if($errors->has('username')): ?>
                <div class="alert alert-danger"><?php echo e($errors->first('username')); ?></div>
            <?php endif; ?>


            <div class="send_btn login_btn" id="admin_login_btn">
                <span class="line"></span>
                <span class="title">ورود به بخش مدیریت</span>
            </div>



            <div class="form-group">
                <input class="form-check-input" checked="checked"  type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                <span class="check_box active" id="login_remember"></span>
                <span class="form-check-label">مرا به خاطر بسپار</span>
            </div>

        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.auth", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/digikala/resources/views/admin/admin_login_form.blade.php ENDPATH**/ ?>