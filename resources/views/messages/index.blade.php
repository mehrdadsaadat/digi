@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[
         ['title'=>'مدیریت پیام ها','url'=>url('admin/messages')],
    ]])
    <div class="panel">

        <div class="header">
            مدیریت پیام ها
            @include('include.item_table',['count'=>$trash_message_count,'route'=>'admin/messages','title'=>'پیام','remove_new_record'=>true])

        </div>

        <div class="panel_content">
            <?php use App\Lib\Jdf;$jdf=new Jdf(); ?>
            @include('include.Alert')
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; ?>
            <form method="get" class="search_form">
                @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                    <input type="hidden" name="trashed" value="true">
                @endif
                <input type="text" name="title" class="form-control" value="{{ $req->get('title','') }}" placeholder="عنوان پیام"><button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>عنوان</th>
                        <th>ارسال کننده</th>
                        <th>دریافت کننده</th>
                        <th>زمان ارسال</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $key=>$value)
                        @php $i++; @endphp
                        <tr>
                            <td>
                                <input type="checkbox" name="messages_id[]" class="check_box" value="{{ $value->id }}"/>
                            </td>
                            <td>{{ replace_number($i) }}</td>
                            <td>
                                {{ $value->title }}
                            </td>
                            <td>
                                @if($value->from_type=='App\\User')
                                   @if($value->from)
                                   <a href="{{ url('admin/users/'.$value->from->id) }}" target="_blank">
                                       <span class="form_link">
                                         {{ $value->from->name }}
                                       </span>
                                   </a>
                                   @endif
                                @else
                                    <a href="{{ url('admin/sellers/'.$value->from->id) }}" target="_blank">
                                       <span class="form_link">
                                         {{ $value->from->brand_name }}
                                       </span>
                                   </a>
                                @endif
                            </td>
                            <td>
                             @if($value->to)
                               @if($value->to_type=='App\\User')
                                   <a href="{{ url('admin/users/'.$value->to->id) }}" target="_blank">
                                       <span class="to_link">
                                         {{ $value->to->name }}
                                       </span>
                                   </a>
                                @else
                                    <a href="{{ url('admin/sellers/'.$value->to->id) }}" target="_blank">
                                       <span class="to_link">
                                         {{ $value->to->brand_name }}
                                       </span>
                                     </a>
                                @endif
                             @elseif($value->to==0)
                                 <span class="to_link">
                                     {{ config('shop-info.shop_name') }}
                                 </span>
                             @endif

                            </td>
                            <td>{{ $jdf->jdate('H:i:s',$value->time)  }} / {{  $jdf->jdate('Y-n-j',$value->time) }}</td>
                            <td>
                                <a href="{{ url('admin/messages/'.$value->id) }}"><span class="fa fa-eye" @if($value->status==1) style="color:red" @endif></span></a>
                                @if($value->trashed())
                                    <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی پیام' onclick="restore_row('{{ url('admin/messages/'.$value->id) }}','{{ Session::token() }}','آیا از بازیابی این پیام مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                @endif

                                @if(!$value->trashed())
                                    <span data-toggle="tooltip" data-placement="bottom"  title='حذف پیام' onclick="del_row('{{ url('admin/messages/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این پیام مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @else
                                    <span data-toggle="tooltip" data-placement="bottom"  title='حذف پیام برای همیشه' onclick="del_row('{{ url('admin/messages/'.$value->id) }}','{{ Session::token() }}','آیا از حذف این پیام مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                     @if(sizeof($messages)==0)
                        <tr>
                            <td colspan="6">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                     @endif
                    </tbody>
                </table>
            </form>
            {{ $messages->links() }}
        </div>
    </div>

@endsection
