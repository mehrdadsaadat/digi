@extends('layouts.admin')

@section('content')

    @include('include.breadcrumb',['data'=>[['title'=>'مدیریت سفارشات','url'=>url('admin/orders')]]])
    <div class="panel">

        <div class="header">
            مدیریت سفارشات

            @include('include.item_table',['count'=>$trash_orders_count,'route'=>'admin/orders','title'=>'سفارش'])
        </div>

        <div class="panel_content">

            @include('include.Alert')
            <form method="get" class="search_form order_search">
               @if(isset($_GET['trashed']) && $_GET['trashed']==true)
                   <input type="hidden" name="trashed" value="true">
               @endif
               <input type="text" autocomplete="off" name="order_id" class="form-control" value="{{ $req->get('order_id','') }}" placeholder="شماره سفارش">
               <input type="text" autocomplete="off" name="first_date" style="margin-right: 10px" class="pdate form-control" id="pcal1" value="{{ $req->get('first_date','') }}" placeholder="شروع از تاریخ">
               <input type="text" autocomplete="off" name="end_date" style="margin: 0px 10px" class="pdate form-control" id="pcal2" value="{{ $req->get('end_date','') }}" placeholder="تا تاریخ">
               <button class="btn btn-primary">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                @csrf
                @include('include.orderList')
            </form>

            {{ $orders->links() }}
        </div>
    </div>

@endsection
@section('head')
    <link href="{{ asset('css/js-persian-cal.css') }}" rel="stylesheet">
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('js/js-persian-cal.min.js') }}"></script>
    <script>
        const pcal1= new AMIB.persianCalendar('pcal1');
        const pcal2= new AMIB.persianCalendar('pcal2');
    </script>
@endsection
