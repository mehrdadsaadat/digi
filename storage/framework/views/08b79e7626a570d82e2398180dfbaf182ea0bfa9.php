<?php $__env->startSection('content'); ?>

    <div class="row">

        <div class="col-md-3">
            <?php echo $__env->make('include.user_panel_menu',['active'=>'profile'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="col-md-9" style="padding-right: 0px">


            <span class="profile_menu_title" style="padding: 0px;margin-top: 20px">اطلاعات شخصی</span>
            <div class="profile_menu" style="padding-top: 20px">
                <table class="table table-bordered order_table_info">
                    <tr>
                        <td>
                            نام و نام خانوادگی :
                            <span>
                                <?php echo e(getUserPersonalData($additionalInfo,'first_name','last_name')); ?>

                            </span>
                        </td>
                        <td>
                            پست الکترنیکی :
                            <span><?php echo e(getUserPersonalData($additionalInfo,'email')); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            شماره تلفن همراه :
                            <span><?php echo e(Auth::user()->mobile); ?></span>
                        </td>
                        <td>
                            کد ملی :
                            <span><?php echo e(getUserPersonalData($additionalInfo,'national_identity_number')); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            دریافت خبرنامه :
                            <span>
                                <?php if(getUserPersonalData($additionalInfo,'newsletter')=='yes'): ?>
                                    بله
                                <?php else: ?>
                                   خیر
                                <?php endif; ?>
                            </span>
                        </td>
                        <td>
                            شماره کارت بانکی
                            <span><?php echo e(getUserPersonalData($additionalInfo,'bank_card_number')); ?></span>
                        </td>
                    </tr>

                    <?php if(!empty(getUserData('company_name',$additionalInfo))): ?>
                        <td>
                            نام شرکت :
                            <span>
                                <?php echo e(getUserPersonalData($additionalInfo,'company_name')); ?>

                            </span>
                        </td>
                        <td>
                            کد اقتصادی :
                            <span><?php echo e(getUserPersonalData($additionalInfo,'company_economic_number')); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            شناسه ملی :
                            <span><?php echo e(getUserPersonalData($additionalInfo,'company_national_identity_number')); ?></span>
                        </td>
                        <td>
                            شماره ثبت :
                            <span><?php echo e(getUserPersonalData($additionalInfo,'company_registration_number')); ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            شماره تلفن ثابت :
                            <span><?php echo e(getUserPersonalData($additionalInfo,'company_phone')); ?></span>
                        </td>
                        <td>
                           استان و شهر :
                           <?php if($additionalInfo): ?>
                             <span><?php echo e($additionalInfo->getProvince->name.' '.$additionalInfo->getCity->name); ?> </span>
                           <?php endif; ?>

                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td colspan="2">
                            <span  style="text-align: center">
                                <a class="data_link" href="<?php echo e(url('user/profile/additional-info')); ?>" style="font-size: 14px">
                                    <i class="fa fa-pencil"></i>
                                    ویرایش اطلاعات
                                </a>
                            </span>
                        </td>
                    </tr>

                </table>
            </div>

            <span class="profile_menu_title" style="padding: 0px">آخرین سفارش های من</span>
            <div class="profile_menu" style="padding-top: 20px">
                <?php echo $__env->make('include.user_order_list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.shop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/userPanel/profile.blade.php ENDPATH**/ ?>