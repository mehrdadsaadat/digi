<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('include.breadcrumb',['data'=>[['title'=>'مدیریت کاربران','url'=>url('admin/users')]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="panel">

        <div class="header">
            مدیریت کاربران

            <?php echo $__env->make('include.item_table',['count'=>$trash_user_count,'route'=>'admin/users','title'=>'کاربر'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <div class="panel_content">

            <?php echo $__env->make('include.Alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php $i=(isset($_GET['page'])) ? (($_GET['page']-1)*10): 0 ; $Jdf=new \App\Lib\Jdf(); ?>

            <form method="get" class="search_form">
                <?php if(isset($_GET['trashed']) && $_GET['trashed']==true): ?>
                    <input type="hidden" name="trashed" value="true">
                <?php endif; ?>
                <input type="text" name="name" class="form-control" value="<?php echo e($req->get('name','')); ?>" placeholder="نام کاربر">
                <input type="text" name="mobile" class="form-control" value="<?php echo e($req->get('mobile','')); ?>" placeholder="شماره موبایل">

                <select name="role" class="selectpicker">
                    <option <?php if($req->get('role','')=='admin'): ?> selected="selected" <?php endif; ?>   value="admin">مدیر</option>
                    <option <?php if($req->get('role','')=='user'): ?> selected="selected" <?php endif; ?>   value="user">کاربر عادی</option>
                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option <?php if($req->get('role','')==$role->id): ?> selected="selected" <?php endif; ?>   value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <button class="btn btn-primary" style="margin-right:80px">جست و جو</button>
            </form>
            <form method="post" id="data_form">
                <?php echo csrf_field(); ?>
                <table class="table table-bordered table-striped user_table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ردیف</th>
                        <th>نام کاربر</th>
                        <th>شماره موبایل</th>
                        <th>تاریخ عضویت</th>
                        <th>وضعیت</th>
                        <th>نقش کاربری</th>
                        <th>عمیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $i++; ?>
                        <tr>
                            <td>
                                <input type="checkbox" name="users_id[]" class="check_box" value="<?php echo e($value->id); ?>"/>
                            </td>
                            <td><?php echo e(replace_number($i)); ?></td>
                            <td>
                                <?php if(!empty($value->name)): ?>
                                   <?php echo e($value->name); ?>

                                <?php else: ?>
                                    ثبت نشده
                                <?php endif; ?>
                            </td>
                            <td><?php echo e(replace_number($value->mobile)); ?></td>
                            <td>
                                <?php
                                    $e=explode(' ',$value->created_at);
                                    $e2=explode('-',$e[0]);
                                ?>
                                <?php echo e(replace_number($Jdf->gregorian_to_jalali($e2[0],$e2[1],$e2[2],'-'))); ?>

                            </td>
                            <td>
                                <?php if($value['account_status']=='active'): ?>
                                    <span class="alert alert-success">فعال</span>
                                <?php else: ?>
                                   <span class="alert alert-danger">غیر فعال</span>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if($value->getRole): ?>
                                    <?php echo e($value->getRole->name); ?>

                                <?php elseif($value->role=='admin'): ?>
                                    مدیر
                                <?php else: ?>
                                   کاربر عادی
                                <?php endif; ?>
                            </td>
                            <td>
                                <a href="<?php echo e(url('admin/users/'.$value->id.'/messages')); ?>" style="color:black">
                                    <span data-toggle="tooltip" data-placement="bottom"  title='پیام ها' class="fa fa-comment-o"></span>
                                </a>
                                <?php if(!$value->trashed()): ?>
                                <a href="<?php echo e(url('admin/users/'.$value->id.'/edit')); ?>"><span class="fa fa-edit"></span></a>
                                <?php endif; ?>
                                <a href="<?php echo e(url('admin/users/'.$value->id)); ?>">
                                    <span  data-toggle="tooltip" data-placement="bottom"  title='سفارش های کاربر' class="fa fa-eye"></span>
                                </a>

                                <?php if($value->trashed()): ?>
                                   <span  data-toggle="tooltip" data-placement="bottom"  title='بازیابی کاربر' onclick="restore_row('<?php echo e(url('admin/users/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از بازیابی این کاربر مطمئن هستین ؟ ')" class="fa fa-refresh"></span>
                                <?php endif; ?>

                                <?php if(!$value->trashed()): ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف کاربر' onclick="del_row('<?php echo e(url('admin/users/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این کاربر مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                <?php else: ?>
                                <span data-toggle="tooltip" data-placement="bottom"  title='حذف کاربر برای همیشه' onclick="del_row('<?php echo e(url('admin/users/'.$value->id)); ?>','<?php echo e(Session::token()); ?>','آیا از حذف این کاربر مطمئن هستین ؟ ')" class="fa fa-remove"></span>
                                 <?php endif; ?>
                            </td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if(sizeof($users)==0): ?>
                        <tr>
                            <td colspan="8">رکوردی برای نمایش وجود ندارد</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </form>

            <?php echo e($users->links()); ?>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\digikala\resources\views/users/index.blade.php ENDPATH**/ ?>